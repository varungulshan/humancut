#!/bin/bash

localDir=/home/varun/shared/work/videoCut2/git/data/
remotePath=/users/varun/work/videoCut2/git/data/
remoteDir=adam:$remotePath
echo Syncing files from $localDir to $remoteDir
#rsync -rLPt --delete $remoteDir $localDir
rsync -rLt --exclude=*.svn --exclude=.gitignore $localDir $remoteDir

