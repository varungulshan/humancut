#!/bin/bash

localDir=/home/varun/shared/work/videoCut2/git/data/
remotePath=/home/varun/work/videoCut2/git/data/
remoteDir=titus:$remotePath
echo Syncing files from $localDir to $remoteDir
#rsync -rLPt --delete $remoteDir $localDir
rsync -rLt --exclude=*.svn --exclude=.gitignore $localDir $remoteDir

