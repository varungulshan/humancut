function trainTestJob(paramFile)
% Code to run a single validation, specified in paramFile

cd ..;
setup;

tmpLoad = load(paramFile); % Provides the variable tmpParams

params = tmpLoad.tmpParams;

trainParams = rmfield(params,{'testBench_cmd_train','testBench_cmd_validate'});

% Override some options for training
trainParams.testBench_cmd = params.testBench_cmd_train;
trainParams.rootOut_dir = [params.rootOut_dir 'train/'];

testParams = rmfield(params,{'testBench_cmd_train','testBench_cmd_validate'});
% Override options for testing
testParams.testBench_cmd = params.testBench_cmd_validate;
testParams.rootOut_dir = [params.rootOut_dir 'validate/'];
testParams.trainDir = trainParams.rootOut_dir;
testParams.evalOpts_string = params.opts.evalOpts_string;

grabCut_trained.trainAndTest(trainParams,testParams);
