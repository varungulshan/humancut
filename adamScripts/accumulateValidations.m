function accumulateValidations(rootDir)
% Accumulates the validations stored in rootDir and stores
% the best validation settings

cd ..;
setup;

scoreString = 'accuracyOverlap';

tmpLoad = load([rootDir 'params.mat']); % Provides the variable params
params = tmpLoad.params;

fH = fopen([rootDir 'validation_summary.txt'],'w');
testBenchOpts=eval(params.testBench_cmd);

numFolds = numel(testBenchOpts.trainTestBenchCmds);
numParameters = numel(params.opts.trainMethod_optsStrings);

perf = zeros(numParameters,numFolds);

for i=1:numParameters
  optsString = params.opts.trainMethod_optsStrings{i};
  for j=1:numFolds
    statsFile = [params.rootOut_dir 'validations/' ...
        sprintf('optsString_%s_foldNum_%d/validate/',...
        optsString,j) 'stats.mat'];
    tmpLoad = load(statsFile);
    perf(i,j) = getAvgPerf(tmpLoad.gStats,scoreString);
    fprintf(fH,'Parameter setting #%02d: %s, validation fold: %d, Score = %.2f\n',i,optsString,j,perf(i,j));
  end
  fprintf(fH,'\n');
end

perf = mean(perf,2);
for i=1:numParameters
  fprintf(fH,'Mean validation perf for parameter setting #%02d: %s = %.2f\n',i,params.opts.trainMethod_optsStrings{i},perf(i));
end

[bestPerf,bestI] = max(perf);
fprintf(fH,'\nBest parameter setting is #%02d: %s\n',bestI,...
    params.opts.trainMethod_optsStrings{bestI});

fclose(fH);

% Run train on the full dataset
trainParams.opts = rmfield(params.opts,'trainMethod_optsStrings');
trainParams.overWrite = params.overWrite;
trainParams.rootOut_dir = [params.rootOut_dir 'bestTrain/'];
trainParams.opts.trainMethod_optsString = params.opts.trainMethod_optsStrings{bestI};
trainParams.testBench_cmd = testBenchOpts.fullTrainTestBenchCmd;

grabCut_trained.trainGrabCut(trainParams);

% Run testing on both train and test dataset
if(params.testEnable)
  testParams = rmfield(params,{'testBench_cmd_test','testBench_cmd'});
  testParams.testBench_cmd = params.testBench_cmd_test;
  testParams.rootOut_dir = params.rootOut_dir_test;
  testParams.trainDir = trainParams.rootOut_dir;
  testParams.evalOpts_string = params.opts.evalOpts_string;

  % Run on test set
  grabCut_trained.testGrabCut(testParams);

  % Run on train set
  testParams.rootOut_dir = [params.rootOut_dir 'evaluateOnTrain/'];
  testParams.testBench_cmd = testBenchOpts.fullTrainTestBenchCmd;
  grabCut_trained.testGrabCut(testParams);
end

function perf = getAvgPerf(gStats,scoreString)

cmd = ['[gStats.stats_' scoreString '.score]'];
perf = eval(cmd);
perf = mean(perf);
