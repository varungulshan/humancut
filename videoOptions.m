function segmenterH=videoOptions(data)
% Function to make the opts structure for segmentation
%
import miscFns.*

% ------ Overwrite with options which were given from the ui ---------

switch(data.segMethod)
  case 'motionCut2'
    gtDir='./data/gtFirstFrame/';
    videoName=miscFns.extractVideoNames({data.pathname},{data.inFileName});
    videoName=videoName{1};
    gtFile=[gtDir videoName '.png'];
    vH=data.videoReaderH_copy;
    debugLevel=data.debugLevel;

    segOpts=motionCut2.helpers.makeOpts('rad40_handTune'); 
    segmenterH=motionCut2.segEngine(debugLevel,gtFile,vH,segOpts);
   case 'motionCut'
    gtDir='./data/gtFirstFrame/';
    videoName=miscFns.extractVideoNames({data.pathname},{data.inFileName});
    videoName=videoName{1};
    gtFile=[gtDir videoName '.png'];
    vH=data.videoReaderH_copy;
    debugLevel=data.debugLevel;

    segOpts=motionCut.helpers.makeOpts('rad40'); 
    segmenterH=motionCut.segEngine(debugLevel,gtFile,vH,segOpts);
  case 'snapCut2'
    gtDir='./data/gtFirstFrame/';
    videoName=miscFns.extractVideoNames({data.pathname},{data.inFileName});
    videoName=videoName{1};
    gtFile=[gtDir videoName '.png'];
    vH=data.videoReaderH_copy;
    debugLevel=data.debugLevel;

    segOpts=snapCut2.helpers.makeOpts('rad40_sigma11'); 
    segmenterH=snapCut2.segEngine(debugLevel,gtFile,vH,segOpts);

  case 'flowProp_edgePbNN'
    flowDir='./pp/flows1/';
    flowDir=[flowDir extractImgName(data.inFileName) '/'];
    edgeDir='./pp/globalPb1/';
    edgeDir=[edgeDir extractImgName(data.inFileName) '/'];
    gtDir='./data/gtFirstFrame/';
    gtFile=[gtDir extractImgName(data.inFileName) '.png'];
    vH=data.videoReaderH_copy;
    debugLevel=data.debugLevel;
    %flowOpts=makeFlowOpts('try1');

    %gcOpts.gamma=300;
    gcOpts.gamma=data.gamma;
    gcOpts.unaryConstant=10;
    gcOpts.gcScale=1000;
    gcOpts.gamma_ising=0.1; 
    gcOpts.nbrHoodType='nbr4';
    %gcOpts.edgeThresh=0.13;
    gcOpts.edgeThresh=0;
    segmenterH=flowPropagatorEdgeLearn2(debugLevel,flowDir,edgeDir,gtFile,vH,gcOpts);

  case 'flowProp_edgePb'
    flowDir='./pp/flows1/';
    flowDir=[flowDir extractImgName(data.inFileName) '/'];
    edgeDir='./pp/globalPb1/';
    edgeDir=[edgeDir extractImgName(data.inFileName) '/'];
    gtDir='./data/gtFirstFrame/';
    gtFile=[gtDir extractImgName(data.inFileName) '.png'];
    vH=data.videoReaderH_copy;
    debugLevel=data.debugLevel;
    %flowOpts=makeFlowOpts('try1');

    %gcOpts.gamma=300;
    gcOpts.gamma=data.gamma;
    gcOpts.unaryConstant=10;
    gcOpts.gcScale=1000;
    gcOpts.gamma_ising=0.1; 
    gcOpts.nbrHoodType='nbr4';
    gcOpts.edgeThresh=0.13;
    segmenterH=flowPropagatorEdgeMap(debugLevel,flowDir,edgeDir,gtFile,vH,gcOpts);

  case 'flowProp_gc'
    flowDir='./pp/flows1/';
    flowDir=[flowDir extractImgName(data.inFileName) '/'];
    gtDir='./data/gtFirstFrame/';
    gtFile=[gtDir extractImgName(data.inFileName) '.png'];
    vH=data.videoReaderH_copy;
    debugLevel=data.debugLevel;
    %flowOpts=makeFlowOpts('try1');

    %gcOpts.gamma=300;
    %if(isfield(data,'gamma'))
      gcOpts.gamma=data.gamma;
    %end
    gcOpts.gamma_ising=0.1; 
    gcOpts.sigma_c='auto';
    gcOpts.unaryConstant=10;
    gcOpts.gcScale=1000;
    gcOpts.nbrHoodType='nbr4';
    segmenterH=flowPropagatorGC(debugLevel,flowDir,gtFile,vH,gcOpts);

  case 'flowProp_try1'
    gamma=300;
    if(isfield(data,'gamma'))
      gamma=data.gamma;
    end
    flowDir='./pp/flows1/';
    flowDir=[flowDir extractImgName(data.inFileName) '/'];
    gtDir='./data/gtFirstFrame/';
    gtFile=[gtDir extractImgName(data.inFileName) '.png'];
    vH=data.videoReaderH_copy;
    debugLevel=data.debugLevel;
    %flowOpts=makeFlowOpts('try1');
    segmenterH=flowPropagatorGT(gamma,debugLevel,flowDir,gtFile,vH);

  case 'Use text file'
    % Override the above options with those from the UI
    if(isfield(data,'gamma'))
      opts.segOpts.gamma=data.gamma;
    end

 otherwise
   error('Invalid method: %s\n',data.segMethod);

end

opts.debugInfo=1;
