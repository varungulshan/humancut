function img=flowImg_withWheel(flow,maxFlow_rad)

if(exist('maxFlow_rad','var'))
  img=broxFlow.flowToColor(flow,maxFlow_rad);
else
  img=broxFlow.flowToColor(flow);
end

cwd=miscFns.extractDirPath(mfilename('fullpath'));
colorWheel=imread([cwd 'colorWheel.png']);

h=size(img,1);w=size(img,2);
[hWheel wWheel nCh_wheel]=size(colorWheel);
img(1:hWheel,w-wWheel+1:w,:)=colorWheel;
