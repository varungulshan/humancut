function compile_mex()

cwd=miscFns.extractDirPath(mfilename('fullpath'));

mexCmds=cell(0,1);

%cvFlags='-I/usr/include/opencv -lcxcore';
cvFlags=['-I/' cwd '../../../../software/OpenCV-2.1.0/include/opencv/ -lcxcore -L' cwd '../../../../software/OpenCV-2.1.0/release/lib/'];

mexCmds{end+1}=sprintf('mex -O %s+cpp/mex_addLabels3.cpp %s+cpp/heaps/f_heap.cc -outdir %s+cpp/',cwd,cwd,cwd);
mexCmds{end+1}=sprintf('mex -O %s+cpp/mex_addShapePosterior.cpp %s+cpp/heaps/f_heap.cc -outdir %s+cpp/',cwd,cwd,cwd);
mexCmds{end+1}=sprintf('mex -O %s+cpp/mex_partShapePrior.cpp %s+cpp/heaps/f_heap.cc -outdir %s+cpp/',cwd,cwd,cwd);

for i=1:length(mexCmds)
  fprintf('Executing %s\n',mexCmds{i});
  eval(mexCmds{i});
end
