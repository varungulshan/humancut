function initPartColorModels(obj)

partPixels=cell(1,obj.numParts);

partList=obj.allParts.partList;

for i=1:obj.numParts
  iPart=partList(i);
  if(iPart.valid)
    partPixels{i}=[partPixels{i} iPart.insidePixels];
  end
end

for i=1:obj.numParts
  if(~isempty(partPixels{i}))
    tmpFeatures=obj.features(:,partPixels{i});
    obj.partColorModels(i)=gmm.init_gmmBS(tmpFeatures,obj.opts.nMixtures_part);
  end
end
