function X=getHeadX(obj)

K=obj.jointNumByName;
marked=obj.jointMarked;
modelVector=obj.modelVector;
modelIdx=obj.jointIds;

%torsoIds=[K.L_Shoulder K.R_Shoulder K.L_Hip K.R_Hip];
torsoIds=[K.L_Shoulder K.R_Shoulder K.L_Hip]; % Remove one of the torso joints
% because it is centroid of torso is set to origin, causing rank deficiency
eyeIds=[K.L_Eye K.R_Eye];
allIds = [torsoIds eyeIds];

assert(all(marked(allIds)),'Eyes and torso should be fully marked\n');

origin = obj.getTorsoOrigin();
torsoLength=obj.getTorsoScale();

allCoordsIdx=vertcat(modelIdx{allIds})';
allCoords=reshape(modelVector(allCoordsIdx(:)),[2 numel(allIds)]);
allCoords=(allCoords-repmat(origin,[1 numel(allIds)]))/torsoLength;
allCoords=allCoords(:);
X=[allCoords;1]; % 1 for bias factor

assert(numel(X)==11);
