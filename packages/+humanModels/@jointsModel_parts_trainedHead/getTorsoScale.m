function torsoLength=getTorsoScale(obj)

K=obj.jointNumByName;
marked=obj.jointMarked;
modelVector=obj.modelVector;
modelIdx=obj.jointIds;

torsoIds=[K.L_Shoulder K.R_Shoulder K.L_Hip K.R_Hip];

assert(all(marked(torsoIds)),'Torso should be fully marked\n');

meanShoulder=mean([modelVector(modelIdx{K.L_Shoulder}) ...
                   modelVector(modelIdx{K.R_Shoulder})],2);
meanHip=mean([modelVector(modelIdx{K.L_Hip}) ...
              modelVector(modelIdx{K.R_Hip})],2);

torsoLength=norm(meanHip-meanShoulder); % Default = L2 norm
