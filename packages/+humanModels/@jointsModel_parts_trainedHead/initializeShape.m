function intializeShape(obj,headMask,headMaskCenter,img)

[h w nCh]=size(img);
obj.features=humanSegmenters.helpers.extractPixels(img);
obj.imgSize=[h w];
obj.syncPart(headMask,headMaskCenter);

partColorModels=struct('mu',[],'sigma',[],'pi',[]);
partColorModels(obj.numParts).mu=[];

obj.partColorModels=partColorModels;
obj.initPartColorModels();
obj.updateColorLikelihoods();
