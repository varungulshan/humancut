function center=getEyesCenter(obj)

K=obj.jointNumByName;
marked=obj.jointMarked;
modelVector=obj.modelVector;
modelIdx=obj.jointIds;

ids=[K.L_Eye K.R_Eye];

assert(all(marked(ids)),'Eye should be fully marked\n');

coordsIdx=vertcat(modelIdx{ids})';
assert(all(size(coordsIdx)==[2 numel(ids)]),'Invalid sizes');
coords = reshape(modelVector(coordsIdx(:)),[2 numel(ids)]);
center = mean(coords,2);
