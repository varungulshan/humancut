function initialize(obj,anno,K)
%function initialize(obj,anno,K,img)


M=length(obj.jointName);
obj.modelVector=zeros(obj.dimensionality,1);
obj.jointVisible=false(M,1);
obj.jointMarked=false(M,1);
obj.jointDepth=zeros(M,1);

coords=anno.coords(:,1:2);
visible=anno.visible;
marked=anno.marked;
layerId=anno.layerNum;

if(marked(K.R_Eye) && ~marked(K.L_Eye))
  coords(K.L_Eye,:)=coords(K.R_Eye,:);
  marked(K.L_Eye)=true;
elseif(marked(K.L_Eye) && ~marked(K.R_Eye))
  coords(K.R_Eye,:)=coords(K.L_Eye,:);
  marked(K.R_Eye)=true;
end

for i=1:M
  jName=obj.jointName{i};
  K_idx=eval(['K.' jName]);
  if(marked(K_idx))
    vectorIds=obj.jointIds{i};
    % vector ids should be 1 x 2, with first coordinate as x, second as y
    % assert(numel(vectorIds)==2);
    obj.modelVector(vectorIds,1)=coords(K_idx,:);
    obj.jointVisible(i,1)=visible(K_idx);
    obj.jointMarked(i,1)=true;
    obj.jointDepth(i,1)=layerId(K_idx);
  end
end

% Skipping the regular initialization, will move it to a separate function
%obj.imgSize=[size(img,1) size(img,2)];
%obj.allParts=[];
%obj.syncPart(false); % false means color models have not yet been intialized

%obj.features=humanSegmenters.helpers.extractPixels(img);
%partColorModels=struct('mu',[],'sigma',[],'pi',[]);
%partColorModels(obj.numParts).mu=[];

%obj.partColorModels=partColorModels;
%obj.initPartColorModels();
%obj.updateColorLikelihoods();
