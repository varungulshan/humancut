function X=getHeadX(obj)

K=obj.jointNumByName;
marked=obj.jointMarked;
modelVector=obj.modelVector;
modelIdx=obj.jointIds;

torsoIds=[K.L_Shoulder K.R_Shoulder K.L_Hip K.R_Hip];
eyeIds=[K.L_Eye K.R_Eye];
allIds = [torsoIds eyeIds];

assert(all(marked(allIds)),'Eyes and torso should be fully marked\n');

origin = obj.getTorsoOrigin();
torsoLength=obj.getTorsoScale();

allCoordsIdx=vertcat(modelIdx{allIds})';
allCoords=reshape(modelVector(allCoordsIdx(:)),[2 numel(allIds)]);
allCoords=(allCoords-repmat(origin,[1 numel(allIds)]))/torsoLength;
allCoords=allCoords(:);
X=allCoords;

assert(numel(X)==12);
