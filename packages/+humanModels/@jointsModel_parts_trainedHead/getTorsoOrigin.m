function origin=getTorsoOrigin(obj)

K=obj.jointNumByName;
marked=obj.jointMarked;
modelVector=obj.modelVector;
modelIdx=obj.jointIds;

torsoIds=[K.L_Shoulder K.R_Shoulder K.L_Hip K.R_Hip];

assert(all(marked(torsoIds)),'Torso should be fully marked\n');

torsoCoordsIdx=vertcat(modelIdx{torsoIds})';
assert(all(size(torsoCoordsIdx)==[2 numel(torsoIds)]),'Invalid sizes');
torsoCoords=reshape(modelVector(torsoCoordsIdx(:)),[2 numel(torsoIds)]);
origin = mean(torsoCoords,2);
