function updatePartColors(obj,seg)

partPixels=cell(1,obj.numParts);

partList=obj.allParts.partList;
for i=1:obj.numParts
  iPart=partList(i);
  if(iPart.valid)
    pixelIdx=iPart.boundaryPixels;
    pixelIdx_mask=(seg(pixelIdx)==255);
    pixelIdx=pixelIdx(pixelIdx_mask);
    partPixels{i}=[partPixels{i} pixelIdx];
  end
end

for i=1:obj.numParts
  if(~isempty(partPixels{i}))
    tmpFeatures=obj.features(:,partPixels{i});
    %assert(~isempty(obj.partColorModels(i).mu),'Gmm should already exist for part in update\n');
    if(isempty(obj.partColorModels(i).mu))
      % Empty means that this part has appeared, either it didnt exist before, or
      % it was outside the image boundaries and could not be initialized
      % so initialize its color model now
      obj.partColorModels(i)=gmm.init_gmmBS(tmpFeatures,obj.opts.nMixtures_part);
    else
      obj.partColorModels(i)=gmm.updateGmm(tmpFeatures,obj.partColorModels(i),...
                            obj.opts.gmmUpdate_iters);
    end
  end
end

obj.updateColorLikelihoods();
