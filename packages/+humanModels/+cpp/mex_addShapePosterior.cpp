/*=================================================================
% Adds shape posterior to existing one, around a polygonal part within a certain
% distance trasnformed region of the part
% 
% shapePosterior=mex_addShapePosterior(shapePosterior,mask,border)
% Inputs:
%   shapePosterior -> [h x w] double, the current shape posterior. This function
%                    will take a max of the current posterior and the computed one
%   mask ->  [h x w] logical, the mask denoting the part form which distance transform
%            is to be taken
%   border -> double, scalar. The border around the part upto which shape posterior needs
%             to be modified. The shape posterior is 1 at d=0 from part. Is 0 at
%             d=border from the part. And it varies linearly in between.
*=================================================================*/

#include "perform_front_propagation_2d_color.h"
#include "heaps/f_heap.hpp"

#define kDead -1
#define kOpen 0
#define kFar 1

/* Global variables */
int n;			// height
int p;			// width
int nPixels; // The number of pixels = n*p
int nbrHoodSize;
double* D = NULL;
double* S = NULL;

#define ACCESS_ARRAY(a,i,j) a[(i)+n*(j)]
#define ACCESS_ARRAY_3D(a,i,j,k) a[(i)+n*(j)+k*nPixels]
#define D_(i,j) ACCESS_ARRAY(D,i,j)
#define S_(i,j) ACCESS_ARRAY(S,i,j)
#define heap_pool_(i,j) ACCESS_ARRAY(heap_pool,i,j)

class point
{
public:
	int i,j;
	point( int ii, int jj )
	{ i = ii; j = jj; }

  friend int operator<(const point& rhs,const point& lhs){
		return D_(lhs.i,lhs.j)< D_(rhs.i,rhs.j);
  }
  // Note: The meaning of the < operator is reversed, because using a max heap here

};

using namespace boost;
fibonacci_heap<point>::pointer* heap_pool = NULL;

typedef std::vector<point*> point_list;

void mexFunction(	int nlhs, mxArray *plhs[], 
				 int nrhs, const mxArray*prhs[] ) 
{ 
	/* retrive arguments */
	if( nrhs!=3 ) 
		mexErrMsgTxt("3 input arguments are required."); 
	if( nlhs!=1 ) 
		mexErrMsgTxt("1 output arguments are required."); 

  if(mxGetClassID(prhs[0])!=mxDOUBLE_CLASS)
    mexErrMsgTxt("prhs[0] (shapePosterior) should be uint8\n");
  if(mxGetClassID(prhs[1])!=mxLOGICAL_CLASS)
    mexErrMsgTxt("prhs[1] (mask) should be logical\n");
  if(mxGetClassID(prhs[2])!=mxDOUBLE_CLASS)
    mexErrMsgTxt("prhs[2] (border) should be double\n");

	// first argument : weight list
	n = mxGetM(prhs[0]); 
	p = mxGetN(prhs[0]);

  nPixels=n*p;
  if(mxGetM(prhs[1])!=n || mxGetN(prhs[1])!=p)
    mexErrMsgTxt("Dimension of mask not same as labelImg!\n");

  mxAssert(mxGetNumberOfElements(prhs[2])==1,"border should be a scalar\n");

  plhs[0]=mxCreateNumericMatrix(n,p,mxDOUBLE_CLASS,mxREAL);
  double *shapePosterior=mxGetPr(plhs[0]);
  memcpy(shapePosterior,mxGetPr(prhs[0]),nPixels*sizeof(double));

  double border=*mxGetPr(prhs[2]);

  nbrHoodSize=8;

	// create the Fibonacci heap
  fibonacci_heap<point> open_heap;
  D=(double*)mxMalloc(nPixels*sizeof(double));
  S=(double*)mxMalloc(nPixels*sizeof(double));

	// initialize points
	for( int i=0; i<n; ++i )
	for( int j=0; j<p; ++j )
	{
		D_(i,j) = GW_INFINITE;
		S_(i,j) = kFar;
	}

	// record all the points
	heap_pool = new fibonacci_heap<point>::pointer[n*p]; 
	memset( heap_pool, NULL, n*p*sizeof(fibonacci_heap<point>::pointer) );

	// inialize open list
	point_list existing_points;
  bool *mask_ptr=(bool*)mxGetData(prhs[1]);
  for(int j=0;j<p;j++){
    for(int i=0;i<n;i++){
      if(*mask_ptr){
		    point* pt = new point( i,j );
		    existing_points.push_back( pt );			// for deleting at the end
		    D_( i,j ) = 0;
		    S_( i,j ) = kOpen;
		    heap_pool_(i,j) = open_heap.push(*pt);			// add to heap
      }
      mask_ptr++;
    }
  }

  if(nbrHoodSize==8){
	  bool stop_iteration = false;
    int iOffsets[8] = {1,1,0,-1,-1,-1,0,1};
    int jOffsets[8] = {0,1,1,1,0,-1,-1,-1};
    double eucledianConstants[8];
    for(int k=0;k<8;k++){
      eucledianConstants[k]=sqrt((SQR(iOffsets[k])+SQR(jOffsets[k])));
    }

    while( !open_heap.empty() && !stop_iteration )
    {

      // current point
      const point& cur_point = open_heap.top();
      open_heap.pop();
      int i = cur_point.i;
      int j = cur_point.j;
      heap_pool_(i,j) = NULL;
      S_(i,j) = kDead;
      double curD=D_(i,j);
      if(curD>border){
          stop_iteration=true;
      }
      else{
        int idx=i+n*j;
        double curPosterior=1-curD/border;
        shapePosterior[idx]=std::max(shapePosterior[idx],curPosterior);
      }

      for( int k=0; k<8; ++k )
      {
        int ii = i+iOffsets[k];
        int jj = j+jOffsets[k];

        if( ii>=0 && jj>=0 && ii<n && jj<p)
        {
          double A1 = D_(i,j) + eucledianConstants[k];
          if( ((int) S_(ii,jj)) == kDead )
          {}
          else if( ((int) S_(ii,jj)) == kOpen )
          {
            // check if action has change.
            if( A1<D_(ii,jj) )
            {
              D_(ii,jj) = A1;
              // Modify the value in the heap
              fibonacci_heap<point>::pointer cur_el = heap_pool_(ii,jj);
              if( cur_el!=NULL ){
                open_heap.increase(cur_el, cur_el->data() );	// use same data for update
                // Its increase instead of decrease because using a max_heap now!!
              }
              else
                mexErrMsgTxt("Error in heap pool allocation."); 
            }
          }
          else if( ((int) S_(ii,jj)) == kFar )
          {
            if(A1<D_(ii,jj)){

              S_(ii,jj) = kOpen;
              D_(ii,jj) = A1;
              // add to open list
              point* pt = new point(ii,jj);
              existing_points.push_back( pt );
              heap_pool_(ii,jj) = open_heap.push(*pt );			// add to heap	
            }
          }
          else 
            mexErrMsgTxt("Unkwnown state."); 

        }
      }		// end for
    }			// end while
  }
  else{mexErrMsgTxt("Invalid neighbourhood size\n");}

	// free point pool
	for( point_list::iterator it = existing_points.begin(); it!=existing_points.end(); ++it )
		GW_DELETE( *it );
	// free fibheap pool
	GW_DELETEARRAY(heap_pool);
  mxFree(S);mxFree(D);
	return;
}
