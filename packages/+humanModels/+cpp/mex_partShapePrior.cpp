/*=================================================================
% Adds shape posterior to existing one, around a polygonal part within a certain
% distance trasnformed region of the part
% 
% [borderPixels,shapePrior]=mex_addShapePosterior(inPixels,border,imgSize)
% Inputs:
%   inPixels -> [1 x N] pixels from which to take distance transform. (int32), indexed
%               starting from 1 (i.e matlab style)
%   border -> double, scalar. The border around the part upto which shape posterior needs
%             to be modified. The shape posterior is 1 at d=0 from part. Is 0 at
%             d=border from the part. And it varies linearly in between.
%   imgSize -> 1 x 2 array (int32) denoting the image size
% Outputs:
%   borderPixels -> [1 x M] int32 pixel indices (1-based) of pixels including the inPixels
%                   and of width border around it.
%   shapePrior  -> [1 x M] double pixel posteriors given by distance transform
*=================================================================*/

#include "perform_front_propagation_2d_color.h"
#include "heaps/f_heap.hpp"
#include <vector>
#include <assert.h>

#define kDead -1
#define kOpen 0
#define kFar 1

/* Global variables */
int n;			// height
int p;			// width
int nPixels; // The number of pixels = n*p
int nbrHoodSize;
double* D = NULL;
double* S = NULL;

#define ACCESS_ARRAY(a,i,j) a[(i)+n*(j)]
#define ACCESS_ARRAY_3D(a,i,j,k) a[(i)+n*(j)+k*nPixels]
#define D_(i,j) ACCESS_ARRAY(D,i,j)
#define S_(i,j) ACCESS_ARRAY(S,i,j)
#define heap_pool_(i,j) ACCESS_ARRAY(heap_pool,i,j)

class point
{
public:
	int i,j;
	point( int ii, int jj )
	{ i = ii; j = jj; }

  friend int operator<(const point& rhs,const point& lhs){
		return D_(lhs.i,lhs.j)< D_(rhs.i,rhs.j);
  }
  // Note: The meaning of the < operator is reversed, because using a max heap here

};

using namespace boost;
using namespace std;
fibonacci_heap<point>::pointer* heap_pool = NULL;

typedef std::vector<point*> point_list;

void mexFunction(	int nlhs, mxArray *plhs[], 
				 int nrhs, const mxArray*prhs[] ) 
{ 
	/* retrive arguments */
	if( nrhs!=3 ) 
		mexErrMsgTxt("3 input arguments are required."); 
	if( nlhs!=2 ) 
		mexErrMsgTxt("2 output arguments are required."); 

  if(mxGetClassID(prhs[0])!=mxINT32_CLASS)
    mexErrMsgTxt("prhs[0] (inPixels) should be int32\n");
  if(mxGetClassID(prhs[1])!=mxDOUBLE_CLASS)
    mexErrMsgTxt("prhs[1] (border) should be double\n");
  if(mxGetClassID(prhs[2])!=mxINT32_CLASS)
    mexErrMsgTxt("prhs[2] (imgSize) should be int32\n");

  int num_inPixels=mxGetNumberOfElements(prhs[0]);
  const int *inPixels=(int*)mxGetData(prhs[0]);

  mxAssert(mxGetNumberOfElements(prhs[1])==1,"border should be a scalar\n");
  double border=*mxGetPr(prhs[1]);

  mxAssert(mxGetNumberOfElements(prhs[2])==2,"img size should have two elements\n");
  const int *imgSize_ptr=(int*)mxGetData(prhs[2]);

	// first argument : weight list
	n = imgSize_ptr[0]; 
	p = imgSize_ptr[1];

  nPixels=n*p;

  vector<int> borderPixels;
  vector<double> shapePrior;

  nbrHoodSize=8;

	// create the Fibonacci heap
  fibonacci_heap<point> open_heap;
  D=(double*)mxMalloc(nPixels*sizeof(double));
  S=(double*)mxMalloc(nPixels*sizeof(double));

	// initialize points
	for( int i=0; i<n; ++i )
	for( int j=0; j<p; ++j )
	{
		D_(i,j) = GW_INFINITE;
		S_(i,j) = kFar;
	}

	// record all the points
	heap_pool = new fibonacci_heap<point>::pointer[n*p]; 
	memset( heap_pool, NULL, n*p*sizeof(fibonacci_heap<point>::pointer) );

	// inialize open list
	point_list existing_points;

  for(int i=0;i<num_inPixels;i++){
    int pixelIdx=inPixels[i]-1;
    int y=pixelIdx%n;
    int x=pixelIdx/n;
		point* pt = new point( y,x );
		D_( y,x ) = 0;
		S_( y,x ) = kOpen;
		heap_pool_(y,x) = open_heap.push(*pt);			// add to heap
		existing_points.push_back( pt );			// for deleting at the end
  }

  if(nbrHoodSize==8){
	  bool stop_iteration = false;
    int iOffsets[8] = {1,1,0,-1,-1,-1,0,1};
    int jOffsets[8] = {0,1,1,1,0,-1,-1,-1};
    double eucledianConstants[8];
    for(int k=0;k<8;k++){
      eucledianConstants[k]=sqrt((SQR(iOffsets[k])+SQR(jOffsets[k])));
    }

    while( !open_heap.empty() && !stop_iteration )
    {

      // current point
      const point& cur_point = open_heap.top();
      open_heap.pop();
      int i = cur_point.i; // y coordinate
      int j = cur_point.j; // x coordinate
      heap_pool_(i,j) = NULL;
      S_(i,j) = kDead;
      double curD=D_(i,j);
      if(curD>border){
          stop_iteration=true;
      }
      else{
        int idx=i+n*j;
        double curPosterior=1-curD/border;
        borderPixels.push_back(idx+1);
        shapePrior.push_back(curPosterior);
      }

      for( int k=0; k<8; ++k )
      {
        int ii = i+iOffsets[k];
        int jj = j+jOffsets[k];

        if( ii>=0 && jj>=0 && ii<n && jj<p)
        {
          double A1 = D_(i,j) + eucledianConstants[k];
          if( ((int) S_(ii,jj)) == kDead )
          {}
          else if( ((int) S_(ii,jj)) == kOpen )
          {
            // check if action has change.
            if( A1<D_(ii,jj) )
            {
              D_(ii,jj) = A1;
              // Modify the value in the heap
              fibonacci_heap<point>::pointer cur_el = heap_pool_(ii,jj);
              if( cur_el!=NULL ){
                open_heap.increase(cur_el, cur_el->data() );	// use same data for update
                // Its increase instead of decrease because using a max_heap now!!
              }
              else
                mexErrMsgTxt("Error in heap pool allocation."); 
            }
          }
          else if( ((int) S_(ii,jj)) == kFar )
          {
            if(A1<D_(ii,jj)){

              S_(ii,jj) = kOpen;
              D_(ii,jj) = A1;
              // add to open list
              point* pt = new point(ii,jj);
              existing_points.push_back( pt );
              heap_pool_(ii,jj) = open_heap.push(*pt );			// add to heap	
            }
          }
          else 
            mexErrMsgTxt("Unkwnown state."); 

        }
      }		// end for
    }			// end while
  }
  else{mexErrMsgTxt("Invalid neighbourhood size\n");}

	// free point pool
	for( point_list::iterator it = existing_points.begin(); it!=existing_points.end(); ++it )
		GW_DELETE( *it );
	// free fibheap pool
	GW_DELETEARRAY(heap_pool);
  mxFree(S);mxFree(D);

  plhs[0]=mxCreateNumericMatrix(1,borderPixels.size(),mxINT32_CLASS,mxREAL);
  plhs[1]=mxCreateNumericMatrix(1,shapePrior.size(),mxDOUBLE_CLASS,mxREAL);
  int *borderPixels_ptr=(int*)mxGetData(plhs[0]);
  double *shapePrior_ptr=mxGetPr(plhs[1]);

  assert(borderPixels.size()==shapePrior.size());
  for(int i=0;i<borderPixels.size();i++,borderPixels_ptr++,shapePrior_ptr++){
    *borderPixels_ptr=borderPixels[i];
    *shapePrior_ptr=shapePrior[i];
  }

	return;
}
