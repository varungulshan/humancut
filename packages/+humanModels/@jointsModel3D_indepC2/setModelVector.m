function setModelVector(obj,modelVector,iFrame)

assert(size(obj.modelVector,1)==size(modelVector,1),'Incorrect modelVector dimensions\n');
assert(~obj.constrained(iFrame),'Cant set model vector for a constrained frame\n');
obj.modelVector(:,iFrame)=modelVector;
