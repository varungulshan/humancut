function initialize(obj,initMethod,varargin)
% initMethod='h3d', varargin: annos,K

switch(initMethod)
  case 'h3d'
    assert(length(varargin)==2,'Invalid arguments for h3d init method\n');
    annos=varargin{1};
    K=varargin{2};
    initialize_fromH3D(obj,annos,K);
  otherwise
    error('Invalid initMethod %s for jointsModel\n',initMethod);
end

function initialize_fromH3D(obj,annos,K)

nFrames=length(annos);
obj.nFrames=nFrames;
M=length(obj.jointName);
obj.modelVector=zeros(obj.dimensionality,nFrames);
obj.jointVisible=logical(zeros(M,nFrames));
obj.jointMarked=logical(zeros(M,nFrames));
obj.jointDepth=zeros(M,nFrames);

for j=1:length(annos)
  anno=annos(j);
  coords=anno.coords(:,1:2);
  visible=anno.visible;
  marked=anno.marked;
  layerId=anno.layerNum;
  
  for i=1:M
    jName=obj.jointName{i};
    switch(jName)
      case 'HeadTop'
        if(marked(K.R_Eye) && marked(K.L_Eye))
          vectorIds=obj.jointIds{i};
          % vector ids should be 1 x 2, with first coordinate as x, second as y
          % assert(numel(vectorIds)==2);
          obj.modelVector(vectorIds,j)=mean([coords(K.L_Eye,:);coords(K.R_Eye,:)],1);
          obj.jointVisible(i,j)=all(visible([K.L_Eye K.R_Eye]));
          obj.jointMarked(i,j)=true;
          obj.jointDepth(i,j)=min(layerId([K.L_Eye K.R_Eye]));
        end
      otherwise
        K_idx=eval(['K.' jName]);
        if(marked(K_idx))
          vectorIds=obj.jointIds{i};
          % vector ids should be 1 x 2, with first coordinate as x, second as y
          % assert(numel(vectorIds)==2);
          obj.modelVector(vectorIds,j)=coords(K_idx,:);
          obj.jointVisible(i,j)=visible(K_idx);
          obj.jointMarked(i,j)=true;
          obj.jointDepth(i,j)=layerId(K_idx);
        end
    end
  end
end
