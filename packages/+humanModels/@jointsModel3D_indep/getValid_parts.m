function allParts=getValid_parts(obj,iFrame)

K=obj.jointNumByName;
marked=obj.jointMarked(:,iFrame);
layerId=obj.jointDepth(:,iFrame);
coords=obj.modelVector(:,iFrame)';
modelIdx=obj.jointIds;
opts=obj.opts;
allParts=struct([]);
partNum=1;

% Process hands and feet
pairIdxs=[K.R_Elbow K.R_Wrist;...
          K.L_Elbow K.L_Wrist;...
          K.R_Knee K.R_Ankle;...
          K.L_Knee K.L_Ankle;...
         ];
names={'Right hand';...
       'Left hand';...
       'Right ankle';...
       'Left ankle';...
       };
widths=[...
        opts.hand_width; ...
        opts.hand_width; ...
        opts.foot_width; ...
        opts.foot_width; ...
      ];
borders=[...
          opts.hand_border; ...
          opts.hand_border; ...
          opts.foot_border; ...
          opts.foot_border; ...
        ];
for i=1:size(pairIdxs,1)
  idx1=pairIdxs(i,1);
  idx2=pairIdxs(i,2);
  if(marked(idx1) && marked(idx2))
    [polygon,axisM]=getHandPolygon_rel(coords(modelIdx{idx1}),...
                                        coords(modelIdx{idx2}),...
                                        widths(i)); 
    axisLength=norm(axisM(1:2)-axisM(3:4));
    allParts(partNum).polygon=polygon;
    allParts(partNum).medialAxis=axisM;
    allParts(partNum).layerId=layerId(idx2);
    allParts(partNum).border=borders(i)*axisLength;
    allParts(partNum).name=names{i};
    partNum=partNum+1;
  end
end

% Process lower arms, upper arms, lower legs, upper legs
pairIdxs=[K.R_Wrist K.R_Elbow;... % right lower arm
          K.L_Wrist K.L_Elbow;... % left lower arm
          K.R_Shoulder K.R_Elbow; ... % right upper arm
          K.L_Shoulder K.L_Elbow; ... % left upper arm
          K.R_Hip K.R_Knee; ... % right upper leg
          K.L_Hip K.L_Knee; ... % left upper leg
          K.R_Ankle K.R_Knee; ... % right lower leg
          K.L_Ankle K.L_Knee; ... % left lower leg
          ];
names={'Right lower arm';...
       'Left lower arm';...
       'Right upper arm';...
       'Left upper arm';...
       'Right upper leg';...
       'Left upper leg';...
       'Right lower leg';...
       'Left lower leg';...
};
halfWidths=[opts.lowerArm_halfWidth;...
            opts.lowerArm_halfWidth;...
            opts.upperArm_halfWidth;...
            opts.upperArm_halfWidth;...
            opts.upperLeg_halfWidth;...
            opts.upperLeg_halfWidth;...
            opts.lowerLeg_halfWidth;...
            opts.lowerLeg_halfWidth;...
            ];
borders=[opts.lowerArm_border;...
         opts.lowerArm_border;...
         opts.upperArm_border;...
         opts.upperArm_border;...
         opts.upperLeg_border;...
         opts.upperLeg_border;...
         opts.lowerLeg_border;...
         opts.lowerLeg_border;...
        ];

for i=1:size(pairIdxs,1)
  idx1=pairIdxs(i,1);
  idx2=pairIdxs(i,2);
  if(marked(idx1) && marked(idx2))
    [polygon,axisM]=getPolygonFromMedian_rel(coords(modelIdx{idx1}),...
                                             coords(modelIdx{idx2}),...
                                             halfWidths(i)); 
    axisLength=norm(axisM(1:2)-axisM(3:4));
    allParts(partNum).polygon=polygon;
    allParts(partNum).medialAxis=axisM;
    allParts(partNum).layerId=min(layerId(idx1),layerId(idx2));
    allParts(partNum).border=borders(i)*axisLength;
    allParts(partNum).name=names{i};
    partNum=partNum+1;
  end
end

% Process face
if(marked(K.R_Shoulder) && marked(K.L_Shoulder) && marked(K.HeadTop))
  [polygon,axisM]=getFacePolygon_rel(coords(modelIdx{K.R_Shoulder}),...
                                     coords(modelIdx{K.L_Shoulder}),...
                                     coords(modelIdx{K.HeadTop}),...
                                     opts.scaleFaceMedian,opts.faceWidth);
  axisLength=norm(axisM(1:2)-axisM(3:4));
  allParts(partNum).polygon=polygon;
  allParts(partNum).medialAxis=axisM;
  allParts(partNum).layerId=layerId(K.HeadTop);
  allParts(partNum).border=opts.faceBorder*axisLength;
  allParts(partNum).name='Face';
  partNum=partNum+1;
end

% Process torso
if(marked(K.R_Shoulder) && marked(K.L_Shoulder) && marked(K.R_Hip) && marked(K.L_Hip))
   [polygon,axisM]=getTorsoPolygon_rel(coords(modelIdx{K.R_Shoulder}),...
                                       coords(modelIdx{K.L_Shoulder}),...
                                       coords(modelIdx{K.R_Hip}),...
                                       coords(modelIdx{K.L_Hip}));
   axisLength=norm(axisM(1:2)-axisM(3:4));
   allParts(partNum).polygon=polygon;
   allParts(partNum).medialAxis=axisM;
   allParts(partNum).layerId=min([layerId(K.L_Shoulder) layerId(K.R_Shoulder) ...
                                 layerId(K.L_Hip) layerId(K.R_Hip)]);
   allParts(partNum).border=opts.torsoBorder*axisLength;
   allParts(partNum).border=opts.torsoBorder*axisLength;
   allParts(partNum).name='Torso';
   partNum=partNum+1;
end

function [polygon,axisM]=getTorsoPolygon_rel(rShoulder,lShoulder,rHip,lHip)

shoulderLength=norm(rShoulder-lShoulder);
hipLength=norm(rHip-lHip);

rescale=hipLength/shoulderLength;
shoulderVec=lShoulder-rShoulder;
lambdaAlong=(1-rescale)/2;
polygon=zeros(4,2);

polygon(1,:)=rShoulder+lambdaAlong*shoulderVec;
polygon(2,:)=lShoulder-lambdaAlong*shoulderVec;
polygon(3,:)=lHip;
polygon(4,:)=rHip;

axisM=[(rHip+lHip)/2 (lShoulder+rShoulder)/2];

function [polygon,axisM]=getFacePolygon_rel(rShoulder,lShoulder,headTop,scaleMedian,faceWidth)
% Makes face polygon on line joining center of eyes to center of shoulders
meanShoulder=0.5*(lShoulder+rShoulder);
headTop=meanShoulder+scaleMedian*(headTop-meanShoulder);

[polygon,axisM]=getPolygonFromMedian_rel(headTop,meanShoulder,faceWidth/2);


function [polygon,axisM]=getPolygonFromMedian_rel(pt1,pt2,rad)

hyp=norm(pt1-pt2);
rad=rad*hyp; % Convert to absolute scale
xBase=pt2(1)-pt1(1);
yBase=pt2(2)-pt1(2);

cosTheta=xBase/hyp;
sinTheta=yBase/hyp; % These are orientation of median line

% Orientation of line rotated 90degree anti-clockwise is
% give by (-sinTheta,cosTheta)
medianVec_perp=[-sinTheta cosTheta];

polygon=zeros(4,2);
polygon(1,:)=pt2+rad*medianVec_perp;
polygon(2,:)=pt2-rad*medianVec_perp;
polygon(3,:)=pt1-rad*medianVec_perp;
polygon(4,:)=pt1+rad*medianVec_perp;

axisM=[pt1 pt2];


function [polygon,axisM]=getHandPolygon_rel(elbow,wrist,boxW)

hyp=norm(elbow-wrist);
boxW=hyp*boxW; % Convert to absolute pixel size
xBase=elbow(1)-wrist(1);
yBase=elbow(2)-wrist(2);

cosTheta=xBase/hyp;
sinTheta=yBase/hyp; % These are orientation of line from write to elbow

% Orientation of line rotated 90degree anti-clockwise is
% give by (-sinTheta,cosTheta)

armVec=[cosTheta sinTheta];
armVec_perp=[-sinTheta cosTheta];

polygon=zeros(4,2);
boxRad=boxW/2;
polygon(1,:)=wrist+boxRad*armVec_perp;
polygon(2,:)=polygon(1,:)-boxW*armVec;
polygon(4,:)=wrist-boxRad*armVec_perp;
polygon(3,:)=polygon(4,:)-boxW*armVec;

axisM=[wrist wrist-boxW*armVec];


