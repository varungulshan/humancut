function shapePosterior=getShapePosterior(obj,imgSize,iFrame)

shapePosterior=zeros(imgSize);
allParts=obj.getValid_parts(iFrame);

partIds=[1:length(allParts)];
layerIds=[allParts.layerId];

[layerIds,sortIdx]=sort(layerIds,2,'descend');
partIds=partIds(sortIdx);

for i=partIds
  iPart=allParts(i);
  polyMask=roipoly(shapePosterior,iPart.polygon(:,1),iPart.polygon(:,2));
  %shapePosterior(polyMask)=1;
  shapePosterior=humanModels.cpp.mex_addShapePosterior(shapePosterior,polyMask,...
                                iPart.border);
end
