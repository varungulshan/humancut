function setModelVector(obj,modelVector,iFrame)

assert(size(obj.modelVector,1)==size(modelVector,1),'Incorrect modelVector dimensions\n');
obj.modelVector(:,iFrame)=modelVector;
