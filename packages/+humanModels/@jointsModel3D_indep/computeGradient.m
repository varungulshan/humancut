function modelGradient=computeGradient(obj,segH)

deltaVector=obj.opts.deltaVector_gradient;
curE=segH.getEnergy_bestSeg(obj);

modelVector=obj.modelVector;
vectorMask=logical(zeros(size(modelVector)));
idxMarked=horzcat(obj.jointIds{obj.jointMarked});
vectorMask(idxMarked)=true;

modelGradient=zeros(size(modelVector));

for i=1:length(modelVector)
  if(vectorMask(i))
    tmpVector=modelVector;
    tmpVector(i)=tmpVector(i)+deltaVector;
    obj.modelVector=tmpVector;
    newE=segH.getEnergy_bestSeg(obj);
    modelGradient(i)=(newE-curE)/deltaVector;
  end
end

obj.modelVector=modelVector; % Restore original state

function modelGradient=computeGradient_old(obj,segH)
% Older version which does not recompute segmentations

deltaVector=obj.opts.deltaVector_gradient;
curE=segH.getEnergy_fixedSeg(obj);

modelVector=obj.modelVector;
vectorMask=logical(zeros(size(modelVector)));
idxMarked=horzcat(obj.jointIds{obj.jointMarked});
vectorMask(idxMarked)=true;

modelGradient=zeros(size(modelVector));

for i=1:length(modelVector)
  if(vectorMask(i))
    tmpVector=modelVector;
    tmpVector(i)=tmpVector(i)+deltaVector;
    obj.modelVector=tmpVector;
    newE=segH.getEnergy_fixedSeg(obj);
    modelGradient(i)=(newE-curE)/deltaVector;
  end
end

obj.modelVector=modelVector; % Restore original state
