function overlayImg=drawModel(obj,img,iFrame)

blendAlpha=0.5;

[h w nCh]=size(img);
labelImg=zeros([h w],'uint8');
allParts=obj.getValid_parts(iFrame);

partIds=[1:length(allParts)];
layerIds=[allParts.layerId];

[layerIds,sortIdx]=sort(layerIds,2,'descend');
partIds=partIds(sortIdx);

for i=partIds
  iPart=allParts(i);
  polyMask=roipoly(labelImg,iPart.polygon(:,1),iPart.polygon(:,2));
  labelImg(polyMask)=i;
end

alphaImg=blendAlpha*ones([h w]);
alphaImg(labelImg==0)=0;

%cmapParts=colorcube(length(allParts));
%cmapParts=[0 0 0;cmapParts]; % Adding extra color for label 0

%overlayImg=label2rgb(labelImg+1,cmapParts);
overlayImg=label2rgb(labelImg);
overlayImg=im2double(overlayImg);
alphaImg=repmat(alphaImg,[1 1 3]);
if(nCh==1), img=repmat(img,[1 1 3]); end;
if(~strcmp(class(img),'double')), img=im2double(img); end;

overlayImg=alphaImg.*overlayImg+(1-alphaImg).*img;
