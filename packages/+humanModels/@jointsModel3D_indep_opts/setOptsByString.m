function obj=setOptsByString(obj,optsString)

switch(optsString)
  case 'jointsModel3D_indep_try1'
    obj.hand_width=0.2; % 0.2 times the lower arm length
    obj.hand_border=1; % times the hand length/width (its a square)
    obj.lowerArm_halfWidth=0.07; % times the lower arm length
    obj.lowerArm_border=0.2; % times the lower arm length
    obj.upperArm_halfWidth=0.07;
    obj.upperArm_border=0.3;
    obj.scaleFaceMedian=1;
    obj.faceWidth=0.328; % times the face length
    obj.faceBorder=0.328;
    obj.torsoBorder=0.43; % times the torso length
    obj.upperLeg_halfWidth=0.08; % times the upper leg length
    obj.upperLeg_border=0.316; % times the upper leg length
    obj.lowerLeg_halfWidth=0.0754; % times the lower leg length
    obj.lowerLeg_border=0.2261; % times the lower leg length
    obj.foot_width=0.1507; % times the lower leg length
    obj.foot_border=0.2261; % times the lower leg length
    
    obj.bgBorder_scale=1;
    obj.deltaVector_gradient=5;
  otherwise
    error('Invalid joint model options string: %s\n',optsString);
end
