function updateColorLikelihoods(obj)

partList=obj.allParts.partList;
partGmms=obj.partColorModels;

for j=1:numel(partList)
  if(partList(j).valid)
    pixelIdx=partList(j).boundaryPixels; 
    partGmm=partGmms(j);
    partList(j).colorNeg_logLikelihood=obj.getPartLikelihood(partGmm,obj.features,...
                                       pixelIdx,obj.opts);
  end
end

obj.allParts.partList=partList;
