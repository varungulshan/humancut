function negLogLikeli=getPartLikelihood(partGmm,features,pixelIdx,opts)

%assert(~isempty(partGmm.mu),'Emtpy gmm passed\n');
if(isempty(partGmm.mu))
  % this means that the part exists, but lies outside the image, so that
  % a color model could not be initialized for it. Best we can 
  % do is give it a uniform prior
  fgLikeli=opts.uniform_value*ones(1,numel(pixelIdx));
else
  features=features(:,pixelIdx);
  fgLikeli=gmm.computeGmm_likelihood(features,partGmm);
end

fgLikeli=opts.uniform_gamma*opts.uniform_value+(1-opts.uniform_gamma)*fgLikeli;
negLogLikeli=-log(fgLikeli);
negLogLikeli(negLogLikeli>opts.unaryBound)=opts.unaryBound;
