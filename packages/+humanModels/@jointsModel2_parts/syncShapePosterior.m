function syncShapePosterior(obj)

imgSize=obj.imgSize;
shapePosterior=zeros(imgSize);
allParts=obj.allParts.partList;
validParts=[allParts.valid];
allParts=allParts(validParts);

partIds=[1:length(allParts)];
layerIds=[allParts.layerId];

[layerIds,sortIdx]=sort(layerIds,2,'descend');
partIds=partIds(sortIdx);

for i=partIds
  iPart=allParts(i);
  shapePosterior(iPart.boundaryPixels)=max(shapePosterior(iPart.boundaryPixels),...
                                         iPart.shapePrior);
end

obj.shapePosterior=shapePosterior;
