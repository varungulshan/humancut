function syncPart(obj,updateColor)

numJoints=size(obj.jointMarked,1);
imgSize=obj.imgSize;

marked=obj.jointMarked(:,1);
layerId=obj.jointDepth(:,1);
modelVector=obj.modelVector(:,1);
coords=modelVector';
modelIdx=obj.jointIds;
opts=obj.opts;
K=obj.jointNumByName;
partNum=1;

if(~exist('updateColor','var'))
  updateColor=true;
end

if(isempty(obj.allParts))
  oldParts=struct([]);
  jointChanged_mask=true(numJoints,1);
  oldPartList=struct([]);
else
  oldParts=obj.allParts;
  jointChanged_mask=false(numJoints,1);
  jointChanged_mask=jointChanged_mask|(marked~=oldParts.marked);
  jointChanged_mask=jointChanged_mask|(layerId~=oldParts.layerId);
  jointChanged_mask=jointChanged_mask|getJointPosition_changeMask(modelVector,...
                    oldParts.modelVector,modelIdx);
  oldPartList=oldParts.partList;
end

newPartList=struct('valid',[],'polygon',[],'medialAxis',[],'layerId',[],...
                   'border',[],'name',[],'insidePixels',[],'boundaryPixels',[],...
                   'shapePrior',[]);
% boundary pixels includes inside pixels
% shapePrior is same size as boundaryPixels (both are 1 x N)

% Process hands and feet
pairIdxs=[K.R_Elbow K.R_Wrist;...
          K.L_Elbow K.L_Wrist;...
          K.R_Knee K.R_Ankle;...
          K.L_Knee K.L_Ankle;...
         ];
names={'Right hand';...
       'Left hand';...
       'Right ankle';...
       'Left ankle';...
       };
widths=[...
        opts.hand_width; ...
        opts.hand_width; ...
        opts.foot_width; ...
        opts.foot_width; ...
      ];
borders=[...
          opts.hand_border; ...
          opts.hand_border; ...
          opts.foot_border; ...
          opts.foot_border; ...
        ];

for i=1:size(pairIdxs,1)
  idx1=pairIdxs(i,1);
  idx2=pairIdxs(i,2);
  if(~any(jointChanged_mask([idx1 idx2])))
    newPartList(partNum)=oldPartList(partNum);
  elseif(marked(idx1) && marked(idx2))
    [polygon,axisM]=getHandPolygon_rel(coords(modelIdx{idx1}),...
                                        coords(modelIdx{idx2}),...
                                        widths(i)); 
    axisLength=norm(axisM(1:2)-axisM(3:4));
    newPartList(partNum).polygon=polygon;
    newPartList(partNum).medialAxis=axisM;
    newPartList(partNum).layerId=layerId(idx2);
    newPartList(partNum).border=borders(i)*axisLength;
    newPartList(partNum).name=names{i};
    newPartList(partNum).valid=true;
    [inPixels,borderPixels,shapePrior]=obj.getPartShape(newPartList(partNum),imgSize);
    newPartList(partNum).insidePixels=inPixels;
    newPartList(partNum).boundaryPixels=borderPixels;
    newPartList(partNum).shapePrior=shapePrior;
    if(updateColor)
      pixelIdx=borderPixels;
      partGmm=obj.partColorModels(partNum);
      newPartList(partNum).colorNeg_logLikelihood=...
                  obj.getPartLikelihood(partGmm,obj.features,pixelIdx,opts);
    end
  else
    newPartList(partNum).valid=false; 
  end

  partNum=partNum+1;
end

% Process lower arms, upper arms, lower legs, upper legs
pairIdxs=[K.R_Wrist K.R_Elbow;... % right lower arm
          K.L_Wrist K.L_Elbow;... % left lower arm
          K.R_Shoulder K.R_Elbow; ... % right upper arm
          K.L_Shoulder K.L_Elbow; ... % left upper arm
          K.R_Hip K.R_Knee; ... % right upper leg
          K.L_Hip K.L_Knee; ... % left upper leg
          K.R_Ankle K.R_Knee; ... % right lower leg
          K.L_Ankle K.L_Knee; ... % left lower leg
          ];
names={'Right lower arm';...
       'Left lower arm';...
       'Right upper arm';...
       'Left upper arm';...
       'Right upper leg';...
       'Left upper leg';...
       'Right lower leg';...
       'Left lower leg';...
};
halfWidths=[opts.lowerArm_halfWidth;...
            opts.lowerArm_halfWidth;...
            opts.upperArm_halfWidth;...
            opts.upperArm_halfWidth;...
            opts.upperLeg_halfWidth;...
            opts.upperLeg_halfWidth;...
            opts.lowerLeg_halfWidth;...
            opts.lowerLeg_halfWidth;...
            ];
borders=[opts.lowerArm_border;...
         opts.lowerArm_border;...
         opts.upperArm_border;...
         opts.upperArm_border;...
         opts.upperLeg_border;...
         opts.upperLeg_border;...
         opts.lowerLeg_border;...
         opts.lowerLeg_border;...
        ];

for i=1:size(pairIdxs,1)
  idx1=pairIdxs(i,1);
  idx2=pairIdxs(i,2);
  if(~any(jointChanged_mask([idx1 idx2])))
    newPartList(partNum)=oldPartList(partNum);
  elseif(marked(idx1) && marked(idx2))
    [polygon,axisM]=getPolygonFromMedian_rel(coords(modelIdx{idx1}),...
                                             coords(modelIdx{idx2}),...
                                             halfWidths(i)); 
    axisLength=norm(axisM(1:2)-axisM(3:4));
    newPartList(partNum).polygon=polygon;
    newPartList(partNum).medialAxis=axisM;
    newPartList(partNum).layerId=min(layerId(idx1),layerId(idx2));
    newPartList(partNum).border=borders(i)*axisLength;
    newPartList(partNum).name=names{i};
    newPartList(partNum).valid=true;
    [inPixels,borderPixels,shapePrior]=obj.getPartShape(newPartList(partNum),imgSize);
    newPartList(partNum).insidePixels=inPixels;
    newPartList(partNum).boundaryPixels=borderPixels;
    newPartList(partNum).shapePrior=shapePrior;
    if(updateColor)
      pixelIdx=borderPixels;
      partGmm=obj.partColorModels(partNum);
      newPartList(partNum).colorNeg_logLikelihood=...
                  obj.getPartLikelihood(partGmm,obj.features,pixelIdx,opts);
    end
  else
    newPartList(partNum).valid=false; 
  end

  partNum=partNum+1;
end

% Process face
idxsFace=[K.R_Shoulder K.L_Shoulder K.HeadTop];
if(~any(jointChanged_mask(idxsFace)))
  newPartList(partNum)=oldPartList(partNum);
elseif(all(marked(idxsFace)))
  if(strcmp(obj.opts.faceMethod,'eyeShoulder_join'))
    [polygon,axisM]=getFacePolygon_rel(coords(modelIdx{K.R_Shoulder}),...
                                      coords(modelIdx{K.L_Shoulder}),...
                                      coords(modelIdx{K.HeadTop}),...
                                      opts.scaleFaceMedian,opts.faceWidth);
  elseif(strcmp(obj.opts.faceMethod,'shoulderPerpendicular'))
    [polygon,axisM]=getFacePolygonShoulder_rel(coords(modelIdx{K.R_Shoulder}),...
        coords(modelIdx{K.L_Shoulder}),...
        coords(modelIdx{K.HeadTop}),...
        opts.scaleFaceMedian,opts.faceWidth);
  end
  axisLength=norm(axisM(1:2)-axisM(3:4));
  newPartList(partNum).polygon=polygon;
  newPartList(partNum).medialAxis=axisM;
  newPartList(partNum).layerId=layerId(K.HeadTop);
  newPartList(partNum).border=opts.faceBorder*axisLength;
  newPartList(partNum).name='Face';
  newPartList(partNum).valid=true;
  [inPixels,borderPixels,shapePrior]=obj.getPartShape(newPartList(partNum),imgSize);
  newPartList(partNum).insidePixels=inPixels;
  newPartList(partNum).boundaryPixels=borderPixels;
  newPartList(partNum).shapePrior=shapePrior;
  if(updateColor)
    pixelIdx=borderPixels;
    partGmm=obj.partColorModels(partNum);
    newPartList(partNum).colorNeg_logLikelihood=...
                obj.getPartLikelihood(partGmm,obj.features,pixelIdx,opts);
  end

else
  newPartList(partNum).valid=false; 
end
partNum=partNum+1;

idxsTorso=[K.R_Shoulder K.L_Shoulder K.R_Hip K.L_Hip];
% Process torso
if(~any(jointChanged_mask(idxsTorso)))
  newPartList(partNum)=oldPartList(partNum);
elseif(all(marked(idxsTorso)))
  [polygon,axisM]=getTorsoPolygon_rel(coords(modelIdx{K.R_Shoulder}),...
                                      coords(modelIdx{K.L_Shoulder}),...
                                      coords(modelIdx{K.R_Hip}),...
                                      coords(modelIdx{K.L_Hip}));
  axisLength=norm(axisM(1:2)-axisM(3:4));
  newPartList(partNum).polygon=polygon;
  newPartList(partNum).medialAxis=axisM;
  newPartList(partNum).layerId=min([layerId(K.L_Shoulder) layerId(K.R_Shoulder) ...
                                layerId(K.L_Hip) layerId(K.R_Hip)]);
  newPartList(partNum).border=opts.torsoBorder*axisLength;
  newPartList(partNum).name='Torso';
  newPartList(partNum).valid=true;
  [inPixels,borderPixels,shapePrior]=obj.getPartShape(newPartList(partNum),imgSize);
  newPartList(partNum).insidePixels=inPixels;
  newPartList(partNum).boundaryPixels=borderPixels;
  newPartList(partNum).shapePrior=shapePrior;
  if(updateColor)
    pixelIdx=borderPixels;
    partGmm=obj.partColorModels(partNum);
    newPartList(partNum).colorNeg_logLikelihood=...
                obj.getPartLikelihood(partGmm,obj.features,pixelIdx,opts);
  end
else
  newPartList(partNum).valid=false; 
end

partNum=partNum+1;

newParts.marked=marked;
newParts.layerId=layerId;
newParts.modelVector=modelVector;
newParts.partList=newPartList;

obj.allParts=newParts;
obj.syncShapePosterior();

function [polygon,axisM]=getTorsoPolygon_rel(rShoulder,lShoulder,rHip,lHip)

shoulderLength=norm(rShoulder-lShoulder);
hipLength=norm(rHip-lHip);

rescale=hipLength/shoulderLength;
shoulderVec=lShoulder-rShoulder;
lambdaAlong=(1-rescale)/2;
polygon=zeros(4,2);

polygon(1,:)=rShoulder+lambdaAlong*shoulderVec;
polygon(2,:)=lShoulder-lambdaAlong*shoulderVec;
polygon(3,:)=lHip;
polygon(4,:)=rHip;

axisM=[(rHip+lHip)/2 (lShoulder+rShoulder)/2];

function [polygon,axisM]=getFacePolygon_rel(rShoulder,lShoulder,headTop,scaleMedian,faceWidth)
% Makes face polygon on line joining center of eyes to center of shoulders
meanShoulder=0.5*(lShoulder+rShoulder);
headTop=meanShoulder+scaleMedian*(headTop-meanShoulder);

[polygon,axisM]=getPolygonFromMedian_rel(headTop,meanShoulder,faceWidth/2);

function [polygon,axisM]=getFacePolygonShoulder_rel...
    (rShoulder,lShoulder,headTop,scaleMedian,faceWidth)
% Makes face polygon on line perpendicular to center of shoulder
meanShoulder=0.5*(lShoulder+rShoulder);
faceLength=scaleMedian*norm(meanShoulder-headTop);

hyp=norm(lShoulder-rShoulder);
xBase=rShoulder(1)-lShoulder(1);
yBase=rShoulder(2)-lShoulder(2);

cosTheta=xBase/hyp;
sinTheta=yBase/hyp;

upVector=[-sinTheta cosTheta];
if(upVector(2)>0) upVector=-upVector; end; % Make sure head points up
headTop=meanShoulder+faceLength*upVector;

[polygon,axisM]=getPolygonFromMedian_rel(headTop,meanShoulder,faceWidth/2);

function [polygon,axisM]=getPolygonFromMedian_rel(pt1,pt2,rad)

hyp=norm(pt1-pt2);
rad=rad*hyp; % Convert to absolute scale
xBase=pt2(1)-pt1(1);
yBase=pt2(2)-pt1(2);

cosTheta=xBase/hyp;
sinTheta=yBase/hyp; % These are orientation of median line

% Orientation of line rotated 90degree anti-clockwise is
% give by (-sinTheta,cosTheta)
medianVec_perp=[-sinTheta cosTheta];

polygon=zeros(4,2);
polygon(1,:)=pt2+rad*medianVec_perp;
polygon(2,:)=pt2-rad*medianVec_perp;
polygon(3,:)=pt1-rad*medianVec_perp;
polygon(4,:)=pt1+rad*medianVec_perp;

axisM=[pt1 pt2];


function [polygon,axisM]=getHandPolygon_rel(elbow,wrist,boxW)

hyp=norm(elbow-wrist);
boxW=hyp*boxW; % Convert to absolute pixel size
xBase=elbow(1)-wrist(1);
yBase=elbow(2)-wrist(2);

cosTheta=xBase/hyp;
sinTheta=yBase/hyp; % These are orientation of line from write to elbow

% Orientation of line rotated 90degree anti-clockwise is
% give by (-sinTheta,cosTheta)

armVec=[cosTheta sinTheta];
armVec_perp=[-sinTheta cosTheta];

polygon=zeros(4,2);
boxRad=boxW/2;
polygon(1,:)=wrist+boxRad*armVec_perp;
polygon(2,:)=polygon(1,:)-boxW*armVec;
polygon(4,:)=wrist-boxRad*armVec_perp;
polygon(3,:)=polygon(4,:)-boxW*armVec;

axisM=[wrist wrist-boxW*armVec];


function mask=getJointPosition_changeMask(oldModelVector,newModelVector,modelIdx)

mask=false(length(modelIdx),1);
for i=1:length(modelIdx)
  coords=modelIdx{i};
  mask(i)=any(oldModelVector(coords)~=newModelVector(coords));
end
