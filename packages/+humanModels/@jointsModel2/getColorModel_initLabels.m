function labelImg=getColorModel_initLabels(obj,colorModelType,imgSize)
% Generates a labelimg for initialization of color models by the segmentation
% method

switch(colorModelType)
  case 'globalModels'
    labelImg=getGrabCutLabels(obj,imgSize);
  otherwise
    error('Unsupported colormodelType=%s\n',colorModelType);
end

function labelImg=getGrabCutLabels(obj,imgSize)
h=imgSize(1);
w=imgSize(2);

labelImg=6*ones([h w],'uint8');
allParts=obj.allParts.partList;

validParts=[allParts.valid];
allParts=allParts(validParts);
partIds=[1:length(allParts)];
layerIds=[allParts.layerId];

[layerIds,sortIdx]=sort(layerIds,2,'descend');
partIds=partIds(sortIdx);
bgBorder_scale=obj.opts.bgBorder_scale;

for i=partIds
  iPart=allParts(i);
  polyMask=roipoly(labelImg,iPart.polygon(:,1),iPart.polygon(:,2));
  labelImg(polyMask)=1;
  labelImg=humanModels.cpp.mex_addLabels3(labelImg,polyMask,[iPart.border ...
           iPart.border*(1+bgBorder_scale)],uint8([0 2]),uint8([6 2]));
  % the last argument says only make changes in regions which have labels
  % 2 or 6
end
