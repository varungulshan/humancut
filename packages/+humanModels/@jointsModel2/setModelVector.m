function setModelVector(obj,modelVector)

assert(size(obj.modelVector,1)==size(modelVector,1),'Incorrect modelVector dimensions\n');
obj.modelVector(:,1)=modelVector;
obj.syncPart();
