function [inPixels,borderPixels,shapePrior]=getPartShape(part,imgSize)

polyMask=true(imgSize(1),imgSize(2));
polyMask=roipoly(polyMask,part.polygon(:,1),part.polygon(:,2));
inPixels=int32(find(polyMask));
[borderPixels,shapePrior]=humanModels.cpp.mex_partShapePrior(inPixels,part.border,...
                          int32(imgSize));
