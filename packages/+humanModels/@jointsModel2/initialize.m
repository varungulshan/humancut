function initialize(obj,anno,K,imgSize)

obj.imgSize=imgSize;

M=length(obj.jointName);
obj.modelVector=zeros(obj.dimensionality,1);
obj.jointVisible=false(M,1);
obj.jointMarked=false(M,1);
obj.jointDepth=zeros(M,1);

coords=anno.coords(:,1:2);
visible=anno.visible;
marked=anno.marked;
layerId=anno.layerNum;

if(marked(K.R_Eye) && ~marked(K.L_Eye))
  coords(K.L_Eye,:)=coords(K.R_Eye,:);
  marked(K.L_Eye)=true;
elseif(marked(K.L_Eye) && ~marked(K.R_Eye))
  coords(K.R_Eye,:)=coords(K.L_Eye,:);
  marked(K.R_Eye)=true;
end

for i=1:M
  jName=obj.jointName{i};
  switch(jName)
    case 'HeadTop'
      if(marked(K.R_Eye) && marked(K.L_Eye))
        vectorIds=obj.jointIds{i};
        % vector ids should be 1 x 2, with first coordinate as x, second as y
        % assert(numel(vectorIds)==2);
        obj.modelVector(vectorIds,1)=mean([coords(K.L_Eye,:);coords(K.R_Eye,:)],1);
        obj.jointVisible(i,1)=all(visible([K.L_Eye K.R_Eye]));
        obj.jointMarked(i,1)=true;
        obj.jointDepth(i,1)=min(layerId([K.L_Eye K.R_Eye]));
      end
    otherwise
      K_idx=eval(['K.' jName]);
      if(marked(K_idx))
        vectorIds=obj.jointIds{i};
        % vector ids should be 1 x 2, with first coordinate as x, second as y
        % assert(numel(vectorIds)==2);
        obj.modelVector(vectorIds,1)=coords(K_idx,:);
        obj.jointVisible(i,1)=visible(K_idx);
        obj.jointMarked(i,1)=true;
        obj.jointDepth(i,1)=layerId(K_idx);
      end
  end
end

obj.allParts=[];
obj.syncPart();
