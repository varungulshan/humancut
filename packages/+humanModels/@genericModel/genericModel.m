% Class defining a general parametrized model (could be of anything, not just humans!)
classdef genericModel < handle
  properties (Abstract,SetAccess=private, GetAccess=public)
    modelVector % K x 1 vector representing the model
    vectorDescriptions % K x 1 cell array, describing each entry in the vector above

    debugOpts % structure with debugLevel, debugDir and debugPrefix
  end

  methods(Abstract)
    initialize(obj,initMethod,varargin); % Function to initialize puppet model
    % initMethod='h3d', varargin: anno,K

    labelImg=getColorModel_initLabels(obj,colorModelType,imgSize);
    % Function to generate a label img
    % for initialization of color models given puppet.
    % colorModelType: 'globalModels' (for grabCut type color models)
    posterior=getShapePosterior(obj,imgSize);
    % Returns a shape posterior for a human model
    gradient=computeGradient(obj,segH);
    % Function to compute gradient of model vector. It uses the segmentation
    % object handle passed to it to compute energies and gradients
    updateModel(obj,gradient,stepSize);
    % Updates the parameters of the model given a gradient and step size
    setModelVector(obj,newModelVector);
    % Set the model vector to the passed value
  end
end
