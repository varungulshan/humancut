function updatePartColors(obj,seg)

assert(size(seg,3)==obj.nFrames);

partPixels=cell(1,obj.numParts);

for j=1:obj.nFrames
  jSeg=seg(:,:,j);
  frameJ_parts=obj.allParts{j}.partList;
  idxOffset=(j-1)*obj.nPixels_perFrame;
  for i=1:obj.numParts
    iPart=frameJ_parts(i);
    if(iPart.valid)
      pixelIdx=iPart.boundaryPixels;
      pixelIdx_mask=(jSeg(pixelIdx)==255);
      pixelIdx=pixelIdx(pixelIdx_mask);
      partPixels{i}=[partPixels{i} idxOffset+pixelIdx];
    end
  end
end

for i=1:obj.numParts
  if(~isempty(partPixels{i}))
    tmpFeatures=obj.features(:,partPixels{i});
    %assert(~isempty(obj.partColorModels(i).mu),'Gmm should already exist for part in update\n');
    if(isempty(obj.partColorModels(i).mu))
      % Empty means that this part has appeared, either it didnt exist before, or
      % it was outside the image boundaries and could not be initialized
      % so initialize its color model now
      obj.partColorModels(i)=gmm.init_gmmBS(tmpFeatures,obj.opts.nMixtures_part);
    else
      obj.partColorModels(i)=gmm.updateGmm(tmpFeatures,obj.partColorModels(i),...
                            obj.opts.gmmUpdate_iters);
    end
  end
end

% Update the likelihoods also now that color models have changed
frameNums=[1:obj.nFrames];
frameNums=frameNums(~obj.constrained);
for i=frameNums
  obj.updateColorLikelihoods(i);
end
