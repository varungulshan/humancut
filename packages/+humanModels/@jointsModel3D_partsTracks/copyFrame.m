function copyFrame(obj,dest,src)
% Does a smart copy, uses track to initialize properly

assert(abs(dest-src)==1,'Cannot copy between non-adjacent frames\n');

opts=obj.opts;

ptCorr=obj.ptCorr(min(src,dest));
if(src>dest)
  pts_init=ptCorr.pts_final;
  pts_final=ptCorr.pts_init;
  pts_initIdxMap=ptCorr.ptsFinal_idxMap;
else
  pts_init=ptCorr.pts_init;
  pts_final=ptCorr.pts_final;
  pts_initIdxMap=ptCorr.ptsInit_idxMap;
end

assert(numel(pts_init)==numel(pts_final),'Inconsistent point correspondances\n');

jointMarked=obj.jointMarked(:,src);
jointNums=[1:numel(jointMarked)];
jointNums=jointNums(jointMarked);

for i=jointNums
  srcJoint=obj.modelVector(obj.jointIds{i},src);
  idxPixel=obj.jointRegions(src,i).idxRegion;
  wts=obj.jointRegions(src,i).wts;
  corrPts_idx=pts_initIdxMap(idxPixel);
  mask=(corrPts_idx~=0);
  corrPts_idx=corrPts_idx(mask);
  wts=wts(mask);
  translation=pts_final(:,corrPts_idx)-pts_init(:,corrPts_idx);
  obj.modelVector(obj.jointIds{i},dest)=getOptimalJoint(wts',translation,srcJoint,opts);
end

%obj.modelVector(:,dest)=obj.modelVector(:,src);
obj.jointMarked(:,dest)=obj.jointMarked(:,src);
obj.jointDepth(:,dest)=obj.jointDepth(:,src);
obj.jointVisible(:,dest)=obj.jointVisible(:,src);

obj.syncPart(dest);

function joint=getOptimalJoint(wts,translation,srcJoint,opts)

% Currently just minimizing the sum of squares, can see better options later
joint=zeros(size(srcJoint));
wtsSum=sum(wts);
if(wtsSum==0), joint=srcJoint;return; end;
joint(1)=sum(wts.*translation(1,:))/wtsSum;
joint(2)=sum(wts.*translation(2,:))/wtsSum;

joint=joint+srcJoint;
