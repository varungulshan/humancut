function fgUnary=getFgUnary(obj,iFrame)

opts=obj.opts;

fgUnary=opts.unaryBound*ones(obj.imgSize);
partList=obj.allParts{iFrame}.partList;

for i=1:numel(partList)
  if(partList(i).valid)
    pixelIdx=partList(i).boundaryPixels;
    fgUnary(pixelIdx)=min(fgUnary(pixelIdx),partList(i).colorNeg_logLikelihood);
  end
end
