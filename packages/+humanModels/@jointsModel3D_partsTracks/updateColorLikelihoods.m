function updateColorLikelihoods(obj,iFrame)

partList=obj.allParts{iFrame}.partList;
partGmms=obj.partColorModels;

idxOffset=(iFrame-1)*obj.nPixels_perFrame;

for j=1:numel(partList)
  if(partList(j).valid)
    pixelIdx=idxOffset+partList(j).boundaryPixels; 
    partGmm=partGmms(j);
    partList(j).colorNeg_logLikelihood=obj.getPartLikelihood(partGmm,obj.features,...
                                       pixelIdx,obj.opts,partList(j).shapePrior);
  end
end

obj.allParts{iFrame}.partList=partList;
