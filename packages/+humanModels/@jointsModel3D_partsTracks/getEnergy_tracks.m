function energy=getEnergy_tracks(obj,i1,i2)

assert(abs(i1-i2)==1,'Invalid frames passed to getEnergy_tracks\n');

iMin=min(i1,i2);
iMax=max(i1,i2);

ptCorr=obj.ptCorr(iMin);
pts_init=ptCorr.pts_init;
pts_final=ptCorr.pts_final;
ptsInit_idxMap=ptCorr.ptsInit_idxMap;
ptsFinal_idxMap=ptCorr.ptsFinal_idxMap;

energy=0;

jointMarked1=obj.jointMarked(:,iMin);
jointMarked2=obj.jointMarked(:,iMax);

jointNums=[1:numel(jointMarked1)];
jointNums=jointNums(jointMarked1 & jointMarked2);

for i=jointNums
  jointIds=obj.jointIds{i};
  srcJoint=obj.modelVector(jointIds,iMin);
  destJoint=obj.modelVector(jointIds,iMax);
  jointTranslation=destJoint-srcJoint;

  % Compute forward energies
  idxPixel=obj.jointRegions(iMin,i).idxRegion;
  wts=obj.jointRegions(iMin,i).wts;
  corrPts_idx=ptsInit_idxMap(idxPixel);
  mask=(corrPts_idx~=0);
  corrPts_idx=corrPts_idx(mask);
  wts=wts(mask);
  if(~isempty(corrPts_idx))
    fwdTranslation=pts_final(:,corrPts_idx)-pts_init(:,corrPts_idx);
    deltas=repmat(jointTranslation,[1 numel(corrPts_idx)])-fwdTranslation;
    deltas=sum(abs(deltas),1)'; % Taking L1 norm
    energy=energy+sum(wts.*deltas);
  end

  % Compute backward energies
  idxPixel=obj.jointRegions(iMax,i).idxRegion;
  wts=obj.jointRegions(iMax,i).wts;
  corrPts_idx=ptsFinal_idxMap(idxPixel);
  mask=(corrPts_idx~=0);
  corrPts_idx=corrPts_idx(mask);
  wts=wts(mask);
  if(~isempty(corrPts_idx))
    bwdTranslation=pts_init(:,corrPts_idx)-pts_final(:,corrPts_idx);
    deltas=repmat(-jointTranslation,[1 numel(corrPts_idx)])-bwdTranslation;
    deltas=sum(abs(deltas),1)'; % Taking L1 norm
    energy=energy+sum(wts.*deltas);
  end

end

gammaTrack=obj.opts.gammaTrack;
energy=energy*gammaTrack;
