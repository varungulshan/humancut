This class implements human model for entire video, supports partial intialization and implements part based color models for each part. The parts based models are smoothed when the likelihood is computed, i.e a hanning window like function is multiplied with the fg likelihood.

Also takes into account energies due to tracks.
