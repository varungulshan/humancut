function negLogLikeli=getPartLikelihood(partGmm,features,pixelIdx,opts,shapePosterior)

%assert(~isempty(partGmm.mu),'Emtpy gmm passed\n');
assert(numel(pixelIdx)==numel(shapePosterior));
if(isempty(partGmm.mu))
  % this means that the part exists, but lies outside the image, so that
  % a color model could not be initialized for it. Best we can 
  % do is give it a uniform prior
  fgLikeli=opts.uniform_value*ones(1,numel(pixelIdx));
else
  features=features(:,pixelIdx);
  fgLikeli=gmm.computeGmm_likelihood(features,partGmm);
end

fgLikeli=opts.uniform_gamma*opts.uniform_value+(1-opts.uniform_gamma)*fgLikeli;

fgLikeli=reweight(fgLikeli,shapePosterior,opts.fgUnaryHanning_threshold);

negLogLikeli=-log(fgLikeli);
negLogLikeli(negLogLikeli>opts.unaryBound)=opts.unaryBound;

function fgLikeli=reweight(fgLikeli,shapePosterior,fallOffThresh)

weights=ones(size(shapePosterior));
mask=(shapePosterior<=fallOffThresh);

weights(mask)=shapePosterior(mask)/fallOffThresh;

fgLikeli=fgLikeli.*weights;
