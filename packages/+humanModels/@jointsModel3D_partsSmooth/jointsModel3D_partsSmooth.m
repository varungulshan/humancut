classdef jointsModel3D_partsSmooth < handle
  properties (SetAccess=private, GetAccess=public)
    modelVector % K x nFrames vector representing the model
    vectorDescriptions % K x 1 cell array, describing each entry in the vector above
    debugOpts % structure with debugLevel, debugDir and debugPrefix
  
    % M = number of joints
    jointIds % Index into the model vector, for the parameters that belong to the joint
            % it is a cell array of size M x 1
            % each entry is 1 x 2, giving index of x and y coordinates of joint
    jointVisible % M x nFrames bool vector, marking visibility of joint
    jointMarked % M x nFrames bool vector, stating if a joint has been labeled or not
    jointDepth % M x nFrames, gives layernumber (smaller means nearer to camera)
    jointName % M x 1, cell array of strings, giving joint names
    jointNumByName % Structure which gives index of joint by name

    opts % Object of type jointModel_opts, see inside for what options there are
    nFrames
    dimensionality % of the modelVector = K
    constrained % binary vector , 1 x nFrames, true if that frame is constrained
    allParts % internal representation in terms of part, kept in sync with
    % the modelVector, also stores regions around parts from which to learn
    % color models and all
    % Type: 1 x nFrames cell array
    imgSize
    numParts % the number of parts the joints representation is converted to
    partColorModels % 1 x numParts structure array of gmm's, if a part does 
    % not exist or doesnt have enough pixels, then the gmm elements are empty
    features
    nPixels_perFrame
  end

  methods
    function obj=jointsModel3D_partsSmooth(debugOpts,opts)
      obj.jointName{1}='R_Shoulder';
      obj.jointNumByName.R_Shoulder=1;
      obj.jointName{2}='L_Shoulder';
      obj.jointNumByName.L_Shoulder=2;
      obj.jointName{3}='R_Hip';
      obj.jointNumByName.R_Hip=3;
      obj.jointName{4}='L_Hip';
      obj.jointNumByName.L_Hip=4;
      obj.jointName{5}='R_Elbow';
      obj.jointNumByName.R_Elbow=5;
      obj.jointName{6}='L_Elbow';
      obj.jointNumByName.L_Elbow=6;
      obj.jointName{7}='R_Wrist';
      obj.jointNumByName.R_Wrist=7;
      obj.jointName{8}='L_Wrist';
      obj.jointNumByName.L_Wrist=8;
      obj.jointName{9}='R_Knee';
      obj.jointNumByName.R_Knee=9;
      obj.jointName{10}='L_Knee';
      obj.jointNumByName.L_Knee=10;
      obj.jointName{11}='R_Ankle';
      obj.jointNumByName.R_Ankle=11;
      obj.jointName{12}='L_Ankle';
      obj.jointNumByName.L_Ankle=12;
      obj.jointName{13}='HeadTop';
      obj.jointNumByName.HeadTop=13;
  
      M=length(obj.jointName);
      obj.modelVector=zeros(2*M,1);
      obj.vectorDescriptions=cell(2*M,1);
      obj.dimensionality=2*M;
      obj.jointIds=cell(M,1);
      obj.constrained=false(1,1);
  
      for i=1:M
        obj.vectorDescriptions{2*i-1}=[obj.jointName{i} '_x']; 
        obj.vectorDescriptions{2*i}=[obj.jointName{i} '_y']; 
        obj.jointIds{i}=[2*i-1 2*i];
      end
  
      obj.nFrames=1; % will be set properly in the initialize function
      obj.jointVisible=logical(zeros(M,1));
      obj.jointMarked=logical(zeros(M,1));
      obj.jointDepth=zeros(M,1);
      assert(isfield(debugOpts,'debugLevel'),'Missing fields from debugOpts\n');
      assert(isfield(debugOpts,'debugDir'),'Missing fields from debugOpts\n');
      assert(isfield(debugOpts,'debugPrefix'),'Missing fields from debugOpts\n');
      obj.debugOpts=debugOpts;
      assert(strcmp(class(opts),'humanModels.jointsModel3D_partsSmooth_opts'),'Invalid class for opts\n');
      obj.opts=opts;
      obj.numParts=14;
    end

    initialize_exact(obj,exactInit_frames,annots,K,idxC,initC,nFrames,imgSize);
    modelStruct=getHumanModel(obj,iFrame);
    labelImg=getColorModel_initLabels(obj,colorModelType,imgSize,iFrame);
    posterior=getShapePosterior(obj,imgSize,iFrame);
    copyFrame(obj,dest,src); % Copies from src frame number to dest frame number
    overlay=drawModel(obj,img,iFrame); % Useful for debugging purposes, overlays the polygons
    setModelVector(obj,newModelVector,iFrame);
    fgUnary=getFgUnary(obj,iFrame); % Return the neg log likelihood image given by part
    % based color model of all the parts
    updatePartColors(obj,seg);
  end % of methods

  methods(Access=private)
    %allParts=getValid_parts(obj,iFrame); % Returns a parts based representation of human
    syncPart(obj,iFrame,optUpdateColor); % Synchronize the external representation (modelVector)
    % with the internal part based representation
    initPartColorModels(obj,initFrames); % uses initFrames to initialize color
    % models for each part
    updateColorLikelihoods(obj,iFrame); % Uses the stored color models to update
    % the color likelihoods for each part
  end

  methods(Static=true)
    [inPixels,borderPixels,shapePrior]=getPartShape(part,imgSize)
    negLogLikeli=getPartLikelihood(partGmm,features,pixelIdx,opts,shapePrior);
  end
end % of classdef
