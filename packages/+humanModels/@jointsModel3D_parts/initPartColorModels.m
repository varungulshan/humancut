function initPartColorModels(obj,initFrames)

partPixels=cell(1,obj.numParts);

for j=initFrames
  frameJ_parts=obj.allParts{j}.partList;
  idxOffset=(j-1)*obj.nPixels_perFrame;
  for i=1:obj.numParts
    iPart=frameJ_parts(i);
    if(iPart.valid)
      partPixels{i}=[partPixels{i} idxOffset+iPart.insidePixels];
    end
  end
end

for i=1:obj.numParts
  if(~isempty(partPixels{i}))
    tmpFeatures=obj.features(:,partPixels{i});
    obj.partColorModels(i)=gmm.init_gmmBS(tmpFeatures,obj.opts.nMixtures_part);
  end
end
