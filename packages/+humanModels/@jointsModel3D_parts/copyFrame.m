function copyFrame(obj,dest,src)

obj.modelVector(:,dest)=obj.modelVector(:,src);
obj.jointMarked(:,dest)=obj.jointMarked(:,src);
obj.jointDepth(:,dest)=obj.jointDepth(:,src);
obj.jointVisible(:,dest)=obj.jointVisible(:,src);

obj.syncPart(dest);
