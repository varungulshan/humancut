function modelStruct=getHumanModel(obj,iFrame)

assert(iFrame>=1 && iFrame<=obj.nFrames,'iFrame out of bounds\n');

modelStruct.modelVector=obj.modelVector(:,iFrame);
modelStruct.jointMarked=obj.jointMarked(:,iFrame);
modelStruct.jointVisible=obj.jointVisible(:,iFrame);
modelStruct.jointDepth=obj.jointDepth(:,iFrame);
