% Copyright (C) 2010 Varun Gulshan
% This class defines the options for jointsModel human representation
classdef jointsModel_opts
  properties (SetAccess=private, GetAccess=public)

    % properties to specify width of parts
    hand_width; % 0.2 times the lower arm length
    hand_border; % times the hand length/width (its a square)
    lowerArm_halfWidth; % times the lower arm length
    lowerArm_border; % times the lower arm length
    upperArm_halfWidth;
    upperArm_border;
    scaleFaceMedian;
    faceWidth; % times the face length
    faceBorder;
    torsoBorder; % times the torso length
    upperLeg_halfWidth; % times the upper leg length
    upperLeg_border; % times the upper leg length
    lowerLeg_halfWidth; % times the lower leg length
    lowerLeg_border; % times the lower leg length
    foot_width; % times the lower leg length
    foot_border; % times the lower leg length

    % propeties to compute gradients
    deltaVector_gradient

    % misc options
    bgBorder_scale % Thickness of boundary of region where bg color model is initialized
                   % from, relative to the thickness of the unknown region in the trimap

  end

  methods
    function obj=jointsModel_opts(optsString)
      obj=obj.setOptsByString(optsString);
    end
    obj=setOptsByString(obj,optsString);
  end

end % of classdef
