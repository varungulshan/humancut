% Class defining a general parametrized model (could be of anything, not just humans!)
classdef genericModel3D < handle
  properties (Abstract,SetAccess=private, GetAccess=public)
    modelVector % K x nFrames vector representing the model, each column
    % is 
    vectorDescriptions % K x 1 cell array, describing each entry in the vector above

    debugOpts % structure with debugLevel, debugDir and debugPrefix
  end

  methods(Abstract)
    initialize(obj,initMethod,varargin); % Function to initialize puppet model
    % initMethod='h3d', varargin: annos,K
    % annos is a structure array, with an entry for each frame

    labelImg=getColorModel_initLabels(obj,colorModelType,imgSize,iFrame);
    % Function to generate a label img
    % for initialization of color models given puppet.
    % colorModelType: 'globalModels' (for grabCut type color models)
    posterior=getShapePosterior(obj,imgSize,iFrame);
    % Returns a shape posterior for a human model
    setModelVector(obj,newModelVector,iFrame);
    % Set the model vector to the passed value
    overlay=drawModel(obj,img,iFrame);
  end
end
