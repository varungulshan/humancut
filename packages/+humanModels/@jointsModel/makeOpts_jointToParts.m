function opts=makeOpts_jointToParts(optsString)

switch(optsString)
  case 'jointToParts_try1'
    opts.hand_width=0.2; % 0.2 times the lower arm length
    opts.hand_border=1; % times the hand length/width (its a square)
    opts.lowerArm_halfWidth=0.07; % times the lower arm length
    opts.lowerArm_border=0.2; % times the lower arm length
    opts.upperArm_halfWidth=0.07;
    opts.upperArm_border=0.3;
    opts.scaleFaceMedian=1;
    opts.faceWidth=0.328; % times the face length
    opts.faceBorder=0.328;
    opts.torsoBorder=0.43; % times the torso length
    opts.upperLeg_halfWidth=0.08; % times the upper leg length
    opts.upperLeg_border=0.316; % times the upper leg length
    opts.lowerLeg_halfWidth=0.0754; % times the lower leg length
    opts.lowerLeg_border=0.2261; % times the lower leg length
    opts.foot_width=0.1507; % times the lower leg length
    opts.foot_border=0.2261; % times the lower leg length
end
