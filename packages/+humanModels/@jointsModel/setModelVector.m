function setModelVector(obj,modelVector)

assert(length(obj.modelVector)==length(modelVector),'Incorrect modelVector dimensions\n');
obj.modelVector=modelVector;
