function initialize(obj,initMethod,varargin)
% initMethod='h3d', varargin: anno,K

switch(initMethod)
  case 'h3d'
    assert(length(varargin)==2,'Invalid arguments for h3d init method\n');
    anno=varargin{1};
    K=varargin{2};
    initialize_fromH3D(obj,anno,K);
  otherwise
    error('Invalid initMethod %s for jointsModel\n',initMethod);
end

function initialize_fromH3D(obj,anno,K)

M=length(obj.jointName);
obj.modelVector(:)=0;
obj.jointVisible(:)=false;
obj.jointMarked(:)=false;
obj.jointDepth(:)=0;

coords=anno.coords(:,1:2);
visible=anno.visible;
marked=anno.marked;
layerId=anno.layerNum;

for i=1:M
  jName=obj.jointName{i};
  switch(jName)
    case 'HeadTop'
      if(marked(K.R_Eye) && marked(K.L_Eye))
        vectorIds=obj.jointIds{i};
        % vector ids should be 1 x 2, with first coordinate as x, second as y
        % assert(numel(vectorIds)==2);
        obj.modelVector(vectorIds)=mean([coords(K.L_Eye,:);coords(K.R_Eye,:)],1);
        obj.jointVisible(i)=all(visible([K.L_Eye K.R_Eye]));
        obj.jointMarked(i)=true;
        obj.jointDepth(i)=min(layerId([K.L_Eye K.R_Eye]));
      end
    otherwise
      K_idx=eval(['K.' jName]);
      if(marked(K_idx))
        vectorIds=obj.jointIds{i};
        % vector ids should be 1 x 2, with first coordinate as x, second as y
        % assert(numel(vectorIds)==2);
        obj.modelVector(vectorIds)=coords(K_idx,:);
        obj.jointVisible(i)=visible(K_idx);
        obj.jointMarked(i)=true;
        obj.jointDepth(i)=layerId(K_idx);
      end
  end
end
