function initialize_exact(obj,exactInit_frames,annos,K,idxC,initC,nFrames,imgSize)

assert(length(annos)==nFrames,'Wrong size of annots\n');
obj.nFrames=nFrames;
obj.imgSize=imgSize;
M=length(obj.jointName);
obj.modelVector=zeros(obj.dimensionality,nFrames);
obj.jointVisible=false(M,nFrames);
obj.jointMarked=false(M,nFrames);
obj.jointDepth=zeros(M,nFrames);

assert(numel(idxC)==numel(initC),'Inconsistent constrain idx and initC\n');
constrained=false(1,nFrames);
constrained(idxC)=true;
constrainVectorIdx=zeros(1,nFrames);
constrainVectorIdx(constrained)=[1:numel(idxC)];
obj.constrained=constrained;

assert(~any(constrained(exactInit_frames)),'Constrained frames cannot be exactly initialized\n');

for j=exactInit_frames
  anno=annos(j);
  coords=anno.coords(:,1:2);
  visible=anno.visible;
  marked=anno.marked;
  layerId=anno.layerNum;
  
  for i=1:M
    jName=obj.jointName{i};
    switch(jName)
      case 'HeadTop'
        if(marked(K.R_Eye) && marked(K.L_Eye))
          vectorIds=obj.jointIds{i};
          % vector ids should be 1 x 2, with first coordinate as x, second as y
          % assert(numel(vectorIds)==2);
          obj.modelVector(vectorIds,j)=mean([coords(K.L_Eye,:);coords(K.R_Eye,:)],1);
          obj.jointVisible(i,j)=all(visible([K.L_Eye K.R_Eye]));
          obj.jointMarked(i,j)=true;
          obj.jointDepth(i,j)=min(layerId([K.L_Eye K.R_Eye]));
        end
      otherwise
        K_idx=eval(['K.' jName]);
        if(marked(K_idx))
          vectorIds=obj.jointIds{i};
          % vector ids should be 1 x 2, with first coordinate as x, second as y
          % assert(numel(vectorIds)==2);
          obj.modelVector(vectorIds,j)=coords(K_idx,:);
          obj.jointVisible(i,j)=visible(K_idx);
          obj.jointMarked(i,j)=true;
          obj.jointDepth(i,j)=layerId(K_idx);
        end
    end
  end
end

for j=idxC
  obj.modelVector(:,j)=initC(j).modelVector;
  obj.jointVisible(:,j)=initC(j).jointVisible;
  obj.jointMarked(:,j)=initC(j).jointMarked;
  obj.jointDepth(:,j)=initC(j).jointDepth;
end

obj.allParts=cell(1,nFrames);

idxInitialized=[idxC exactInit_frames];
for j=idxInitialized
  obj.syncPart(j);
end
