% Copyright (C) 2010 Varun Gulshan
% This class defines the options for jointsModel human representation
classdef jointsModel_parts_trainedHead_opts
  properties (SetAccess=private, GetAccess=public)

    % properties to specify width of parts
    hand_width; % 0.2 times the lower arm length
    hand_border; % times the hand length/width (its a square)
    lowerArm_halfWidth; % times the lower arm length
    lowerArm_border; % times the lower arm length
    upperArm_halfWidth;
    upperArm_border;
    scaleFaceMedian;
    %faceMethod; % how to estimate the face
    %faceWidth; % times the face length
    faceBorder;
    faceThresh;
    torsoBorder; % times the torso length
    upperLeg_halfWidth; % times the upper leg length
    upperLeg_border; % times the upper leg length
    lowerLeg_halfWidth; % times the lower leg length
    lowerLeg_border; % times the lower leg length
    foot_width; % times the lower leg length
    foot_border; % times the lower leg length

    % misc options
    bgBorder_scale % Thickness of boundary of region where bg color model is initialized
                   % from, relative to the thickness of the unknown region in the trimap


    nMixtures_part
    uniform_gamma
    uniform_value
    unaryBound

    gmmUpdate_iters
  end

  methods
    function obj=jointsModel_parts_trainedHead_opts(optsString)
      obj=obj.setOptsByString(optsString);
    end
    obj=setOptsByString(obj,optsString);
  end

end % of classdef
