function setupImageList()
% Soft links images into h3d directory, so that they can be annotated.
% Creates a list file also

cwd=miscFns.extractDirPath(mfilename('fullpath'));

h3d_dir=[cwd '../../../../software/h3d_version1.0/data/people/'];
testBench_cmd='testBench.getTests_proxy2()';

[videoPaths,videoFiles]=eval(testBench_cmd);
videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);

listFile=[h3d_dir 'image_list.txt'];
fH=fopen(listFile,'w');

for i=1:length(videoNames)
  videoDir=videoPaths{i};
  images=dir([videoDir '*.png']);
  for j=1:length(images)
    srcFile=[videoDir images(j).name];
    dstLinkName=[videoNames{i} '-' images(j).name];
    destFile=[h3d_dir 'images/' dstLinkName];
    cmd=['ln -s ' srcFile ' ' destFile];
    fprintf('Executing %s\n',cmd);
    system(cmd);
    fprintf(fH,'%s\n',dstLinkName);
  end
end

fclose(fH);
