function copyAnnotations()
% Copies over the annotations produced by the h3d java app
% from the h3d directory into our directories, properly renaming them also

cwd=miscFns.extractDirPath(mfilename('fullpath'));

h3d_dir=[cwd '../../../../software/h3d_version1.0/data/people/'];
testBench_cmd='testBench.getTests_proxy2()';
root_outDir=[cwd '../../data/h3dAnnotations_2/'];

[videoPaths,videoFiles]=eval(testBench_cmd);
videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);

for i=1:length(videoNames)
  videoDir=videoPaths{i};
  images=dir([videoDir '*.png']);
  outDir=[root_outDir videoNames{i} '/'];
  if(~exist(outDir,'dir')), mkdir(outDir); end; 

  for j=1:length(images)
    srcFile=[h3d_dir 'info/' videoNames{i} '-' images(j).name '.gnd'];
    destFile=[outDir images(j).name '.gnd'];
    copyfile(srcFile,destFile);
  end
end
