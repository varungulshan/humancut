function [a,h3d_k]=loadAnnotations_full(annoDir,imgDir,imgList,opt_loadSeg)
% Reads in h3d annotations, including the segmentation labels
% Returns structure of size=number of annotated humans, with following fields
% coords -> 3d coords of each joint
% visible, layerNum, marked,
% segment_ids , segment_labels,
% body_type, origImgPath

cwd=miscFns.extractDirPath(mfilename('fullpath'));
h3dCodeDir=[cwd '../../../../software/h3d_version1.0/h3d_tools/'];
%imgDir=[cwd '../../../../software/h3d_version1.0/data/people/images/'];
%annoDir=[cwd '../../../../software/h3d_version1.0/data/people/info/'];
%pbDir=[cwd '../../../../software/h3d_version1.0/data/people/pb/'];

%imgList=[cwd '../../../../software/h3d_version1.0/data/people/image_list_original.txt'];

curDir=pwd;
cd(h3dCodeDir);
h3d_config_noClear;
cd(curDir);

% ------- Code copied and modified from generate_annotations.m in h3d_tools -----
if(~exist('opt_loadSeg','var'))
  loadSeg=true;
else
  loadSeg=opt_loadSeg;
end

global K;
h3d_k=K;

images=textread(imgList,'%s');

%images=dir([videoPath '*.png']);
%[xx,idx]=sort({images.name});
%images=images(idx);

info_files=images;
numUniqueAnnotations = size(info_files,1);

a=struct('coords',{},'visible',{},'layerNum',{},'marked',{},'segment_ids',{},...
         'segment_labels',{},'body_type',{},'imgPath',{},'imgStem',{},...
         'fullImgName',{} );

for i=1:numUniqueAnnotations
    stem = info_files{i}(1:findstr(info_files{i},'.jpg')-1);
    a(i).imgStem=stem;
    a(i).imgPath=[imgDir stem '.jpg'];
    a(i).fullImgName=info_files{i};
    a(i).coords = zeros([K.NumLabels 3]);

    fid = fopen([annoDir info_files{i} '.gnd'],'r');
    C = textscan(fid,'%s %f %f %f %s %s');
    a(i).body_type = textscan(fid,'%s',1);

    % read region info
    if(loadSeg)
      seg_ids = [];
      seg_labels = [];
      while 1
          region_name = textscan(fid,'%s',1);
          if isempty(region_name{1})
              break;
          end
          region = find(ismember(K.AreaNames, region_name{1}{1}));
          assert(isscalar(region));
          vals = textscan(fid,'%d');  
          seg_ids = [seg_ids; vals{1}]; %#ok<AGROW>
          seg_labels = [seg_labels; ones(length(vals{1}),1)*region]; %#ok<AGROW>
      end
      a(i).segment_ids = seg_ids;
      a(i).segment_labels = seg_labels;
    end
  
    fclose(fid);

    % Process the body parts scanned in
    coords = nan(K.NumLabels,3);
    visible = false(K.NumLabels,1);
    marked = false(K.NumLabels,1);
    layerNum = zeros(K.NumLabels,1);
    keypoints = C{1};
    for j=1:length(keypoints)
        idx = find(ismember(K.Labels,keypoints{j}));
        if isscalar(idx)
            coords(idx,:) = [C{2}(j) C{3}(j) C{4}(j)];
            visible(idx) = isequal(C{6}{j},'visible');
            marked(idx)=true;
            depth=C{5}{j};
            switch(depth)
              case 'Equal'
                layerNum(idx)=2;
              case 'Close'
                layerNum(idx)=1;
              case 'Far'
                layerNum(idx)=3;
            end
        end
    end
        
    a(i).coords = coords;
    a(i).visible = visible;
    a(i).marked=marked;
    a(i).layerNum=layerNum;
end

% Add the helper keypoints
for j=1:length(a)
  for i=1:size(K.MID_KEYPOINTS,1)
      a(j).coords(K.MID_KEYPOINTS(i,1),:) = mean(a(j).coords([K.MID_KEYPOINTS(i,2:3)],:));
  end
end

% ------- End Code copied and modified from generate_annotations.m in h3d_tools -----

