function [a,h3d_k]=loadAnnotations(videoPath,videoFile,videoName,root_annoDir)
% Function that reads in annotations of h3d dataset

cwd=miscFns.extractDirPath(mfilename('fullpath'));
h3d_dir=[cwd '../../../../software/h3d_version1.0/h3d_tools/'];
%root_annoDir=[cwd '../../data/h3dAnnotations/'];

annoDir=[root_annoDir videoName '/'];

curDir=pwd;
cd(h3d_dir);
h3d_config_noClear;

% ------- Code copied and modified from generate_annotations.m in h3d_tools -----
global K;
h3d_k=K;

if ~exist('info_list_file','var')
    info_list_file = 'image_list.txt';
end

images=dir([videoPath '*.png']);
[xx,idx]=sort({images.name});
images=images(idx);

info_files=cell(0,0);
for i=1:length(images)
  info_files{i,1}=images(i).name;
end

numUniqueAnnotations = size(info_files,1);
numAnnotations = numUniqueAnnotations;

a=struct('coords',{},'visible',{},'layerNum',{},'marked',{});

for i=1:numUniqueAnnotations
    a(i).coords = zeros([K.NumLabels 3]);

    fid = fopen([annoDir info_files{i} '.gnd'],'r');
    C = textscan(fid,'%s %f %f %f %s %s');
    body_type = textscan(fid,'%s',1);
    
    fclose(fid);

    coords = nan(K.NumLabels,3);
    visible = false(K.NumLabels,1);
    marked = false(K.NumLabels,1);
    layerNum = zeros(K.NumLabels,1);
    keypoints = C{1};
    for j=1:length(keypoints)
        idx = find(ismember(K.Labels,keypoints{j}));
        if isscalar(idx)
            coords(idx,:) = [C{2}(j) C{3}(j) C{4}(j)];
            visible(idx) = isequal(C{6}{j},'visible');
            marked(idx)=true;
            depth=C{5}{j};
            switch(depth)
              case 'Equal'
                layerNum(idx)=2;
              case 'Close'
                layerNum(idx)=1;
              case 'Far'
                layerNum(idx)=3;
            end
        end
    end
        
    a(i).coords = coords;
    a(i).visible = visible;
    a(i).marked=marked;
    a(i).layerNum=layerNum;
end

% Add the helper keypoints
for j=1:length(a)
  for i=1:size(K.MID_KEYPOINTS,1)
      a(j).coords(K.MID_KEYPOINTS(i,1),:) = mean(a(j).coords([K.MID_KEYPOINTS(i,2:3)],:));
  end
end

% ------- End Code copied and modified from generate_annotations.m in h3d_tools -----
cd(curDir);
