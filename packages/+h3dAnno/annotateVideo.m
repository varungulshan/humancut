function annotateVideo()
% Use this function along with the java app of lubomir bourdev to annotate
% a video. Essentially, this function will copy over the .gnd (ground truth annotation)
% of the previous frame to the next frame, so you wont have to initialise
% everything from scratch

cwd=miscFns.extractDirPath(mfilename('fullpath'));

h3d_dir=[cwd '../../../../software/h3d_version1.0/data/people/'];
testBench_cmd='testBench.getTests_tmp()';

[videoPaths,videoFiles]=eval(testBench_cmd);
videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);

counter=1;
for i=1:length(videoNames)
  fprintf('\nNow annotating video: %s\n\n',videoNames{i});
  videoDir=videoPaths{i};
  images=dir([videoDir '*.png']);
  [xx,idx]=sort({images.name});
  images=images(idx);

  prevFile=[h3d_dir 'info/' videoNames{i} '-' images(1).name '.gnd'];
  for j=2:length(images)
    fprintf('Next to copy: #%d to #%d, hit any key to proceed\n',counter,counter+1);
    pause;
    nxtFile=[h3d_dir 'info/' videoNames{i} '-' images(j).name '.gnd'];
    if(~exist(nxtFile,'file'))
      copyfile(prevFile,nxtFile);
    else
      fprintf('Destination file exists! will not overwrite\n'); 
    end
    prevFile=nxtFile;
    counter=counter+1;
  end

  counter=counter+1;
end
