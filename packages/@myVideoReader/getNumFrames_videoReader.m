function nFrames=getNumFrames_videoReader(fileName)

vH=videoReader(fileName);
nFrames=0;
nFrames_approximated=get(vH,'numFrames');
while(next(vH)),
  nFrames=nFrames+1;
end

fprintf('Actual video has %d frames, videoReader returned = %d\n',nFrames,nFrames_approximated);

close(vH);
