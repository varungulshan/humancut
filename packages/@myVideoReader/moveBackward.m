function moveBackward(obj)
  switch(obj.videoType)
    case 'imgSequence'
      %warning('ImgSequence not yet implemented\n');
      obj.frameNum=obj.frameNum-1;
      obj.curFrame=imread([obj.fullPath sprintf(obj.imgRegex,obj.frameNum)]);
    case 'video'
      if(~step(obj.vH,-1)),
        if(obj.frameNum==1)
          warning('Trying to goto frame number %d, does not exist\n',obj.frameNum-1);
          next(obj.vH);
        else
          warning('Stepping to previous frame not supported by video plugin\n');
        end
        return;
      end
      obj.curFrame=getframe(obj.vH);
      obj.frameNum=obj.frameNum-1;
  end
