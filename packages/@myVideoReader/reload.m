function reload(obj)
switch(obj.videoType)
  case 'imgSequence'
    %error('Reloading for imgSequence is not supported\n');
    reloadImgSeq(obj);
  case 'video'
    reloadVideoReader(obj);
end

function reloadImgSeq(obj)
obj.curFrame=imread([obj.fullPath sprintf(obj.imgRegex,1)]);
obj.frameNum=1;

function reloadVideoReader(obj)
close(obj.vH);
obj.vH=videoReader(obj.fullPath);
next(obj.vH);
obj.curFrame=getframe(obj.vH);
obj.frameNum=1;
