function nFrames=getNumFrames_imgSeq(fullPath,imgRegex)

nFrames=0;
while(true)
  if(exist([fullPath sprintf(imgRegex,nFrames+1)],'file')),
    nFrames=nFrames+1;
  else
    break;
  end
end
