classdef myVideoReader < handle
  properties (SetAccess=private, GetAccess=public)
    filename
    pathname
    fullPath
    extension
    videoType % video, imgSequence
    frameNum
    h
    w
    nFrames
    vH % video reader handle
    curFrame % 3 channel uint8 image
    loadOk
    imgRegex
  end

  methods
    [h w nCh nFrames]=videoSize(obj)
    moveForward(obj)
    moveBackward(obj)
    reload(obj)

    function obj=myVideoReader(pathname,filename)
      obj.pathname=pathname;
      obj.filename=filename;
      obj.extension=filename(end-2:end);
      if(isImage(obj.extension)),
        %warning('Image sequences not yet supported\n');
        obj.videoType='imgSequence';
        obj.imgRegex=['%05d.' obj.extension];
        obj.fullPath=pathname;
        obj.curFrame=imread([obj.fullPath sprintf(obj.imgRegex,1)]);
        obj.h=size(obj.curFrame,1);
        obj.w=size(obj.curFrame,2);
        obj.nFrames=obj.getNumFrames_imgSeq(obj.fullPath,obj.imgRegex);
        obj.loadOk=true;
        obj.frameNum=1;
      elseif(isVideo(obj.extension))
        obj.fullPath=[pathname filename];
        obj.vH=videoReader(obj.fullPath);
        obj.h=get(obj.vH,'height');
        obj.w=get(obj.vH,'width');
        obj.nFrames=obj.getNumFrames_videoReader(obj.fullPath);
        next(obj.vH);
        obj.curFrame=getframe(obj.vH);
        obj.videoType='video';
        obj.loadOk=true;
        obj.frameNum=1;
      end
    end % of constructor

    function delete(obj)
      switch(obj.videoType)
        case 'video'
          close(obj.vH);
      end
    end % of destructor

  end % of methods

  methods (Static)
    nFrames=getNumFrames_videoReader(fullPath);
    nFrames=getNumFrames_imgSeq(fullPath,imgRegex);
  end % of static methods

end % of classdef

function ans=isImage(ext)
  ans=false;
  switch(ext)
    case {'png','jpg','bmp'}
      ans=true;
    otherwise
      ans=false;
  end
end

function ans=isVideo(ext)
  ans=false;
  switch(ext)
    case {'avi','mpg','mp4','mov'}
      ans=true;
    otherwise
      ans=false;
  end
end
