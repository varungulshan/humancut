function moveForward(obj)
  switch(obj.videoType)
    case 'imgSequence'
      %warning('ImgSequence not yet implemented\n');
      obj.frameNum=obj.frameNum+1;
      obj.curFrame=imread([obj.fullPath sprintf(obj.imgRegex,obj.frameNum)]);
    case 'video'
      if(~next(obj.vH)),
        warning('Trying to goto frame number %d, it does not exist\n',obj.frameNum+1);
        if(~step(obj.vH,-1)),
          error('You overshoot the video, and the stream cant move backward\n');
        end
        return;
      end
      obj.curFrame=getframe(obj.vH);
      obj.frameNum=obj.frameNum+1;
  end
