function opts=makeOpts(optsString)

switch(optsString)

case 'rad40_onlyMotion'
    opts=motionCut2.segEngine_opts();
    opts.gcGamma_e=0.5;
    opts.gcGamma_s=0.1;
    opts.gcGamma_ssd=1;
    opts.gcGamma_c=0.1;
    opts.gcGamma_i=0;
    opts.gcScale=100000;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.winRad=40;
    opts.srchRad_bg=30;
    opts.srchRad_fg=30;
    opts.minPixels_match=400;

    opts.ssd_fullConfidence=1; % is always confident
    opts.ssd_noConfidence=1;
    opts.ssdCurve_fullConf_depth=0; % is always confident
    opts.ssdCurve_noConf_depth=0; % is always confident
    opts.ssdCurve_depthRad=2;

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.gmmUni_value=1; % Assuming features in [0,1]
    opts.gmmLikeli_gamma=0.05; 

    visOpts=opts.visOpts;
    visOpts.motionMultiplier=1;
    visOpts.fgArrowClr=uint8([0 0 255]); % Blue
    visOpts.bgArrowClr=uint8([255 255 0]); % Yellow
    visOpts.arrowWidth=int32(1);
    visOpts.centerClr=uint8([255 128 0]);
    visOpts.centerRad=int32(1);
    visOpts.betaVisualize_unary=10;
    visOpts.skipField=int32(25); % gap in pixels at which to draw motion field
    visOpts.segBoundaryWidth=2; % when outlining segmentation, line width to use
    visOpts.unaryVis_gammaMult=1; % Alternate way of visualizing unaries, relative
    opts.visOpts=visOpts;
 
case 'rad40_handTune'
    opts=motionCut2.segEngine_opts();
    opts.gcGamma_e=0.5;
    opts.gcGamma_s=0.1;
    opts.gcGamma_ssd=1;
    opts.gcGamma_c=0.1;
    opts.gcGamma_i=0;
    opts.gcScale=100000;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.winRad=40;
    opts.srchRad_bg=30;
    opts.srchRad_fg=30;
    opts.minPixels_match=400;

    opts.ssd_fullConfidence=0.0015; 
    opts.ssd_noConfidence=0.003;
    opts.ssdCurve_noConf_depth=0.0002; 
    opts.ssdCurve_fullConf_depth=0.0005;
    opts.ssdCurve_depthRad=2;

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.gmmUni_value=1; % Assuming features in [0,1]
    opts.gmmLikeli_gamma=0.05; 

    visOpts=opts.visOpts;
    visOpts.motionMultiplier=1;
    visOpts.fgArrowClr=uint8([0 0 255]); % Blue
    visOpts.bgArrowClr=uint8([255 255 0]); % Yellow
    visOpts.arrowWidth=int32(1);
    visOpts.centerClr=uint8([255 128 0]);
    visOpts.centerRad=int32(1);
    visOpts.betaVisualize_unary=10;
    visOpts.skipField=int32(25); % gap in pixels at which to draw motion field
    visOpts.segBoundaryWidth=2; % when outlining segmentation, line width to use
    visOpts.unaryVis_gammaMult=1; % Alternate way of visualizing unaries, relative
    opts.visOpts=visOpts;
  
  
 
case 'rad40_onlyColor'
    opts=motionCut2.segEngine_opts();
    opts.gcGamma_e=0.5;
    opts.gcGamma_s=0.1;
    opts.gcGamma_ssd=1;
    opts.gcGamma_c=0.1;
    opts.gcGamma_i=0;
    opts.gcScale=100000;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.winRad=40;
    opts.srchRad_bg=30;
    opts.srchRad_fg=30;
    opts.minPixels_match=400;

    opts.ssd_fullConfidence=0; % is never confident
    opts.ssd_noConfidence=0;
    opts.ssdCurve_noConf_depth=10; % is never confident (any num >1 will do)
    opts.ssdCurve_fullConf_depth=10; % is never confident
    opts.ssdCurve_depthRad=2;

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.gmmUni_value=1; % Assuming features in [0,1]
    opts.gmmLikeli_gamma=0.05; 

    visOpts=opts.visOpts;
    visOpts.motionMultiplier=1;
    visOpts.fgArrowClr=uint8([0 0 255]); % Blue
    visOpts.bgArrowClr=uint8([255 255 0]); % Yellow
    visOpts.arrowWidth=int32(1);
    visOpts.centerClr=uint8([255 128 0]);
    visOpts.centerRad=int32(1);
    visOpts.betaVisualize_unary=10;
    visOpts.skipField=int32(25); % gap in pixels at which to draw motion field
    visOpts.segBoundaryWidth=2; % when outlining segmentation, line width to use
    visOpts.unaryVis_gammaMult=1; % Alternate way of visualizing unaries, relative
    opts.visOpts=visOpts;
  
  otherwise
    error('Invalid options string %s\n',optsString);
end
