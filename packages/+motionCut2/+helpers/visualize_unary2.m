function posterior=visualizeUnary2(unaryEnergy,unaryMask,gammaMultiplier,gamma)
% Alternative way of visualizing energies, taking into account gamma
% It visualizies -(fgEnergy-bgEnergy)/(gamma*gammaMultiplier) and is clipped between
% +1 and -1, and then shifted to the range [0,1]

if(~exist('unaryMask','var')),
  unaryMask=128*ones([size(unaryEnergy,1) size(unaryEnergy,2)],'uint8');
end

if(~exist('betaVisualize','var')),
  betaVisualize=10;
end
divisor=gammaMultiplier*gamma;
fgE=unaryEnergy(:,:,1)/divisor;
bgE=unaryEnergy(:,:,2)/divisor;
diffE=bgE-fgE;
diffE=0.5*(diffE+1);
diffE=max(0,min(1,diffE));

posterior=diffE;

posterior(unaryMask==255)=1;
posterior(unaryMask==0)=0;
