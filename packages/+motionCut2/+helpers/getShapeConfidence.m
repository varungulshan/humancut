function conf=getShapeConfidence(mask,sigma_s)

sigma_s_sqr=sigma_s^2;
conf=zeros(size(mask));
fgMask=(mask);
bgMask=(~mask);
dFG=bwdist(fgMask,'quasi-euclidean');
dBG=bwdist(bgMask,'quasi-euclidean');

conf(bgMask)=1-exp(-(dFG(bgMask).^2)/sigma_s_sqr);
conf(fgMask)=1-exp(-(dBG(fgMask).^2)/sigma_s_sqr);
