function compile_mex()

cwd=miscFns.extractDirPath(mfilename('fullpath'));

cvFlags='-I/usr/include/opencv -lcxcore';
mexCmds=cell(0,1);
mexCmds{end+1}=sprintf('mex -O %s+cpp/motion/mex_getUnaries.cpp -outdir %s+cpp/',cwd,cwd);
mexCmds{end+1}=sprintf('mex -O %s+cpp/mex_drawMotionField.cpp %s -outdir %s+cpp/',cwd,cvFlags,cwd);
mexCmds{end+1}=sprintf('mex -O %s+cpp/mex_drawMotion.cpp %s -outdir %s+cpp/',cwd,cvFlags,cwd);
mexCmds{end+1}=sprintf('mex -O %s+cpp/motion/mex_getUnariesMotion.cpp -outdir %s+cpp/',cwd,cwd);
mexCmds{end+1}=sprintf('mex -O %s+cpp/mex_setupTransductionGraph.cpp -outdir %s+cpp/',cwd,cwd);
mexCmds{end+1}=sprintf('mex -O %s+cpp/mex_getWindowPts2.cpp -outdir %s+cpp/',cwd,cwd);
mexCmds{end+1}=sprintf('mex -O -I%s+cpp/graphCut/ %s+cpp/graphCut/mex_gcBand.cpp %s+cpp/graphCut/graph.cpp -outdir %s+cpp/',cwd,cwd,cwd,cwd);

for i=1:length(mexCmds)
  fprintf('Executing %s\n',mexCmds{i});
  eval(mexCmds{i});
end
