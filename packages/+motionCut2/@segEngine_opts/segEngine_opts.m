classdef segEngine_opts 
  properties (SetAccess=public, GetAccess=public)
    gcGamma_e  % gamma of pairwise term
    gcGamma_s  % gamma of shape term
    gcGamma_ssd  % gamma of ssd term
    gcGamma_c  % gamma of color unary term
    gcGamma_i  % gamma of pairwise term with ising model
    gcNbrType % 'nbr4' or 'nbr8'
    gcSigma_c % 'auto' or a numeric value
    gcScale   % scaling from flot to integer

    winRad   % local window radius, usually 15-40
    srchRad_bg % search radius for bg motion
    srchRad_fg % search radius for fg motion
    minPixels_match % minimum pixels to be considered for computing SSD (to prevent estimating
                    % total occlusion or moving out of the image)

    gmmNmix_fg % number of gaussian mixtures for fg modeling within local window
    gmmNmix_bg % number of gaussian mixtures for bg modeling within local window
    gmmUni_value  % parameter for adding uniform distribution to color model
    gmmLikeli_gamma % parameter for adding uniform distribution to color model 

    % Options related to detection of motion failure
    ssd_fullConfidence % if SSD is less than this value, confidence of 1 is assigned
                      % to the motion estimation, the scale of this value is 
                      % [0,1]. The ssd is divided by the number of pixels
                      % it is computed over, and then also normalized as if the 
                      % range of image was [0,1] and further normalized for number of image
                      % channels
    ssd_noConfidence % if SSD is greater than this value, confidence of 0
                     % linear interpolation for confidence, if ssd lies in between this
                     % same scale as above

    ssdCurve_fullConf_depth % if ssd curve is deeper than this value, then full confidence
                          % it means the patch was tracked distinctively
                          % its units are same as ssd_fullConfidence
    ssdCurve_noConf_depth % if ssd curve is less deep than this value, then no confidence
                          % it means the patch was not distinctive to track
                          % its units are same as ssd_fullConfidence
    ssdCurve_depthRad % the radius at which to compute the depth of the ssd curve at

    visOpts  % Structure containing various options for visualization
             % Fields include:
             % motionMultiplier:  motion vector is multiplied by this number for 
             %                    visualization purposes
             % fgArrowClr: color for drawing fg arrows of motion (uint8)
             % bgArrowClr: color for drawing bg arrows of motion (uint8)
             % arrowWidth: width of line to draw (int32)
             % centerClr: color for drawing the center of the box (uint8)
             % centerRad: radius of center of box (int32)
             % betaVisualize_unary: for visualizing unary
  end

  methods
    function obj=segEngine_opts()
      % Set the default options
      obj.gcGamma_e=5;
      obj.gcGamma_s=0.1;
      obj.gcGamma_i=0;
      obj.gcScale=100000;
      obj.gcNbrType='nbr8';
      obj.gcSigma_c='auto';

      obj.ssd_fullConfidence=1;
      obj.ssd_noConfidence=1;
      obj.ssdCurve_fullConf_depth=0;
      obj.ssdCurve_noConf_depth=0;
      obj.ssdCurve_depthRad=2;

      obj.winRad=30;
      obj.srchRad_bg=10;
      obj.srchRad_fg=15;
      obj.minPixels_match=100;

      obj.gmmNmix_fg=3;
      obj.gmmNmix_bg=3;
      obj.gmmUni_value=1; % Assuming features in [0,1]
      obj.gmmLikeli_gamma=0.05; 

      visOpts.motionMultiplier=1;
      visOpts.fgArrowClr=uint8([0 0 255]); % Blue
      visOpts.bgArrowClr=uint8([255 255 0]); % Yellow
      visOpts.arrowWidth=int32(2);
      visOpts.centerClr=uint8([255 128 0]);
      
      visOpts.betaVisualize_unary=10;
      visOpts.skipField=int32(10); % gap in pixels at which to draw motion field
      visOpts.segBoundaryWidth=2; % when outlining segmentation, line width to use
      visOpts.unaryVis_gammaMult=1; % Alternate way of visualizing unaries, relative
      % to the pairwise edge strenght. See visualize_unary2.m for details
      obj.visOpts=visOpts;
 
    end
  end

end % of classdef
