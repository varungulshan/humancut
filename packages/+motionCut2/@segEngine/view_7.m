function img=view_7(obj)
% visualize the unary obtained from color models (shape+ssd)

fprintf('View 07: Color unary energies visualization (alternate) at time t\n');
if(obj.debugLevel>0)
  if(obj.frameNum==1),
    img=zeros([obj.h obj.w 3]);
    msg=sprintf('Frame_1_does_not_learn_color_models',obj.frameNum);
    %img=miscFns.rendertext(img,msg,[1 1 1],'ovr'); 
    img=miscFns.mex_drawText(im2uint8(img),int32(15),msg);
  else
    debugInfo=obj.debugInfo;
    maxValue=1;
    img=motionCut2.helpers.visualize_unary2(debugInfo.colorUnaries,debugInfo.unaryMask,...
                  1,maxValue);
    img=repmat(img,[1 1 3]);
  end
else
  img=zeros([obj.h obj.w 3]);
  img=miscFns.rendertext(img,'No view 1 at debugLevel 0',[1 1 1],'ovr'); 
end
