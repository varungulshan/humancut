function img=view_6(obj)
% visualize the unary obtained from motion propagation (shape+ssd)

fprintf('View 06: Motion Unary energies visualization (alterate), at time t\n');
if(obj.debugLevel>0)
  if(obj.frameNum==1),
    img=zeros([obj.h obj.w 3]);
    msg=sprintf('Frame_1_does_not_learn_color_models',obj.frameNum);
    %img=miscFns.rendertext(img,msg,[1 1 1],'ovr'); 
    img=miscFns.mex_drawText(im2uint8(img),int32(15),msg);
  else
    debugInfo=obj.debugInfo;
    maxValue=min(2*obj.opts.gcGamma_s,obj.opts.gcGamma_s+obj.opts.gcGamma_ssd);
    img=motionCut2.helpers.visualize_unary2(debugInfo.motionUnaries,debugInfo.unaryMask,...
                  1,maxValue);
    img=repmat(img,[1 1 3]);
  end
else
  img=zeros([obj.h obj.w 3]);
  img=miscFns.rendertext(img,'No view 1 at debugLevel 0',[1 1 1],'ovr'); 
end
