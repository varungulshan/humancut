classdef segEngine < videoSegmenter
  properties (SetAccess=private, GetAccess=public)
    debugLevel
    gtFile % gt for first frame
    vH
    gtSeg_firstFrame
    h
    w
    nCh
    nFrames
    state % 'initialized','pped','segStarted'
    seg % uint8, seg variable for the entire volume
    frameNum
    framesSegmented
    beta
    roffset %int32
    coffset %int32
    opts % object of class segEngine_opts, see class definiton for settings
    localWindows_idx % only set if debugLevel>0, for visualization purposes
    debugInfo % structure with useful debugging information contains:
              % N denotes the number of local windows below
              % motionFG -> [2 x N] fg motion vectors for each local window
              % motionBG -> [2 x N] bg motion vectors for each local window
              % bestFG_ssd -> [1 x N] avg SSD for fg motion
              % bestBG_ssd -> [1 x N] avg SSD for bg motion
              % validFG_ssd -> [1 x N] number of valid pixels used for computing fg ssd
              % validBG_ssd -> [1 x N] number of valid pixels used for computing bg ssd
              % unaryEnergy -> [h x w x 2] double of computed unaries
              % unaryMask -> valid mask for unary energies
  end

  methods
    function obj=segEngine(debugLevel,gtFile,vH,opts)
      obj.debugLevel=debugLevel;
      obj.gtFile=gtFile; 
      obj.vH=vH;
      [obj.h obj.w obj.nCh obj.nFrames]=obj.vH.videoSize();
      obj.state='initialized';
      obj.seg=zeros([obj.h obj.w obj.nFrames],'uint8');
      obj.framesSegmented=0;
      obj.frameNum=1;
      obj.opts=opts;
      obj.beta=[];
   end
    img=view_1(obj) % Shows the local windows for motion estimation in prev frame
    img=view_2(obj) % Shows the estimated motion vectors for both bg and fg
    img=view_3(obj) % Shows the local windows for motion estimation -- i.e in 
                    % previous frame also shows the box numbers overlaid
    img=view_4(obj) % Shows unaries for SSD only (at time t)
    img=view_5(obj) % Shows unaries for shape propagation only (at time t)
    img=view_6(obj) % Shows unaries for motion (ssd+shape) (at time t)
    img=view_7(obj) % Shows unaries for color only (at time t)
    img=view_8(obj) % Shows unaries for combined color+motion (at time t)
    img=view_9(obj) % Shows the output segmentation outlined on original frame
    img=view_10(obj) % Shows the output segmentation

    printOptions(obj,fH) % prints the segmentation options to a file or std-out
                         % If not fH is specified, prints to stdout
  end

  methods (Access=private)
    seg=gcSeg(obj,unaryEnergy,mask,img)
    nxtSeg=motionCut_seg(obj,curFrame,nxtFrame,curSeg)
  end

  methods (Static=true)
    viewStr=describeViews(); % Returns a string describing all the views
  end

end % of classdef
