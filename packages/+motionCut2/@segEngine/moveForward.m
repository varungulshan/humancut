function ok=moveForward(obj,labelImg)

ok=false;

if(~strcmp(obj.state,'segStarted')),
  warning('Cant move unless segmentation is started\n');
end

if(obj.frameNum==obj.nFrames)
  warning('Cant go beyond nFrames\n');
  ok=false;
  return;
end

if(obj.frameNum<obj.framesSegmented),
  obj.frameNum=obj.frameNum+1;
  obj.vH.moveForward();
  ok=true;
  return;
end

if(obj.frameNum>obj.framesSegmented),
  ok=false;
  warning('Bug in motionCut2, frameNum>framesSegmented\n');
  return;
end

curFrame=obj.vH.curFrame;
obj.vH.moveForward();
nxtFrame=obj.vH.curFrame;
curSeg=obj.seg(:,:,obj.frameNum);

nxtSeg=obj.motionCut_seg(curFrame,nxtFrame,curSeg);

obj.framesSegmented=obj.framesSegmented+1;
obj.frameNum=obj.frameNum+1;
obj.seg(:,:,obj.frameNum)=nxtSeg;

ok=true;
