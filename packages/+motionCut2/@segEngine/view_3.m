function img=view_3(obj)
% Shows local windows in previous frame which were used to propagate segmentation
% to the current frame

fprintf('View 03: Local windows with box numbers at time t-1\n');

if(obj.debugLevel>0)
  if(obj.frameNum==1),
    img=zeros([obj.h obj.w 3]);
    msg=sprintf('No local windows in previous frame',obj.frameNum);
    %img=miscFns.rendertext(img,msg,[1 1 1],'ovr'); 
    img=miscFns.mex_drawText(im2uint8(img),int32(15),msg);
  else
    obj.vH.moveBackward();
    prevFrame=im2double(obj.vH.curFrame);
    prevSeg=obj.seg(:,:,obj.frameNum-1);
    obj.vH.moveForward();
    [segBoundaryMask,segBoundaryColors]= ...
    miscFns.getSegBoundary_twoColors(prevSeg,[0 1 0],[1 0 0],1,1);
    prevFrame(segBoundaryMask)=segBoundaryColors;
    img=drawBoxes(prevFrame,obj.localWindows_idx,obj.opts.winRad);
  end
else
  img=zeros([obj.h obj.w 3]);
  img=miscFns.rendertext(img,'No view 1 at debugLevel 0',[1 1 1],'ovr'); 
end

function img=drawBoxes(img,windows_idx,winRad)

winClr=[1 1 0]; % yellow
penRad=1;

[h w nCh]=size(img);

for i=1:length(windows_idx)
  [winY,winX]=ind2sub([h w],windows_idx(i));
  xMin=max(1,winX-winRad);
  xMax=min(w,winX+winRad);
  yMin=max(1,winY-winRad);
  yMax=min(h,winY+winRad);
  img=drawBox(img,winClr,penRad,xMin,xMax,yMin,yMax,i-1);
end

function img=drawBox(img,winClr,penRad,xMin,xMax,yMin,yMax,boxIdx)

[h w nCh]=size(img);

% Clockwise: left bar, top bar, right bar, bottom bar
xL=[xMin-penRad xMin-penRad xMax-penRad xMin-penRad];
xR=[xMin+penRad xMax+penRad xMax+penRad xMax+penRad];
yT=[yMin-penRad yMin-penRad yMin-penRad yMax-penRad];
yB=[yMax+penRad yMin+penRad yMax+penRad yMax+penRad];

xL=max(1,xL);
xR=min(w,xR);
yT=max(1,yT);
yB=min(h,yB);

for j=1:4
  x_left=xL(j);
  x_right=xR(j);
  y_top=yT(j);
  y_bottom=yB(j);

  for i=1:3
    img(y_top:y_bottom,x_left:x_right,i)=winClr(i);
  end
end

x_left=xL(1);
y_top=yT(1);

boxTxt=im2double(printText(boxIdx));
textW=min(size(boxTxt,2),w-x_left+1);
textH=min(size(boxTxt,1),h-y_top+1);
img(y_top:(y_top+textH-1),x_left:(x_left+textW-1),:)=boxTxt(1:textH,1:textW,:);

function txtImg=printText(boxNumber)

hText=20;
wText=20;

img=zeros([hText wText],'uint8');
txtImg=miscFns.renderText(img,sprintf('%02d',boxNumber),[255 255 255],'ovr');
