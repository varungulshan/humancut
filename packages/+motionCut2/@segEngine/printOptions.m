function printOptions(obj,fH)

if(~exist('fH','var')),
  fH=1;
end;

% Convert obj.opts object into a structure
warning('off','MATLAB:structOnObject');
opts=struct(obj.opts);
warning('on','MATLAB:structOnObject');
opts.beta=obj.beta;
opts.debugLevel=obj.debugLevel;
opts.gtFile=obj.gtFile;
%opts.visOpts=obj.visOpts;

fprintf(fH,'Printing options for motionCut2:\n');
miscFns.printStructure(opts,fH);
