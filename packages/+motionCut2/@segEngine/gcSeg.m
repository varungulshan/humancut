function seg=gcSeg(obj,unaryImg,mask,img)

import motionCut2.cpp.*

opts=obj.opts;
beta=obj.beta;

[seg,flow]=mex_gcBand(mask,unaryImg(:,:,1),unaryImg(:,:,2),opts.gcGamma_e,beta,...
                      obj.roffset',obj.coffset',opts.gcScale,opts.gcGamma_i,...
                      img);

% TO CHECK empirically if mex_gcBand handles overflow properly!
