function nxtSeg=motionCut_seg(obj,curFrame,nxtFrame,curSeg)
% curFrame and nxtFrame should be uint8
import motionCut2.*;

if(~strcmp(class(curFrame),'uint8')|~strcmp(class(nxtFrame),'uint8'))
  error('motionCut_seg expects uint8 inputs for images\n');
end
curFrame=int32(curFrame);
nxtFrame=int32(nxtFrame);

opts=obj.opts;
winRad=opts.winRad;
[bdryMask,localWindows_idx]=cpp.mex_getWindowPts2(curSeg==255,int32(winRad));

motionParams.winRad=int32(winRad);
motionParams.srchRad_bg=int32(opts.srchRad_bg);
motionParams.srchRad_fg=int32(opts.srchRad_fg);
motionParams.gcGamma_s=opts.gcGamma_s;
motionParams.gcGamma_ssd=opts.gcGamma_ssd;
motionParams.gcGamma_c=opts.gcGamma_c;
motionParams.minPixels_match=int32(opts.minPixels_match);
motionParams.ssd_fullConfidence=opts.ssd_fullConfidence;
motionParams.ssd_noConfidence=opts.ssd_noConfidence;
motionParams.ssdCurve_fullConf_depth=opts.ssdCurve_fullConf_depth;
motionParams.ssdCurve_noConf_depth=opts.ssdCurve_noConf_depth;
motionParams.ssdCurve_depthRad=int32(opts.ssdCurve_depthRad);

gmmOpts.nMix_fg=opts.gmmNmix_fg;
gmmOpts.nMix_bg=opts.gmmNmix_bg;
gmmOpts.uniValue=opts.gmmUni_value;
gmmOpts.likeliGamma=opts.gmmLikeli_gamma;

debugLevel=int32(obj.debugLevel);

[gmmf,gmmb]=learnLocalGmms(double(curFrame)/255,localWindows_idx,winRad,...
              curSeg==255,gmmOpts);
% gmmfs and gmmb are arrays of structures

[unaryEnergy,unaryEnergy_valid,debugInfo]=cpp.mex_getUnaries(curFrame,nxtFrame,...
             curSeg==255,localWindows_idx-1,motionParams,debugLevel,...
             gmmf,gmmb,gmmOpts);

unaryMask=curSeg;
unaryMask(unaryEnergy_valid)=128;
nxtSeg=obj.gcSeg(unaryEnergy,unaryMask,double(nxtFrame)/255);
obj.localWindows_idx=localWindows_idx;
if(obj.debugLevel>=1)
  debugInfo.unaryEnergy=unaryEnergy;
  debugInfo.unaryMask=unaryMask;
end
obj.debugInfo=debugInfo;

function [gmmfAll,gmmbAll]=learnLocalGmms(img,winIdx,winRad,mask,opts)

[h w nCh]=size(img);

for i =1:length(winIdx)
  cur_winIdx=winIdx(i);
  [winY,winX]=ind2sub([h w],cur_winIdx);

  xMin=max(1,winX-winRad);xL_pad1=(xMin-(winX-winRad));
  xMax=min(w,winX+winRad);
  yMin=max(1,winY-winRad);yT_pad1=(yMin-(winY-winRad));
  yMax=min(h,winY+winRad);

  xRng=[xMin:xMax];yRng=[yMin:yMax];
  win1_h=length(yRng);
  win1_w=length(xRng);

  fgMask=mask(yRng,xRng);
  bgMask=~fgMask;
  cropImg=img(yRng,xRng,:);

  fgFts=cropImg(repmat(fgMask,[1 1 nCh]));
  numFg_fts=nnz(fgMask);
  fgFts=reshape(fgFts,[numFg_fts nCh])';
  fgGmm=gmm.init_gmmBS(fgFts,opts.nMix_fg);
  for j=1:size(fgGmm.sigma,3)
    fgGmm.denom(j)=1/sqrt( (2*pi)^nCh * abs(det(fgGmm.sigma(:,:,j))) );
    fgGmm.invSigma(:,:,j)=inv(fgGmm.sigma(:,:,j));
  end

  bgFts=cropImg(repmat(bgMask,[1 1 nCh]));
  numBg_fts=nnz(bgMask);
  bgFts=reshape(bgFts,[numBg_fts nCh])';
  bgGmm=gmm.init_gmmBS(bgFts,opts.nMix_bg);
  for j=1:size(bgGmm.sigma,3)
    bgGmm.denom(j)=1/sqrt( (2*pi)^nCh * abs(det(bgGmm.sigma(:,:,j))) );
    bgGmm.invSigma(:,:,j)=inv(bgGmm.sigma(:,:,j));
  end

  gmmfAll(i)=fgGmm;
  gmmbAll(i)=bgGmm;
end
