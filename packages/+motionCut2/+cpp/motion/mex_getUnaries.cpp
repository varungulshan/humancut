// Computes unaries for video segmentation using propagated masks, SSD differences and
// local color models

#include "mex.h"
#include <float.h>
#include <memory.h>
#include <cmath>
#include "matrix.h"
#include <iostream>
#include <vector>
#include <cfloat> // For limits of floating points

#define EPS 1e-11

typedef unsigned int uint;
void printStruct_fieldNames(const mxArray* st);
inline double gmmLikeli(const double* ftr,int nMix,const double* mu,const double* invSigma,
    const double* pi, const double* multiplier);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters
     rhs[0] = curFrame     [h x w x nCh] (int32) current frame
     rhs[1] = nxtFrame     [h x w x nCh] (int32) next frame
     rhs[2] = curSeg       [h x w] (logical) current segmentation
     rhs[3] = windowIdx    [N x 1] (int32) index of window centers (0 indexed)
     rhs[4] = motionParams struct with following fields:
       winRad -> (scalar int32) radius of window support
       srchRad_bg -> (scalar int32) search radius for bg motion estimation
       srchRad_fg -> (scalar int32) search radius for fg motion estimation
       gcGamma_s -> (scalar double) weighing of shape mask unary
       gcGamma_ssd -> (scalar double) weighing of ssd unary
       gcGamma_c -> (scalar double) weighing of color unary
       minPixels_match -> (scalar int32) the min. valid pixels to be considered for SSD
                          computation.
       ssd_fullConfidence -> (scalar double) see segEngine_opts for description
       ssd_noConfidence -> (scalar double) see segEngine_opts for description
       ssdCurve_fullConf_depth -> (scalar double) see segEngine_opts for description
       ssdCurve_noConf_depth -> (scalar double) see segEngine_opts for description
       ssdCurve_depthRad -> (scalar int32) see segEngine_opts for description
     rhs[5] = (scalar int32) debug level
     rhs[6] = [N x 1] stucture of foreground gmm's with following fields:
              mu [nCh x K], invSigma [nCh x nCh x K] , pi [K x 1]  and denom [K x 1]
     rhs[7] = [N x 1] stucture of background gmm's with following fields:
              mu [nCh x K], invSigma [nCh x nCh x K] , pi [K x 1]  and denom [K x 1]
     rhs[8] = gmmOpts structure with (atleast the) following fields:
              uniValue (scalar double)
              likeliGamma (scalar double)

     Output: 
      lhs[0] = [h x w x 2] (double) unaries. (:,:,1) denotes fg data terms
                                   and (:,:,2) denotes bg data terms
      lhs[1] = [h x w] (logical) mask denoting where the unaries got computed
      lhs[2] = struct for debuggin info. depending on debugLevel different fields are returned
      various fields are:
          motionFG -> [2 x N] (int32) array for fg motion vector, one vector per window
                      (debugLevel>=1)
          motionBG -> [2 x N] (int32) array for bg motion vector, one vector per window
                      (debugLevel>=1)
          bestFG_ssd -> [1 x N] (double) array for average SSD for fg motion
                      (debugLevel>=1)
          bestBG_ssd -> [1 x N] (double) array for average SSD for bg motion
                      (debugLevel>=1)
          validFG_ssd -> [1 x N] (int32) the number of valid pixels used for computing fg motion (debugLevel>=1)
          validBG_ssd -> [1 x N] (int32) the number of valid pixels used for computing bg motion (debugLevel>=1)
          ssdCurveFG_depths -> [1 x N] double array, which gives the curvature of SSD surface
                            for the fg motion estimation (debugLevel>=1)
          ssdUnaries -> [h x w x 2] (double) unaries using SSD (debugLevel>=1)
          shapeUnaries -> [h x w x 2] (double) unaries using propagated shape mask
                                      (debugLevel>=1)
          motionUnaries -> [h x w x 2] (double) unaries combining SSD+shape (debugLevel>=1)
          colorUnaries -> [h x w x 2] (double) unaires using color models (debugLevel>=1)
          motionConf   -> [1 x N] (double) array for computed value of motion confidence
                          (debugLevel>=1)

     Notes:
     It is possible that the SSD might cause integer overflow. Its unlikely given the nature
     of images, but is something you should correct in the future to make the code super
     robust. I have kept it integer for speed reasons (i assume integer arithmetic is faster
     than double right?)
  */

  if (nrhs != 9)
    mexErrMsgTxt("9 input arguments expected.");
  if (nlhs != 3)
    mexErrMsgTxt("3 output arguments expected.");

  if(mxGetClassID(prhs[0])!=mxINT32_CLASS) mexErrMsgTxt("prhs[0] (curFrame) shd be of type int32\n");
  if(mxGetClassID(prhs[1])!=mxINT32_CLASS) mexErrMsgTxt("prhs[1] (nxtFrame) shd be of type int32\n");
  if(mxGetClassID(prhs[2])!=mxLOGICAL_CLASS) mexErrMsgTxt("prhs[2] (curSeg) shd be of type logical\n");
  if(mxGetClassID(prhs[3])!=mxINT32_CLASS) mexErrMsgTxt("prhs[3] (windowIdx) shd be of type int32\n");
  if(!mxIsStruct(prhs[4])) mexErrMsgTxt("prhs[4] (motionParams) shd be of type struct\n");
  if(mxGetClassID(prhs[5])!=mxINT32_CLASS) mexErrMsgTxt("prhs[5] (debugLevel) shd be of type int32\n");
  if(!mxIsStruct(prhs[6])) mexErrMsgTxt("prhs[6] (fgGmms) shd be of type struct\n");
  if(!mxIsStruct(prhs[7])) mexErrMsgTxt("prhs[7] (bgGmms) shd be of type struct\n");
  if(!mxIsStruct(prhs[8])) mexErrMsgTxt("prhs[8] (gmmOpts) shd be of type struct\n");

  int debugLevel;
  debugLevel=*((int*)mxGetData(prhs[5]));
  int h=mxGetDimensions(prhs[0])[0];
  int w=mxGetDimensions(prhs[0])[1];
  int nCh;
  if(mxGetNumberOfDimensions(prhs[0])<3){nCh=1;}
  else{nCh=mxGetDimensions(prhs[0])[2];}
  int N=h*w;

  if(nCh!=3)
    mexErrMsgTxt("Currently only RGB images are supported\n");

  if(mxGetDimensions(prhs[1])[0]!=h||mxGetDimensions(prhs[1])[1]!=w)
     mexErrMsgTxt("Invalid dimensions for prhs[1]\n");
  int nCh_nxt;
  if(mxGetNumberOfDimensions(prhs[1])<3){nCh_nxt=1;}
  else{nCh_nxt=mxGetDimensions(prhs[1])[2];}
  if(nCh_nxt!=nCh)
     mexErrMsgTxt("Invalid dimensions for prhs[1]\n");

  if(mxGetM(prhs[2])!=h||mxGetN(prhs[2])!=w)
     mexErrMsgTxt("Invalid dimensions for prhs[2]\n");
    
  int nWindows=mxGetM(prhs[3]);
  if(mxGetN(prhs[3])!=1)
    mexErrMsgTxt("windowIdx should be a column vector\n");

  if(mxGetNumberOfElements(prhs[4])!=1)
    mexErrMsgTxt("Params structure should be 1x1\n");
  if(mxGetNumberOfElements(prhs[6])!=nWindows)
    mexErrMsgTxt("There should be nWindows gmms in fgGmm\n");
  if(mxGetNumberOfElements(prhs[7])!=nWindows)
    mexErrMsgTxt("There should be nWindows gmms in bgGmm\n");
  if(mxGetNumberOfElements(prhs[8])!=1)
    mexErrMsgTxt("There should be one structure in gmmOpts\n");

  mxArray *mx_winRad=mxGetField(prhs[4],0,"winRad");
  mxArray *mx_srchRad_bg=mxGetField(prhs[4],0,"srchRad_bg");
  mxArray *mx_srchRad_fg=mxGetField(prhs[4],0,"srchRad_fg");
  mxArray *mx_gcGamma_s=mxGetField(prhs[4],0,"gcGamma_s");
  mxArray *mx_gcGamma_ssd=mxGetField(prhs[4],0,"gcGamma_ssd");
  mxArray *mx_gcGamma_c=mxGetField(prhs[4],0,"gcGamma_c");
  mxArray *mx_minPixels_match=mxGetField(prhs[4],0,"minPixels_match");
  mxArray *mx_ssd_fullConfidence=mxGetField(prhs[4],0,"ssd_fullConfidence");
  mxArray *mx_ssd_noConfidence=mxGetField(prhs[4],0,"ssd_noConfidence");
  mxArray *mx_ssdCurve_fullConfD=mxGetField(prhs[4],0,"ssdCurve_fullConf_depth");
  mxArray *mx_ssdCurve_noConfD=mxGetField(prhs[4],0,"ssdCurve_noConf_depth");
  mxArray *mx_ssdCurve_depthRad=mxGetField(prhs[4],0,"ssdCurve_depthRad");

  if(debugLevel>=2)
    printStruct_fieldNames(prhs[4]);

  if(mx_winRad==NULL)
    mexErrMsgTxt("winRad field not found in params structure\n"); if(mx_srchRad_fg==NULL) mexErrMsgTxt("srchRad_fg field not found in params structure\n");
  if(mx_srchRad_bg==NULL)
    mexErrMsgTxt("srchRad_bg field not found in params structure\n");
  if(mx_gcGamma_s==NULL)
    mexErrMsgTxt("gcGamma_s field not found in params structure\n");
  if(mx_gcGamma_ssd==NULL)
    mexErrMsgTxt("gcGamma_ssd field not found in params structure\n");
  if(mx_gcGamma_c==NULL)
    mexErrMsgTxt("gcGamma_c field not found in params structure\n");
  if(mx_minPixels_match==NULL){
    mexErrMsgTxt("minPixels_match field not found in params structure\n");
  }
  if(mx_ssd_fullConfidence==NULL){
    mexErrMsgTxt("ssd_fullConfidence field not found in params structure\n");
  }
  if(mx_ssd_noConfidence==NULL){
    mexErrMsgTxt("ssd_noConfidence field not found in params structure\n");
  }
  if(mx_ssdCurve_fullConfD==NULL){
    mexErrMsgTxt("ssdCurve_fullConf_depth field not found in params structure\n");
  }
  if(mx_ssdCurve_noConfD==NULL){
    mexErrMsgTxt("ssdCurve_noConf_depth field not found in params structure\n");
  }
  if(mx_ssdCurve_depthRad==NULL){
    mexErrMsgTxt("ssdCurve_depthRad field not found in params structure\n");
  }

  mxArray *mx_uniValue=mxGetField(prhs[8],0,"uniValue"); 
  mxArray *mx_likeliGamma=mxGetField(prhs[8],0,"likeliGamma"); 
  if(mx_uniValue==NULL)
    mexErrMsgTxt("uniValue field not found in gmmOpts structure\n");
  if(mx_likeliGamma==NULL)
    mexErrMsgTxt("likeliGamma field not found in gmmOpts structure\n");

  if(mxGetClassID(mx_winRad)!=mxINT32_CLASS)
    mexErrMsgTxt("params.winRad needs to be int32\n");
  if(mxGetClassID(mx_srchRad_bg)!=mxINT32_CLASS)
    mexErrMsgTxt("params.srchRad_bg needs to be int32\n");
  if(mxGetClassID(mx_srchRad_fg)!=mxINT32_CLASS)
    mexErrMsgTxt("params.srchRad_fg needs to be int32\n");
  if(mxGetClassID(mx_gcGamma_s)!=mxDOUBLE_CLASS)
    mexErrMsgTxt("params.gcGamma_s needs to be double\n");
  if(mxGetClassID(mx_gcGamma_ssd)!=mxDOUBLE_CLASS)
    mexErrMsgTxt("params.gcGamma_ssd needs to be double\n");
  if(mxGetClassID(mx_gcGamma_c)!=mxDOUBLE_CLASS)
    mexErrMsgTxt("params.gcGamma_c needs to be double\n");
  if(mxGetClassID(mx_minPixels_match)!=mxINT32_CLASS)
    mexErrMsgTxt("params.minPixels_match needs to be int32\n");
  if(mxGetClassID(mx_ssd_fullConfidence)!=mxDOUBLE_CLASS)
    mexErrMsgTxt("params.ssd_fullConfidence needs to be double\n");
  if(mxGetClassID(mx_ssd_noConfidence)!=mxDOUBLE_CLASS)
    mexErrMsgTxt("params.ssd_noConfidence needs to be double\n");
  if(mxGetClassID(mx_ssdCurve_fullConfD)!=mxDOUBLE_CLASS)
    mexErrMsgTxt("params.ssdCurve_fullConf_depth needs to be double\n");
  if(mxGetClassID(mx_ssdCurve_noConfD)!=mxDOUBLE_CLASS)
    mexErrMsgTxt("params.ssdCurve_noConf_depth needs to be double\n");
  if(mxGetClassID(mx_ssdCurve_depthRad)!=mxINT32_CLASS)
    mexErrMsgTxt("params.ssdCurve_depthRad needs to be int32\n");
  if(mxGetClassID(mx_uniValue)!=mxDOUBLE_CLASS)
    mexErrMsgTxt("gmmOpts.uniValue needs to be double\n");
  if(mxGetClassID(mx_likeliGamma)!=mxDOUBLE_CLASS)
    mexErrMsgTxt("gmmOpts.likeliGamma needs to be double\n");

  int *curFrame,*nxtFrame;
  bool *curSeg;
  int *windowIdx;
  int winRad,srchRad_bg,srchRad_fg;
  double gcGamma_s,gcGamma_c,gcGamma_ssd;
  int minPixels_match;
  double ssd_fullConf,ssd_noConf,ssdCurve_fullConfD,ssdCurve_noConfD;
  int ssdCurve_depthRad;
  double uniValue,likeliGamma;

  curFrame=(int*)mxGetData(prhs[0]);
  nxtFrame=(int*)mxGetData(prhs[1]);
  curSeg=(bool*)mxGetData(prhs[2]);
  windowIdx=(int*)mxGetData(prhs[3]);
  winRad=*((int*)mxGetData(mx_winRad));
  srchRad_bg=*((int*)mxGetData(mx_srchRad_bg));
  srchRad_fg=*((int*)mxGetData(mx_srchRad_fg));
  gcGamma_s=*mxGetPr(mx_gcGamma_s);
  gcGamma_ssd=*mxGetPr(mx_gcGamma_ssd);
  gcGamma_c=*mxGetPr(mx_gcGamma_c);
  minPixels_match=*((int*)mxGetData(mx_minPixels_match));
  ssd_fullConf=*mxGetPr(mx_ssd_fullConfidence);
  ssd_noConf=*mxGetPr(mx_ssd_noConfidence);
  ssdCurve_fullConfD=*mxGetPr(mx_ssdCurve_fullConfD);
  ssdCurve_noConfD=*mxGetPr(mx_ssdCurve_noConfD);
  ssdCurve_depthRad=*(int*)mxGetData(mx_ssdCurve_depthRad);
  uniValue=*mxGetPr(mx_uniValue);
  likeliGamma=*mxGetPr(mx_likeliGamma);

  int dims[3];dims[0]=h;dims[1]=w;dims[2]=2;
  plhs[0]=mxCreateNumericArray(3,dims,mxDOUBLE_CLASS,mxREAL);
  int dims2[2];dims2[0]=h;dims2[1]=w;
  plhs[1]=mxCreateNumericArray(2,dims2,mxLOGICAL_CLASS,mxREAL);
  plhs[2]=mxCreateStructMatrix(1,1,0,NULL);
  mxArray *debugOut=plhs[2];
  mxArray *motionFG,*motionBG,*bestFG_ssd,*bestBG_ssd,*validFG_ssd,*validBG_ssd;
  int *motionFG_ptr,*motionBG_ptr;
  double *bestFG_ssd_ptr,*bestBG_ssd_ptr;
  int *validFG_ssd_ptr,*validBG_ssd_ptr;
  mxArray *mx_ssdCurveFG_depths,*mx_ssdUnaries,*mx_shapeUnaries,
          *mx_motionUnaries,*mx_colorUnaries,*mx_motionConf;
  double *ssdCurveFG_depths,*ssdUnaries,*shapeUnaries,*motionUnaries,
         *colorUnaries,*motionConf;

  if(debugLevel>=2){
    mexPrintf("Printing parameters for motion seg:\n");
    mexPrintf("Window radius: %d\nBG motion search radius: %d\nFG motion search radius: %d\ngcGamma_s: %f\nminPixels_match: %d\n",winRad,srchRad_bg,srchRad_fg,gcGamma_s,minPixels_match);
    mexPrintf("gcGamma_c: %f\ngcGamma_ssd: %f\nssd_fullConf: %f\nssd_noConf: %f\nssdCurve_fullConfD: %f\nssdCurve_noConfD: %f\nssdCurve_depthRad: %d\n",gcGamma_c,gcGamma_ssd,ssd_fullConf,ssd_noConf,ssdCurve_fullConfD,ssdCurve_noConfD,ssdCurve_depthRad);
  }

  if(debugLevel>=1){
    mxAddField(debugOut,"motionFG");
    mxAddField(debugOut,"motionBG");
    mxAddField(debugOut,"bestFG_ssd");
    mxAddField(debugOut,"bestBG_ssd");
    mxAddField(debugOut,"validFG_ssd");
    mxAddField(debugOut,"validBG_ssd");
    mxAddField(debugOut,"ssdCurveFG_depths");
    mxAddField(debugOut,"ssdUnaries");
    mxAddField(debugOut,"shapeUnaries");
    mxAddField(debugOut,"motionUnaries");
    mxAddField(debugOut,"colorUnaries");
    mxAddField(debugOut,"motionConf");
    int dims_tmp[2];
    dims_tmp[0]=2;dims_tmp[1]=nWindows;
    motionFG=mxCreateNumericArray(2,dims_tmp,mxINT32_CLASS,mxREAL);
    motionBG=mxCreateNumericArray(2,dims_tmp,mxINT32_CLASS,mxREAL);
    dims_tmp[0]=1;dims_tmp[1]=nWindows;
    bestFG_ssd=mxCreateNumericArray(2,dims_tmp,mxDOUBLE_CLASS,mxREAL);
    bestBG_ssd=mxCreateNumericArray(2,dims_tmp,mxDOUBLE_CLASS,mxREAL);
    validFG_ssd=mxCreateNumericArray(2,dims_tmp,mxINT32_CLASS,mxREAL);
    validBG_ssd=mxCreateNumericArray(2,dims_tmp,mxINT32_CLASS,mxREAL);
    mx_ssdCurveFG_depths=mxCreateNumericArray(2,dims_tmp,mxDOUBLE_CLASS,mxREAL);
    mx_motionConf=mxCreateNumericArray(2,dims_tmp,mxDOUBLE_CLASS,mxREAL);
    int dims_tmp2[3];
    dims_tmp2[0]=h;dims_tmp2[1]=w;dims_tmp2[2]=2;
    mx_ssdUnaries=mxCreateNumericArray(3,dims_tmp2,mxDOUBLE_CLASS,mxREAL);
    mx_shapeUnaries=mxCreateNumericArray(3,dims_tmp2,mxDOUBLE_CLASS,mxREAL);
    mx_motionUnaries=mxCreateNumericArray(3,dims_tmp2,mxDOUBLE_CLASS,mxREAL);
    mx_colorUnaries=mxCreateNumericArray(3,dims_tmp2,mxDOUBLE_CLASS,mxREAL);
  
    mxSetField(debugOut,0,"motionFG",motionFG);
    mxSetField(debugOut,0,"motionBG",motionBG);
    mxSetField(debugOut,0,"bestFG_ssd",bestFG_ssd);
    mxSetField(debugOut,0,"bestBG_ssd",bestBG_ssd);
    mxSetField(debugOut,0,"validFG_ssd",validFG_ssd);
    mxSetField(debugOut,0,"validBG_ssd",validBG_ssd);
    mxSetField(debugOut,0,"ssdCurveFG_depths",mx_ssdCurveFG_depths);
    mxSetField(debugOut,0,"ssdUnaries",mx_ssdUnaries);
    mxSetField(debugOut,0,"shapeUnaries",mx_shapeUnaries);
    mxSetField(debugOut,0,"motionUnaries",mx_motionUnaries);
    mxSetField(debugOut,0,"colorUnaries",mx_colorUnaries);
    mxSetField(debugOut,0,"motionConf",mx_motionConf);
    motionFG_ptr=(int*)mxGetData(motionFG);
    motionBG_ptr=(int*)mxGetData(motionBG);
    bestFG_ssd_ptr=mxGetPr(bestFG_ssd);
    bestBG_ssd_ptr=mxGetPr(bestBG_ssd);
    validFG_ssd_ptr=(int*)mxGetData(validFG_ssd);
    validBG_ssd_ptr=(int*)mxGetData(validBG_ssd);
    ssdCurveFG_depths=mxGetPr(mx_ssdCurveFG_depths);
    ssdUnaries=mxGetPr(mx_ssdUnaries);
    shapeUnaries=mxGetPr(mx_shapeUnaries);
    motionUnaries=mxGetPr(mx_motionUnaries);
    colorUnaries=mxGetPr(mx_colorUnaries);
    motionConf=mxGetPr(mx_motionConf);

    memset(ssdUnaries,0,2*N*sizeof(double));
    memset(shapeUnaries,0,2*N*sizeof(double));
    memset(motionUnaries,0,2*N*sizeof(double));
    memset(colorUnaries,0,2*N*sizeof(double));
  }

  double *unaries=mxGetPr(plhs[0]);
  memset(unaries,0,2*N*sizeof(double));
  bool *unaryMask=(bool*)mxGetData(plhs[1]);
  memset(unaryMask,0,N*sizeof(bool));

  int *winCount=(int*)mxMalloc(N*sizeof(int));
  memset(winCount,0,N*sizeof(int));
  int boxDia=2*winRad+1;
  int srchDia_fg=2*srchRad_fg+1;
  double *ssdSurface=(double*)mxMalloc(srchDia_fg*srchDia_fg*sizeof(double));
  double *tmpUnary=(double*)mxMalloc(2*N*sizeof(double));
  double ssdDivisor=255*255*nCh;
  mxArray *mx_tmpConf;
  // -------- All variable checked and initialized above ---------

  for(int iBox=0;iBox<nWindows;iBox++){
    int boxIdx=windowIdx[iBox];
    int xBox=boxIdx/h;
    int yBox=boxIdx%h;
    int xT=std::max(0,xBox-winRad);
    int yT=std::max(0,yBox-winRad);
    int boxW=xBox-xT+1+std::min(winRad,w-1-xBox);
    int boxH=yBox-yT+1+std::min(winRad,h-1-yBox); 
    int idxTop=xT*h+yT;
    //int xB=std::min(w-1,xBox+winRad);
    //int yB=std::min(h-1,yBox+winRad);

    if(debugLevel>=2){
      mexPrintf("Processing box #%d, (xT,yT)=(%d,%d) (w,h)=(%d,%d)\n",iBox,
          xT,yT,boxW,boxH);
    }

    // ---- find (u_f,v_f) ---------
    double bestSSD_fg=DBL_MAX;
    int uf=0;int vf=0;
    double *ssdSurface_ptr=ssdSurface;
    for(int deltaX=-srchRad_fg;deltaX<=srchRad_fg;deltaX++){
      // box1 indexes pixel (u,v)
      // box2 indexes pixel (u+uf,v+vf)
      int tmp_xT_2=xT+deltaX;
      int xT_2=std::max(0,tmp_xT_2);
      int xOffset=xT_2-tmp_xT_2;
      int tmp_xB_2=tmp_xT_2+boxW;
      int wClip=boxW-xOffset-std::max(0,tmp_xB_2-w);
      int tmp_offset=xOffset*h;
      int tmp_idx2=xT_2*h;
      for(int deltaY=-srchRad_fg;deltaY<=srchRad_fg;deltaY++,ssdSurface_ptr++){
        int tmp_yT_2=yT+deltaY;
        int yT_2=std::max(0,tmp_yT_2);
        int yOffset=yT_2-tmp_yT_2;
        int tmp_yB_2=tmp_yT_2+boxH;
        int hClip=boxH-yOffset-std::max(0,tmp_yB_2-h);

        int idx1=idxTop+tmp_offset+yOffset;
        int idx2=tmp_idx2+yT_2;
        // box1 indexes pixel (u,v)
        // box2 indexes pixel (u+uf,v+vf)

        int ySkip=(h-hClip);
        bool *seg_ptr=curSeg+idx1; // for y1(u,v)
        int *cur_r=curFrame+idx1;  // for x1(u,v)
        int *cur_g=cur_r+N;
        int *cur_b=cur_g+N;
        int *nxt_r=nxtFrame+idx2;  // for x2(u+uf,v+vf)
        int *nxt_g=nxt_r+N;
        int *nxt_b=nxt_g+N;

        int ssdSum=0;
        int valid=0;
        for(int iX=0;iX<wClip;iX++){
          for(int iY=0;iY<hClip;iY++){
            if(*seg_ptr){
              int rDiff=*cur_r-*nxt_r;
              int gDiff=*cur_g-*nxt_g;
              int bDiff=*cur_b-*nxt_b;
              valid++;
              ssdSum+=rDiff*rDiff+gDiff*gDiff+bDiff*bDiff;
            }
            cur_r++;cur_g++;cur_b++;
            nxt_r++;nxt_g++;nxt_b++;
            seg_ptr++;
          }
          cur_r+=ySkip;cur_g+=ySkip;cur_b+=ySkip;
          nxt_r+=ySkip;nxt_g+=ySkip;nxt_b+=ySkip;
          seg_ptr+=ySkip;
        }

        if(valid>=minPixels_match){
          double avgSSD=((double)ssdSum)/(valid*ssdDivisor);
          *ssdSurface_ptr=avgSSD;
          if(avgSSD<bestSSD_fg){
            bestSSD_fg=avgSSD;uf=deltaX;vf=deltaY;
            if(debugLevel>=1){
              bestFG_ssd_ptr[iBox]=bestSSD_fg;
              validFG_ssd_ptr[iBox]=valid;
            }
          }
        }
        else{*ssdSurface_ptr=DBL_MAX;}
      }// end loop for searching over y displacement
    }// end loop for searching over x displacement

    if(debugLevel>=1){
      motionFG_ptr[2*iBox]=uf;
      motionFG_ptr[2*iBox+1]=vf;
      if(bestSSD_fg==DBL_MAX)
        mexPrintf("No fg match found for window idx: %d (0 based), perhaps number of fg pixels in this window is less than minPixels_match\n",iBox);
    }

    // --- the above loop found the best uf, vf, and also the ssd surface
    // use this ssd surface to compute the depth of the SSD curve around the optima
    double ssdDepth=DBL_MAX;
    {
      int xCentre=uf+srchRad_fg;int yCentre=vf+srchRad_fg;
      int xT=std::max(0,xCentre-ssdCurve_depthRad);
      int yT=std::max(0,yCentre-ssdCurve_depthRad);
      int xB=std::min(srchDia_fg-1,xCentre+ssdCurve_depthRad);
      int yB=std::min(srchDia_fg-1,yCentre+ssdCurve_depthRad);

      int iX,iY;
      // travel top most line of rectangle
      iY=yT;
      for(iX=xT;iX<=(xB-1);iX++){
        double tmpDepth=ssdSurface[iX*srchDia_fg+iY];
        if(tmpDepth<ssdDepth){ssdDepth=tmpDepth;}
      }
      // travel right most line of rectangle
      iX=xB;
      for(iY=yT;iY<=(yB-1);iY++){
        double tmpDepth=ssdSurface[iX*srchDia_fg+iY];
        if(tmpDepth<ssdDepth){ssdDepth=tmpDepth;}
      }
      // travel bottom most line of rectangle
      iY=yB;
      for(iX=xB;iX>=(xT+1);iX--){
        double tmpDepth=ssdSurface[iX*srchDia_fg+iY];
        if(tmpDepth<ssdDepth){ssdDepth=tmpDepth;}
      }
      // travel left most line of rectangle
      iX=xT;
      for(iY=yB;iY>=(yT+1);iY--){
        double tmpDepth=ssdSurface[iX*srchDia_fg+iY];
        if(tmpDepth<ssdDepth){ssdDepth=tmpDepth;}
      }

      ssdDepth=ssdDepth-bestSSD_fg;
      if(debugLevel>=1){
        ssdCurveFG_depths[iBox]=ssdDepth;
      }
    }

    // ---- found best uf,vf, now find ub,vb ----------------------
    double bestSSD_bg=DBL_MAX;
    int ub=0;int vb=0;
    for(int deltaX=-srchRad_bg;deltaX<=srchRad_bg;deltaX++){
      // box 1 is (u,v)
      // box 2 is (u+ub,v+vb) and box 3 is (u+ub-uf,v+vb-vf)
      int tmp_xT_2=xT+deltaX;
      int tmp_xT_3=xT+deltaX-uf;
      int xOffset_2=std::max(0,-tmp_xT_2);
      int xOffset_3=std::max(0,-tmp_xT_3);
      int xOffset=std::max(xOffset_2,xOffset_3);

      int xT_2=tmp_xT_2+xOffset;
      int xT_3=tmp_xT_3+xOffset;

      int tmp_xB_2=tmp_xT_2+boxW;
      int tmp_xB_3=tmp_xT_3+boxW;
      xOffset_2=std::max(0,tmp_xB_2-w);
      xOffset_3=std::max(0,tmp_xB_3-w);

      int wClip=boxW-xOffset-std::max(xOffset_2,xOffset_3);
      int tmp_offset=xOffset*h;
      int tmp_idx2=xT_2*h;
      int tmp_idx3=xT_3*h;
      for(int deltaY=-srchRad_bg;deltaY<=srchRad_bg;deltaY++){
        int tmp_yT_2=yT+deltaY;
        int tmp_yT_3=yT+deltaY-vf;
        int yOffset_2=std::max(0,-tmp_yT_2);
        int yOffset_3=std::max(0,-tmp_yT_3);
        int yOffset=std::max(yOffset_2,yOffset_3);

        int yT_2=tmp_yT_2+yOffset;
        int yT_3=tmp_yT_3+yOffset;

        int tmp_yB_2=tmp_yT_2+boxH;
        int tmp_yB_3=tmp_yT_3+boxH;
        yOffset_2=std::max(0,tmp_yB_2-h);
        yOffset_3=std::max(0,tmp_yB_3-h);

        int hClip=boxH-yOffset-std::max(yOffset_2,yOffset_3);

        // box 1 is (u,v)
        // box 2 is (u+ub,v+vb) and box 3 is (u+ub-uf,v+vb-vf)
        int idx1=idxTop+tmp_offset+yOffset;
        int idx2=tmp_idx2+yT_2;
        int idx3=tmp_idx3+yT_3;

        int ySkip=(h-hClip);
        bool *seg_ptr=curSeg+idx1; // for y1(u,v)
        bool *seg_ptr2=curSeg+idx3;// for y1(u+ub-uf,v+vb-vf)
        int *cur_r=curFrame+idx1;  // for x1(u,v)
        int *cur_g=cur_r+N;
        int *cur_b=cur_g+N;
        int *nxt_r=nxtFrame+idx2;  // for x2(u+ub,v+vb)
        int *nxt_g=nxt_r+N;
        int *nxt_b=nxt_g+N;

        int ssdSum=0;
        int valid=0;
        for(int iX=0;iX<wClip;iX++){
          for(int iY=0;iY<hClip;iY++){
            if(!(*seg_ptr || *seg_ptr2)){
              int rDiff=*cur_r-*nxt_r;
              int gDiff=*cur_g-*nxt_g;
              int bDiff=*cur_b-*nxt_b;
              valid++;
              ssdSum+=rDiff*rDiff+gDiff*gDiff+bDiff*bDiff;
            }
            cur_r++;cur_g++;cur_b++;
            nxt_r++;nxt_g++;nxt_b++;
            seg_ptr++;
            seg_ptr2++;
          }
          cur_r+=ySkip;cur_g+=ySkip;cur_b+=ySkip;
          nxt_r+=ySkip;nxt_g+=ySkip;nxt_b+=ySkip;
          seg_ptr+=ySkip;
          seg_ptr2+=ySkip;
        }

        if(valid>=minPixels_match){
          double avgSSD=((double)ssdSum)/(valid*ssdDivisor);
          if(avgSSD<bestSSD_bg){
            bestSSD_bg=avgSSD;ub=deltaX;vb=deltaY;
            if(debugLevel>=1){
              bestBG_ssd_ptr[iBox]=bestSSD_bg;
              validBG_ssd_ptr[iBox]=valid;
            }
          } 
        }

      } // end loop for search over y displacement
    } // end loop for searching over x displacement

    if(debugLevel>=1){
      motionBG_ptr[2*iBox]=ub;
      motionBG_ptr[2*iBox+1]=vb;
      if(bestSSD_bg==DBL_MAX)
        mexPrintf("No bg match found for window idx: %d (0 based), perhaps number of bg pixels in this window is less than minPixels_match\n",iBox);
    }
    // ----- have estimated uf,vf and ub,vb, now construct unaries for y2 ------

    // ---- make the shape penalty unary ---------------------------------------
    {
      // box 1 is (u,v)
      // box 2 is (u+uf,v+vf)
      int tmp_xT_2=xT+uf;
      int xT_2=std::max(0,tmp_xT_2);
      int xOffset=xT_2-tmp_xT_2;
      int tmp_xB_2=tmp_xT_2+boxW;
      int wClip=boxW-xOffset-std::max(0,tmp_xB_2-w);
      int tmp_offset=xOffset*h;
      int tmp_idx2=xT_2*h;

      int tmp_yT_2=yT+vf;
      int yT_2=std::max(0,tmp_yT_2);
      int yOffset=yT_2-tmp_yT_2;
      int tmp_yB_2=tmp_yT_2+boxH;
      int hClip=boxH-yOffset-std::max(0,tmp_yB_2-h);

      // box 1 is (u,v)
      // box 2 is (u+uf,v+vf)
      int idx1=idxTop+tmp_offset+yOffset;
      int idx2=tmp_idx2+yT_2;
      int ySkip=(h-hClip);

      bool *seg_ptr=curSeg+idx1; // y1(u,v)
      double *fgUnary_ptr=tmpUnary+idx2; // y2(u+uf,v+vf)
      double *bgUnary_ptr=fgUnary_ptr+N;// y2(u+uf,v+vf)
      for(int iX=0;iX<wClip;iX++){
        for(int iY=0;iY<hClip;iY++){
          if(*seg_ptr){
            *bgUnary_ptr=gcGamma_s;
            *fgUnary_ptr=0;
          }
          else{
            *fgUnary_ptr=gcGamma_s;
            *bgUnary_ptr=0;
          }
          seg_ptr++;
          fgUnary_ptr++;bgUnary_ptr++;
        }
        seg_ptr+=ySkip;
        fgUnary_ptr+=ySkip;bgUnary_ptr+=ySkip;
      }

      // Store the shape unaries separately also if debugLeve>=1
      if(debugLevel>=1){
        bool *seg_ptr=curSeg+idx1; // y1(u,v)
        double *fgUnary_ptr=shapeUnaries+idx2; // y2(u+uf,v+vf)
        double *bgUnary_ptr=fgUnary_ptr+N;// y2(u+uf,v+vf)
        for(int iX=0;iX<wClip;iX++){
          for(int iY=0;iY<hClip;iY++){
            if(*seg_ptr){
              *bgUnary_ptr+=1;
            }
            else{
              *fgUnary_ptr+=1;
            }
            seg_ptr++;
            fgUnary_ptr++;bgUnary_ptr++;
          }
          seg_ptr+=ySkip;
          fgUnary_ptr+=ySkip;bgUnary_ptr+=ySkip;
        } 
      }

    }

    // --------- shape penalty unaries added, now add SSD unaries ------
    {
      // box 1 is (u,v)
      // box 2 is (u-uf,v-vf)
      // box 3 is (u-ub,v-vb)

      // Note that box 1 goes over the original local window offset by 
      // (+uf,+vf), so that's why the extra code below for finding box 1
      // This is opposed to all the cases above, where box1 was the original
      // local window
      int tmp_xT_1=xT+uf;
      int xT_1=std::max(0,tmp_xT_1);
      int xB_1=std::min(w,tmp_xT_1+boxW);
      int boxW_1=xB_1-xT_1;

      int tmp_yT_1=yT+vf;
      int yT_1=std::max(0,tmp_yT_1);
      int yB_1=std::min(h,tmp_yT_1+boxH);
      int boxH_1=yB_1-yT_1;

      int xT_2=xT_1-uf;
      int yT_2=yT_1-vf;

      int tmp_xT_3=xT_1-ub; 
      int xT_3=std::max(0,tmp_xT_3);
      int xOffset=xT_3-tmp_xT_3;
      int tmp_xB_3=tmp_xT_3+boxW_1;
      int wClip=boxW_1-xOffset-std::max(0,tmp_xB_3-w);

      int tmp_yT_3=yT_1-vb;
      int yT_3=std::max(0,tmp_yT_3);
      int yOffset=yT_3-tmp_yT_3;
      int tmp_yB_3=tmp_yT_3+boxH_1;
      int hClip=boxH_1-yOffset-std::max(0,tmp_yB_3-h);

      // box 1 is (u,v)
      // box 2 is (u-uf,v-vf)
      // box 3 is (u-ub,v-vb)
      int idx1=(xT_1+xOffset)*h+yT_1+yOffset;
      int idx2=(xT_2+xOffset)*h+yT_2+yOffset;
      int idx3=xT_3*h+yT_3;

      int ySkip=(h-hClip);
      double *fgUnary_ptr=tmpUnary+idx1;   // y2(u,v)
      double *bgUnary_ptr=fgUnary_ptr+N;  // y2(u,v)
      double *fgUnary_ssd_ptr,*bgUnary_ssd_ptr;
      if(debugLevel>=1){
        fgUnary_ssd_ptr=ssdUnaries+idx1;
        bgUnary_ssd_ptr=fgUnary_ssd_ptr+N;
      }
      int *r_1=nxtFrame+idx1; // x2(u,v)
      int *g_1=r_1+N;
      int *b_1=g_1+N;
      int *r_2=curFrame+idx2; // x1(u-uf,v-vf)
      int *g_2=r_2+N;
      int *b_2=g_2+N;
      bool *seg2_ptr=curSeg+idx2; //y1(u-uf,v-vf)
      int *r_3=curFrame+idx3;     //x1(u-ub,v-vb)
      int *g_3=r_3+N;
      int *b_3=g_3+N;
      bool *seg3_ptr=curSeg+idx3; //y1(u-ub,v-vb)

      for(int iX=0;iX<wClip;iX++){
        for(int iY=0;iY<hClip;iY++){
          int r_diff,g_diff,b_diff;
          double ssd;
          r_diff=*r_1-*r_2;
          g_diff=*g_1-*g_2;
          b_diff=*b_1-*b_2;
          ssd=(double)(r_diff*r_diff+g_diff*g_diff+b_diff*b_diff);
          ssd=ssd/(255*255*3);
          (*fgUnary_ptr)+=gcGamma_ssd*ssd;
          if(debugLevel>=1){(*fgUnary_ssd_ptr)+=ssd;}
          if( *seg2_ptr || !(*seg3_ptr)){
            r_diff=*r_1-*r_3;
            g_diff=*g_1-*g_3;
            b_diff=*b_1-*b_3;
            ssd=(double)(r_diff*r_diff+g_diff*g_diff+b_diff*b_diff);
            ssd=ssd/(255*255*3);
            (*bgUnary_ptr)+=gcGamma_ssd*ssd;
            if(debugLevel>=1){(*bgUnary_ssd_ptr)+=ssd;}
          }
          fgUnary_ptr++;bgUnary_ptr++;
          fgUnary_ssd_ptr++;bgUnary_ssd_ptr++;
          r_1++;g_1++;b_1++;
          r_2++;g_2++;b_2++;
          seg2_ptr++;
          r_3++;g_3++;b_3++;
          seg3_ptr++;
        }

        fgUnary_ptr+=ySkip;bgUnary_ptr+=ySkip;
        fgUnary_ssd_ptr+=ySkip;bgUnary_ssd_ptr+=ySkip;
        r_1+=ySkip;g_1+=ySkip;b_1+=ySkip;
        r_2+=ySkip;g_2+=ySkip;b_2+=ySkip;
        seg2_ptr+=ySkip;
        r_3+=ySkip;g_3+=ySkip;b_3+=ySkip;
        seg3_ptr+=ySkip;
      }
    }
    // ---- SSD unaries computed, also motion unaries computed ----------------

    // --- Now compute the confidence values using distance tranf ----------------
    {
      double ssdAbsolute_conf,ssdSurface_conf; 
      if(ssdDepth<ssdCurve_noConfD)
        ssdSurface_conf=0;
      else if(ssdDepth>=ssdCurve_fullConfD)
        ssdSurface_conf=1;
      else{
        double tmp=ssdDepth-ssdCurve_noConfD;
        double width=ssdCurve_fullConfD-ssdCurve_noConfD;
        ssdSurface_conf=tmp/width;
      }

      if(bestSSD_fg<ssd_fullConf)
        ssdAbsolute_conf=1;
      else if(bestSSD_fg>=ssd_noConf)
        ssdAbsolute_conf=0;
      else{
        double tmp=ssd_noConf-bestSSD_fg;
        double width=ssd_noConf-ssd_fullConf;
        ssdAbsolute_conf=tmp/width;
      }

      double conf=ssdAbsolute_conf*ssdSurface_conf;
      motionConf[iBox]=conf;

      // --- extract the propagated mask ---------
      int tmp_xT_2=xT+uf;
      int xT_2=std::max(0,tmp_xT_2);
      int xOffset=xT_2-tmp_xT_2;
      int tmp_xB_2=tmp_xT_2+boxW;
      int wClip=boxW-xOffset-std::max(0,tmp_xB_2-w);
      int tmp_offset=xOffset*h;
      int tmp_idx2=xT_2*h;

      int tmp_yT_2=yT+vf;
      int yT_2=std::max(0,tmp_yT_2);
      int yOffset=yT_2-tmp_yT_2;
      int tmp_yB_2=tmp_yT_2+boxH;
      int hClip=boxH-yOffset-std::max(0,tmp_yB_2-h);

      // box 1 is (u,v)
      // box 2 is (u+uf,v+vf)
      int idx1=idxTop+tmp_offset+yOffset;
      int idx2=tmp_idx2+yT_2;
      int ySkip=(h-hClip);

      bool *seg_ptr=curSeg+idx1; // y1(u,v)
      mxArray *mx_tmpMask=mxCreateNumericMatrix(hClip,wClip,mxLOGICAL_CLASS,mxREAL);
      bool *tmpMask_ptr=(bool*)mxGetData(mx_tmpMask);
      for(int iX=0;iX<wClip;iX++){
        for(int iY=0;iY<hClip;iY++){
          *tmpMask_ptr=*seg_ptr;
          seg_ptr++;tmpMask_ptr++;
        }
        seg_ptr+=ySkip;
      }
      // ---- propagated mask is now in mx_tmpMask, call distance transform on it --
      double sigma_s=std::max((double)0,(double)winRad*(1-conf)); // convert confidence into 
                              // bandwidth around seg boundary in snapCut style
      mxArray *lhs[1];mxArray *rhs[2];
      mxArray *mx_sigma_s=mxCreateDoubleMatrix(1,1,mxREAL);
      *mxGetPr(mx_sigma_s)=sigma_s+EPS;
      rhs[0]=mx_tmpMask;rhs[1]=mx_sigma_s;

      if(sigma_s<=EPS){
        lhs[0]=mxCreateDoubleMatrix(hClip,wClip,mxREAL); 
        double *tmpConf=mxGetPr(lhs[0]);
        for(int i=0;i<hClip*wClip;i++,tmpConf++){*tmpConf=1;}
      }
      else{
        mexCallMATLAB(1,lhs,2,rhs,"motionCut2.helpers.getShapeConfidence");
      }
      // --- free all the temporary memories -----
      mxDestroyArray(mx_tmpMask);
      mxDestroyArray(mx_sigma_s);

      mx_tmpConf=lhs[0];
    }

    // --- Now compute color unaries ------------------------------------------
    {
      mxArray *mx_muFg=mxGetField(prhs[6],iBox,"mu");
      mxArray *mx_sigmaFg=mxGetField(prhs[6],iBox,"invSigma");
      mxArray *mx_piFg=mxGetField(prhs[6],iBox,"pi");
      mxArray *mx_detFg=mxGetField(prhs[6],iBox,"denom");

      mxArray *mx_muBg=mxGetField(prhs[7],iBox,"mu");
      mxArray *mx_sigmaBg=mxGetField(prhs[7],iBox,"invSigma");
      mxArray *mx_piBg=mxGetField(prhs[7],iBox,"pi");
      mxArray *mx_detBg=mxGetField(prhs[7],iBox,"denom");

      int nMix_fg=mxGetNumberOfElements(mx_piFg);
      int nMix_bg=mxGetNumberOfElements(mx_piBg);

      double *muFg=mxGetPr(mx_muFg);
      double *sigmaFg=mxGetPr(mx_sigmaFg);
      double *piFg=mxGetPr(mx_piFg);
      double *detFg=mxGetPr(mx_detFg);

      double *muBg=mxGetPr(mx_muBg);
      double *sigmaBg=mxGetPr(mx_sigmaBg);
      double *piBg=mxGetPr(mx_piBg);
      double *detBg=mxGetPr(mx_detBg);

      // box 1 is (u,v)
      // box 2 is (u+uf,v+vf)
      int tmp_xT_2=xT+uf;
      int xT_2=std::max(0,tmp_xT_2);
      int xOffset=xT_2-tmp_xT_2;
      int tmp_xB_2=tmp_xT_2+boxW;
      int wClip=boxW-xOffset-std::max(0,tmp_xB_2-w);
      int tmp_offset=xOffset*h;
      int tmp_idx2=xT_2*h;

      int tmp_yT_2=yT+vf;
      int yT_2=std::max(0,tmp_yT_2);
      int yOffset=yT_2-tmp_yT_2;
      int tmp_yB_2=tmp_yT_2+boxH;
      int hClip=boxH-yOffset-std::max(0,tmp_yB_2-h);

      // box 1 is (u,v)
      // box 2 is (u+uf,v+vf)
      int idx1=idxTop+tmp_offset+yOffset;
      int idx2=tmp_idx2+yT_2;
      int ySkip=(h-hClip);
      int *ptr_r=nxtFrame+idx2;
      int *ptr_g=ptr_r+N;
      int *ptr_b=ptr_g+N;
      bool *unaryMask_ptr=unaryMask+idx2;
      double *fgUnary_ptr=unaries+idx2; // pointer on final unary
      double *bgUnary_ptr=fgUnary_ptr+N;
      double *fgUnaryTmp_ptr=tmpUnary+idx2; // pointer on motion unary
      double *bgUnaryTmp_ptr=fgUnaryTmp_ptr+N;
      int *winCount_ptr=winCount+idx2; // pointer on window count for pixels
      double *conf_ptr=mxGetPr(mx_tmpConf); // pointer on confidence value of motion unary

      double *fgUnaryMotion_ptr,*bgUnaryMotion_ptr; // debuging output on motion unaries
      double *fgUnaryColor_ptr,*bgUnaryColor_ptr; // debugging output on color unaries

      if(debugLevel>=1){
        fgUnaryMotion_ptr=motionUnaries+idx2;
        bgUnaryMotion_ptr=fgUnaryMotion_ptr+N;
        fgUnaryColor_ptr=colorUnaries+idx2;
        bgUnaryColor_ptr=fgUnaryColor_ptr+N;
      }

      for(int iX=0;iX<wClip;iX++){
        for(int iY=0;iY<hClip;iY++){
          double ftr[3];
          ftr[0]=((double)*ptr_r)/255;
          ftr[1]=((double)*ptr_g)/255;
          ftr[2]=((double)*ptr_b)/255;
          double fgLikeli=gmmLikeli(ftr,nMix_fg,muFg,sigmaFg,piFg,detFg);
          double bgLikeli=gmmLikeli(ftr,nMix_bg,muBg,sigmaBg,piBg,detBg);
          fgLikeli=likeliGamma*uniValue+(1-likeliGamma)*fgLikeli;
          bgLikeli=likeliGamma*uniValue+(1-likeliGamma)*bgLikeli;

          double likeliSum=(fgLikeli+bgLikeli);
          double posterior;
          if(likeliSum>0) posterior=fgLikeli/likeliSum;
          else posterior=0.5;

          double conf=*conf_ptr;
          (*fgUnary_ptr)+=(conf*(*fgUnaryTmp_ptr)+(1-conf)*(1-posterior)*gcGamma_c);
          (*bgUnary_ptr)+=(conf*(*bgUnaryTmp_ptr)+(1-conf)*(posterior)*gcGamma_c);
          (*winCount_ptr)+=1;
          *unaryMask_ptr=true;
          if(debugLevel>=1){
            (*fgUnaryColor_ptr)+=(1-posterior);
            (*bgUnaryColor_ptr)+=posterior;
            (*fgUnaryMotion_ptr)+=(*fgUnaryTmp_ptr);
            (*bgUnaryMotion_ptr)+=(*bgUnaryTmp_ptr);
            fgUnaryColor_ptr++;bgUnaryColor_ptr++;
            fgUnaryMotion_ptr++;bgUnaryMotion_ptr++;
          }

          unaryMask_ptr++;
          fgUnary_ptr++;bgUnary_ptr++;
          fgUnaryTmp_ptr++;bgUnaryTmp_ptr++;
          winCount_ptr++;conf_ptr++;
          ptr_r++;ptr_g++;ptr_b++;
        }
        if(debugLevel>=1){
          fgUnaryColor_ptr+=ySkip;bgUnaryColor_ptr+=ySkip;
          fgUnaryMotion_ptr+=ySkip;bgUnaryMotion_ptr+=ySkip;
        }
        unaryMask_ptr+=ySkip;
        fgUnary_ptr+=ySkip;bgUnary_ptr+=ySkip;
        fgUnaryTmp_ptr+=ySkip;bgUnaryTmp_ptr+=ySkip;
        winCount_ptr+=ySkip;//conf_ptr+=ySkip; you dont need to increment conf!
        ptr_r+=ySkip;ptr_g+=ySkip;ptr_b+=ySkip;
      }

      mxDestroyArray(mx_tmpConf);

    }

    if(debugLevel>=2){
      mexPrintf("Box #%d processed, (uf,vf)=(%d,%d) (ub,vb)=(%d,%d)\n",
          iBox,uf,vf,ub,vb);
      mexPrintf("Avg fg SSD = %f (over %d pixels), SSD Depth = %f, Motion conf = %f , avg bg SSD = %f (over %d pixels),\n",
          bestFG_ssd_ptr[iBox],ssdCurveFG_depths[iBox],
          motionConf[iBox],validFG_ssd_ptr[iBox],
          bestBG_ssd_ptr[iBox],validBG_ssd_ptr[iBox]);
    }

  }

  // --- Now normalize the unaries, by the number of windows that cover each pixel
  {
    int *winCount_ptr=winCount;
    double *fgUnary_ptr=unaries;
    double *bgUnary_ptr=fgUnary_ptr+N;
    double *fgUnaryMotion_ptr,*bgUnaryMotion_ptr;
    double *fgUnaryColor_ptr,*bgUnaryColor_ptr;
    double *fgUnaryShape_ptr,*bgUnaryShape_ptr;
    double *fgUnarySSD_ptr,*bgUnarySSD_ptr;

    if(debugLevel>=1){
      fgUnaryMotion_ptr=motionUnaries;
      bgUnaryMotion_ptr=fgUnaryMotion_ptr+N;
      fgUnaryColor_ptr=colorUnaries;
      bgUnaryColor_ptr=fgUnaryColor_ptr+N;
      fgUnaryShape_ptr=shapeUnaries;
      bgUnaryShape_ptr=fgUnaryShape_ptr+N;
      fgUnarySSD_ptr=ssdUnaries;
      bgUnarySSD_ptr=fgUnarySSD_ptr+N;
    }

    for(int i=0;i<N;i++){
      double count=(double)(*winCount_ptr);
      if(count>0){
        (*fgUnary_ptr)/=count;
        (*bgUnary_ptr)/=count;
        if(debugLevel>=1){
          (*fgUnaryMotion_ptr)/=count;
          (*bgUnaryMotion_ptr)/=count;
          (*fgUnaryColor_ptr)/=count;
          (*bgUnaryColor_ptr)/=count;
          (*fgUnarySSD_ptr)/=count;
          (*bgUnarySSD_ptr)/=count;
          (*fgUnaryShape_ptr)/=count;
          (*bgUnaryShape_ptr)/=count;
        }
      }
      fgUnary_ptr++;bgUnary_ptr++;
      fgUnaryMotion_ptr++;bgUnaryMotion_ptr++;
      fgUnaryColor_ptr++;bgUnaryColor_ptr++;
      fgUnaryShape_ptr++;bgUnaryShape_ptr++;
      fgUnarySSD_ptr++;bgUnarySSD_ptr++;
      winCount_ptr++;
    }
  }

  // ----- To free -----------------------------------------------
  mxFree(winCount);
  mxFree(ssdSurface);
  mxFree(tmpUnary);
}

void printStruct_fieldNames(const mxArray* st){
  int numFields=mxGetNumberOfFields(st);
  mexPrintf("%d fields in structure:\n",numFields);

  for(int i=0;i<numFields;i++){
    mexPrintf("%02d: %s\n",i+1,mxGetFieldNameByNumber(st,i));
  }
}

inline double gmmLikeli(const double* ftr,int nMix,const double* mu,const double* invSigma,
    const double* pi, const double* multiplier){
  
  double p=0;

  for(int i=0;i<nMix;i++){
    double dX[3]={ftr[0]-mu[0],ftr[1]-mu[1],ftr[2]-mu[2]};
    double num=0;
    for(int x=0;x<3;x++){
      double tmp=0;
      for(int y=0;y<3;y++,invSigma++)
        tmp+=(dX[y]*(*invSigma));

      num+=tmp*dX[x];
    }
    p+=pi[i]*multiplier[i]*exp(num*(-0.5));
    mu+=3;
  }

  return p;
}
