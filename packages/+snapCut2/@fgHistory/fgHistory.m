classdef fgHistory < handle
% Class to maintain and propagate foreground representation
  properties (SetAccess=private, GetAccess=public)
    fgImg % h x w x D img
    mask_known % h x w, logical
    debugLevel
    frameNum
    
    flow_maxBox % resize image to fit into maxBox before computing flow
    flow_xRad % flow range in x [-xRad,+xRad]
    flow_yRad % flow range in y [-yRad,+yRad]
  end

  methods
    function obj=fgHistory(debugLevel,initImg,mask_known,maxBox,xRad,yRad)
      obj.debugLevel=debugLevel;
      obj.fgImg=initImg;
      obj.mask_known=mask_known;
      obj.frameNum=1;
      obj.flow_maxBox=maxBox;
      obj.flow_xRad=xRad;
      obj.flow_yRad=yRad;
    end
    warpFG=propagate(obj,nxtFrame,motionEnable)
    update(obj,nxtSeg,nxtFrame,computedWarp);
  end

  methods (Access=private)
  end

end % of classdef
