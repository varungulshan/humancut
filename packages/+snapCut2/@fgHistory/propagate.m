function warpFG=propagate(obj,nxtFrame,motionEnable)

warpFG.img=obj.fgImg;
warpFG.mask_known=obj.mask_known;
obj.frameNum=obj.frameNum+1;

if(motionEnable)
  opts.maxBox=obj.flow_maxBox;
  opts.xRad=obj.flow_xRad;
  opts.yRad=obj.flow_yRad;
  [warpFG.flowImg,warpFG.occlMask]=snapCut2.optFlow.getFlow_ogale(obj.fgImg,nxtFrame,...
                obj.mask_known,opts);
else
  [h w nCh]=size(obj.fgImg);
  warpFG.flowImg=zeros([h w 2]);
  warpFG.occlMask=logical(zeros([h w]));
end
