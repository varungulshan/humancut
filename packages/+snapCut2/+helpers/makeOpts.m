function opts=makeOpts(optsString)

switch(optsString)
   case 'try_rad30'
    opts=snapCut2.segEngine_opts();
    opts.gcGamma=20;
    opts.gcScale=1000;
    opts.gcIsing=0.1;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.winRad=30;
    opts.sigma_s=11;
    opts.winWts_eps=0.1;

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.gmmUni_value=1; % assuming features in [0,1]
    opts.gmmLikeli_gamma=0.05;

    opts.flow_maxBox=200;
    opts.flow_xRad=7;
    opts.flow_yRad=7;

case 'rad20_noMotion'
    opts=snapCut2.segEngine_opts();
    opts.gcGamma=20;
    opts.gcScale=1000;
    opts.gcIsing=0.1;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.winRad=20;
    opts.winWts_eps=0.1;

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.gmmUni_value=1; % assuming features in [0,1]
    opts.gmmLikeli_gamma=0.05;

    opts.motionEnable=false;


case 'rad40_noMotion'
    opts=snapCut2.segEngine_opts();
    opts.gcGamma=20;
    opts.gcScale=1000;
    opts.gcIsing=0.1;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.winRad=40;
    opts.winWts_eps=0.1;

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.gmmUni_value=1; % assuming features in [0,1]
    opts.gmmLikeli_gamma=0.05;

    opts.motionEnable=false;


case 'rad30_noMotion'
    opts=snapCut2.segEngine_opts();
    opts.gcGamma=20;
    opts.gcScale=1000;
    opts.gcIsing=0.1;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.winRad=30;
    opts.winWts_eps=0.1;

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.gmmUni_value=1; % assuming features in [0,1]
    opts.gmmLikeli_gamma=0.05;

    opts.motionEnable=false;

case 'rad30_sigma0'
    opts=snapCut2.segEngine_opts();
    opts.gcGamma=20;
    opts.gcScale=1000;
    opts.gcIsing=0.1;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.winRad=30;
    opts.sigma_s=0.01;
    opts.winWts_eps=0.1;

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.gmmUni_value=1; % assuming features in [0,1]
    opts.gmmLikeli_gamma=0.05;

    opts.flow_maxBox=200;
    opts.flow_xRad=7;
    opts.flow_yRad=7;
   
case 'rad30_sigma5'
    opts=snapCut2.segEngine_opts();
    opts.gcGamma=20;
    opts.gcScale=1000;
    opts.gcIsing=0.1;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.winRad=30;
    opts.sigma_s=5;
    opts.winWts_eps=0.1;

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.gmmUni_value=1; % assuming features in [0,1]
    opts.gmmLikeli_gamma=0.05;

    opts.flow_maxBox=200;
    opts.flow_xRad=7;
    opts.flow_yRad=7;

case 'rad40_sigma11'
    opts=snapCut2.segEngine_opts();
    opts.gcGamma=20;
    opts.gcScale=1000;
    opts.gcIsing=0.1;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.winRad=40;
    opts.sigma_s=11;
    opts.winWts_eps=0.1;

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.gmmUni_value=1; % assuming features in [0,1]
    opts.gmmLikeli_gamma=0.05;

    opts.flow_maxBox=200;
    opts.flow_xRad=7;
    opts.flow_yRad=7;
    
case 'rad30_sigma11'
    opts=snapCut2.segEngine_opts();
    opts.gcGamma=20;
    opts.gcScale=1000;
    opts.gcIsing=0.1;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.winRad=30;
    opts.sigma_s=11;
    opts.winWts_eps=0.1;

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.gmmUni_value=1; % assuming features in [0,1]
    opts.gmmLikeli_gamma=0.05;

    opts.flow_maxBox=200;
    opts.flow_xRad=7;
    opts.flow_yRad=7;
   
   case 'rad30_sigmaInf'
    opts=snapCut2.segEngine_opts();
    opts.gcGamma=20;
    opts.gcScale=1000;
    opts.gcIsing=0.1;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.winRad=30;
    opts.sigma_s=inf;
    opts.winWts_eps=0.1;

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.gmmUni_value=1; % assuming features in [0,1]
    opts.gmmLikeli_gamma=0.05;

    opts.flow_maxBox=200;
    opts.flow_xRad=7;
    opts.flow_yRad=7;
   otherwise
    error('Invalid options string %s\n',optsString);
end
