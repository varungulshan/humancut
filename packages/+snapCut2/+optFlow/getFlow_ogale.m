function [flowImg,occl_mask]=getFlow_ogale(curImg,nxtImg,mask,flowOpts)

maxBox=flowOpts.maxBox;
[h w nCh]=size(curImg);
hScale=maxBox/h;
wScale=maxBox/w;
scale=min(hScale,wScale);
scale=min(scale,1);
invScale=1/scale;

i1=imresize(curImg,scale,'method','bilinear');
i2=imresize(nxtImg,scale,'method','bilinear');

minShiftX=-flowOpts.xRad;
maxShiftX=flowOpts.xRad;
minShiftY=-flowOpts.yRad;
maxShiftY=flowOpts.yRad;

[flowFwd_x,flowFwd_y,occL_fwd,flowBwd_x,flowBwd_y,occl_bwd]=...
        OvFlowMatlab(i1,i2,minShiftX,maxShiftX,minShiftY,maxShiftY);
      
flowFwd_x(occL_fwd==1)=0;
flowFwd_y(occL_fwd==1)=0; % The occluded areas get random values otherwise!

flowImg=zeros([h w 2]);
flowImg(:,:,1)=imresize(invScale*flowFwd_x,[h w],'method','bilinear');
flowImg(:,:,2)=imresize(invScale*flowFwd_y,[h w],'method','bilinear');

occl_mask=imresize(occL_fwd==1,[h w],'method','nearest');
