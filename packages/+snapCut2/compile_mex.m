function compile_mex()

cwd=miscFns.extractDirPath(mfilename('fullpath'));

mexCmds{1}=sprintf('mex -O %s+cpp/mex_getWindowPts2.cpp -outdir %s+cpp/',cwd,cwd);
mexCmds{end+1}=sprintf('mex -O %s+cpp/mex_getWindowPts.cpp -outdir %s+cpp/',cwd,cwd);
mexCmds{end+1}=sprintf('mex -O %s+gmm/vag_norm_fast.cpp -outdir %s+gmm/',cwd,cwd);
mexCmds{end+1}=sprintf('mex -O -I%s+cpp/graphCut/ %s+cpp/graphCut/mex_gcBand.cpp %s+cpp/graphCut/graph.cpp -outdir %s+cpp/',cwd,cwd,cwd,cwd);

for i=1:length(mexCmds)
  fprintf('Executing %s\n',mexCmds{i});
  eval(mexCmds{i});
end
