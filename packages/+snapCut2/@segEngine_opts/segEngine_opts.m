classdef segEngine_opts 
  properties (SetAccess=public, GetAccess=public)
    gcGamma  % gamma of graph cuts
    gcScale  % scaling to convert to integers
    gcIsing  % ising model strength
    gcNbrType % 'nbr4' or 'nbr8'
    gcSigma_c % 'auto' or a numeric value

    winRad   % local window radius, usually 15-40
    winWts_eps % see eq (6) of snapCut paper
    sigma_s % sigma (in pixel unit), denotes the size of the confidence
            % region around the propagated mask

    gmmNmix_fg % number of gmm mixtures for fg, usually 2-3
    gmmNmix_bg % number of gmm mixtures for bg, usually 2-3
    gmmUni_value % uniform likelihood mixing, see constructor for default
    gmmLikeli_gamma % strenght of uniform likliehood mixing

    flow_maxBox % resize image to fit into maxBox before computing flow
    flow_xRad % flow range in x [-xRad,+xRad]
    flow_yRad % flow range in y [-yRad,+yRad]

    motionEnable % to enable tracking of windows, and shape propagation
  end

  methods
    function obj=segEngine_opts()
      % Set the default options
      obj.gcGamma=20;
      obj.gcScale=1000;
      obj.gcIsing=0.1;
      obj.gcNbrType='nbr8';
      obj.gcSigma_c='auto';

      obj.winRad=25;
      obj.sigma_s=5;
      obj.winWts_eps=0.1;

      obj.gmmNmix_fg=3;
      obj.gmmNmix_bg=3;
      obj.gmmUni_value=1; % assuming features in [0,1]
      obj.gmmLikeli_gamma=0.05;
  
      obj.flow_maxBox=200;
      obj.flow_xRad=7;
      obj.flow_yRad=7;

      obj.motionEnable=true;
    end
  end

end % of classdef
