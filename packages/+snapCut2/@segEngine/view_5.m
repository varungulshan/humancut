function img=view_5(obj)
% Shows the shape likelihood at test time (i.e current frame)
% KNOWN FLAW: as the segEngine only stores index of the tracked windows, it can cause
% a window that goes outside the image to roll over and display in the wrong place
% It only affects the visualizations, and not the algorithm itself.

if(obj.debugLevel>0)
  if(obj.frameNum==1),
    img=zeros([obj.h obj.w 3]);
    msg=sprintf('Frame_1_does_not_have_shape_likelihood',obj.frameNum);
    img=miscFns.rendertext(img,msg,[1 1 1],'ovr'); 
  else
    prevSeg=obj.seg(:,:,obj.frameNum-1);
    img=getLhood_image(obj,prevSeg);
  end
else
  img=zeros([obj.h obj.w 3]);
  img=miscFns.rendertext(img,'No_view_5_at_debugLevel_0',[1 1 1],'ovr'); 
end

function postImg=getLhood_image(obj,prevSeg)

import snapCut2.*;

fgMask=(prevSeg==255);
winRad=obj.opts.winRad;
localWindows_idx=obj.localWindows_idx;

img=obj.vH.curFrame;
mask_likelihood=zeros(size(fgMask),'uint8');
mask_likelihood(fgMask)=255;
[postImg,maskPosterior]=...
      localClassifiers(localWindows_idx,img,winRad,...
      obj.opts,mask_likelihood,fgMask,obj.trackedWindows_idx,...
      obj.flow_occlMask);

postImg(maskPosterior==255)=1;
postImg(maskPosterior==0)=0;

function [posteriors,maskPosterior]=...
    localClassifiers(winIdx,img,winRad,opts,maskPosterior,prevSeg,...
    trackedWindows_idx,occlMask)

import snapCut2.*

[h w nCh]=size(img);
img=im2double(img);

posteriors=zeros([h w]);
weights=zeros([h w]);

spWeights=getWindow_wts(winRad,opts.winWts_eps);
winSize=2*winRad+1;

for i =1:length(winIdx)
  cur_winIdx=winIdx(i);
  [winY,winX]=ind2sub([h w],cur_winIdx);

  xMin=max(1,winX-winRad);xL_pad1=(xMin-(winX-winRad));
  xMax=min(w,winX+winRad);
  yMin=max(1,winY-winRad);yT_pad1=(yMin-(winY-winRad));
  yMax=min(h,winY+winRad);

  xRng=[xMin:xMax];yRng=[yMin:yMax];
  win1_h=length(yRng);
  win1_w=length(xRng);

  fgMask=prevSeg(yRng,xRng);
  trackedMask=~occlMask(yRng,xRng);

  trackedIdx=trackedWindows_idx(i);
  [winY,winX]=ind2sub([h w],trackedIdx);

  xMin=max(1,winX-winRad);xL_pad2=(xMin-(winX-winRad));
  xMax=min(w,winX+winRad);xR_pad2=(winX+winRad-xMax);
  yMin=max(1,winY-winRad);yT_pad2=(yMin-(winY-winRad));
  yMax=min(h,winY+winRad);yB_pad2=(winY+winRad-yMax);

  xRng=[xMin:xMax];yRng=[yMin:yMax];
  win2_h=length(yRng);
  win2_w=length(xRng);

  fgMask_prop=0.5*ones(win2_h,win2_w); 
  delta_padY=yT_pad1-yT_pad2;
  yRng_mask1=[max(1,1-delta_padY):min(win1_h,win2_h-delta_padY)];
  yRng_mask2=yRng_mask1+delta_padY;
  delta_padX=xL_pad1-xL_pad2;
  xRng_mask1=[max(1,1-delta_padX):min(win1_w,win2_w-delta_padX)];
  xRng_mask2=xRng_mask1+delta_padX;

  fgMask=double(fgMask);fgMask(~trackedMask)=0.5;
  fgMask_prop(yRng_mask2,xRng_mask2)=fgMask(yRng_mask1,xRng_mask1);

  localShape=fgMask_prop;
  localWts=spWeights( (1+yT_pad2):(winSize-yB_pad2), (1+xL_pad2):(winSize-xR_pad2) );
  posteriors(yRng,xRng)=posteriors(yRng,xRng)+localWts.*localShape;
  weights(yRng,xRng)=weights(yRng,xRng)+localWts;
  maskPosterior(yRng,xRng)=128;
end

mask=(maskPosterior==128);
posteriors(mask)=min(1,posteriors(mask)./weights(mask)); % doing the min with 1, for numerical
posteriors(~mask)=0.5;

function wts=getWindow_wts(rad,epsilon)
  dia=2*rad+1;
  wts=zeros(dia,dia);
  [X,Y]=meshgrid([-rad:rad],[-rad:rad]);
  d=sqrt(X.*X+Y.*Y);
  wts=1./(d+epsilon);
