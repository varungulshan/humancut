classdef segEngine < videoSegmenter
  properties (SetAccess=private, GetAccess=public)
    debugLevel
    gtFile % gt for first frame
    vH
    gtSeg_firstFrame
    h
    w
    nCh
    nFrames
    state % 'initialized','pped','segStarted'
    seg % uint8, seg variable for the entire volume
    frameNum
    framesSegmented
    video % only if debugLevel > 9
    beta
    roffset %int32
    coffset %int32
    bgH % structure to maintain bg information
    fgH  % structure to maintain fg information
    opts % object of class segEngine_opts, see class definiton for settings
    localWindows_idx % only set if debugLevel>0, for visualization purposes
    trackedWindows_idx
    flowImg
    flow_occlMask
    postImg
    maskPosterior
  end

  methods
    function obj=segEngine(debugLevel,gtFile,vH,opts)
      obj.debugLevel=debugLevel;
      obj.gtFile=gtFile; 
      obj.vH=vH;
      [obj.h obj.w obj.nCh obj.nFrames]=obj.vH.videoSize();
      obj.state='initialized';
      obj.seg=zeros([obj.h obj.w obj.nFrames],'uint8');
      obj.framesSegmented=0;
      obj.frameNum=1;
      obj.video=[];
      obj.opts=opts;
      obj.beta=[];
    end
    img=view_1(obj) % Shows the local windows for learning -- i.e in previous frame
    img=view_2(obj) % Shows the local windows used at test time -- i.e to segment cur frame
    img=view_3(obj) % Shows the likelihood image at train time (color only)
    img=view_4(obj) % Shows the likelihood image at test time (color only)
    img=view_5(obj) % Shows the likelihood image at test time (shape only)
    img=view_6(obj) % Shows the likelihood image at test time (combined)
    img=view_7(obj) % Shows the optical flow from prev frame to current frame
    img=view_8(obj) % Shows the occlusion mask for optical flow
    img=view_10(obj) % Show the fg segmentation

    printOptions(obj,fH) % prints the segmentation options to a file or std-out
                     % If not fH is specified, prints to stdout
  end

  methods (Access=private)
    preProcess_GC(obj)
    initBG_history(obj)
    initFG_history(obj)
    seg=gcSeg(obj,postImg,maskPosterior,img);
  end

end % of classdef
