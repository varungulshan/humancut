function img=view_7(obj)
% Shows the flow from prevFrame to current Frame

if(obj.debugLevel>0)
  if(obj.frameNum==1),
    img=zeros([obj.h obj.w 3]);
    msg=sprintf('Frame_1_does_not_have_flow_propagated to it',obj.frameNum);
    img=miscFns.rendertext(img,msg,[1 1 1],'ovr'); 
  else
    [h w nCh]=size(obj.flowImg);
    if(h~=obj.h | w~=obj.w | nCh~=2),
      error('Improper dimesnions for flow\n');
    end
    
    img=flowToColor(obj.flowImg);
    colorWheel=makeColorWheel(25);
    [h w nCh]=size(img);
    [hWheel wWheel nCh_wheel]=size(colorWheel);
    colorWheel=colorWheel(1:min(h,hWheel),1:min(w,wWheel),:);
    [hWheel wWheel nCh_wheel]=size(colorWheel);
    img(1:hWheel,w-wWheel+1:w,:)=colorWheel;
  end
else
  img=zeros([obj.h obj.w 3]);
  img=miscFns.rendertext(img,'No_view_7_at_debugLevel_0',[1 1 1],'ovr'); 
end
