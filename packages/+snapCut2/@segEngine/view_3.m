function img=view_3(obj)
% Shows the color model posterior img at train time (i.e the color models are applied
% to the same frame as they are learnt from)

if(obj.debugLevel>0)
  if(obj.frameNum==1),
    img=zeros([obj.h obj.w 3]);
    msg=sprintf('Frame_1_does_not_learn_color_models',obj.frameNum);
    img=miscFns.rendertext(img,msg,[1 1 1],'ovr'); 
  else
    obj.vH.moveBackward();
    prevFrame=obj.vH.curFrame;
    prevSeg=obj.seg(:,:,obj.frameNum-1);
    obj.vH.moveForward();
    img=getLhood_image(obj,prevFrame,prevSeg);
  end
else
  img=zeros([obj.h obj.w 3]);
  img=miscFns.rendertext(img,'No_view_3_at_debugLevel_0',[1 1 1],'ovr'); 
end

function postImg=getLhood_image(obj,prevFrame,prevSeg)

import snapCut2.*;
% This code is a copy(+minor changes) of the code in snapCut_seg.m, if you change that code
% then you should update this also, otherwise the visualization will not be correct!

fgMask=prevSeg;
fgMask=(fgMask==255);
winRad=obj.opts.winRad;
localWindows_idx=obj.localWindows_idx;

mask_likelihood=zeros(size(fgMask),'uint8');
mask_likelihood(fgMask)=255;
[postImg,maskPosterior]=...
    localClassifiers(fgMask,localWindows_idx,prevFrame,winRad,obj.opts,mask_likelihood);

postImg(maskPosterior==255)=1;
postImg(maskPosterior==0)=0;

function [posteriors,maskPosterior]=...
    localClassifiers(curMask,winIdx,img,winRad,opts,maskPosterior)

import snapCut2.*

[h w nCh]=size(img);

img=im2double(img);

posteriors=zeros([h w]);
weights=zeros([h w]);

spWeights=getWindow_wts(winRad,opts.winWts_eps);
winSize=2*winRad+1;

for i =1:length(winIdx)
  cur_winIdx=winIdx(i);
  [winY,winX]=ind2sub([h w],cur_winIdx);

  xMin=max(1,winX-winRad);xL_pad1=(xMin-(winX-winRad));
  xMax=min(w,winX+winRad);xR_pad1=(winX+winRad-xMax);
  yMin=max(1,winY-winRad);yT_pad1=(yMin-(winY-winRad));
  yMax=min(h,winY+winRad);yB_pad1=(winY+winRad-yMax);

  xRng=[xMin:xMax];yRng=[yMin:yMax];
  win1_h=length(yRng);
  win1_w=length(xRng);

  fgMask=curMask(yRng,xRng);
  bgMask=~fgMask;
  prevImg=img(yRng,xRng,:);

  fgFts=prevImg(repmat(fgMask,[1 1 nCh]));
  numFg_fts=nnz(fgMask);
  fgFts=reshape(fgFts,[numFg_fts nCh])';
  fgGmm=gmm.init_gmmBS(fgFts,opts.gmmNmix_fg);

  bgFts=prevImg(repmat(bgMask,[1 1 nCh]));
  numBg_fts=nnz(bgMask);
  bgFts=reshape(bgFts,[numBg_fts nCh])';
  bgGmm=gmm.init_gmmBS(bgFts,opts.gmmNmix_bg);

  boxImg=img(yRng,xRng,:);

  numFts=size(boxImg,1)*size(boxImg,2);
  fts=reshape(boxImg,[numFts nCh])';
  
  l_gamma=opts.gmmLikeli_gamma;
  unifValue=opts.gmmUni_value;
  
  localPosterior=gmm.compute_gmmPosteriors_mixtured(fts,fgGmm,bgGmm,l_gamma,unifValue);
  localPosterior=reshape(localPosterior,[win1_h win1_w]);
  localWts=spWeights( (1+yT_pad1):(winSize-yB_pad1), (1+xL_pad1):(winSize-xR_pad1) );
  posteriors(yRng,xRng)=posteriors(yRng,xRng)+localWts.*localPosterior;
  weights(yRng,xRng)=weights(yRng,xRng)+localWts;
  maskPosterior(yRng,xRng)=128;
end

mask=(maskPosterior==128);
posteriors(mask)=min(1,posteriors(mask)./weights(mask)); % doing the min with 1, for numerical
posteriors(~mask)=0.5;
% reasons

function wts=getWindow_wts(rad,epsilon)
  dia=2*rad+1;
  wts=zeros(dia,dia);
  [X,Y]=meshgrid([-rad:rad],[-rad:rad]);
  d=sqrt(X.*X+Y.*Y);
  wts=1./(d+epsilon);
