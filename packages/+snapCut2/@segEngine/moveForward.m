function ok=moveForward(obj,labelImg)

import snapCut2.*

ok=false;

if(~strcmp(obj.state,'segStarted')),
  warning('Cant move unless segmentation is started\n');
end

if(obj.frameNum==obj.nFrames)
  warning('Cant go beyond nFrames\n');
  ok=false;
  return;
end

if(obj.frameNum<obj.framesSegmented),
  obj.frameNum=obj.frameNum+1;
  obj.vH.moveForward();
  ok=true;
  return;
end

if(obj.frameNum>obj.framesSegmented),
  ok=false;
  warning('Bug in flow propagator, frameNum>framesSegmented\n');
  return;
end

obj.vH.moveForward();
curFrame=obj.vH.curFrame;

warpBG=obj.bgH.propagate(curFrame);
warpFG=obj.fgH.propagate(curFrame,obj.opts.motionEnable);

obj.frameNum=obj.frameNum+1;

curSeg=obj.snapCut_seg(warpBG,warpFG,im2double(curFrame));
obj.seg(:,:,obj.frameNum)=curSeg;

obj.bgH.update(curSeg,curFrame,warpBG);
obj.fgH.update(curSeg,curFrame,warpFG);

obj.framesSegmented=obj.framesSegmented+1;

ok=true;
