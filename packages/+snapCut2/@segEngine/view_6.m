function img=view_6(obj)
% Shows the posterior image (color+shape combined) at test time

if(obj.debugLevel>0)
  if(obj.frameNum==1),
    img=zeros([obj.h obj.w 3]);
    msg=sprintf('Frame_1_does_not_test_color_models',obj.frameNum);
    img=miscFns.rendertext(img,msg,[1 1 1],'ovr'); 
  else
    img=obj.postImg;
    img(obj.maskPosterior==255)=1;
    img(obj.maskPosterior==0)=0;
  end
else
  img=zeros([obj.h obj.w 3]);
  img=miscFns.rendertext(img,'No_view_6_at_debugLevel_0',[1 1 1],'ovr'); 
end


