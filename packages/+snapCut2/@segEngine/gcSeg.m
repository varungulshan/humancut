function seg=gcSeg(obj,postImg,mask,img)

import snapCut2.cpp.*

opts=obj.opts;
beta=obj.beta;
neglHood_fg=-log(postImg);
neglHood_bg=-log(1-postImg);

myUB=realmax/(opts.gcScale*100);

neglHood_fg(neglHood_fg>myUB)=myUB;
neglHood_bg(neglHood_bg>myUB)=myUB;
[seg,flow]=mex_gcBand(mask,neglHood_fg,neglHood_bg,opts.gcGamma,beta,...
                      obj.roffset',obj.coffset',opts.gcScale,opts.gcIsing,...
                      img);

% TO CHECK empirically if mex_gcBand handles overflow properly!
