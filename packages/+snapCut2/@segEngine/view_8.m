function img=view_8(obj)
% Shows the occlusion mask, in the flow computed from prev frame to cur frame

if(obj.debugLevel>0)
  if(obj.frameNum==1),
    img=zeros([obj.h obj.w 3]);
    msg=sprintf('Frame_1_does_not_have_flow_propagated to it',obj.frameNum);
    img=miscFns.rendertext(img,msg,[1 1 1],'ovr'); 
  else
    img=double(obj.flow_occlMask);
  end
else
  img=zeros([obj.h obj.w 3]);
  img=miscFns.rendertext(img,'No_view_8_at_debugLevel_0',[1 1 1],'ovr'); 
end
