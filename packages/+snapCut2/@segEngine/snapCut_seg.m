function seg=snapCut_seg(obj,warpBG,warpFG,img)
% img should be double
import snapCut2.*;

fgMask=warpFG.mask_known;
winRad=obj.opts.winRad;
[bdryMask,localWindows_idx]=cpp.mex_getWindowPts2(fgMask,int32(winRad));

mask_likelihood=zeros(size(fgMask),'uint8');
mask_likelihood(fgMask)=255;

[postImg,maskPosterior,trackedWindows_idx]=...
      localClassifiers(localWindows_idx,warpBG,warpFG,img,winRad,...
      obj.opts,mask_likelihood);

seg=obj.gcSeg(postImg,maskPosterior,img);
if(obj.debugLevel>0)
  obj.localWindows_idx=localWindows_idx;
  obj.trackedWindows_idx=trackedWindows_idx;
  obj.flowImg=warpFG.flowImg;
  obj.flow_occlMask=warpFG.occlMask;
  obj.postImg=postImg;
  obj.maskPosterior=maskPosterior;
end

function [posteriors,maskPosterior,trackedIdx]=...
    localClassifiers(winIdx,warpBG,warpFG,img,winRad,opts,maskPosterior)

import snapCut2.*

[h w nCh]=size(warpFG.img);

warpFG.img=im2double(warpFG.img);

posteriors=zeros([h w]);
weights=zeros([h w]);

flowX=warpFG.flowImg(:,:,1);
flowY=warpFG.flowImg(:,:,2);
trackedIdx=zeros(size(winIdx));
spWeights=getWindow_wts(winRad,opts.winWts_eps);
winSize=2*winRad+1;

if(~opts.motionEnable), opts.sigma_s=inf; end;

for i =1:length(winIdx)
  cur_winIdx=winIdx(i);
  [winY,winX]=ind2sub([h w],cur_winIdx);

  xMin=max(1,winX-winRad);xL_pad1=(xMin-(winX-winRad));
  xMax=min(w,winX+winRad);
  yMin=max(1,winY-winRad);yT_pad1=(yMin-(winY-winRad));
  yMax=min(h,winY+winRad);

  xRng=[xMin:xMax];yRng=[yMin:yMax];
  win1_h=length(yRng);
  win1_w=length(xRng);

  fgMask=warpFG.mask_known(yRng,xRng);
  trackedMask=~warpFG.occlMask(yRng,xRng);
  bgMask=~fgMask;
  prevImg=warpFG.img(yRng,xRng,:);

  fgFts=prevImg(repmat(fgMask,[1 1 nCh]));
  numFg_fts=nnz(fgMask);
  fgFts=reshape(fgFts,[numFg_fts nCh])';
  fgGmm=gmm.init_gmmBS(fgFts,opts.gmmNmix_fg);

  bgFts=prevImg(repmat(bgMask,[1 1 nCh]));
  numBg_fts=nnz(bgMask);
  bgFts=reshape(bgFts,[numBg_fts nCh])';
  bgGmm=gmm.init_gmmBS(bgFts,opts.gmmNmix_bg);

  localFlow_x=flowX(yRng,xRng);
  localFlow_y=flowY(yRng,xRng);
  deltaX=round(mean(localFlow_x(fgMask & trackedMask)));
  deltaY=round(mean(localFlow_y(fgMask & trackedMask)));
  if(isnan(deltaX)), deltaX=0; end;
  if(isnan(deltaY)), deltaY=0; end;

  winX=winX+deltaX;
  winY=winY+deltaY;
  trackedIdx(i)=(winX-1)*h+winY;
  xMin=max(1,winX-winRad);xL_pad2=(xMin-(winX-winRad));
  xMax=min(w,winX+winRad);xR_pad2=(winX+winRad-xMax);
  yMin=max(1,winY-winRad);yT_pad2=(yMin-(winY-winRad));
  yMax=min(h,winY+winRad);yB_pad2=(winY+winRad-yMax);

  xRng=[xMin:xMax];yRng=[yMin:yMax];
  win2_h=length(yRng);
  win2_w=length(xRng);

  fgMask_prop=0.5*ones(win2_h,win2_w); 
  delta_padY=yT_pad1-yT_pad2;
  yRng_mask1=[max(1,1-delta_padY):min(win1_h,win2_h-delta_padY)];
  yRng_mask2=yRng_mask1+delta_padY;
  delta_padX=xL_pad1-xL_pad2;
  xRng_mask1=[max(1,1-delta_padX):min(win1_w,win2_w-delta_padX)];
  xRng_mask2=xRng_mask1+delta_padX;

  fgMask=double(fgMask);fgMask(~trackedMask)=0.5;
  fgMask_prop(yRng_mask2,xRng_mask2)=fgMask(yRng_mask1,xRng_mask1);

  boxImg=img(yRng,xRng,:);

  numFts=size(boxImg,1)*size(boxImg,2);
  fts=reshape(boxImg,[numFts nCh])';
  
  l_gamma=opts.gmmLikeli_gamma;
  unifValue=opts.gmmUni_value;
  
  localPosterior=gmm.compute_gmmPosteriors_mixtured(fts,fgGmm,bgGmm,l_gamma,unifValue);
  localPosterior=reshape(localPosterior,[win2_h win2_w]);
  localShape=fgMask_prop;
  shapeConfidence=getShapeConfidence(fgMask_prop,opts.sigma_s); 
  localWts=spWeights( (1+yT_pad2):(winSize-yB_pad2), (1+xL_pad2):(winSize-xR_pad2) );
  posteriors(yRng,xRng)=posteriors(yRng,xRng)+...
        localWts.*(shapeConfidence.*localShape+(1-shapeConfidence).*localPosterior);
  weights(yRng,xRng)=weights(yRng,xRng)+localWts;
  maskPosterior(yRng,xRng)=128;
end

mask=(maskPosterior==128);
posteriors(mask)=min(1,posteriors(mask)./weights(mask)); % doing the min with 1, for numerical
posteriors(~mask)=0.5;


function wts=getWindow_wts(rad,epsilon)
  dia=2*rad+1;
  wts=zeros(dia,dia);
  [X,Y]=meshgrid([-rad:rad],[-rad:rad]);
  d=sqrt(X.*X+Y.*Y);
  wts=1./(d+epsilon);

function conf=getShapeConfidence(mask,sigma_s)

sigma_s_sqr=sigma_s^2;
conf=zeros(size(mask));
fgMask=(mask==1);
bgMask=(mask==0);
dFG=bwdist(fgMask,'quasi-euclidean');
dBG=bwdist(bgMask,'quasi-euclidean');

conf(bgMask)=1-exp(-(dFG(bgMask).^2)/sigma_s_sqr);
conf(fgMask)=1-exp(-(dBG(fgMask).^2)/sigma_s_sqr);
