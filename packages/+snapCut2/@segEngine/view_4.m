function img=view_4(obj)
% Shows the color model posterior img at test time (i.e current frame)

if(obj.debugLevel>0)
  if(obj.frameNum==1),
    img=zeros([obj.h obj.w 3]);
    msg=sprintf('Frame_1_does_not_test_color_models',obj.frameNum);
    img=miscFns.rendertext(img,msg,[1 1 1],'ovr'); 
  else
    obj.vH.moveBackward();
    prevFrame=obj.vH.curFrame;
    prevSeg=obj.seg(:,:,obj.frameNum-1);
    obj.vH.moveForward();
    img=getLhood_image(obj,prevFrame,prevSeg);
  end
else
  img=zeros([obj.h obj.w 3]);
  img=miscFns.rendertext(img,'No_view_4_at_debugLevel_0',[1 1 1],'ovr'); 
end

function postImg=getLhood_image(obj,prevFrame,prevSeg)

import snapCut2.*;

fgMask=(prevSeg==255);
winRad=obj.opts.winRad;
localWindows_idx=obj.localWindows_idx;

img=obj.vH.curFrame;
mask_likelihood=zeros(size(fgMask),'uint8');
mask_likelihood(fgMask)=255;
[postImg,maskPosterior]=...
      localClassifiers(localWindows_idx,img,winRad,...
      obj.opts,mask_likelihood,prevFrame,fgMask,obj.trackedWindows_idx);

postImg(maskPosterior==255)=1;
postImg(maskPosterior==0)=0;

function [posteriors,maskPosterior]=...
    localClassifiers(winIdx,img,winRad,opts,maskPosterior,prevFrame,prevSeg,trackedWindows_idx)

import snapCut2.*

[h w nCh]=size(img);

img=im2double(img);
prevFrame=im2double(prevFrame);

posteriors=zeros([h w]);
weights=zeros([h w]);

spWeights=getWindow_wts(winRad,opts.winWts_eps);
winSize=2*winRad+1;

for i =1:length(winIdx)
  cur_winIdx=winIdx(i);
  [winY,winX]=ind2sub([h w],cur_winIdx);

  xMin=max(1,winX-winRad);xL_pad1=(xMin-(winX-winRad));
  xMax=min(w,winX+winRad);
  yMin=max(1,winY-winRad);yT_pad1=(yMin-(winY-winRad));
  yMax=min(h,winY+winRad);

  xRng=[xMin:xMax];yRng=[yMin:yMax];
  win1_h=length(yRng);
  win1_w=length(xRng);

  fgMask=prevSeg(yRng,xRng);
  bgMask=~fgMask;
  prevImg=prevFrame(yRng,xRng,:);

  fgFts=prevImg(repmat(fgMask,[1 1 nCh]));
  numFg_fts=nnz(fgMask);
  fgFts=reshape(fgFts,[numFg_fts nCh])';
  fgGmm=gmm.init_gmmBS(fgFts,opts.gmmNmix_fg);

  bgFts=prevImg(repmat(bgMask,[1 1 nCh]));
  numBg_fts=nnz(bgMask);
  bgFts=reshape(bgFts,[numBg_fts nCh])';
  bgGmm=gmm.init_gmmBS(bgFts,opts.gmmNmix_bg);

  trackedIdx=trackedWindows_idx(i);
  [winY,winX]=ind2sub([h w],trackedIdx);

  xMin=max(1,winX-winRad);xL_pad2=(xMin-(winX-winRad));
  xMax=min(w,winX+winRad);xR_pad2=(winX+winRad-xMax);
  yMin=max(1,winY-winRad);yT_pad2=(yMin-(winY-winRad));
  yMax=min(h,winY+winRad);yB_pad2=(winY+winRad-yMax);

  xRng=[xMin:xMax];yRng=[yMin:yMax];
  win2_h=length(yRng);
  win2_w=length(xRng);

  boxImg=img(yRng,xRng,:);

  numFts=size(boxImg,1)*size(boxImg,2);
  fts=reshape(boxImg,[numFts nCh])';
  
  l_gamma=opts.gmmLikeli_gamma;
  unifValue=opts.gmmUni_value;
  
  localPosterior=gmm.compute_gmmPosteriors_mixtured(fts,fgGmm,bgGmm,l_gamma,unifValue);
  localPosterior=reshape(localPosterior,[win2_h win2_w]);
  localWts=spWeights( (1+yT_pad2):(winSize-yB_pad2), (1+xL_pad2):(winSize-xR_pad2) );
  posteriors(yRng,xRng)=posteriors(yRng,xRng)+localWts.*localPosterior;
  weights(yRng,xRng)=weights(yRng,xRng)+localWts;
  maskPosterior(yRng,xRng)=128;
end

mask=(maskPosterior==128);
posteriors(mask)=min(1,posteriors(mask)./weights(mask)); % doing the min with 1, for numerical
posteriors(~mask)=0.5;

function wts=getWindow_wts(rad,epsilon)
  dia=2*rad+1;
  wts=zeros(dia,dia);
  [X,Y]=meshgrid([-rad:rad],[-rad:rad]);
  d=sqrt(X.*X+Y.*Y);
  wts=1./(d+epsilon);
