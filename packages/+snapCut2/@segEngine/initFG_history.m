function initFG_history(obj)

firstSeg=obj.gtSeg_firstFrame;
opts=obj.opts;
obj.fgH=snapCut2.fgHistory(obj.debugLevel,obj.vH.curFrame,firstSeg==255,...
                 opts.flow_maxBox,opts.flow_xRad,opts.flow_yRad);
