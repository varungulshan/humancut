function posteriors=compute_gmmPosteriorsIndep(features,fgGmm,bgGmm)
% Function to compute posteriors of each feature
% given the gmm. Each channel is treated independently and
% likelihoods are multiplied
% Inputs:
%  features: a N x D array of features (D=dimensionality)
%  fgGmm, bgGmm: gmm structures for the foreground and background
%  gmm

fgLikeli=computeGmm_likelihoodIndep(features,fgGmm);
bgLikeli=computeGmm_likelihoodIndep(features,bgGmm);

divByZero=(fgLikeli==0 & bgLikeli==0);
fgLikeli(divByZero)=1;
bgLikeli(divByZero)=1;
posteriors=fgLikeli./(fgLikeli+bgLikeli);

function likeli=computeGmm_likelihoodIndep(features,gmm)

likeli=zeros(1,size(features,1));
for i = 1:length( gmm.pi )
  likeli=likeli+vag_normPdf_indep(features,gmm.mu(:,i),gmm.sigma(:,:,i),gmm.pi(i));
end

