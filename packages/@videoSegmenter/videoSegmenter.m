classdef videoSegmenter < handle
  % Abstract class, defining interface for video segmentation methods
  properties (Abstract, SetAccess=private, GetAccess=public)
    debugLevel
  end

  methods(Abstract)
    preProcess(obj) 
    startOk=start(obj,labelImg,frameNum)
    updateSeg(obj,labelImg,frameNum)
    moveOk=moveForward(obj,labelImg)
    moveOk=moveBackward(obj,labelImg)
    seg=curFrame_seg(obj)
  end
end
