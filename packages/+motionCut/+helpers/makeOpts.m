function opts=makeOpts(optsString)

switch(optsString)

case 'rad40_vis5x'
    % motion multiplier for visualization is 5 times
    opts=motionCut.segEngine_opts();
    opts.gcGamma_e=0.5;
    opts.gcGamma_s=0.1;
    opts.gcGamma_i=0;
    opts.gcScale=100000;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.winRad=40;
    opts.srchRad_bg=30;
    opts.srchRad_fg=30;
    opts.minPixels_match=400;

    visOpts=opts.visOpts;
    visOpts.motionMultiplier=5;
    visOpts.fgArrowClr=uint8([0 0 255]); % Blue
    visOpts.bgArrowClr=uint8([255 255 0]); % Yellow
    visOpts.arrowWidth=int32(2);
    visOpts.centerClr=uint8([255 128 0]);
    visOpts.centerRad=int32(1);
    visOpts.betaVisualize_unary=10;
    visOpts.skipField=int32(10); % gap in pixels at which to draw motion field
    visOpts.segBoundaryWidth=2; % when outlining segmentation, line width to use
    visOpts.unaryVis_gammaMult=1; % Alternate way of visualizing unaries, relative
    opts.visOpts=visOpts;
 
case 'rad40'
    opts=motionCut.segEngine_opts();
    opts.gcGamma_e=0.5;
    opts.gcGamma_s=0.1;
    opts.gcGamma_i=0;
    opts.gcScale=100000;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.winRad=40;
    opts.srchRad_bg=30;
    opts.srchRad_fg=30;
    opts.minPixels_match=400;

    visOpts=opts.visOpts;
    visOpts.motionMultiplier=1;
    visOpts.fgArrowClr=uint8([0 0 255]); % Blue
    visOpts.bgArrowClr=uint8([255 255 0]); % Yellow
    visOpts.arrowWidth=int32(1);
    visOpts.centerClr=uint8([255 128 0]);
    visOpts.centerRad=int32(1);
    visOpts.betaVisualize_unary=10;
    visOpts.skipField=int32(25); % gap in pixels at which to draw motion field
    visOpts.segBoundaryWidth=2; % when outlining segmentation, line width to use
    visOpts.unaryVis_gammaMult=1; % Alternate way of visualizing unaries, relative
    opts.visOpts=visOpts;
  
case 'rad30'
    opts=motionCut.segEngine_opts();
    opts.gcGamma_e=0.5;
    opts.gcGamma_s=0.1;
    opts.gcGamma_i=0;
    opts.gcScale=100000;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.winRad=30;
    opts.srchRad_bg=15;
    opts.srchRad_fg=20;
    opts.minPixels_match=100;
   otherwise
    error('Invalid options string %s\n',optsString);
end
