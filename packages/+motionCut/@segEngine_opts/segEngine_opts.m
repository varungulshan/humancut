classdef segEngine_opts 
  properties (SetAccess=public, GetAccess=public)
    gcGamma_e  % gamma of pairwise term
    gcGamma_s  % gamma of shape term
    gcGamma_i  % gamma of pairwise term with ising model
    gcNbrType % 'nbr4' or 'nbr8'
    gcSigma_c % 'auto' or a numeric value
    gcScale   % scaling from flot to integer

    winRad   % local window radius, usually 15-40
    srchRad_bg % search radius for bg motion
    srchRad_fg % search radius for fg motion
    minPixels_match % minimum pixels to be considered for computing SSD (to prevent estimating
                    % total occlusion or moving out of the image)
    visOpts  % Structure containing various options for visualization
             % Fields include:
             % motionMultiplier:  motion vector is multiplied by this number for 
             %                    visualization purposes
             % fgArrowClr: color for drawing fg arrows of motion (uint8)
             % bgArrowClr: color for drawing bg arrows of motion (uint8)
             % arrowWidth: width of line to draw (int32)
             % centerClr: color for drawing the center of the box (uint8)
             % centerRad: radius of center of box (int32)
             % betaVisualize_unary: for visualizing unary
  end

  methods
    function obj=segEngine_opts()
      % Set the default options
      obj.gcGamma_e=5;
      obj.gcGamma_s=0.1;
      obj.gcGamma_i=0;
      obj.gcScale=100000;
      obj.gcNbrType='nbr8';
      obj.gcSigma_c='auto';

      obj.winRad=30;
      obj.srchRad_bg=10;
      obj.srchRad_fg=15;
      obj.minPixels_match=100;

      visOpts.motionMultiplier=1;
      visOpts.fgArrowClr=uint8([0 0 255]); % Blue
      visOpts.bgArrowClr=uint8([255 255 0]); % Yellow
      visOpts.arrowWidth=int32(2);
      visOpts.centerClr=uint8([255 128 0]);
      visOpts.centerRad=int32(1);
      visOpts.betaVisualize_unary=10;
      visOpts.skipField=int32(10); % gap in pixels at which to draw motion field
      visOpts.segBoundaryWidth=2; % when outlining segmentation, line width to use
      visOpts.unaryVis_gammaMult=1; % Alternate way of visualizing unaries, relative
      % to the pairwise edge strenght. See visualize_unary2.m for details
      obj.visOpts=visOpts;
 
    end
  end

end % of classdef
