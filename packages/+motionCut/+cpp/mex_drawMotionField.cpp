// Draw motion vectors using OpenCV functions

#include "mex.h"
#include <cmath>
#include <iostream>
#include "cv.h"

typedef unsigned int uint;
typedef unsigned char uchar;

void matlabTo_openCV_matrix(uchar* img,cv::Mat &cvImg,int h,int w,int nCh);
void openCV_toMatlab_matrix(cv::Mat inImg,uchar *outImg,int h,int w,int nCh);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters
     rhs[0] = img     [h x w x nCh] (uint8) current frame, column major order
     rhs[1] = windowIdx [N x 1] (int32) index of window centers (0 indexed)
     rhs[2] = motionVec [2 x N] (int32) for each window, specify the horizontal and
                        vertical motion horizontal motion in motionVec(1,:),
                        vertical in motionVec(2,:)
     rhs[3] = lineClr [1 x 3] (uint8) color value for drawing the line (r g b)
     rhs[4] = arrowWidth (scalar int32)
     rhs[5] = centerClr [1 x 3] (uint8) color value for drawing the center (r g b)
     rhs[6] = centerRad (scalar int32)
     rhs[7] = winRad (scalar int32) radius of window in which to draw motion field
     rhs[8] = skipField (scalar int32) gap between field arrows (>=0, 0 means every pixel)
     rhs[9] = mask [h x w] (logical) mask within which to draw field

     Output: 
      lhs[0] = [h x w x nCh] (uint8) Output image with lines drawn
  */

  if (nrhs != 10)
    mexErrMsgTxt("10 input arguments expected.");
  if (nlhs != 1)
    mexErrMsgTxt("1 output arguments expected.");

  if(mxGetClassID(prhs[0])!=mxUINT8_CLASS) mexErrMsgTxt("prhs[0] (img) shd be of type uint8\n");
  if(mxGetClassID(prhs[1])!=mxINT32_CLASS) mexErrMsgTxt("prhs[1] (windowIdx) shd be of type int32\n");
  if(mxGetClassID(prhs[2])!=mxINT32_CLASS) mexErrMsgTxt("prhs[2] (motionVec) shd be of type int32\n");
  if(mxGetClassID(prhs[3])!=mxUINT8_CLASS) mexErrMsgTxt("prhs[3] (lineClr) shd be of type uint8\n");
  if(mxGetClassID(prhs[4])!=mxINT32_CLASS) mexErrMsgTxt("prhs[4] (arrowWidth) shd be of type int32\n");
  if(mxGetClassID(prhs[5])!=mxUINT8_CLASS) mexErrMsgTxt("prhs[5] (centerClr) shd be of type uint8\n");
  if(mxGetClassID(prhs[6])!=mxINT32_CLASS) mexErrMsgTxt("prhs[6] (centerRad) shd be of type int32\n");
  if(mxGetClassID(prhs[7])!=mxINT32_CLASS) mexErrMsgTxt("prhs[7] (winRad) shd be of type int32\n");
  if(mxGetClassID(prhs[8])!=mxINT32_CLASS) mexErrMsgTxt("prhs[8] (skipField) shd be of type int32\n");
  if(mxGetClassID(prhs[9])!=mxLOGICAL_CLASS) mexErrMsgTxt("prhs[9] (mask) shd be of type logical\n");

  int h=mxGetDimensions(prhs[0])[0];
  int w=mxGetDimensions(prhs[0])[1];
  int nCh;
  if(mxGetNumberOfDimensions(prhs[0])<3){nCh=1;}
  else{nCh=mxGetDimensions(prhs[0])[2];}
  int N=h*w;

  if(nCh!=3)
    mexErrMsgTxt("Currently only RGB images are supported\n");

  int nWindows=mxGetM(prhs[1]);
  if(mxGetN(prhs[1])!=1)
    mexErrMsgTxt("windowIdx should be a column vector\n");

  if(mxGetM(prhs[2])!=2 || mxGetN(prhs[2])!=nWindows)
    mexErrMsgTxt("motionVec of inappropriate size\n");
  if(mxGetM(prhs[3])!=1 || mxGetN(prhs[3])!=3)
    mexErrMsgTxt("lineClr of inappropriate size\n");
  if(mxGetM(prhs[5])!=1 || mxGetN(prhs[5])!=3)
    mexErrMsgTxt("centerClr of inappropriate size\n");
  if(mxGetM(prhs[9])!=h || mxGetN(prhs[9])!=w)
    mexErrMsgTxt("mask not of same size as img\n");

  uchar *img,*outImg;
  int *windowIdx;
  int *motionVec;
  uchar *lineClr,*centerClr;
  int arrowWidth,centerRad,winRad,skipField;
  bool *mask;

  img=(uchar*)mxGetData(prhs[0]);
  windowIdx=(int*)mxGetData(prhs[1]);
  motionVec=(int*)mxGetData(prhs[2]);
  lineClr=(uchar*)mxGetData(prhs[3]);
  arrowWidth=*(int*)mxGetData(prhs[4]);
  centerClr=(uchar*)mxGetData(prhs[5]);
  centerRad=*(int*)mxGetData(prhs[6]);
  winRad=*(int*)mxGetData(prhs[7]);
  skipField=*(int*)mxGetData(prhs[8]);
  mask=(bool*)mxGetData(prhs[9]);

  skipField+=1; // to make it the increment in pixels
  int dims[3];dims[0]=h;dims[1]=w;dims[2]=nCh;
  plhs[0]=mxCreateNumericArray(3,dims,mxUINT8_CLASS,mxREAL);
  outImg=(uchar*)mxGetData(plhs[0]);

  cv::Mat inImg=cv::Mat(h,w,CV_8UC3);
  matlabTo_openCV_matrix(img,inImg,h,w,nCh);

  for(int i=0;i<nWindows;i++){
    int winIdx=windowIdx[i];
    int xWin=winIdx/h;
    int yWin=winIdx%h;
    int deltaX=motionVec[2*i];
    int deltaY=motionVec[2*i+1];
    for(int x=xWin-winRad;x<=xWin+winRad;x+=skipField){
      if(x>=0 && x<w){
        for(int y=yWin-winRad;y<=yWin+winRad;y+=skipField){
          if(y>=0 && y<w){
            if(mask[x*h+y]){
              cv::Point pt1=cv::Point(x,y);
              cv::Point pt2=cv::Point(x+deltaX,y+deltaY);
              cv::line(inImg,pt1,pt2,cv::Scalar((double)lineClr[0],(double)lineClr[1],
                    (double)lineClr[2]),arrowWidth,CV_AA);
              cv::circle(inImg,pt1,centerRad,cv::Scalar((double)centerClr[0],
              (double)centerClr[1],(double)centerClr[2]),centerRad,CV_AA);
            }
          }
        }
      }
    }
  }

  openCV_toMatlab_matrix(inImg,outImg,h,w,nCh);

  // ----- To free -----------------------------------------------

}

void matlabTo_openCV_matrix(uchar* img,cv::Mat &cvImg,int h,int w,int nCh){
  int N=h*w;
  for(int y=0;y<h;y++){
    uchar* rowY=cvImg.ptr<uchar>(y);
    for(int x=0;x<w;x++){
      for(int d=0;d<nCh;d++){
        (*rowY)=img[d*N+h*x+y];
        rowY++;
      }
    }
  }
}

void openCV_toMatlab_matrix(cv::Mat cvImg,uchar *outImg,int h,int w,int nCh){
  int N=h*w;
  for(int y=0;y<h;y++){
    uchar* rowY=cvImg.ptr<uchar>(y);
    for(int x=0;x<w;x++){
      for(int d=0;d<nCh;d++){
        outImg[d*N+h*x+y]=(*rowY);
        rowY++;
      }
    }
  }
}
