function preProcess(obj)
  if(~strcmp(obj.state,'initialized')),
    error('Cant call preProcess from %s state\n',obj.state);
    return;
  end

  gtFile=obj.gtFile;
  if(~exist(gtFile,'file')),
    warning('No GT file found!\n');
    obj.gtSeg_firstFrame=0*ones([obj.h obj.w],'uint8');
  else
    obj.gtSeg_firstFrame=imread(gtFile);
  end

  preProcess_GC(obj);
  obj.state='pped'; 

function preProcess_GC(obj)

switch(obj.opts.gcNbrType)
  case 'nbr4'
    roffset = int32([ 1,  0 ]);
    coffset = int32([  0, 1 ]);
  case 'nbr8'
    roffset = int32([ 1, 1, 0,-1 ]);
    coffset = int32([ 0, 1, 1, 1 ]);
  otherwise
    error('Invalid nbrhood type %s\n',obj.gcOpts.nbrHoodType);
end

obj.roffset=roffset;
obj.coffset=coffset;

if(isnumeric(obj.opts.gcSigma_c))
  D=obj.nCh;
  obj.beta=1/(2*D*obj.opts.gcSigma_c^2);
elseif(strcmp(obj.opts.gcSigma_c,'auto'))
  C=im2double(obj.vH.curFrame);
  [lEdges,rEdges,colorWeights,spWeights]=motionCut.cpp.mex_setupTransductionGraph(C,roffset',coffset');
  %obj.beta=1/(0.5*mean(colorWeights));
  obj.beta=1/(2*mean(colorWeights));
end
