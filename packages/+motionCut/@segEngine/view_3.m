function img=view_3(obj)
% Shows the motion vectors of fg relative to bg

if(obj.debugLevel>0)
  if(obj.frameNum==1),
    img=zeros([obj.h obj.w 3]);
    msg=sprintf('No motion vectors from previous frame',obj.frameNum);
    %img=miscFns.rendertext(img,msg,[1 1 1],'ovr'); 
    img=miscFns.mex_drawText(im2uint8(img),int32(15),msg);
  else
    obj.vH.moveBackward();
    prevFrame=im2double(obj.vH.curFrame);
    prevSeg=obj.seg(:,:,obj.frameNum-1);
    obj.vH.moveForward();
    [segBoundaryMask,segBoundaryColors]= ...
    miscFns.getSegBoundary_twoColors(prevSeg,[0 1 0],[1 0 0],1,1);
    prevFrame(segBoundaryMask)=segBoundaryColors;
    img=drawMotion(prevFrame,obj.localWindows_idx,obj.opts.winRad,obj.debugInfo,...
                  obj.opts.visOpts);
  end
else
  img=zeros([obj.h obj.w 3]);
  img=miscFns.rendertext(img,'No view 1 at debugLevel 0',[1 1 1],'ovr'); 
end

function img=drawMotion(img,windows_idx,winRad,debugInfo,visOpts)

import motionCut.cpp.*;
img=im2uint8(img);
img=mex_drawMotion(img,windows_idx-1,...
                   visOpts.motionMultiplier*(debugInfo.motionFG-debugInfo.motionBG),...
                   visOpts.fgArrowClr,visOpts.arrowWidth,visOpts.centerClr,...
                   visOpts.centerRad);
img=im2double(img);
