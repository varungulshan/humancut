function nxtSeg=motionCut_seg(obj,curFrame,nxtFrame,curSeg)
% curFrame and nxtFrame should be uint8
import motionCut.*;

if(~strcmp(class(curFrame),'uint8')|~strcmp(class(nxtFrame),'uint8'))
  error('motionCut_seg expects uint8 inputs for images\n');
end
curFrame=int32(curFrame);
nxtFrame=int32(nxtFrame);

opts=obj.opts;
winRad=opts.winRad;
[bdryMask,localWindows_idx]=cpp.mex_getWindowPts2(curSeg==255,int32(winRad));

motionParams.winRad=int32(winRad);
motionParams.srchRad_bg=int32(opts.srchRad_bg);
motionParams.srchRad_fg=int32(opts.srchRad_fg);
motionParams.gcGamma_s=opts.gcGamma_s;
motionParams.minPixels_match=int32(opts.minPixels_match);

debugLevel=int32(obj.debugLevel);

[unaryEnergy,unaryEnergy_valid,debugInfo]=cpp.mex_getUnariesMotion(curFrame,nxtFrame,...
                                   curSeg==255,localWindows_idx-1,motionParams,debugLevel);

unaryMask=curSeg;
unaryMask(unaryEnergy_valid)=128;
nxtSeg=obj.gcSeg(unaryEnergy,unaryMask,double(nxtFrame)/255);
obj.localWindows_idx=localWindows_idx;
if(obj.debugLevel>=1)
  debugInfo.unaryEnergy=unaryEnergy;
  debugInfo.unaryMask=unaryMask;
end
obj.debugInfo=debugInfo;
