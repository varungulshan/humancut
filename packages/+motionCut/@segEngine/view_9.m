function img=view_9(obj)
% Shows output segmentation outlined on original frame
segBoundaryWidth=obj.opts.visOpts.segBoundaryWidth;
frameNum=obj.frameNum;
seg=obj.seg(:,:,frameNum);

img=im2double(obj.vH.curFrame);

[segBoundaryMask,segBoundaryColors]= ...
miscFns.getSegBoundary_twoColors(seg,[0 1 0],[1 0 0],...
        segBoundaryWidth,segBoundaryWidth);
img(segBoundaryMask)=segBoundaryColors;
