function img=view_6(obj)
% visualize unary, relative to gamma of segmentation

if(obj.debugLevel>0)
  if(obj.frameNum==1),
    img=zeros([obj.h obj.w 3]);
    msg=sprintf('Frame_1_does_not_learn_color_models',obj.frameNum);
    %img=miscFns.rendertext(img,msg,[1 1 1],'ovr'); 
    img=miscFns.mex_drawText(im2uint8(img),int32(15),msg);
  else
    debugInfo=obj.debugInfo;
    gamma=obj.opts.gcGamma_i+obj.opts.gcGamma_e;
    img=motionCut.helpers.visualize_unary2(debugInfo.unaryEnergy,debugInfo.unaryMask,...
                  obj.opts.visOpts.unaryVis_gammaMult,gamma);
    img=repmat(img,[1 1 3]);
  end
else
  img=zeros([obj.h obj.w 3]);
  img=miscFns.rendertext(img,'No view 1 at debugLevel 0',[1 1 1],'ovr'); 
end
