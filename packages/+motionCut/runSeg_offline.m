function runSeg_offline(inVideoPath,inVideoFile,gtFile,outDir,visualize,views,segOpts,...
                        overWrite);

if(exist(outDir,'dir')),
  if(~overWrite)
    fprintf('Cannot overwrite exisiting results in: %s\n',outDir);
    return;
  else
    fprintf('\n\n---- Overwriting results in %s----------\n\n\n',outDir);
  end
else
  mkdir(outDir);
end

vH=myVideoReader(inVideoPath,inVideoFile);
segH=motionCut.segEngine(9,gtFile,vH,segOpts);

optsFile=[outDir 'opts.txt'];
fH=fopen(optsFile,'w');
segH.printOptions(fH);
fclose(fH);
viewsFile=[outDir 'views.txt'];
fH=fopen(viewsFile,'w');
viewStr=segH.describeViews();
fprintf(fH,'%s',viewStr);
fclose(fH);

segH.preProcess();
segH.start([],1); % empty labels being passed, as they dont get used

if(visualize)
  for j=1:length(views)
    cmd=sprintf('segH.view_%d()',views(j));
    view=eval(cmd);
    outFile=sprintf('view%d_%03d.png',views(j),segH.frameNum);
    imwrite(view,[outDir outFile]);
  end
else
  fprintf('Visualization is turned off\n');
end

for i=1:vH.nFrames-1
  moveOk=segH.moveForward();
  if(~moveOk)
    error('Trouble in moving forward from frame %d to next\n',i);
  end
  if(visualize)
    for j=1:length(views)
      cmd=sprintf('segH.view_%d()',views(j));
      view=eval(cmd);
      outFile=sprintf('view%d_%03d.png',views(j),segH.frameNum);
      imwrite(view,[outDir outFile]);
    end
  end
end

seg=segH.seg;
save([outDir 'seg.mat'],'seg');

delete(segH);
delete(vH);
