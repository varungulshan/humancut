function runTests(params)

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('params','var'))
  params.firstFrameGT_dir=[cwd '../../data/gtFirstFrame/'];
  params.rootOut_dir=[cwd '../../results/motionCut_visualize/rad40/'];
  params.visualize=true;
  params.views=[1 2 3 4 5 6 9 10];
  params.optsString='rad40';
  params.evalOpts_string='try1';
  params.overWrite=true;
  params.testBench_cmd='testBench.getTests_tmp()';
  %params.testBench_cmd='testBench.getTests_20frames()';
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
end

[videoPaths,videoFiles]=eval(params.testBench_cmd);

optsString=params.optsString;
curOutDir=params.rootOut_dir;

videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);
for i=1:length(videoNames)
  firstFrameGT_files{i}=[params.firstFrameGT_dir videoNames{i} '.png'];
  outDirs{i}=[curOutDir videoNames{i} '/'];
end

segOpts=motionCut.helpers.makeOpts(optsString);

for i=1:length(videoNames)
  fprintf('\nNow running on %s\n',[videoPaths{i} videoFiles{i}]);
  motionCut.runSeg_offline(videoPaths{i},videoFiles{i},firstFrameGT_files{i},...
                        outDirs{i},params.visualize,params.views,...
                        segOpts,params.overWrite);
end

testBench.computeStats(curOutDir,params.evalOpts_string);
%testBench.plotStats(rootOut_dir);

function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);
