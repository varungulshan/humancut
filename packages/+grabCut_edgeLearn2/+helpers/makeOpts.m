function opts=makeOpts(optsString)

switch(optsString)

  case 'iterQuickLearn'
    opts=grabCut_edgeLearn2.segOpts();
    opts.gcGamma=50;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=1;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.kovesiSigma = 1;
    opts.kovesiAutoThresh = 0.7;
    opts.enableEdgePosterior = true;

    opts.gmmNmix_yesEdge = 6;
    opts.gmmNmix_noEdge = 8;
    opts.itersBeforeLearning = 0;

  case 'iterQuick'
    opts=grabCut_edgeLearn2.segOpts();
    opts.gcGamma=50;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=1;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.kovesiSigma = 1;
    opts.kovesiAutoThresh = 0.7;
    opts.enableEdgePosterior = false;
    opts.itersBeforeLearning = 0;

   case 'iter10_2'
    opts=grabCut_edgeLearn2.segOpts();
    opts.gcGamma=50;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.kovesiSigma = 1;
    opts.kovesiAutoThresh = 0.7;
    opts.enableEdgePosterior = false;
    opts.itersBeforeLearning = 0;

   case 'iter10_2_edgeLearn'
    opts=grabCut_edgeLearn2.segOpts();
    opts.gcGamma=50;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.kovesiAutoThresh = 0.7;
    opts.enableEdgePosterior = true;

    opts.gmmNmix_yesEdge = 6;
    opts.gmmNmix_noEdge = 8;
    opts.kovesiSigma = 1;
    opts.itersBeforeLearning = 0;

   case 'iter10_3_soft'
    opts=grabCut_edgeLearn2.segOpts();
    opts.gcGamma=25;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.kovesiSigma = 1;
    opts.kovesiAutoThresh = 0.7;
    opts.enableEdgePosterior = false;
    opts.itersBeforeLearning = 0;
    opts.enableSoftEdges = true;
    
   case 'iter10_3'
    opts=grabCut_edgeLearn2.segOpts();
    opts.gcGamma=25;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.kovesiSigma = 1;
    opts.kovesiAutoThresh = 0.7;
    opts.enableEdgePosterior = false;
    opts.itersBeforeLearning = 0;

   case 'iter10_3_edgeLearnContinue2'
    opts=grabCut_edgeLearn2.segOpts();
    opts.gcGamma=25;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.kovesiAutoThresh = 0.7;
    opts.enableEdgePosterior = true;

    opts.gmmNmix_yesEdge = 6;
    opts.gmmNmix_noEdge = 8;
    opts.kovesiSigma = 1;
    opts.itersBeforeLearning = 0;
    opts.continuityOn = true;
    opts.continuityThresh = 35;
 
   case 'iter10_3_edgeLearnContinue'
    opts=grabCut_edgeLearn2.segOpts();
    opts.gcGamma=25;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.kovesiAutoThresh = 0.7;
    opts.enableEdgePosterior = true;

    opts.gmmNmix_yesEdge = 6;
    opts.gmmNmix_noEdge = 8;
    opts.kovesiSigma = 1;
    opts.itersBeforeLearning = 0;
    opts.continuityOn = true;
    opts.continuityThresh = 91;
 
   case 'iter10_3_edgeLearnWait'
    opts=grabCut_edgeLearn2.segOpts();
    opts.gcGamma=25;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.kovesiAutoThresh = 0.7;
    opts.enableEdgePosterior = true;

    opts.gmmNmix_yesEdge = 6;
    opts.gmmNmix_noEdge = 8;
    opts.kovesiSigma = 1;
    opts.itersBeforeLearning = 1;
  
   case 'iter10_3_edgeLearnLong'
    opts=grabCut_edgeLearn2.segOpts();
    opts.gcGamma=25;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.kovesiAutoThresh = 0.7;
    opts.enableEdgePosterior = true;

    opts.gmmNmix_yesEdge = 6;
    opts.gmmNmix_noEdge = 8;
    opts.kovesiSigma = 1;
    opts.itersBeforeLearning = 0;
    opts.edgeFeatureExtend = 4;
     
   case 'iter10_3_edgeLearn'
    opts=grabCut_edgeLearn2.segOpts();
    opts.gcGamma=25;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.kovesiAutoThresh = 0.7;
    opts.enableEdgePosterior = true;

    opts.gmmNmix_yesEdge = 6;
    opts.gmmNmix_noEdge = 8;
    opts.kovesiSigma = 1;
    opts.itersBeforeLearning = 0;
    
  otherwise
    error('Invalid options string %s\n',optsString);
end
