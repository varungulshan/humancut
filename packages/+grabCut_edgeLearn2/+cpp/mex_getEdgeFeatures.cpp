#include "mex.h"
#include <float.h>
#include <memory.h>
#include <math.h>
#include "matrix.h"
#include <iostream>
#include <vector>

typedef unsigned int uint;

inline void myAssert(bool condition,char *msg){
  if(!condition){
    mexErrMsgTxt(msg);
  }
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters
     features  [D x N] (double)
     edgeIdx   [2 x K] (int32) 1 indexed
     featureLength scalar int32
     h         scalar int32
     w         scalar int32

     Output:
     edgeFeatures [2D x K] (double)
  */

  myAssert(nrhs == 5, "5 input arguments expected.");
  if (nlhs != 1)
    mexErrMsgTxt("1 output arguments expected.");

  myAssert(mxGetClassID(prhs[0])==mxDOUBLE_CLASS,"prhs[0] (features) shd be of type double\n");
  myAssert(mxGetClassID(prhs[1])==mxINT32_CLASS,
      "prhs[1] (edgeIdx) shd be of type int32\n");
  myAssert(mxGetClassID(prhs[2])==mxINT32_CLASS,
      "prhs[2] (featureLength) shd be of type int32\n");
  myAssert(mxGetClassID(prhs[3])==mxINT32_CLASS,
      "prhs[3] (height) shd be of type double\n");
  myAssert(mxGetClassID(prhs[4])==mxINT32_CLASS,
      "prhs[4] (width) shd be of type double\n");

  int D = mxGetM(prhs[0]);
  int N = mxGetN(prhs[0]);
  myAssert(mxGetM(prhs[1])==2,"Invalid dimensions for edgeIdx\n");
  int numEdges = mxGetN(prhs[1]);
  int h = *(int*)mxGetData(prhs[3]);
  int w = *(int*)mxGetData(prhs[4]);
  myAssert(N==h*w,"Invalid h,w\n");

  double *features = mxGetPr(prhs[0]);
  int *edgeIdx = (int*)mxGetData(prhs[1]);
  int ftrLength = *(int*)mxGetData(prhs[2]);

  plhs[0] = mxCreateNumericMatrix(2*D,numEdges,mxDOUBLE_CLASS,mxREAL);
  double *outputFtr = mxGetPr(plhs[0]);

  int *ptrEdgeIdx = edgeIdx;
  double *ptrOutput = outputFtr;
  for(int i=0;i<numEdges;i++){
    int lEdge = *ptrEdgeIdx - 1;ptrEdgeIdx++;
    int rEdge = *ptrEdgeIdx - 1;ptrEdgeIdx++;
    int idxDiff = lEdge-rEdge;
    int signDiff = abs(idxDiff)/idxDiff;
    int dx = signDiff * ( abs(idxDiff) / h);
    int dy = signDiff * (idxDiff % h);
    
    // Get the left edge feature
    {
      int numFtrs = 0;
      int nxtIdx = lEdge;
      int nxtX = lEdge / h;
      int nxtY = lEdge % h;
      for(int d=0;d<D;d++){ptrOutput[d]=0;}
      for(int j=0;j<ftrLength;j++){
        if(nxtX<0 || nxtX>=w || nxtY<0 || nxtY >=h){break;}
        double *tmpFtr = features+D*nxtIdx;
        for(int d=0;d<D;d++){
          ptrOutput[d]+=tmpFtr[d];
        }
        numFtrs++;
        nxtX+=dx;
        nxtY+=dy;
        nxtIdx += idxDiff;
      }
      for(int d=0;d<D;d++){ptrOutput[d]/=numFtrs;}
    }
    ptrOutput+=D;
    idxDiff*=-1;dx*=-1;dy*=-1;
    // Get the right edge feature 
    {
      int numFtrs = 0;
      int nxtIdx = rEdge;
      int nxtX = rEdge / h;
      int nxtY = rEdge % h;
      for(int d=0;d<D;d++){ptrOutput[d]=0;}
      for(int j=0;j<ftrLength;j++){
        if(nxtX<0 || nxtX>=w || nxtY<0 || nxtY >=h){break;}
        double *tmpFtr = features+D*nxtIdx;
        for(int d=0;d<D;d++){
          ptrOutput[d]+=tmpFtr[d];
        }
        numFtrs++;
        nxtX+=dx;
        nxtY+=dy;
        nxtIdx += idxDiff;
      }
      for(int d=0;d<D;d++){ptrOutput[d]/=numFtrs;}
    }
    ptrOutput+=D;
  }

}
