#include "mex.h"
#include "graph.h"
#include <cmath>
#include <iostream>
#include <climits>
#include <vector>

using namespace std;
inline void myAssert(bool condition,char *msg){
  if(!condition){
    mexErrMsgTxt(msg);
  }
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters combinations
     rhs[0] = mask [h x w] (255 for fg, 0 for bg, 
              128 for where to run graph cut) [uint8]
     rhs[1] = unary terms [h x w x 2] (double) first channel for label=1 unaries
              second channel for label=2 unaries
     rhs[2] = graphInfo structure with following fields:
       dgcHandle -> handle to graph
       fgUnaryEdits_offset
       fgUnaryEdits_value
       bgUnaryEdits_offset
       bgUnaryEdits_value
     rhs[3] = opts structure with following fields:
      gcGamma_e -> double scalar
      gcGamma_i -> double scalar
      gcScale -> double scalar
      beta -> double scalar
      xoffset -> [ L x 1 ] int32 offsets
      yoffset -> [ L x 1 ] int32 offsets
     rhs[4] = [1 x N] (int32) index of canny edges (first idx)
     rhs[5] = [1 x N] (int32) index of canny edges (second idx)
     rhs[6] = [1 x N] (double) edgePosterior indicating edge is on or not

     lhs[0] -> returns the seg labels of all nodes (array of type hxwxnFrames uint8)
     lhs[1] -> returns the flow

     Notes on memory consumption:
     This code allocated following sized arrays (only big ones noted here):
     let nPix=h*w*nFrames;
     let nEdges=number of edges in graph (roughly 5*nPix for 8 neighbourhood in space
     and 1 nbrhood in time)
     idxMap -> [h x w x nFrames] (int32)
     graph structure (kolmogorov) -> 58 * nEdges + 44 * nPix
     (In practice nPix will be less because this code removes pixels which are
     hard constrained!)
   */

  if (nrhs != 7)
    mexErrMsgTxt("7 inputs required");

  if(mxGetClassID(prhs[0])!=mxUINT8_CLASS)
    mexErrMsgTxt("prhs[0] (mask) should be of type uint8\n");
  if(mxGetClassID(prhs[1])!=mxDOUBLE_CLASS)
    mexErrMsgTxt("prhs[1] (unaryImg) should be of type double\n");
  if(!mxIsStruct(prhs[2]))
    mexErrMsgTxt("prhs[2] (graphInfo) should be a structure\n");
  if(!mxIsStruct(prhs[3]))
    mexErrMsgTxt("prhs[3] (opts) should be a structure\n");
  myAssert(mxGetClassID(prhs[4])==mxINT32_CLASS,"prhs[4] (lEdges) should be int32\n");
  myAssert(mxGetClassID(prhs[5])==mxINT32_CLASS,"prhs[5] (rEdges) should be int32\n");
  myAssert(mxGetClassID(prhs[6])==mxDOUBLE_CLASS,"prhs[6] (edgePosterior) should be double\n");

  typedef Graph<int,int,int> GraphInt;
  int h=mxGetDimensions(prhs[0])[0];
  int w=mxGetDimensions(prhs[0])[1];
  int frameRes=h*w;
  int nOffsets,nPix;
  double gcScale;

  nPix=frameRes;

  // --- Check dimensions of prhs[1] -----
  myAssert(mxGetNumberOfDimensions(prhs[1])==3,
          "Incorrect number of dimensions for prhs[1]\n");

  if(mxGetDimensions(prhs[1])[0]!=h || mxGetDimensions(prhs[1])[1]!=w 
    || mxGetDimensions(prhs[1])[2]!=2) {
      mexErrMsgTxt("prhs[1] (unary img) incorrect dimensions\n");
  }

  // --- Check prhs[2] ------
  mxArray *tmp;
  int *fgUnaryEdits_offset,*bgUnaryEdits_offset;
  double *fgUnaryEdits_value,*bgUnaryEdits_value;
  int numFgEdits,numBgEdits;
  GraphInt *g;
  int numBytes_handle=sizeof(GraphInt*);
  double gcGamma;

  tmp=mxGetField(prhs[2],0,"dgcHandle");
  myAssert(tmp!=NULL,"dgcHandle field not found\n");
  memcpy(&g,mxGetData(tmp),numBytes_handle);

  tmp=mxGetField(prhs[2],0,"fgUnaryEdits_offset");
  myAssert(tmp!=NULL,"fgUnaryEdits_offset field not found\n");
  fgUnaryEdits_offset=(int*)mxGetData(tmp);
  numFgEdits=mxGetNumberOfElements(tmp);

  tmp=mxGetField(prhs[2],0,"fgUnaryEdits_value");
  myAssert(tmp!=NULL,"fgUnaryEdits_value field not found\n");
  fgUnaryEdits_value=mxGetPr(tmp);
  myAssert(numFgEdits==mxGetNumberOfElements(tmp),"Invalid dimensions\n");

  tmp=mxGetField(prhs[2],0,"bgUnaryEdits_offset");
  myAssert(tmp!=NULL,"bgUnaryEdits_offset field not found\n");
  bgUnaryEdits_offset=(int*)mxGetData(tmp);
  numBgEdits=mxGetNumberOfElements(tmp);

  tmp=mxGetField(prhs[2],0,"bgUnaryEdits_value");
  myAssert(tmp!=NULL,"bgUnaryEdits_value field not found\n");
  bgUnaryEdits_value=mxGetPr(tmp);
  myAssert(numBgEdits==mxGetNumberOfElements(tmp),"Invalid dimensions\n");

  // --- Check and initialize from the options structure -----

  tmp=mxGetField(prhs[3],0,"gcScale");
  myAssert(tmp!=NULL,"gcScale field not found\n");
  gcScale=*mxGetPr(tmp);

  tmp=mxGetField(prhs[3],0,"gcGamma");
  myAssert(tmp!=NULL,"gcGamma field not found\n");
  gcGamma=*mxGetPr(tmp);

  // --- Check the lEdges, rEdges and edgePosteriors --------
  int numCannyEdges = mxGetNumberOfElements(prhs[4]);
  myAssert(mxGetNumberOfElements(prhs[5])==numCannyEdges,"Invalid edges\n");

  // ---- Data verified ok, now set up all the pointers

  unsigned char *mask=(unsigned char*)mxGetData(prhs[0]);
  //double *unaryImg=mxGetPr(prhs[1]);
  double *unaryImg=(double*)mxMalloc(sizeof(double)*nPix*2);
  memcpy(unaryImg,mxGetPr(prhs[1]),sizeof(double)*nPix*2);
  double *fgL=unaryImg;
  double *bgL=fgL+frameRes;
  int *lEdges = (int*)mxGetData(prhs[4]);
  int *rEdges = (int*)mxGetData(prhs[5]);
  double *edgePosteriors = mxGetPr(prhs[6]);

  int *idxMap=(int*)mxMalloc(nPix*sizeof(int));

  // -- scan the mask to index the pixels ----
  unsigned char *it_mask=mask;
  int *it_idxMap=idxMap;
  int idx=0;
  for(int i=0;i<nPix;i++,it_mask++,it_idxMap++){
    if(*it_mask==128){*it_idxMap=idx;idx++;}
    else{*it_idxMap=-1;}
  }

  // -- alter unaries as given in fg and bg unary edits ---------
  for(int i=0;i<numFgEdits;i++){
    int offset=fgUnaryEdits_offset[i];
    fgL[offset]+=fgUnaryEdits_value[i];
  }
  for(int i=0;i<numBgEdits;i++){
    int offset=bgUnaryEdits_offset[i];
    bgL[offset]+=bgUnaryEdits_value[i];
  }

  // ----- Pass through the canny edges and reduce edge weights
  // ----- also update the unaries (for pixels which lie on hard constrianed
  // boundaries ----------
  int *lEdges_ptr=lEdges;int *rEdges_ptr=rEdges;
  double *edgePosterior_ptr=edgePosteriors;
  for(int i=0;i<numCannyEdges;i++){
    int lEdge_idx=*lEdges_ptr-1;
    int rEdge_idx=*rEdges_ptr-1;
    unsigned char lMask = mask[lEdge_idx];
    unsigned char rMask = mask[rEdge_idx];
    double edgeWt_reduction = (*edgePosterior_ptr) * gcGamma;
    double newEdgeWt = gcGamma - edgeWt_reduction;
    myAssert(*edgePosterior_ptr <=1 && *edgePosterior_ptr >=0,"Invalid posterior");
    if(lMask==128){
      if(rMask==128){
        // add edge to graph
        int edgeW=(int)(gcScale*newEdgeWt);
        g->edit_edge(idxMap[lEdge_idx],idxMap[rEdge_idx],edgeW,edgeW); 
        //g->mark_node(idxMap[lEdge_idx]);
        //g->mark_node(idxMap[rEdge_idx]);
        // TODO: Mark nodes?
      }
      else if(rMask==255){
        double *lEdge_bgL=bgL+lEdge_idx;
        *lEdge_bgL=*lEdge_bgL-edgeWt_reduction;
      }
      else if(rMask==0){
        double *lEdge_fgL=fgL+lEdge_idx;
        *lEdge_fgL=*lEdge_fgL-edgeWt_reduction;
      }
      else{
        mexErrMsgTxt("Invalid value in likelihood mask\n");
      }
    } // end if *it_mask==128
    else if(rMask==128){
      switch(lMask){
        case 0 :
          {double *rEdge_fgL=fgL+rEdge_idx;
            *rEdge_fgL=*rEdge_fgL-edgeWt_reduction;}
            break;
        case 255 :
            {double *rEdge_bgL=bgL+rEdge_idx;
              *rEdge_bgL=*rEdge_bgL-edgeWt_reduction;}
              break;
        default:
              mexErrMsgTxt("Unexpected mask value in likeli_mask\n");
      }
    } // end if nxtMask==128
 
    lEdges_ptr++;
    rEdges_ptr++;
    edgePosterior_ptr++;
  }

  it_mask=mask;
  it_idxMap=idxMap;
  int intMax=(1<<30);

  for(int i=0;i<frameRes;i++,it_mask++,it_idxMap++){
    if(*it_mask==128){
      int curIdx=*it_idxMap;
      double fgLikeli=fgL[i];
      double bgLikeli=bgL[i];
      double diff=gcScale*(fgLikeli-bgLikeli);
      double dbl_intMax=(double)(intMax);
      double dbl_intMin=(double)(-intMax);
      int fgUnary;
      if(diff>dbl_intMax){fgUnary=intMax;}
      else if(diff<dbl_intMin){fgUnary=-intMax;}
      else{fgUnary=(int)diff;}
      g->edit_tweights(curIdx,0,fgUnary);
      g->mark_node(curIdx);
    }
  }

  int flow=g->maxflow(true);

  // --- Now prepare the output ------------
  myAssert(nlhs>=2,"Atleast two outputs required\n");
  int dims[2];dims[0]=h;dims[1]=w;

  plhs[0]=mxCreateNumericArray(2,dims,mxUINT8_CLASS,mxREAL);
  plhs[1]=mxCreateDoubleScalar((double)flow);

  unsigned char *it_seg=(unsigned char*)mxGetData(plhs[0]);
  it_mask=mask;
  it_idxMap=idxMap;
  for(int i=0;i<nPix;i++,it_seg++,it_mask++,it_idxMap++){
    if(*it_mask==128){
      int curIdx=*it_idxMap;
      *it_seg=(g->what_segment(curIdx)==GraphInt::SOURCE?255:0); // 255 for source, 0 for sink
    }
    else{*it_seg=*it_mask;}
  }

  // ------- To free -------------
  mxFree(idxMap);
  mxFree(unaryImg);
  //deleteGraph<int,int,int>(&g);
}
