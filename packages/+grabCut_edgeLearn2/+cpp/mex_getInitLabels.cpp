#include "mex.h"
#include <float.h>
#include <memory.h>
#include <math.h>
#include "matrix.h"
#include <iostream>
#include <vector>
#include <queue>


typedef unsigned int uint;

inline void myAssert(bool condition,char *msg){
  if(!condition){
    mexErrMsgTxt(msg);
  }
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters
     edgeMap    [ h x w] (logical)
     initMask   [ h x w] (uint8)
     orient     [h x w] (double)
     contThresh scalar double
     learningEdges [1 x K] (int32)
     Output:
     edgeLabels [1 x K] (logical) true = learn yes edge, false = learn no edge
  */

  myAssert(nrhs == 5, "5 input arguments expected.");
  if (nlhs != 1)
    mexErrMsgTxt("1 output arguments expected.");

  if(mxGetClassID(prhs[0])!=mxLOGICAL_CLASS)
    mexErrMsgTxt("prhs[0] (edgeMape) shd be logical \n");
  myAssert(mxGetClassID(prhs[1])==mxUINT8_CLASS,"prhs[1] (initMask) shd be of type uint8\n");
  myAssert(mxGetClassID(prhs[2])==mxDOUBLE_CLASS,
      "prhs[2] (orient) shd be of type double\n");
  if(mxGetClassID(prhs[3])!=mxDOUBLE_CLASS) 
      mexErrMsgTxt("prhs[3] (contThres) shd be of type double\n");
  if(mxGetClassID(prhs[4])!=mxINT32_CLASS)
    mexErrMsgTxt("prhs[4] (learningEdges) shd be of type int32\n");

  int numDims=mxGetNumberOfDimensions(prhs[0]);
  
  int h=mxGetM(prhs[0]);
  int w=mxGetN(prhs[0]);
  int N=h*w;

  myAssert(mxGetM(prhs[1])==h && mxGetN(prhs[1])==w,"Invalid initMask dimensions\n");
  myAssert(mxGetM(prhs[2])==h && mxGetN(prhs[2])==w,"Invalid orient dimensions\n");
  myAssert(mxGetNumberOfElements(prhs[3])==1,"contThresh should be scalar\n");

  bool *edgeMap = (bool*)mxGetData(prhs[0]);
  unsigned char *initMask = (unsigned char*)mxGetData(prhs[1]);
  double *orients = mxGetPr(prhs[2]);
  double contThresh = mxGetScalar(prhs[3]);
  int *learnEdges = (int*)mxGetData(prhs[4]);
  int numLearnEdges = mxGetNumberOfElements(prhs[4]);

  unsigned char *finalMask = (unsigned char*)mxMalloc(N*sizeof(unsigned char));
  memcpy(finalMask,initMask,N*sizeof(unsigned char));

  int roffsets[] = {+1, 0,-1,-1,-1, 0,+1,+1};
  int coffsets[] = {-1,-1,-1, 0,+1,+1,+1, 0};
  int numOffsets = sizeof(roffsets)/sizeof(int);

  int *indexOffsets=(int*)mxMalloc(numOffsets*sizeof(int));
  for(int i=0;i<numOffsets;i++) {
    indexOffsets[i]=coffsets[i]*h+roffsets[i];
  }

  bool *ptrEdges = edgeMap;
  unsigned char *ptrMask = finalMask;
  std::queue<int> bfsQ;
  for(int i=0;i<N;i++,ptrEdges++,ptrMask++){
      if(*ptrMask==128 ||!*ptrEdges || *ptrMask ==255) continue;
      bfsQ.push(i);
  }

  while(!bfsQ.empty()){
    int curIdx = bfsQ.front();
    bfsQ.pop();
    int x = curIdx / h;
    int y = curIdx % h;
    double curOrient = orients[curIdx];
    for(int i=0;i<numOffsets;i++){
      int nbrX = x + coffsets[i];
      int nbrY = y + roffsets[i];
      if(nbrX >= 0 && nbrY >=0 && nbrX < w && nbrY < h){
        int nbrIdx = curIdx + indexOffsets[i];
        if(edgeMap[nbrIdx] && finalMask[nbrIdx]==255){
          double diffAngle = fabs(curOrient-orients[nbrIdx]);
          diffAngle = std::min(diffAngle,180-diffAngle);
          if(diffAngle<contThresh)
          {finalMask[nbrIdx] = 0;bfsQ.push(nbrIdx);}
        }
      }
    }
  }

  plhs[0]=mxCreateNumericMatrix(1,numLearnEdges,mxLOGICAL_CLASS,mxREAL);
  bool *edgeLabels=(bool*)mxGetData(plhs[0]);
  int *ptrEdgeIdx = learnEdges;

  for(int i=0;i<numLearnEdges;i++,edgeLabels++,ptrEdgeIdx++){
    int edgeIdx = *ptrEdgeIdx - 1;
    *edgeLabels = (finalMask[edgeIdx]==255)?true:false;
    myAssert(edgeMap[edgeIdx],"Labeling a non existent edge!\n");
  }

  // --- To free ---
  mxFree(finalMask);
  mxFree(indexOffsets);

}
