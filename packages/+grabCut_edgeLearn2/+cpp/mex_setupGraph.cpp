#include "mex.h"
#include <float.h>
#include <memory.h>
#include <math.h>
#include "matrix.h"
#include <iostream>
#include <vector>

typedef unsigned int uint;

inline void myAssert(bool condition,char *msg){
  if(!condition){
    mexErrMsgTxt(msg);
  }
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters
     C         [ h x w x D] (double)
     edge      [ h x w] (logical)
     roffset   [ K x 1] (int32)
     coffset   [ K x 1] (int32)
     Output:
     lEdges, rEdges [1 x N] (int32), 1 indexed edge indices
  */

  myAssert(nrhs == 4, "4 input arguments expected.");
  if (nlhs != 2)
    mexErrMsgTxt("2 output arguments expected.");

  if(mxGetClassID(prhs[0])!=mxDOUBLE_CLASS) mexErrMsgTxt("prhs[0] (image) shd be of type double\n");
  myAssert(mxGetClassID(prhs[1])==mxLOGICAL_CLASS,"prhs[1] (edge) shd be of type logical\n");
  if(mxGetClassID(prhs[2])!=mxINT32_CLASS) mexErrMsgTxt("prhs[2] (roffset) shd be of type int32\n");
  if(mxGetClassID(prhs[3])!=mxINT32_CLASS) mexErrMsgTxt("prhs[3] (coffset) shd be of type int32\n");

  int numDims=mxGetNumberOfDimensions(prhs[0]);
  
  int h=mxGetDimensions(prhs[0])[0];
  int w=mxGetDimensions(prhs[0])[1];
  int N=h*w;
  int D;
  if(numDims==3)
    D=mxGetDimensions(prhs[0])[2];
  else
    D=1;

  myAssert(mxGetM(prhs[1])==h && mxGetN(prhs[1])==w,"Invalid edge map dimensions\n");

  if(mxGetN(prhs[2])!=1 || mxGetN(prhs[3])!=1)
      mexErrMsgTxt("Invalid size for roffset or coffset\n");

  int K=mxGetM(prhs[2]);
  if(mxGetM(prhs[3])!=K)
    mexErrMsgTxt("Invalid size for roffset or coffset\n");

  bool *edgeMap = (bool*)mxGetData(prhs[1]);
  int *roffsets=(int*)mxGetData(prhs[2]);
  int *coffsets=(int*)mxGetData(prhs[3]);

  double *C=mxGetPr(prhs[0]);

  int *indexOffsets=(int*)mxMalloc(K*sizeof(int));
  for(int i=0;i<K;i++) {
    indexOffsets[i]=coffsets[i]*h+roffsets[i];
  }

  std::vector<int> lEdges;
  std::vector<int> rEdges;

  int x,y;
  bool *ptrEdges = edgeMap;
  for(x=0;x<w;x++){
    for(y=0;y<h;y++,ptrEdges++){
      if(!(*ptrEdges)){continue;}
      int lEdge=y+x*h;
      for(int i=0;i<K;i++){
        double gradient[2]={-1,-1};
        int r[2];
        int c[2];
        int rEdge[2];
        r[0]=y+roffsets[i];
        r[1]=y-roffsets[i];

        if(r[0]>=0 && r[0]<h){
          c[0]=x+coffsets[i];
          if(c[0]>=0 && c[0]<w){
            // compute gradient, if not edge
            int idxOffset = indexOffsets[i];
            if(!*(ptrEdges+idxOffset)){
              rEdge[0]=lEdge+idxOffset;
              double colorW=0;
              for(int d=0;d<D;d++){
                double diff=(C[lEdge+d*N]-C[rEdge[0]+d*N]);
                colorW+=diff*diff;
              }
              gradient[0]=colorW;
            }
          }
        }

        if(r[1]>=0 && r[1]<h){
          c[1]=x-coffsets[i];
          if(c[1]>=0 && c[1]<w){
            // compute gradient, if not edge
            int idxOffset = -indexOffsets[i];
            if(!*(ptrEdges+idxOffset)){
              rEdge[1]=lEdge+idxOffset;
              double colorW=0;
              for(int d=0;d<D;d++){
                double diff=(C[lEdge+d*N]-C[rEdge[1]+d*N]);
                colorW+=diff*diff;
              }
              gradient[1]=colorW;
            }
          }
        }
        int maxIdx = -1;
        maxIdx = (gradient[0]>gradient[1]) ? 0:1;
        if(gradient[maxIdx]!=-1){
          lEdges.push_back(lEdge);
          rEdges.push_back(rEdge[maxIdx]);
        }
      }
    }
  }

  int numEdges=lEdges.size();

  plhs[0]=mxCreateNumericMatrix(1,numEdges,mxINT32_CLASS,mxREAL);
  plhs[1]=mxCreateNumericMatrix(1,numEdges,mxINT32_CLASS,mxREAL);

  int *lEdges_out=(int*)mxGetData(plhs[0]);
  int *rEdges_out=(int*)mxGetData(plhs[1]);

  for(int i=0;i<numEdges;i++){
    lEdges_out[i]=lEdges[i]+1;
    rEdges_out[i]=rEdges[i]+1;
  }

  // --- To free ---
  mxFree(indexOffsets);

}
