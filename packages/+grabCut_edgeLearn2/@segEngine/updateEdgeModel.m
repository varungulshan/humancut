function updateEdgeModel(obj)

opts = obj.opts;
if(opts.enableEdgePosterior & obj.nextIterationNum >= opts.itersBeforeLearning)
  seg = obj.seg;
  
  edgeLabels = false(size(obj.lEdgesLearn));
  trueEdgesMask = seg(obj.lEdgesLearn)~=seg(obj.rEdgesLearn);
  edgeLabels(trueEdgesMask) = true;

  gmmOpts.nMix_yesEdge = opts.gmmNmix_yesEdge;
  gmmOpts.nMix_noEdge = opts.gmmNmix_noEdge;
  [obj.gmmYesEdge,obj.gmmNoEdge] = grabCut_edgeLearn2.initEdgeGmm(obj.edgeFeatures,...
                                   edgeLabels,gmmOpts);

end
