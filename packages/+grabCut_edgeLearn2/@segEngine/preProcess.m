function preProcess(obj,img)
  if(~strcmp(obj.state,'init')),
    error('Cant call preProcess from %s state\n',obj.state);
    return;
  end

  if(~strcmp(class(img),'double')),
    error('img should be of type double\n');
  end

  [obj.cannyEdgeMap,obj.cannyOrient,obj.cannyMagnitude]=getCanny(img,obj.opts);
  obj.cannyMagnitudeBeta = 1/(2*mean(obj.cannyMagnitude(:)));
  obj.cannyMagnitude = exp(-obj.cannyMagnitudeBeta * obj.cannyMagnitude);
  miscFns.saveDebug(obj.debugOpts,obj.cannyEdgeMap,'canny.png');
  preProcess_GC(obj,img);
  obj.features=grabCut_edgeLearn2.extractPixels(img);
  obj.img=img;
  obj.state='pped'; 

function [edgeMap,edgeOrient,edgeMag] = getCanny(img,opts)

[edgeMap,edgeOrient,edgeMag] = getKovesiEdgeMap(img,opts.cannyThresh, ...
                       opts.kovesiSigma,opts.kovesiAutoThresh);

function preProcess_GC(obj,img)

switch(obj.opts.gcNbrType)
  case 'nbr4'
    roffset = int32([ 1,  0 ]);
    coffset = int32([  0, 1 ]);
  case 'nbr8'
    roffset = int32([ 1, 1, 0,-1 ]);
    coffset = int32([ 0, 1, 1, 1 ]);
  otherwise
    error('Invalid nbrhood type %s\n',obj.opts.nbrHoodType);
end

obj.roffset=roffset;
obj.coffset=coffset;

orientIdxMap = binOrientations(obj.cannyOrient,roffset,coffset);
[obj.lEdgesLearn,obj.rEdgesLearn] = ...
    grabCut_edgeLearn2.cpp.mex_getLearningEdges(img,obj.cannyEdgeMap,...
                                               orientIdxMap,roffset',coffset');
[obj.lEdgesCanny,obj.rEdgesCanny] = ...
    grabCut_edgeLearn2.cpp.mex_setupGraph(img,obj.cannyEdgeMap,roffset',coffset');

function idxMap = binOrientations(orient,roffset,coffset)

thetas = -90+atan2(double(coffset),double(roffset))*180/pi; 
% Rotating all thetas by -90 because orients is actually orientation
% along the canny edge (and not perpendicular to the canny edge)
thetas(thetas<0)=thetas(thetas<0)+180; % all angles between [0,180]

thetaCopy = thetas;
thetaCopy(thetas>90) = thetaCopy(thetas>90)-180;
thetaCopy(thetas<=90) = thetaCopy(thetas<=90)+180;
thetaIdx = repmat(int32([1:numel(roffset)]),[1 2]);
thetas = [thetas thetaCopy]';

assert(all(orient(:)>=0 & orient(:)<=180));
deltas = abs(repmat(thetas,[1 numel(orient)])-repmat(orient(:)',[numel(thetas) 1]));
[deltas,idxs] = min(deltas,[],1);

idxMap = reshape(thetaIdx(idxs),[size(orient,1) size(orient,2)]);

function [edgeMap,orient,derivativeMag] = getKovesiEdgeMap(img,thresh,sigma,autoThresh)

img=rgb2gray(img);
[derivativeMag,orient] = canny(img,sigma);

maxMag = max(derivativeMag(:));
derivativeMag = derivativeMag/maxMag;

upperThresh = 0;
if(isnumeric(thresh)), upperThresh = min(1,thresh);
elseif(strcmp(thresh,'auto')), upperThresh = getAutoThresh(derivativeMag,autoThresh);
else error('Invalid threshold for kovesi: %s\n',mat2str(thresh));
end

lowerThresh = 0.4 * upperThresh;
allEdges = nonmaxsup(derivativeMag,orient,1.35);
edgeMap = hysthresh(allEdges,upperThresh,lowerThresh);

function highThresh = getAutoThresh(mag,PercentOfPixelsNotEdges)

[m,n] = size(mag);
%PercentOfPixelsNotEdges = 0.7; % copied from matlab
counts=imhist(mag, 64);
highThresh = find(cumsum(counts) > PercentOfPixelsNotEdges*m*n,...
                  1,'first') / 64;
