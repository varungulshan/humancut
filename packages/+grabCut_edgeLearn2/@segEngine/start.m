function start(obj,labelImg)

if(~strcmp(obj.state,'pped')),
  error('Can only call start if state is pped\n');
end
[h,w]=size(labelImg);

debugOpts = obj.debugOpts;
obj.state='started';
obj.labelImg=labelImg;
[obj.gmmf,obj.gmmb]=grabCut_edgeLearn2.initColorModels(obj.features,labelImg,...
                                            obj.opts.gmmNmix_fg,...
                                            obj.opts.gmmNmix_bg);
[unaryImg,unaryMask]=obj.createUnaryImg();

obj.initEdgeModel();
edgePosteriors = obj.getEdgePosterior();
obj.seg=obj.gcSeg(unaryImg,unaryMask,edgePosteriors);
if(debugOpts.debugLevel>=10)
  tmpImg=obj.drawEdgePosteriors(edgePosteriors);
  miscFns.saveDebug(debugOpts,tmpImg,sprintf('edges_iter%03d.png',obj.nextIterationNum-1));
  tmpImg=grabCut_trained.helpers.overlaySeg(obj.img,obj.seg);
  miscFns.saveDebug(debugOpts,tmpImg,sprintf('seg_iter%03d.jpg',obj.nextIterationNum-1));
  tmpImg=grabCut_trained.helpers.visualize_unary(unaryImg,unaryMask);
  miscFns.saveDebug(debugOpts,tmpImg,sprintf('unary_iter%03d.jpg',obj.nextIterationNum-1));
end

% One iteration is over already
for i=2:obj.opts.numIters
  obj.iterateOnce();
end
