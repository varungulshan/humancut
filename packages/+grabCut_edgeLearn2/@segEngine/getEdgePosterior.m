function edgePosteriors = getEdgePosterior(obj)

if(obj.opts.enableEdgePosterior & obj.nextIterationNum>=obj.opts.itersBeforeLearning)
  yesEdge_likeli = gmm.computeGmm_likelihood(obj.edgeFeatures,obj.gmmYesEdge);
  
  noEdge_likeli = gmm.computeGmm_likelihood(obj.edgeFeatures,obj.gmmNoEdge);
  
  sumLikeli = yesEdge_likeli+noEdge_likeli;
  sumLikeli(sumLikeli==0)=1;
  tmpPosteriors = yesEdge_likeli./sumLikeli;
  posteriorImg = zeros(size(obj.cannyEdgeMap));
  posteriorImg(obj.lEdgesLearn) = tmpPosteriors;
  edgePosteriors = posteriorImg(obj.lEdgesCanny);
elseif(obj.opts.enableSoftEdges)
  edgePosteriors = 1-obj.cannyMagnitude(obj.lEdgesCanny);
else
  edgePosteriors = ones(size(obj.lEdgesCanny));
end
