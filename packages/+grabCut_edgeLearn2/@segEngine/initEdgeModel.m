function initEdgeModel(obj)

opts = obj.opts;
if(opts.enableEdgePosterior)
  labelImg = obj.labelImg;

  % Remove canny edges that lie inside cropped out areas
  mask = (labelImg <=4);
  cannyEdgeMask = mask(obj.lEdgesLearn);
  cannyEdgeMask = cannyEdgeMask | mask(obj.rEdgesLearn);
  obj.lEdgesLearn = obj.lEdgesLearn(cannyEdgeMask);
  obj.rEdgesLearn = obj.rEdgesLearn(cannyEdgeMask);

  cannyEdgeMask = mask(obj.lEdgesCanny);
  cannyEdgeMask = cannyEdgeMask | mask(obj.rEdgesCanny);
  obj.lEdgesCanny = obj.lEdgesCanny(cannyEdgeMask);
  obj.rEdgesCanny = obj.rEdgesCanny(cannyEdgeMask);

  edgeFtrIdx = [obj.lEdgesLearn;obj.rEdgesLearn];
  edgeFtrIdx = sort(edgeFtrIdx,1,'ascend'); % To orient edges in one particular direction

  %edgeFtrIdx = edgeFtrIdx(:);
  %D = size(obj.features,1);
  %obj.edgeFeatures = obj.features(:,edgeFtrIdx);
  %obj.edgeFeatures = reshape(obj.edgeFeatures,[2*size(obj.edgeFeatures,1) ...
                                               %size(obj.edgeFeatures,2)/2]);

  obj.edgeFeatures = grabCut_edgeLearn2.cpp.mex_getEdgeFeatures(...
      obj.features,int32(edgeFtrIdx),int32(obj.opts.edgeFeatureExtend),...
      int32(size(obj.img,1)),int32(size(obj.img,2)));

  if(obj.nextIterationNum >= opts.itersBeforeLearning)
    if(opts.continuityOn)
      mask = 128*ones(size(obj.cannyEdgeMap),'uint8');
      mask(labelImg==1|labelImg==2) = 0;
      mask(labelImg==3|labelImg==4|labelImg==0) = 255;
      edgeLabels = grabCut_edgeLearn2.cpp.mex_getInitLabels(obj.cannyEdgeMap,...
         mask,obj.cannyOrient,opts.continuityThresh,obj.lEdgesLearn);
    else
      edgeLabels = true(size(obj.lEdgesLearn));
      noEdgeMask = (labelImg==2 | labelImg==1);
      noEdgeMask = noEdgeMask(obj.lEdgesLearn);
      edgeLabels(noEdgeMask) = false;
    end
  
    gmmOpts.nMix_yesEdge = opts.gmmNmix_yesEdge;
    gmmOpts.nMix_noEdge = opts.gmmNmix_noEdge;
    [obj.gmmYesEdge,obj.gmmNoEdge] = grabCut_edgeLearn2.initEdgeGmm(obj.edgeFeatures,...
        edgeLabels,gmmOpts);

    if(obj.debugOpts.debugLevel>=10)
      tmpEdgeImg = zeros(size(obj.cannyEdgeMap),'uint8');
      tmpEdgeImg(obj.lEdgesLearn(edgeLabels)) = 255;
      tmpEdgeImg(obj.lEdgesLearn(~edgeLabels)) = 128;
      miscFns.saveDebug(obj.debugOpts,tmpEdgeImg,'initEdgeLabels.png');
    end
  else
    obj.gmmYesEdge = [];
    obj.gmmNoEdge = [];
  end

end
