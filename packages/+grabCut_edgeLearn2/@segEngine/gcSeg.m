function seg=gcSeg(obj,unaryImg,unaryMask,edgePosteriors)

import grabCut_edgeLearn2.cpp.*

objOpts=obj.opts;

opts.gcGamma=objOpts.gcGamma;
opts.gcScale=objOpts.gcScale;
opts.xoffset=obj.coffset';
opts.yoffset=obj.roffset';

if(isempty(obj.graphInfo))
  [seg,flow,graphInfo]=mex_dgcBand2D_init(unaryMask,unaryImg,obj.img,opts,...
                                          obj.lEdgesCanny,obj.rEdgesCanny, ...
                                          edgePosteriors);
  obj.graphInfo=graphInfo;
else
  [seg,flow]=mex_dgcBand2D_repeat(unaryMask,unaryImg,obj.graphInfo,opts,...
                                  obj.lEdgesCanny,obj.rEdgesCanny,...
                                  edgePosteriors);
end
obj.nextIterationNum = obj.nextIterationNum+1;
