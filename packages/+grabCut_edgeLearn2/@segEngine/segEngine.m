% Copyright (C) 2010 Varun Gulshan
% This class implements grabCut_edgeLearn2 segmentation
classdef segEngine < handle
  properties (SetAccess=private, GetAccess=public)
    state % string = 'init','pped','started'
    opts % object of type grabCut_edgeLearn2.segOpts
    img % double img, b/w [0,1]
    posteriorImage % data terms for graph cut, only if debugLevel > 0
    debugOpts % 
    seg % uint8, current segmentation
    labelImg % saves the annotation provided, as it gets used across iterations

    roffset % internal variables for graphcut
    coffset % internal variables for graphcut
    beta % internal variable for graphCut

    gmmf % internal variable for color model
    gmmb % internal variable for color model

    gmmNoEdge % internal variable for edge model
    gmmYesEdge % internal variable for edge model

    features % internal variable
    edgeFeatures
    graphInfo % internal variable
    cannyEdgeMap % internal variable, type = logical
    cannyOrient % orientations of edges
    cannyMagnitude % magnitude of edge
    cannyMagnitudeBeta % beta computed from magnitude
    lEdgesCanny % 1 indexed
    rEdgesCanny % 1 indexed
    lEdgesLearn % 1 indexed
    rEdgesLearn % 1 indexed
    
    nextIterationNum % gets updated in gcSeg function
  end

  methods
    function obj=segEngine(debugOpts,segOpts)
      obj.debugOpts=debugOpts;
      assert(strcmp(class(segOpts),'grabCut_edgeLearn2.segOpts'));
      obj.opts=segOpts;
      obj.state='init';
      obj.seg=[];
      obj.img=[];
      obj.posteriorImage=[];
      obj.graphInfo=[];
      obj.cannyEdgeMap = [];
      obj.cannyOrient = [];
      obj.lEdgesCanny = [];
      obj.rEdgesCanny = [];
      obj.lEdgesLearn = [];
      obj.rEdgesLearn = [];
      obj.gmmNoEdge = [];
      obj.gmmYesEdge = [];
      obj.edgeFeatures = [];
      obj.nextIterationNum = 0;
    end
    preProcess(obj,img) % Call this function first to preprocess image if needed
                        % img should be double, between [0,1]
    start(obj,labelImg) %  Run grabCut_edgeLearn2 given the initial labelImg
    % The number of iterations is given in opts, labelImg is encoded as follows:
    % labelImg=0 is empty
    % labelImg=1 is FG (hard constrained)
    % labelImg=2 is BG (hard contrained)
    % labelImg=3 is FG (soft, used only in first iteration to initialize color model)
    % labelImg=4 is BG (soft, used only in first iteration to initialize color model)
    % labelImg=5 is FG (hard constrained, but not used for color models)
    % labelImg=6 is BG (hard constrained, but not used for color models)
    iterateOnce(obj) % Performs one more iteration of grabCut_edge, can only be called after 
                     % start has been called
    delete(obj) % Destructor to clear graph cuts
  end

  methods (Access=private)
    [unaryImg,unaryMask]=createUnaryImg(obj) % Uses the stored gmm models and labelImg
                         % to compute unaries, also stores information that can be used
                         % to update color models for next iteration
    seg=gcSeg(obj,unaryImg,unaryMask,edgePosteriors)
    initEdgeModel(obj) % Initializes the edge features and edge gmm's
    updateEdgeModel(obj) % Initializes the edge features and edge gmm's
    edgePosteriors=getEdgePosterior(obj) % Returns edge classifier posterior
    img=drawEdgePosteriors(obj,edgePosteriors) % Returns edge classifier posterior
  end
end
