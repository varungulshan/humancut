function [gmmf,gmmb]=updateColorModels(features,seg,gmmf,gmmb,updateIters,mask)

mask=mask(:);
tmpFeatures=features(:,seg(:)==255 & mask);
gmmf=gmm.updateGmm(tmpFeatures,gmmf,updateIters);

tmpFeatures=features(:,seg(:)==0 & mask);
gmmb=gmm.updateGmm(tmpFeatures,gmmb,updateIters);
