function [gmmYes,gmmNo]=initEdgeGmm(features,edgeLabels,gmmOpts)

assert(strcmp(class(edgeLabels),'logical'),'EdgeLabels should be of class logical\n');

tmpFeatures=features(:,edgeLabels);
gmmYes=gmm.init_gmmBS(tmpFeatures,gmmOpts.nMix_yesEdge);

tmpFeatures=features(:,~edgeLabels);
gmmNo=gmm.init_gmmBS(tmpFeatures,gmmOpts.nMix_noEdge);
