classdef flowPropagatorGC < videoSegmenter
  properties (SetAccess=private, GetAccess=public)
    gamma
    debugLevel
    flowDir
    gtFile % gt for first frame
    vH
    gtSeg_firstFrame
    h
    w
    nCh
    nFrames
    state % 'initialized','pped','segStarted'
    seg % uint8, seg variable for the entire volume
    frameNum
    framesSegmented
    video % only if debugLevel > 0
    gcOpts % contains sigma_c, gamma, unaryConstant, gcScale, nbrHoodType
    beta
    roffset %int32
    coffset %int32
  end

  methods
    function obj=flowPropagatorGC(debugLevel,flowDir,gtFile,vH,gcOpts)
      obj.debugLevel=debugLevel;
      obj.flowDir=flowDir;
      obj.gtFile=gtFile; 
      obj.vH=vH;
      [obj.h obj.w obj.nCh obj.nFrames]=obj.vH.videoSize();
      obj.state='initialized';
      obj.seg=zeros([obj.h obj.w obj.nFrames],'uint8');
      obj.framesSegmented=0;
      obj.frameNum=1;
      obj.video=[];
      obj.gcOpts=gcOpts;
      beta=[];
    end
  end

  methods (Access=private)
    flowVector=getNextFlow(obj)
    %beta=getBetaAuto_GC(obj)
    preProcess_GC(obj)
    seg=graphCutSeg(obj,flowSeg,labels)
  end

end % of classdef
