function opts=makeOpts(optsString)

switch(optsString)
   case 'iter10_rad150_2'
    opts=localGrabCut3D.segOpts();
    opts.gcGamma_e=50;
    opts.gcGamma_i=0;
    opts.gcGammaT_e=50;
    opts.gcGammaT_i=0;

    opts.gcScale=500;
    opts.gcNbrType_spatial='nbr8';
    opts.gcNbrType_time='nbr1';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.min_validFraction=0.02;
    opts.localWin_rad=150;


   case 'iter10_rad80_2'
    opts=localGrabCut3D.segOpts();
    opts.gcGamma_e=50;
    opts.gcGamma_i=0;
    opts.gcGammaT_e=50;
    opts.gcGammaT_i=0;

    opts.gcScale=500;
    opts.gcNbrType_spatial='nbr8';
    opts.gcNbrType_time='nbr1';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.min_validFraction=0.02;
    opts.localWin_rad=80;

  otherwise
    error('Invalid options string %s\n',optsString);
end
