function opts=makeOpts_withStitching(optsString)
% Make options for grabCut3D with stitching

switch(optsString)
   case '21frames_iter10_2_rad80'
     opts.tSlice=22;
     opts.opts_string='iter10_rad80_2';
   case '21frames_iter10_2_rad150'
     opts.tSlice=22;
     opts.opts_string='iter10_rad150_2';
  otherwise
    error('Invalid options string %s\n',optsString);
end
