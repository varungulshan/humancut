% Copyright (C) 2010 Varun Gulshan
% This class implements localgrabCut segmentation of volumes
classdef segEngine < handle
  properties (SetAccess=private, GetAccess=public)
    state % string = 'init','pped','started'
    opts % object of type localGrabCut3D.segOpts
    img % uint8 img volume [h x w x nCh x nFrames]
    debugLevel % =0 no debugging, >0 different levels of debuggin
    seg % uint8 [h x w x nFrames], volume segmentation
    labelImg % uint8 saves the annotation provided, as it gets used across iterations

    xoffset % internal variables for graphcut
    yoffset % internal variables for graphcut
    zoffset % internal variables for graphcut

    beta % internal variable for graphCut
    beta_t % internal variable for graphCut

  end

  methods
    function obj=segEngine(debugLevel,segOpts)
      obj.debugLevel=debugLevel;
      obj.opts=segOpts;
      obj.state='init';
      obj.seg=[];
      obj.img=[];
    end

    preProcess(obj,img) % Call this function first to preprocess image if needed
                        % img should be uint8
    start(obj,labelImg) %  Run grabCut given the initial labelImg
    % The number of iterations is given in opts, labelImg is encoded as follows:
    % labelImg=0 is empty
    % labelImg=1 is FG (hard constrained)
    % labelImg=2 is BG (hard contrained)
    % labelImg=3 is FG (soft, used only in first iteration to initialize color model)
    % labelImg=4 is BG (soft, used only in first iteration to initialize color model)
    % labelImg=5 is FG (hard constrained, but not used for color models)
    % labelImg=6 is BG (hard constrained, but not used for color models)
    iterateOnce(obj) % Performs one more iteration of grabCut, can only be called after 
                     % start has been called

  end

  methods (Access=private)
    seg=gcSeg(obj,unaryImg,unaryMask)
    [unaryImg,unaryMask]=createUnaryImg(obj);
  end

  methods (Static=true)
  end
end
