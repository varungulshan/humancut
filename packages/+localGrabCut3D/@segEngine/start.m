function start(obj,labelImg)

if(~strcmp(obj.state,'pped')),
  error('Can only call start if state is pped\n');
end
[h,w,nFrames]=size(labelImg);

obj.state='started';
obj.labelImg=labelImg;

seg=128*ones(size(labelImg),'uint8');
seg(labelImg==1 | labelImg==3 | labelImg==5)=255;
seg(labelImg==2 | labelImg==4 | labelImg==6)=0;
obj.seg=seg;

[unaryImg,unaryMask]=obj.createUnaryImg();
obj.seg=obj.gcSeg(unaryImg,unaryMask);

% One iteration is over already
for i=2:obj.opts.numIters
  obj.iterateOnce();
end
