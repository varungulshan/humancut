function seg=gcSeg(obj,unaryImg,unaryMask)

import localGrabCut3D.cpp.*

objOpts=obj.opts;

opts.gcGamma_e=objOpts.gcGamma_e;
opts.gcGamma_i=objOpts.gcGamma_i;
opts.gcGammaT_e=objOpts.gcGammaT_e;
opts.gcGammaT_i=objOpts.gcGammaT_i;
opts.gcScale=objOpts.gcScale;
opts.beta=obj.beta;
opts.beta_t=obj.beta_t;
opts.xoffset=obj.xoffset';
opts.yoffset=obj.yoffset';
opts.zoffset=obj.zoffset';

[seg,flow]=mex_gcBand3D(unaryMask,unaryImg,obj.img,opts);
