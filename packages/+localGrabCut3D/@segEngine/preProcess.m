function preProcess(obj,img)
  if(~strcmp(obj.state,'init')),
    error('Cant call preProcess from %s state\n',obj.state);
    return;
  end

  if(~strcmp(class(img),'uint8')),
    error('img should be of type uint8\n');
  end

  preProcess_GC(obj,img);
  %obj.features=grabCut.extractPixels(img);
  obj.img=img;
  obj.state='pped'; 

function preProcess_GC(obj,img)

switch(obj.opts.gcNbrType_spatial)
  case 'nbr4'
    yoffset = int32([ 1,  0 ]);
    xoffset = int32([  0, 1 ]);
  case 'nbr8'
    yoffset = int32([ 1, 1, 0,-1 ]);
    xoffset = int32([ 0, 1, 1, 1 ]);
  otherwise
    error('Invalid nbrhood type %s\n',obj.opts.nbrHoodType);
end

switch(obj.opts.gcNbrType_time)
  case 'nbr1'
    yoffset_all = [yoffset 0];
    xoffset_all = [xoffset 0];
    zoffset_all = [zeros([1 length(yoffset)],'int32') 1];
  otherwise
    error('Invalid nbrhood type %s\n',obj.opts.nbrHoodType);
end


obj.xoffset=xoffset_all;
obj.yoffset=yoffset_all;
obj.zoffset=zoffset_all;

if(isnumeric(obj.opts.gcSigma_c))
  D=size(img,3);
  obj.beta=1/(2*D*obj.opts.gcSigma_c^2);
elseif(strcmp(obj.opts.gcSigma_c,'auto'))
  [lEdges,rEdges,colorWeights,spWeights]=localGrabCut3D.cpp.mex_setupTransductionGraph(...
                                         double(img(:,:,:,1)),yoffset',xoffset');
  obj.beta=1/(2*mean(colorWeights));
end
obj.beta_t=obj.beta; % Currently just using the same beta in both spatial and temporal
