function test()
% Code to test localGrabCut3D on a small sequence

cwd=miscFns.extractDirPath(mfilename('fullpath'));

videoPath=[cwd '../../data/snapCut_videos/footballer_cut2/'];
videoName='00001.png';
boxDir=[cwd '../../data/misc/footballer_cut2boxes/'];

vH=myVideoReader(videoPath,videoName);

nFrames=vH.nFrames;
nChannels=size(vH.curFrame,3);
labelImg=zeros([vH.h vH.w nFrames],'uint8');
img=zeros([vH.h vH.w nChannels nFrames],'uint8');

for i=1:nFrames
  labelFile=[boxDir sprintf('%05d.png',i)];
  labelImg(:,:,i)=imread(labelFile);
  img(:,:,:,i)=vH.curFrame;
  if(i~=nFrames)
    vH.moveForward();
  end
end

segOpts=localGrabCut3D.helpers.makeOpts('iter10_rad80_2');
segH=localGrabCut3D.segEngine(1,segOpts);
segH.preProcess(img);
segH.start(labelImg);

seg=segH.seg;
delete(vH);
delete(segH);

figure;
for i=1:nFrames
  segOverlay=humanCut.overlaySeg(im2double(img(:,:,:,i)),seg(:,:,i),2);
  imshow(segOverlay);
  title(sprintf('Segmentation at Frame #%03d',i));
  pause(0.5);
end

