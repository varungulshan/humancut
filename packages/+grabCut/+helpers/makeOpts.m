function opts=makeOpts(optsString)

switch(optsString)
   case 'bmvc_bestOpts'
    opts=grabCut.segOpts();
    opts.gcGamma_e=25;
    opts.gcGamma_i=0;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';
    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.gmmUpdate_iters=1;
    opts.postProcess=0;
    opts.numIters=10;

   case 'iter10'
    opts=grabCut.segOpts();
    opts.gcGamma_e=140;
    opts.gcGamma_i=0;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;
  
  case 'iterQuick'
    opts=grabCut.segOpts();
    opts.gcGamma_e=50;
    opts.gcGamma_i=0;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=1;
    opts.gmmUpdate_iters=1;

   case 'iter10_3'
    opts=grabCut.segOpts();
    opts.gcGamma_e=50;
    opts.gcGamma_i=0;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

     
   case 'iter10_2'
    opts=grabCut.segOpts();
    opts.gcGamma_e=50;
    opts.gcGamma_i=0;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

   case 'iter10_ising'
    opts=grabCut.segOpts();
    opts.gcGamma_e=0;
    opts.gcGamma_i=25;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;
   
  otherwise
    error('Invalid options string %s\n',optsString);
end
