This package implements iterated color models. Right now a new graph cut instance is created every time, but that can be replaced potentially by dynamic graph cuts.
