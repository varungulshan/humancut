function iterateOnce(obj)
% Run one iteration of grabCut
% first update color model, then update segmentation

if(~strcmp(obj.state,'started')),
  error('Can only call iterateOnce, once state is started\n');
end

labelImg=obj.labelImg;
ftrMask=(labelImg~=5 & labelImg~=6);
[obj.gmmf,obj.gmmb]=grabCut.updateColorModels(obj.features,obj.seg,obj.gmmf,obj.gmmb,...
                    obj.opts.gmmUpdate_iters,ftrMask);
[unaryImg,unaryMask]=obj.createUnaryImg();
obj.seg=obj.gcSeg(unaryImg,unaryMask);

if(obj.debugOpts.debugLevel>=10)
  tmpImg=grabCut_trained.helpers.visualize_unary(unaryImg,unaryMask);
  miscFns.saveDebug(obj.debugOpts,tmpImg,sprintf('unary_iter%03d.jpg',obj.nextIterationNum-1));
  tmpImg=grabCut_trained.helpers.overlaySeg(obj.img,obj.seg);
  miscFns.saveDebug(obj.debugOpts,tmpImg,sprintf('seg_iter%03d.jpg',obj.nextIterationNum-1));
end
