function delete(obj)
% Destructor function, needs to clear the dynamic graph cut handle
if(~isempty(obj.graphInfo))
  grabCut.cpp.mex_dgcBand2D_cleanUp(obj.graphInfo);
  obj.graphInfo=[];
end
