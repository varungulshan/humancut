function start(obj,labelImg)

if(~strcmp(obj.state,'pped')),
  error('Can only call start if state is pped\n');
end
[h,w]=size(labelImg);

obj.state='started';
obj.labelImg=labelImg;
% TO CALCULATE OBJ.FEATURES
[obj.gmmf,obj.gmmb]=grabCut.initColorModels(obj.features,labelImg,obj.opts.gmmNmix_fg,...
                                            obj.opts.gmmNmix_bg);
[unaryImg,unaryMask]=obj.createUnaryImg();
obj.seg=obj.gcSeg(unaryImg,unaryMask);

if(obj.debugOpts.debugLevel>=10)
  tmpImg=grabCut_trained.helpers.visualize_unary(unaryImg,unaryMask);
  miscFns.saveDebug(obj.debugOpts,tmpImg,sprintf('unary_iter%03d.jpg',obj.nextIterationNum-1));
  tmpImg=grabCut_trained.helpers.overlaySeg(obj.img,obj.seg);
  miscFns.saveDebug(obj.debugOpts,tmpImg,sprintf('seg_iter%03d.jpg',obj.nextIterationNum-1));
end

% One iteration is over already
for i=2:obj.opts.numIters
  obj.iterateOnce();
end
