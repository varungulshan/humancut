function opts=makeOpts(optsString)

switch(optsString)
   case 'iter10_2'
    opts=grabCut3D_flow.segOpts();
    opts.gcGamma_e=50;
    opts.gcGamma_i=0;
    opts.gcGammaT_e=50;
    opts.gcGammaT_i=0;

    opts.gcScale=500;
    opts.gcNbrType_spatial='nbr8';
    opts.gcNbrType_time='nbr1';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;


   case 'iter10_2_noTemporal'
    opts=grabCut3D_flow.segOpts();
    opts.gcGamma_e=50;
    opts.gcGamma_i=0;
    opts.gcGammaT_e=0; % No temporal term!
    opts.gcGammaT_i=0;

    opts.gcScale=500;
    opts.gcNbrType_spatial='nbr8';
    opts.gcNbrType_time='nbr1';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

  otherwise
    error('Invalid options string %s\n',optsString);
end
