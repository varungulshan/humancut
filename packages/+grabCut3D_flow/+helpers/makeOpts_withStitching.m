function opts=makeOpts_withStitching(optsString,videoName)
% Make options for grabCut3D with stitching

switch(optsString)
   case '21frames_iter10_2_stickmanFlow'
     opts.tSlice=22;
     opts.grabCutOpts_string='iter10_2';
     opts.flowDir='/data2/varun/stickmanFlow/';
   case '21frames_iter10_2'
     opts.tSlice=22;
     opts.grabCutOpts_string='iter10_2';
     opts.flowDir='/data2/varun/broxFlow/';
   case '21frames_iter10_2_noTemp'
     opts.tSlice=22;
     opts.grabCutOpts_string='iter10_2_noTemporal';
     opts.flowDir='/data2/varun/broxFlow/';
  otherwise
    error('Invalid options string %s\n',optsString);
end

opts.flowDir=[opts.flowDir videoName '/'];
