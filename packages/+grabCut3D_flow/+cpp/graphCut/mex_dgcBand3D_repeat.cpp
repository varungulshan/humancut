#include "mex.h"
#include "graph.h"
#include <cmath>
#include <iostream>
#include <climits>
#include <vector>

using namespace std;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters combinations
     rhs[0] = mask [h x w x nFrames] (255 for fg, 0 for bg, 
              128 for where to run graph cut) [uint8]
     rhs[1] = unary terms [h x w x 2 x nFrames] (double) first channel for label=1 unaries
              second channel for label=2 unaries
     rhs[2] = graphInfo structure with following fields:
       dgcHandle -> handle to graph
       fgUnaryEdits_offset
       fgUnaryEdits_value
       bgUnaryEdits_offset
       bgUnaryEdits_value
     rhs[3] = opts structure with following fields:
      gcGamma_e -> double scalar
      gcGamma_i -> double scalar
      gcGammaT_e -> double scalar
      gcGammaT_i -> double scalar
      gcScale -> double scalar
      beta -> double scalar
      beta_t -> double scalar
      xoffset -> [ L x 1 ] int32 offsets
      yoffset -> [ L x 1 ] int32 offsets
      zoffset -> [ L x 1 ] int32 offsets

     lhs[0] -> returns the seg labels of all nodes (array of type hxwxnFrames uint8)
     lhs[1] -> returns the flow

     Notes on memory consumption:
     This code allocated following sized arrays (only big ones noted here):
     let nPix=h*w*nFrames;
     let nEdges=number of edges in graph (roughly 5*nPix for 8 neighbourhood in space
     and 1 nbrhood in time)
     idxMap -> [h x w x nFrames] (int32)
     graph structure (kolmogorov) -> 58 * nEdges + 44 * nPix
     (In practice nPix will be less because this code removes pixels which are
     hard constrained!)
   */

  if (nrhs != 4)
    mexErrMsgTxt("4 inputs required");

  if(mxGetClassID(prhs[0])!=mxUINT8_CLASS)
    mexErrMsgTxt("prhs[0] (mask) should be of type uint8\n");
  if(mxGetClassID(prhs[1])!=mxDOUBLE_CLASS)
    mexErrMsgTxt("prhs[1] (unaryImg) should be of type double\n");
  if(!mxIsStruct(prhs[2]))
    mexErrMsgTxt("prhs[2] (graphInfo) should be a structure\n");
  if(!mxIsStruct(prhs[3]))
    mexErrMsgTxt("prhs[3] (opts) should be a structure\n");

  typedef Graph<int,int,int> GraphInt;
  int h=mxGetDimensions(prhs[0])[0];
  int w=mxGetDimensions(prhs[0])[1];
  int frameRes=h*w;
  int nFrames,nOffsets,nPix;
  double gcScale;
  if(mxGetNumberOfDimensions(prhs[0])<3){nFrames=1;}
  else{nFrames=mxGetDimensions(prhs[0])[2];}
  nPix=frameRes*nFrames;

  // --- Check dimensions of prhs[1] -----
  mxAssert(mxGetNumberOfDimensions(prhs[1])>=3,
          "Incorrect number of dimensions for prhs[1]\n");

  if(mxGetDimensions(prhs[1])[0]!=h || mxGetDimensions(prhs[1])[1]!=w 
    || mxGetDimensions(prhs[1])[2]!=2) {
      mexErrMsgTxt("prhs[1] (unary img) incorrect dimensions\n");
  }
  if(nFrames>1){
    if(mxGetNumberOfDimensions(prhs[1])<4)
      mexErrMsgTxt("Incorrect number of dimensions for prhs[1]\n");
    if(nFrames!=mxGetDimensions(prhs[1])[3])
      mexErrMsgTxt("Incorrect nFrames for prhs[1]\n");
  }

  // --- Check prhs[2] ------
  mxArray *tmp;
  int *fgUnaryEdits_offset,*bgUnaryEdits_offset;
  double *fgUnaryEdits_value,*bgUnaryEdits_value;
  int numFgEdits,numBgEdits;
  GraphInt *g;
  int numBytes_handle=sizeof(GraphInt*);

  tmp=mxGetField(prhs[2],0,"dgcHandle");
  mxAssert(tmp!=NULL,"dgcHandle field not found\n");
  memcpy(&g,mxGetData(tmp),numBytes_handle);

  tmp=mxGetField(prhs[2],0,"fgUnaryEdits_offset");
  mxAssert(tmp!=NULL,"fgUnaryEdits_offset field not found\n");
  fgUnaryEdits_offset=(int*)mxGetData(tmp);
  numFgEdits=mxGetNumberOfElements(tmp);

  tmp=mxGetField(prhs[2],0,"fgUnaryEdits_value");
  mxAssert(tmp!=NULL,"fgUnaryEdits_value field not found\n");
  fgUnaryEdits_value=mxGetPr(tmp);
  mxAssert(numFgEdits=mxGetNumberOfElements(tmp),"Invalid dimensions\n");

  tmp=mxGetField(prhs[2],0,"bgUnaryEdits_offset");
  mxAssert(tmp!=NULL,"bgUnaryEdits_offset field not found\n");
  bgUnaryEdits_offset=(int*)mxGetData(tmp);
  numBgEdits=mxGetNumberOfElements(tmp);

  tmp=mxGetField(prhs[2],0,"bgUnaryEdits_value");
  mxAssert(tmp!=NULL,"bgUnaryEdits_value field not found\n");
  bgUnaryEdits_value=mxGetPr(tmp);
  mxAssert(numBgEdits=mxGetNumberOfElements(tmp),"Invalid dimensions\n");

  // --- Check and initialize from the options structure -----

  tmp=mxGetField(prhs[3],0,"gcScale");
  mxAssert(tmp!=NULL,"gcScale field not found\n");
  gcScale=*mxGetPr(tmp);

  // ---- Data verified ok, now set up all the pointers

  unsigned char *mask=(unsigned char*)mxGetData(prhs[0]);
  double *unaryImg=mxGetPr(prhs[1]);
  double *fgL=unaryImg;
  double *bgL=fgL+frameRes;

  int *idxMap=(int*)mxMalloc(nPix*sizeof(int));

  // -- scan the mask to index the pixels ----
  unsigned char *it_mask=mask;
  int *it_idxMap=idxMap;
  int idx=0;
  for(int i=0;i<nPix;i++,it_mask++,it_idxMap++){
    if(*it_mask==128){*it_idxMap=idx;idx++;}
    else{*it_idxMap=-1;}
  }

  // -- alter unaries as given in fg and bg unary edits ---------
  for(int i=0;i<numFgEdits;i++){
    int offset=fgUnaryEdits_offset[i];
    fgL[offset]+=fgUnaryEdits_value[i];
  }
  for(int i=0;i<numBgEdits;i++){
    int offset=bgUnaryEdits_offset[i];
    bgL[offset]+=bgUnaryEdits_value[i];
  }

  it_mask=mask;
  it_idxMap=idxMap;
  int intMax=(1<<30);

  for(int z=0;z<nFrames;z++){
    for(int i=0;i<frameRes;i++,it_mask++,it_idxMap++){
      if(*it_mask==128){
        int curIdx=*it_idxMap;
        double fgLikeli=fgL[z*frameRes*2+i];
        double bgLikeli=bgL[z*frameRes*2+i];
        double diff=gcScale*(fgLikeli-bgLikeli);
        double dbl_intMax=(double)(intMax);
        double dbl_intMin=(double)(-intMax);
        int fgUnary;
        if(diff>dbl_intMax){fgUnary=intMax;}
        else if(diff<dbl_intMin){fgUnary=-intMax;}
        else{fgUnary=(int)diff;}
        g->edit_tweights(curIdx,0,fgUnary);
        g->mark_node(curIdx);
      }
    }
  }

  int flow=g->maxflow(true);

  // --- Now prepare the output ------------
  mxAssert(nlhs>=2,"Atleast two outputs required\n");
  int dims[3];dims[0]=h;dims[1]=w;dims[2]=nFrames;

  plhs[0]=mxCreateNumericArray(3,dims,mxUINT8_CLASS,mxREAL);
  plhs[1]=mxCreateDoubleScalar((double)flow);

  unsigned char *it_seg=(unsigned char*)mxGetData(plhs[0]);
  it_mask=mask;
  it_idxMap=idxMap;
  for(int i=0;i<nPix;i++,it_seg++,it_mask++,it_idxMap++){
    if(*it_mask==128){
      int curIdx=*it_idxMap;
      *it_seg=(g->what_segment(curIdx)==GraphInt::SOURCE?255:0); // 255 for source, 0 for sink
    }
    else{*it_seg=*it_mask;}
  }

  // ------- To free -------------
  mxFree(idxMap);
  //deleteGraph<int,int,int>(&g);
}
