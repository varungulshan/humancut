function seg=run3D_withStiching(streamCallbackFn,opts)
% Function which makes multiple calls to grabCut3D.segEngine to segment
% an entire video
% streamCallbackFn is a function handle that returns the image
% and labelImg 

if(opts.tSlice<2)
  warning('tSlice cannot be set to < 2, enforcing that now\n');
end
tSlice=max(2,opts.tSlice);

done=false;
numLoaded=0;
img=uint8([]);
labelImg=uint8([]);
seg=[];
idxStart=1;

segOpts=grabCut3D_flow.helpers.makeOpts(opts.grabCutOpts_string);

while(~done)
% Assert: numLoaded contains number of already loaded frames
% in the variables img and labelImg
  while(numLoaded < tSlice)
    [tmpImg,tmpLabels]=streamCallbackFn();
    if(isempty(tmpImg)||isempty(tmpLabels))
      done=true;
      break;
    else
      img(:,:,:,numLoaded+1)=tmpImg;
      labelImg(:,:,numLoaded+1)=tmpLabels;
      numLoaded=numLoaded+1;
    end
  end
% Assert: The block to be processed next is loaded onto
% img and labelImg now
  segH=grabCut3D_flow.segEngine(1,segOpts,opts.flowDir,[idxStart idxStart+numLoaded-1]);
  segH.preProcess(img);
  segH.start(labelImg);
  seg(:,:,idxStart:(idxStart+numLoaded-1))=segH.seg;
  delete(segH);
  % Update the variables to follow invariants for stiching
  lastSeg=seg(:,:,idxStart+numLoaded-1);
  idxStart=idxStart+numLoaded-1;
  img=img(:,:,:,end);
  labelImg=labelImg(:,:,end);
  labelImg(lastSeg==255)=5;
  labelImg(lastSeg==0)=6;
  numLoaded=1;
end
