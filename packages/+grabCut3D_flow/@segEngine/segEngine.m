% Copyright (C) 2010 Varun Gulshan
% This class implements grabCut segmentation of volumes
classdef segEngine < handle
  properties (SetAccess=private, GetAccess=public)
    state % string = 'init','pped','started'
    opts % object of type grabCut3D.segOpts
    img % uint8 img volume [h x w x nCh x nFrames]
    debugLevel % =0 no debugging, >0 different levels of debuggin
    seg % uint8 [h x w x nFrames], volume segmentation
    labelImg % uint8 saves the annotation provided, as it gets used across iterations

    xoffset % internal variables for graphcut
    yoffset % internal variables for graphcut

    beta % internal variable for graphCut
    beta_t % internal variable for graphCut

    gmmf % internal variable for color model
    gmmb % internal variable for color model

    graphInfo % state of graph (for dynamic graph cuts)

    flowDir
    idx_flowStart
    idx_flowEnd

  end

  methods
    function obj=segEngine(debugLevel,segOpts,flowDir,flowIdxs)
      obj.debugLevel=debugLevel;
      obj.opts=segOpts;
      obj.state='init';
      obj.seg=[];
      obj.img=[];
      obj.graphInfo=[];
      obj.flowDir=flowDir;
      obj.idx_flowStart=flowIdxs(1);
      obj.idx_flowEnd=flowIdxs(2);
    end

    preProcess(obj,img) % Call this function first to preprocess image if needed
                        % img should be uint8
    start(obj,labelImg) %  Run grabCut given the initial labelImg
    % The number of iterations is given in opts, labelImg is encoded as follows:
    % labelImg=0 is empty
    % labelImg=1 is FG (hard constrained)
    % labelImg=2 is BG (hard contrained)
    % labelImg=3 is FG (soft, used only in first iteration to initialize color model)
    % labelImg=4 is BG (soft, used only in first iteration to initialize color model)
    % labelImg=5 is FG (hard constrained, but not used for color models)
    % labelImg=6 is BG (hard constrained, but not used for color models)
    iterateOnce(obj) % Performs one more iteration of grabCut, can only be called after 
                     % start has been called

    function delete(obj) % Cleans up graph handle
      if(~isempty(obj.graphInfo))
        grabCut3D_flow.cpp.mex_dgcBand3D_cleanUp(obj.graphInfo);
      end
    end % of destructor
  end

  methods (Access=private)
    seg=gcSeg(obj,unaryImg,unaryMask)
  end

  methods (Static=true)
    [unaryImg,unaryMask]=createUnaryImg(img,gmmf,gmmb,labelImg,gcScale,...
                         uniform_value,uniform_gamma) 
  end
end
