function seg=segmentVolume(obj)
% Does graph cut on volume, stiching volumes if necessary
% the gmms' are stored in obj.gmmf and obj.gmmb

opts=obj.opts;
nFrames=size(obj.img,4);
h=size(obj.img,1);
w=size(obj.img,2);

stTime=clock;

unaryImg=zeros([h w 2 nFrames]);
unaryMask=zeros([h w nFrames],'uint8');
if(obj.debugLevel>0)
  whosTmp=whos('unaryImg');
  miscFns.printMemory(whosTmp,'unaryImg');
  whosTmp=whos('unaryMask');
  miscFns.printMemory(whosTmp,'unaryMask');
end

for j=1:nFrames
  [tmp_unaryImg,tmp_unaryMask]=obj.createUnaryImg(double(obj.img(:,:,:,j)),obj.gmmf{j},...
                               obj.gmmb{j},obj.labelImg(:,:,j),opts.gcScale);
  unaryImg(:,:,:,j)=tmp_unaryImg;
  unaryMask(:,:,j)=tmp_unaryMask;
end

seg=obj.gcSeg(unaryImg,unaryMask);

if(obj.debugLevel>0)
  fprintf('Time taken to segment volume (1 iteration) = %.2f seconds\n',etime(clock,stTime));
end
