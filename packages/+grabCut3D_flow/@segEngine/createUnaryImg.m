function [unaryImg,unaryMask]=createUnaryImg(img,gmmf,gmmb,labelImg,gcScale,...
                                             uniform_value,uniform_gamma)

if(~strcmp(class(img),'double')),
  error('img should be of type double\n');
end

if(~exist('uniform_value','var'))
  uniform_value=1/(255^3);
end

if(~exist('uniform_gamma','var'))
  uniform_gamma=0; % no mixing of uniform density function
end

features=grabCut3D_flow.extractPixels(img);

labelImg(labelImg==5)=1; % For this function, these labels are equivalent
labelImg(labelImg==6)=2;

ftrMask=(labelImg~=1 & labelImg~=2);
features=features(:,ftrMask(:));

fgLikeli=gmm.computeGmm_likelihood(features,gmmf);
bgLikeli=gmm.computeGmm_likelihood(features,gmmb);

fgLikeli=uniform_gamma*uniform_value+(1-uniform_gamma)*fgLikeli;
bgLikeli=uniform_gamma*uniform_value+(1-uniform_gamma)*bgLikeli;

fgLikeli=-log(fgLikeli);
bgLikeli=-log(bgLikeli);
myUB=realmax/(gcScale*100);
fgLikeli(fgLikeli>myUB)=myUB;
bgLikeli(bgLikeli>myUB)=myUB;

[h w]=size(labelImg);
unaryImg=zeros([h w 2]);
tmp=zeros([h w]);tmp(ftrMask)=fgLikeli;
unaryImg(:,:,1)=tmp;
tmp=zeros([h w]);tmp(ftrMask)=bgLikeli;
unaryImg(:,:,2)=tmp;

unaryMask=128*ones([h w],'uint8');
unaryMask(labelImg==1)=255;
unaryMask(labelImg==2)=0;
