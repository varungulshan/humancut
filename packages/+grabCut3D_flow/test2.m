function test2()
% Code to test grabCut3D_flow with stiching on a small sequence

cwd=miscFns.extractDirPath(mfilename('fullpath'));

videoPath=[cwd '../../data/snapCut_videos/footballer_cut2/'];
videoName='00001.png';
boxDir=[cwd '../../data/misc/footballer_cut2boxes/'];
flowDir=[cwd '../../results/broxFlow/footballer_cut2/'];

vH=myVideoReader(videoPath,videoName);

nFrames=vH.nFrames;
nChannels=size(vH.curFrame,3);

curIdx=1;

function [img,labels]=streamImg()
  if(curIdx<=nFrames)
    img=vH.curFrame;
    labelFile=[boxDir sprintf('%05d.png',curIdx)];
    labels=imread(labelFile);
    if(curIdx<nFrames)
      vH.moveForward();
    end
    curIdx=curIdx+1;
  else
    img=[];labels=[];
  end
end % of enclosed function

callbackFn_handle=@streamImg;
opts.tSlice=2;
opts.grabCutOpts_string='iter10_2';
opts.flowDir=flowDir;
seg=grabCut3D_flow.run3D_withStiching(callbackFn_handle,opts);
delete(vH);

vH=myVideoReader(videoPath,videoName);
nFrames=vH.nFrames;
nChannels=size(vH.curFrame,3);
img=zeros([vH.h vH.w nChannels nFrames],'uint8');

for i=1:nFrames
  img(:,:,:,i)=vH.curFrame;
  if(i~=nFrames)
    vH.moveForward();
  end
end

figure;

for i=1:nFrames
  segOverlay=humanCut.overlaySeg(im2double(img(:,:,:,i)),seg(:,:,i),2);
  imshow(segOverlay);
  %imshow(seg(:,:,i));
  title(sprintf('Segmentation at Frame #%03d',i));
  pause(0.5);
end

end % of function test2
