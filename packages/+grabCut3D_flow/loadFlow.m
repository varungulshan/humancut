function [xflow,yflow]=loadFlow(flowDir,idx)

loadFile=sprintf('%sflow_%05d.mat',flowDir,idx);
tmpLoad=load(loadFile);
xflow=tmpLoad.flow(:,:,1);
yflow=tmpLoad.flow(:,:,2);
