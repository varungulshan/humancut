function [gmmf,gmmb]=initColorModels(img,labelImg,gmmNmix_fg,gmmNmix_bg)

nFrames=size(img,4);
gmmf=cell(nFrames,1);
gmmb=cell(nFrames,1);

for i=1:nFrames
  tmpImg=double(img(:,:,:,i));
  tmpLabels=labelImg(:,:,i);

  tmpLabels(tmpLabels==3)=1; % Soft labels are used for initializing color models also
  tmpLabels(tmpLabels==4)=2;
  
  features=grabCut3D_flow.extractPixels(tmpImg);
  tmpFeatures=features(:,tmpLabels(:)==1);
  gmmf{i}=gmm.init_gmmBS(tmpFeatures,gmmNmix_fg);
  
  tmpFeatures=features(:,tmpLabels(:)==2);
  gmmb{i}=gmm.init_gmmBS(tmpFeatures,gmmNmix_bg);
end
