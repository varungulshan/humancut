function opts=makeOpts(optsString)

switch(optsString)
   case 'iter10'
    opts=grabCut_edgeLearn.segOpts();
    opts.gcGamma=140;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;
    opts.cannyThresh = 'auto';
    opts.cannyMethod = 'matlab';
    opts.enableEdgePosterior = false;

   case 'iterQuick'
    opts=grabCut_edgeLearn.segOpts();
    opts.gcGamma=50;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=1;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.cannyMethod = 'matlab';
    opts.enableEdgePosterior = false;

   case 'iter10_2'
    opts=grabCut_edgeLearn.segOpts();
    opts.gcGamma=50;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.cannyMethod = 'matlab';
    opts.enableEdgePosterior = false;

   case 'iter10_3_edgeLearn'
    opts=grabCut_edgeLearn.segOpts();
    opts.gcGamma=25;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.cannyMethod = 'matlab';
    opts.enableEdgePosterior = true;

    opts.gmmNmix_yesEdge = 6;
    opts.gmmNmix_noEdge = 8;


   case 'iter10_2_edgeLearn'
    opts=grabCut_edgeLearn.segOpts();
    opts.gcGamma=50;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.cannyMethod = 'matlab';
    opts.enableEdgePosterior = true;

    opts.gmmNmix_yesEdge = 6;
    opts.gmmNmix_noEdge = 8;

   case 'iter10_ising'
    opts=grabCut_edgeLearn.segOpts();
    opts.gcGamma=25;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 1;
    opts.cannyMethod = 'matlab';
    opts.enableEdgePosterior = false;

   case 'iterQuickKovesi'
    opts=grabCut_edgeLearn.segOpts();
    opts.gcGamma=50;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=1;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.cannyMethod = 'kovesi';
    opts.kovesiSigma = 1;
    opts.kovesiAutoThresh = 0.7;
    opts.enableEdgePosterior = false;
  
   case 'iter10_3_kovesi'
    opts=grabCut_edgeLearn.segOpts();
    opts.gcGamma=25;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.cannyMethod = 'kovesi';
    opts.kovesiSigma = 1;
    opts.kovesiAutoThresh = 0.7;
    opts.enableEdgePosterior = false;

   case 'iter10_2_kovesi'
    opts=grabCut_edgeLearn.segOpts();
    opts.gcGamma=50;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.cannyMethod = 'kovesi';
    opts.kovesiSigma = 1;
    opts.kovesiAutoThresh = 0.7;
    opts.enableEdgePosterior = false;

   case 'iter10_3_edgeLearn_kovesi'
    opts=grabCut_edgeLearn.segOpts();
    opts.gcGamma=25;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.cannyMethod = 'kovesi';
    opts.enableEdgePosterior = true;
    opts.kovesiAutoThresh = 0.7;
    opts.kovesiSigma = 1;

    opts.gmmNmix_yesEdge = 6;
    opts.gmmNmix_noEdge = 8;

   case 'iter10_2_edgeLearn_kovesi'
    opts=grabCut_edgeLearn.segOpts();
    opts.gcGamma=50;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.cannyMethod = 'kovesi';
    opts.kovesiAutoThresh = 0.7;
    opts.enableEdgePosterior = true;

    opts.gmmNmix_yesEdge = 6;
    opts.gmmNmix_noEdge = 8;
    opts.kovesiSigma = 1;

  otherwise
    error('Invalid options string %s\n',optsString);
end
