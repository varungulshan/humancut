function [gmmYes,gmmNo]=initEdgeGmm(features,features_flip,edgeLabels,gmmOpts)

assert(strcmp(class(edgeLabels),'logical'),'EdgeLabels should be of class logical\n');

tmpFeatures=[features(:,edgeLabels) features_flip(:,edgeLabels)];
gmmYes=gmm.init_gmmBS(tmpFeatures,gmmOpts.nMix_yesEdge);

tmpFeatures=[features(:,~edgeLabels) features_flip(:,~edgeLabels)];
gmmNo=gmm.init_gmmBS(tmpFeatures,gmmOpts.nMix_noEdge);
