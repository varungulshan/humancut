function preProcess(obj,img)
  if(~strcmp(obj.state,'init')),
    error('Cant call preProcess from %s state\n',obj.state);
    return;
  end

  if(~strcmp(class(img),'double')),
    error('img should be of type double\n');
  end

  obj.cannyEdgeMap=getCanny(img,obj.opts);
  miscFns.saveDebug(obj.debugOpts,obj.cannyEdgeMap,'canny.png');
  preProcess_GC(obj,img);
  obj.features=grabCut_edgeLearn.extractPixels(img);
  obj.img=img;
  obj.state='pped'; 

function edgeMap = getCanny(img,opts)

if(strcmp(opts.cannyMethod,'matlab')),
  if(isnumeric(opts.cannyThresh))
    edgeMap = edge(rgb2gray(img),'canny',opts.cannyThresh);
  elseif(strcmp(opts.cannyThresh,'auto'))
    edgeMap = edge(rgb2gray(img),'canny');
  else
    error('Invalid threshold for canny: %s\n',mat2str(opts.cannyThresh));
  end
elseif(strcmp(opts.cannyMethod,'kovesi')),
  edgeMap = getKovesiEdgeMap(img,opts.cannyThresh,opts.kovesiSigma,opts.kovesiAutoThresh);
else
  error('Invalid canny method: %s\n',opts.cannyMethod);
end

function preProcess_GC(obj,img)

switch(obj.opts.gcNbrType)
  case 'nbr4'
    roffset = int32([ 1,  0 ]);
    coffset = int32([  0, 1 ]);
  case 'nbr8'
    roffset = int32([ 1, 1, 0,-1 ]);
    coffset = int32([ 0, 1, 1, 1 ]);
  otherwise
    error('Invalid nbrhood type %s\n',obj.opts.nbrHoodType);
end

obj.roffset=roffset;
obj.coffset=coffset;

[obj.lEdgesCanny,obj.rEdgesCanny] = ...
    grabCut_edgeLearn.cpp.mex_setupGraph(img,obj.cannyEdgeMap,roffset',coffset');

function edgeMap = getKovesiEdgeMap(img,thresh,sigma,autoThresh)

img=rgb2gray(img);
[derivativeMag,or] = canny(img,sigma);

maxMag = max(derivativeMag(:));
derivativeMag = derivativeMag/maxMag;

upperThresh = 0;
if(isnumeric(thresh)), upperThresh = min(1,thresh);
elseif(strcmp(thresh,'auto')), upperThresh = getAutoThresh(derivativeMag,autoThresh);
else error('Invalid threshold for kovesi: %s\n',mat2str(thresh));
end

lowerThresh = 0.4 * upperThresh;
allEdges = nonmaxsup(derivativeMag,or,1.35);
edgeMap = hysthresh(allEdges,upperThresh,lowerThresh);

function highThresh = getAutoThresh(mag,PercentOfPixelsNotEdges)

[m,n] = size(mag);
%PercentOfPixelsNotEdges = 0.7; % copied from matlab
counts=imhist(mag, 64);
highThresh = find(cumsum(counts) > PercentOfPixelsNotEdges*m*n,...
                  1,'first') / 64;
