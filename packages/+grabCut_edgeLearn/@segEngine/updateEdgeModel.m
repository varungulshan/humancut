function updateEdgeModel(obj)

opts = obj.opts;
if(opts.enableEdgePosterior)
  seg = obj.seg;
  
  edgeLabels = false(size(obj.lEdgesCanny));
  trueEdgesMask = seg(obj.lEdgesCanny)~=seg(obj.rEdgesCanny);
  edgeLabels(trueEdgesMask) = true;

  gmmOpts.nMix_yesEdge = opts.gmmNmix_yesEdge;
  gmmOpts.nMix_noEdge = opts.gmmNmix_noEdge;
  [obj.gmmYesEdge,obj.gmmNoEdge] = grabCut_edgeLearn.initEdgeGmm(obj.edgeFeatures,...
      obj.edgeFeatures_flip,edgeLabels,gmmOpts);

end
