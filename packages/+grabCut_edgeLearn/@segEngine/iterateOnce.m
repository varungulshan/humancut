function iterateOnce(obj)
% Run one iteration of grabCut_edgeLearn
% first update color model, then update segmentation

if(~strcmp(obj.state,'started')),
  error('Can only call iterateOnce, once state is started\n');
end

debugOpts = obj.debugOpts;
labelImg=obj.labelImg;
ftrMask=(labelImg~=5 & labelImg~=6);
[obj.gmmf,obj.gmmb]=grabCut_edgeLearn.updateColorModels(obj.features,obj.seg,...
                    obj.gmmf,obj.gmmb,...
                    obj.opts.gmmUpdate_iters,ftrMask);
[unaryImg,unaryMask]=obj.createUnaryImg();

obj.updateEdgeModel();
edgePosteriors = obj.getEdgePosterior();

obj.seg=obj.gcSeg(unaryImg,unaryMask,edgePosteriors);

if(debugOpts.debugLevel>=10)
  if(obj.opts.enableEdgePosterior)
    tmpImg=obj.drawEdgePosteriors(edgePosteriors);
    miscFns.saveDebug(debugOpts,tmpImg,sprintf('edges_iter%03d.png',obj.nextIterationNum-1));
  end
  tmpImg=grabCut_trained.helpers.overlaySeg(obj.img,obj.seg);
  miscFns.saveDebug(debugOpts,tmpImg,sprintf('seg_iter%03d.jpg',obj.nextIterationNum-1));
  tmpImg=grabCut_trained.helpers.visualize_unary(unaryImg,unaryMask);
  miscFns.saveDebug(debugOpts,tmpImg,sprintf('unary_iter%03d.jpg',obj.nextIterationNum-1));
end
