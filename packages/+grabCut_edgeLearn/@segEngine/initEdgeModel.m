function initEdgeModel(obj)

opts = obj.opts;
if(opts.enableEdgePosterior)
  labelImg = obj.labelImg;

  % Remove canny edges that lie inside cropped out areas
  mask = (labelImg <=4);
  cannyEdgeMask = mask(obj.lEdgesCanny);
  cannyEdgeMask = cannyEdgeMask | mask(obj.rEdgesCanny);
  obj.lEdgesCanny = obj.lEdgesCanny(cannyEdgeMask);
  obj.rEdgesCanny = obj.rEdgesCanny(cannyEdgeMask);

  edgeFtrIdx = [obj.lEdgesCanny;obj.rEdgesCanny];
  edgeFtrIdx = edgeFtrIdx(:);

  D = size(obj.features,1);
  obj.edgeFeatures = obj.features(:,edgeFtrIdx);
  obj.edgeFeatures = reshape(obj.edgeFeatures,[2*size(obj.edgeFeatures,1) ...
                                               size(obj.edgeFeatures,2)/2]);
  obj.edgeFeatures_flip = zeros(size(obj.edgeFeatures));
  obj.edgeFeatures_flip(1:D,:) = obj.edgeFeatures((D+1):2*D,:);
  obj.edgeFeatures_flip((D+1):2*D,:) = obj.edgeFeatures(1:D,:);

  edgeLabels = true(size(obj.lEdgesCanny));
  noEdgeMask = (labelImg==2 | labelImg==1);
  noEdgeMask = noEdgeMask(obj.lEdgesCanny);
  edgeLabels(noEdgeMask) = false;

  gmmOpts.nMix_yesEdge = opts.gmmNmix_yesEdge;
  gmmOpts.nMix_noEdge = opts.gmmNmix_noEdge;
  [obj.gmmYesEdge,obj.gmmNoEdge] = grabCut_edgeLearn.initEdgeGmm(obj.edgeFeatures,...
      obj.edgeFeatures_flip,edgeLabels,gmmOpts);

end
