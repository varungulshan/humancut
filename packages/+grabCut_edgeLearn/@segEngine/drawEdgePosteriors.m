function img=drawEdgePosteriors(obj,edgePosteriors)

img=zeros([size(obj.img,1) size(obj.img,2)]);
img(obj.lEdgesCanny) = edgePosteriors;
