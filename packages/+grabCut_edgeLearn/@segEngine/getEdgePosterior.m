function edgePosteriors = getEdgePosterior(obj)

if(obj.opts.enableEdgePosterior)
  yesEdge_likeli = gmm.computeGmm_likelihood(obj.edgeFeatures,obj.gmmYesEdge);
  yesEdge_likeli = yesEdge_likeli + ...
                  gmm.computeGmm_likelihood(obj.edgeFeatures_flip,obj.gmmYesEdge);
  
  noEdge_likeli = gmm.computeGmm_likelihood(obj.edgeFeatures,obj.gmmNoEdge);
  noEdge_likeli = noEdge_likeli + ...
                  gmm.computeGmm_likelihood(obj.edgeFeatures_flip,obj.gmmNoEdge);
  
  sumLikeli = yesEdge_likeli+noEdge_likeli;
  sumLikeli(sumLikeli==0)=1;
  edgePosteriors = yesEdge_likeli./sumLikeli;
else
  edgePosteriors = ones(size(obj.lEdgesCanny));
end
