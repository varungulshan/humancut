function view=view_1(obj)

edgeMap=obj.edgeMap_orig;
edgeMap=im2double(255*uint8(edgeMap));
view=edgeMap;

fprintf('Showing original edge map\n');
