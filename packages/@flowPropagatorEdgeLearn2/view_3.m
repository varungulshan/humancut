function view=view_3(obj)

frameNum=obj.frameNum;
if(frameNum==1),
  warning('No flow vector in backward direction for frame 1\n');
  view=zeros([size(obj.video,1) size(obj.video,2)]);
  return;
end

flowFile=[obj.flowDir sprintf('backward-%04d.mat',frameNum)];
if(~exist(flowFile,'file')),
  error('No flow file %s exists\n',flowFile);
end
tmpLoad=load(flowFile);
flowVec=tmpLoad.flowVec;
[h w nCh]=size(flowVec);
if(h~=obj.h | w~=obj.w | nCh~=2),
  error('Improper dimesnions in flow file: %s\n',flowFile);
end

view=flowToColor(flowVec);
colorWheel=makeColorWheel(25);
[h w nCh]=size(view);
[hWheel wWheel nCh_wheel]=size(colorWheel);
colorWheel=colorWheel(1:min(h,hWheel),1:min(w,wWheel),:);
[hWheel wWheel nCh_wheel]=size(colorWheel);
view(1:hWheel,w-wWheel+1:w,:)=colorWheel;

fprintf('Showing backward flow from frame %d to frame %d\n',frameNum,frameNum-1);
