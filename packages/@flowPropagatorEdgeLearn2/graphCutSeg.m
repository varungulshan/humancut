function seg=graphCutSeg(obj,flowSeg,labels)

curImg=im2double(obj.vH.curFrame);
[h w nCh]=size(curImg);
n=h*w;

unaries=zeros([2 n],'int32');
flowSeg=flowSeg(:);
bgSeg=(flowSeg==0);
fgSeg=(flowSeg==255);
const=int32(obj.gcOpts.unaryConstant*obj.gcOpts.gcScale);
unaries(2,bgSeg)=const;
unaries(1,fgSeg)=const;

infConst=int32(intmax-const-1);
bgHard=(labels(:)==2);
fgHard=(labels(:)==1);
unaries(2,bgHard)=infConst;
unaries(1,fgHard)=infConst;

gamma=obj.gcOpts.gamma;
gamma_ising=obj.gcOpts.gamma_ising;

edgeMap=obj.edgeMap_learnt;

stTime=clock;
[cut,flow]=mex_graphCut_edgeMap(uint32(n),gamma,gamma_ising,unaries,curImg,obj.roffset',...
                        obj.coffset',obj.gcOpts.gcScale,edgeMap);
seg=255*uint8(reshape(~cut,h,w));
if(obj.debugLevel>0),
  fprintf('GC for frame num %d completed in %.3f seconds\n',obj.frameNum,etime(clock,stTime));
end;
%keyboard;
