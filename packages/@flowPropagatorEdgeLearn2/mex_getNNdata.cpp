#include "mex.h"
#include <float.h>
#include <memory.h>
#include <math.h>
#include "matrix.h"
#include <iostream>
#include <vector>

typedef unsigned int uint;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters
     rhs[0]= seg    [ h x w] (uint8)
     rhs[1]= img    [ h x w x 3] (double)
     rhs[2]= patchRad  [ 1 x 1] (uint32)
     rhs[3]= edgeMap [ h x w ] (logical)
     rhs[4]= roffset (int32)
     rhs[5]= coffset (int32)

     Output:
     nnData,nnLabels
  */

  if (nrhs != 6)
    mexErrMsgTxt("6 input arguments expected.");
  if (nlhs != 2)
    mexErrMsgTxt("2 output arguments expected.");

  if(mxGetClassID(prhs[0])!=mxUINT8_CLASS) mexErrMsgTxt("prhs[0] shd be of type uint8\n");
  if(mxGetClassID(prhs[1])!=mxDOUBLE_CLASS) mexErrMsgTxt("prhs[1] shd be of type double\n");
  if(mxGetClassID(prhs[2])!=mxUINT32_CLASS) mexErrMsgTxt("prhs[2] shd be of type uint32\n");
  if(mxGetClassID(prhs[3])!=mxLOGICAL_CLASS) mexErrMsgTxt("prhs[3] shd be of type logical\n");
  if(mxGetClassID(prhs[4])!=mxINT32_CLASS) mexErrMsgTxt("prhs[4] shd be of type int32\n");
  if(mxGetClassID(prhs[5])!=mxINT32_CLASS) mexErrMsgTxt("prhs[5] shd be of type int32\n");

  int numDims=mxGetNumberOfDimensions(prhs[1]);
  
  int h=mxGetDimensions(prhs[1])[0];
  int w=mxGetDimensions(prhs[1])[1];
  int N=h*w;
  int D;
  if(numDims==3)
    D=mxGetDimensions(prhs[1])[2];
  else
    D=1;

  if(mxGetM(prhs[0])!=h || mxGetN(prhs[0])!=w)
    mexErrMsgTxt("Invalid size for prhs[0]\n");
  if(mxGetM(prhs[3])!=h || mxGetN(prhs[3])!=w)
    mexErrMsgTxt("Invalid size for prhs[3]\n");

  if(mxGetM(prhs[2])!=1 || mxGetN(prhs[2])!=1)
      mexErrMsgTxt("Invalid size for prhs[2]\n");

  int K=mxGetM(prhs[4]);
  if(mxGetM(prhs[5])!=K)
    mexErrMsgTxt("Invalid size for roffset or coffset\n");

  int *roffsets=(int*)mxGetData(prhs[4]);
  int *coffsets=(int*)mxGetData(prhs[5]);

  double *C=mxGetPr(prhs[1]);
  bool *edgeLabels=(bool*)mxMalloc(N*sizeof(bool));
  memset(edgeLabels,0,N*sizeof(bool));
  char *seg=(char*)mxGetData(prhs[0]); // 1 byte storage
  bool *edgeMap=(bool*)mxGetData(prhs[3]);

  int *indexOffsets=(int*)mxMalloc(K*sizeof(int));
  int validEdges=0;
  int patchRad=*(unsigned int*)mxGetData(prhs[2]);

  for(int i=0;i<K;i++) {
    indexOffsets[i]=coffsets[i]*h+roffsets[i];
  }

  int x,y;
  int wLim=w-patchRad;
  int hLim=h-patchRad;
  for(x=patchRad;x<wLim;x++){
    for(y=patchRad;y<hLim;y++){
      int lEdge=y+x*h;
      if(!edgeMap[lEdge]){continue;}
      validEdges++;
      bool edgeLabel=false;
      char lEdge_seg=seg[lEdge];
      for(int i=0;i<K;i++){
        int r=y+roffsets[i];
        if(r>=0 && r<h){
          int c=x+coffsets[i];
          if(c>=0 && c<w){
            int rEdge=lEdge+indexOffsets[i];
            if(seg[rEdge]!=lEdge_seg){edgeLabel=true;i=K;}
          }
        }
      }
      edgeLabels[lEdge]=edgeLabel;
    }
  }

  int patchPixels=(2*patchRad+1)*(2*patchRad+1);
  int dPatch=patchPixels*D;

  int patchOffsets[patchPixels];
  int *iter_pchOffset=patchOffsets;
  for(x=-patchRad;x<=patchRad;x++){
    for(y=-patchRad;y<=patchRad;y++){
      *iter_pchOffset=x*h+y;
      iter_pchOffset++;
    }
  }
  int dims[2]={1,validEdges};

  plhs[0]=mxCreateDoubleMatrix(dPatch,validEdges,mxREAL);
  plhs[1]=mxCreateNumericArray(2,dims,mxLOGICAL_CLASS,mxREAL);

  double *nnData=mxGetPr(plhs[0]);
  bool *nnLabels=(bool*)mxGetData(plhs[1]);

  double *iter_nnData=nnData;
  bool *iter_nnLabels=nnLabels;
  for(x=patchRad;x<wLim;x++){
    for(y=patchRad;y<hLim;y++){
      int lEdge=y+x*h;
      if(!edgeMap[lEdge]){continue;}
      *iter_nnLabels=edgeLabels[lEdge];
      iter_nnLabels++;
      for(int d=0;d<D;d++){
        double *imgChannel=C+d*N;
        int *iter_pchOffset=patchOffsets;
        for(int k=0;k<patchPixels;k++){
          int pchPixel=lEdge+(*iter_pchOffset);
          iter_pchOffset++;
          *iter_nnData=imgChannel[pchPixel];
          iter_nnData++;
        }
      }
    }
  }

  // --- To free ---
  mxFree(indexOffsets);
  mxFree(edgeLabels);
}
