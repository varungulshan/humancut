classdef flowPropagatorEdgeLearn2 < videoSegmenter
% Uses a pre-processed edge map for graph cut edges
% The pre-processed edge map is thresholded at edgeThresh to give a 
% binary edge map
% Does a classification of the binary edge map based on nearest neighbour
% of the edge map in the previous frame
  properties (SetAccess=private, GetAccess=public)
    gamma
    debugLevel
    flowDir
    edgeDir
    gtFile % gt for first frame
    vH
    gtSeg_firstFrame
    h
    w
    nCh
    nFrames
    state % 'initialized','pped','segStarted'
    seg % uint8, seg variable for the entire volume
    frameNum
    framesSegmented
    video % only if debugLevel > 0
    gcOpts % contains edgeThresh, gamma, gamma_ising, unaryConstant, gcScale, nbrHoodType
    roffset %int32
    coffset %int32
    edgeMap_orig % edgeMap of the last segmented frame as given by Pb detector
    edgeMap_learnt % edgeMap of the last segmented frame after NN search
    segPropagated
  end

  methods
    function obj=flowPropagatorEdgeLearn2(debugLevel,flowDir,edgeDir,gtFile,vH,gcOpts)
      obj.debugLevel=debugLevel;
      obj.flowDir=flowDir;
      obj.edgeDir=edgeDir;
      obj.gtFile=gtFile; 
      obj.vH=vH;
      [obj.h obj.w obj.nCh obj.nFrames]=obj.vH.videoSize();
      obj.state='initialized';
      obj.seg=zeros([obj.h obj.w obj.nFrames],'uint8');
      obj.framesSegmented=0;
      obj.frameNum=1;
      obj.video=[];
      obj.gcOpts=gcOpts;
      beta=[];
    end
    view=view_1(obj) % shows the Pb detector output
    view=view_2(obj) % shows Pb detector after NN classification
    view=view_3(obj) % visualizes flow to the previous frame
    view=view_4(obj) % view the propagated segmentation
  end

  methods (Access=private)
    flowVector=getNextFlow(obj)
    %beta=getBetaAuto_GC(obj)
    preProcess_GC(obj)
    seg=graphCutSeg(obj,flowSeg,labels)
    edgeMap_learnt=classifyEdges_NN(obj,curSeg,curFrame,edgeMap_prev,nxtFrame,edgeMap_nxt);
  end

  %methods (Static)
  %end

end % of classdef
