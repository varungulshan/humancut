function updateSeg(obj,labelImg,frameNum)

error('Update seg not yet supported!!\n');
if(obj.frameNum~=frameNum)
  warning('Seems like bug in segmented, ui frame number and segmenter frame number differ\n');
end;

obj.seg(:,:,frameNum)=obj.graphCutSeg(obj.seg(:,:,frameNum),labelImg(:,:,frameNum));
obj.framesSegmented=obj.frameNum;

%fprintf('Nothing to update in flowPropagatorGT,user interaction is ignored\n');
