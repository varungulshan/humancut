function ok=moveForward(obj,labelImg)

ok=false;

if(~strcmp(obj.state,'segStarted')),
  warning('Cant move unless segmentation is started\n');
end

if(obj.frameNum==obj.nFrames)
  warning('Cant go beyond nFrames\n');
  ok=false;
  return;
end

if(obj.frameNum<obj.framesSegmented),
  obj.frameNum=obj.frameNum+1;
  obj.vH.moveForward();
  ok=true;
  return;
end

if(obj.frameNum>obj.framesSegmented),
  ok=false;
  warning('Bug in flow propagator, frameNum>framesSegmented\n');
  return;
end

curSeg=obj.seg(:,:,obj.frameNum);
flowVector=obj.getNextFlow();
nxtSeg=propagateSeg(curSeg,flowVector);
obj.segPropagated=nxtSeg;

%nxtSeg(nxtSeg==128)=0; % All unknown pixels are assigned to bg right now

obj.frameNum=obj.frameNum+1;
edgeMap_prev=obj.edgeMap_orig;

edgeThresh=obj.gcOpts.edgeThresh;
edgeFile=[obj.edgeDir sprintf('%04d.mat',obj.frameNum)];
if(~exist(edgeFile,'file')),
  error('Could not find edge file %s\n',edgeFile);
end
tmpLoad=load(edgeFile);
obj.edgeMap_orig=(tmpLoad.pb_thin>edgeThresh);

curFrame=obj.vH.curFrame;
obj.vH.moveForward();
nxtFrame=obj.vH.curFrame;
obj.edgeMap_learnt=obj.classifyEdges_NN(curSeg,curFrame,edgeMap_prev,...
                                        nxtFrame,obj.edgeMap_orig);

obj.seg(:,:,obj.frameNum)=obj.graphCutSeg(nxtSeg,labelImg(:,:,obj.frameNum));
obj.framesSegmented=obj.framesSegmented+1;
ok=true;
