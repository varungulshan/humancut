function ok=start(obj,labels,frameNum)
ok=true;
if(~strcmp(obj.state,'pped')),
  ok=false;
  warning('Cant start segmentation from %s state\n',obj.state);
  return;
end

if(frameNum~=1),
  warning('Cant start flowPropagator from %d frame number\n',frameNum);
  ok=false;
  return;
end

obj.frameNum=1;
edgeThresh=obj.gcOpts.edgeThresh;
edgeFile=[obj.edgeDir sprintf('%04d.mat',obj.frameNum)];
if(~exist(edgeFile,'file')),
  error('Could not find edge file %s\n',edgeFile);
end
tmpLoad=load(edgeFile);
obj.edgeMap_orig=(tmpLoad.pb_thin>edgeThresh);
obj.edgeMap_learnt=obj.edgeMap_orig;

obj.seg(:,:,1)=obj.graphCutSeg(obj.gtSeg_firstFrame,labels(:,:,1));
%obj.seg(:,:,1)=obj.gtSeg_firstFrame;
obj.framesSegmented=1;
obj.state='segStarted';
