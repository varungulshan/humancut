function edgeMap_learnt=classifyEdges_NN(obj,curSeg,curFrame,edgeMap_prev,nxtFrame,edgeMap_nxt)

patchRad=uint32(1); % patchSize=2*patchRad+1
stTime=clock;

roffset=obj.roffset';
coffset=obj.coffset';

roffset=[roffset;-roffset];
coffset=[coffset;-coffset];

[nnData,nnLabels]=mex_getNNdata(curSeg,im2double(curFrame), patchRad, ...
                  edgeMap_prev,roffset,coffset);

[srchData,origIdx]=mex_getNNdata_srch(im2double(nxtFrame),patchRad,edgeMap_nxt);
srchLabels=logical(zeros(size(srchData,2),1));

if(obj.debugLevel>0)
  fprintf('Extracted data for NN search (%d dimensional) in %.3f seconds\n',...
          size(srchData,1),etime(clock,stTime));
end

stTime=clock;
[kd,D]=vgg_kdtree2_build(nnData);

for i=1:size(srchData,2)
  nrstIdx=vgg_kdtree2_closest_point(kd,D,srchData(:,i));
  nrstIdx=kd.orig_indices(nrstIdx);
  srchLabels(i)=nnLabels(nrstIdx);
end

edgeMap_learnt=logical(zeros(size(curSeg)));
edgeMap_learnt(origIdx)=srchLabels;
if(obj.debugLevel>0)
  fprintf('NN classification took %.3f seconds\n',etime(clock,stTime));
end
