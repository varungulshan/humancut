function view=view_2(obj)

edgeMap=obj.edgeMap_learnt;
edgeMap=im2double(255*uint8(edgeMap));
view=edgeMap;

fprintf('Showing edgemap after NN classfication\n');
