#include "mex.h"
#include <float.h>
#include <memory.h>
#include <math.h>
#include "matrix.h"
#include <iostream>
#include <vector>

typedef unsigned int uint;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters
     rhs[0]= img    [ h x w x 3] (double)
     rhs[1]= patchRad  [ 1 x 1] (uint32)
     rhs[2]= edgeMap [ h x w ] (logical)

     Output:
     lhs[0]=srchData [D x validEdges ] (double)
     lhs[1]=origIdx (1 based index) [1 x validEdges] (int32)
  */

  if (nrhs != 3)
    mexErrMsgTxt("3 input arguments expected.");
  if (nlhs != 2)
    mexErrMsgTxt("2 output arguments expected.");

  if(mxGetClassID(prhs[0])!=mxDOUBLE_CLASS) mexErrMsgTxt("prhs[0] shd be of type double\n");
  if(mxGetClassID(prhs[1])!=mxUINT32_CLASS) mexErrMsgTxt("prhs[1] shd be of type uint32\n");
  if(mxGetClassID(prhs[2])!=mxLOGICAL_CLASS) mexErrMsgTxt("prhs[2] shd be of type logical\n");

  int numDims=mxGetNumberOfDimensions(prhs[0]);
  
  int h=mxGetDimensions(prhs[0])[0];
  int w=mxGetDimensions(prhs[0])[1];
  int N=h*w;
  int D;
  if(numDims==3)
    D=mxGetDimensions(prhs[0])[2];
  else
    D=1;

  if(mxGetM(prhs[2])!=h || mxGetN(prhs[2])!=w)
    mexErrMsgTxt("Invalid size for prhs[2]\n");

  if(mxGetM(prhs[1])!=1 || mxGetN(prhs[1])!=1)
      mexErrMsgTxt("Invalid size for prhs[1]\n");


  double *C=mxGetPr(prhs[0]);
  bool *edgeMap=(bool*)mxGetData(prhs[2]);

  int validEdges=0;
  int patchRad=*(unsigned int*)mxGetData(prhs[1]);

  int x,y;
  int wLim=w-patchRad;
  int hLim=h-patchRad;
  for(x=patchRad;x<wLim;x++){
    for(y=patchRad;y<hLim;y++){
      int lEdge=y+x*h;
      if(!edgeMap[lEdge]){continue;}
      validEdges++;
    }
  }

  int patchPixels=(2*patchRad+1)*(2*patchRad+1);
  int dPatch=patchPixels*D;

  int patchOffsets[patchPixels];
  int *iter_pchOffset=patchOffsets;
  for(x=-patchRad;x<=patchRad;x++){
    for(y=-patchRad;y<=patchRad;y++){
      *iter_pchOffset=x*h+y;
      iter_pchOffset++;
    }
  }
  int dims[2]={1,validEdges};

  plhs[0]=mxCreateDoubleMatrix(dPatch,validEdges,mxREAL);
  plhs[1]=mxCreateNumericArray(2,dims,mxINT32_CLASS,mxREAL);

  double *srchData=mxGetPr(plhs[0]);
  int *origIdx=(int*)mxGetData(plhs[1]);

  double *iter_srchData=srchData;
  int *iter_origIdx=origIdx;
  for(x=patchRad;x<wLim;x++){
    for(y=patchRad;y<hLim;y++){
      int lEdge=y+x*h;
      if(!edgeMap[lEdge]){continue;}
      *iter_origIdx=lEdge+1;
      iter_origIdx++;
      for(int d=0;d<D;d++){
        double *imgChannel=C+d*N;
        int *iter_pchOffset=patchOffsets;
        for(int k=0;k<patchPixels;k++){
          int pchPixel=lEdge+(*iter_pchOffset);
          iter_pchOffset++;
          *iter_srchData=imgChannel[pchPixel];
          iter_srchData++;
        }
      }
    }
  }

}
