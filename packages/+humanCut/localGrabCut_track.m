function localGrabCut_track(params)
% Function to run grab cut on each frame of video individually

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('params','var'))
  params.tracksDir=[cwd '../../results/felzenBody_tracks/'];
  params.rootOut_dir=[cwd '../../results/felzenTracks_localGrabCut/'];
  params.overWrite=true;
  params.opts.boxHowTo='simple1';
  params.opts.localGrabCutOpts_string='iter10_rad150';
  params.opts.visOpts.enable=true;
  params.opts.visOpts.segBoundaryWidth=2;
  params.opts.visOpts.imgExt='jpg';
  params.evalOpts_string='try1';
  
  params.testBench_cmd='testBench.getTests_tmp()';
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',params.rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
end

[videoPaths,videoFiles]=eval(params.testBench_cmd);
videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);

for i=1:length(videoNames)
  fprintf('Running localGrabCut on video: %s\n',videoNames{i});
  runOnVideo(videoPaths{i},videoFiles{i},videoNames{i},params.tracksDir,...
            params.rootOut_dir,params.overWrite,params.opts);
end

testBench.computeStats_humanCut(params.rootOut_dir,params.evalOpts_string);

function runOnVideo(videoPath,videoFile,videoName,tracksDir,rootOut_dir,overWrite,opts)

outDir=[rootOut_dir videoName '/'];
if(~exist(outDir,'dir')),
  mkdir(outDir);
else
  if(~overWrite)
    fprintf('Output dir: %s already exists, skipping\n',outDir);
    return;
  else
    fprintf('--- Overwriting %s -----\n',outDir);
  end
end

tracksDir=[tracksDir videoName '/'];
trackFile=[tracksDir 'tracks.txt'];

fH=fopen([outDir 'opts.txt'],'w');
miscFns.printStructure(opts,fH);
fprintf(fH,'For specific localGrabCut opts, look into localGrabCut_opts.txt\n');
fclose(fH);

localGrabCut_opts=localGrabCut.helpers.makeOpts(opts.localGrabCutOpts_string);

fH=fopen([outDir 'localGrabCut_opts.txt'],'w');
warning('off','MATLAB:structOnObject');
miscFns.printStructure(struct(localGrabCut_opts),fH);
warning('on','MATLAB:structOnObject');
fclose(fH);

tracks=humanCut.parseTrackFile(trackFile); % returns structure array of size N x 1 where 
% N=number of tracks, field of structure array are:
% trackSt -> frame number where track starts
% trackEnd -> frame number where track ends
% detections -> K x 4 array of bounding boxes, where K=(trackEnd-trackSt+1)

for i=1:length(tracks)
  segOutDir=[outDir sprintf('track%03d_files/',i)];
  if(~exist(segOutDir,'dir')),mkdir(segOutDir); end;
  vH=myVideoReader(videoPath,videoFile);
  h=vH.h;w=vH.w;
  nFrames=vH.nFrames;
  seg=128*ones([h w nFrames],'uint8');
  trackSt=tracks(i).trackSt;
  trackEnd=tracks(i).trackEnd;
  detections=tracks(i).boxes;
  for j=1:(trackSt-1)
    vH.moveForward();
  end

  for j=trackSt:trackEnd
    img=im2double(vH.curFrame);
    labelImg=humanCut.boxes.getBox(opts.boxHowTo,[h w],detections(j-trackSt+1,:));
    segH=localGrabCut.segEngine(0,localGrabCut_opts);
    segH.preProcess(img);
    segH.start(labelImg);
    curSeg=segH.seg;
    seg(:,:,j)=curSeg;
    delete(segH);
    if(opts.visOpts.enable)
      boxesOverlay=humanCut.boxes.overlayBox_labels(img,labelImg);
      segOverlay=humanCut.overlaySeg(img,curSeg,opts.visOpts.segBoundaryWidth);
      imwrite(boxesOverlay,sprintf([segOutDir 'boxes_%03d.%s'],j,opts.visOpts.imgExt));
      imwrite(segOverlay,sprintf([segOutDir 'seg_%03d.%s'],j,opts.visOpts.imgExt));
    end
    if(j~=trackEnd)
      vH.moveForward();
    end
  end
  save([segOutDir 'seg.mat'],'seg','trackSt','trackEnd');
  delete(vH);
end

function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);
