function localGrabCut3D_track(params)
% Function to run grab cut on each frame of video individually

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('params','var'))
  params.tracksDir=[cwd '../../results/felzenBody_tracks/'];
  params.rootOut_dir=[cwd '../../results/felzenTracks_grabCut/'];
  params.overWrite=true;
  params.opts.boxHowTo='simple3';
  params.opts.localGrabCut3D_optsString='21frames_iter10_2_rad80';
  params.opts.visOpts.enable=true;
  params.opts.visOpts.segBoundaryWidth=2;
  params.opts.visOpts.imgExt='jpg';
  params.evalOpts_string='try_proxy1';
  
  params.testBench_cmd='testBench.getTests_tmp()';
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',params.rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
end

[videoPaths,videoFiles]=eval(params.testBench_cmd);
videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);

for i=1:length(videoNames)
  fprintf('Running grabCut on video: %s\n',videoNames{i});
  runOnVideo(videoPaths{i},videoFiles{i},videoNames{i},params.tracksDir,...
            params.rootOut_dir,params.overWrite,params.opts);
end

testBench.computeStats_humanCut(params.rootOut_dir,params.evalOpts_string);

end % of function localGrabCut3D_track

function runOnVideo(videoPath,videoFile,videoName,tracksDir,rootOut_dir,overWrite,opts)

outDir=[rootOut_dir videoName '/'];
if(~exist(outDir,'dir')),
  mkdir(outDir);
else
  if(~overWrite)
    fprintf('Output dir: %s already exists, skipping\n',outDir);
    return;
  else
    fprintf('--- Overwriting %s -----\n',outDir);
  end
end

tracksDir=[tracksDir videoName '/'];
trackFile=[tracksDir 'tracks.txt'];

fH=fopen([outDir 'opts.txt'],'w');
miscFns.printStructure(opts,fH);
fprintf(fH,'For specific grabCut opts, look into grabCut_opts.txt\n');
fclose(fH);

grabCut_opts=localGrabCut3D.helpers.makeOpts_withStitching(opts.localGrabCut3D_optsString);

fH=fopen([outDir 'localGrabCut3D_opts.txt'],'w');
warning('off','MATLAB:structOnObject');
fprintf(fH,'Printing structure of grabCut options (stitching)\n');
miscFns.printStructure(struct(grabCut_opts),fH);
fprintf(fH,'Printing structure of localGrabCut3D options\n');
miscFns.printStructure(struct(localGrabCut3D.helpers.makeOpts(grabCut_opts.opts_string))...
                      ,fH);
warning('on','MATLAB:structOnObject');
fclose(fH);

tracks=humanCut.parseTrackFile(trackFile); % returns structure array of size N x 1 where 
% N=number of tracks, field of structure array are:
% trackSt -> frame number where track starts
% trackEnd -> frame number where track ends
% detections -> K x 4 array of bounding boxes, where K=(trackEnd-trackSt+1)

for i=1:length(tracks)
  segOutDir=[outDir sprintf('track%03d_files/',i)];
  if(~exist(segOutDir,'dir')),mkdir(segOutDir); end;
  vH=myVideoReader(videoPath,videoFile);
  h=vH.h;w=vH.w;
  nFrames=vH.nFrames;
  seg=128*ones([h w nFrames],'uint8');
  trackSt=tracks(i).trackSt;
  trackEnd=tracks(i).trackEnd;
  detections=tracks(i).boxes;
  for j=1:(trackSt-1)
    vH.moveForward();
  end

  curIdx=trackSt;
  streamFnHandle=getStreamFunctionHandle(curIdx,vH,trackEnd,opts,detections,h,w,...
                                         segOutDir,trackSt);
  seg(:,:,trackSt:trackEnd)=localGrabCut3D.run3D_withStiching(streamFnHandle,grabCut_opts);
  delete(vH);

  vH=myVideoReader(videoPath,videoFile);
  for j=1:(trackSt-1)
    vH.moveForward();
  end

  for j=trackSt:trackEnd
    img=im2double(vH.curFrame);
    curSeg=seg(:,:,j);
    if(opts.visOpts.enable)
      segOverlay=humanCut.overlaySeg(img,curSeg,opts.visOpts.segBoundaryWidth);
      imwrite(segOverlay,sprintf([segOutDir 'seg_%03d.%s'],j,opts.visOpts.imgExt));
    end
    if(j~=trackEnd)
      vH.moveForward();
    end
  end

  delete(vH);
  save([segOutDir 'seg.mat'],'seg','trackSt','trackEnd');
end

end % of function runVideo
  
function fnH=getStreamFunctionHandle(curIdx,vH,trackEnd,opts,detections,h,w,...
                                         segOutDir,trackSt)
  function [img,labels]=streamData()
    if(curIdx<=trackEnd)
      %img=im2double(vH.curFrame);
      img=vH.curFrame;
      labels=humanCut.boxes.getBox(opts.boxHowTo,[h w],detections(curIdx-trackSt+1,:));
      if(opts.visOpts.enable)
        boxesOverlay=humanCut.boxes.overlayBox_labels(im2double(img),labels);
        imwrite(boxesOverlay,sprintf([segOutDir 'boxes_%03d.%s'],curIdx,opts.visOpts.imgExt));
      end
      if(curIdx<trackEnd)
        vH.moveForward();
      end
      curIdx=curIdx+1;
    else
      img=[];labels=[];
    end
  end
  fnH=@streamData;
end

function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);

end
