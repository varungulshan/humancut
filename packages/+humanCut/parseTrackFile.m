function tracks=parseTrackFile(trackFile)
% Takes in a track file outputted by marcin's tracker code and returns
% a structure array of size N x 1 where N=number of tracks
% Fields of structure array are:
% trackSt -> frame number where track starts
% trackEnd -> frame number where track ends
% detections -> K x 4 array of bounding boxes, where K=(trackEnd-trackSt+1)

if(~exist(trackFile,'file')),
  error('Trackfile: %s does not exist\n',trackFile);
end

fH=fopen(trackFile,'r');
tracks=struct('trackSt',{},'trackEnd',{},'boxes',{});
done=false;
curTrack=1;
while(1)
  % Invariant, fH points to a line which is either empty or begins with #
  % or has reached eof
  curLine=fgetl(fH);
  while(1)
    if(~ischar(curLine))
      done=true;break;
    end
    if(~isempty(curLine))
      if(curLine(1)~='#')
        break;
      end
    end
    curLine=fgetl(fH);
  end
  % Invariant, done=false and fH points to the first line of a track 
  % or done=true and fH has hit eof
  if(done), break; end;
  
  % Invariant, curLine contains the first line of the track
  tmp=textscan(curLine,'%d %d %d %d %d %*f');trackSt=tmp{1};
  boxes=zeros(0,4);
  prevFrame=trackSt-1;
  while(1)
    tmp=textscan(curLine,'%d %d %d %d %d %*f');
    if(tmp{1}~=(prevFrame+1))
      error('Unexpected seq in tracks\n');
    end
    boxes(end+1,:)=[tmp{2} tmp{3} tmp{4} tmp{5}];
    prevFrame=tmp{1};
    curLine=fgetl(fH);
    if(~ischar(curLine))
        done=true;break;
    end
    if(isempty(curLine))
      break;
    end
    if(curLine(1)=='#')
      break;
    end
  end
  tracks(curTrack).trackSt=trackSt;
  tracks(curTrack).trackEnd=prevFrame;
  tracks(curTrack).boxes=boxes;
  curTrack=curTrack+1;
end
