function labelImg=getSimpleBox2(imgSize,detection,opts)
% Creatings a simple bounding box annotation for grabCut
% opts includes:
%   softBgBand_x -> width of soft bg annotation around detection in x direction, unit
%                   is in fraction of detection width
%   softBgBand_y -> same as above in the y direction
%
%   softFgBand_x -> width of soft fg annotation inside detection in x direction, unit
%                   is in fraction of detection width.
%   softFgBand_y -> same as above in the y direction
%   The rest of the inside segmentation is hard bg!
%
% Inside the box is soft fg, outside the box in the band is soft bg, and further outside
% is hard bg (with no color modeling)

labelImg=6*ones(imgSize,'uint8'); % BG-noLearn

h=imgSize(1);w=imgSize(2);
hBox=detection(4);wBox=detection(3);

hBand=round(hBox*opts.softBgBand_y);
wBand=round(wBox*opts.softBgBand_x);

xT=max(1,detection(1)-wBand);
yT=max(1,detection(2)-hBand);
xB=min(w,detection(1)+wBox+wBand-1);
yB=min(h,detection(2)+hBox+hBand-1);

labelImg(yT:yB,xT:xB)=4; % BG-soft

xT=max(1,detection(1));
yT=max(1,detection(2));
xB=min(w,detection(1)+wBox-1);
yB=min(h,detection(2)+hBox-1);

labelImg(yT:yB,xT:xB)=3; % FG-soft

hBand=round(hBox*opts.softFgBand_y);
wBand=round(wBox*opts.softFgBand_x);

xT=max(1,detection(1)+wBand);
yT=max(1,detection(2)+hBand);
xB=min(w,detection(1)+wBox-wBand-1);
yB=min(h,detection(2)+hBox-hBand-1);

labelImg(yT:yB,xT:xB)=1; % FG-hard
