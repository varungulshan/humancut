function labelImg=getBox(boxHowTo,imgSize,detection)
% Function to create annotation for grabCut
% boxHowTo is a string, imgSize = [h w], detection is [xTop yTop width height]

switch(boxHowTo)
  case 'simple1'
    opts.softBgBand_x=0.50; % Size is in percentage of detection size
    opts.softBgBand_y=0.15; % Size is in percentage of detection size
    labelImg=humanCut.boxes.getSimpleBox(imgSize,detection,opts);
  case 'simple2'
    opts.softBgBand_x=0.50; % Size is in percentage of detection size
    opts.softBgBand_y=0.15; % Size is in percentage of detection size
    opts.softFgBand_x=0.40; % Size is in percentage of detection size
    opts.softFgBand_y=0.25; % Size is in percentage of detection size
    labelImg=humanCut.boxes.getSimpleBox2(imgSize,detection,opts);
  case 'simple3'
    opts.hardBgBand_x=0.50; % Size is in percentage of detection size
    opts.hardBgBand_y=0.15; % Size is in percentage of detection size
    opts.softFgBand_x=0.40; % Size is in percentage of detection size
    opts.softFgBand_y=0.25; % Size is in percentage of detection size
    labelImg=humanCut.boxes.getSimpleBox3(imgSize,detection,opts);
   otherwise
    error('Invalid boxHowTo: %s\n',boxHowTo);
end
