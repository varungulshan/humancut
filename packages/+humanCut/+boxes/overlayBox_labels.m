function blendImg=overlayBox_labels(img,labelImg)
% Overlays in transparent, the labelImg onto the img
% Also puts in text describing label names
% img is in double

[h w nCh]=size(img);
maxLabels=7;
blendAlpha=0.5;

cmap_labels=jet(7);
overlayImg=im2double(label2rgb(labelImg,cmap_labels));
alphaImg=blendAlpha*ones([h w]);
alphaImg(labelImg==0)=0;

blendImg=blendImages(overlayImg,img,alphaImg);
blendImg=im2uint8(blendImg);

for i=1:(maxLabels-1) % Skip labelImg==0
  idx=find(labelImg==i,1,'first')+15;
  if(~isempty(idx))
    blendImg=miscFns.mex_drawText(blendImg,int32(idx),labelText(i));
  end
end

blendImg=im2double(blendImg);

function img=blendImages(img1,img2,alpha)

img=zeros(size(img1));
for i=1:size(img,3)
  img(:,:,i)=alpha.*img1(:,:,i)+(1-alpha).*img2(:,:,i);
end

function txt=labelText(idx)

switch(idx)
  case 0
    txt='noLabel';
  case 1
    txt='fgHard';
  case 2
    txt='bgHard';
  case 3
    txt='fgSoft';
  case 4
    txt='bgSoft';
  case 5
    txt='fgNoLearn';
  case 6
    txt='bgNoLearn';
  otherwise
    error('Invalid idx: %d\n',idx);
end
