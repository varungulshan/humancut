function imgCrop=getSimpleBox1_PS(img,detection,opts)

hBox=detection(4);
wBox=detection(3);

[h w nCh]=size(img);

scale=opts.height_normal/hBox;
xMin=max(1,detection(1)-round(opts.xExpand*wBox));
xMax=min(w,detection(1)+wBox+round(opts.xExpand*wBox));

yMin=max(1,detection(2)-round(opts.yExpand*hBox));
yMax=min(h,detection(2)+hBox+round(opts.yExpand*hBox));

imgCrop=img(yMin:yMax,xMin:xMax,:);
imgCrop=imresize(imgCrop,scale);
