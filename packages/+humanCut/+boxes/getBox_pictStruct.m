function imgCrop=getBox_pictStruct(boxHowTo,img,detection)
% Function to create a cropped image for running andriuluka pictorial structure
% code 
% boxHowTo is a string, detection is [xTop yTop width height]

switch(boxHowTo)
  case 'simple_upperBody_1'
    opts.height_normal=200;
    opts.xExpand=0.50; % Size is in percentage of detection size
    opts.yExpand=0.15; % Size is in percentage of detection size
    imgCrop=humanCut.boxes.getSimpleBox1_PS(img,detection,opts);
   otherwise
    error('Invalid boxHowTo: %s\n',boxHowTo);
end
