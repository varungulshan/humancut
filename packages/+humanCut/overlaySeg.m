function img=overlaySeg(img,seg,lineWidth)

[segBdryMask,segBdryColors]=miscFns.getSegBoundary_twoColors(seg,[0 1 0],...
                            [1 0 0],lineWidth,lineWidth);
img(segBdryMask)=segBdryColors;
