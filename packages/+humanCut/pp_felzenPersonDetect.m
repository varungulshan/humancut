function pp_felzenPersonDetect()

% Runs the felzenswalb detector on a video

cwd=miscFns.extractDirPath(mfilename('fullpath'));
rootOut_dir='../../results/felzenBody_detections/';
modelFile='../../../../software/voc-release4/VOC2009/person_final.mat';

rootOut_dir=[cwd rootOut_dir];
modelFile=[cwd modelFile];
[videoPaths,videoFiles]=testBench.getTests_20frames();
videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);

%addpath('/home/varun/windows/rugbyShared/work/software/voc-release4/');
for i=1:length(videoNames)
  fprintf('\nRunning detections on video: %s\n',videoNames{i});
  runOnVideo(videoPaths{i},videoFiles{i},videoNames{i},rootOut_dir,modelFile);
end
%rmpath('/home/varun/windows/rugbyShared/work/software/voc-release4/');

function runOnVideo(pathname,filename,videoName,rootOutDir,modelFile)

load(modelFile); % provides the variable model

outDir=[rootOutDir videoName '/'];

if(~exist(outDir,'dir')),
  mkdir(outDir);
end

vH=myVideoReader(pathname,filename);
nFrames=vH.nFrames;

for i=1:nFrames
  curFrame=vH.curFrame;
  outName=[outDir sprintf('%05d.jpg',i)];
  fprintf('Running detections in frame %05d\n',i);
  saveDetections(curFrame,outName,model);
  if(i<nFrames), vH.moveForward(); end;
end

delete(vH);

function saveDetections(curFrame,outName,model)

bbox=process(curFrame,model,0);
img=drawBox(curFrame,bbox);

imwrite(img,outName);

function img=drawBox(img,boxes)
boxRadius=1;
[h,w,nCh]=size(img);
if ~isempty(boxes)
  numfilters = floor(size(boxes, 2)/4);
  for i = 1:numfilters
    x1 = round(boxes(:,1+(i-1)*4));
    y1 = round(boxes(:,2+(i-1)*4));
    x2 = round(boxes(:,3+(i-1)*4));
    y2 = round(boxes(:,4+(i-1)*4));
    conf=boxes(:,5+(i-1)*4);
    if i == 1
      %c = 'r';
      c=[255 0 0];
    else
      %c = 'b';
      c=[0 0 255];
    end
    %line([x1 x1 x2 x2 x1]', [y1 y2 y2 y1 y1]', 'color', c, 'linewidth', 3);
    for l=1:length(x1)
      outerX1=max(1,x1(l)-boxRadius);
      outerY1=max(1,y1(l)-boxRadius);
      outerX2=min(w,x2(l)+boxRadius);
      outerY2=min(h,y2(l)+boxRadius);
  
      innerX1=min(w,x1(l)+boxRadius);
      innerY1=min(h,y1(l)+boxRadius);
      innerX2=max(1,x2(l)-boxRadius);
      innerY2=max(1,y2(l)-boxRadius);
  
      tmpCopy=img(innerY1:innerY2,innerX1:innerX2,:);
      for j=1:nCh
        img(outerY1:outerY2,outerX1:outerX2,j)=c(j);
      end
      img(innerY1:innerY2,innerX1:innerX2,:)=tmpCopy;
      confText=printConfText(conf(l));
      textW=min(size(confText,2),w-innerX1+1);
      textH=min(size(confText,1),h-innerY1+1);
      img(innerY1:(innerY1+textH-1),innerX1:(innerX1+textW-1),:)=confText(1:textH,1:textW,:);
    end
  end
end


function txtImg=printConfText(confidence)

hText=20;
wText=45;

img=zeros([hText wText],'uint8');
txtImg=miscFns.renderText(img,sprintf('%.2f',confidence),[255 255 255],'ovr');
