function pp_felzenPersonTrack(opts)

% Runs the felzenswalb detector on a video

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('opts','var'))
  rootOut_dir=[cwd '../../results/felzenBody_tracks/'];
  modelFile=[cwd '../../../../software/voc-release4/VOC2009/person_final.mat'];
  detectEnable=true;
  overWrite=true;
  
  [videoPaths,videoFiles]=testBench.getTests_20frames_3();
else
  rootOut_dir=opts.rootOut_dir;
  modelFile=opts.modelFile;
  detectEnable=opts.detectEnable;
  overWrite=opts.overWrite;
  [videoPaths,videoFiles]=eval(opts.testBench_cmd);
end

videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);

%addpath('/home/varun/windows/rugbyShared/work/software/voc-release4/');
for i=1:length(videoNames)
  fprintf('\nRunning detections on video: %s\n',videoNames{i});
  runOnVideo(videoPaths{i},videoFiles{i},videoNames{i},rootOut_dir,modelFile,...
             detectEnable,cwd,overWrite);
end
%rmpath('/home/varun/windows/rugbyShared/work/software/voc-release4/');

function runOnVideo(pathname,filename,videoName,rootOutDir,modelFile,detectEnable,...
                    cwd,overWrite)

load(modelFile); % provides the variable model

outDir=[rootOutDir videoName '/'];

if(~exist(outDir,'dir')),
  mkdir(outDir);
else
  if(~overWrite)
    fprintf('Output directory: %s already exists, skipping video\n',outDir);
    return;
  else
    fprintf('--- Overwriting on directory: %s ---\n',outDir);
  end
end

vH=myVideoReader(pathname,filename);
nFrames=vH.nFrames;

if(~strcmp(vH.videoType,'imgSequence')),
  error('Currently only supported for image sequences\n');
end

if(detectEnable)
  fH_detections=fopen([outDir 'detections.txt'],'w');

  for i=1:nFrames
    curFrame=vH.curFrame;
    imgPath=[vH.fullPath sprintf(vH.imgRegex,i)];
    fprintf('Running detections in frame %05d\n',i);
    saveDetections(curFrame,model,imgPath,fH_detections);
    if(i<nFrames), vH.moveForward(); end;
  end

  fclose(fH_detections);
end

outVideo=[outDir 'tracks.avi'];
outTracks=[outDir 'tracks.txt'];
makeTracks([outDir 'detections.txt'],outVideo,outTracks,vH.fullPath,vH.imgRegex,nFrames,cwd);

delete(vH);

function makeTracks(detFile,outVideo,outTracks,fullPath,imgRegex,nFrames,cwd)
cmd1='export PATH="/projects/metapkg/bin64:$PATH"; export LD_LIBRARY_PATH=""';
cmd2=[cwd '../../../../software/marcinTracker/detlink -f -s 1 -e ' sprintf('%d',nFrames+1) ...
     ' -v ' outVideo ' -t 1 ' fullPath imgRegex ' <' detFile ' 1>' outTracks ...
     ' 2>/dev/null'];

cmd=[cmd1 '; ' cmd2];
fprintf('Executing: %s\n\n',cmd);
system(cmd);


function bbox=saveDetections(curFrame,model,imgPath,fH_detections)

bbox=process(curFrame,model,0);
if(length(bbox)==0), bbox=zeros(0,5); end
bbox(:,3)=bbox(:,3)-bbox(:,1);
bbox(:,4)=bbox(:,4)-bbox(:,2);
bbox(:,1:4)=round(bbox(:,1:4));
scales=sqrt(bbox(:,3).*bbox(:,4)/(128*128));
for i=1:size(bbox,1)
  fprintf(fH_detections,'%s %d %d %d %d %.6f %.6f\n',imgPath,bbox(i,1),bbox(i,2)...
          ,bbox(i,3),bbox(i,4),scales(i),bbox(i,5));
end
