function pictStruct_track(params)
% Function to run andriluka's code on each frame of the track

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('params','var'))
  params.tracksDir=[cwd '../../results/felzenBody_tracks/'];
  params.tmpDir=[cwd 'tmp/'];
  params.rootOut_dir=[cwd '../../results/felzenTracks_pictStruct/'];
  params.overWrite=true;
  params.opts.boxHowTo='simple_upperBody_1';
  params.opts.pictStructOpts_template=[cwd 'pictStructOpts_files/tryOpts_1.txt'];
  params.opts.trainedClassifiers=[cwd 'pictStructOpts_files/class_buffy/'];
  params.opts.testFile_sprintf=[cwd 'pictStructOpts_files/testFile_sprintf.txt'];
  params.opts.binaryPath=[cwd '../../../../software/partapp-r2/run_partapp.sh'];
  params.opts.visOpts.enable=true;
  params.opts.visOpts.imgExt='jpg';
  
  params.testBench_cmd='testBench.getTests_20frames_ps()';
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',params.rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
end

[videoPaths,videoFiles]=eval(params.testBench_cmd);
videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);

for i=1:length(videoNames)
  fprintf('Running pictorial structures on video: %s\n',videoNames{i});
  runOnVideo(videoPaths{i},videoFiles{i},videoNames{i},params.tracksDir,...
            params.rootOut_dir,params.overWrite,params.opts,params.tmpDir);
end

function runOnVideo(videoPath,videoFile,videoName,tracksDir,rootOut_dir,overWrite,opts,tmpDir)

outDir=[rootOut_dir videoName '/'];
if(~exist(outDir,'dir')),
  mkdir(outDir);
else
  if(~overWrite)
    fprintf('Output dir: %s already exists, skipping\n',outDir);
    return;
  else
    fprintf('--- Overwriting %s -----\n',outDir);
  end
end

tracksDir=[tracksDir videoName '/'];
trackFile=[tracksDir 'tracks.txt'];

fH=fopen([outDir 'opts.txt'],'w');
miscFns.printStructure(opts,fH);
fclose(fH);

tracks=humanCut.parseTrackFile(trackFile); % returns structure array of size N x 1 where 
% N=number of tracks, field of structure array are:
% trackSt -> frame number where track starts
% trackEnd -> frame number where track ends
% detections -> K x 4 array of bounding boxes, where K=(trackEnd-trackSt+1)

generateInputFiles(tmpDir,opts);

for i=1:length(tracks)
  fprintf('Processing track # %02d\n',i);
  segOutDir=[outDir sprintf('track%03d_files/',i)];
  if(~exist(segOutDir,'dir')),mkdir(segOutDir); end;
  vH=myVideoReader(videoPath,videoFile);
  h=vH.h;w=vH.w;
  nFrames=vH.nFrames;

  trackSt=tracks(i).trackSt;
  trackEnd=tracks(i).trackEnd;
  detections=tracks(i).boxes;
  for j=1:(trackSt-1)
    vH.moveForward();
  end

  for j=trackSt:trackEnd
    img=im2double(vH.curFrame);
    %img=vH.curFrame;
    imgCrop=humanCut.boxes.getBox_pictStruct(opts.boxHowTo,img,detections(j-trackSt+1,:));

    tmpFileName=[tmpDir 'inImg.png'];
    imwrite(im2uint8(imgCrop),tmpFileName);

    cmd=[opts.binaryPath ' --expopt ' tmpDir '/tmp_opts.txt ' ' --part_detect --find_obj' ...
        ' 1> /dev/null'];

    system(cmd);
    cmd=[opts.binaryPath ' --expopt ' tmpDir '/tmp_opts.txt ' ' --eval_segments' ...
        ' 1> /dev/null'];
    system(cmd);

    if(opts.visOpts.enable)
      imwrite(im2uint8(imgCrop),[segOutDir sprintf('boxCrop_%03d.%s',j,opts.visOpts.imgExt)]);
      srcFile=[tmpDir 'log_dir/tmp_opts/part_marginals/seg_eval_images/img_000.png'];
      destFile=[segOutDir sprintf('parts_%03d.png',j)];
      copyfile(srcFile,destFile);
    end

    if(j~=trackEnd)
      vH.moveForward();
    end
  end
  delete(vH);
end

rmdir([tmpDir 'log_dir'],'s');

function generateInputFiles(tmpDir,opts)

optsFile=[tmpDir 'tmp_opts.txt'];
fH=fopen(optsFile,'w');
fprintf(fH,'train_dataset: "%strain.al"\n',tmpDir);
fprintf(fH,'validation_dataset: "%strain.al"\n',tmpDir);
fprintf(fH,'test_dataset: "%stest.al"\n',tmpDir);
fprintf(fH,'log_dir: "%slog_dir"\n',tmpDir);

fH2=fopen(opts.pictStructOpts_template,'r');
template=fread(fH2,'*char')';
fclose(fH2);

fprintf(fH,template);
fclose(fH);

trainFile=[tmpDir 'train.al'];
fH=fopen(trainFile,'w');fclose(fH); % empty file created
testFile=[tmpDir 'test.al'];

fH=fopen(opts.testFile_sprintf,'r');
testFile_expr=fread(fH,'*char');
fclose(fH);

fH=fopen(testFile,'w');
fprintf(fH,testFile_expr,[tmpDir 'inImg.png']);
fclose(fH);

classifierDir=[tmpDir 'log_dir/tmp_opts/class'];
if(~exist(classifierDir,'dir')), mkdir(classifierDir); end;

copyfile(opts.trainedClassifiers,classifierDir);

function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);


