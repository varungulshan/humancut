function pp_upperBodyDetect()
% Code to store preprocessed upper body detections in video
% currently only stores the visualization of the detections
% uses marcin's trained upper body detector

cwd=miscFns.extractDirPath(mfilename('fullpath'));
rootOut_dir='../../results/upperBody_detections/';
tmpDir=[cwd 'tmp/'];
svmFile=[cwd 'portrait-base.svm'];

rootOut_dir=[cwd rootOut_dir];
[videoPaths,videoFiles]=testBench.getTests_20frames();
videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);

for i=1:length(videoNames)
  fprintf('\nRunning detections on video: %s\n',videoNames{i});
  runOnVideo(videoPaths{i},videoFiles{i},videoNames{i},rootOut_dir,tmpDir,svmFile);
end

function runOnVideo(pathname,filename,videoName,rootOutDir,tmpDir,svmFile)

outDir=[rootOutDir videoName '/'];

if(~exist(outDir,'dir')),
  mkdir(outDir);
end

vH=myVideoReader(pathname,filename);
nFrames=vH.nFrames;

for i=1:nFrames
  curFrame=vH.curFrame;
  outName=[outDir sprintf('%05d.jpg',i)];
  fprintf('Running detections in frame %05d\n',i);
  saveDetections(curFrame,outName,tmpDir,svmFile);
  if(i<nFrames), vH.moveForward(); end;
end

delete(vH);

function saveDetections(curFrame,outName,tmpDir,svmFile)

tmpFile_img=[tmpDir 'tmp.png'];
imwrite(curFrame,tmpFile_img);
tmpFile_detections=[tmpDir 'detections.txt'];

cmd=['/projects/metapkg/opt/olt/bin/classify_rhog -v 0 -W 128x128 -i ' outName ...
     ' ' tmpFile_img ' ' tmpFile_detections ' ' svmFile];
system(cmd);
