function trivialCut_track(params)
% Function to run the trivial segmentation on a track
% (just label the inside of the box as foreground, and other variations in the future)
% A very crude baseline, but will be useful

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('params','var'))
  params.tracksDir=[cwd '../../results/felzenBody_tracks/'];
  params.rootOut_dir=[cwd '../../results/felzenTracks_trivialCut/'];
  params.overWrite=true;
  params.evalOpts_string='try1';
  params.opts.trivialCut_type='fgInsideBox';
  params.opts.visOpts.enable=true;
  params.opts.visOpts.segBoundaryWidth=2;
  params.opts.visOpts.imgExt='jpg';
  
  params.testBench_cmd='testBench.getTests_tmp()';
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',params.rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
end

[videoPaths,videoFiles]=eval(params.testBench_cmd);
videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);

for i=1:length(videoNames)
  fprintf('Running trivialCut on video: %s\n',videoNames{i});
  runOnVideo(videoPaths{i},videoFiles{i},videoNames{i},params.tracksDir,...
            params.rootOut_dir,params.overWrite,params.opts);
end

testBench.computeStats_humanCut(params.rootOut_dir,params.evalOpts_string);

function runOnVideo(videoPath,videoFile,videoName,tracksDir,rootOut_dir,overWrite,opts)

outDir=[rootOut_dir videoName '/'];
if(~exist(outDir,'dir')),
  mkdir(outDir);
else
  if(~overWrite)
    fprintf('Output dir: %s already exists, skipping\n',outDir);
    return;
  else
    fprintf('--- Overwriting %s -----\n',outDir);
  end
end

tracksDir=[tracksDir videoName '/'];
trackFile=[tracksDir 'tracks.txt'];

fH=fopen([outDir 'opts.txt'],'w');
miscFns.printStructure(opts,fH);
fclose(fH);

tracks=humanCut.parseTrackFile(trackFile); % returns structure array of size N x 1 where 
% N=number of tracks, field of structure array are:
% trackSt -> frame number where track starts
% trackEnd -> frame number where track ends
% detections -> K x 4 array of bounding boxes, where K=(trackEnd-trackSt+1)

for i=1:length(tracks)
  segOutDir=[outDir sprintf('track%03d_files/',i)];
  if(~exist(segOutDir,'dir')),mkdir(segOutDir); end;
  vH=myVideoReader(videoPath,videoFile);
  h=vH.h;w=vH.w;
  nFrames=vH.nFrames;
  seg=128*ones([h w nFrames],'uint8');
  trackSt=tracks(i).trackSt;
  trackEnd=tracks(i).trackEnd;
  detections=tracks(i).boxes;
  for j=1:(trackSt-1)
    vH.moveForward();
  end

  for j=trackSt:trackEnd
    img=im2double(vH.curFrame);
    curDetection=detections(j-trackSt+1,:);
    curSeg=buildTrivialSeg([h w],curDetection,opts.trivialCut_type);
    seg(:,:,j)=curSeg;
    if(opts.visOpts.enable)
      segOverlay=humanCut.overlaySeg(img,curSeg,opts.visOpts.segBoundaryWidth);
      imwrite(segOverlay,sprintf([segOutDir 'seg_%03d.%s'],j,opts.visOpts.imgExt));
    end
    if(j~=trackEnd)
      vH.moveForward();
    end
  end
  save([segOutDir 'seg.mat'],'seg','trackSt','trackEnd');
  delete(vH);
end

function seg=buildTrivialSeg(imgSize,detection,trivialType)

seg=zeros(imgSize,'uint8');
h=imgSize(1);w=imgSize(2);
switch(trivialType)
  case 'fgInsideBox'
    xL=max(1,detection(1));
    xR=min(w,detection(1)+detection(3));
    yT=max(1,detection(2));
    yB=min(h,detection(2)+detection(4));
    seg(yT:yB,xL:xR)=255;
  otherwise
    error('Invalid trivial segmentation type:%s\n',trivialType);
end

function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);
