function pp_upperBodyTrack(params)
% Code to store preprocessed upper body detections in video
% uses marcin's trained upper body detector
% The output is stored in a format amenable to marcin's tracking code

cwd=miscFns.extractDirPath(mfilename('fullpath'));

if(~exist('params','var')),
  params.rootOut_dir=[cwd '../../results/upperBody_tracks2/'];
  params.tmpDir=[cwd 'tmp/'];
  params.svmFile=[cwd 'portrait-base.svm'];
  params.detectEnable=true; % set to true if you want to compute detections, if false it uses already computed detections
  params.detectStore=true; % set to true if you want save the detections in addition to the tracks
  params.detectOpts.threshold=1;
  params.trackOpts.nThreads=1;
  params.overWrite=true;

  params.testBench_cmd='testBench.getTests_20frames_humans()';
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',params.rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
end


[videoPaths,videoFiles]=eval(params.testBench_cmd);
videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);

for i=1:length(videoNames)
  fprintf('\nRunning detections on video: %s\n',videoNames{i});
  runOnVideo(videoPaths{i},videoFiles{i},videoNames{i},params.rootOut_dir,...
  params.tmpDir,params.svmFile,params.detectEnable,cwd,params.detectStore,...
  params.detectOpts,params.trackOpts);
end

function runOnVideo(pathname,filename,videoName,rootOutDir,tmpDir,svmFile,...
         detectEnable,cwd,detectStore,detectOpts,trackOpts)

outDir=[rootOutDir videoName '/'];

if(~exist(outDir,'dir')),
  mkdir(outDir);
else
  fprintf('--- Overwriting on directory: %s ---\n',outDir);
end

vH=myVideoReader(pathname,filename);
nFrames=vH.nFrames;

if(~strcmp(vH.videoType,'imgSequence')),
  error('Currently only supported for image sequences\n');
end

if(detectEnable)

  fH_detOpts=fopen([outDir 'detectOpts.txt'],'w');
  miscFns.printStructure(detectOpts,fH_detOpts);
  fclose(fH_detOpts);

  fH_detections=fopen([outDir 'detections.txt'],'w');
  for i=1:nFrames
    fprintf('Running detections in frame %05d\n',i);
    imgPath=[vH.fullPath sprintf(vH.imgRegex,i)];
    if(detectStore)
      outDetections=[outDir sprintf('%05d_detections.jpg',i)];
    else
      outDetections=[tmpDir 'tmpDetections.jpg'];
    end
    detFile=saveDetections(imgPath,tmpDir,svmFile,outDetections,detectOpts);
    writeDetections(imgPath,detFile,fH_detections);
    if(i<nFrames), vH.moveForward(); end;
  end
  
  fclose(fH_detections);
end

outVideo=[outDir 'tracks.avi'];
outTracks=[outDir 'tracks.txt'];
fH_tracks=fopen([outDir 'trackOpts.txt'],'w');
miscFns.printStructure(trackOpts,fH_tracks);
fclose(fH_tracks);
makeTracks([outDir 'detections.txt'],outVideo,outTracks,vH.fullPath,vH.imgRegex,...
           nFrames,cwd,trackOpts);
delete(vH);

function makeTracks(detFile,outVideo,outTracks,fullPath,imgRegex,nFrames,cwd,trackOpts)
cmd1='export PATH="/projects/metapkg/bin64:$PATH"; export LD_LIBRARY_PATH=""';
cmd2=[cwd '../../../../software/marcinTracker/detlink -f -s 1 -e ' sprintf('%d',nFrames+1) ...
     ' -v ' outVideo ' -t ' sprintf('%d ',trackOpts.nThreads) fullPath imgRegex ...
     ' <' detFile ' 1>' outTracks ' 2>/dev/null'];

cmd=[cmd1 '; ' cmd2];
fprintf('Executing: %s\n\n',cmd);
system(cmd);

function writeDetections(framePath,detFile,fH)

[x,y,w,h,sc,conf,xx]=textread(detFile,'%d %d %d %d %f %f %d\n');
% Ignore the first line as that does not have any valid detections
for i=2:length(x)
  fprintf(fH,'%s %d %d %d %d %f %f\n',framePath,x(i),y(i),w(i),h(i),sc(i),conf(i));
end

function tmpFile_detections=saveDetections(imgPath,tmpDir,svmFile,outFile,detectOpts)

tmpFile_detections=[tmpDir 'detections.txt'];

cmd='export LC_ALL=C; export LD_LIBRARY_PATH=""; ';
cmd=[cmd sprintf('/projects/metapkg/opt/olt/bin/classify_rhog -v 0 -W 128x128 -t %f -i %s ',...
         detectOpts.threshold,outFile) ...
     imgPath ' ' tmpFile_detections ' ' svmFile];
system(cmd);

function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);
