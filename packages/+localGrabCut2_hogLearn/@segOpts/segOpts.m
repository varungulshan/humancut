% Copyright (C) 2010 Varun Gulshan
% This class defines the options for local grabCut segmentation
classdef segOpts
  properties (SetAccess=public, GetAccess=public)
    gcGamma_e  % gamma of graph cuts (for contrast dependant term)
    gcGamma_i  % gamma of graph cuts (for ising model term)
    gcScale  % scaling to convert to integers
    gcNbrType % 'nbr4' or 'nbr8'
    gcSigma_c % 'auto' or a numeric value

    gmmNmix_fg % number of gmm mixtures for fg, usually 5
    gmmNmix_bg % number of gmm mixtures for bg, usually 5

    postProcess % 0 = off, 1 = on
    postProcessHogOutput % 0 = off, 1 = on (this is for post processing hog)
    postProcessConnectivity % 4 or 8
    numIters % Number of iterations to run grabCut for

    localWin_rad % radius of local window
    hogLearnUnary_rescale
    hogLearnType

    winWts_eps
    radRatioForOverlaying
  end

  methods
    function obj=segOpts()
      % Set the default options
      obj.gcGamma_e=140;
      obj.gcGamma_i=5;
      obj.gcScale=50;
      obj.gcNbrType='nbr8';
      obj.gcSigma_c='auto';

      obj.gmmNmix_fg=5;
      obj.gmmNmix_bg=5;
      obj.postProcess=0;
      obj.numIters=10;
      
      obj.localWin_rad=80;
      obj.hogLearnUnary_rescale = 1;
      obj.hogLearnType = 'hogLearnLocal';
      obj.postProcessConnectivity = 4;
      obj.postProcessHogOutput = false;

      obj.winWts_eps = 0.1;
      obj.radRatioForOverlaying = 1;
    end
  end

end % of classdef
