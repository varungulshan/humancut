function iterateOnce(obj)
% Run one iteration of grabCut
% first update color model, then update segmentation

if(~strcmp(obj.state,'started')),
  error('Can only call iterateOnce, once state is started\n');
end

[unaryImg,unaryMask]=obj.createUnaryImg();
obj.seg=obj.gcSeg(unaryImg,unaryMask);
