function [unaryImg,unaryMask]=createUnaryImg(obj)

import localGrabCut2_hogLearn.*;

labelImg=obj.labelImg;
features=obj.features;
seg=obj.seg;

opts=obj.opts;

[h w nCh]=size(obj.img);

unaryImg=zeros([h w 2]);
unaryMask=seg;
idxImg=reshape([1:h*w],[h w]);

winRad = opts.localWin_rad;
fgMaskFull = seg==255;
[bdryMask,winIdx]=cpp.mex_getWindowPts2(fgMaskFull,...
    int32(opts.radRatioForOverlaying*winRad));

posteriors=zeros([h w]);
weights=zeros([h w]);

spWeights=getWindow_wts(winRad,opts.winWts_eps);
winSize=2*winRad+1;

myUB=realmax/(opts.gcScale*100);

for i =1:length(winIdx)
  cur_winIdx=winIdx(i);
  [winY,winX]=ind2sub([h w],cur_winIdx);

  xMin=max(1,winX-winRad);xL_pad1=(xMin-(winX-winRad));
  xMax=min(w,winX+winRad);xR_pad1=(winX+winRad-xMax);
  yMin=max(1,winY-winRad);yT_pad1=(yMin-(winY-winRad));
  yMax=min(h,winY+winRad);yB_pad1=(winY+winRad-yMax);

  xRng=[xMin:xMax];yRng=[yMin:yMax];
  win1_h=length(yRng);
  win1_w=length(xRng);

  ftrIds=idxImg(yMin:yMax,xMin:xMax);
  fgMask=fgMaskFull(yRng,xRng);
  bgMask=~fgMask;

  allFts = obj.features(:,ftrIds(:));
  fgFts=allFts(:,fgMask(:));
  fgGmm=gmm.init_gmmBS(fgFts,opts.gmmNmix_fg);

  bgFts=allFts(:,~fgMask(:));
  bgGmm=gmm.init_gmmBS(bgFts,opts.gmmNmix_bg);

  fgLikeli=gmm.computeGmm_likelihood(allFts,fgGmm)+eps;
  bgLikeli=gmm.computeGmm_likelihood(allFts,bgGmm)+eps;

  fgLikeli=-log(fgLikeli);
  bgLikeli=-log(bgLikeli);
  fgLikeli(fgLikeli>myUB)=myUB;
  bgLikeli(bgLikeli>myUB)=myUB;

  fgLikeli=reshape(fgLikeli,[win1_h win1_w]);
  bgLikeli=reshape(bgLikeli,[win1_h win1_w]);
  
  localWts=spWeights( (1+yT_pad1):(winSize-yB_pad1), (1+xL_pad1):(winSize-xR_pad1) );
  unaryImg(yRng,xRng,1) = unaryImg(yRng,xRng,1)+localWts.*fgLikeli;
  unaryImg(yRng,xRng,2) = unaryImg(yRng,xRng,2)+localWts.*bgLikeli;

  weights(yRng,xRng)=weights(yRng,xRng)+localWts;
  unaryMask(yRng,xRng)=128;
end

% Restore the hard constraints in unaryMask
unaryMask(labelImg == 5 | labelImg == 1) = 255;
unaryMask(labelImg == 6 | labelImg == 2) = 0;
weights(unaryMask~=128)=1;
unaryImg = unaryImg./repmat(weights,[1 1 2]);

function wts=getWindow_wts(rad,epsilon)
  dia=2*rad+1;
  wts=zeros(dia,dia);
  [X,Y]=meshgrid([-rad:rad],[-rad:rad]);
  d=sqrt(X.*X+Y.*Y);
  wts=1./(d+epsilon);
