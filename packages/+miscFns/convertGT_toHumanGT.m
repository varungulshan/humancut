function convertGT_toHumanGT()
% Takes the video segmentation ground truth (one object per frame)
% and converts it into humanCut GT. There is no conversion as such
% just that directory structure for humanCut GT is different as it allows
% for multiple humans in one video

cwd=miscFns.extractDirPath(mfilename('fullpath'));

inGT_dir=[cwd '../../data/manualGT/'];
outGT_dir=[cwd '../../data/manualGT_humanCut/'];
dataset_cmd='testBench.getTests_20frames_humans';

[videoPaths,videoFiles]=eval(dataset_cmd);
videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);

for i=1:length(videoNames)
  srcDir=[inGT_dir videoNames{i} '/'];
  destDir=[outGT_dir videoNames{i} '/human_001/'];
  if(exist(destDir,'dir')),
    fprintf('Skipping video %s, its GT already exists\n',videoNames{i});
    continue;
  else
    mkdir(destDir);
  end
  copyfile(srcDir,destDir);
  fprintf('Copied over GT for %s\n',videoNames{i});
end
