function printStructure(opts,fid)
% Usage: printStructure(opts,[fid])
%   used to print out a structure
%   The opts here can be any general structure
%
% Inputs:
%  opts -> the parameter structure 
%  fid -> optional parameter, if ommitted it prints to std out, else prints to a file

if(~exist('fid','var')), fid=1; end;

fprintf(fid,'\n ------- Printing structure ---------\n\n');

if(strcmp(class(opts),'cell'))
  for i=1:length(opts)
    printStructure(fid,opts{i},sprintf('opts{%d}.',i));
    fprintf(fid,'\n');
  end
else
  do_printStructure(fid,opts,'opts.');
end;

fprintf(fid,'\n ------- Structure printing ends ---------\n\n');

function do_printStructure(fid,s,prePend)
  lFields=fieldnames(s);
  for i=1:length(lFields)
    l=getfield(s,lFields{i});
    if(isstruct(l)),
      %fprintf(fid,'%s%s substructure\n',prePend,lFields{i});
      do_printStructure(fid,l,sprintf('  %s%s.',prePend,lFields{i}));
    else
      printElement(fid,sprintf('%s%s',prePend,lFields{i}),l);
    end;
  end

function printElement(fid,name,value)
if(iscell(value)), value=cell2mat(value); end;
fprintf(fid,'%s=%s\n',name,mat2str(value));
