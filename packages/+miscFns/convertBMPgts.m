function convertBMPgts()

gtDir='../../data/gtFirstFrame/';

cwd=miscFns.extractDirPath(mfilename('fullpath'));
gtDir=[cwd gtDir];

bmps=dir([gtDir '*.bmp']);

for i=1:length(bmps)
  imgName=miscFns.extractImgName(bmps(i).name);
  inName=[gtDir bmps(i).name];
  outName=[gtDir imgName '.png'];
  img=imread(inName);
  imwrite(img(:,:,1),outName);
  fprintf('Converted %s to %s\n',inName,outName);
end
