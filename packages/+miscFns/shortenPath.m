function pathShort=shortenPath(pathString)
% Function that makes the path relative to present working directory
% if this decreases the string lenght, its returned, otherwise
% the original string is returned

if(pathString(1)~='/')
  pathShort=pathString;
  return;
end

pwdStr=[pwd '/'];

idxDiff=1;
maxLen=min(length(pathString),length(pwdStr));

done=false;
while(~done)
  if(idxDiff<=maxLen)
    if(pathString(idxDiff)==pwdStr(idxDiff))
      idxDiff=idxDiff+1;
    else
      done=true;
    end
  else
    done=true;
  end
end

pwdRemaining=pwdStr(idxDiff:end);
pathRemaining=pathString(idxDiff:end);

if(length(pwdRemaining)>=1)
  if(pwdRemaining(1)=='/')
    pwdRemaining=pwdRemaining(2:end);
  end
end

pwdPrefix='';
if(isempty(pwdRemaining))
  pwdPrefix='./';
end

numSlashes=nnz(pwdRemaining=='/');
pwdPrefix=[pwdPrefix repmat('../',[1 numSlashes])];

pathShort=[pwdPrefix pathRemaining];
if(length(pathShort)>length(pathString))
  pathShort=pathString;
end
