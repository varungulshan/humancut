function compile_mex()

cwd=miscFns.extractDirPath(mfilename('fullpath'));

cvFlags=['-I/' cwd '../../../../software/OpenCV-2.1.0/include/opencv/ -lcxcore -L' cwd '../../../../software/OpenCV-2.1.0/release/lib/'];
mexCmds=cell(0,1);
mexCmds{end+1}=sprintf('mex -O %scpp/mex_drawText.cpp %s -outdir %s',cwd,cvFlags,cwd);
mexCmds{end+1}=sprintf('mex -O %scpp/mex_drawTextBig.cpp %s -outdir %s',cwd,cvFlags,cwd);

for i=1:length(mexCmds)
  fprintf('Executing %s\n',mexCmds{i});
  eval(mexCmds{i});
end
