// Draw text on image at a location, using openCV

#include "mex.h"
#include <cmath>
#include <iostream>
#include "cv.h"

typedef unsigned int uint;
typedef unsigned char uchar;

void matlabTo_openCV_matrix(uchar* img,cv::Mat &cvImg,int h,int w,int nCh);
void openCV_toMatlab_matrix(cv::Mat inImg,uchar *outImg,int h,int w,int nCh);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters
     rhs[0] = img     [h x w x nCh] (uint8) img to draw on, column major order
     rhs[1] = drawLocation [1 x 2] (double) (x,y) coordinate
     rhs[2] = text (char string)

     rhs[3] (optional) [r g b] values in [0,255]

     Output: 
     lhs[0] = [h x w x nCh] (uint8) Output image with lines drawn
  */

  if (nrhs < 3)
    mexErrMsgTxt("Atleast 3 input arguments expected.");
  if (nlhs != 1)
    mexErrMsgTxt("1 output arguments expected.");

  if(mxGetClassID(prhs[0])!=mxUINT8_CLASS) mexErrMsgTxt("prhs[0] (img) shd be of type uint8\n");
  if(mxGetClassID(prhs[1])!=mxDOUBLE_CLASS) mexErrMsgTxt("prhs[1] (idx) shd be of type double\n");
  if(mxGetClassID(prhs[2])!=mxCHAR_CLASS) mexErrMsgTxt("prhs[2] (text) shd be of type char\n");
  if(mxGetNumberOfElements(prhs[1])!=2) mexErrMsgTxt("prhs[1] should have 2 elements only\n");

  int h=mxGetDimensions(prhs[0])[0];
  int w=mxGetDimensions(prhs[0])[1];
  int nCh;
  if(mxGetNumberOfDimensions(prhs[0])<3){nCh=1;}
  else{nCh=mxGetDimensions(prhs[0])[2];}
  int N=h*w;
  int r=255,g=255,b=255;
  if(nrhs == 4){
    if(mxGetNumberOfElements(prhs[3])!=3)
      mexErrMsgTxt("Incorrect rgb vector\n");
    double *ptr = mxGetPr(prhs[3]);
    r = (int) ptr[0];
    g = (int) ptr[1];
    b = (int) ptr[2];
  }

  if(nCh!=3)
    mexErrMsgTxt("Currently only RGB images are supported\n");

  int drawX=(int)*(mxGetPr(prhs[1]))-1;
  int drawY=(int)*(mxGetPr(prhs[1])+1)-1;

  uchar *img,*outImg;

  img=(uchar*)mxGetData(prhs[0]);

  int dims[3];dims[0]=h;dims[1]=w;dims[2]=nCh;
  plhs[0]=mxCreateNumericArray(3,dims,mxUINT8_CLASS,mxREAL);
  outImg=(uchar*)mxGetData(plhs[0]);

  cv::Mat inImg=cv::Mat(h,w,CV_8UC3);
  matlabTo_openCV_matrix(img,inImg,h,w,nCh);

  char *txt=mxArrayToString(prhs[2]);  
  cv::putText(inImg,std::string(txt),cv::Point(drawX,drawY),
      cv::FONT_HERSHEY_PLAIN,2.7,cv::Scalar(r,g,b),3,8,false);

  openCV_toMatlab_matrix(inImg,outImg,h,w,nCh);

  // ----- To free -----------------------------------------------
  mxFree(txt);
}

void matlabTo_openCV_matrix(uchar* img,cv::Mat &cvImg,int h,int w,int nCh){
  int N=h*w;
  for(int y=0;y<h;y++){
    uchar* rowY=cvImg.ptr<uchar>(y);
    for(int x=0;x<w;x++){
      for(int d=0;d<nCh;d++){
        (*rowY)=img[d*N+h*x+y];
        rowY++;
      }
    }
  }
}

void openCV_toMatlab_matrix(cv::Mat cvImg,uchar *outImg,int h,int w,int nCh){
  int N=h*w;
  for(int y=0;y<h;y++){
    uchar* rowY=cvImg.ptr<uchar>(y);
    for(int x=0;x<w;x++){
      for(int d=0;d<nCh;d++){
        outImg[d*N+h*x+y]=(*rowY);
        rowY++;
      }
    }
  }
}
