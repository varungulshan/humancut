function printMemory(whos_struct,varName)
% Print memory taken by variables in whos_struct

fprintf('Memory used by %s = %.3f MB\n',varName,whos_struct.bytes/2^20);
