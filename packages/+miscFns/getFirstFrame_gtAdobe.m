function getFirstFrame_gtAdobe()
% Takes the first frame of segmentations in the adobe results
% and stores it as the first frame to be used for other methods

cwd=miscFns.extractDirPath(mfilename('fullpath'));

outDir=[cwd '../../data/gtFirstFrame_adobe/'];
inDir=[cwd '../../results/adobeSnapCut/'];
dataset_cmd='testBench.getTests_20frames_4()';

[videoPaths,videoFiles]=eval(dataset_cmd);
videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);

if(~exist(outDir)),
  mkdir(outDir);
end

for i=1:length(videoNames)
  outGT_file=[outDir videoNames{i} '.png'];
  if(exist(outGT_file,'file')),
    fprintf('Skipping video:%s, gtFile already exists\n',videoNames{i});
    continue;
  end
  segFile=[inDir videoNames{i} '/seg.mat'];
  load(segFile); % Provides the variable seg
  imwrite(seg(:,:,1),outGT_file);
  fprintf('Saved gt for %s from %s\n',videoNames{i},segFile);
end
