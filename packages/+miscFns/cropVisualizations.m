function cropVisualizations(userOpts)


% ---- default opts -----
opts.videoPath='../../data/cuts/maninhat-1to21/';
opts.videoFile='00001.png';
%opts.videoPath='../../data/snapCut_videos/';
%opts.videoFile='maninhat-1to21.mov';
opts.resDir=cell(0,0);
%opts.resDir{end+1}='../../results/snapCut2_visualize/rad40_noMotion/';
opts.resDir{end+1}='../../results/snapCut2_visualize/rad30_noMotion/';
%opts.resDir{end+1}='../../results/snapCut2_visualize/rad20_noMotion/';
%opts.views=[1 2 3 4 5 6 7 8 10];
%opts.views=[1 2 4 10];
opts.views=[10];
opts.showView=2;
opts.startFrame=13;
opts.deltaFrames=[-1 15];
opts.outDir=cell(0,0);
opts.invertNaming=true; % true: file name of type view004_1, 004 is frame number, 1 is view
                         % number. if false, file name is view1_004
%opts.outDir{end+1}='../../results/croppedVisualizations/rad40/';
opts.outDir{end+1}='../../results/croppedVisualizations/manHatCrop2/';
%opts.outDir{end+1}='../../results/croppedVisualizations/rad20/';
% ------ end default opts -------

if(~exist('userOpts','var')), userOpts=struct(); end;
opts=vl_override(opts,userOpts);
%miscFns.printStructure(opts);

cwd=miscFns.extractDirPath(mfilename('fullpath'));

opts.videoPath=[cwd opts.videoPath];
videoName=miscFns.extractVideoNames({opts.videoPath},{opts.videoFile});
videoName=videoName{1};

for i=1:length(opts.resDir)
  fullResDir{i}=[cwd opts.resDir{i} videoName '/'];
  outDir{i}=[cwd opts.outDir{i}];
  if(exist(outDir{i},'dir')), rmdir(outDir{i},'s'); end;
  mkdir(outDir{i});
end

viewFile=[fullResDir{1} sprintf('view%d_%03d.png',opts.showView,opts.startFrame)];
imView=imread(viewFile);

boxSize=getCropBox(imView);

vH=myVideoReader(opts.videoPath,opts.videoFile);
minFrame=max(1,opts.startFrame+opts.deltaFrames(1));
maxFrame=min(vH.nFrames,opts.startFrame+opts.deltaFrames(2));

for i=1:(minFrame-1)
  vH.moveForward();
end

for i=minFrame:maxFrame
  orig=myCrop(vH.curFrame,boxSize);
  for k=1:length(outDir)
    filName=[outDir{k} sprintf('orig_%03d.png',i)];
    imwrite(orig,filName);
  end

  for j=1:length(opts.views)
    for k=1:length(outDir)
      inFile=[fullResDir{k} sprintf('view%d_%03d.png',opts.views(j),i)];
      if(exist(inFile,'file'))
        view=myCrop(imread(inFile),boxSize);
        if(opts.invertNaming)
          filName=[outDir{k} sprintf('view%03d_%d.png',i,opts.views(j))];
        else
          filName=[outDir{k} sprintf('view%d_%03d.png',opts.views(j),i)];
        end
        imwrite(view,filName);
      end
    end
  end
  if(i<vH.nFrames)
    vH.moveForward();
  end
end

delete(vH);

function img=myCrop(img,boxSize)
img=img(boxSize(1):boxSize(2),boxSize(3):boxSize(4),:);

function boxSize=getCropBox(img)
% boxSize = [top bottom left right]

fprintf('Draw box to crop\n');
figure;imshow(img);
[nrows ncols nDim]=size(img);
k = waitforbuttonpress;
point1 = get(gca,'CurrentPoint');    % button down detected
finalRect = rbbox;                   % return figure units
point2 = get(gca,'CurrentPoint');    % button up detected
point1 = point1(1,1:2);              % extract x and y
point2 = point2(1,1:2);
point1 = max( point1, [1, 1] );
point1 = min( point1, [ncols, nrows] );
point2 = max( point2, [1, 1] );
point2 = min( point2, [ncols, nrows] );

p1 = round(min(point1,point2));      % calculate locations
p2 = round(max(point1,point2));

boxSize=[p1(2) p2(2) p1(1) p2(1)];
close;
