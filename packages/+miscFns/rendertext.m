%Render RBG text over RBG matrices
%
%out=rendertext(target, text, color, mode)
%
%target: must be MxNx3 for rgb support.
%text: is a string
%color: is a vector in the form [r g b]
%mode: 'ovr' to overwrite, 'bnd' to blend text over image
%out: has same size of target
%
%example:
%
%in=imread('football.jpg);
%out=rendertext(in,'Text Here',[0 255 0],'ovr';
%imshow(out)
%
%
function out=rendertext(target, text, color, mode)

r=color(1);
g=color(2);
b=color(3);

n=numel(text);
cwd=miscFns.extractDirPath(mfilename('fullpath'));

base=uint8(1-logical(imread([cwd 'chars.bmp'])));
base=cat(3,base*r, base*g, base*b);

table='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890''�!"�$%&/()=?^�+���,.-<\|;:_>簧�*@#[]{} ';

coord(2,n)=0;

for i=1:n    
    for j=1:numel(table)
        if text(i)==table(j);
            coord(:,i)=[j 0];
        end
    end    
end

for i=1:n
    if coord(1,i)<=26
        coord(:,i)=[1 (coord(1,i)-1)*13];
    end
    if coord(1,i)>26 && coord(1,i)<=52
        coord(:,i)=[20 (coord(1,i)-26-1)*13];
    end
    if coord(1,i)>=53 && coord(1,i)<=78
        coord(:,i)=[40 (coord(1,i)-52-1)*13];
    end
    if coord(1,i)>=79
        coord(:,i)=[60 (coord(1,i)-78-1)*13];
    end    
end

overlay(20,1,3)=0;

for i=1:n
    cr=imcrop(base,[coord(2,i) coord(1,i) 13 19]);
    overlay=([overlay cr]);
end

if mode=='ovr'    
    target([1:(size(overlay,1))],[1:size(overlay,2)],1:3)=overlay; 
end
if mode=='bnd'
    target([1:(size(overlay,1))],[1:size(overlay,2)],1:3)=overlay+target([1:(size(overlay,1))],[1:size(overlay,2)],1:3);
end

out=target;



