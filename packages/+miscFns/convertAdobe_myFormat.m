function convertAdobe_myFormat()
% Converts the output segmentation by Adobe CS5 snapCut, to a format
% amenable for visualization and evaluation by my functions

cwd=miscFns.extractDirPath(mfilename('fullpath'));

root_outResDir=[cwd '../../results/adobeSnapCut/'];
inDir=[cwd '../../results/cs5_projects/'];
dataset_cmd='testBench.getTests_20frames_4()';
canvasColor=[0 0 255];
segBoundaryWidth=2;

[videoPaths,videoFiles]=eval(dataset_cmd);
videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);


for i=1:length(videoNames)

  outDir=[root_outResDir videoNames{i} '/'];

  if(exist(outDir,'dir')),
    fprintf('Skipping already existing directory: %s\n',outDir);
    continue;
  else
    mkdir(outDir);
    fprintf('Processing output video: %s\n',videoNames{i});
  end

  fH=fopen([outDir 'views.txt'],'w');
  fprintf(fH,'View 01: Segmentation outline\n');
  fclose(fH);


  vH=myVideoReader(inDir,[videoNames{i} '.avi']);
  vH2=myVideoReader(videoPaths{i},videoFiles{i});
  h=vH.h;
  w=vH.w;
  nFrames=vH.nFrames;
  if(nFrames~=vH2.nFrames)
    error('Unexpected case: # of original video frames != snapcut segmentation frames\n');
  end
  seg=zeros([h w nFrames],'uint8');

  opts.sourceVideo=[inDir videoNames{i} '.avi'];
  opts.canvasColor=canvasColor;

  fH=fopen([outDir 'opts.txt'],'w');
  fprintf(fH,'Printing options for adobe snapCut:\n');
  miscFns.printStructure(opts,fH);
  fclose(fH);

  for j=1:nFrames
    curFrame=vH.curFrame;
    bgMask=(curFrame(:,:,1)==canvasColor(1) & curFrame(:,:,2)==canvasColor(2) ...
           & curFrame(:,:,3)==canvasColor(3));
    curSeg=255*ones([h w],'uint8');
    curSeg(bgMask)=uint8(0);
    seg(:,:,j)=curSeg;
    [segBdryMask,segBdryColors]=miscFns.getSegBoundary_twoColors(...
                 curSeg,[0 1 0],[1 0 0],segBoundaryWidth,segBoundaryWidth);
    img=im2double(vH2.curFrame);
    img(segBdryMask)=segBdryColors;
    imwrite(img,[outDir sprintf('view1_%03d.png',j)]);
    if(j~=nFrames)
      vH.moveForward();
      vH2.moveForward();
    end
  end

  save([outDir 'seg.mat'],'seg');
  delete(vH);
  delete(vH2);
end
