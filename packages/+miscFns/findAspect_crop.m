function [found,w,h,xMin,xMax,yMin,yMax]=findAspect_crop(file,videoName)

if(~exist(file,'file')),
  error('File %s does not exist\n',file);
end

[names,wS,hS,xMins,xMaxs,yMins,yMaxs]=textread(file,'%s %s %s %s %s %s %s\n');

found=false;w=[];h=[];xMin=[];xMax=[];yMin=[];yMax=[];

for i=2:length(names)
  if(strcmp(names{i},videoName)),
    w=str2num(wS{i});
    h=str2num(hS{i});
    xMin=str2num(xMins{i});
    xMax=str2num(xMaxs{i});
    yMin=str2num(yMins{i});
    yMax=str2num(yMaxs{i});
    found=true; return;
  end
end

