function printClass(opts,fid)
% Usage: printStructure(opts,[fid])
%   used to print out a structure
%   The opts here can be any general structure
%
% Inputs:
%  opts -> the parameter structure 
%  fid -> optional parameter, if ommitted it prints to std out, else prints to a file

if(~exist('fid','var')), fid=1; end;

fprintf(fid,'\n ------- Printing members of class: %s ---------\n\n',...
       class(opts));

warning('off','MATLAB:structOnObject');
opts=struct(opts);
warning('on','MATLAB:structOnObject');
miscFns.printStructure(opts,fid);

