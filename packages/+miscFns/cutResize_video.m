function cutResize_video()

import miscFns.* 
root_outVideoDir=['../../data/tinyTests/'];
[videoPaths,videoFiles]=testBench.getTests_tmp();
cutStart=1; % 1 based index
cutEnd=3; % 1 based index
maxDim=200; % to resize
aspectCropFile=['../../data/myVideos/aspect_crops.txt'];

%inVideoPath=['../../data/snapCut_videos/footballer/'];
%inVideoFile='0001.png';

%inVideoPath=['../../data/myVideos/'];
%inVideoFile='gandhi_1.avi';

%outVideoDir=[cwd '../../data/myVideos/as_good_as_it_gets_1/'];
%outVideoDir=['../../data/cuts/gandhi_1'];

cwd=extractDirPath(mfilename('fullpath'));
aspectCropFile=[cwd aspectCropFile];
root_outVideoDir=[cwd root_outVideoDir];
videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);
%inVideoPath=[cwd inVideoPath];

for i=1:length(videoPaths)

  vH=myVideoReader(videoPaths{i},videoFiles{i});
  h=vH.h;
  w=vH.w;
  nFrames=vH.nFrames;
  
  hScale=min(1,maxDim/h);
  wScale=min(1,maxDim/w);
  
  scale=min(hScale,wScale);
  
  cutStart=min(cutStart,nFrames);
  if(cutStart>nFrames),
    error('cutStart=%d > nFrames=%d\n',cutStart,nFrames);
  end
  cutEnd=min(cutEnd,nFrames);
  
  outVideoDir=[root_outVideoDir sprintf('%s-%dto%d/',videoNames{i},cutStart,cutEnd)];
  if(exist(outVideoDir,'dir')),
    warning('Skipping existing dir %s, it already exists\n',outVideoDir);
    continue;
  else
    mkdir(outVideoDir);
  end
  
  [aspCorrect,aspW,aspH,xMin,xMax,yMin,yMax]=miscFns.findAspect_crop(aspectCropFile,...
                                                                    videoNames{i});

  for j=1:(cutStart-1)
    vH.moveForward();
  end
  
  for j=cutStart:(cutEnd-1)
    img=vH.curFrame;
    if(scale~=1),
      img=imresize(img,scale);
    end
    if(aspCorrect)
      img=correctAspect(img,aspW,aspH,xMin,xMax,yMin,yMax); 
    end
    offset=(j-cutStart+1);
    imwrite(img,[outVideoDir sprintf('%05d.png',offset)]);
    fprintf('Frame %d written\n',offset);
    vH.moveForward();
  end
  
  j=cutEnd;
  img=vH.curFrame;
  if(scale~=1),
    img=imresize(img,scale);
  end
  if(aspCorrect)
    img=correctAspect(img,aspW,aspH,xMin,xMax,yMin,yMax); 
  end
  offset=(j-cutStart+1);
  imwrite(img,[outVideoDir sprintf('%05d.png',offset)]);
  fprintf('Frame %d written\n',offset);
  
  delete(vH);
end

function img=correctAspect(img,aW,aH,xMin,xMax,yMin,yMax)

[h w nCh]=size(img);
curAsp=w/h;
realAsp=aW/aH;
if(realAsp>curAsp)
  hNew=ceil(w/realAsp);
  wNew=w;
  hScale=hNew/h;
  wScale=1;
elseif(realAsp<curAsp)
  wNew=ceil(h*realAsp);
  hNew=h;
  hScale=1;
  wScale=wNew/w;
else
  hNew=h;
  wNew=w;
  hScale=1;
  wScale=1;
end

xMin=round(xMin*wScale+1); %+1 is to convert it to 1 based index
xMax=round(xMax*wScale+1);
yMin=round(yMin*hScale+1);
yMax=round(yMax*hScale+1);

img=imresize(img,'outputsize',[hNew wNew],'method','bilinear');

xMin=max(1,xMin);
xMax=min(wNew,xMax);
yMin=max(1,yMin);
yMax=min(hNew,yMax);

img=img(yMin:yMax,xMin:xMax,:);
