function videoNames=extractVideoNames(paths,names)
% Checks if video is a avi file, or if it is a seq of images.
% returns the names accordingly


for i=1:length(names)
  extension=names{i}(end-2:end);
  if(isImage(extension))
    videoNames{i}=miscFns.extractDirName(paths{i});
  elseif(isVideo(extension))
    videoNames{i}=names{i}(1:end-4);
  else
    error('Unrecognised extension %s in extractVideoNames\n',extension);
  end
end

function ans=isImage(ext)
  ans=false;
  switch(ext)
    case {'png','jpg','bmp'}
      ans=true;
    otherwise
      ans=false;
  end

function ans=isVideo(ext)
  ans=false;
  switch(ext)
    case {'avi','mpg','mp4','mov'}
      ans=true;
    otherwise
      ans=false;
  end
