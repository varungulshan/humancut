function cutResize_video_tracks()
% Cuts resizes videos, and the correponding h3d anno and klt tracks also

import miscFns.* 
cwd=extractDirPath(mfilename('fullpath'));

tracksDir=['/data/adam2/varun/kltTracks/'];
tracksDir_out=[cwd '../../data/kltTracks_tiny/'];
h3dDir=[cwd '../../data/h3dAnnotations_2/'];
h3dDir_out=[cwd '../../data/h3dAnnotations_2_tiny/'];
root_outVideoDir=[cwd '../../data/tinyTests_h3d/'];
[videoPaths,videoFiles]=testBench.getTests_proxy2();
cutStart=1; % 1 based index
cutEnd=3; % 1 based index
maxDim=200; % to resize
aspectCropFile=[cwd '../../data/myVideos/aspect_crops.txt'];

videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);
%inVideoPath=[cwd inVideoPath];

for i=1:length(videoPaths)

  vH=myVideoReader(videoPaths{i},videoFiles{i});
  h=vH.h;
  w=vH.w;
  nFrames=vH.nFrames;
  
  hScale=min(1,maxDim/h);
  wScale=min(1,maxDim/w);
  
  scale=min(hScale,wScale);
  
  cutStart=min(cutStart,nFrames);
  if(cutStart>nFrames),
    error('cutStart=%d > nFrames=%d\n',cutStart,nFrames);
  end
  cutEnd=min(cutEnd,nFrames);
  
  outVideoDir=[root_outVideoDir sprintf('%s-%dto%d/',videoNames{i},cutStart,cutEnd)];
  outH3dDir=[h3dDir_out sprintf('%s-%dto%d/',videoNames{i},cutStart,cutEnd)];
  outTrackDir=[tracksDir_out sprintf('%s-%dto%d/',videoNames{i},cutStart,cutEnd)];
  if(exist(outVideoDir,'dir')),
    warning('Skipping existing dir %s, it already exists\n',outVideoDir);
    continue;
  else
    mkdir(outVideoDir);
  end
  if(exist(outH3dDir,'dir'))
    warning('Skipping existing dir %s, it already exists\n',outH3dDir);
    continue;
  else
    mkdir(outH3dDir);
  end
  if(exist(outTrackDir,'dir'))
    warning('Skipping existing dir %s, it already exists\n',outTrackDir);
    continue;
  else
    mkdir(outTrackDir);
  end
  
  [aspCorrect,aspW,aspH,xMin,xMax,yMin,yMax]=miscFns.findAspect_crop(aspectCropFile,...
                                                                    videoNames{i});

  for j=1:(cutStart-1)
    vH.moveForward();
  end
  
  for j=cutStart:(cutEnd-1)
    img=vH.curFrame;
    if(aspCorrect)
      img=correctAspect(img,aspW,aspH,xMin,xMax,yMin,yMax); 
    end
    if(scale~=1),
      img=imresize(img,scale);
    end
    offset=(j-cutStart+1);
    imwrite(img,[outVideoDir sprintf('%05d.png',offset)]);
    h3dAnno_file=[h3dDir videoNames{i} sprintf('/%05d.png.gnd',j)];
    h3dAnno_file_out=[outH3dDir sprintf('%05d.png.gnd',offset)];
    resizeH3d_anno(h3dAnno_file,h3dAnno_file_out,scale);

    trackFile=[tracksDir videoNames{i} sprintf('/%05d.mat',j)];
    trackFile_out=[outTrackDir sprintf('%05d.mat',offset)];
    resizeTracks(trackFile,trackFile_out,scale);

    fprintf('Frame %d written\n',offset);
    vH.moveForward();
  end
  
  j=cutEnd;
  img=vH.curFrame;
  if(aspCorrect)
    img=correctAspect(img,aspW,aspH,xMin,xMax,yMin,yMax); 
  end
  if(scale~=1),
    img=imresize(img,scale);
  end
  offset=(j-cutStart+1);
  imwrite(img,[outVideoDir sprintf('%05d.png',offset)]);
  h3dAnno_file=[h3dDir videoNames{i} sprintf('/%05d.png.gnd',j)];
  h3dAnno_file_out=[outH3dDir sprintf('/%05d.png.gnd',offset)];
  resizeH3d_anno(h3dAnno_file,h3dAnno_file_out,scale);
  fprintf('Frame %d written\n',offset);
  % No need to resize tracks for last frame
  
  delete(vH);
end

function resizeTracks(inFile,outFile,scale)

tmpLoad=load(inFile);
pts_init=tmpLoad.pts_init*scale;
pts_final=tmpLoad.pts_final*scale;
mask_tracked=tmpLoad.mask_tracked;

save(outFile,'pts_init','pts_final','mask_tracked');

function resizeH3d_anno(inFile,outFile,scale)

fH_in=fopen(inFile,'r');
if(exist(outFile,'file')),
  error('Cannot overwrite outFile: %s\n',outFile);
end
fH_out=fopen(outFile,'w');

nxtLine=fgets(fH_in);
while(ischar(nxtLine))
  nxtIdx=1;
  while(nxtIdx<=length(nxtLine))
    nxtLine=nxtLine(nxtIdx:end);
    [tmpStr,xx,xx,nxtIdx]=sscanf(nxtLine,'%s',1);
    num=str2double(tmpStr);
    if(isnan(num))
      fprintf(fH_out,'%s ',tmpStr);
    else
      fprintf(fH_out,'%.2f ',num*scale);
    end
  end
  fprintf(fH_out,'\n');
  nxtLine=fgets(fH_in);
end

fclose(fH_in);
fclose(fH_out);

function img=correctAspect(img,aW,aH,xMin,xMax,yMin,yMax)

[h w nCh]=size(img);
curAsp=w/h;
realAsp=aW/aH;
if(realAsp>curAsp)
  hNew=ceil(w/realAsp);
  wNew=w;
  hScale=hNew/h;
  wScale=1;
elseif(realAsp<curAsp)
  wNew=ceil(h*realAsp);
  hNew=h;
  hScale=1;
  wScale=wNew/w;
else
  hNew=h;
  wNew=w;
  hScale=1;
  wScale=1;
end

xMin=round(xMin*wScale+1); %+1 is to convert it to 1 based index
xMax=round(xMax*wScale+1);
yMin=round(yMin*hScale+1);
yMax=round(yMax*hScale+1);

img=imresize(img,'outputsize',[hNew wNew],'method','bilinear');

xMin=max(1,xMin);
xMax=min(wNew,xMax);
yMin=max(1,yMin);
yMax=min(hNew,yMax);

img=img(yMin:yMax,xMin:xMax,:);
