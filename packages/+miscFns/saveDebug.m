function saveDebug(debugOpts,img,filename)
% function saveDebug(debugOpts,img,filename)
% Function to write images if debug mode is set in debugOpts

if(debugOpts.debugLevel>0)
  imwrite(img,[debugOpts.debugDir debugOpts.debugPrefix filename]);
end
