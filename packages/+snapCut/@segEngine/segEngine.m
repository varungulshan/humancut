classdef segEngine < videoSegmenter
  properties (SetAccess=private, GetAccess=public)
    debugLevel
    gtFile % gt for first frame
    vH
    gtSeg_firstFrame
    h
    w
    nCh
    nFrames
    state % 'initialized','pped','segStarted'
    seg % uint8, seg variable for the entire volume
    frameNum
    framesSegmented
    video % only if debugLevel > 0
    beta
    roffset %int32
    coffset %int32
    bgH % structure to maintain bg information
    fgH  % structure to maintain fg information
    localWindows_idx % only set if debugLevel>0, for visualization purposes
    lHoods % only set if debugLevel>0, for visualization purposes
    lHoods_mask % only set if debugLevel>0, for visualization purposes
    opts % object of class segEngine_opts, see class definiton for settings
  end

  methods
    function obj=segEngine(debugLevel,gtFile,vH,opts)
      obj.debugLevel=debugLevel;
      obj.gtFile=gtFile; 
      obj.vH=vH;
      [obj.h obj.w obj.nCh obj.nFrames]=obj.vH.videoSize();
      obj.state='initialized';
      obj.seg=zeros([obj.h obj.w obj.nFrames],'uint8');
      obj.framesSegmented=0;
      obj.frameNum=1;
      obj.video=[];
      obj.opts=opts;
      obj.beta=[];
    end
    img=view_1(obj) % Disabled (gives error)
    img=view_2(obj) % Shows the local windows used to generate posteriors in cur frame
    img=view_3(obj) % Shows the likelihood image
    img=view_4(obj) % Shows the local windows in prev frame from which color models were learnt
    img=view_5(obj) % Disabled (gives error)
    img=view_10(obj) % Show the fg segmentation
    printOptions(obj,fH) % prints the segmentation options to a file or std-out
                     % If not fH is specified, prints to stdout
  end

  methods (Access=private)
    preProcess_GC(obj)
    initBG_history(obj)
    initFG_history(obj)
  end

end % of classdef
