function img=view_3(obj)
% Shows the likelihood image

if(obj.debugLevel>0)
  if(obj.frameNum==1),
    img=zeros([obj.h obj.w 3]);
    img=miscFns.rendertext(img,'Frame_1_has_no_likelihoods',[1 1 1],'ovr'); 
  else
    img=getLhood_image(obj.lHoods,obj.lHoods_mask);
  end
else
  img=zeros([obj.h obj.w 3]);
  img=miscFns.rendertext(img,'No_view_3_at_debugLevel_0',[1 1 1],'ovr'); 
end


function img=getLhood_image(lHoods,lHoods_mask)

[h w]=size(lHoods.fg);
img=zeros([h w]);
img(lHoods_mask==255)=1;
img(lHoods_mask==0)=0;

posterior=lHoods.fg./(lHoods.fg+lHoods.bg);
img(lHoods_mask==128)=posterior(lHoods_mask==128);
