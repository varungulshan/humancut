function seg=snapCut_seg(obj,warpBG,warpFG,img)
% img should be double
import snapCut.*;

fgMask=warpFG.mask_known;
winRad=obj.opts.winRad;
%[bdryMask,localWindows_idx]=cpp.mex_getWindowPts(fgMask,int32(winRad));
[bdryMask,localWindows_idx]=cpp.mex_getWindowPts2(fgMask,int32(winRad));

mask_likelihood=zeros(size(fgMask),'uint8');
mask_likelihood(fgMask)=255;
[lHoods,mask_likelihood]=localClassifiers(localWindows_idx,warpBG,warpFG,img,winRad,...
                                        obj.opts,mask_likelihood);
seg=obj.gcSeg(lHoods,mask_likelihood,img);
if(obj.debugLevel>0)
  obj.localWindows_idx=localWindows_idx;
  obj.lHoods=lHoods;
  obj.lHoods_mask=mask_likelihood;
end

function [lHoods,mask_l]=localClassifiers(winIdx,warpBG,warpFG,img,winRad,opts,mask_l)

import snapCut.*

[h w nCh]=size(warpBG.img);

warpBG.img=im2double(warpBG.img);
warpFG.img=im2double(warpFG.img);

unariesFG=zeros([h w]);
unariesBG=zeros([h w]);

for i =1:length(winIdx)
  [winY,winX]=ind2sub([h w],winIdx(i));
  xMin=max(1,winX-winRad);
  xMax=min(w,winX+winRad);
  yMin=max(1,winY-winRad);
  yMax=min(h,winY+winRad);

  xRng=[xMin:xMax];yRng=[yMin:yMax];

  boxImg=img(yRng,xRng,:);

  bgMask=~warpBG.mask_unknown(yRng,xRng);
  fgMask=warpFG.mask_known(yRng,xRng);
  bgImg=warpBG.img(yRng,xRng,:);
  fgImg=warpFG.img(yRng,xRng,:);

  fgFts=fgImg(repmat(fgMask,[1 1 nCh]));
  numFg_fts=nnz(fgMask);
  fgFts=reshape(fgFts,[numFg_fts nCh])';
  fgGmm=gmm.init_gmmBS(fgFts,opts.gmmNmix_fg);

  bgFts=bgImg(repmat(bgMask,[1 1 nCh]));
  numBg_fts=nnz(bgMask);
  bgFts=reshape(bgFts,[numBg_fts nCh])';
  bgGmm=gmm.init_gmmBS(bgFts,opts.gmmNmix_bg);

  numFts=size(boxImg,1)*size(boxImg,2);
  fts=reshape(boxImg,[numFts nCh])';
  
  fgLikeli=gmm.computeGmm_likelihood(fts,fgGmm);
  bgLikeli=gmm.computeGmm_likelihood(fts,bgGmm);

  l_gamma=opts.gmmLikeli_gamma;
  unifValue=opts.gmmUni_value;
  fgLikeli=l_gamma*unifValue+(1-l_gamma)*fgLikeli;  
  bgLikeli=l_gamma*unifValue+(1-l_gamma)*bgLikeli;  

  fgLikeli=reshape(fgLikeli,[yMax-yMin+1 xMax-xMin+1]);
  bgLikeli=reshape(bgLikeli,[yMax-yMin+1 xMax-xMin+1]);
  
  unariesFG(yRng,xRng)=max(unariesFG(yRng,xRng),fgLikeli);
  unariesBG(yRng,xRng)=max(unariesBG(yRng,xRng),bgLikeli);

  mask_l(yRng,xRng)=128;
end
lHoods.fg=unariesFG;
lHoods.bg=unariesBG;
