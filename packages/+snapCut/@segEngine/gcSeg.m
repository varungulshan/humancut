function seg=gcSeg(obj,lHoods,mask_l,img)

import snapCut.cpp.*

opts=obj.opts;
beta=obj.beta;
neglHood_fg=-log(lHoods.fg);
neglHood_bg=-log(lHoods.bg);

myUB=realmax/(opts.gcScale*100);

neglHood_fg(neglHood_fg>myUB)=myUB;
neglHood_bg(neglHood_bg>myUB)=myUB;
[seg,flow]=mex_gcBand(mask_l,neglHood_fg,neglHood_bg,opts.gcGamma,beta,...
                      obj.roffset',obj.coffset',opts.gcScale,opts.gcIsing,...
                      img);
