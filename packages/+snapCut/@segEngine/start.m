function ok=start(obj,labels,frameNum)
ok=true;
if(~strcmp(obj.state,'pped')),
  ok=false;
  warning('Cant start segmentation from %s state\n',obj.state);
  return;
end

if(frameNum~=1),
  warning('Cant start snapCut from %d frame number\n',frameNum);
  ok=false;
  return;
end

obj.frameNum=1;
obj.seg(:,:,1)=obj.gtSeg_firstFrame;
obj.framesSegmented=1;

obj.initBG_history();
obj.initFG_history();

obj.state='segStarted';
