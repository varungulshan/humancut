function img=view_10(obj)

frameNum=obj.frameNum;
seg=obj.seg(:,:,frameNum);
mask=(seg==255);

img=im2double(obj.vH.curFrame);

canvasColor=[0 0 1];
for i=1:3
  tmpImg=img(:,:,i);
  tmpImg(~mask)=canvasColor(i);
  img(:,:,i)=tmpImg;
end
