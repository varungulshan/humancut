function opts=makeOpts(optsString)

switch(optsString)
  case 'try_rad20'
    opts=snapCut.segEngine_opts();
    opts.gcGamma=20;
    opts.gcScale=1000;
    opts.gcIsing=0.1;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.winRad=20;

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.gmmUni_value=1; % assuming features in [0,1]
    opts.gmmLikeli_gamma=0.05;
   case 'try_rad30'
    opts=snapCut.segEngine_opts();
    opts.gcGamma=20;
    opts.gcScale=1000;
    opts.gcIsing=0.1;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.winRad=30;

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.gmmUni_value=1; % assuming features in [0,1]
    opts.gmmLikeli_gamma=0.05;
   case 'try_rad40'
    opts=snapCut.segEngine_opts();
    opts.gcGamma=20;
    opts.gcScale=1000;
    opts.gcIsing=0.1;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.winRad=40;

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.gmmUni_value=1; % assuming features in [0,1]
    opts.gmmLikeli_gamma=0.05;
  otherwise
    error('Invalid options string %s\n',optsString);
end
