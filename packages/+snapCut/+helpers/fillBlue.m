function img=fillBlue(img,mask)
% Fills a blue color in the mask (if its a 3 channel image)
% fills white in a single channel image
%
% mask is h x w x 1, img can be h x w x (1|3)
% img should be uint8 

[h w nCh]=size(img);
if(nCh==1)
  img(mask)=255;
elseif(nCh==3)
blueImg=zeros([h w 3],'uint8');
blueImg(:,:,3)=255;
mask=repmat(mask,[1 1 3]);
img(mask)=blueImg(mask);
else
  error('Invalid number of channels in fillBlue\n');
end
