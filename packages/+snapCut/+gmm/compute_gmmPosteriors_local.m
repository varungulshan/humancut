function posteriors=compute_gmmPosteriors_local(features,localGmm,imgLikeli)
% Function to compute posteriors for the localGmm (given the likelihood of the image)
% Inputs:
%  features: a D x N array of features (D=dimensionality)
%  localGmm: gmm structure for the local gmm
%  imgLikeli: 1 x N double array of image likelihoods

localLikeli=computeGmm_likelihood(features,localGmm);

divByZero=(localLikeli==0 & imgLikeli==0);
localLikeli(divByZero)=1;
imgLikeli(divByZero)=1;
posteriors=localLikeli./(localLikeli+imgLikeli);
