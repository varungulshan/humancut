classdef bgHistory < handle
% Class to maintain and propagate background representation
% Uses pre-processed KLT tracks
  properties (SetAccess=private, GetAccess=public)
    bgImg % h x w x D img
    mask_unknown % h x w, logical
    mask_bg % h x w, logical, the current bg mask
    debugLevel
    frameNum
  end

  methods
    function obj=bgHistory(debugLevel,initImg,mask_unknown)
      obj.debugLevel=debugLevel;
      obj.bgImg=initImg;
      obj.mask_unknown=mask_unknown;
      obj.mask_bg=~mask_unknown;
      obj.frameNum=1;
      [h w nCh]=size(initImg);
      %obj.bgImg(repmat(mask_unknown,[1 1 nCh]))=0;
      obj.bgImg=snapCut.helpers.fillBlue(obj.bgImg,mask_unknown);
    end
    warpBG=propagate(obj,nxtFrame)
    update(obj,nxtSeg,nxtFrame,computedWarp);
  end

  methods (Access=private)
    initKLT(obj)
  end

end % of classdef
