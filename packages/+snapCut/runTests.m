function runTests()

% ---------- config options for runTests -----------
% Give paths relative to this file, it will be converted to absolute path later on
firstFrameGT_dir='../../data/gtFirstFrame/';
rootOut_dir='../../results/snapCut_visualize/';
[videoPaths,videoFiles]=testBench.getTests_20frames();
visualize=false;
views=[2 3 4 10];
optsString='try_rad20';
evalOpts_string='try1';
% ---------- end config options for runTests -------

cwd=miscFns.extractDirPath(mfilename('fullpath'));
rootOut_dir=[cwd rootOut_dir optsString '/'];
firstFrameGT_dir=[cwd firstFrameGT_dir];

videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);
for i=1:length(videoNames)
  firstFrameGT_files{i}=[firstFrameGT_dir videoNames{i} '.png'];
  outDirs{i}=[rootOut_dir videoNames{i} '/'];
end

segOpts=snapCut.helpers.makeOpts(optsString);

for i=1:length(videoNames)
  fprintf('\nNow running on %s\n',[videoPaths{i} videoFiles{i}]);
  snapCut.runSeg_offline(videoPaths{i},videoFiles{i},firstFrameGT_files{i},...
                         outDirs{i},visualize,views,segOpts);
end

testBench.computeStats(rootOut_dir,videoNames,evalOpts_string);
testBench.plotStats(rootOut_dir);
