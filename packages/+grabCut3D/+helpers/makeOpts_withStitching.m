function opts=makeOpts_withStitching(optsString)
% Make options for grabCut3D with stitching

switch(optsString)
   case 'try1_21frames'
     opts.tSlice=22;
     opts.opts_string='try1';
   case '21frames_iter10_2'
     opts.tSlice=22;
     opts.opts_string='iter10_2';
   case '21frames_iter10_2_noTemp'
     opts.tSlice=22;
     opts.opts_string='iter10_2_noTemporal';
  otherwise
    error('Invalid options string %s\n',optsString);
end
