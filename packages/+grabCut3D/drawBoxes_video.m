function drawBoxes_video()
% Function to draw bounding boxes on a video to test grabCut3D

cwd=miscFns.extractDirPath(mfilename('fullpath'));

boxMargin=30;
videoPath=[cwd '../../data/snapCut_videos/footballer_cut2/'];
videoName='00001.png';
boxDir=[cwd '../../data/misc/footballer_cut2boxes/'];

vH=myVideoReader(videoPath,videoName);
nFrames=vH.nFrames;

if(~exist(boxDir,'dir')), mkdir(boxDir); end;

figH=figure;
cmap_labels=jet(7);
for i=1:nFrames
  img=vH.curFrame;
  imshow(img); 
  fprintf('Draw bounding box for Frame #%03d\n',i);
  
  k = waitforbuttonpress;
  point1 = get(gca,'CurrentPoint');    % button down detected
  finalRect = rbbox;                   % return figure units
  point2 = get(gca,'CurrentPoint');    % button up detected
  point1 = point1(1,1:2);              % extract x and y
  point2 = point2(1,1:2);
  p1 = round(min(point1,point2));             
  p2 = round(max(point1,point2));
  
  [h w nCh]=size(img);
  labelImg=6*ones([h w],'uint8');
  
  xT=max(1,p1(1)-boxMargin);yT=max(1,p1(2)-boxMargin);
  xB=min(w,p2(1)+boxMargin);yB=min(h,p2(2)+boxMargin);
  labelImg(yT:yB,xT:xB)=2;
  
  xT=max(1,p1(1));yT=max(1,p1(2));
  xB=min(w,p2(1));yB=min(h,p2(2));
  
  labelImg(yT:yB,xT:xB)=3;

  imwrite(labelImg,cmap_labels,[boxDir sprintf('%05d.png',i)],'png');

  if(i~=nFrames)
    vH.moveForward();
  end
end

close(figH);
