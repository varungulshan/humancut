function [gmmf,gmmb]=updateColorModels(img,seg,gmmf,gmmb,updateIters,labelImg)

nFrames=size(img,4);

for i=1:nFrames

  tmpLabels=labelImg(:,:,i);
  mask=(tmpLabels~=5 & tmpLabels~=6);
  mask=mask(:);
  features=grabCut3D.extractPixels(double(img(:,:,:,i)));

  tmpSeg=seg(:,:,i);
  tmpFeatures=features(:,tmpSeg(:)==255 & mask);
  gmmf{i}=gmm.updateGmm(tmpFeatures,gmmf{i},updateIters);
  
  tmpFeatures=features(:,tmpSeg(:)==0 & mask);
  gmmb{i}=gmm.updateGmm(tmpFeatures,gmmb{i},updateIters);

end
