% Copyright (C) 2010 Varun Gulshan
% This class defines the options for grabCut3D segmentation
classdef segOpts
  properties (SetAccess=public, GetAccess=public)
    gcGamma_e  % gamma of graph cuts (for contrast dependant term), spatial
    gcGamma_i  % gamma of graph cuts (for ising model term), spatial

    gcGammaT_e  % gamma of graph cuts (for contrast dependant term), temporal
    gcGammaT_i  % gamma of graph cuts (for ising model term), temporal
    
    gcScale  % scaling to convert to integers
    gcNbrType_spatial % 'nbr4' or 'nbr8', in space
    gcNbrType_time % only 'nbr1' supported right now

    gcSigma_c % 'auto' or a numeric value, in spatial domain, the temporal domain right now
              % is the same as spatial domain

    gmmNmix_fg % number of gmm mixtures for fg, usually 5
    gmmNmix_bg % number of gmm mixtures for bg, usually 5
    gmmUpdate_iters % number of iterations to update GMM in the EM algorithm

    postProcess % 0 = off, 1 = on
    numIters % Number of iterations to run grabCut for
  end

  methods
    function obj=segOpts()
      % Set the default options
      obj.gcGamma_e=140;
      obj.gcGamma_i=5;
      obj.gcGammaT_e=140;
      obj.gcGammaT_i=5;

      obj.gcScale=50;
      obj.gcNbrType_spatial='nbr8';
      obj.gcNbrType_time='nbr1';
      obj.gcSigma_c='auto';

      obj.gmmNmix_fg=5;
      obj.gmmNmix_bg=5;
      obj.postProcess=0;
      obj.numIters=10;
      
      obj.gmmUpdate_iters=1;
      
    end
  end

end % of classdef
