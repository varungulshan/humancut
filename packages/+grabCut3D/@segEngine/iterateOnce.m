function iterateOnce(obj)
% Run one iteration of grabCut
% first update color model, then update segmentation

if(~strcmp(obj.state,'started')),
  error('Can only call iterateOnce, once state is started\n');
end

[obj.gmmf,obj.gmmb]=grabCut3D.updateColorModels(obj.img,obj.seg,obj.gmmf,obj.gmmb,...
                    obj.opts.gmmUpdate_iters,obj.labelImg);

%[unaryImg,unaryMask]=obj.createUnaryImg();
%obj.seg=obj.gcSeg(unaryImg,unaryMask);
obj.seg=obj.segmentVolume();
