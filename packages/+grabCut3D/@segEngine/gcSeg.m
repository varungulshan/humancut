function seg=gcSeg(obj,unaryImg,unaryMask)

import grabCut3D.cpp.*

objOpts=obj.opts;

opts.gcGamma_e=objOpts.gcGamma_e;
opts.gcGamma_i=objOpts.gcGamma_i;
opts.gcGammaT_e=objOpts.gcGammaT_e;
opts.gcGammaT_i=objOpts.gcGammaT_i;
opts.gcScale=objOpts.gcScale;
opts.beta=obj.beta;
opts.beta_t=obj.beta_t;
opts.xoffset=obj.xoffset';
opts.yoffset=obj.yoffset';
opts.zoffset=obj.zoffset';

if(isempty(obj.graphInfo))
  [seg,flow,graphInfo]=mex_dgcBand3D_init(unaryMask,unaryImg,obj.img,opts);
  obj.graphInfo=graphInfo;
else
  [seg,flow]=mex_dgcBand3D_repeat(unaryMask,unaryImg,obj.graphInfo,opts);
end
