function start(obj,labelImg)

if(~strcmp(obj.state,'pped')),
  error('Can only call start if state is pped\n');
end
[h,w,nFrames]=size(labelImg);

obj.state='started';
obj.labelImg=labelImg;

[obj.gmmf,obj.gmmb]=grabCut3D.initColorModels(obj.img,labelImg,obj.opts.gmmNmix_fg,...
                                              obj.opts.gmmNmix_bg);
%[unaryImg,unaryMask]=obj.createUnaryImg();
%obj.seg=obj.gcSeg(unaryImg,unaryMask);
obj.seg=obj.segmentVolume();

% One iteration is over already
for i=2:obj.opts.numIters
  obj.iterateOnce();
end
