function compile_mex()

cwd=miscFns.extractDirPath(mfilename('fullpath'));

mexCmds=cell(0,1);
mexCmds{end+1}=sprintf('mex -O %s+cpp/mex_setupTransductionGraph.cpp -outdir %s+cpp/',cwd,cwd);
mexCmds{end+1}=sprintf('mex -O -I%s+cpp/graphCut/ %s+cpp/graphCut/mex_gcBand3D.cpp %s+cpp/graphCut/graph.cpp -outdir %s+cpp/',cwd,cwd,cwd,cwd);
mexCmds{end+1}=sprintf('mex -O -I%s+cpp/graphCut/ %s+cpp/graphCut/mex_dgcBand3D_init.cpp %s+cpp/graphCut/graph.cpp -outdir %s+cpp/',cwd,cwd,cwd,cwd);
mexCmds{end+1}=sprintf('mex -O -I%s+cpp/graphCut/ %s+cpp/graphCut/mex_dgcBand3D_repeat.cpp %s+cpp/graphCut/graph.cpp -outdir %s+cpp/',cwd,cwd,cwd,cwd);
mexCmds{end+1}=sprintf('mex -O -I%s+cpp/graphCut/ %s+cpp/graphCut/mex_dgcBand3D_cleanUp.cpp %s+cpp/graphCut/graph.cpp -outdir %s+cpp/',cwd,cwd,cwd,cwd);

for i=1:length(mexCmds)
  fprintf('Executing %s\n',mexCmds{i});
  eval(mexCmds{i});
end
