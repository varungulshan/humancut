function [hOptModel,hOpt_opts]=createHumanModel_optimizer(opts,debugOpts)

switch(opts.humanModel_optimizeMethod)
  case 'dumbGradientDescent'
    hOpt_opts=humanOptimizers.dumbGradientDescent_opts(opts.humanModel_optimizeMethod_opts);
    hOptModel=humanOptimizers.dumbGradientDescent(debugOpts,hOpt_opts);
  case 'fminuncDescent'
    hOpt_opts=humanOptimizers.fminuncDescent_opts(opts.humanModel_optimizeMethod_opts);
    hOptModel=humanOptimizers.fminuncDescent(debugOpts,hOpt_opts);
  case 'fminuncDescent3D_skipInit'
    hOpt_opts=humanOptimizers.fminuncDescent3D_skipInit_opts(opts.humanModel_optimizeMethod_opts);
    hOptModel=humanOptimizers.fminuncDescent3D_skipInit(debugOpts,hOpt_opts);
  case 'fminuncDescent3D_partModels'
    hOpt_opts=humanOptimizers.fminuncDescent3D_partModels_opts(opts.humanModel_optimizeMethod_opts);
    hOptModel=humanOptimizers.fminuncDescent3D_partModels(debugOpts,hOpt_opts);
  case 'fminuncDescent3D_indep'
    hOpt_opts=humanOptimizers.fminuncDescent3D_indep_opts(opts.humanModel_optimizeMethod_opts);
    hOptModel=humanOptimizers.fminuncDescent3D_indep(debugOpts,hOpt_opts);
  case 'fminuncDescent3D_indepC'
    hOpt_opts=humanOptimizers.fminuncDescent3D_indepC2_opts(opts.humanModel_optimizeMethod_opts);
    hOptModel=humanOptimizers.fminuncDescent3D_indepC2(debugOpts,hOpt_opts);
  case 'fminsearchDescent'
    hOpt_opts=humanOptimizers.fminsearchDescent_opts(opts.humanModel_optimizeMethod_opts);
    hOptModel=humanOptimizers.fminsearchDescent(debugOpts,hOpt_opts);
  case 'fminsearchDescent3D_partModels'
    hOpt_opts=humanOptimizers.fminsearchDescent3D_partModels_opts...
              (opts.humanModel_optimizeMethod_opts);
    hOptModel=humanOptimizers.fminsearchDescent3D_partModels(debugOpts,hOpt_opts);
  case 'fminunc_partsAndTracks'
    hOpt_opts=humanOptimizers.fminunc_partsAndTracks_opts...
              (opts.humanModel_optimizeMethod_opts);
    hOptModel=humanOptimizers.fminunc_partsAndTracks(debugOpts,hOpt_opts);
  otherwise
    error('Invalid human model optimization method: %s\n',opts.humanModel_optimizeMethod);
end
