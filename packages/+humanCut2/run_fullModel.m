function run_fullModel(params)
% Runs tests on a dataset , 
% All three parameters (color model,segmentation and puppet) are optimized
% in an alternating fashion

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('params','var'))
  params.rootOut_dir=[cwd '../../results/humanCut2_fullModel/grabCut/'];
  %params.opts.h3dAnno_dir=[cwd '../../data/h3dAnnotations_2/'];
  params.opts.h3dAnno_dir=[cwd '../../data/h3dAnnotations_2_tiny/'];

  params.opts.method='partModels3D_skipInit';
  params.opts.numIter_proxyUpdates=1;
  params.opts.stitchSize=2; % process chunks of 2 frames at a time
  params.opts.uninitializedZone=1000; % a radius of 1 frame around every initialzed
  % frame. that means for every 3 frames, the middle frame is intiialized perfectly
  % and the 2 neighbours are initialized from the middle frame

  %params.opts.method='stitching3D_exactInit';
  %params.opts.stitchSize=2; % process chunks of 2 frames at a time
  %params.opts.numIter_proxyUpdates=1;

  params.opts.humanModel_type='jointsModel3D_partsSmooth';
  params.opts.humanModel_optsString='try1';
  params.opts.segMethod='grabCut_partFG';
  params.opts.segMethod_optsString='iter5_2';
  %params.opts.humanModel_optimizeMethod='dumbGradientDescent';
  %params.opts.humanModel_optimizeMethod_opts='try1';
  params.opts.humanModel_optimizeMethod='fminsearchDescent3D_partModels';
  params.opts.humanModel_optimizeMethod_opts='try1_tiny';

  params.opts.visualize=true;
  params.opts.visOpts.debugVisualize=true;
  params.opts.debugLevel=10;
  params.opts.visOpts.imgExt='jpg';
  params.opts.visOpts.segBdryWidth=2;
  params.overWrite=true;

  params.evalOpts_string='try_proxy1';
  params.testBench_cmd='testBench.getTests_proxy2_tiny()';
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',params.rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
end

timingFile=[params.rootOut_dir 'timing.txt'];
fH_time=fopen(timingFile,'w');
[videoPaths,videoFiles]=eval(params.testBench_cmd);

videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);
for i=1:length(videoNames)
  outDirs{i}=[params.rootOut_dir videoNames{i} '/'];
end

stTime_global=clock;

parfor i=1:length(videoNames)
%for i=1:length(videoNames)  
  fprintf('\nNow running on %s\n',[videoPaths{i} videoFiles{i}]);
  %stTime=clock;
  runOnVideo(videoPaths{i},videoFiles{i},outDirs{i},params.opts,params.overWrite);
  %vidTime=etime(clock,stTime);
  %fprintf(fH_time,'Time to process video %s = %.2f minutes ( = %.2f hours)\n',...
                  %videoNames{i},vidTime/60,vidTime/3600);
end

fprintf(fH_time,'\n\nTotal time to run on all videos = %.2f hours\n',...
        etime(clock,stTime_global)/3600);
fclose(fH_time);

testBench.computeStats_humanCut(params.rootOut_dir,params.evalOpts_string);
%testBench.plotStats(rootOut_dir);

function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);

opts=params.opts;
if(opts.visualize & opts.visOpts.debugVisualize)
  debugOpts.debugLevel=opts.debugLevel;
else
  debugOpts.debugLevel=0;
end
debugOpts.debugDir='';
debugOpts.debugPrefix='';

fH=fopen([params.rootOut_dir 'opts_humanModel.txt'],'w');
[hModel,hModel_opts]=humanCut2.createHumanModel(opts,debugOpts);
miscFns.printClass(hModel_opts,fH);
delete(hModel);
fclose(fH);

fH=fopen([params.rootOut_dir 'opts_segModel.txt'],'w');
[segH,segH_opts]=humanCut2.createSegObject(opts,debugOpts);
miscFns.printClass(segH_opts,fH);
delete(segH);
fclose(fH);

fH=fopen([params.rootOut_dir 'opts_humanOptimizer.txt'],'w');
[optimizer,optimizer_opts]=humanCut2.createHumanModel_optimizer(opts,debugOpts);
miscFns.printClass(optimizer_opts,fH);
delete(optimizer);
fclose(fH);

function runOnVideo(videoPath,videoFile,outDir,opts,overWrite)

switch(opts.method)
  case 'frameByFrame_exactInit'
    runFrameByFrame_exactInit(videoPath,videoFile,outDir,opts,overWrite);
  case 'full3D_exactInit'
    humanCut2.runFull3D_exactInit(videoPath,videoFile,outDir,opts,overWrite);
  case 'stitching3D_exactInit'
    humanCut2.runStitching3D_exactInit(videoPath,videoFile,outDir,opts,overWrite);
  case {'stitching3D_skipInit','partModels3D_skipInit'}
    humanCut2.runStitching3D_skipInit(videoPath,videoFile,outDir,opts,overWrite);
  otherwise
    error('Invalid full model method %s\n',opts.method);
end

function runFrameByFrame_exactInit(videoPath,videoFile,outDir,opts,overWrite)

if(exist(outDir,'dir')),
  if(~overWrite)
    fprintf('Cannot overwrite exisiting results in: %s\n',outDir);
    return;
  else
    fprintf('\n\n---- Overwriting results in %s----------\n\n\n',outDir);
  end
else
  mkdir(outDir);
end

trackOut_dir=[outDir 'track001_files/'];
if(~exist(trackOut_dir,'dir')), mkdir(trackOut_dir); end;

videoName=miscFns.extractVideoNames({videoPath},{videoFile});
videoName=videoName{1};
[annots,K]=h3dAnno.loadAnnotations(videoPath,videoFile,videoName,opts.h3dAnno_dir);
vH=myVideoReader(videoPath,videoFile);

allSeg=zeros([vH.h vH.w vH.nFrames],'uint8');

assert(vH.nFrames==length(annots));

% --- Process each frame individually -----
for i=1:length(annots)
  img=im2double(vH.curFrame);

  if(opts.visualize & opts.visOpts.debugVisualize)
    debugOpts.debugLevel=opts.debugLevel;
    debugOpts.debugDir=[trackOut_dir 'debug/'];
    if(~exist(debugOpts.debugDir,'dir')), mkdir(debugOpts.debugDir); end;
    debugOpts.debugPrefix=sprintf('%03d_',i);
  else
    debugOpts.debugLevel=0;
    debugOpts.debugDir='';
    debugOpts.debugPrefix='';
  end

  % --- Initializations:
  % First initialize humanModel
  % Then initialize color model
  % Then initialize segmentation
  [hModel,hModel_opts]=humanCut2.createHumanModel(opts,debugOpts);
  hModel.initialize('h3d',annots(i),K);
  [segH,segH_opts]=humanCut2.createSegObject(opts,debugOpts);
  segH.initializeFull(img,hModel);
  [hOptModel,hOptsModel_opts]=humanCut2.createHumanModel_optimizer(opts,debugOpts);

  if(debugOpts.debugLevel>0)
    curEnergy=segH.getEnergy_fixedSeg(hModel);
    segOverlay=humanCut.overlaySeg(img,segH.seg,opts.visOpts.segBdryWidth);
    humanOverlay=hModel.drawModel(segOverlay);

    humanOverlay=miscFns.mex_drawText(im2uint8(humanOverlay),int32(15),...
                 sprintf('Energy=%d',curEnergy));
    miscFns.saveDebug(debugOpts,humanOverlay,sprintf('segProxyOverlay-%03d.%s',0,...
                      opts.visOpts.imgExt));
                    
    fprintf('Human model, segmentation and color models initialized\n');                
    fprintf('Initialization energy: %d\n',curEnergy);
  end

  % -- Human model, segmentation and color models have all been initialized
  % -- now iterate

  for j=1:opts.numIter_proxyUpdates
    hOptModel.updateHumanModel(hModel,segH);
    segH.updateSeg_andColorModel(hModel);

    if(debugOpts.debugLevel>0)
      curEnergy=segH.getEnergy_fixedSeg(hModel);
      segOverlay=humanCut.overlaySeg(img,segH.seg,opts.visOpts.segBdryWidth);
      humanOverlay=hModel.drawModel(segOverlay);
      humanOverlay=miscFns.mex_drawText(im2uint8(humanOverlay),int32(15),...
                   sprintf('Energy=%d',curEnergy));
      miscFns.saveDebug(debugOpts,humanOverlay,sprintf('segProxyOverlay-%03d.%s',j,...
                        opts.visOpts.imgExt));
      fprintf('Energy after %03d updates of human model, color models and segmentations: %d\n',j,curEnergy);                
    end
  end

  allSeg(:,:,i)=segH.seg;

  if(i~=vH.nFrames)
    vH.moveForward();
  end
  delete(hModel);
  delete(segH);
  delete(hOptModel);

end

trackSt=1;
trackEnd=vH.nFrames;
delete(vH);
seg=allSeg;
save([trackOut_dir 'seg.mat'],'seg','trackSt','trackEnd'); % Need to add the track variables
%
