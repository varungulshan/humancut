function runFull3D_exactInit(videoPath,videoFile,outDir,opts,overWrite)

if(exist(outDir,'dir')),
  if(~overWrite)
    fprintf('Cannot overwrite exisiting results in: %s\n',outDir);
    return;
  else
    fprintf('\n\n---- Overwriting results in %s----------\n\n\n',outDir);
  end
else
  mkdir(outDir);
end

trackOut_dir=[outDir 'track001_files/'];
if(~exist(trackOut_dir,'dir')), mkdir(trackOut_dir); end;

videoName=miscFns.extractVideoNames({videoPath},{videoFile});
videoName=videoName{1};
[annots,K]=h3dAnno.loadAnnotations(videoPath,videoFile,videoName,opts.h3dAnno_dir);
vH=myVideoReader(videoPath,videoFile);

[h w nCh nFrames]=vH.videoSize();
fullVideo=zeros([h w nCh nFrames],'uint8');

assert(vH.nFrames==length(annots));

for i=1:nFrames-1
  fullVideo(:,:,:,i)=vH.curFrame;
  vH.moveForward();
end
fullVideo(:,:,:,nFrames)=vH.curFrame;

delete(vH);

if(opts.visualize & opts.visOpts.debugVisualize)
  debugOpts.debugLevel=opts.debugLevel;
  debugOpts.debugDir=[trackOut_dir 'debug/'];
  if(~exist(debugOpts.debugDir,'dir')), mkdir(debugOpts.debugDir); end;
  debugOpts.debugPrefix='';
else
  debugOpts.debugLevel=0;
  debugOpts.debugDir='';
  debugOpts.debugPrefix='';
end

[hModel,hModel_opts]=humanCut2.createHumanModel(opts,debugOpts);
hModel.initialize('h3d',annots,K);
[segH,segH_opts]=humanCut2.createSegObject(opts,debugOpts);
segH.initializeFull(fullVideo,hModel);
[hOptModel,hOptsModel_opts]=humanCut2.createHumanModel_optimizer(opts,debugOpts);

if(debugOpts.debugLevel>0)
  for(i=1:segH.nFrames)
    posterior=segH.getColorPosterior(i);
    miscFns.saveDebug(debugOpts,posterior,sprintf('%03d_posterior_iter%03d.jpg',i,0));
    segOverlay=humanCut.overlaySeg(im2double(fullVideo(:,:,:,i)),...
              segH.seg(:,:,i),opts.visOpts.segBdryWidth);
    humanOverlay=hModel.drawModel(segOverlay,i);

    miscFns.saveDebug(debugOpts,humanOverlay,sprintf('%03d_segProxyOverlay-%03d.%s',i,...
                      0,opts.visOpts.imgExt));
  
  end
end

for j=1:opts.numIter_proxyUpdates
  hOptModel.updateHumanModel(hModel,segH);
  segH.updateSeg_andColorModel(hModel);
  
  if(debugOpts.debugLevel>0)
    for(i=1:segH.nFrames)
      posterior=segH.getColorPosterior(i);
      miscFns.saveDebug(debugOpts,posterior,sprintf('%03d_posterior_iter%03d.jpg',i,j));
      segOverlay=humanCut.overlaySeg(im2double(fullVideo(:,:,:,i)),...
                segH.seg(:,:,i),opts.visOpts.segBdryWidth);
      humanOverlay=hModel.drawModel(segOverlay,i);
  
      miscFns.saveDebug(debugOpts,humanOverlay,sprintf('%03d_segProxyOverlay-%03d.%s',i,...
                        j,opts.visOpts.imgExt));
    
    end
  end
  
end

seg=segH.seg;
delete(segH);
delete(hModel);
delete(hOptModel);

trackSt=1;
trackEnd=nFrames;
save([trackOut_dir 'seg.mat'],'seg','trackSt','trackEnd'); % Need to add the track variables
%
