This package is attempt at implementing segmentation of humans using a proxy (like a stick man) to help the segmentation.

To start labeling a proxy:
1. Call h3dAnno.setupImageList (correct the parameters to what you need).
2. Call h3dAnno.annotateVideo (along with loading lubomir's tool in parallel).
3. Once annotated, called h3dAnno.copyAnnotations to copy over the annotations to the right place.

Right now, the proxy is input by hand using lubomir bourdev's tool. This tool lets you input all the joints, mark the depth of the joints (one of near, equal and far, so max. 3 layers), and also mark the visibility of joints. Here is the guidelines I followed to mark the points:

Regarding depth of points:
1. Always kept the torso at depth=equal, and the limbs are either near or far.

Regarding occluded points:
Parts that are totally occluded are not marked at all.
For partial occlusions, following guidelines apply:

3 cases for occlusion:
1. Self-occlusion: If a part of person is occluded by another part, i mark the occluded part, giving the point the correct depth. Note that I still call the point visible. (the label invisible is used for other cases as you will see next).

2. Image boundary: If a part of a person is cut-off by image boundary, i still label the part (with a reasonable guess). This time I call the point as invisible.

3. Occlusion by another object: If a part of a person is occluded by another object, I label it if I think enough percentage of the part is visible (say atleast 80% of part is visible). I do label the point as invisible though, to indicate a possible third party occlusion of the part. This case a bit of a gray area for annotation, so description might be inconsistent with actual labelings :).
