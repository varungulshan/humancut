function [segH,segOpts]=createSegObject(opts,debugOpts)

switch(opts.segMethod)
  case 'grabCut_humanModel'
    segOpts=grabCut_humanModel.helpers.makeOpts(opts.segMethod_optsString);
    segH=grabCut_humanModel.segEngine(segOpts,debugOpts);
  case 'grabCut_singleFGmodel'
    segOpts=humanSegmenters.grabCut_singleFGmodel.segOpts(opts.segMethod_optsString);
    segH=humanSegmenters.grabCut_singleFGmodel.segEngine(segOpts,debugOpts);
  case 'grabCut_singleFG_skipInit'
    segOpts=humanSegmenters.grabCut_singleFG_skipInit.segOpts(opts.segMethod_optsString);
    segH=humanSegmenters.grabCut_singleFG_skipInit.segEngine(segOpts,debugOpts);
  case 'grabCut_partFG'
    segOpts=humanSegmenters.grabCut_partFG.segOpts(opts.segMethod_optsString);
    segH=humanSegmenters.grabCut_partFG.segEngine(segOpts,debugOpts);
  case 'grabCut_singleFGmodelC'
    segOpts=humanSegmenters.grabCut_singleFGmodelC2.segOpts(opts.segMethod_optsString);
    segH=humanSegmenters.grabCut_singleFGmodelC2.segEngine(segOpts,debugOpts);
  otherwise
    error('Invalid segmentation method: %s\n',opts.segMethod);
end
