function runStitching3D_exactInit(videoPath,videoFile,outDir,opts,overWrite)

if(exist(outDir,'dir')),
  if(~overWrite)
    fprintf('Cannot overwrite exisiting results in: %s\n',outDir);
    return;
  else
    fprintf('\n\n---- Overwriting results in %s----------\n\n\n',outDir);
  end
else
  mkdir(outDir);
end

trackOut_dir=[outDir 'track001_files/'];
if(~exist(trackOut_dir,'dir')), mkdir(trackOut_dir); end;

videoName=miscFns.extractVideoNames({videoPath},{videoFile});
videoName=videoName{1};
[annots,K]=h3dAnno.loadAnnotations(videoPath,videoFile,videoName,opts.h3dAnno_dir);
vH=myVideoReader(videoPath,videoFile);

[h w nCh nFrames]=vH.videoSize();
stitchSize=opts.stitchSize;

%blockSeg=zeros([h w 0],'uint8');
seg=zeros([h w nFrames],'uint8');

done=false;
numLoaded=0;
idxStart=1;
nxtFrame=1;
blockNum=1;

blockVideo=zeros([h w nCh 0],'uint8');
lastSeg=[];
lastHumanModel=[];

while(~done)
  % assert: idxStart is the frame where the stitching starts
  % assert: nxtFrame is the frame of the video which will be loaded next
  % nxtFrame==idxStart+numLoaded
  % block video is of size (h x w x nCh x numLoaded)
  while(numLoaded<stitchSize)
    % assert: nxtFrame is a valid frame to be loaded
    numLoaded=numLoaded+1;
    blockVideo(:,:,:,numLoaded)=vH.curFrame;
    
    nxtFrame=nxtFrame+1;
    if(nxtFrame>nFrames)
      done=true;
      break;
    else
      vH.moveForward();
    end
  end

  % assert: nxtFrame=idxStart+numLoaded
  % assert: the block to be segmented is
  % [idxStart,nxtFrame-1] (boundaries included)

  % Setup constraint variables (only for blockNum>1)
  if(idxStart~=1)
    assert(~isempty(lastHumanModel),'Last human model should not be empty\n');
    hModel_constraintIdx=1;
    hModel_constraintVectors=lastHumanModel;

    assert(~isempty(lastSeg),'Last seg should not be empty\n');
    segH_constraintIdx=1;
    segH_constraintSegs={lastSeg};
  else
    hModel_constraintIdx=[];
    hModel_constraintVectors=[];
    segH_constraintIdx=[];
    segH_constraintSegs=cell(1,0);
  end

  debugOpts=makeDebugOpts(opts,trackOut_dir,blockNum);

  [hModel,hModel_opts]=humanCut2.createHumanModel(opts,debugOpts);
  tmpAnnots=annots(idxStart:(nxtFrame-1));
  hModel.initialize('h3d',tmpAnnots,K,hModel_constraintIdx,hModel_constraintVectors);

  [segH,segH_opts]=humanCut2.createSegObject(opts,debugOpts);
  segH.initializeFull(blockVideo,hModel,segH_constraintIdx,segH_constraintSegs);

  [hOptModel,hOptsModel_opts]=humanCut2.createHumanModel_optimizer(opts,debugOpts);

  for j=1:opts.numIter_proxyUpdates
    hOptModel.updateHumanModel(hModel,segH);
    segH.updateSeg_andColorModel(hModel);
  end

  % Save results from this block's computation
  seg(:,:,idxStart:(nxtFrame-1))=segH.seg;
  lastSeg=segH.seg(:,:,end);
  lastHumanModel=hModel.modelVector(:,end);

  % Clean up the current block processing and setup for the next block
  delete(segH);delete(hModel);delete(hOptModel);

  blockVideo=blockVideo(:,:,:,end);
  idxStart=nxtFrame-1;
  numLoaded=1;

  blockNum=blockNum+1;
end

delete(vH);

trackSt=1;
trackEnd=nFrames;
save([trackOut_dir 'seg.mat'],'seg','trackSt','trackEnd'); % Need to add the track variables
%

function debugOpts=makeDebugOpts(opts,trackDir,blockNum)

if(opts.visualize & opts.visOpts.debugVisualize)
  debugOpts.debugLevel=opts.debugLevel;
  debugOpts.debugDir=[trackDir 'debug/'];
  if(~exist(debugOpts.debugDir,'dir')), mkdir(debugOpts.debugDir); end;
  debugOpts.debugPrefix=sprintf('Block%02d_',blockNum);;
else
  debugOpts.debugLevel=0;
  debugOpts.debugDir='';
  debugOpts.debugPrefix='';
end
