/*=================================================================
% Draws medial axis using openCV line drawing functions
% 
% labelImg=mex_addMedialAxis(labelImg,medialAxis,axisWidth,axisLabel)
% Inputs:
%   labelImg -> [h x w] uint8, current labelImg, the label 1 will not be overwritten
%               all other labels will be
%   medialAxis -> [4 x 1] double ([x1 y1 x2 y2] format medial axis)
%   axisWidth  -> scalar int32 width of axis 
%   axisLabel  -> scalar uint8 label to draw
*=================================================================*/

#include "mex.h"
#include "cv.h"

void matlabTo_openCV_matrix(uchar* img,cv::Mat &cvImg,int h,int w,int nCh);
void openCV_toMatlab_matrix(cv::Mat inImg,uchar *outImg,int h,int w,int nCh);

void mexFunction(	int nlhs, mxArray *plhs[], 
				 int nrhs, const mxArray*prhs[] ) 
{ 
	/* retrive arguments */
	if( nrhs!=4 ) 
		mexErrMsgTxt("4 input arguments are required."); 
	if( nlhs!=1 ) 
		mexErrMsgTxt("1 output arguments are required."); 

  if(mxGetClassID(prhs[0])!=mxUINT8_CLASS)
    mexErrMsgTxt("prhs[0] (labelImg) should be uint8\n");
  if(mxGetClassID(prhs[1])!=mxDOUBLE_CLASS)
    mexErrMsgTxt("prhs[1] (medialAxis) should be double\n");
  if(mxGetClassID(prhs[2])!=mxINT32_CLASS)
    mexErrMsgTxt("prhs[2] (axis width) should be int32\n");
  if(mxGetClassID(prhs[3])!=mxUINT8_CLASS)
    mexErrMsgTxt("prhs[3] (label) should be uint8\n");

	int h = mxGetM(prhs[0]); 
	int w = mxGetN(prhs[0]);
  int nPixels=h*w;

  if(mxGetNumberOfElements(prhs[1])!=4)
    mexErrMsgTxt("Dimension of medialAxis incorrect!\n");

  unsigned char *labelImg=(unsigned char*)mxGetData(prhs[0]);
  plhs[0]=mxCreateNumericMatrix(h,w,mxUINT8_CLASS,mxREAL);

  unsigned char *outLabels=(unsigned char*)mxGetData(plhs[0]);
  //memcpy(outLabels,labelImg,nPixels*sizeof(unsigned char));

  int axisWidth=*(int*)mxGetData(prhs[2]);
  unsigned char axisLabel=*(unsigned char*)mxGetData(prhs[3]);

  cv::Mat cvLabelImg=cv::Mat(h,w,CV_8UC1);
  matlabTo_openCV_matrix(labelImg,cvLabelImg,h,w,1);
  double *medialAxis=mxGetPr(prhs[1]);
  int x1=(int)medialAxis[0];
  int y1=(int)medialAxis[1];
  int x2=(int)medialAxis[2];
  int y2=(int)medialAxis[3];
  cv::Point pt1=cv::Point(x1,y1);
  cv::Point pt2=cv::Point(x2,y2);

  cv::line(cvLabelImg,pt1,pt2,cv::Scalar((double)axisLabel),axisWidth,8);

  openCV_toMatlab_matrix(cvLabelImg,outLabels,h,w,1);

}

void matlabTo_openCV_matrix(uchar* img,cv::Mat &cvImg,int h,int w,int nCh){
  int N=h*w;
  for(int y=0;y<h;y++){
    uchar* rowY=cvImg.ptr<uchar>(y);
    for(int x=0;x<w;x++){
      for(int d=0;d<nCh;d++){
        (*rowY)=img[d*N+h*x+y];
        rowY++;
      }
    }
  }
}

void openCV_toMatlab_matrix(cv::Mat cvImg,uchar *outImg,int h,int w,int nCh){
  int N=h*w;
  for(int y=0;y<h;y++){
    uchar* rowY=cvImg.ptr<uchar>(y);
    for(int x=0;x<w;x++){
      for(int d=0;d<nCh;d++){
        outImg[d*N+h*x+y]=(*rowY);
        rowY++;
      }
    }
  }
}
