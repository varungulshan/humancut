function run_fullModelTracks(params)
% Builds on top of run_fullModel, adding tracks to the energy
% Runs tests on a dataset , 
% All three parameters (color model,segmentation and puppet) are optimized
% in an alternating fashion

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('params','var'))
  params.rootOut_dir=[cwd '../../results/humanCut2_fullModel/grabCut/'];
  %params.opts.h3dAnno_dir=[cwd '../../data/h3dAnnotations_2/'];
  params.opts.h3dAnno_dir=[cwd '../../data/h3dAnnotations_2_tiny/'];
  %params.opts.tracksDir=['/data/adam2/varun/kltTracks/'];
  params.opts.tracksDir=[cwd '../../data/kltTracks_tiny/'];

  params.opts.method='partModelsTracks_firstTry';
  params.opts.numIter_proxyUpdates=1;
  params.opts.stitchSize=2; % process chunks of 2 frames at a time
  params.opts.uninitializedZone=1; % a radius of 1 frame around every initialzed
  % frame. that means for every 3 frames, the middle frame is intiialized perfectly
  % and the 2 neighbours are initialized from the middle frame

  params.opts.humanModel_type='jointsModel3D_partsAndTracks';
  params.opts.humanModel_optsString='try1';
  params.opts.segMethod='grabCut_partFG';
  params.opts.segMethod_optsString='iter5_2';
  params.opts.humanModel_optimizeMethod='fminunc_partsAndTracks';
  params.opts.humanModel_optimizeMethod_opts='try1_tiny';

  params.opts.visualize=true;
  params.opts.visOpts.debugVisualize=true;
  params.opts.debugLevel=10;
  params.opts.visOpts.imgExt='jpg';
  params.opts.visOpts.segBdryWidth=2;
  params.overWrite=true;

  params.evalOpts_string='try_proxy1';
  params.testBench_cmd='testBench.getTests_proxy2_tiny()';
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',params.rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
end

timingFile=[params.rootOut_dir 'timing.txt'];
fH_time=fopen(timingFile,'w');
[videoPaths,videoFiles]=eval(params.testBench_cmd);

videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);
for i=1:length(videoNames)
  outDirs{i}=[params.rootOut_dir videoNames{i} '/'];
end

stTime_global=clock;

parfor i=1:length(videoNames)
%for i=1:length(videoNames)  
  fprintf('\nNow running on %s\n',[videoPaths{i} videoFiles{i}]);
  %stTime=clock;
  runOnVideo(videoPaths{i},videoFiles{i},outDirs{i},params.opts,... 
            params.overWrite);
  %vidTime=etime(clock,stTime);
  %fprintf(fH_time,'Time to process video %s = %.2f minutes ( = %.2f hours)\n',...
                  %videoNames{i},vidTime/60,vidTime/3600);
end

fprintf(fH_time,'\n\nTotal time to run on all videos = %.2f hours\n',...
        etime(clock,stTime_global)/3600);
fclose(fH_time);

testBench.computeStats_humanCut(params.rootOut_dir,params.evalOpts_string);
%testBench.plotStats(rootOut_dir);

function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);

opts=params.opts;
if(opts.visualize & opts.visOpts.debugVisualize)
  debugOpts.debugLevel=opts.debugLevel;
else
  debugOpts.debugLevel=0;
end
debugOpts.debugDir='';
debugOpts.debugPrefix='';

fH=fopen([params.rootOut_dir 'opts_humanModel.txt'],'w');
[hModel,hModel_opts]=humanCut2.createHumanModel(opts,debugOpts);
miscFns.printClass(hModel_opts,fH);
delete(hModel);
fclose(fH);

fH=fopen([params.rootOut_dir 'opts_segModel.txt'],'w');
[segH,segH_opts]=humanCut2.createSegObject(opts,debugOpts);
miscFns.printClass(segH_opts,fH);
delete(segH);
fclose(fH);

fH=fopen([params.rootOut_dir 'opts_humanOptimizer.txt'],'w');
[optimizer,optimizer_opts]=humanCut2.createHumanModel_optimizer(opts,debugOpts);
miscFns.printClass(optimizer_opts,fH);
delete(optimizer);
fclose(fH);

function runOnVideo(videoPath,videoFile,outDir,opts,overWrite)

switch(opts.method)
  case {'partModelsTracks_firstTry'}
    humanCut2.runStitching3D_withTracks(videoPath,videoFile,outDir,opts,...
        overWrite);
  otherwise
    error('Invalid full model method %s\n',opts.method);
end
