function runManualProxy(videoPath,videoFile,outDir,opts,overWrite)

% Runs a segmentation algorithm, where the proxies for humans are manually
% specified

switch(opts.segMethod)
  case {'localGrabCut','grabCut'}
    runFrameByFrame(videoPath,videoFile,outDir,opts,overWrite);
  case {'grabCut3D','localGrabCut3D','grabCut3D_flow'}
    run3D(videoPath,videoFile,outDir,opts,overWrite);
end

end % of function runManualProxy

function run3D(videoPath,videoFile,outDir,opts,overWrite)

if(exist(outDir,'dir')),
  if(~overWrite)
    fprintf('Cannot overwrite exisiting results in: %s\n',outDir);
    return;
  else
    fprintf('\n\n---- Overwriting results in %s----------\n\n\n',outDir);
  end
else
  mkdir(outDir);
end

trackOut_dir=[outDir 'track001_files/'];
if(~exist(trackOut_dir,'dir')), mkdir(trackOut_dir); end;

videoName=miscFns.extractVideoNames({videoPath},{videoFile});
videoName=videoName{1};
[annots,K]=h3dAnno.loadAnnotations(videoPath,videoFile,videoName,opts.h3dAnno_dir);
vH=myVideoReader(videoPath,videoFile);

h3dOpts_conversion=humanCut2.makeOpts_h3dConversion(opts.optsString_h3dconversion);
switch(opts.segMethod)
  case 'grabCut3D'
    gcOpts=grabCut3D.helpers.makeOpts_withStitching(opts.segMethod_optsString);
  case 'grabCut3D_flow'
    gcOpts=grabCut3D_flow.helpers.makeOpts_withStitching(opts.segMethod_optsString,...
                                  videoName);
  case 'localGrabCut3D'
    gcOpts=localGrabCut3D.helpers.makeOpts_withStitching(opts.segMethod_optsString);
  otherwise
    error('Invalid segmentation method: %s\n',opts.segMethod);
end

fH=fopen([outDir 'h3dConversion_opts.txt'],'w');
miscFns.printStructure(h3dOpts_conversion,fH);
fclose(fH);

switch(opts.segMethod)
  case 'grabCut3D_flow'
    fH=fopen([outDir 'gcOpts.txt'],'w');
    warning('off','MATLAB:structOnObject');
    fprintf(fH,'Printing structure of grabCut3D_flow options (stitching)\n');
    miscFns.printStructure(struct(gcOpts),fH);
    fprintf(fH,'Printing structure of grabCut3D_flow options\n');
    miscFns.printStructure(struct(grabCut3D_flow.helpers.makeOpts(gcOpts.grabCutOpts_string)),fH);
    warning('on','MATLAB:structOnObject');
    fclose(fH);
  case 'grabCut3D'
    fH=fopen([outDir 'gcOpts.txt'],'w');
    warning('off','MATLAB:structOnObject');
    fprintf(fH,'Printing structure of grabCut options (stitching)\n');
    miscFns.printStructure(struct(gcOpts),fH);
    fprintf(fH,'Printing structure of grabCut3D options\n');
    miscFns.printStructure(struct(grabCut3D.helpers.makeOpts(gcOpts.opts_string))...
                           ,fH);
    warning('on','MATLAB:structOnObject');
    fclose(fH);
  case 'localGrabCut3D'
    fH=fopen([outDir 'gcOpts.txt'],'w');
    warning('off','MATLAB:structOnObject');
    fprintf(fH,'Printing structure of localGrabCut options (stitching)\n');
    miscFns.printStructure(struct(gcOpts),fH);
    fprintf(fH,'Printing structure of localGrabCut3D options\n');
    miscFns.printStructure(struct(localGrabCut3D.helpers.makeOpts(gcOpts.opts_string))...
                           ,fH);
    warning('on','MATLAB:structOnObject');
    fclose(fH);

end

assert(vH.nFrames==length(annots));
nFrames=vH.nFrames;
curIdx=1;

function [img,labels]=streamData()
  if(curIdx<=nFrames)
    img=vH.curFrame;
    hModel=humanCut2.humanPolygon();
    hModel.initializeFromH3D(annots(curIdx),K,h3dOpts_conversion);
    labels=hModel.generateGrabCutLabels([size(img,1) size(img,2)]);
    if(opts.visualize)
      imgDbl=im2double(img);
      polygonOverlay=hModel.drawPolygons(imgDbl,0.5);
      imwrite(polygonOverlay,sprintf([trackOut_dir 'humanPolygon_%03d.%s']...
              ,curIdx,opts.visOpts.imgExt));
      labelOverlay=humanCut.boxes.overlayBox_labels(imgDbl,labels);
      imwrite(labelOverlay,sprintf([trackOut_dir 'grabCutLabels_%03d.%s'],...
              curIdx,opts.visOpts.imgExt));
      %segOverlay=humanCut.overlaySeg(img,seg(:,:,i),opts.visOpts.segBdryWidth);
      %imwrite(segOverlay,sprintf([trackOut_dir 'seg_%03d.%s'],i,opts.visOpts.imgExt));
    end
    delete(hModel);
    if(curIdx<nFrames)
      vH.moveForward();
    end
    curIdx=curIdx+1;
  else
    img=[];labels=[];
  end
end

switch(opts.segMethod)
case 'grabCut3D_flow'
  seg=grabCut3D_flow.run3D_withStiching(@streamData,gcOpts);
case 'grabCut3D'
  seg=grabCut3D.run3D_withStiching(@streamData,gcOpts);
case 'localGrabCut3D'
  seg=localGrabCut3D.run3D_withStiching(@streamData,gcOpts);
end

delete(vH);
vH=myVideoReader(videoPath,videoFile);

for i=1:nFrames
  img=im2double(vH.curFrame);

  if(opts.visualize)
    segOverlay=humanCut.overlaySeg(img,seg(:,:,i),opts.visOpts.segBdryWidth);
    imwrite(segOverlay,sprintf([trackOut_dir 'seg_%03d.%s'],i,opts.visOpts.imgExt));
  end

  if(i~=vH.nFrames)
    vH.moveForward();
  end
  delete(hModel);
end

trackSt=1;
trackEnd=vH.nFrames;
delete(vH);
save([trackOut_dir 'seg.mat'],'seg','trackSt','trackEnd'); % Need to add the track variables
% to ensure evaluation works properly

end % of function run3D

function runFrameByFrame(videoPath,videoFile,outDir,opts,overWrite)

if(exist(outDir,'dir')),
  if(~overWrite)
    fprintf('Cannot overwrite exisiting results in: %s\n',outDir);
    return;
  else
    fprintf('\n\n---- Overwriting results in %s----------\n\n\n',outDir);
  end
else
  mkdir(outDir);
end

trackOut_dir=[outDir 'track001_files/'];
if(~exist(trackOut_dir,'dir')), mkdir(trackOut_dir); end;

videoName=miscFns.extractVideoNames({videoPath},{videoFile});
videoName=videoName{1};
[annots,K]=h3dAnno.loadAnnotations(videoPath,videoFile,videoName,opts.h3dAnno_dir);
vH=myVideoReader(videoPath,videoFile);

h3dOpts_conversion=humanCut2.makeOpts_h3dConversion(opts.optsString_h3dconversion);
switch(opts.segMethod)
  case 'localGrabCut'
    localGC_opts=localGrabCut.helpers.makeOpts(opts.segMethod_optsString);
  case 'grabCut'
    gcOpts=grabCut.helpers.makeOpts(opts.segMethod_optsString);
  otherwise
    error('Invalid segmentation method: %s\n',opts.segMethod);
end

fH=fopen([outDir 'h3dConversion_opts.txt'],'w');
miscFns.printStructure(h3dOpts_conversion,fH);
fclose(fH);

switch(opts.segMethod)
  case 'localGrabCut'
    fH=fopen([outDir 'localGC_opts.txt'],'w');
    warning('off','MATLAB:structOnObject');
    miscFns.printStructure(struct(localGC_opts),fH);
    warning('on','MATLAB:structOnObject');
    fclose(fH);
  case 'grabCut'
    fH=fopen([outDir 'gcOpts.txt'],'w');
    warning('off','MATLAB:structOnObject');
    miscFns.printStructure(struct(gcOpts),fH);
    warning('on','MATLAB:structOnObject');
    fclose(fH);
end


seg=zeros([vH.h vH.w vH.nFrames],'uint8');

assert(vH.nFrames==length(annots));

for i=1:length(annots)
  img=im2double(vH.curFrame);
  hModel=humanCut2.humanPolygon();
  hModel.initializeFromH3D(annots(i),K,h3dOpts_conversion);

  switch(opts.segMethod)
    case 'localGrabCut'
      labelImg=hModel.generateGrabCutLabels([size(img,1) size(img,2)]);
      segH=localGrabCut.segEngine(0,localGC_opts);
      segH.preProcess(img);
      segH.start(labelImg);
      seg(:,:,i)=segH.seg;
      delete(segH);
    case 'grabCut'
      labelImg=hModel.generateGrabCutLabels([size(img,1) size(img,2)]);
      segH=grabCut.segEngine(0,gcOpts);
      segH.preProcess(img);
      segH.start(labelImg);
      seg(:,:,i)=segH.seg;
      delete(segH);
    otherwise
      error('Invalid segmentation method: %s\n',opts.segMethod);
  end

  if(opts.visualize)
    polygonOverlay=hModel.drawPolygons(img,0.5);
    imwrite(polygonOverlay,sprintf([trackOut_dir 'humanPolygon_%03d.%s'],i,opts.visOpts.imgExt));
    labelOverlay=humanCut.boxes.overlayBox_labels(img,labelImg);
    imwrite(labelOverlay,sprintf([trackOut_dir 'grabCutLabels_%03d.%s'],i,opts.visOpts.imgExt));
    segOverlay=humanCut.overlaySeg(img,seg(:,:,i),opts.visOpts.segBdryWidth);
    imwrite(segOverlay,sprintf([trackOut_dir 'seg_%03d.%s'],i,opts.visOpts.imgExt));
  end

  if(i~=vH.nFrames)
    vH.moveForward();
  end
  delete(hModel);
end

trackSt=1;
trackEnd=vH.nFrames;
delete(vH);
save([trackOut_dir 'seg.mat'],'seg','trackSt','trackEnd'); % Need to add the track variables
% to ensure evaluation works properly

end % of function runFrameByFrame
