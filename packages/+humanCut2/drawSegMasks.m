function drawSegMasks(params)
% Function to load the computed segmentation masks and save them as binary images
% Useful to see if the masks help in finding the stickman of the videos

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('params','var'))
  %params.inputResults_dir=[cwd '../../results/humanCut/'];
  params.inputResults_dir=['/data/adam2/varun/humanCut2/manualProxy/run020/'];
  params.rootOut_dir=[cwd '../../results/humanCut/segMasks/run020/'];
  params.overWrite=true;
  params.testBench_cmd='testBench.getTests_proxy2()';
  params.message='This directory stores segmentation masks only';
  %params.testBench_cmd='testBench.getTests_20frames()';
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
  srcDir=params.inputResults_dir;
  destDir=[params.rootOut_dir 'origResults_softLink'];
  cmd=['ln -s ' srcDir ' ' destDir ';'];
  system(cmd);
end

[videoPaths,videoFiles]=eval(params.testBench_cmd);

videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);
for i=1:length(videoNames)
  outDirs{i}=[params.rootOut_dir videoNames{i} '/'];
  inDirs{i}=[params.inputResults_dir videoNames{i} '/'];
end

for i=1:length(videoNames)
  fprintf('\nNow running on %s\n',[videoPaths{i} videoFiles{i}]);
  convertVideo(outDirs{i},inDirs{i});
end


function convertVideo(outDir,inDir)
  done=false;
  trackNum=1;
  while(~done)
    trackDir=sprintf('track%03d_files/',trackNum);
    trackIn_dir=[inDir trackDir];
    trackOut_dir=[outDir trackDir];
    if(exist(trackIn_dir,'dir'))
      if(~exist(trackOut_dir,'dir')),mkdir(trackOut_dir); end;
      srcFile=[trackIn_dir 'seg.mat'];
      tmpLoad=load(srcFile);
      seg=tmpLoad.seg;
      for i=1:size(seg,3)
        outFile=[trackOut_dir sprintf('seg_%05d.png',i)];
        imwrite(seg(:,:,i),outFile);
      end
    else
      done=true;
    end
    trackNum=trackNum+1;
  end

function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);

