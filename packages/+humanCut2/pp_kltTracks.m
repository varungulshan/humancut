function pp_kltTracks(params)
% Code to store preprocessed KLT tracks for video
% uses vgg_klt code for preprocessing

% filename of video to process

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('params','var'))
  params.rootOut_dir=[cwd '../../pp/kltTracks/'];
  params.opts.visualize=true;
  params.opts.nFeatures=1000;
  params.opts.pyramid_levels=2;
  params.opts.visOpts.imgExt='jpg';
  params.opts.visOpts.drawRad=1;
  params.overWrite=true;
  params.testBench_cmd='testBench.getTests_proxy2()';
  %params.testBench_cmd='testBench.getTests_20frames()';
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',params.rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
end

[videoPaths,videoFiles]=eval(params.testBench_cmd);

videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);
for i=1:length(videoNames)
  outDirs{i}=[params.rootOut_dir videoNames{i} '/'];
end

for i=1:length(videoNames)
  fprintf('\nNow running on %s\n',[videoPaths{i} videoFiles{i}]);
  runOnVideo(videoPaths{i},videoFiles{i},outDirs{i},params.opts,params.overWrite);
end


function runOnVideo(videoFilePath,videoFileName,outDir,opts,overWrite)

nFeatures=opts.nFeatures; % number of features per frame
pyramid_levels=opts.pyramid_levels;

vH=myVideoReader(videoFilePath,videoFileName);
nFrames=vH.nFrames;

f1 = 1;   
f2 = nFrames;

tc = vgg_klt_init('nfeats',nFeatures,'mindisp',0.5,'pyramid_levels',pyramid_levels);

if(exist(outDir,'dir'))
  if(~overWrite)
    error('Cant overwrite existing results in %s\n',outDir);
  end
else
  mkdir(outDir);
end

save([outDir 'kltOpts.mat'],'tc');
fOpts=fopen([outDir 'kltOpts.txt'],'w');
miscFns.printStructure(tc,fOpts);
fclose(fOpts);

K = zeros(3,tc.nfeats,f2-f1+1);

I = vH.curFrame;
prevFrame=im2double(I);
I = im2double(rgb2gray(I));
If1 = I;
M = [];
stTime=clock;
[tc,P] = vgg_klt_selfeats(tc,I,M);
fprintf('Time taken to select %d features= %.3f seconds\n',tc.nfeats,etime(clock,stTime));

n=sum(P(3,:)>=0);
vgg_printf('%d features found in first frame\n', n);

% tracking frames (f1+1):f2
for f = (f1+1):f2    

    vH.moveForward();
    I = vH.curFrame;
    I_color=im2double(I);
    I = im2double(rgb2gray(I));

    pts_init=P(1:2,:);

    stTime=clock;
    [tc,P] = vgg_klt_track(tc,P,I,M);
    fprintf('Tracking features = %.3f seconds\n',etime(clock,stTime));
    pts_final=P(1:2,:);

    mask_tracked=(P(3,:)>=0);
    nt = nnz(mask_tracked);
    vgg_printf('%d tracked\n', n);
    save([outDir sprintf('%05d.mat',f-1)],'pts_init','pts_final','mask_tracked');
    
    stTime=clock;
    [tc,P] = vgg_klt_selfeats(tc,I,M,P);    
    fprintf('Replacing features = %.3f seconds\n',etime(clock,stTime));
    
    nn = sum(P(3,:)>=0);
    vgg_printf('now %d features (%d replaced)\n', nn, nn-nt);

    if(opts.visualize)
      [v1,v2]=drawFeatures(pts_init,pts_final,mask_tracked,prevFrame,I_color,...
                           opts.visOpts.drawRad);
      imwrite(v1,[outDir sprintf('tracks_%05d_0.%s',f-1,opts.visOpts.imgExt)]);
      imwrite(v2,[outDir sprintf('tracks_%05d_1.%s',f-1,opts.visOpts.imgExt)]);
    end

    prevFrame=I_color;
end

delete(vH);

function [v1,v2]=drawFeatures(p1,p2,trackedMask,i1,i2,drawRad)

v1=i1;v2=i2;

for i=1:size(p1,2)
  if(trackedMask(i)), drawColor=[0 1 0];
  else drawColor=[1 0 0];
  end
  v1=drawCross(v1,p1(1,i),p1(2,i),drawColor,drawRad);
end

for i=1:size(p2,2)
  if(trackedMask(i)),
    drawColor=[0 1 0];
    v2=drawCross(v2,p2(1,i),p2(2,i),drawColor,drawRad);
  end
end

[h w nCh]=size(i1);
p1=round(p1(:,trackedMask));
p2=round(p2(:,trackedMask));
lineStart=int32(sub2ind([h w],p1(2,:),p1(1,:)))'-1;
motionVec=int32(p2-p1);
lineClr=uint8([0 0 255]);
arrowWidth=int32(1);
centerClr=uint8([0 0 255]);
centerRad=int32(0);

v1=motionCut2.cpp.mex_drawMotion(im2uint8(v1),lineStart,motionVec,lineClr,arrowWidth,...
                                 centerClr,centerRad);
%v2=motionCut2.cpp.mex_drawMotion(im2uint8(v2),lineStart,motionVec,lineClr,arrowWidth,...
                                 %centerClr,centerRad);


function img=drawCross(img,x,y,color,rad)
  x=round(x);y=round(y);
  [h,w,nCh]=size(img);
  xTop=max(1,x-3);
  yTop=max(1,y-rad);
  xBot=min(w,x+3);
  yBot=min(h,y+rad);

  for i=1:3
    img(yTop:yBot,xTop:xBot,i)=color(i);
  end

  xTop=max(1,x-rad);
  yTop=max(1,y-3);
  xBot=min(w,x+rad);
  yBot=min(h,y+3);

  for i=1:3
    img(yTop:yBot,xTop:xBot,i)=color(i);
  end

function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);
