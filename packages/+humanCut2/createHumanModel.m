function [hModel,hModel_opts]=createHumanModel(opts,debugOpts)
% Function to create an empty human model (whose interface is
% defined by humanModels.genericModel.
% opts.humanModel_type specifies the type of human model to create
% opts.humanModel_optsString specified option string for the human model

switch(opts.humanModel_type)
  case 'jointsModel'
    hModel_opts=humanModels.jointsModel_opts(opts.humanModel_optsString);
    hModel=humanModels.jointsModel(debugOpts,hModel_opts);
  case 'jointsModel3D_indep'
    hModel_opts=humanModels.jointsModel3D_indep_opts(opts.humanModel_optsString);
    hModel=humanModels.jointsModel3D_indep(debugOpts,hModel_opts);
  case 'jointsModel3D_skipInit'
    hModel_opts=humanModels.jointsModel3D_skipInit_opts(opts.humanModel_optsString);
    hModel=humanModels.jointsModel3D_skipInit(debugOpts,hModel_opts);
  case 'jointsModel3D_parts'
    hModel_opts=humanModels.jointsModel3D_parts_opts(opts.humanModel_optsString);
    hModel=humanModels.jointsModel3D_parts(debugOpts,hModel_opts);
  case 'jointsModel3D_partsSmooth'
    hModel_opts=humanModels.jointsModel3D_partsSmooth_opts(opts.humanModel_optsString);
    hModel=humanModels.jointsModel3D_partsSmooth(debugOpts,hModel_opts);
  case 'jointsModel3D_partsAndTracks'
    hModel_opts=humanModels.jointsModel3D_partsTracks_opts(opts.humanModel_optsString);
    hModel=humanModels.jointsModel3D_partsTracks(debugOpts,hModel_opts);
  case 'jointsModel3D_indepC' % C stand for with constraints, used in stitching
    hModel_opts=humanModels.jointsModel3D_indepC2_opts(opts.humanModel_optsString);
    hModel=humanModels.jointsModel3D_indepC2(debugOpts,hModel_opts);
  otherwise
    error('Invalid model type: %s in createHumanModel\n',modelType);
end
