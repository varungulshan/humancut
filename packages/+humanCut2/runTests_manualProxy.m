function runTests_manualProxy(params)
% Runs tests on a dataset , where human pose is annotated manually 
% using lubomir's tool

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('params','var'))
  params.rootOut_dir=[cwd '../../results/humanCut2/grabCut3D/'];
  params.opts.visualize=true;
  params.opts.optsString_h3dconversion='relative_softPolygonHardMedial2';
  params.opts.h3dAnno_dir=[cwd '../../data/h3dAnnotations_2/'];
  params.opts.segMethod='grabCut3D';
  params.opts.segMethod_optsString='21frames_iter10_2';
  params.opts.visOpts.imgExt='jpg';
  params.opts.visOpts.segBdryWidth=2;
  params.evalOpts_string='try_proxy1';
  params.overWrite=true;
  params.testBench_cmd='testBench.getTests_proxy2()';
  %params.testBench_cmd='testBench.getTests_20frames()';
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',params.rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
end

[videoPaths,videoFiles]=eval(params.testBench_cmd);

videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);
for i=1:length(videoNames)
  outDirs{i}=[params.rootOut_dir videoNames{i} '/'];
end

for i=1:length(videoNames)
  fprintf('\nNow running on %s\n',[videoPaths{i} videoFiles{i}]);
  humanCut2.runManualProxy(videoPaths{i},videoFiles{i},outDirs{i},params.opts,params.overWrite);
end

testBench.computeStats_humanCut(params.rootOut_dir,params.evalOpts_string);
%testBench.plotStats(rootOut_dir);

function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);
