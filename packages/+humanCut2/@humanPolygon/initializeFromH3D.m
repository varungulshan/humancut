function initializeFromH3D(obj,anno,K,opts)
% Initializes polygons from h3d annotation 
% K is the configuration from h3d

obj.medialAxis_width=opts.medialAxis_width;
obj.stickManType=opts.stickManType;

switch(opts.widthType)
  case 'absolute'
    do_absoluteStickMan(obj,anno,K,opts);
  case 'relative'
    do_relativeStickMan(obj,anno,K,opts);
  otherwise
    error('Invalid width type %s in initializeFromH3D\n',opts.widthType);
end

function do_relativeStickMan(obj,anno,K,opts)

coords=anno.coords(:,1:2);
visible=anno.visible;
marked=anno.marked;
layerId=anno.layerNum;

% Start with hands, obtained by extending lower right arm
if(marked(K.R_Wrist) && marked(K.R_Elbow))
   [polygon,axisM]=getHandPolygon_rel(coords(K.R_Elbow,:),coords(K.R_Wrist,:),...
                   opts.hand_width); 
   axisLength=norm(axisM(1:2)-axisM(3:4));
   obj.allParts(1).polygon=polygon;
   obj.allParts(1).medialAxis=axisM;
   obj.allParts(1).detected=1;
   obj.allParts(1).layerId=layerId(K.R_Wrist);
   obj.allParts(1).border=opts.hand_border*axisLength;
else
  obj.allParts(1).detected=0;
  obj.allParts(1).polygon=[];
end

if(marked(K.L_Wrist) && marked(K.L_Elbow))
   [polygon,axisM]=getHandPolygon_rel(coords(K.L_Elbow,:),coords(K.L_Wrist,:),...
                   opts.hand_width); 
   axisLength=norm(axisM(1:2)-axisM(3:4));
   obj.allParts(4).polygon=polygon;
   obj.allParts(4).medialAxis=axisM;
   obj.allParts(4).detected=1;
   obj.allParts(4).layerId=layerId(K.L_Wrist);
   obj.allParts(4).border=opts.hand_border*axisLength;
else
  obj.allParts(4).detected=0;
  obj.allParts(4).polygon=[];
end

% Process lower arms
if(marked(K.R_Wrist) && marked(K.R_Elbow))
   [polygon,axisM]=getPolygonFromMedian_rel(coords(K.R_Elbow,:),coords(K.R_Wrist,:),...
                                                opts.lowerArm_halfWidth); 
   axisLength=norm(axisM(1:2)-axisM(3:4));
   obj.allParts(2).polygon=polygon;
   obj.allParts(2).medialAxis=axisM;
   obj.allParts(2).detected=1;
   obj.allParts(2).layerId=min(layerId(K.R_Wrist),layerId(K.R_Elbow));
   obj.allParts(2).border=opts.lowerArm_border*axisLength;
else
  obj.allParts(2).detected=0;
  obj.allParts(2).polygon=[];
end

if(marked(K.L_Wrist) && marked(K.L_Elbow))
   [polygon,axisM]=getPolygonFromMedian_rel(coords(K.L_Elbow,:),coords(K.L_Wrist,:),...
                                               opts.lowerArm_halfWidth); 
   axisLength=norm(axisM(1:2)-axisM(3:4));
   obj.allParts(5).polygon=polygon;
   obj.allParts(5).medialAxis=axisM;
   obj.allParts(5).detected=1;
   obj.allParts(5).layerId=min(layerId(K.L_Wrist),layerId(K.L_Elbow));
   obj.allParts(5).border=opts.lowerArm_border*axisLength;
else
  obj.allParts(5).detected=0;
  obj.allParts(5).polygon=[];
end

% Process upper arms
if(marked(K.R_Shoulder) && marked(K.R_Elbow))
   [polygon,axisM]=getPolygonFromMedian_rel(coords(K.R_Elbow,:),coords(K.R_Shoulder,:),...
                                               opts.upperArm_halfWidth); 
   axisLength=norm(axisM(1:2)-axisM(3:4));
   obj.allParts(3).polygon=polygon;
   obj.allParts(3).medialAxis=axisM;
   obj.allParts(3).detected=1;
   obj.allParts(3).layerId=min(layerId(K.R_Shoulder),layerId(K.R_Elbow));
   obj.allParts(3).border=opts.upperArm_border*axisLength;
else
  obj.allParts(3).detected=0;
  obj.allParts(3).polygon=[];
end

if(marked(K.L_Shoulder) && marked(K.L_Elbow))
   [polygon,axisM]=getPolygonFromMedian_rel(coords(K.L_Elbow,:),coords(K.L_Shoulder,:),...
                                              opts.upperArm_halfWidth); 
   axisLength=norm(axisM(1:2)-axisM(3:4));
   obj.allParts(6).polygon=polygon;
   obj.allParts(6).medialAxis=axisM;
   obj.allParts(6).detected=1;
   obj.allParts(6).layerId=min(layerId(K.L_Shoulder),layerId(K.L_Elbow));
   obj.allParts(6).border=opts.upperArm_border*axisLength;
else
  obj.allParts(6).detected=0;
  obj.allParts(6).polygon=[];
end

% Process face
switch(opts.faceMethod)
  case 'eyeShoulder_join'
    if(marked(K.R_Shoulder) && marked(K.L_Shoulder) && marked(K.R_Eye) && marked(K.L_Eye))
      [polygon,axisM]=getFacePolygon_rel(coords(K.R_Shoulder,:),coords(K.L_Shoulder,:),...
                                              coords(K.R_Eye,:),coords(K.L_Eye,:),...
                                              opts.scaleFaceMedian,opts.faceWidth);
      axisLength=norm(axisM(1:2)-axisM(3:4));
      obj.allParts(7).polygon=polygon;
      obj.allParts(7).medialAxis=axisM;
      obj.allParts(7).detected=1;
      obj.allParts(7).layerId=min(layerId(K.L_Eye),layerId(K.R_Eye));
      obj.allParts(7).border=opts.faceBorder*axisLength;
    else
      obj.allParts(7).detected=0;
      obj.allParts(7).polygon=[];
    end

  case 'shoulderPerpendicular'
    if(marked(K.R_Shoulder) && marked(K.L_Shoulder) && marked(K.R_Eye) && marked(K.L_Eye))
      [polygon,axisM]=getFacePolygon2_rel(coords(K.R_Shoulder,:),coords(K.L_Shoulder,:),...
                                              coords(K.R_Eye,:),coords(K.L_Eye,:),...
                                              opts.scaleFaceMedian,opts.faceWidth);
      axisLength=norm(axisM(1:2)-axisM(3:4));
      obj.allParts(7).polygon=polygon;
      obj.allParts(7).medialAxis=axisM;
      obj.allParts(7).detected=1;
      obj.allParts(7).layerId=min(layerId(K.L_Eye),layerId(K.R_Eye));
      obj.allParts(7).border=opts.faceBorder*axisLength;
    else
      obj.allParts(7).detected=0;
      obj.allParts(7).polygon=[];
    end
  otherwise
    error('Unexpected face method: %s\n',opts.faceMethod);
end

% Process torso
if(marked(K.R_Shoulder) && marked(K.L_Shoulder) && marked(K.R_Hip) && marked(K.L_Hip))
   [polygon,axisM]=getTorsoPolygon_rel(coords(K.R_Shoulder,:),coords(K.L_Shoulder,:),...
                                           coords(K.R_Hip,:),coords(K.L_Hip,:));
   axisLength=norm(axisM(1:2)-axisM(3:4));
   obj.allParts(8).polygon=polygon;
   obj.allParts(8).medialAxis=axisM;
   obj.allParts(8).detected=1;
   obj.allParts(8).layerId=min([layerId(K.L_Shoulder) layerId(K.R_Shoulder) ...
                                layerId(K.L_Hip) layerId(K.R_Hip)]);
   obj.allParts(8).border=opts.torsoBorder*axisLength;
else
  obj.allParts(8).detected=0;
  obj.allParts(8).polygon=[];
end

% Process upper legs
if(marked(K.R_Hip) && marked(K.R_Knee))
   [polygon,axisM]=getPolygonFromMedian_rel(coords(K.R_Hip,:),coords(K.R_Knee,:),...
                                               opts.upperLeg_halfWidth); 
   axisLength=norm(axisM(1:2)-axisM(3:4));
   obj.allParts(9).polygon=polygon;
   obj.allParts(9).medialAxis=axisM;
   obj.allParts(9).detected=1;
   obj.allParts(9).layerId=min(layerId(K.R_Hip),layerId(K.R_Knee));
   obj.allParts(9).border=opts.upperLeg_border*axisLength;
else
  obj.allParts(9).detected=0;
  obj.allParts(9).polygon=[];
end

if(marked(K.L_Hip) && marked(K.L_Knee))
   [polygon,axisM]=getPolygonFromMedian_rel(coords(K.L_Hip,:),coords(K.L_Knee,:),...
                                                opts.upperLeg_halfWidth); 
   axisLength=norm(axisM(1:2)-axisM(3:4));
   obj.allParts(12).polygon=polygon;
   obj.allParts(12).medialAxis=axisM;
   obj.allParts(12).detected=1;
   obj.allParts(12).layerId=min(layerId(K.L_Hip),layerId(K.L_Knee));
   obj.allParts(12).border=opts.upperLeg_border*axisLength;
else
  obj.allParts(12).detected=0;
  obj.allParts(12).polygon=[];
end

% Process lower legs
if(marked(K.R_Ankle) && marked(K.R_Knee))
   [polygon,axisM]=getPolygonFromMedian_rel(coords(K.R_Ankle,:),coords(K.R_Knee,:),...
                                                opts.lowerLeg_halfWidth); 
   axisLength=norm(axisM(1:2)-axisM(3:4));
   obj.allParts(10).polygon=polygon;
   obj.allParts(10).medialAxis=axisM;
   obj.allParts(10).detected=1;
   obj.allParts(10).layerId=min(layerId(K.R_Ankle),layerId(K.R_Knee));
   obj.allParts(10).border=opts.lowerLeg_border*axisLength;
else
  obj.allParts(10).detected=0;
  obj.allParts(10).polygon=[];
end

if(marked(K.L_Ankle) && marked(K.L_Knee))
   [polygon,axisM]=getPolygonFromMedian_rel(coords(K.L_Ankle,:),coords(K.L_Knee,:),...
                                                opts.lowerLeg_halfWidth); 
   axisLength=norm(axisM(1:2)-axisM(3:4));
   obj.allParts(13).polygon=polygon;
   obj.allParts(13).medialAxis=axisM;
   obj.allParts(13).detected=1;
   obj.allParts(13).layerId=min(layerId(K.L_Ankle),layerId(K.L_Knee));
   obj.allParts(13).border=opts.lowerLeg_border*axisLength;
else
  obj.allParts(13).detected=0;
  obj.allParts(13).polygon=[];
end

% Process feet, obtained by extending ankles 
if(marked(K.R_Knee) && marked(K.R_Ankle))
   [polygon,axisM]=getHandPolygon_rel(coords(K.R_Knee,:),coords(K.R_Ankle,:),opts.foot_width); 
   % Yes, use the same HandPolygon function for feet at the moment, its not a bug :)
   axisLength=norm(axisM(1:2)-axisM(3:4));
   obj.allParts(11).polygon=polygon;
   obj.allParts(11).medialAxis=axisM;
   obj.allParts(11).detected=1;
   obj.allParts(11).layerId=layerId(K.R_Ankle);
   obj.allParts(11).border=opts.foot_border*axisLength;
else
  obj.allParts(11).detected=0;
  obj.allParts(11).polygon=[];
end

if(marked(K.L_Knee) && marked(K.L_Ankle))
   [polygon,axisM]=getHandPolygon_rel(coords(K.L_Knee,:),coords(K.L_Ankle,:),opts.foot_width); 
   % Yes, use the same HandPolygon function for feet at the moment, its not a bug :)
   axisLength=norm(axisM(1:2)-axisM(3:4));
   obj.allParts(14).polygon=polygon;
   obj.allParts(14).medialAxis=axisM;
   obj.allParts(14).detected=1;
   obj.allParts(14).layerId=layerId(K.L_Ankle);
   obj.allParts(14).border=opts.foot_border*axisLength;
else
  obj.allParts(14).detected=0;
  obj.allParts(14).polygon=[];
end


function do_absoluteStickMan(obj,anno,K,opts)

coords=anno.coords(:,1:2);
visible=anno.visible;
marked=anno.marked;
layerId=anno.layerNum;

% Start with hands, obtained by extending lower right arm
if(marked(K.R_Wrist) && marked(K.R_Elbow))
   [polygon,axisM]=getHandPolygon(coords(K.R_Elbow,:),coords(K.R_Wrist,:),opts.hand_width); 
   obj.allParts(1).polygon=polygon;
   obj.allParts(1).medialAxis=axisM;
   obj.allParts(1).detected=1;
   obj.allParts(1).layerId=layerId(K.R_Wrist);
   obj.allParts(1).border=opts.hand_border;
else
  obj.allParts(1).detected=0;
  obj.allParts(1).polygon=[];
end

if(marked(K.L_Wrist) && marked(K.L_Elbow))
   [polygon,axisM]=getHandPolygon(coords(K.L_Elbow,:),coords(K.L_Wrist,:),opts.hand_width); 
   obj.allParts(4).polygon=polygon;
   obj.allParts(4).medialAxis=axisM;
   obj.allParts(4).detected=1;
   obj.allParts(4).layerId=layerId(K.L_Wrist);
   obj.allParts(4).border=opts.hand_border;
else
  obj.allParts(4).detected=0;
  obj.allParts(4).polygon=[];
end

% Process lower arms
if(marked(K.R_Wrist) && marked(K.R_Elbow))
   [polygon,axisM]=getPolygonFromMedian(coords(K.R_Elbow,:),coords(K.R_Wrist,:),...
                                                opts.lowerArm_halfWidth); 
   obj.allParts(2).polygon=polygon;
   obj.allParts(2).medialAxis=axisM;
   obj.allParts(2).detected=1;
   obj.allParts(2).layerId=min(layerId(K.R_Wrist),layerId(K.R_Elbow));
   obj.allParts(2).border=opts.lowerArm_border;
else
  obj.allParts(2).detected=0;
  obj.allParts(2).polygon=[];
end

if(marked(K.L_Wrist) && marked(K.L_Elbow))
   [polygon,axisM]=getPolygonFromMedian(coords(K.L_Elbow,:),coords(K.L_Wrist,:),...
                                               opts.lowerArm_halfWidth); 
   obj.allParts(5).polygon=polygon;
   obj.allParts(5).medialAxis=axisM;
   obj.allParts(5).detected=1;
   obj.allParts(5).layerId=min(layerId(K.L_Wrist),layerId(K.L_Elbow));
   obj.allParts(5).border=opts.lowerArm_border;
else
  obj.allParts(5).detected=0;
  obj.allParts(5).polygon=[];
end

% Process upper arms
if(marked(K.R_Shoulder) && marked(K.R_Elbow))
   [polygon,axisM]=getPolygonFromMedian(coords(K.R_Elbow,:),coords(K.R_Shoulder,:),...
                                               opts.upperArm_halfWidth); 
   obj.allParts(3).polygon=polygon;
   obj.allParts(3).medialAxis=axisM;
   obj.allParts(3).detected=1;
   obj.allParts(3).layerId=min(layerId(K.R_Shoulder),layerId(K.R_Elbow));
   obj.allParts(3).border=opts.upperArm_border;
else
  obj.allParts(3).detected=0;
  obj.allParts(3).polygon=[];
end

if(marked(K.L_Shoulder) && marked(K.L_Elbow))
   [polygon,axisM]=getPolygonFromMedian(coords(K.L_Elbow,:),coords(K.L_Shoulder,:),...
                                              opts.upperArm_halfWidth); 
   obj.allParts(6).polygon=polygon;
   obj.allParts(6).medialAxis=axisM;
   obj.allParts(6).detected=1;
   obj.allParts(6).layerId=min(layerId(K.L_Shoulder),layerId(K.L_Elbow));
   obj.allParts(6).border=opts.upperArm_border;
else
  obj.allParts(6).detected=0;
  obj.allParts(6).polygon=[];
end

% Process face
switch(opts.faceMethod)
  case 'eyeShoulder_join'
    if(marked(K.R_Shoulder) && marked(K.L_Shoulder) && marked(K.R_Eye) && marked(K.L_Eye))
      [polygon,axisM]=getFacePolygon(coords(K.R_Shoulder,:),coords(K.L_Shoulder,:),...
                                              coords(K.R_Eye,:),coords(K.L_Eye,:),...
                                              opts.scaleFaceMedian,opts.faceWidth);
      obj.allParts(7).polygon=polygon;
      obj.allParts(7).medialAxis=axisM;
      obj.allParts(7).detected=1;
      obj.allParts(7).layerId=min(layerId(K.L_Eye),layerId(K.R_Eye));
      obj.allParts(7).border=opts.faceBorder;
    else
      obj.allParts(7).detected=0;
      obj.allParts(7).polygon=[];
    end

  case 'shoulderPerpendicular'
    if(marked(K.R_Shoulder) && marked(K.L_Shoulder) && marked(K.R_Eye) && marked(K.L_Eye))
      [polygon,axisM]=getFacePolygon2(coords(K.R_Shoulder,:),coords(K.L_Shoulder,:),...
                                              coords(K.R_Eye,:),coords(K.L_Eye,:),...
                                              opts.scaleFaceMedian,opts.faceWidth);
      obj.allParts(7).polygon=polygon;
      obj.allParts(7).medialAxis=axisM;
      obj.allParts(7).detected=1;
      obj.allParts(7).layerId=min(layerId(K.L_Eye),layerId(K.R_Eye));
      obj.allParts(7).border=opts.faceBorder;
    else
      obj.allParts(7).detected=0;
      obj.allParts(7).polygon=[];
    end
  otherwise
    error('Unexpected face method: %s\n',opts.faceMethod);
end

% Process torso
if(marked(K.R_Shoulder) && marked(K.L_Shoulder) && marked(K.R_Hip) && marked(K.L_Hip))
   [polygon,axisM]=getTorsoPolygon(coords(K.R_Shoulder,:),coords(K.L_Shoulder,:),...
                                           coords(K.R_Hip,:),coords(K.L_Hip,:));
   obj.allParts(8).polygon=polygon;
   obj.allParts(8).medialAxis=axisM;
   obj.allParts(8).detected=1;
   obj.allParts(8).layerId=min([layerId(K.L_Shoulder) layerId(K.R_Shoulder) ...
                                layerId(K.L_Hip) layerId(K.R_Hip)]);
   obj.allParts(8).border=opts.torsoBorder;
else
  obj.allParts(8).detected=0;
  obj.allParts(8).polygon=[];
end

% Process upper legs
if(marked(K.R_Hip) && marked(K.R_Knee))
   [polygon,axisM]=getPolygonFromMedian(coords(K.R_Hip,:),coords(K.R_Knee,:),...
                                               opts.upperLeg_halfWidth); 
   obj.allParts(9).polygon=polygon;
   obj.allParts(9).medialAxis=axisM;
   obj.allParts(9).detected=1;
   obj.allParts(9).layerId=min(layerId(K.R_Hip),layerId(K.R_Knee));
   obj.allParts(9).border=opts.upperLeg_border;
else
  obj.allParts(9).detected=0;
  obj.allParts(9).polygon=[];
end

if(marked(K.L_Hip) && marked(K.L_Knee))
   [polygon,axisM]=getPolygonFromMedian(coords(K.L_Hip,:),coords(K.L_Knee,:),...
                                                opts.upperLeg_halfWidth); 
   obj.allParts(12).polygon=polygon;
   obj.allParts(12).medialAxis=axisM;
   obj.allParts(12).detected=1;
   obj.allParts(12).layerId=min(layerId(K.L_Hip),layerId(K.L_Knee));
   obj.allParts(12).border=opts.upperLeg_border;
else
  obj.allParts(12).detected=0;
  obj.allParts(12).polygon=[];
end

% Process lower legs
if(marked(K.R_Ankle) && marked(K.R_Knee))
   [polygon,axisM]=getPolygonFromMedian(coords(K.R_Ankle,:),coords(K.R_Knee,:),...
                                                opts.lowerLeg_halfWidth); 
   obj.allParts(10).polygon=polygon;
   obj.allParts(10).medialAxis=axisM;
   obj.allParts(10).detected=1;
   obj.allParts(10).layerId=min(layerId(K.R_Ankle),layerId(K.R_Knee));
   obj.allParts(10).border=opts.lowerLeg_border;
else
  obj.allParts(10).detected=0;
  obj.allParts(10).polygon=[];
end

if(marked(K.L_Ankle) && marked(K.L_Knee))
   [polygon,axisM]=getPolygonFromMedian(coords(K.L_Ankle,:),coords(K.L_Knee,:),...
                                                opts.lowerLeg_halfWidth); 
   obj.allParts(13).polygon=polygon;
   obj.allParts(13).medialAxis=axisM;
   obj.allParts(13).detected=1;
   obj.allParts(13).layerId=min(layerId(K.L_Ankle),layerId(K.L_Knee));
   obj.allParts(13).border=opts.lowerLeg_border;
else
  obj.allParts(13).detected=0;
  obj.allParts(13).polygon=[];
end

% Process feet, obtained by extending ankles 
if(marked(K.R_Knee) && marked(K.R_Ankle))
   [polygon,axisM]=getHandPolygon(coords(K.R_Knee,:),coords(K.R_Ankle,:),opts.foot_width); 
   % Yes, use the same HandPolygon function for feet at the moment, its not a bug :)
   obj.allParts(11).polygon=polygon;
   obj.allParts(11).medialAxis=axisM;
   obj.allParts(11).detected=1;
   obj.allParts(11).layerId=layerId(K.R_Ankle);
   obj.allParts(11).border=opts.foot_border;
else
  obj.allParts(11).detected=0;
  obj.allParts(11).polygon=[];
end

if(marked(K.L_Knee) && marked(K.L_Ankle))
   [polygon,axisM]=getHandPolygon(coords(K.L_Knee,:),coords(K.L_Ankle,:),opts.foot_width); 
   % Yes, use the same HandPolygon function for feet at the moment, its not a bug :)
   obj.allParts(14).polygon=polygon;
   obj.allParts(14).medialAxis=axisM;
   obj.allParts(14).detected=1;
   obj.allParts(14).layerId=layerId(K.L_Ankle);
   obj.allParts(14).border=opts.foot_border;
else
  obj.allParts(14).detected=0;
  obj.allParts(14).polygon=[];
end

function [polygon,axisM]=getFacePolygon2_rel(rShoulder,lShoulder,rEye,lEye,scaleMedian,faceWidth)
% Makes face polygon on line perpendicular to line joining center of shoulders

meanEye=0.5*(rEye+lEye);
meanShoulder=0.5*(lShoulder+rShoulder);

lengthFace=scaleMedian*norm(meanEye-meanShoulder);

hyp=norm(lShoulder-rShoulder);
xBase=lShoulder(1)-rShoulder(1);
yBase=lShoulder(2)-rShoulder(2);

cosTheta=xBase/hyp;
sinTheta=yBase/hyp; % These are orientation of shoulder line

upVector=[-sinTheta cosTheta];
if(upVector(2)>0) upVector=-upVector; end; % Make sure head points up

headTop=meanShoulder+lengthFace*upVector;

[polygon,axisM]=getPolygonFromMedian_rel(headTop,meanShoulder,faceWidth/2);

function [polygon,axisM]=getFacePolygon_rel(rShoulder,lShoulder,rEye,lEye,scaleMedian,faceWidth)
% Makes face polygon on line joining center of eyes to center of shoulders
meanEye=0.5*(rEye+lEye);
meanShoulder=0.5*(lShoulder+rShoulder);

headTop=meanShoulder+scaleMedian*(meanEye-meanShoulder);
[polygon,axisM]=getPolygonFromMedian_rel(headTop,meanShoulder,faceWidth/2);

function [polygon,axisM]=getTorsoPolygon_rel(rShoulder,lShoulder,rHip,lHip)
% This is same as getTorsoPolygon, didnt realise there were no relative
% measurements here!

shoulderLength=norm(rShoulder-lShoulder);
hipLength=norm(rHip-lHip);

rescale=hipLength/shoulderLength;
shoulderVec=lShoulder-rShoulder;
lambdaAlong=(1-rescale)/2;
polygon=zeros(4,2);

polygon(1,:)=rShoulder+lambdaAlong*shoulderVec;
polygon(2,:)=lShoulder-lambdaAlong*shoulderVec;
polygon(3,:)=lHip;
polygon(4,:)=rHip;

axisM=[(rHip+lHip)/2 (lShoulder+rShoulder)/2];

function [polygon,axisM]=getPolygonFromMedian_rel(pt1,pt2,rad)

hyp=norm(pt1-pt2);
rad=rad*hyp; % Convert to absolute scale
xBase=pt2(1)-pt1(1);
yBase=pt2(2)-pt1(2);

cosTheta=xBase/hyp;
sinTheta=yBase/hyp; % These are orientation of median line

% Orientation of line rotated 90degree anti-clockwise is
% give by (-sinTheta,cosTheta)
medianVec_perp=[-sinTheta cosTheta];

polygon=zeros(4,2);
polygon(1,:)=pt2+rad*medianVec_perp;
polygon(2,:)=pt2-rad*medianVec_perp;
polygon(3,:)=pt1-rad*medianVec_perp;
polygon(4,:)=pt1+rad*medianVec_perp;

axisM=[pt1 pt2];

function [polygon,axisM]=getHandPolygon_rel(elbow,wrist,boxW)

hyp=norm(elbow-wrist);
boxW=hyp*boxW; % Convert to absolute pixel size
xBase=elbow(1)-wrist(1);
yBase=elbow(2)-wrist(2);

cosTheta=xBase/hyp;
sinTheta=yBase/hyp; % These are orientation of line from write to elbow

% Orientation of line rotated 90degree anti-clockwise is
% give by (-sinTheta,cosTheta)

armVec=[cosTheta sinTheta];
armVec_perp=[-sinTheta cosTheta];

polygon=zeros(4,2);
boxRad=boxW/2;
polygon(1,:)=wrist+boxRad*armVec_perp;
polygon(2,:)=polygon(1,:)-boxW*armVec;
polygon(4,:)=wrist-boxRad*armVec_perp;
polygon(3,:)=polygon(4,:)-boxW*armVec;

axisM=[wrist wrist-boxW*armVec];

function [polygon,axisM]=getFacePolygon2(rShoulder,lShoulder,rEye,lEye,scaleMedian,faceWidth)
% Makes face polygon on line perpendicular to line joining center of shoulders

meanEye=0.5*(rEye+lEye);
meanShoulder=0.5*(lShoulder+rShoulder);

lengthFace=scaleMedian*norm(meanEye-meanShoulder);

hyp=norm(lShoulder-rShoulder);
xBase=lShoulder(1)-rShoulder(1);
yBase=lShoulder(2)-rShoulder(2);

cosTheta=xBase/hyp;
sinTheta=yBase/hyp; % These are orientation of shoulder line

upVector=[-sinTheta cosTheta];
if(upVector(2)>0) upVector=-upVector; end; % Make sure head points up

headTop=meanShoulder+lengthFace*upVector;

[polygon,axisM]=getPolygonFromMedian(headTop,meanShoulder,faceWidth/2);

function [polygon,axisM]=getFacePolygon(rShoulder,lShoulder,rEye,lEye,scaleMedian,faceWidth)
% Makes face polygon on line joining center of eyes to center of shoulders
meanEye=0.5*(rEye+lEye);
meanShoulder=0.5*(lShoulder+rShoulder);

headTop=meanShoulder+scaleMedian*(meanEye-meanShoulder);
[polygon,axisM]=getPolygonFromMedian(headTop,meanShoulder,faceWidth/2);

function [polygon,axisM]=getTorsoPolygon(rShoulder,lShoulder,rHip,lHip)

shoulderLength=norm(rShoulder-lShoulder);
hipLength=norm(rHip-lHip);

rescale=hipLength/shoulderLength;
shoulderVec=lShoulder-rShoulder;
lambdaAlong=(1-rescale)/2;
polygon=zeros(4,2);

polygon(1,:)=rShoulder+lambdaAlong*shoulderVec;
polygon(2,:)=lShoulder-lambdaAlong*shoulderVec;
polygon(3,:)=lHip;
polygon(4,:)=rHip;

axisM=[(rHip+lHip)/2 (lShoulder+rShoulder)/2];

function [polygon,axisM]=getPolygonFromMedian(pt1,pt2,rad)

hyp=norm(pt1-pt2);
xBase=pt2(1)-pt1(1);
yBase=pt2(2)-pt1(2);

cosTheta=xBase/hyp;
sinTheta=yBase/hyp; % These are orientation of median line

% Orientation of line rotated 90degree anti-clockwise is
% give by (-sinTheta,cosTheta)
medianVec_perp=[-sinTheta cosTheta];

polygon=zeros(4,2);
polygon(1,:)=pt2+rad*medianVec_perp;
polygon(2,:)=pt2-rad*medianVec_perp;
polygon(3,:)=pt1-rad*medianVec_perp;
polygon(4,:)=pt1+rad*medianVec_perp;

axisM=[pt1 pt2];

function [polygon,axisM]=getHandPolygon(elbow,wrist,boxW)

hyp=norm(elbow-wrist);
xBase=elbow(1)-wrist(1);
yBase=elbow(2)-wrist(2);

cosTheta=xBase/hyp;
sinTheta=yBase/hyp; % These are orientation of line from write to elbow

% Orientation of line rotated 90degree anti-clockwise is
% give by (-sinTheta,cosTheta)

armVec=[cosTheta sinTheta];
armVec_perp=[-sinTheta cosTheta];

polygon=zeros(4,2);
boxRad=boxW/2;
polygon(1,:)=wrist+boxRad*armVec_perp;
polygon(2,:)=polygon(1,:)-boxW*armVec;
polygon(4,:)=wrist-boxRad*armVec_perp;
polygon(3,:)=polygon(4,:)-boxW*armVec;

axisM=[wrist wrist-boxW*armVec];
