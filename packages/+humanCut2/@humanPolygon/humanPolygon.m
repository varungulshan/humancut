% This class stores parts of a human, as polygons, each polygon with a layer number
% and also a trimapWidth (i.e width of unknown region outside polygon

classdef humanPolygon < handle
  properties (SetAccess=public, GetAccess=public)

    allParts % Array of structures, each idx represents a particular part
    % The order of parts is as follows:
    % 01 -> right hand
    % 02 -> right lower arm
    % 03 -> right upper arm
    % 04 -> left hand
    % 05 -> left lower arm
    % 06 -> left upper arm
    % 07 -> face
    % 08 -> torso
    % 09 -> right upper leg
    % 10 -> right lower leg
    % 11 -> right foot
    % 12 -> left upper leg
    % 13 -> left lower leg
    % 14 -> left foot
    % Each part consists of the following fields:
    %   polygon -> [K x 2] array of polygon coordinates
    %   name    -> human readable name of part
    %   border  -> width (in pixels) of unknown region outside polygon
    %   layerId -> layer index (smaller is nearer)
    %   detected -> 0 means not visible/detected (polygon will be empty),
    %               1 means visible (but could be partially occluded by 
    %               another layer of the same person or another object
    %               or cropped by the image)
    %   medialAxis -> 4 x 1 array of [x1;y1;x2;y2] coordinates of medial axis

    partNames % array with partnames shown above
    numParts % scalar value = number of parts in our model
    stickManType % hardPolygon, softPolygon_hardMedial
    medialAxis_width % in pixels

  end

  methods
    function obj=humanPolygon()
      partNames{01}='Right hand';
      partNames{02}='Right lower arm';
      partNames{03}='Right upper arm';
      partNames{04}='Left hand';
      partNames{05}='Left lower arm';
      partNames{06}='Left upper arm';
      partNames{07}='Face';
      partNames{08}='Torso';
      partNames{09}='Right upper leg';
      partNames{10}='Right lower leg';
      partNames{11}='Right foot';
      partNames{12}='Left upper leg';
      partNames{13}='Left lower leg';
      partNames{14}='Left foot';
      obj.partNames=partNames;
      obj.numParts=length(partNames);
      for i=1:length(partNames)
        allParts(i).name=partNames{i};
        allParts(i).polygon=[];
        allParts(i).border=0;
        allParts(i).layerId=0;
        allParts(i).detected=0;
        allParts(i).medialAxis=[];
      end
      obj.allParts=allParts;
    end

    initializeFromH3D(obj,h3dAnno,K,opts); % Initializes parts from h3d format
    img=drawPolygons(obj,img,blendAlpha); % Draws the polygons which are marked as detected
    labels=generateGrabCutLabels(obj,labelsSize); % Generates labeling suitable for use
                                       % with grabCut/localGrabCut
  end

  methods (Access=private)
  end

end % of classdef
