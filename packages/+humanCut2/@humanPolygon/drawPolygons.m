function img=drawPolygons(obj,img,blendAlpha)

if(~exist('blendAlpha','var')),
  blendAlpha=0.5;
end

[h w nCh]=size(img);
labelImg=zeros([h w],'uint8');

partIds=[1:obj.numParts];
detected=logical([obj.allParts.detected]);
layerIds=[obj.allParts.layerId];

partIds=partIds(detected);
layerIds=layerIds(detected);

[layerIds,sortIdx]=sort(layerIds,2,'descend');
partIds=partIds(sortIdx);

for i=partIds
  iPart=obj.allParts(i);
  polyMask=roipoly(labelImg,iPart.polygon(:,1),iPart.polygon(:,2));
  labelImg(polyMask)=i;
end

alphaImg=blendAlpha*ones([h w]);
alphaImg(labelImg==0)=0;

cmapParts=colorcube(obj.numParts);
cmapParts=[0 0 0;cmapParts]; % Adding extra color for label 0

overlayImg=label2rgb(labelImg+1,cmapParts);
overlayImg=im2double(overlayImg);
alphaImg=repmat(alphaImg,[1 1 3]);
if(nCh==1), img=repmat(img,[1 1 3]); end;
if(~strcmp(class(img),'double')), img=im2double(img); end;

img=alphaImg.*overlayImg+(1-alphaImg).*img;
