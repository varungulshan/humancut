function labelImg=generateGrabCutLabels(obj,imgSize)
% Generates labeling suitable for use
% with grabCut/localGrabCut from the polygon representation

switch(obj.stickManType)
  case 'hardPolygon_noBug'
    % There was a minor bug (wont change results too much) in 
    % mex_addLabels, which has been fixed here, not removing the old
    % implementation because I generated results with it
    labelImg=getHardPolygon_labels_noBug(obj,imgSize);
  case 'hardPolygon'
    labelImg=getHardPolygon_labels(obj,imgSize);
  case 'softPolygon_hardMedial'
    labelImg=getSoftPolygon_labels(obj,imgSize);
  otherwise
    error('Invalid stickManType %s\n',obj.stickManType);
end

function labelImg=getSoftPolygon_labels(obj,imgSize)

h=imgSize(1);w=imgSize(2);
labelImg=2*ones([h w],'uint8');

partIds=[1:obj.numParts];
detected=logical([obj.allParts.detected]);
layerIds=[obj.allParts.layerId];

partIds=partIds(detected);
layerIds=layerIds(detected);

[layerIds,sortIdx]=sort(layerIds,2,'descend');
partIds=partIds(sortIdx);

for i=partIds
  iPart=obj.allParts(i);
  polyMask=roipoly(labelImg,iPart.polygon(:,1),iPart.polygon(:,2));
  labelImg(polyMask)=3;
  %labelImg=humanCut2.cpp.mex_addLabels(labelImg,polyMask,[iPart.border],uint8([0]));
  labelImg=humanCut2.cpp.mex_addMedialAxis(labelImg,iPart.medialAxis,int32(obj.medialAxis_width),uint8([1]));
  labelImg=humanCut2.cpp.mex_addLabels2(labelImg,polyMask,[iPart.border],uint8([0]),uint8(2));
  % Will only overwrite labelImg=2, so if a lower layer labels something as known, it wont alter it
end

function labelImg=getHardPolygon_labels_noBug(obj,imgSize)

h=imgSize(1);w=imgSize(2);
labelImg=2*ones([h w],'uint8');

partIds=[1:obj.numParts];
detected=logical([obj.allParts.detected]);
layerIds=[obj.allParts.layerId];

partIds=partIds(detected);
layerIds=layerIds(detected);

[layerIds,sortIdx]=sort(layerIds,2,'descend');
partIds=partIds(sortIdx);

for i=partIds
  iPart=obj.allParts(i);
  polyMask=roipoly(labelImg,iPart.polygon(:,1),iPart.polygon(:,2));
  labelImg(polyMask)=1;
  labelImg=humanCut2.cpp.mex_addLabels_noBug(labelImg,polyMask,[iPart.border],uint8([0]));
  % Will not overwrite labelImg=1 from the lower layers
end
function labelImg=getHardPolygon_labels(obj,imgSize)

h=imgSize(1);w=imgSize(2);
labelImg=2*ones([h w],'uint8');

partIds=[1:obj.numParts];
detected=logical([obj.allParts.detected]);
layerIds=[obj.allParts.layerId];

partIds=partIds(detected);
layerIds=layerIds(detected);

[layerIds,sortIdx]=sort(layerIds,2,'descend');
partIds=partIds(sortIdx);

for i=partIds
  iPart=obj.allParts(i);
  polyMask=roipoly(labelImg,iPart.polygon(:,1),iPart.polygon(:,2));
  labelImg(polyMask)=1;
  labelImg=humanCut2.cpp.mex_addLabels(labelImg,polyMask,[iPart.border],uint8([0]));
  % Will not overwrite labelImg=1 from the lower layers
end
