function convertSnapCut_toHumanCut(params)
% Converts segmentation outputs of a snapCut like system (i.e one that propagates
% segmentation starting from first frame), to a humanCut like format
% so that the snapCut like outputs can be evaluated in a common framework to
% the humanCut outputs

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('params','var'))
  params.inputResults_dir=[cwd '../../results/adobeSnapCut/'];
  params.rootOut_dir=[cwd '../../results/adobeSnapCut_proxyDataset/'];
  params.evalOpts_string='try_proxy1';
  params.overWrite=true;
  params.testBench_cmd='testBench.getTests_proxy()';
  %params.testBench_cmd='testBench.getTests_20frames()';
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
  srcDir=params.inputResults_dir;
  destDir=[params.rootOut_dir 'origResults_softLink'];
  cmd=['ln -s ' srcDir ' ' destDir ';'];
  system(cmd);
end

[videoPaths,videoFiles]=eval(params.testBench_cmd);

videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);
for i=1:length(videoNames)
  outDirs{i}=[params.rootOut_dir videoNames{i} '/'];
  inDirs{i}=[params.inputResults_dir videoNames{i} '/'];
end

for i=1:length(videoNames)
  fprintf('\nNow running on %s\n',[videoPaths{i} videoFiles{i}]);
  convertVideo(outDirs{i},inDirs{i});
end

testBench.computeStats_humanCut(params.rootOut_dir,params.evalOpts_string);
%testBench.plotStats(rootOut_dir);

function convertVideo(outDir,inDir)
  load([inDir 'seg.mat']); % Should provide the variable seg
  trackSt=1;
  trackEnd=size(seg,3);
  trackOut_dir=[outDir 'track001_files/'];
  if(~exist(trackOut_dir,'dir')),mkdir(trackOut_dir); end;
  save([trackOut_dir 'seg.mat'],'seg','trackSt','trackEnd');

function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);

