% Class defining a general parametrized model (could be of anything, not just humans!)
classdef genericHumanSegmenter < handle
  properties (Abstract,SetAccess=private, GetAccess=public)
    seg % stores the segmentation

    debugOpts % structure with debugLevel, debugDir and debugPrefix
  end

  methods(Abstract)
    initializeFull(obj,img,hModel); % Initializes the segmentation and color models
    % given a human model. Initialization is first done for color models (using
    % human model), and then segmentation is initialized. color models and 
    % segmentations are then iterated
    updateSeg_andColorModel(obj,hModel); % This function is called if the 
    % underlying hModel changes. In this case, the segmentation is first
    % updated and then the color models updated, and then normal grabCut proceeds
    energy=getEnergy_fixedSeg(obj,hModel);
    % Computes energy, given the current seg (in obj.seg)
    % color models and the passed human model. Does not make a call to dynamic graph cuts!
    energy=getEnergy_bestSeg(obj,hModel); % Computes energy, minimizing wrt seg 
    % Uses existing color models and the passed human model. 
    % Makes a call to dynamic graph cuts
  end
end
