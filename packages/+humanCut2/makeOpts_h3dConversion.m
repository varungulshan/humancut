function opts=makeOpts_h3dConversion(optsString)
% Makes options for converting h3d annotation to human polygon representation

switch(optsString)
  case 'try1'
    opts.widthType='absolute'; % or relative
    opts.stickManType='hardPolygon';
    opts.border_constrainType
    opts.hand_width=20;
    opts.hand_border=20;
    opts.lowerArm_halfWidth=7;
    opts.lowerArm_border=20;
    opts.upperArm_halfWidth=7;
    opts.upperArm_border=30;
    opts.scaleFaceMedian=1.3;
    opts.faceWidth=40;
    opts.faceBorder=40;
    opts.faceMethod='eyeShoulder_join';
    opts.torsoBorder=80;
    opts.upperLeg_halfWidth=10;
    opts.upperLeg_border=25;
    opts.lowerLeg_halfWidth=10;
    opts.lowerLeg_border=25;
    opts.foot_width=20;
    opts.foot_border=30;
    opts.medialAxis_width=2;

  case 'try2'
    opts.widthType='absolute'; % or relative
    opts.stickManType='hardPolygon';
    opts.hand_width=20;
    opts.hand_border=20;
    opts.lowerArm_halfWidth=7;
    opts.lowerArm_border=20;
    opts.upperArm_halfWidth=7;
    opts.upperArm_border=30;
    opts.scaleFaceMedian=1.5;
    opts.faceWidth=40;
    opts.faceBorder=40;
    opts.faceMethod='shoulderPerpendicular';
    opts.torsoBorder=80;
    opts.upperLeg_halfWidth=10;
    opts.upperLeg_border=40;
    opts.lowerLeg_halfWidth=10;
    opts.lowerLeg_border=30;
    opts.foot_width=20;
    opts.foot_border=30;
    opts.medialAxis_width=2;

  case 'relative_hardPolygon2'
    opts.widthType='relative'; 
    opts.stickManType='hardPolygon';
    opts.hand_width=0.2; % 0.2 times the lower arm length
    opts.hand_border=1; % times the hand length/width (its a square)
    opts.lowerArm_halfWidth=0.07; % times the lower arm length
    opts.lowerArm_border=0.2; % times the lower arm length
    opts.upperArm_halfWidth=0.07;
    opts.upperArm_border=0.3;
    opts.scaleFaceMedian=1;
    opts.faceWidth=0.328; % times the face length
    opts.faceBorder=0.328;
    opts.faceMethod='eyeShoulder_join';
    opts.torsoBorder=0.43; % times the torso length
    opts.upperLeg_halfWidth=0.08; % times the upper leg length
    opts.upperLeg_border=0.316; % times the upper leg length
    opts.lowerLeg_halfWidth=0.0754; % times the lower leg length
    opts.lowerLeg_border=0.2261; % times the lower leg length
    opts.foot_width=0.1507; % times the lower leg length
    opts.foot_border=0.2261; % times the lower leg length
    opts.medialAxis_width=2;

  case 'relative_softPolygonHardMedial2'
    opts.widthType='relative'; 
    opts.stickManType='softPolygon_hardMedial';
    opts.hand_width=0.2; % 0.2 times the lower arm length
    opts.hand_border=1; % times the hand length/width (its a square)
    opts.lowerArm_halfWidth=0.07; % times the lower arm length
    opts.lowerArm_border=0.2; % times the lower arm length
    opts.upperArm_halfWidth=0.07;
    opts.upperArm_border=0.3;
    opts.scaleFaceMedian=1;
    opts.faceWidth=0.328; % times the face length
    opts.faceBorder=0.328;
    opts.faceMethod='eyeShoulder_join';
    opts.torsoBorder=0.43; % times the torso length
    opts.upperLeg_halfWidth=0.08; % times the upper leg length
    opts.upperLeg_border=0.316; % times the upper leg length
    opts.lowerLeg_halfWidth=0.0754; % times the lower leg length
    opts.lowerLeg_border=0.2261; % times the lower leg length
    opts.foot_width=0.1507; % times the lower leg length
    opts.foot_border=0.2261; % times the lower leg length
    opts.medialAxis_width=2;
 
  case 'relative_hardPolygon'
    opts.widthType='relative'; 
    opts.stickManType='hardPolygon';
    opts.hand_width=0.2; % 0.2 times the lower arm length
    opts.hand_border=1; % times the hand length/width (its a square)
    opts.lowerArm_halfWidth=0.07; % times the lower arm length
    opts.lowerArm_border=0.2; % times the lower arm length
    opts.upperArm_halfWidth=0.07;
    opts.upperArm_border=0.3;
    opts.scaleFaceMedian=1.5;
    opts.faceWidth=0.328; % times the face length
    opts.faceBorder=0.328;
    opts.faceMethod='shoulderPerpendicular';
    opts.torsoBorder=0.43; % times the torso length
    opts.upperLeg_halfWidth=0.08; % times the upper leg length
    opts.upperLeg_border=0.316; % times the upper leg length
    opts.lowerLeg_halfWidth=0.0754; % times the lower leg length
    opts.lowerLeg_border=0.2261; % times the lower leg length
    opts.foot_width=0.1507; % times the lower leg length
    opts.foot_border=0.2261; % times the lower leg length
    opts.medialAxis_width=2;

  case 'relative_softPolygonHardMedial'
    opts.widthType='relative'; 
    opts.stickManType='softPolygon_hardMedial';
    opts.hand_width=0.2; % 0.2 times the lower arm length
    opts.hand_border=1; % times the hand length/width (its a square)
    opts.lowerArm_halfWidth=0.07; % times the lower arm length
    opts.lowerArm_border=0.2; % times the lower arm length
    opts.upperArm_halfWidth=0.07;
    opts.upperArm_border=0.3;
    opts.scaleFaceMedian=1.5;
    opts.faceWidth=0.328; % times the face length
    opts.faceBorder=0.328;
    opts.faceMethod='shoulderPerpendicular';
    opts.torsoBorder=0.43; % times the torso length
    opts.upperLeg_halfWidth=0.08; % times the upper leg length
    opts.upperLeg_border=0.316; % times the upper leg length
    opts.lowerLeg_halfWidth=0.0754; % times the lower leg length
    opts.lowerLeg_border=0.2261; % times the lower leg length
    opts.foot_width=0.1507; % times the lower leg length
    opts.foot_border=0.2261; % times the lower leg length
    opts.medialAxis_width=2;
 
  otherwise
    error('Invalid options string %s\n',optsString);
end
