function ok=moveForward(obj,labelImg)

ok=false;

if(~strcmp(obj.state,'segStarted')),
  warning('Cant move unless segmentation is started\n');
end

if(obj.frameNum==obj.nFrames)
  warning('Cant go beyond nFrames\n');
  ok=false;
  return;
end

if(obj.frameNum<obj.framesSegmented),
  obj.frameNum=obj.frameNum+1;
  obj.vH.moveForward();
  ok=true;
  return;
end

if(obj.frameNum>obj.framesSegmented),
  ok=false;
  warning('Bug in flow propagator, frameNum>framesSegmented\n');
  return;
end

curSeg=obj.seg(:,:,obj.frameNum);
flowVector=obj.getNextFlow();
nxtSeg=propagateSeg(curSeg,flowVector);
obj.segPropagated=nxtSeg;

%nxtSeg(nxtSeg==128)=0; % All unknown pixels are assigned to bg right now

obj.frameNum=obj.frameNum+1;
obj.framesSegmented=obj.framesSegmented+1;
obj.vH.moveForward();
obj.seg(:,:,obj.frameNum)=obj.graphCutSeg(nxtSeg,labelImg(:,:,obj.frameNum));
ok=true;
