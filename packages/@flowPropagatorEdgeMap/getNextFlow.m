function flowVec=getNextFlow(obj)
% flowVec is [h x w x 2] array
% flowVec(:,:,1) returns x motion
% flowVec(:,:,2) returns y motion
% The flow vector is the flow from (obj.frameNum+1) to (obj.frameNum)

frameNum=obj.frameNum;
flowFile=[obj.flowDir sprintf('backward-%04d.mat',frameNum+1)];
if(~exist(flowFile,'file')),
  error('No flow file %s exists\n',flowFile);
end
tmpLoad=load(flowFile);
flowVec=tmpLoad.flowVec;
[h w nCh]=size(flowVec);
if(h~=obj.h | w~=obj.w | nCh~=2),
  error('Improper dimesnions in flow file: %s\n',flowFile);
end
