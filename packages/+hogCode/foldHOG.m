function f = foldHOG(w, hogOpts)
% f = foldHOG(w)
% Condense HOG features into one orientation channel.

if hogOpts.classicOnly
  if hogOpts.featureMap
    f = [w(:,:,1:9) w(:,:,(1:9)+9) w(:,:,(1:9)+18) ] ;
  else
    f = w(:,:,1:9) ;
  end
else
  if hogOpts.featureMap
    f = [w(:,:,(1:9)+0)+w(:,:,(1:9)+9+0 )+w(:,:,(1:9)+18+0), ...
        w(:,:,(1:9)+0+31)+w(:,:,(1:9)+9+31)+w(:,:,(1:9)+18+31), ...
        w(:,:,(1:9)+0+62)+w(:,:,(1:9)+9+62)+w(:,:,(1:9)+18+62)] ;
  else
    f = [w(:,:,1:9)+w(:,:,(1:9)+9)+w(:,:,(1:9)+18)] ;
  end
end
