function compile_mex()

cwd=miscFns.extractDirPath(mfilename('fullpath'));

mexCmds=cell(0,1);
%mexCmds{end+1} = sprintf('mex -O %s/resize.cc -outdir %s/',cwd,cwd);
%mexCmds{end+1} = sprintf('mex -O %s/dt.cc -outdir %s/',cwd,cwd);
%mexCmds{end+1} = sprintf('mex -O %s/features.cc -outdir %s/',cwd,cwd);
mexCmds{end+1} = sprintf('mex -O %s/mex_getHog.cc -outdir %s/',cwd,cwd);
mexCmds{end+1} = sprintf('mex -O %s/mex_getHogWithMask.cc -outdir %s/',cwd,cwd);
%mexCmds{end+1} = sprintf('mex -O %s/fconvsMT.cc -outdir %s/',cwd,cwd);

for i=1:length(mexCmds)
  fprintf('Executing %s\n',mexCmds{i});
  eval(mexCmds{i});
end
