function gmm=updateGmm(features,gmm,updateIters)

for i=1:updateIters
  w=computeW(features,gmm);
  gmm=computeGmm(features,w);
end

function gmmModel=computeGmm(features,w)
  N=size(w,2);
  nMix=size(w,1);
  D=size(features,1);

  pi=zeros(nMix,1);
  mu=zeros(D,nMix);
  sigma=zeros(D,D,nMix);

  for c=1:nMix
    psum=sum(w(c,:));
    if(N<=2), 
      pi(c)=0;
    else
      pi(c)=psum/N;  
    end;
    if(pi(c)<eps)
      fprintf('Gaussian mixture number %d now has weight 0 , will be ignored\n',c);
      mu(:,c)=1;
      sigma(:,:,c)=eye(D);
      pi(c)=0;
    else
      tmpMu=sum(features.*repmat(w(c,:),D,1),2)/psum;
      mu(:,c)=tmpMu;
      tmpFeatures=features-repmat(tmpMu,1,N);
      tmpSigma=(tmpFeatures.*repmat(w(c,:),D,1))*tmpFeatures';
      tmpSigma=tmpSigma/psum;
      if(max(diag(tmpSigma))<1e-50),
        fprintf('Gaussian number %d was too unform, resetting its covariance\n',c);
        sigma(:,:,c)=1e-50*eye(D); % Assuming data between [0,1] , quick hack!
      else
        sigma(:,:,c)=gmm.vag_covarFix(tmpSigma);
      end
    end
  end
  gmmModel.mu=mu;
  gmmModel.sigma=sigma;
  gmmModel.pi=pi;

function w=computeW(features,gmmModel)

  w=zeros(length(gmmModel.pi),size(features,2));
  for i = 1:length( gmmModel.pi )
    w(i,:)=gmm.vag_normPdf(features,gmmModel.mu(:,i),gmmModel.sigma(:,:,i),gmmModel.pi(i));
  end
  gmmp=sum(w,1);
  % Normalise the w's
  % Sometimes, far away points can be added, and gmmp 
  % is actually zero, hack around and assign them to the largest
  % gaussian
  [maxPi,maxPi_idx]=max(gmmModel.pi);
  zeroMask=(gmmp==0);
  gmmp(zeroMask)=1;
  w(maxPi_idx,zeroMask)=1;
  w=w./repmat(gmmp,length(gmmModel.pi),1);
