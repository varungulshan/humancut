function gmm = init_gmmBS(data,nMixtures);
%% Initialize a GMM using binary split
% 
% Input:
%  data - D x N array of D-dimensional features
%  nMixtures - the number of gaussian mixtures
% Output parameters:
%   gmm - A Gaussian mixture model structure.

import gmm.*

[D,N]=size(data);
[cluster, colours] = nema_vector_quantize( data, nMixtures );
gmm.mu=zeros(D,nMixtures);
gmm.sigma=zeros([D D nMixtures]);
gmm.pi=zeros(1,nMixtures);
for i = 1:nMixtures
  idx = find( cluster == i );
  
  if(length(idx)<=2), 
    gmm.sigma(:,:,i)=eye(D);
    gmm.pi(i)=0; %Effectively removing the gaussian
    gmm.mu(:,i)=0;
    fprintf('bs init:Gaussian number %d has been set to weight=0 (<=2 features assigned)\n',i);
  else
    gmm.mu( :, i ) = mean( data( :, idx ), 2 );
    gmm.sigma( :, :, i ) = cov( data( :, idx )' );
    if(max(diag(gmm.sigma(:,:,i)))<realmin),
      fprintf('bs init: Gaussian number %d was too unform, resetting its covariance\n',i);
      fprintf('This gaussian was assigned %d features\n',length(idx));
      %gmm.pi(i)=0;
      gmm.pi( i ) = length( idx );
      gmm.sigma(:,:,i)=0.0001*eye(D);  % I AM ASSUMING DATA BETWEEN [0,1]!!, QUICK HACK
      %fprintf('bs init: Gaussian number %d has been set to weight=0 (too uniform)\n',i);
      %fprintf('This gaussian was assigned %d features\n',length(idx));
      %gmm.pi(i)=0;
      %gmm.sigma(:,:,i)=eye(D);
    else
      gmm.sigma(:,:,i)=vag_covarFix(gmm.sigma(:,:,i));
      gmm.pi( i ) = length( idx );
    end
  end;
end
piSum=sum(gmm.pi);
if(piSum>0) % Sometimes piSum = 0, not enough data points are passed, hence all pi are set to 0.
  gmm.pi = gmm.pi / sum( gmm.pi );
end
