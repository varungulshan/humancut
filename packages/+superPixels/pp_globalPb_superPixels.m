function ppSuperpixels(params)
% Runs brox's code on dataset, and saves and visualizes it

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('params','var'))
  params.rootOut_dir=[cwd '../../results/gPb_sPixels/'];
  params.opts.maxSize_pb=100;
  params.opts.visualize=true;
  params.opts.tmpDir=[cwd 'tmp/'];
  params.opts.visOpts.imgExt='jpg';
  params.opts.visOpts.kThresh=[10 30 50 100];
  params.opts.visOpts.boundaryClr=[255 0 0];
  params.opts.visOpts.boundaryWidth=1;
  params.overWrite=true;
  params.testBench_cmd='testBench.getTests_tmp()';
  %params.testBench_cmd='testBench.getTests_20frames()';
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',params.rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
end

[videoPaths,videoFiles]=eval(params.testBench_cmd);

videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);
for i=1:length(videoNames)
  outDirs{i}=[params.rootOut_dir videoNames{i} '/'];
end

for i=1:length(videoNames)
  fprintf('\nNow running on %s\n',[videoPaths{i} videoFiles{i}]);
  runOnVideo(videoPaths{i},videoFiles{i},outDirs{i},params.opts,params.overWrite);
end

function runOnVideo(videoPath,videoFile,outDir,opts,overWrite)

if(exist(outDir,'dir')),
  if(~overWrite)
    fprintf('Cannot overwrite exisiting results in: %s\n',outDir);
    return;
  else
    fprintf('\n\n---- Overwriting results in %s----------\n\n\n',outDir);
  end
else
  mkdir(outDir);
end

videoName=miscFns.extractVideoNames({videoPath},{videoFile});
videoName=videoName{1};
vH=myVideoReader(videoPath,videoFile);
nFrames=vH.nFrames;

h=vH.h;w=vH.w;
maxDim=max(h,w);
rescale=min(1,opts.maxSize_pb/maxDim);

for i=1:nFrames
  img=vH.curFrame;
  tmp_inFile=[opts.tmpDir 'inImg.png'];
  imwrite(img,tmp_inFile);
  outFile=[outDir sprintf('gPb_%05d.mat',i)];
  globalPb(miscFns.shortenPath(tmp_inFile),miscFns.shortenPath(outFile),rescale);
  load(outFile,'gPb_orient');
  ucm=contours2ucm(gPb_orient,'imageSize');
  ucm2=contours2ucm(gPb_orient,'doubleSize');
  imwrite(ucm,[outDir sprintf('ucm_%05d.bmp',i)]);
  imwrite(ucm2,[outDir sprintf('ucm2_%05d.bmp',i)]);
  if(opts.visualize)
    for j=1:length(opts.visOpts.kThresh)
      kT=opts.visOpts.kThresh(j);
      superPixelImg=drawBoundary(ucm,kT,img,opts.visOpts.boundaryClr,...
                                 opts.visOpts.boundaryWidth);
      imwrite(superPixelImg,[outDir sprintf('sp_%05d_thresh%03d.%s',i,...
                            kT,opts.visOpts.imgExt)]);
    end
  end
  if(i~=nFrames)
    vH.moveForward();
  end
end

delete(vH);

function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);

function img=drawBoundary(ucm,kT,img,bdryClr,bdryWidth)

mask=(ucm>=kT);
se=strel('disk',bdryWidth);
mask=imdilate(mask,se);

nnzMask=nnz(mask);
nCh=size(img,3);
mask=repmat(mask,[1 1 nCh]);
bdryClr=reshape(bdryClr,[1 nCh]);
bdryClr=repmat(bdryClr,[nnzMask 1]);

img(mask)=bdryClr(:);
