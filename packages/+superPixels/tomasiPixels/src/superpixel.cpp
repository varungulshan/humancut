#include "superpixel.h"
#include "cv.h"
#include <cxerror.h>
#include <algorithm>
#include <queue>
#include <vector>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include "assert.h"
#include <common.h>

using namespace std;

/// implements the operator (sp1->quality) > (sp2->quality)
struct compareQuality{
  public:
    bool operator()(const superpixel* sp1,const superpixel* sp2){
        return (sp1->quality) > (sp2->quality);
    }
};

vector<superpixel*> makeSuperpixels(const cv::Mat &inImg,
              const cv::Mat &mask,const superpixelOpts &opts){

  // ---- Do sanity checks on images --------------
  if(!inImg.isContinuous())
    CV_Error(CV_BadStep,"inImg should be continuos storage\n");
  if(!mask.isContinuous())
    CV_Error(CV_BadStep,"mask should be continuos storage\n");

  if(inImg.depth()!=CV_64F)
    CV_Error(CV_BadDepth,"inImg should be of type double\n");
  if(mask.type()!=CV_8UC1)
    CV_Error(CV_BadDepth,"mask should be of type unsigned char\n");

  cv::Size imgSize=inImg.size();
  // Compute X and Y gradients of inImg
  cv::Mat Ix=cv::Mat(imgSize,inImg.type());
  cv::Mat Iy=cv::Mat(imgSize,inImg.type());

  cv::Sobel(inImg,Ix,Ix.depth(),1,0,opts.derivativeScale);
  cv::Sobel(inImg,Iy,Iy.depth(),0,1,opts.derivativeScale);

  cv::Mat tmp_Ixy=cv::Mat(imgSize,inImg.type());
  cv::multiply(Ix,Iy,tmp_Ixy);

  cv::Mat Ixy=cv::Mat(imgSize,CV_64FC1);
  sumDimensions(tmp_Ixy,Ixy);
  tmp_Ixy.release();

  cv::multiply(Ix,Ix,Ix); // Ix=Ix.*Ix
  cv::multiply(Iy,Iy,Iy); // Iy=Iy.*Iy

  // Note: now Ix and Iy actually store Ixx, Iyy
  cv::Mat Ixx=cv::Mat(imgSize,CV_64FC1);
  cv::Mat Iyy=cv::Mat(imgSize,CV_64FC1);

  sumDimensions(Ix,Ixx);
  sumDimensions(Iy,Iyy);

  Ix.release();Iy.release();

  assert(Ixx.isContinuous());
  assert(Iyy.isContinuous());
  assert(Ixy.isContinuous());
  double *ptr_Ixy,*ptr_Ixx,*ptr_Iyy;
  const unsigned char *ptr_mask;
  int w=imgSize.width;
  int h=imgSize.height;
  ptr_Ixx=Ixx.ptr<double>();
  ptr_Iyy=Iyy.ptr<double>();
  ptr_Ixy=Ixy.ptr<double>();
  ptr_mask=mask.ptr<unsigned char>();

  vector<superpixel*> spForest; // Stores the forest of superpixels 
  // Can be a forest because the mask can have >= 1 connected components

  priority_queue<superpixel*,vector<superpixel*>,compareQuality> pixelQ;
  pixelGraph g(0); // Create empty graph
  superpixel *spList[h*w]; // to initially store superpixels for each pixel

  for(int i=0;i<h*w;i++) {spList[i]=NULL;}

  for(int y=0;y<h;y++){
    for(int x=0;x<w;x++){
      if(*ptr_mask){
        superpixel *sp = new superpixel(x,y);
        cv::Mat *m=new cv::Mat(2,2,CV_64FC1);
        m->at<double>(0,0)=*ptr_Ixx;
        m->at<double>(1,1)=*ptr_Iyy;
        m->at<double>(0,1)=*ptr_Ixy;
        m->at<double>(1,0)=*ptr_Ixy;

        sp->M=m;
        sp->quality=getQuality(*m);
        sp->area=1;

        vertex tmp=boost::add_vertex(g);
        g[tmp]=sp; // Setting the internal property of the vertex to the superpixel
        sp->v=tmp; // Set the vertex descriptor of the superpixel to this node
        spList[y*w+x]=sp;

        pixelQ.push(sp);
      }
      ptr_Ixx++;ptr_Iyy++;ptr_Ixy++;
      ptr_mask++;
    }
  }
  // --- all base superpixels created, and nodes for the graph created
  // --- now create the graph structure, and then we can start merging nodes --

  const unsigned char *maskData=mask.ptr<unsigned char>();
  int nOffsets=opts.rOffset.size();
  assert(nOffsets==opts.cOffset.size());
  for(int y=0;y<h;y++){
    for(int x=0;x<w;x++){
      int idx=y*w+x; 
      if(maskData[idx]){
        superpixel *sp=spList[idx];
        assert(sp!=NULL);
        vertex v=sp->v;
        // Loop over neighbours
        for(int i=0;i<nOffsets;i++){
          int nbrX=x+opts.cOffset[i];
          int nbrY=y+opts.rOffset[i];
          if(nbrX>=0 && nbrX<w && nbrY>=0 && nbrY<h){
            int nbrIdx=nbrY*w+nbrX;
            if(maskData[nbrIdx]){
              superpixel *nbrSp=spList[nbrIdx];
              assert(nbrSp!=NULL);
              vertex nbrV=nbrSp->v;
              boost::add_edge(v,nbrV,g);
            }
          }
        }

      }
    }
  }

  // --- graph and superpixels have been initialized, now start merging superpixels

  while(!pixelQ.empty()){
    superpixel *sp=pixelQ.top();
    pixelQ.pop();
    // check if this superpixel already has a parent or not
    if(sp->parent==NULL){
      // assert: sp->v is a valid vertex descriptor
      vertex v=sp->v;

      if(boost::out_degree(v,g)==0){
        spForest.push_back(sp);
      }
      else{
        adjIter ai,ai_end;
        vertex nbrV;
        double minQuality=DBL_MAX;
        for(boost::tie(ai,ai_end)=boost::adjacent_vertices(v,g);
            ai!=ai_end;++ai){
          // assert: the superpixel pointer of *ai should not be null
          assert(g[*ai]!=NULL);
          if(g[*ai]->quality < minQuality){
            nbrV=*ai;minQuality=g[nbrV]->quality;
          }
        }

        // assert: nbrV is the neigbour of v with least quality
        superpixel *nbrSp=g[nbrV];

        // assert: parent of any graph nodes superpixel should be null
        assert(nbrSp->parent==NULL);

        // assert: the vertices to be merged are v and nbrV
        if(boost::out_degree(v,g)<boost::out_degree(nbrV,g)){
          vertex tmp=v;v=nbrV;nbrV=tmp;  
        }

        // assert: vertex v has a higher degree than nbrV

        // --- merge the node nbrV into v ------------------------

        // adding all neighbours of nbrV as neighbours of v
        for(boost::tie(ai,ai_end)=boost::adjacent_vertices(nbrV,g);
            ai!=ai_end;++ai){
          if(*ai!=v){
            std::pair<edge,bool> edgeCheck;
            edgeCheck=boost::edge(*ai,v,g);
            if(edgeCheck.second==false)
              boost::add_edge(*ai,v,g);
          }
        }
        // now delete the node nbrV and all its edges
        boost::clear_vertex(nbrV,g);
        boost::remove_vertex(nbrV,g);

        // --- merge the super-pixel structures in sp and nbrSp ---
        superpixel *spUnion=new superpixel();
        spUnion->child1=sp;
        spUnion->child2=nbrSp;
        spUnion->area=sp->area+nbrSp->area;
        spUnion->v=v;
        spUnion->M=new cv::Mat(2,2,CV_64FC1);
        cv::add(*(sp->M),*(nbrSp->M),*(spUnion->M));
        spUnion->quality=getQuality(*(spUnion->M));

        // --- update the children of this combined superpixel
        sp->parent=spUnion;
        nbrSp->parent=spUnion;

        // --- push this superpixel into the queue ---
        pixelQ.push(spUnion);

        // --- set the graph node superpixel to this one ---
        g[v]=spUnion;

      } // endif: >= 0 neigbours of vertex v
    }// endif: superpixel sp has no parent
  } //end while: the heap is not empty

  return spForest;

}

superpixel::superpixel(){
  this->init();
}

superpixel::superpixel(int x,int y){
  this->init();
  this->x=x;this->y=y;
}

void superpixel::init(){
  parent=NULL;
  child1=NULL;
  child2=NULL;
  quality=0;
  M=NULL;
  x=-1;
  y=-1;
  area=0;
}

superpixel::~superpixel(){
  if(child1!=NULL) delete(child1);
  if(child2!=NULL) delete(child2);
  if(M!=NULL) delete(M);
}

superpixelOpts::superpixelOpts(){
  this->init();
}

void superpixelOpts::init(){
  derivativeScale=3;
  //int rOffset[]={1,1,0,-1};
  //int cOffset[]={0,1,1,1};
  int rOffset[]={1,0};
  int cOffset[]={0,1};
  this->rOffset=vector<int>(rOffset,rOffset+sizeof(rOffset)/sizeof(int));
  this->cOffset=vector<int>(cOffset,cOffset+sizeof(cOffset)/sizeof(int));
}
