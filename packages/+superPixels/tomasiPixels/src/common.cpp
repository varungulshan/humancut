#include <common.h>
#include "cv.h"
#include <cxerror.h>
#include "assert.h"
#include <cstdio>
#include <queue>

void sumDimensions(const cv::Mat &src,cv::Mat &dst){
  if( (!src.isContinuous() )|| (!dst.isContinuous())){
    CV_Error(CV_BadStep,"input images should be continuous\n");
  }

  int nCh=src.channels();
  assert(src.depth()==CV_64F && dst.depth()==CV_64F);
  assert(src.size()==dst.size());
  assert(dst.channels()==1);

  const double *ptr_src=src.ptr<double>();
  double *ptr_dst=dst.ptr<double>();

  cv::Size imgSize=src.size();
  for(int i=0;i<imgSize.width*imgSize.height;i++){
    *ptr_dst=0;
    for(int j=0;j<nCh;j++){
      (*ptr_dst)+=(*ptr_src);
      ptr_src++;
    }
    ptr_dst++;
  }
}

double getQuality(const cv::Mat &M){
  cv::Mat tmpEigen=cv::Mat(2,1,CV_64FC1);
  cv::eigen(M,tmpEigen,1,1); // Only compute the smallest eigen value
  return tmpEigen.at<double>(0,0);
}

void blendLabels(const cv::Mat &inImg,const cv::Mat &labelImg,
                cv::Mat &blendImg,double alpha){

  assert(inImg.isContinuous());
  assert(labelImg.isContinuous());
  assert(blendImg.isContinuous());

  assert(inImg.depth()==CV_64F);
  assert(blendImg.depth()==CV_64F);

  assert(inImg.channels()==3);
  assert(blendImg.channels()==3);

  char colorCube_file[]="../misc/colors.txt";
  FILE *fH=fopen(colorCube_file,"r");
  if(fH==NULL){
    CV_Error(CV_StsError,"colorCube file not found!\n");
  }

  int numColors;
  fscanf(fH,"%d",&numColors);
  double colorCube[3*numColors];
  for(int i=0;i<numColors;i++){
    double r,g,b;
    fscanf(fH,"%lf %lf %lf",&r,&g,&b);
    colorCube[3*i]=b;
    colorCube[3*i+1]=g;
    colorCube[3*i+2]=r;
  }
  fclose(fH);
  
  const double *ptr_img;
  double *ptr_blend;
  const int *ptr_labels;
  
  ptr_img=inImg.ptr<double>();
  ptr_blend=blendImg.ptr<double>();
  ptr_labels=labelImg.ptr<int>();

  int h=inImg.rows;
  int w=inImg.cols;
  int N=h*w;
  for(int i=0;i<N;i++){
    int label=*ptr_labels;
    int clrIdx=label%numColors;
    double *clr=colorCube+3*clrIdx;
    *ptr_blend=alpha*(*clr)+(1-alpha)*(*ptr_img);
    ptr_blend++;clr++;ptr_img++;
    *ptr_blend=alpha*(*clr)+(1-alpha)*(*ptr_img);
    ptr_blend++;clr++;ptr_img++;
    *ptr_blend=alpha*(*clr)+(1-alpha)*(*ptr_img);
    ptr_blend++;clr++;ptr_img++;
    ptr_labels++;
  }

#if 1
  // Draw superpixel boundaries in black
  double bdryColor[3]={0,0,0};
  int rOffsets[]={0 ,-1, 0,+1};
  int cOffsets[]={-1, 0,+1, 0};
  int nOffsets=sizeof(rOffsets)/sizeof(int);
  typedef std::pair<int,int> pix;
  pix pixOne(0,0);
  bool *visited=new bool[N];
  memset(visited,0,N*sizeof(bool));
  std::queue<pix> bfs;
  bfs.push(pixOne);
  visited[0]=true;
  while(!bfs.empty()){
    pix p=bfs.front();
    bfs.pop();
    int x,y;
    x=p.first;y=p.second;
    //assert(visited[y*w+x]);
    //visited[y*w+x]=true;
    const int myLabel=labelImg.at<int>(y,x);
    bool allSame=true;
    for(int i=0;i<nOffsets;i++){
      int x2,y2;
      x2=x+cOffsets[i];
      y2=y+rOffsets[i];
      if(x2>=0 && x2<w && y2>=0 && y2<h){
        if(!visited[y2*w+x2]){
          pix p2(x2,y2);
          bfs.push(p2);
          visited[y2*w+x2]=true;
        }
        const int nbrLabel=labelImg.at<int>(y2,x2);
        if(nbrLabel!=myLabel) allSame=false;
      }
    }
    if(!allSame) {// Then i am a superpixel boundary pixel
      double *ptr_blend=blendImg.ptr<double>(y)+3*x;
      for(int i=0;i<3;i++){
        *ptr_blend=bdryColor[i];ptr_blend++;
      }
    }
  }
  delete(visited);
#endif
  
}
