#include <superpixel.h>
#include <superpixel2.h>
#include <queue>
#include <cstdio>
#include <cv.h>
#include <cxcore.h>
#include <cxerror.h>

using namespace std;

void drawPixel(superpixel *sp,int idx,cv::Mat &labelImg);
void drawPixel(superpixel2 *sp,int idx,cv::Mat &labelImg);

void getLabelImg(vector<superpixel*> spForest,double lambdaThresh,int areaThresh,cv::Mat &labelImg){
  
  assert(labelImg.type()==CV_32SC1);
  assert(labelImg.isContinuous());

  int *ptr_labels=labelImg.ptr<int>();
  cv::Size imgSize=labelImg.size();
  int h=imgSize.height;
  int w=imgSize.width;

  for(int i=0;i<h*w;i++,ptr_labels++) *ptr_labels=(int)0;

  queue<superpixel*> q;
  for(int i=0;i<spForest.size();i++){
    q.push(spForest[i]);
  }

  int nxtIdx=1;
  while(!q.empty()){
    superpixel *sp=q.front();
    q.pop();
    if(sp->child1 == NULL || sp->child2 ==NULL){
      drawPixel(sp,nxtIdx,labelImg);
      nxtIdx++;
    }
    else{
      superpixel *child1=sp->child1;
      superpixel *child2=sp->child2;
      if(child1->quality >= lambdaThresh && child1->area >= areaThresh &&
         child2->quality >= lambdaThresh && child2->area >= areaThresh){
        q.push(child1);
        q.push(child2);
      }
      else{
        drawPixel(sp,nxtIdx,labelImg);
        nxtIdx++;
      }

    }
  }

}

void drawPixel(superpixel *sp,int idx,cv::Mat &labelImg){
  queue<superpixel*> q;
  q.push(sp);
  while(!q.empty()){
    superpixel *spTop=q.front();
    q.pop();
    if(spTop->child1==NULL || spTop->child2==NULL){
      // This is bottom most pixel
      int x=spTop->x;int y=spTop->y;
      labelImg.at<int>(y,x)=idx;
    }else{
      q.push(spTop->child1);
      q.push(spTop->child2);
    }
  }
}

void getLabelImg(vector<superpixel2*> spForest,double lambdaThresh,int areaThresh,cv::Mat &labelImg){
  
  assert(labelImg.type()==CV_32SC1);
  assert(labelImg.isContinuous());

  int *ptr_labels=labelImg.ptr<int>();
  cv::Size imgSize=labelImg.size();
  int h=imgSize.height;
  int w=imgSize.width;

  for(int i=0;i<h*w;i++,ptr_labels++) *ptr_labels=(int)0;

  queue<superpixel2*> q;
  for(int i=0;i<spForest.size();i++){
    q.push(spForest[i]);
  }

  int nxtIdx=1;
  while(!q.empty()){
    superpixel2 *sp=q.front();
    q.pop();
    if(sp->child1 == NULL || sp->child2 ==NULL){
      drawPixel(sp,nxtIdx,labelImg);
      nxtIdx++;
    }
    else{
      superpixel2 *child1=sp->child1;
      superpixel2 *child2=sp->child2;
      if(child1->quality >= lambdaThresh && child1->area >= areaThresh &&
         child2->quality >= lambdaThresh && child2->area >= areaThresh){
        q.push(child1);
        q.push(child2);
      }
      else{
        drawPixel(sp,nxtIdx,labelImg);
        nxtIdx++;
      }

    }
  }

}

void drawPixel(superpixel2 *sp,int idx,cv::Mat &labelImg){
  queue<superpixel2*> q;
  q.push(sp);
  while(!q.empty()){
    superpixel2 *spTop=q.front();
    q.pop();
    if(spTop->child1==NULL || spTop->child2==NULL){
      // This is bottom most pixel
      int x=spTop->x;int y=spTop->y;
      labelImg.at<int>(y,x)=idx;
    }else{
      q.push(spTop->child1);
      q.push(spTop->child2);
    }
  }
}

