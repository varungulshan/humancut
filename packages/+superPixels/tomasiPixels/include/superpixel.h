#ifndef _SUPERPIXEL_H
#define _SUPERPIXEL_H

#include <vector>
#include "cv.h"
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <common.h>

/// Class to store the hierarchical super pixel structure
/*!
  Super pixels are stored as binary trees
*/

class superpixel; // forward declaration

typedef boost::adjacency_list<boost::listS,boost::listS,boost::undirectedS,superpixel*>
        pixelGraph;
typedef boost::graph_traits<pixelGraph>::adjacency_iterator adjIter;
typedef boost::graph_traits<pixelGraph>::vertex_descriptor vertex;
typedef boost::graph_traits<pixelGraph>::edge_descriptor edge;

using namespace std;

class superpixel{

  public:
    superpixel *parent;
    superpixel *child1,*child2;
    double quality;
    cv::Mat *M;
    int x,y; // Only useful for leaf nodes
    vertex v; // Vertex descriptor of the corresponding graph node
    int area; // in number of pixels

    superpixel(); /// Default constructor

    superpixel(int,int); /// superpixel(x,y);

    ~superpixel(); /// Destructor desctroys the entire subtree

  private:
    void init(); /// Called from constructor to set everyting to empty
};

class superpixelOpts{
  public:
    int derivativeScale; // default=3
    vector<int> rOffset; // default is 4 nbrhood 
    vector<int> cOffset; // default is 4 nbrhood
    superpixelOpts();
  private:
    void init(); /// Initialize with default options
};

/// Function to make superpixel tree given an image
/*!
  mask -> only build superpixel in this region
*/
vector<superpixel*> makeSuperpixels(const cv::Mat &inImg,const cv::Mat &mask, const superpixelOpts &opts);

/// Function to make a labelImg, given the superpixel forest
/*!
  label 0 for all the pixels which are not covered by the forest, all other get labels starting at 1
*/
void getLabelImg(vector<superpixel*> spForest,double lambdaThresh,int areaThresh,cv::Mat &labelImg);

#endif
