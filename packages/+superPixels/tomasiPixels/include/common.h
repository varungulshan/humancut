#ifndef _COMMON_H
#define _COMMON_H

#include <cxcore.h>

void blendLabels(const cv::Mat &inImg,const cv::Mat &labelImg,
                cv::Mat &blendImg,double blendAlpha=0.5);

/// Returns the min eigen value of matrix M
double getQuality(const cv::Mat &M);

/// Sums the dimensions of src into one dimension of dst
void sumDimensions(const cv::Mat &src,cv::Mat &dst);

#endif
