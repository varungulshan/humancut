#ifndef _SUPERPIXEL2_H
#define _SUPERPIXEL2_H

#include <vector>
#include "cv.h"
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <common.h>

/// Class to store the hierarchical super pixel structure
/*!
  Super pixels are stored as binary trees
*/

class superpixel2; // forward declaration

typedef boost::adjacency_list<boost::listS,boost::listS,boost::undirectedS,superpixel2*,int>
        pixelGraph2;
typedef boost::graph_traits<pixelGraph2>::adjacency_iterator adjIter2;
typedef boost::graph_traits<pixelGraph2>::out_edge_iterator outEdgeIter2;
typedef boost::graph_traits<pixelGraph2>::vertex_descriptor vertex2;
typedef boost::graph_traits<pixelGraph2>::edge_descriptor edge2;

using namespace std;

class superpixel2{

  public:
    superpixel2 *parent;
    superpixel2 *child1,*child2;
    double quality;
    cv::Mat *M;
    int x,y; // Only useful for leaf nodes
    vertex2 v; // Vertex descriptor of the corresponding graph node
    int area; // in number of pixels
    int perimeter; // boundary length of superpixel

    superpixel2(); /// Default constructor

    superpixel2(int,int); /// superpixel(x,y);

    ~superpixel2(); /// Destructor desctroys the entire subtree

  private:
    void init(); /// Called from constructor to set everyting to empty
};

class superpixel2Opts{
  public:
    int derivativeScale; // default=3
    vector<int> rOffset; // default is 4 nbrhood 
    vector<int> cOffset; // default is 4 nbrhood 
    superpixel2Opts();
  private:
    void init(); /// Initialize with default options
};

/// Function to make superpixels, different merging criteria
/*!
  superpixel is merged with the neighbour, so as to yield the highest
  area/perimeter ratio (compactness)
*/
vector<superpixel2*> makeSuperpixels2(const cv::Mat &inImg,const cv::Mat &mask, const superpixel2Opts &opts);

/// Function to make a labelImg, given the superpixel forest
/*!
  label 0 for all the pixels which are not covered by the forest, all other get labels starting at 1
*/
void getLabelImg(vector<superpixel2*> spForest,double lambdaThresh,int areaThresh,cv::Mat &labelImg);

#endif
