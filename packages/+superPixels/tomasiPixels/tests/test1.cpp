#include <superpixel.h>
#include <highgui.h>
#include <cv.h>
#include <iostream>
#include <cstdio>

int main(int argc,char *argv[]){
  char imgPath[]="../tests/testImg.png";
  char maskPath[]="../tests/testMask.png";

  vector<superpixel*> ans;
  cv::Mat inImg=cv::imread(imgPath,-1);
  cv::Mat inMask=cv::imread(maskPath,-1);
  cv::Mat inImg_dbl;

  if(inImg.empty()){
    CV_Error(CV_HeaderIsNull,"Could not read input image\n");
  }

  if(inMask.empty()){
    CV_Error(CV_HeaderIsNull,"Could not read mask image\n");
  }

  inImg.convertTo(inImg_dbl,CV_64F,1.0/255);
  superpixelOpts opts; // Use constructor for default opts
  ans=makeSuperpixels(inImg_dbl,inMask,opts);

  for(int i=0;i<ans.size();i++){
    printf("Superpixel #%d, Area=%d, Quality=%.2f\n",i,ans[i]->area,ans[i]->quality);
  }

  for(int i=0;i<ans.size();i++){
    delete(ans[i]); // The destructor for superpixel should clear the entire tree
                    // recursively
  }
}
