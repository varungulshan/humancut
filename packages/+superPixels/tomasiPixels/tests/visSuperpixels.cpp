#include <superpixel.h>
#include <highgui.h>
#include <cv.h>
#include <cxcore.h>
#include <iostream>
#include <cstdio>

int main(int argc,char *argv[]){
  char imgPath[]="../tests/testImg.png";
  char maskPath[]="../tests/testMask.png";
  char savePath[]="./tmp.png";

  vector<superpixel*> ans;
  cv::Mat inImg=cv::imread(imgPath,-1);
  cv::Mat inMask=cv::imread(maskPath,-1);
  cv::Mat inImg_dbl;

  if(inImg.empty()){
    CV_Error(CV_HeaderIsNull,"Could not read input image\n");
  }

  if(inMask.empty()){
    CV_Error(CV_HeaderIsNull,"Could not read mask image\n");
  }

  inImg.convertTo(inImg_dbl,CV_64F,1.0/255);
  superpixelOpts opts; // Use constructor for default opts
  ans=makeSuperpixels(inImg_dbl,inMask,opts);

  for(int i=0;i<ans.size();i++){
    printf("Superpixel #%d, Area=%d, Quality=%.2f\n",i,ans[i]->area,ans[i]->quality);
  }

  double lambdaStep=10;
  double lambdaThresh=160;
  int areaThresh=200;
  const int upKey_code=1113938;const int downKey_code=1113940;
  const int exitKey=1048696;
  const int saveKey_code=1048691;
  double blendAlpha=0.2;
  cv::Mat labelImg=cv::Mat(inImg.size(),CV_32SC1);
  cv::Mat blendImg=cv::Mat(inImg_dbl.size(),inImg_dbl.type());

  getLabelImg(ans,lambdaThresh,areaThresh,labelImg);
  blendLabels(inImg_dbl,labelImg,blendImg,blendAlpha);
  
  cv::namedWindow("superpixels",CV_WINDOW_AUTOSIZE);
  cv::imshow("superpixels",blendImg);
  printf("Showing superpixels at lambda thresh=%.2f, area thresh=%d\n",
      lambdaThresh,areaThresh);
  int key=0;
  printf("Keys:\nx->exit\nup down arrow keys -> change lambda thresh\n");
  while(key!=exitKey){
    key=cv::waitKey();
    printf("Code of key pressed = %d\n",key);
    switch(key){
      case upKey_code:
        lambdaThresh+=lambdaStep;
        printf("Showing superpixels at lambda thresh=%.2f, area thresh=%d\n",
              lambdaThresh,areaThresh);
        getLabelImg(ans,lambdaThresh,areaThresh,labelImg);
        blendLabels(inImg_dbl,labelImg,blendImg,blendAlpha);
        cv::imshow("superpixels",blendImg);
        break;
      case downKey_code:
        lambdaThresh-=lambdaStep;
        printf("Showing superpixels at lambda thresh=%.2f, area thresh=%d\n",
              lambdaThresh,areaThresh);
        getLabelImg(ans,lambdaThresh,areaThresh,labelImg);
        blendLabels(inImg_dbl,labelImg,blendImg,blendAlpha);
        cv::imshow("superpixels",blendImg);
        break;
      case saveKey_code:
        cv::Mat tmp;
        blendImg.convertTo(tmp,CV_8UC3,255.0);
        cv::imwrite(savePath,tmp);
        break;
    }
  }

  for(int i=0;i<ans.size();i++){
    delete(ans[i]); // The destructor for superpixel should clear the entire tree
                    // recursively
  }
}
