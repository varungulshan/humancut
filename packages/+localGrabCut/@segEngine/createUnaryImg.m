function [unaryImg,unaryMask]=createUnaryImg(obj)

labelImg=obj.labelImg;
features=obj.features;
seg=obj.seg;

opts=obj.opts;
localWin_rad=opts.localWin_rad;
localWin_dia=2*localWin_rad+1;

stepSize=localWin_dia/2;
[h w]=size(labelImg);

idxImg=reshape([1:h*w],[h w]);

unaryImg=zeros([h w 2]);
unaryMask=seg;
unaryCount=zeros([h w]);

for x=1:stepSize:w
  xMin=round(x);xMax=min(w,xMin+localWin_dia-1);
  for y=1:stepSize:h
    yMin=round(y);yMax=min(h,yMin+localWin_dia-1);
    cropSeg=seg(yMin:yMax,xMin:xMax);
    winSize=numel(cropSeg);
    smallerFraction=min(nnz(cropSeg==0),nnz(cropSeg==255))/winSize;
    if(smallerFraction<=opts.min_validFraction), continue; end;
    cropLabels=labelImg(yMin:yMax,xMin:xMax);
    ftrIds=idxImg(yMin:yMax,xMin:xMax);
    cropFeatures=features(:,ftrIds(:));
    [cropUnaryImg,cropUnaryMask]=boxUnary(cropSeg,cropLabels,cropFeatures,...
                                         opts);
    unaryImg(yMin:yMax,xMin:xMax,:)=unaryImg(yMin:yMax,xMin:xMax,:)+cropUnaryImg;
    unaryMask(yMin:yMax,xMin:xMax)=cropUnaryMask;
    unaryCount(yMin:yMax,xMin:xMax)=unaryCount(yMin:yMax,xMin:xMax)+1;
  end
end

unaryCount(unaryCount==0)=1; % To prevent div by 0 in next command
unaryImg(:,:,1)=unaryImg(:,:,1)./unaryCount;
unaryImg(:,:,2)=unaryImg(:,:,2)./unaryCount;

function [unaryImg,unaryMask]=boxUnary(seg,labels,features,opts)

modelLearnMask=(labels~=5 & labels~=6);
fgMask=(modelLearnMask & seg==255);
tmpFeatures=features(:,fgMask(:));
gmmf=gmm.init_gmmBS(tmpFeatures,opts.gmmNmix_fg);

bgMask=(modelLearnMask & seg==0);
tmpFeatures=features(:,bgMask(:));
gmmb=gmm.init_gmmBS(tmpFeatures,opts.gmmNmix_bg);

% -- now compute the unaries
labels(labels==5)=1; 
labels(labels==6)=2;

ftrMask=(labels~=1 & labels~=2);
features=features(:,ftrMask(:));

fgLikeli=gmm.computeGmm_likelihood(features,gmmf)+eps;
bgLikeli=gmm.computeGmm_likelihood(features,gmmb)+eps;

fgLikeli=-log(fgLikeli);
bgLikeli=-log(bgLikeli);
myUB=realmax/(opts.gcScale*100);
fgLikeli(fgLikeli>myUB)=myUB;
bgLikeli(bgLikeli>myUB)=myUB;

[h w]=size(labels);
unaryImg=zeros([h w 2]);
tmp=zeros([h w]);tmp(ftrMask)=fgLikeli;
unaryImg(:,:,1)=tmp;
tmp=zeros([h w]);tmp(ftrMask)=bgLikeli;
unaryImg(:,:,2)=tmp;

unaryMask=128*ones([h w],'uint8');
unaryMask(labels==1)=255;
unaryMask(labels==2)=0;
