function opts=makeOpts(optsString)

switch(optsString)
   case 'quickTest'
    % use this if you dont really want to test localGrabCut itself
    % but just want to make sure its being called properly
    opts=localGrabCut.segOpts();
    opts.gcGamma_e=50;
    opts.gcGamma_i=0;
    opts.gcScale=50;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=1;
    
    opts.min_validFraction=0.02;
    opts.localWin_rad=10000;

   case 'iter10_global'
    opts=localGrabCut.segOpts();
    opts.gcGamma_e=150;
    opts.gcGamma_i=0;
    opts.gcScale=50;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=10;
    
    opts.min_validFraction=0.02;
    opts.localWin_rad=10000;

  case 'iter10_rad150'
    opts=localGrabCut.segOpts();
    opts.gcGamma_e=150;
    opts.gcGamma_i=0;
    opts.gcScale=50;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=10;
    
    opts.min_validFraction=0.02;
    opts.localWin_rad=150;

   case 'iter10_global_2'
    opts=localGrabCut.segOpts();
    opts.gcGamma_e=50;
    opts.gcGamma_i=0;
    opts.gcScale=50;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=10;
    
    opts.min_validFraction=0.02;
    opts.localWin_rad=10000;

  case 'iter10_rad150_2'
    opts=localGrabCut.segOpts();
    opts.gcGamma_e=50;
    opts.gcGamma_i=0;
    opts.gcScale=50;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=10;
    
    opts.min_validFraction=0.02;
    opts.localWin_rad=150;
  
  case 'iter10_rad80_2'
    opts=localGrabCut.segOpts();
    opts.gcGamma_e=50;
    opts.gcGamma_i=0;
    opts.gcScale=50;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=10;
    
    opts.min_validFraction=0.02;
    opts.localWin_rad=80;
    
  otherwise
    error('Invalid options string %s\n',optsString);
end
