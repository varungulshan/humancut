function view=view_4(obj)

frameNum=obj.frameNum;
if(frameNum==1)
  view=obj.seg(:,:,1);
  fprintf('First frame, no propagation of seg yet happened\n');
else
  view=obj.segPropagated;
  fprintf('Showing propageted segmentation from frame %d to frame %d\n',frameNum-1,frameNum);
end

