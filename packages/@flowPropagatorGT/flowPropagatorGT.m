classdef flowPropagatorGT < videoSegmenter
  properties (SetAccess=private, GetAccess=public)
    gamma
    debugLevel
    flowDir
    gtFile % gt for first frame
    vH
    gtSeg_firstFrame
    h
    w
    nFrames
    state % 'initialized','pped','segStarted'
    seg % uint8, seg variable for the entire volume
    frameNum
    framesSegmented
    video % only if debugLevel > 0
    segPropagated
  end

  methods
    function obj=flowPropagatorGT(gamma,debugLevel,flowDir,gtFile,vH)
      obj.gamma=gamma;
      obj.debugLevel=debugLevel;
      obj.flowDir=flowDir;
      obj.gtFile=gtFile; 
      obj.vH=vH;
      [obj.h obj.w xx obj.nFrames]=obj.vH.videoSize();
      obj.state='initialized';
      obj.seg=zeros([obj.h obj.w obj.nFrames],'uint8');
      obj.framesSegmented=0;
      obj.frameNum=1;
      obj.video=[];
    end
  end

  methods (Access=private)
    flowVector=getNextFlow(obj)
  end

end % of classdef
