function preProcess(obj)
  if(~strcmp(obj.state,'initialized')),
    error('Cant call preProcess from %s state\n',obj.state);
    return;
  end

  gtFile=obj.gtFile;
  if(~exist(gtFile,'file')),
    warning('No GT file found!\n');
    obj.gtSeg_firstFrame=255*ones([obj.h obj.w],'uint8');
  else
    obj.gtSeg_firstFrame=imread(gtFile);
  end

  if(~exist(obj.flowDir,'dir')),
    warning('Preprocessed flow dir %s does not exist!!\n',obj.flowDir);
  end

  if(obj.debugLevel>0),
    obj.video=getFullVideo(obj);
  end

  obj.state='pped'; 

function video=getFullVideo(obj)
  vH=obj.vH;
  curFrame_vH=vH.frameNum;
  [h w nCh nFrames]=vH.videoSize();
  vH.reload();
  video=zeros([h w nCh nFrames],'uint8');
  
  video(:,:,:,1)=vH.curFrame;
  for i=2:vH.nFrames
    vH.moveForward();
    video(:,:,:,i)=vH.curFrame;
  end
  vH.reload();
  for i=1:(curFrame_vH-1)
    vH.moveForward();
  end
  whos_output=whos('video');
  numBytes=whos_output.bytes;
  fprintf('Loaded entire video volume (=%.2f MB) in memory for diagnositics\n',numBytes/(1024*1024));
