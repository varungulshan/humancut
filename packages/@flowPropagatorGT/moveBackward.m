function ok=moveBackward(obj,labelImg)
ok=false;

if(~strcmp(obj.state,'segStarted')),
  warning('Cant move unless segmentation is started\n');
end

if(obj.frameNum==1),
  warning('Cant move backward, already on frame 1\n');
  ok=false;
  return;
end

obj.frameNum=obj.frameNum-1;
ok=true;
