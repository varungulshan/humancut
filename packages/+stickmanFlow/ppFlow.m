function ppFlow(params)
% Create stickman flow on dataset, and saves and visualizes it

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('params','var'))
  params.rootOut_dir=[cwd '../../results/stickmanFlow/'];
  params.opts.broxFlow_dir='/data/adam2/varun/broxFlow/';
  params.opts.optsString_h3dconversion='relative_softPolygonHardMedial2';
  params.opts.h3dAnno_dir=[cwd '../../data/h3dAnnotations_2/'];
  params.opts.visualize=true;
  params.opts.visOpts.imgExt='jpg';
  params.opts.visOpts.maxFlow_saturate=20;
  params.message='Flow is from t to t+1, so 00001.mat stores flow from t=1 to t=2';
  params.overWrite=true;
  params.testBench_cmd='testBench.getTests_proxy2()';
  %params.testBench_cmd='testBench.getTests_20frames()';
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',params.rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
end

[videoPaths,videoFiles]=eval(params.testBench_cmd);

videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);
for i=1:length(videoNames)
  outDirs{i}=[params.rootOut_dir videoNames{i} '/'];
end

for i=1:length(videoNames)
  fprintf('\nNow running on %s\n',[videoPaths{i} videoFiles{i}]);
  runOnVideo(videoPaths{i},videoFiles{i},outDirs{i},params.opts,params.overWrite);
end

function runOnVideo(videoPath,videoFile,outDir,opts,overWrite)

if(exist(outDir,'dir')),
  if(~overWrite)
    fprintf('Cannot overwrite exisiting results in: %s\n',outDir);
    return;
  else
    fprintf('\n\n---- Overwriting results in %s----------\n\n\n',outDir);
  end
else
  mkdir(outDir);
end

videoName=miscFns.extractVideoNames({videoPath},{videoFile});
videoName=videoName{1};
vH=myVideoReader(videoPath,videoFile);
nFrames=vH.nFrames;

[annots,K]=h3dAnno.loadAnnotations(videoPath,videoFile,videoName,opts.h3dAnno_dir);
h3dOpts_conversion=humanCut2.makeOpts_h3dConversion(opts.optsString_h3dconversion);

hModel_prev=humanCut2.humanPolygon();
hModel_prev.initializeFromH3D(annots(1),K,h3dOpts_conversion);

flowDir=[opts.broxFlow_dir videoName '/'];

for i=1:nFrames-1
  tmpLoad=load([flowDir sprintf('flow_%05d.mat',i)]);
  flow=tmpLoad.flow;
  hModel_cur=humanCut2.humanPolygon();
  hModel_cur.initializeFromH3D(annots(i+1),K,h3dOpts_conversion);
  flow=stickmanFlow.addStickManFlow(flow,hModel_prev,hModel_cur);
  save(sprintf([outDir 'flow_%05d.mat'],i),'flow');
  if(opts.visualize)
    flowImg=broxFlow.flowImg_withWheel(flow,opts.visOpts.maxFlow_saturate);
    imwrite(flowImg,[outDir sprintf('visFlow_%05d.%s',i,opts.visOpts.imgExt)]);
  end
  delete(hModel_prev);
  hModel_prev=hModel_cur;
end

delete(hModel_prev);

delete(vH);

function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);
