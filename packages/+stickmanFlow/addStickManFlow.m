function flow=addStickManFlow(flow,hModel1,hModel2)
% Adds the flow between hModel1 and hModel2 to flow

partIds=[1:hModel1.numParts];
detected=logical([hModel1.allParts.detected])&logical([hModel2.allParts.detected]);
layerIds=[hModel1.allParts.layerId];

partIds=partIds(detected);
layerIds=layerIds(detected);

[layerIds,sortIdx]=sort(layerIds,2,'descend');
partIds=partIds(sortIdx);

[h,w,nCh]=size(flow);

for i=partIds
  iPart1=hModel1.allParts(i);
  iPart2=hModel2.allParts(i);
  mask=roipoly(h,w,iPart1.polygon(:,1),iPart1.polygon(:,2));
  flow=addPartFlow(flow,iPart1,iPart2,mask);
end

function flow=addPartFlow(flow,part1,part2,mask)

axis1=part1.medialAxis;
axis2=part2.medialAxis;
A=[axis1(1) -axis1(2) 1 0;
   axis1(2) axis1(1)  0 1;
   axis1(3) -axis1(4) 1 0;
   axis1(4) axis1(3)  0 1];
b=[axis2(1) axis2(2) axis2(3) axis2(4)]';

motion=A\b;

sR=[motion(1) -motion(2); % The rotation and scaling matrix
    motion(2) motion(1)];
uv=[motion(3); % the translation component
    motion(4)];

sR=sR-eye(2); % To make it into a flow instead of transformation

[h,w]=size(mask);
idxValid=find(mask);
[yValid xValid]=ind2sub([h w],idxValid);

coords=[xValid';yValid'];
flows=sR*coords+repmat(uv,[1 length(idxValid)]);
flows=[flows(1,:) flows(2,:)];
mask=repmat(mask,[1 1 2]);
flow(mask)=flows;
