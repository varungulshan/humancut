classdef fgHistory < handle
% Class to maintain and propagate foreground representation
  properties (SetAccess=private, GetAccess=public)
    fgImg % h x w x D img
    mask_known % h x w, logical
    debugLevel
    frameNum
  end

  methods
    function obj=fgHistory(debugLevel,initImg,mask_known)
      obj.debugLevel=debugLevel;
      obj.fgImg=initImg;
      obj.mask_known=mask_known;
      obj.frameNum=1;
      [h w nCh]=size(initImg);
      obj.fgImg=layerCut.helpers.fillBlue(obj.fgImg,~mask_known);
    end
    warpFG=propagate(obj,nxtFrame)
    update(obj,nxtSeg,nxtFrame,computedWarp);
  end

  methods (Access=private)
  end

end % of classdef
