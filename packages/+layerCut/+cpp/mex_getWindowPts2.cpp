// Samples windows on the boundary of the object
// different sampling strategy than mex_getWindowsPts, it takes one window, masks out all the points in the window and then follows the contour until it finds a point which is not masked
// The guarentee is that each boundary pixel is covered by atleast one window

#include "mex.h"
#include <float.h>
#include <memory.h>
#include <cmath>
#include "matrix.h"
#include <iostream>
#include <vector>

typedef unsigned int uint;

void fillBox(bool *mask,int curX,int curY,int winInterval,int w,int h);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters
     mask         [ h x w] (logical)
     winInterval   1 x 1 (int32)

     Output: bdryMask    [h x w] logical windows_idx [N x 1] int32 of index of window centers
  */

  if (nrhs != 2)
    mexErrMsgTxt("2 input arguments expected.");
  if (nlhs != 2)
    mexErrMsgTxt("2 output arguments expected.");

  if(mxGetClassID(prhs[0])!=mxLOGICAL_CLASS) mexErrMsgTxt("prhs[0] (mask) shd be of type logical\n");
  if(mxGetClassID(prhs[1])!=mxINT32_CLASS) mexErrMsgTxt("prhs[1] (winInterval) shd be of type int32\n");

  if(mxGetN(prhs[1])!=1 || mxGetM(prhs[1])!=1)
      mexErrMsgTxt("Invalid size for winInterval\n");

  int h=mxGetM(prhs[0]);
  int w=mxGetN(prhs[0]);
  int N=h*w;

  int winInterval=*(int*)mxGetData(prhs[1]);
  bool *mask=(bool*)mxGetData(prhs[0]);

  int dims[2];dims[0]=h;dims[1]=w;
  plhs[0]=mxCreateNumericArray(2,dims,mxLOGICAL_CLASS,mxREAL);
  bool *bdryMask=(bool*)mxGetData(plhs[0]);
  memset(bdryMask,0,N*sizeof(bool));

  // Find bdry mask -- vertical scan
  for(int x=0;x<w;x++){
    uint xOffset=x*h;
    bool prevMask=mask[xOffset];
    for(int y=1;y<h;y++){
      uint curPixel=xOffset+y;
      bool curMask=mask[curPixel];
      if(curMask!=prevMask){
        uint prevPixel=curPixel-1;
        uint bdryPixel;
        bdryPixel=curMask?curPixel:prevPixel;
        bdryMask[bdryPixel]=true;
      }
      prevMask=curMask;
    }
  }

  // Find bdry mask -- horizontal scan
  for(int y=0;y<h;y++){
    bool prevMask=mask[y];
    for(int x=1;x<w;x++){
      uint curPixel=x*h+y;
      bool curMask=mask[curPixel];
      if(curMask!=prevMask){
        uint prevPixel=curPixel-h;
        uint bdryPixel;
        bdryPixel=curMask?curPixel:prevPixel;
        bdryMask[bdryPixel]=true;
      }
      prevMask=curMask;
    }
  }

  // bdryMask created -- now scan to find boundary points
  std::vector<int> winCenters; // 1 indexed
  bool *visited=(bool*)mxMalloc(N*sizeof(bool));
  memset(visited,0,N*sizeof(bool));
  bool *covered=(bool*)mxMalloc(N*sizeof(bool));
  memset(covered,0,N*sizeof(bool));

  int nbrsY[8]={-1, 0,+1,-1,+1,-1, 0,+1};
  int nbrsX[8]={-1,-1,-1, 0, 0,+1,+1,+1};

  bool *it_visited=visited;
  bool *it_bdryMask=bdryMask;
  for(int i=0;i<N;i++,it_visited++,it_bdryMask++){
    if(*it_bdryMask && !(*it_visited)){
      bool more=true;
      int curX=i/h;
      int curY=i%h;
      while(more){
        int curIdx=curX*h+curY;
        visited[curIdx]=true;
        if(!covered[curIdx]){
          winCenters.push_back(curIdx+1);
          fillBox(covered,curX,curY,winInterval,w,h);
        }
        more=false;
        for(int k=0;k<8;k++){
          int nxtX=curX+nbrsX[k];
          int nxtY=curY+nbrsY[k];
          if(nxtX>=0 && nxtX < w && nxtY>=0 && nxtY < h){
            int nxtIdx=nxtX*h+nxtY;
            if(bdryMask[nxtIdx] && !visited[nxtIdx]){
              more=true;
              curX=nxtX;curY=nxtY;k=8;
            }
          }
        }
      }
    }
  }

  int numWindows=winCenters.size();
  int dims_rhs2[2];
  dims_rhs2[0]=numWindows;dims_rhs2[1]=1;
  plhs[1]=mxCreateNumericArray(2,dims_rhs2,mxINT32_CLASS,mxREAL);

  int *it_winCenters=(int*)mxGetData(plhs[1]);
  for(int i=0;i<numWindows;i++,it_winCenters++){
    *it_winCenters=winCenters[i];
  }
  // --- To free ---
  mxFree(visited);
  mxFree(covered);

}

void fillBox(bool *mask,int curX,int curY,int winInterval,int w,int h){
  int minX=std::max(0,curX-winInterval);
  int maxX=std::min(w-1,curX+winInterval);
  int minY=std::max(0,curY-winInterval);
  int maxY=std::min(h-1,curY+winInterval);

  for(int x=minX;x<=maxX;x++){
    for(int y=minY;y<=maxY;y++){
      mask[x*h+y]=true;
    }
  }
}
