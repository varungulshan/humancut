function update(obj,nxtSeg,nxtFrame,computedWarp)

obj.mask_bg=(nxtSeg==0);
obj.mask_unknown=(computedWarp.mask_unknown & ~obj.mask_bg);

mask_retain=~obj.mask_bg;
obj.bgImg=nxtFrame;
[h w nCh]=size(nxtFrame);
mask_retain=repmat(mask_retain,[1 1 nCh]);
obj.bgImg(mask_retain)=computedWarp.img(mask_retain);
