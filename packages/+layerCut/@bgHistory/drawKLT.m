function img=drawKLT(obj,nFrames,curFrame)
% Draws KLT points in current frame
% Green will denote features that get tracked to next frame
% Red denotes that get lost in the next frame

% For the last frame, only shows features that were tracked from the previous frame (all in red)

drawRad=1;
frameNum=obj.frameNum;
img=im2double(curFrame);
if(frameNum>=nFrames),

  klt_ppFile=[obj.klt_ppDir sprintf('%05d.mat',nFrames-1)];
  tmpLoad=load(klt_ppFile);

  pts_init=tmpLoad.pts_init;
  pts_final=tmpLoad.pts_final;
  mask_tracked=tmpLoad.mask_tracked;

  pts_final=pts_final(:,mask_tracked);
  tmpMask_tracked=true([1 size(pts_final,2)]);
  tmpMask_bg=true([1 size(pts_final,2)]);

  img=drawFeatures(img,pts_final,tmpMask_tracked,tmpMask_bg,drawRad); 
else
  klt_ppFile=[obj.klt_ppDir sprintf('%05d.mat',obj.frameNum)];
  tmpLoad=load(klt_ppFile);

  pts_init=tmpLoad.pts_init;
  pts_final=tmpLoad.pts_final;
  mask_tracked=tmpLoad.mask_tracked;

  % Select the tracked points within the mask
  mask_bgpoints=findBGpts(obj.mask_bg,pts_init,obj.rad_erode);

  img=drawFeatures(img,pts_init,mask_tracked,mask_bgpoints,drawRad); 
end

function mask_valid=findBGpts(mask_bg,pts,rad_erode)

stEl=strel('disk',rad_erode);
mask_bg=imerode(mask_bg,stEl);

[h,w]=size(mask_bg);
pts=round(pts); % To take care of fractional coordinates
ptsIdx=sub2ind([h w],pts(2,:),pts(1,:));

mask_valid=mask_bg(ptsIdx);
mask_valid=mask_valid(:)';

function img=drawFeatures(img,pts,mask_tracked,mask_valid,drawRad)

pts=pts(:,mask_valid);
mask_tracked=mask_tracked(mask_valid);

clr_tracked=[0 1 0];
clr_lost=[1 0 0];

for i=1:size(pts,2)
  if(mask_tracked(i)),clr=clr_tracked;
  else clr=clr_lost;
  end
  img=drawCross(img,pts(1,i),pts(2,i),clr,drawRad);
end

function img=drawCross(img,x,y,color,rad)
  x=round(x);y=round(y);
  [h,w,nCh]=size(img);
  xTop=max(1,x-3);
  yTop=max(1,y-rad);
  xBot=min(w,x+3);
  yBot=min(h,y+rad);

  for i=1:3
    img(yTop:yBot,xTop:xBot,i)=color(i);
  end

  xTop=max(1,x-rad);
  yTop=max(1,y-3);
  xBot=min(w,x+rad);
  yBot=min(h,y+3);

  for i=1:3
    img(yTop:yBot,xTop:xBot,i)=color(i);
  end
