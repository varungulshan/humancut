function warpBG=propagate(obj,nxtFrame)

klt_ppFile=[obj.klt_ppDir sprintf('%05d.mat',obj.frameNum)];
tmpLoad=load(klt_ppFile);

pts_init=tmpLoad.pts_init;
pts_final=tmpLoad.pts_final;
mask_tracked=tmpLoad.mask_tracked;

% Select the tracked points within the mask
mask_bgpoints=findBGpts(obj.mask_bg,pts_init,obj.rad_erode);

mask_valid=mask_tracked & mask_bgpoints;

H=affineReg_ransac(pts_init(:,mask_valid),pts_final(:,mask_valid));
warpBG.img=affWarp(obj.bgImg,H);
warpBG.mask_unknown=affWarp(obj.mask_unknown,H);
warpBG.H=H;
obj.frameNum=obj.frameNum+1;

function img=affWarp(img,H)
tForm=maketform('affine',H');
[h w nCh]=size(img);
[img,xx,xx]=imtransform(img,tForm,'xdata',[1 w],'ydata',[1 h]);

function mask_valid=findBGpts(mask_bg,pts,rad_erode)

stEl=strel('disk',rad_erode);
mask_bg=imerode(mask_bg,stEl);

[h,w]=size(mask_bg);
pts=round(pts); % To take care of fractional coordinates
ptsIdx=sub2ind([h w],pts(2,:),pts(1,:));

mask_valid=mask_bg(ptsIdx);
mask_valid=mask_valid(:)';

function H=affineReg_ransac(ptsL,ptsR)
% H(ptsL)=ptsR
% No ransac implemented yet

[xx N]=size(ptsL);

% Form the A matrix
A=[ptsL' ones(N,1)];

b1=ptsR(1,:)';
b2=ptsR(2,:)';

if(1)
  % L2 norm minimization of Ax-b1
  pseudoInv=inv(A'*A)*A';
  x1=pseudoInv*b1;
  x2=pseudoInv*b2;
else
  % L1 norm minimization of Ax-b1, A is NxM, X is Mx1, B is Nx1
  [N M]=size(A);
  f=[zeros(M,1);ones(N,1)];
  bigA=[A -eye(N,N); -A -eye(N,N)];
  bigX=linprog(f,bigA,[b1;-b1]);
  x1=bigX(1:M);
  bigX=linprog(f,bigA,[b2;-b2]);
  x2=bigX(1:M);
end
H=[x1';x2'];
