classdef bgHistory < handle
% Class to maintain and propagate background representation
% Uses pre-processed KLT tracks
  properties (SetAccess=private, GetAccess=public)
    bgImg % h x w x D img
    mask_unknown % h x w, logical
    mask_bg % h x w, logical, the current bg mask
    debugLevel
    frameNum
    klt_ppDir % dir with filenames such as 00001.mat, which stores tracks
    rad_erode % mask_bg is eroded by this rad for feature tracking
  end

  methods
    %function obj=layerCut(debugLevel,flowDir,gtFile,vH,gcOpts)
    function obj=bgHistory(debugLevel,initImg,mask_unknown,klt_ppDir)
      obj.debugLevel=debugLevel;
      obj.bgImg=initImg;
      obj.mask_unknown=mask_unknown;
      obj.mask_bg=~mask_unknown;
      obj.klt_ppDir=klt_ppDir;
      obj.frameNum=1;
      obj.rad_erode=3;
      [h w nCh]=size(initImg);
      %obj.bgImg(repmat(mask_unknown,[1 1 nCh]))=0;
      obj.bgImg=layerCut.helpers.fillBlue(obj.bgImg,mask_unknown);
    end
    warpBG=propagate(obj,nxtFrame)
    update(obj,nxtSeg,nxtFrame,computedWarp);
    img=drawKLT(obj,nFrames,curFrame) % debugging fn to see KLT tracks
  end

  methods (Access=private)
    initKLT(obj)
  end

end % of classdef
