function runSeg_offline()

import layerCut.*

visualize=false; % Set to true if you want to output debugging images
views=[1 2 3 4 5 10];
%views=[10];

cwd=miscFns.extractDirPath(mfilename('fullpath'));

inVideoPath=[cwd '../../data/myVideos/'];
inVideoFile='buffy_s5e10_1.avi';
gtFile=[cwd '../../data/gtFirstFrame/buffy_s5e10_1.png'];
kltDir=[cwd '../../pp/kltTracks_vgg/buffy_s5e10_1/'];
debugDir=[cwd '../../results/layerCut_visualize/buffy_s5e10_1/'];

%inVideoPath=[cwd '../../data/myVideos/love_actually_1_cut1/'];
%inVideoFile='00001.png';
%gtFile=[cwd '../../data/gtFirstFrame/love_actually_1_cut1.png'];
%kltDir=[cwd '../../pp/kltTracks_vgg/love_actually_1_cut1/'];
%debugDir=[cwd '../../results/layerCut_visualize/love_actually_1_cut1/'];

%inVideoPath=[cwd '../../data/snapCut_videos/'];
%inVideoFile='surfer.avi';
%gtFile=[cwd '../../data/gtFirstFrame/surfer.png'];
%kltDir=[cwd '../../pp/kltTracks_vgg/surfer/'];
%debugDir=[cwd '../../results/layerCut_visualize/surfer/'];

%inVideoPath=[cwd '../../data/snapCut_videos/footballer_cut2/'];
%inVideoFile='00001.png';
%gtFile=[cwd '../../data/gtFirstFrame/footballer_cut2.png'];
%kltDir=[cwd '../../pp/kltTracks_vgg/footballer_cut2/'];
%debugDir=[cwd '../../results/layerCut_visualize/footballer_cut2/'];

maxFrames=10;
gcOpts.gamma=20;
gcOpts.gcScale=1000;
gcOpts.gamma_ising=0.1;
gcOpts.nbrHoodType='nbr4';
gcOpts.sigma_c='auto';


vH=myVideoReader(inVideoPath,inVideoFile);
segH=segEngine(1,gtFile,vH,gcOpts,kltDir);
segH.preProcess();
segH.start([],1); % empty labels being passed, as they dont get used

if(visualize)
  if(exist(debugDir,'dir')),
    msg=sprintf('Overwrite exisiting %s visualization? [y/n] ',debugDir);
    inp=input(msg,'s');
    if(~strcmp(inp,'y')),
      delete(segH);
      delete(vH);
      return;
    end
  else
    mkdir(debugDir);
  end

  optsFile=[debugDir 'opts.txt'];
  fH=fopen(optsFile,'w');
  segH.printOptions(fH);
  fclose(fH);

  for j=1:length(views)
    cmd=sprintf('segH.view_%d()',views(j));
    view=eval(cmd);
    outFile=sprintf('view%d_%03d.png',views(j),segH.frameNum);
    imwrite(view,[debugDir outFile]);
  end
else
  fprintf('Visualization is turned off\n');
end

for i=1:min(maxFrames-1,(vH.nFrames-1))
  moveOk=segH.moveForward();
  if(~moveOk)
    error('Trouble in moving forward from frame %d to next\n',i);
  end
  if(visualize)
    for j=1:length(views)
      cmd=sprintf('segH.view_%d()',views(j));
      view=eval(cmd);
      outFile=sprintf('view%d_%03d.png',views(j),segH.frameNum);
      imwrite(view,[debugDir outFile]);
    end
  end
end

delete(segH);
delete(vH);
