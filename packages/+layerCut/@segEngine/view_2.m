function img=view_2(obj)
% shows the local windows, which have been propagated from the previous frame
% in the current code, there isnt any propagation, windows just get copied over

import layerCut.*

if(obj.debugLevel>0)
  if(obj.frameNum==1),
    img=zeros([obj.h obj.w 3]);
    img=miscFns.rendertext(img,'Frame_1_has_no_windows_propagated_to_it',[1 1 1],'ovr'); 
  else
    curFrame=im2double(obj.vH.curFrame);
    img=drawBoxes(curFrame,obj.localWindows_idx,obj.winRad);
  end
else
  img=zeros([obj.h obj.w 3]);
  img=miscFns.rendertext(img,'No view 2 at debugLevel 0',[1 1 1],'ovr'); 
end

function img=drawBoxes(img,windows_idx,winRad)

winClr=[1 1 0]; % yellow
penRad=1;

[h w nCh]=size(img);

for i=1:length(windows_idx)
  [winY,winX]=ind2sub([h w],windows_idx(i));
  xMin=max(1,winX-winRad);
  xMax=min(w,winX+winRad);
  yMin=max(1,winY-winRad);
  yMax=min(h,winY+winRad);
  img=drawBox(img,winClr,penRad,xMin,xMax,yMin,yMax);
end

function img=drawBox(img,winClr,penRad,xMin,xMax,yMin,yMax)

[h w nCh]=size(img);

% Clockwise: left bar, top bar, right bar, bottom bar
xL=[xMin-penRad xMin-penRad xMax-penRad xMin-penRad];
xR=[xMin+penRad xMax+penRad xMax+penRad xMax+penRad];
yT=[yMin-penRad yMin-penRad yMin-penRad yMax-penRad];
yB=[yMax+penRad yMin+penRad yMax+penRad yMax+penRad];

xL=max(1,xL);
xR=min(w,xR);
yT=max(1,yT);
yB=min(h,yB);

for j=1:4
  x_left=xL(j);
  x_right=xR(j);
  y_top=yT(j);
  y_bottom=yB(j);
  
  for i=1:3
    img(y_top:y_bottom,x_left:x_right,i)=winClr(i);
  end
end
