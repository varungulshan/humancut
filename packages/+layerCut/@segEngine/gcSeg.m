function seg=gcSeg(obj,lHoods,mask_l,img)

import layerCut.cpp.*

gcOpts=obj.gcOpts;
beta=obj.beta;
neglHood_fg=-log(lHoods.fg);
neglHood_bg=-log(lHoods.bg);

myUB=realmax/(gcOpts.gcScale*100);

neglHood_fg(neglHood_fg>myUB)=myUB;
neglHood_bg(neglHood_bg>myUB)=myUB;
[seg,flow]=mex_gcBand(mask_l,neglHood_fg,neglHood_bg,gcOpts.gamma,beta,...
                      obj.roffset',obj.coffset',gcOpts.gcScale,gcOpts.gamma_ising,...
                      img);

%seg=zeros(size(seg),'uint8');
%seg(tmpSeg)=255;
