function printOptions(obj,fH)

if(~exist('fH','var')),
  fH=1;
end;

% -- Make a fictitous structure for printing the options
opts.winRad=obj.winRad;
opts.gcOpts=obj.gcOpts;
opts.kltDir=obj.kltDir;
opts.beta=obj.beta;
opts.gmmOpts=obj.gmmOpts;

fprintf(fH,'Printing options for layerCut:\n');
miscFns.printStructure(opts,fH);
