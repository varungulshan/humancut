function img=view_4(obj)
% Shows local windows from which color models are learnt

import layerCut.*

if(obj.debugLevel>0)
  if(obj.frameNum==1),
    img=zeros([obj.h obj.w 3]);
    msg=sprintf('Frame_1_does_not_learn_color_models',obj.frameNum);
    img=miscFns.rendertext(img,msg,[1 1 1],'ovr'); 
  else
    obj.vH.moveBackward();
    prevFrame=im2double(obj.vH.curFrame);
    prevSeg=obj.seg(:,:,obj.frameNum-1);
    obj.vH.moveForward();
    [segBoundaryMask,segBoundaryColors]= ...
    miscFns.getSegBoundary_twoColors(prevSeg,[0 1 0],[1 0 0],1,1);
    prevFrame(segBoundaryMask)=segBoundaryColors;
    img=drawBoxes(prevFrame,obj.localWindows_idx,obj.winRad);
  end
else
  img=zeros([obj.h obj.w 3]);
  img=miscFns.rendertext(img,'No view 4 at debugLevel 0',[1 1 1],'ovr'); 
end

function img=drawBoxes(img,windows_idx,winRad)

winClr=[1 1 0]; % yellow
penRad=1;

[h w nCh]=size(img);

for i=1:length(windows_idx)
  [winY,winX]=ind2sub([h w],windows_idx(i));
  xMin=max(1,winX-winRad);
  xMax=min(w,winX+winRad);
  yMin=max(1,winY-winRad);
  yMax=min(h,winY+winRad);
  img=drawBox(img,winClr,penRad,xMin,xMax,yMin,yMax);
end

function img=drawBox(img,winClr,penRad,xMin,xMax,yMin,yMax)

[h w nCh]=size(img);

% Clockwise: left bar, top bar, right bar, bottom bar
xL=[xMin-penRad xMin-penRad xMax-penRad xMin-penRad];
xR=[xMin+penRad xMax+penRad xMax+penRad xMax+penRad];
yT=[yMin-penRad yMin-penRad yMin-penRad yMax-penRad];
yB=[yMax+penRad yMin+penRad yMax+penRad yMax+penRad];

xL=max(1,xL);
xR=min(w,xR);
yT=max(1,yT);
yB=min(h,yB);

for j=1:4
  x_left=xL(j);
  x_right=xR(j);
  y_top=yT(j);
  y_bottom=yB(j);
  
  for i=1:3
    img(y_top:y_bottom,x_left:x_right,i)=winClr(i);
  end
end
