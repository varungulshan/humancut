function pp_kltTracks()
% Code to store preprocessed KLT tracks for video
% uses vgg_klt code for preprocessing

% filename of video to process

cwd=miscFns.extractDirPath(mfilename('fullpath'));

%videoFilePath='../../../data/myVideos/love_actually_1_cut1/'; % relative to this m file
%videoFileName='00001.png';
%outDir=['../../../pp/kltTracks_vgg/love_actually_1_cut1/']; % relative to this m file

%videoFilePath='../../../data/snapCut_videos/footballer_cut2/'; % relative to this m file
%videoFileName='00001.png';
%outDir=['../../../pp/kltTracks_vgg/footballer_cut2/']; % relative to this m file

videoFilePath='../../../data/snapCut_videos/'; % relative to this m file
videoFileName='surfer.avi';
outDir=['../../../pp/kltTracks_vgg/surfer/']; % relative to this m file

%videoFilePath='../../../data/myVideos/'; % relative to this m file
%videoFileName='buffy_s5e03_2.avi';
%outDir=['../../../pp/kltTracks_vgg/buffy_s5e03_2/']; % relative to this m file

% Basic options
nFeatures=1000; % number of features per frame
pyramid_levels=2;

% initializations
outDir=[cwd outDir];
if(~exist(outDir,'dir')),
  mkdir(outDir);
end
videoFilePath=[cwd videoFilePath];
vH=myVideoReader(videoFilePath,videoFileName);
nFrames=vH.nFrames;

f1 = 1;   
f2 = nFrames;

tc = vgg_klt_init('nfeats',nFeatures,'mindisp',0.5,'pyramid_levels',pyramid_levels);
if(exist([outDir 'kltOpts.mat'],'file')),
  msg=sprintf('Overwrite existing klt tracks in %s? (y/n)[n] ',outDir);
  inp=input(msg,'s');
  if(~strcmp(inp,'y')),
    return;
  end
end

save([outDir 'kltOpts.mat'],'tc');
fOpts=fopen([outDir 'kltOpts.txt'],'w');
miscFns.printStructure(tc,fOpts);
fclose(fOpts);

K = zeros(3,tc.nfeats,f2-f1+1);

I = vH.curFrame;
I = im2double(rgb2gray(I));
If1 = I;
M = [];
stTime=clock;
[tc,P] = vgg_klt_selfeats(tc,I,M);
fprintf('Time taken to select %d features= %.3f seconds\n',tc.nfeats,etime(clock,stTime));

n=sum(P(3,:)>=0);
vgg_printf('%d features found in first frame\n', n);

% tracking frames (f1+1):f2
for f = (f1+1):f2    

    vH.moveForward();
    I = vH.curFrame;
    I_color=im2double(I);
    I = im2double(rgb2gray(I));

    pts_init=P(1:2,:);

    stTime=clock;
    [tc,P] = vgg_klt_track(tc,P,I,M);
    fprintf('Tracking features = %.3f seconds\n',etime(clock,stTime));
    pts_final=P(1:2,:);

    mask_tracked=(P(3,:)>=0);
    nt = nnz(mask_tracked);
    vgg_printf('%d tracked\n', n);
    save([outDir sprintf('%05d.mat',f-1)],'pts_init','pts_final','mask_tracked');
    
    stTime=clock;
    [tc,P] = vgg_klt_selfeats(tc,I,M,P);    
    fprintf('Replacing features = %.3f seconds\n',etime(clock,stTime));
    
    nn = sum(P(3,:)>=0);
    vgg_printf('now %d features (%d replaced)\n', nn, nn-nt);
end
