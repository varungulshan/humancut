function grabCut_track(params)
% Function to run vito's pose estimation on tracks

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('params','var'))
  params.tracksDir='/data/adam2/varun/humanCut/manuelTracks/run002/';
  params.rootOut_dir=[cwd '../../results/vitoPose/'];
  params.overWrite=true;
  params.tmpDir=[cwd 'tmp/'];
  params.opts.whichModel='Buffy2to6andPascal';
  params.opts.poseCodeDir='/home/varun/windows/rugbyShared/work/software/pose_estimation_code_release_v1.05/code/';
  
  params.testBench_cmd='testBench.getTests_20frames_humans()';
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',params.rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
end

[videoPaths,videoFiles]=eval(params.testBench_cmd);
videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);

addpath(params.opts.poseCodeDir);
addpath([params.opts.poseCodeDir 'utils/']);
poseCodeParams=load([params.opts.poseCodeDir 'env.mat']);
for i=1:length(videoNames)
  fprintf('Running grabCut on video: %s\n',videoNames{i});
  runOnVideo(videoPaths{i},videoFiles{i},videoNames{i},params.tracksDir,...
            params.rootOut_dir,params.overWrite,params.opts,params.tmpDir,...
            poseCodeParams);
end
rmpath(params.opts.poseCodeDir);
rmpath([params.opts.poseCodeDir 'utils/']);

function runOnVideo(videoPath,videoFile,videoName,tracksDir,rootOut_dir,overWrite,opts,...
                    tmpDir,poseCodeParams)

outDir=[rootOut_dir videoName '/'];
if(~exist(outDir,'dir')),
  mkdir(outDir);
else
  if(~overWrite)
    fprintf('Output dir: %s already exists, skipping\n',outDir);
    return;
  else
    fprintf('--- Overwriting %s -----\n',outDir);
  end
end

tracksDir=[tracksDir videoName '/'];
trackFile=[tracksDir 'tracks.txt'];

fH=fopen([outDir 'opts.txt'],'w');
miscFns.printStructure(opts,fH);
fclose(fH);

tracks=humanCut.parseTrackFile(trackFile); % returns structure array of size N x 1 where 
% N=number of tracks, field of structure array are:
% trackSt -> frame number where track starts
% trackEnd -> frame number where track ends
% detections -> K x 4 array of bounding boxes, where K=(trackEnd-trackSt+1)

fghigh_params=poseCodeParams.fghigh_params;
parseparams=getfield(poseCodeParams,['parse_params_' opts.whichModel]);
addInf_params=getfield(poseCodeParams,['addinf_params_' opts.whichModel]);
pm2segms_params=poseCodeParams.pm2segms_params;

for i=1:length(tracks)
  segOutDir=[outDir sprintf('track%03d_files/',i)];
  if(~exist(segOutDir,'dir')),mkdir(segOutDir); end;
  vH=myVideoReader(videoPath,videoFile);
  h=vH.h;w=vH.w;
  nFrames=vH.nFrames;
  seg=128*ones([h w nFrames],'uint8');
  trackSt=tracks(i).trackSt;
  trackEnd=tracks(i).trackEnd;
  detections=tracks(i).boxes;
  for j=1:(trackSt-1)
    vH.moveForward();
  end

  for j=trackSt:trackEnd
    img=im2double(vH.curFrame);
    imgPath=[tmpDir '0.jpg'];
    imwrite(img,imgPath);
    
    bbox=detections(j-trackSt+1,:)';

    [xx xx]=PoseEstimStillImage(tmpDir,'','%d.jpg',0,'ubf',bbox,fghigh_params,...
            parseparams,addInf_params,pm2segms_params,true);

    srcFile=[tmpDir 'segms_add/0.jpg'];
    destFile=[segOutDir sprintf('stickman_%03d.jpg',j)];
    copyfile(srcFile,destFile);

    rmdir([tmpDir 'fghigh'],'s');
    rmdir([tmpDir 'poses'],'s');
    rmdir([tmpDir 'poses_add'],'s');
    rmdir([tmpDir 'segms_add'],'s');

    if(j~=trackEnd)
      vH.moveForward();
    end
  end
  delete(vH);
end

function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);
