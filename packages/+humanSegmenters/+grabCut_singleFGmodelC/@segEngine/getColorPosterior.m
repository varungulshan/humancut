function posterior=getColorPosterior(obj,iFrame)

unaryImg=obj.unaryImg_color{iFrame};
fgExp=exp(-unaryImg(:,:,1));
bgExp=exp(-unaryImg(:,:,2));

posterior=fgExp./(fgExp+bgExp);
