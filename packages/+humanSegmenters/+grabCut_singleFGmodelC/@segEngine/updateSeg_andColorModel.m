function updateSeg_andColorModel(obj,hModel)

% First update segmentation, as the human model has changed.
obj.updateSeg(hModel); 

% Also need to update the labelImg as the human model has changed
for i=1:obj.nFrames
  obj.labelImg(:,:,i)=hModel.getColorModel_initLabels('globalModels',...
                  [obj.h obj.w],i);
end

for i=1:obj.opts.numIters_newProxy
  obj.updateColorModel();
  obj.updateSeg(hModel);
end
