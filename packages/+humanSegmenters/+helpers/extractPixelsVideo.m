function features=extractPixelsVideo(fullVideo)
% Returns a nCh x (h*w*nFrames) array of features (of type double)
% fullVideo is of type uint8

assert(strcmp(class(fullVideo),'uint8'),'Full video should be of type uint8\n');

[h w nCh nFrames]=size(fullVideo);
features=zeros([nCh h*w*nFrames]);

for i=1:nFrames
  features(:,(1+(i-1)*h*w):(i*h*w))=humanSegmenters.helpers.extractPixels(...
                                    im2double(fullVideo(:,:,:,i)));
end
