function initColorModel_fromHumanModel(obj,hModel)

obj.labelImg=zeros([obj.h obj.w obj.nFrames],'uint8');
for i=1:obj.nFrames
  if(obj.constrained(i))
    labelImg=hModel.getColorModel_initLabels('globalModels',[obj.h obj.w],i);
    labelImg=modifyLabels(labelImg,obj.seg(:,:,i));
    obj.labelImg(:,:,i)=labelImg;
  else
    obj.labelImg(:,:,i)=hModel.getColorModel_initLabels('globalModels',[obj.h obj.w],i);
  end
end

[obj.gmmf,obj.gmmb]=humanSegmenters.grabCut_singleFGmodelC2.initColorModels...
                    (obj.features,obj.labelImg,...
                    obj.opts.gmmNmix_fg,obj.opts.gmmNmix_bg);

function labelImg=modifyLabels(labelImg,seg)
% Uses to the seg to set the unconstrained regions in labelImg
% to soft initializations

%mask=(labelImg==0);
mask=(labelImg~=5 & labelImg~=6);
labelImg(mask & seg==255)=3; % Soft fg
labelImg(mask & seg==0)=4; % Soft bg
