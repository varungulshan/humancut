function seg=gcSeg(obj,unaryImg,iFrame)

assert(~obj.constrained(iFrame),'Calling gcSeg for constrained frame not allowed\n');

import humanSegmenters.cpp.*

objOpts=obj.opts;

opts.gcScale=objOpts.gcScale;

if(isempty(obj.graphInfo{iFrame}))
  [seg,flow,graphInfo]=mex_dgc_init(unaryImg,obj.lEdges{iFrame},obj.rEdges{iFrame},... 
                       obj.edgeWeights{iFrame},opts);
  obj.graphInfo{iFrame}=graphInfo;
else
  [seg,flow]=mex_dgc_repeat(unaryImg,obj.graphInfo{iFrame},opts);
end
