function unaryImg=getColorUnary(obj,iFrame)

assert(~obj.constrained(iFrame),'Calling color unary for constrained frame not allowed\n');

opts=obj.opts;
nPixels=obj.h*obj.w;

features=obj.features(:,(1+(iFrame-1)*nPixels):(iFrame*nPixels));
fgLikeli=gmm.computeGmm_likelihood(features,obj.gmmf);
bgLikeli=gmm.computeGmm_likelihood(features,obj.gmmb(iFrame));

fgLikeli=opts.uniform_gamma*opts.uniform_value+(1-opts.uniform_gamma)*fgLikeli;
bgLikeli=opts.uniform_gamma*opts.uniform_value+(1-opts.uniform_gamma)*bgLikeli;

fgLikeli=-log(fgLikeli);
bgLikeli=-log(bgLikeli);
myUB=opts.unaryBound;
fgLikeli(fgLikeli>myUB)=myUB;
bgLikeli(bgLikeli>myUB)=myUB;

fgLikeli=reshape(fgLikeli,[obj.h obj.w]);
bgLikeli=reshape(bgLikeli,[obj.h obj.w]);

unaryImg=zeros([obj.h obj.w 2]);
unaryImg(:,:,1)=fgLikeli;
unaryImg(:,:,2)=bgLikeli;
