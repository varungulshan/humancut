function initializeFull(obj,fullVideo,hModel,idxC,segC)

[h w nCh nFrames]=size(fullVideo);
obj.h=h;obj.w=w;obj.nCh=nCh;obj.nFrames=nFrames;
obj.graphInfo=cell(nFrames,1);
obj.unaryImg_color=cell(nFrames,1);
obj.seg=zeros([h w nFrames],'uint8');

constrained=false(1,nFrames);
constrained(idxC)=true;
assert(numel(idxC)==numel(segC),'Inconsistent idxC and segC\n');
obj.constrained=constrained;

for i=1:length(segC)
  obj.seg(:,:,idxC(i))=segC{i};
end

obj.preProcess(fullVideo);
obj.initColorModel_fromHumanModel(hModel);
obj.updateSeg(hModel);


if(obj.debugOpts.debugLevel>0)
  for i=1:nFrames
    miscFns.saveDebug(obj.debugOpts,hModel.getShapePosterior([h w],i),...
            sprintf('%03d_initShapePosterior.png',i));
    %posterior_color=obj.getColorPosterior();
    %miscFns.saveDebug(obj.debugOpts,posterior_color,sprintf('iter%03d_colorUnaries.jpg',1));
  end
  obj.printMemoryEstimate();
end

for i=2:obj.opts.numIters
  obj.updateColorModel(); % updates it using current segmentation, and stored labelImg 
  % the stored labelImg is a function of hModel
  obj.updateSeg(hModel); % update segmentation using current color model
                         % and the passed human model
  %if(obj.debugOpts.debugLevel>0)
    %posterior_color=obj.getColorPosterior();
    %miscFns.saveDebug(obj.debugOpts,posterior_color,sprintf('iter%03d_colorUnaries.jpg',i));
  %end
end
