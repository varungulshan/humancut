function printMemoryEstimate(obj)

debugOpts=obj.debugOpts;

outFile=[debugOpts.debugDir debugOpts.debugPrefix 'memoryEstimate.txt'];
fH=fopen(outFile,'w');

varNames=cell(0,1);
varBytes=zeros(0,1);

varNames{end+1}='seg';
varBytes(end+1)=numel(obj.seg);

varNames{end+1}='features';
varBytes(end+1)=numel(obj.features)*8;

nPixels=obj.h*obj.w;
nEdges=length(obj.lEdges{1});
varNames{end+1}='graphCut graphs(guess)';
varBytes(end+1)=obj.nFrames*(nPixels*44+58*nEdges);

varNames{end+1}='labelImg';
varBytes(end+1)=numel(obj.labelImg);

varNames{end+1}='unaryImg_color';
varBytes(end+1)=numel(obj.unaryImg_color{1})*8*obj.nFrames;

varNames{end+1}='edges';
varBytes(end+1)=numel(obj.lEdges{1})*4*2*obj.nFrames; % *2 because lEdges and rEdges, 2 variables

varNames{end+1}='edgeWeights';
varBytes(end+1)=numel(obj.edgeWeights{1})*8*obj.nFrames; % *2 because lEdges and rEdges, 2 variables

for i=1:length(varNames)
  fprintf(fH,'Memory used by variable %s = %.2fMB\n',varNames{i},...
          varBytes(i)/2^20);
end
fprintf(fH,'\nTotal memory used (guess) = %.2fMB\n',sum(varBytes)/2^20);

fclose(fH);
