function initColorModel_fromHumanModel(obj,hModel)

obj.labelImg=zeros([obj.h obj.w obj.nFrames],'uint8');
for i=1:obj.nFrames
  obj.labelImg(:,:,i)=hModel.getColorModel_initLabels('globalModels',[obj.h obj.w],i);
end

[obj.gmmf,obj.gmmb]=humanSegmenters.grabCut_singleFGmodel.initColorModels...
                    (obj.features,obj.labelImg,...
                    obj.opts.gmmNmix_fg,obj.opts.gmmNmix_bg);
