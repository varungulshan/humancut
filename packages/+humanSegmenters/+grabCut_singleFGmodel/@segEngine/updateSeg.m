function updateSeg(obj,hModel)

opts=obj.opts;
h=obj.h;
w=obj.w;

for i=1:obj.nFrames
  unaryImg_color=obj.getColorUnary(i);
  shapePosterior=hModel.getShapePosterior([h w],i);
  
  unaryImg_shape=zeros([h w 2]);
  myUB=opts.unaryBound;
  tmp=-log(shapePosterior);
  tmp(tmp>myUB)=myUB;
  unaryImg_shape(:,:,1)=tmp;
  tmp=-log(1-shapePosterior);
  tmp(tmp>myUB)=myUB;
  unaryImg_shape(:,:,2)=tmp;
  
  obj.unaryImg_color{i}=unaryImg_color;
  unaryImg=opts.wtShape*unaryImg_shape+(1-opts.wtShape)*unaryImg_color;
  obj.seg(:,:,i)=obj.gcSeg(unaryImg,i);

end
