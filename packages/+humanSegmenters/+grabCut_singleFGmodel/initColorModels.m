function [gmmf,gmmb]=initColorModels(features,labelImg,gmmNmix_fg,gmmNmix_bg)
% gmmf is a 1x1 structure
% gmmb is a nFrames x 1 structure

[h w nFrames]=size(labelImg);

labelImg(labelImg==3)=1; % Soft labels are used for initializing color models also
labelImg(labelImg==4)=2;

tmpFeatures=features(:,labelImg(:)==1);
gmmf=gmm.init_gmmBS(tmpFeatures,gmmNmix_fg);

nPixels=h*w;
for i=1:nFrames
  tmpFeatures=features(:,(1+(i-1)*nPixels):(i*nPixels));
  tmpLabels=labelImg(:,:,i);
  tmpFeatures=tmpFeatures(:,tmpLabels(:)==2);
  gmmb(i)=gmm.init_gmmBS(tmpFeatures,gmmNmix_bg);
end
