#include "mex.h"
#include "graph.h"
#include <cmath>
#include <iostream>
#include <climits>
#include <vector>

using namespace std;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters combinations
     rhs[0] = unary terms [h x w x 2] (double) first channel for label=1 unaries
              second channel for label=2 unaries
     rhs[1] = graphInfo structure with following fields:
       dgcHandle -> handle to graph
     rhs[2] = opts structure with following fields:
      gcScale -> double scalar

     lhs[0] -> returns the seg labels of all nodes (array of type hxwxnFrames uint8)
     lhs[1] -> returns the flow

     Notes on memory consumption:
     This code allocated following sized arrays (only big ones noted here):
     let nPix=h*w*nFrames;
     let nEdges=number of edges in graph (roughly 5*nPix for 8 neighbourhood in space
     and 1 nbrhood in time)
     graph structure (kolmogorov) -> 58 * nEdges + 44 * nPix
     (In practice nPix will be less because this code removes pixels which are
     hard constrained!)
   */

  if (nrhs != 3)
    mexErrMsgTxt("3 inputs required");


  mxAssert(mxGetClassID(prhs[0])==mxDOUBLE_CLASS,
           "prhs[0] (unaries) should be of type double\n");
  mxAssert(mxIsStruct(prhs[1]),"prhs[1] (graphInfo) should be of type struct\n");
  mxAssert(mxIsStruct(prhs[2]),"prhs[2] (opts) should be of type struct\n");

  typedef Graph<int,int,int> GraphInt;
  int h=mxGetDimensions(prhs[0])[0];
  int w=mxGetDimensions(prhs[0])[1];
  int frameRes=h*w;
  double gcScale;

  // --- Check dimensions of prhs[0] -----
  mxAssert(mxGetNumberOfDimensions(prhs[0])==3,"unaries need to have 3 dimensions\n");
  mxAssert(mxGetDimensions(prhs[0])[2]==2,"3rd dimension needs to be 2 for unaries\n");

  // --- Check prhs[1] ------
  mxArray *tmp;
  GraphInt *g;
  int numBytes_handle=sizeof(GraphInt*);

  tmp=mxGetField(prhs[1],0,"dgcHandle");
  mxAssert(tmp!=NULL,"dgcHandle field not found\n");
  memcpy(&g,mxGetData(tmp),numBytes_handle);

  // --- Check prhs[2] and initialize from the options structure -----

  tmp=mxGetField(prhs[2],0,"gcScale");
  mxAssert(tmp!=NULL,"gcScale field not found\n");
  gcScale=*mxGetPr(tmp);

  // ---- Data verified ok, now set up all the pointers

  double *unaryImg=mxGetPr(prhs[0]);
  double *fgL=unaryImg;
  double *bgL=fgL+frameRes;

  int intMax=INT_INF;

  for(int i=0;i<frameRes;i++){
    double fgLikeli=fgL[i];
    double bgLikeli=bgL[i];
    double diff=gcScale*(fgLikeli-bgLikeli);
    double dbl_intMax=(double)(intMax);
    double dbl_intMin=(double)(-intMax);
    int fgUnary;
    if(diff>dbl_intMax){fgUnary=intMax;}
    else if(diff<dbl_intMin){fgUnary=-intMax;}
    else{fgUnary=(int)diff;}
    g->edit_tweights(i,0,fgUnary);
    g->mark_node(i);
  }

  int flow=g->maxflow(true);

  // --- Now prepare the output ------------
  mxAssert(nlhs>=2,"Atleast two outputs required\n");
  int dims[2];dims[0]=h;dims[1]=w;

  plhs[0]=mxCreateNumericArray(2,dims,mxUINT8_CLASS,mxREAL);
  plhs[1]=mxCreateDoubleScalar((double)flow);

  unsigned char *it_seg=(unsigned char*)mxGetData(plhs[0]);
  for(int i=0;i<frameRes;i++,it_seg++){
    *it_seg=(g->what_segment(i)==GraphInt::SOURCE?255:0); // 255 for source, 0 for sink
  }

  // ------- To free -------------
  //deleteGraph<int,int,int>(&g);
}
