#include "mex.h"
#include "graph.h"
#include <cmath>
#include <iostream>
#include <climits>
#include <vector>

using namespace std;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters combinations
     rhs[0] = graphInfo structure with following fields:
       dgcHandle -> handle to graph
       fgUnaryEdits_offset
       fgUnaryEdits_value
       bgUnaryEdits_offset
       bgUnaryEdits_value
   */

  if (nrhs != 1)
    mexErrMsgTxt("1 input required");

  if(!mxIsStruct(prhs[0]))
    mexErrMsgTxt("prhs[0] (graphInfo) should be a structure\n");

  typedef Graph<int,int,int> GraphInt;

  mxArray *tmp;
  GraphInt *g;
  int numBytes_handle=sizeof(GraphInt*);

  tmp=mxGetField(prhs[0],0,"dgcHandle");
  mxAssert(tmp!=NULL,"dgcHandle field not found\n");
  memcpy(&g,mxGetData(tmp),numBytes_handle);

  deleteGraph<int,int,int>(&g);
  g=NULL;
}
