#include "mex.h"
#include "graph.h"
#include <cmath>
#include <iostream>
#include <climits>
#include <vector>

using namespace std;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters combinations
     rhs[0] = mask [h x w x nFrames] (255 for fg, 0 for bg, 
              128 for where to run graph cut) [uint8]
     rhs[1] = unary terms [h x w x 2 x nFrames] (double) first channel for label=1 unaries
              second channel for label=2 unaries
     rhs[2] = img [h x w x nCh x nFrames] (uint8)
     rhs[3] = opts structure with following fields:
     gcGamma_e -> double scalar
     gcGamma_i -> double scalar
     gcGammaT_e -> double scalar
     gcGammaT_i -> double scalar
     gcScale -> double scalar
     beta -> double scalar
     beta_t -> double scalar
     xoffset -> [ L x 1 ] int32 offsets
     yoffset -> [ L x 1 ] int32 offsets
     zoffset -> [ L x 1 ] int32 offsets

     lhs[0] -> returns the seg labels of all nodes (array of type hxwxnFrames uint8)
     lhs[1] -> returns the flow
     lhs[2] -> returns a structure graphInfo (for future calls)

     Notes on memory consumption:
     This code allocated following sized arrays (only big ones noted here):
     let nPix=h*w*nFrames;
     let nEdges=number of edges in graph (roughly 5*nPix for 8 neighbourhood in space
     and 1 nbrhood in time)
     idxMap -> [h x w x nFrames] (int32)
     graph structure (kolmogorov) -> 58 * nEdges + 44 * nPix
     (In practice nPix will be less because this code removes pixels which are
     hard constrained!)
   */

  if (nrhs != 4)
    mexErrMsgTxt("4 inputs required");

  if(mxGetClassID(prhs[0])!=mxUINT8_CLASS)
    mexErrMsgTxt("prhs[0] (mask) should be of type uint8\n");
  if(mxGetClassID(prhs[1])!=mxDOUBLE_CLASS)
    mexErrMsgTxt("prhs[1] (unaryImg) should be of type double\n");
  if(mxGetClassID(prhs[2])!=mxUINT8_CLASS)
    mexErrMsgTxt("prhs[2] (img) should be of type uint8\n");
  if(!mxIsStruct(prhs[3]))
    mexErrMsgTxt("prhs[3] (opts) should be a structure\n");

  typedef Graph<int,int,int> GraphInt;
  int h=mxGetDimensions(prhs[0])[0];
  int w=mxGetDimensions(prhs[0])[1];
  int frameRes=h*w;
  int nFrames,nCh,nOffsets,nPix;
  double gcGamma_e,gcGamma_i,gcGammaT_e,gcGammaT_i;
  double gcScale,beta,beta_t;
  vector <int>xoffset;
  vector <int>yoffset;
  vector <int>zoffset;
  vector <bool>isTemporal;
  if(mxGetNumberOfDimensions(prhs[0])<3){nFrames=1;}
  else{nFrames=mxGetDimensions(prhs[0])[2];}
  nPix=frameRes*nFrames;

  // --- Check dimensions of prhs[1] -----
  mxAssert(mxGetNumberOfDimensions(prhs[1])>=3,
          "Incorrect number of dimensions for prhs[1]\n");

  if(mxGetDimensions(prhs[1])[0]!=h || mxGetDimensions(prhs[1])[1]!=w 
    || mxGetDimensions(prhs[1])[2]!=2) {
      mexErrMsgTxt("prhs[1] (unary img) incorrect dimensions\n");
  }
  if(nFrames>1){
    if(mxGetNumberOfDimensions(prhs[1])<4)
      mexErrMsgTxt("Incorrect number of dimensions for prhs[1]\n");
    if(nFrames!=mxGetDimensions(prhs[1])[3])
      mexErrMsgTxt("Incorrect nFrames for prhs[1]\n");
  }

  // --- Check dimensions of prhs[2] ------

  if(mxGetDimensions(prhs[2])[0]!=h || mxGetDimensions(prhs[2])[1]!=w) mexErrMsgTxt("prhs[2] (img) incorrect dimensions\n");
  if(nFrames>1){
    if(mxGetNumberOfDimensions(prhs[2])<4)
      mexErrMsgTxt("Incorrect number of dimensions for prhs[2]\n");
    if(nFrames!=mxGetDimensions(prhs[2])[3])
      mexErrMsgTxt("Incorrect nFrames for prhs[2]\n");
  }

  if(mxGetNumberOfDimensions(prhs[2])<3) nCh=1;
  else nCh=mxGetDimensions(prhs[2])[2];

  // --- Check and initialize from the options structure -----
  mxArray *tmp;

  tmp=mxGetField(prhs[3],0,"gcGamma_e");
  mxAssert(tmp!=NULL,"gcGamma_e field not found\n");
  gcGamma_e=*mxGetPr(tmp);

  tmp=mxGetField(prhs[3],0,"gcGamma_i");
  mxAssert(tmp!=NULL,"gcGamma_i field not found\n");
  gcGamma_i=*mxGetPr(tmp);

  tmp=mxGetField(prhs[3],0,"gcGammaT_e");
  mxAssert(tmp!=NULL,"gcGammaT_e field not found\n");
  gcGammaT_e=*mxGetPr(tmp);

  tmp=mxGetField(prhs[3],0,"gcGammaT_i");
  mxAssert(tmp!=NULL,"gcGammaT_i field not found\n");
  gcGammaT_i=*mxGetPr(tmp);

  tmp=mxGetField(prhs[3],0,"gcScale");
  mxAssert(tmp!=NULL,"gcScale field not found\n");
  gcScale=*mxGetPr(tmp);

  tmp=mxGetField(prhs[3],0,"beta");
  mxAssert(tmp!=NULL,"beta field not found\n");
  beta=*mxGetPr(tmp);

  tmp=mxGetField(prhs[3],0,"beta_t");
  mxAssert(tmp!=NULL,"beta_t field not found\n");
  beta_t=*mxGetPr(tmp);

  tmp=mxGetField(prhs[3],0,"xoffset");
  mxAssert(tmp!=NULL,"xoffset field not found\n");
  mxAssert(mxGetClassID(tmp)==mxINT32_CLASS,"xoffset not of type int32\n");
  nOffsets=mxGetNumberOfElements(tmp); 
  int *ptrTmp=(int*)mxGetData(tmp);
  xoffset=vector<int>(ptrTmp,ptrTmp+nOffsets);
  mxAssert(xoffset.size()==nOffsets,"xoffset vector not of same size as nOffsets\n");
  // Assert above is not really needed, just there because i am
  // not sure if I am using vector constructor properly

  tmp=mxGetField(prhs[3],0,"yoffset");
  mxAssert(tmp!=NULL,"yoffset field not found\n");
  mxAssert(mxGetNumberOfElements(tmp)==nOffsets,"yoffset incorrect size\n");
  mxAssert(mxGetClassID(tmp)==mxINT32_CLASS,"yoffset not of type int32\n");
  ptrTmp=(int*)mxGetData(tmp);
  yoffset=vector<int>(ptrTmp,ptrTmp+nOffsets);

  tmp=mxGetField(prhs[3],0,"zoffset");
  mxAssert(tmp!=NULL,"zoffset field not found\n");
  mxAssert(mxGetNumberOfElements(tmp)==nOffsets,"zoffset incorrect size\n");
  mxAssert(mxGetClassID(tmp)==mxINT32_CLASS,"zoffset not of type int32\n");
  ptrTmp=(int*)mxGetData(tmp);
  zoffset=vector<int>(ptrTmp,ptrTmp+nOffsets);

  isTemporal=vector<bool>(nOffsets,false);
  for(int i=0;i<nOffsets;i++){isTemporal[i]=(zoffset[i]!=0);}

  if(sizeof(int)<4)
    mexErrMsgTxt("Code assumes int is > 4 bytes, it isnt\n");

  // ---- Data verified ok, now set up all the pointers

  unsigned char *mask=(unsigned char*)mxGetData(prhs[0]);
  //double *unaryImg=mxGetPr(prhs[1]);
  double *unaryImg=(double*)mxMalloc(sizeof(double)*nPix*2);
  memcpy(unaryImg,mxGetPr(prhs[1]),sizeof(double)*nPix*2);
  double *fgL=unaryImg;
  double *bgL=fgL+frameRes;
  unsigned char *img=(unsigned char*)mxGetData(prhs[2]);

  int *idxMap=(int*)mxMalloc(nPix*sizeof(int));

  // -- scan the mask to index the pixels ----
  unsigned char *it_mask=mask;
  int *it_idxMap=idxMap;
  int idx=0;
  for(int i=0;i<nPix;i++,it_mask++,it_idxMap++){
    if(*it_mask==128){*it_idxMap=idx;idx++;}
    else{*it_idxMap=-1;}
  }

  // -- setup the graph now ---
  int nNodes=idx;
  int mBound=nNodes*nOffsets; // Upper bound on number of edges in graph

  GraphInt *g=newGraph<int,int,int>(nNodes,mBound,NULL); 
  g->add_node(nNodes);

  // -- add pairwise terms and alter unaries (for pixels nxt to hard constraints) --

  int *indexOffsets_idx=(int*)mxMalloc(nOffsets*sizeof(int));
  int *indexOffsets_img=(int*)mxMalloc(nOffsets*sizeof(int));
  int *indexOffsets_unary=(int*)mxMalloc(nOffsets*sizeof(int));
  for(int i=0;i<nOffsets;i++) {
    indexOffsets_idx[i]=xoffset[i]*h+yoffset[i]+zoffset[i]*frameRes;
    indexOffsets_img[i]=xoffset[i]*h+yoffset[i]+zoffset[i]*nCh*frameRes;
    indexOffsets_unary[i]=xoffset[i]*h+yoffset[i]+zoffset[i]*2*frameRes;
  }

  it_mask=mask;
  it_idxMap=idxMap;

  vector<int> fgUnaryEdits_offset;
  vector<double> fgUnaryEdits_value;
  vector<int> bgUnaryEdits_offset;
  vector<double> bgUnaryEdits_value;

  for(int z=0;z<nFrames;z++){
    int zStart_idx=z*frameRes;
    int zStart_img=z*frameRes*nCh;
    int zStart_unary=z*frameRes*2;
    for(int x=0;x<w;x++){
      int xStart_idx=zStart_idx+x*h;
      int xStart_img=zStart_img+x*h;
      int xStart_unary=zStart_unary+x*h;
      for(int y=0;y<h;y++,it_mask++,it_idxMap++){
        int lEdge_idx=y+xStart_idx;
        int lEdge_img=y+xStart_img;
        int lEdge_unary=y+xStart_unary;
        for(int i=0;i<nOffsets;i++){
          int y2=y+yoffset[i];
          if(y2>=0 && y2<h){
            int x2=x+xoffset[i];
            if(x2>=0 && x2<w){
              int z2=z+zoffset[i];
              if(z2>=0 && z2<nFrames){
                int nbrOffset_idx=indexOffsets_idx[i];
                int rEdge_idx=lEdge_idx+nbrOffset_idx;
                int rEdge_img=lEdge_img+indexOffsets_img[i];
                unsigned char nxt_mask=*(it_mask+nbrOffset_idx);
                if(*it_mask==128){
                  // compute edge weight
                  double colorW=0;
                  for(int d=0;d<nCh;d++){
                    double diff=((double)img[lEdge_img+d*frameRes]-
                                 (double)img[rEdge_img+d*frameRes]);
                    colorW+=diff*diff;
                  }
                  double edgeW_dbl=0;
                  if(isTemporal[i]){
                    edgeW_dbl=(gcGammaT_i+gcGammaT_e*exp(-beta_t*colorW));
                  }
                  else{
                    edgeW_dbl=(gcGamma_i+gcGamma_e*exp(-beta*colorW));
                  }
                  if(nxt_mask==128){
                    // add edge to graph
                    int edgeW=(int)(gcScale*edgeW_dbl);
                    g->add_edge(*it_idxMap,*(it_idxMap+nbrOffset_idx),edgeW,edgeW); 
                  }
                  else if(nxt_mask==255){
                    double *lEdge_bgL=bgL+lEdge_unary;
                    *lEdge_bgL=*lEdge_bgL+edgeW_dbl;
                    bgUnaryEdits_offset.push_back(lEdge_unary);
                    bgUnaryEdits_value.push_back(edgeW_dbl);
                  }
                  else if(nxt_mask==0){
                    double *lEdge_fgL=fgL+lEdge_unary;
                    *lEdge_fgL=*lEdge_fgL+edgeW_dbl;
                    fgUnaryEdits_offset.push_back(lEdge_unary);
                    fgUnaryEdits_value.push_back(edgeW_dbl);
                  }
                  else{
                    mexErrMsgTxt("Invalid value in likelihood mask\n");
                  }
                } // end if *it_mask==128
                else if(nxt_mask==128){
                  int rEdge_unary=lEdge_unary+indexOffsets_unary[i];
                  double colorW=0;
                  for(int d=0;d<nCh;d++){
                    double diff=((double)img[lEdge_img+d*frameRes]-
                                 (double)img[rEdge_img+d*frameRes]);
                    colorW+=diff*diff;
                  }
                  double edgeW_dbl=0;
                  if(isTemporal[i]){
                    edgeW_dbl=(gcGammaT_i+gcGammaT_e*exp(-beta_t*colorW));
                  }
                  else{
                    edgeW_dbl=(gcGamma_i+gcGamma_e*exp(-beta*colorW));
                  }
                  switch(*it_mask){
                    case 0 :
                      {double *rEdge_fgL=fgL+rEdge_unary;
                        *rEdge_fgL=*rEdge_fgL+edgeW_dbl;
                        fgUnaryEdits_offset.push_back(rEdge_unary);
                        fgUnaryEdits_value.push_back(edgeW_dbl);}
                        break;
                    case 255 :
                        {double *rEdge_bgL=bgL+rEdge_unary;
                          *rEdge_bgL=*rEdge_bgL+edgeW_dbl;
                          bgUnaryEdits_offset.push_back(rEdge_unary);
                          bgUnaryEdits_value.push_back(edgeW_dbl);}
                          break;
                    default:
                          mexErrMsgTxt("Unexpected mask value in likeli_mask\n");
                  }
                } // end if nxtMask==128
              }
            }
          }
        }
      } // end loop over y=0 to h-1
    } // end loop over x=0 to w-1
  } // end loop over z=0 to nFrames-1

  // ---- add the fg and bg unary edits to the returned graphInfo structure ---
  // ---- also add the graph handle to the structure -----
  plhs[2]=mxCreateStructMatrix(1,1,0,NULL);
  mxArray* graphInfo=plhs[2];
  mxAddField(graphInfo,"dgcHandle");
  mxAddField(graphInfo,"fgUnaryEdits_offset");
  mxAddField(graphInfo,"fgUnaryEdits_value");
  mxAddField(graphInfo,"bgUnaryEdits_offset");
  mxAddField(graphInfo,"bgUnaryEdits_value");

  mxArray *tmpArr;
  mxArray *tmpArr2;
  int *tmpPtr_int;double *tmpPtr_dbl;

  int numFgEdits=fgUnaryEdits_value.size();
  mxAssert(numFgEdits==fgUnaryEdits_offset.size(),"Bug in unary edits\n");
  tmpArr=mxCreateNumericMatrix(1,numFgEdits,mxINT32_CLASS,mxREAL);
  tmpArr2=mxCreateNumericMatrix(1,numFgEdits,mxDOUBLE_CLASS,mxREAL);
  tmpPtr_int=(int*)mxGetData(tmpArr);
  tmpPtr_dbl=mxGetPr(tmpArr2);
  vector<int>::iterator i1;
  vector<double>::iterator i2;
  for(i1=fgUnaryEdits_offset.begin(), 
      i2=fgUnaryEdits_value.begin();i1<fgUnaryEdits_offset.end();
      i1++,i2++,tmpPtr_int++,tmpPtr_dbl++){
     
     *tmpPtr_int=*i1;
     *tmpPtr_dbl=*i2;
  }
  mxSetField(graphInfo,0,"fgUnaryEdits_offset",tmpArr);
  mxSetField(graphInfo,0,"fgUnaryEdits_value",tmpArr2);

  int numBgEdits=bgUnaryEdits_value.size();
  mxAssert(numBgEdits==bgUnaryEdits_offset.size(),"Bug in unary edits\n");
  tmpArr=mxCreateNumericMatrix(1,numBgEdits,mxINT32_CLASS,mxREAL);
  tmpArr2=mxCreateNumericMatrix(1,numBgEdits,mxDOUBLE_CLASS,mxREAL);
  tmpPtr_int=(int*)mxGetData(tmpArr);
  tmpPtr_dbl=mxGetPr(tmpArr2);
  for(i1=bgUnaryEdits_offset.begin(), 
      i2=bgUnaryEdits_value.begin();i1<bgUnaryEdits_offset.end();
      i1++,i2++,tmpPtr_int++,tmpPtr_dbl++){
     
     *tmpPtr_int=*i1;
     *tmpPtr_dbl=*i2;
  }
  mxSetField(graphInfo,0,"bgUnaryEdits_offset",tmpArr);
  mxSetField(graphInfo,0,"bgUnaryEdits_value",tmpArr2);

  int numBytes_handle=sizeof(GraphInt*);
  mxArray *dgcHandle=mxCreateNumericMatrix(numBytes_handle,1,mxINT8_CLASS,mxREAL);
  memcpy(mxGetData(dgcHandle),&g,numBytes_handle);
  mxSetField(graphInfo,0,"dgcHandle",dgcHandle);

  it_mask=mask;
  it_idxMap=idxMap;
  int intMax=(1<<30);

  for(int z=0;z<nFrames;z++){
    for(int i=0;i<frameRes;i++,it_mask++,it_idxMap++){
      if(*it_mask==128){
        int curIdx=*it_idxMap;
        double fgLikeli=fgL[z*frameRes*2+i];
        double bgLikeli=bgL[z*frameRes*2+i];
        double diff=gcScale*(fgLikeli-bgLikeli);
        double dbl_intMax=(double)(intMax);
        double dbl_intMin=(double)(-intMax);
        int fgUnary;
        if(diff>dbl_intMax){fgUnary=intMax;}
        else if(diff<dbl_intMin){fgUnary=-intMax;}
        else{fgUnary=(int)diff;}
        g->add_tweights(curIdx,0,fgUnary);
      }
    }
  }

  int flow=g->maxflow();

  // --- Now prepare the output ------------
  mxAssert(nlhs>=3,"Atleast three outputs required\n");
  int dims[3];dims[0]=h;dims[1]=w;dims[2]=nFrames;

  plhs[0]=mxCreateNumericArray(3,dims,mxUINT8_CLASS,mxREAL);
  plhs[1]=mxCreateDoubleScalar((double)flow);

  unsigned char *it_seg=(unsigned char*)mxGetData(plhs[0]);
  it_mask=mask;
  it_idxMap=idxMap;
  for(int i=0;i<nPix;i++,it_seg++,it_mask++,it_idxMap++){
    if(*it_mask==128){
      int curIdx=*it_idxMap;
      *it_seg=(g->what_segment(curIdx)==GraphInt::SOURCE?255:0); // 255 for source, 0 for sink
    }
    else{*it_seg=*it_mask;}
  }

  // ------- To free -------------
  mxFree(idxMap);
  mxFree(indexOffsets_idx);
  mxFree(indexOffsets_unary);
  mxFree(indexOffsets_img);
  mxFree(unaryImg);
  //deleteGraph<int,int,int>(&g);
}
