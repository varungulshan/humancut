#include "mex.h"
#include "graph.h"
#include <cmath>
#include <iostream>
#include <climits>
#include <vector>

using namespace std;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters combinations
     rhs[0] = unary terms [h x w x 2] (double) first channel for label=1 unaries
              second channel for label=2 unaries
     rhs[1] = lEdges [1 x M] (int32). Edge indexes, left hand side (1-indexed)
     rhs[2] = rEdges [1 x M] (int32). Edge indexes, right hand side (1-indexed)
     rhs[3] = edgeWeights [1 x M] (double). Edge weights (symettric right now)
     rhs[4] = opts structure with following fields:
     gcScale -> double scalar

     lhs[0] -> returns the seg labels of all nodes (array of type hxwxnFrames uint8)
     lhs[1] -> returns the flow
     lhs[2] -> returns a structure graphInfo (for future calls)

     Notes on memory consumption:
     This code allocated following sized arrays (only big ones noted here):
     let nPix=h*w*nFrames;
     let nEdges=number of edges in graph (roughly 5*nPix for 8 neighbourhood in space
     and 1 nbrhood in time)
     graph structure (kolmogorov) -> 58 * nEdges + 44 * nPix
     (In practice nPix will be less because this code removes pixels which are
     hard constrained!)
   */

  if (nrhs != 5)
    mexErrMsgTxt("5 inputs required");

  mxAssert(mxGetClassID(prhs[0])==mxDOUBLE_CLASS,
           "prhs[0] (unaries) should be of type double\n");
  mxAssert(mxGetClassID(prhs[1])==mxINT32_CLASS,
           "prhs[1] (lEdges) should be of type int32\n");
  mxAssert(mxGetClassID(prhs[2])==mxINT32_CLASS,
           "prhs[2] (rEdges) should be of type int32\n");
  mxAssert(mxGetClassID(prhs[3])==mxDOUBLE_CLASS,
           "prhs[3] (edgeweights) should be of type double\n");
  mxAssert(mxIsStruct(prhs[4]),"prhs[4] (opts) should be of type struct\n");

  typedef Graph<int,int,int> GraphInt;
  int h=mxGetDimensions(prhs[0])[0];
  int w=mxGetDimensions(prhs[0])[1];
  int frameRes=h*w;
  int nEdges;
  double gcScale;

  // --- Check dimensions of prhs[0] -----
  mxAssert(mxGetNumberOfDimensions(prhs[0])==3,"unaries need to have 3 dimensions\n");
  mxAssert(mxGetDimensions(prhs[0])[2]==2,"3rd dimension needs to be 2 for unaries\n");

  // --- Check dimensions of prhs[1,2,3] -----
  nEdges=mxGetNumberOfElements(prhs[1]);
  mxAssert(mxGetNumberOfElements(prhs[2])==nEdges,"Inconsistent nEdges\n");
  mxAssert(mxGetNumberOfElements(prhs[3])==nEdges,"Inconsistent nEdges\n");


  // --- Check and initialize from the options structure -----
  mxArray *tmp;

  tmp=mxGetField(prhs[4],0,"gcScale");
  mxAssert(tmp!=NULL,"gcScale field not found\n");
  gcScale=*mxGetPr(tmp);

  if(sizeof(int)<4)
    mexErrMsgTxt("Code assumes int is > 4 bytes, it isnt\n");

  // ---- Data verified ok, now set up all the pointers

  double *unaryImg=mxGetPr(prhs[0]);
  double *fgL=unaryImg;
  double *bgL=fgL+frameRes;

  // -- setup the graph now ---
  int nNodes=frameRes;
  int mBound=nEdges; // Upper bound on number of edges in graph

  GraphInt *g=newGraph<int,int,int>(nNodes,mBound,NULL); 
  g->add_node(nNodes);

  // -- add pairwise terms and alter unaries (for pixels nxt to hard constraints) --
  int *lEdges=(int*)mxGetData(prhs[1]);
  int *rEdges=(int*)mxGetData(prhs[2]);
  double* edgeW=mxGetPr(prhs[3]);

  for(int i=0;i<nEdges;i++,lEdges++,rEdges++,edgeW++){
    int wt=(int)(gcScale*(*edgeW));
    g->add_edge( (*lEdges)-1,(*rEdges)-1,wt,wt);
  }

  // ---- add the fg and bg unary edits to the returned graphInfo structure ---
  // ---- also add the graph handle to the structure -----
  plhs[2]=mxCreateStructMatrix(1,1,0,NULL);
  mxArray* graphInfo=plhs[2];
  mxAddField(graphInfo,"dgcHandle");

  int numBytes_handle=sizeof(GraphInt*);
  mxArray *dgcHandle=mxCreateNumericMatrix(numBytes_handle,1,mxINT8_CLASS,mxREAL);
  memcpy(mxGetData(dgcHandle),&g,numBytes_handle);
  mxSetField(graphInfo,0,"dgcHandle",dgcHandle);

  int intMax=INT_INF; // You might want to reduce this limit (in matlabPorts.h) to make sure
  // dynamic graph cuts can handle infinity energies properly when a dynamic
  // call is made
  double dbl_intMax=(double)(intMax);
  double dbl_intMin=(double)(-intMax);

  for(int i=0;i<frameRes;i++){
    double fgLikeli=fgL[i];
    double bgLikeli=bgL[i];
    double diff=gcScale*(fgLikeli-bgLikeli);
    int fgUnary;
    if(diff>dbl_intMax){fgUnary=intMax;}
    else if(diff<dbl_intMin){fgUnary=-intMax;}
    else{fgUnary=(int)diff;}
    g->add_tweights(i,0,fgUnary);
  }

  int flow=g->maxflow();

  // --- Now prepare the output ------------
  mxAssert(nlhs>=3,"Atleast three outputs required\n");
  int dims[2];dims[0]=h;dims[1]=w;

  plhs[0]=mxCreateNumericArray(2,dims,mxUINT8_CLASS,mxREAL);
  plhs[1]=mxCreateDoubleScalar((double)flow);

  unsigned char *it_seg=(unsigned char*)mxGetData(plhs[0]);
  for(int i=0;i<frameRes;i++,it_seg++){
    *it_seg=(g->what_segment(i)==GraphInt::SOURCE?255:0); // 255 for source, 0 for sink
  }

  // ------- To free -------------
  //deleteGraph<int,int,int>(&g);
}
