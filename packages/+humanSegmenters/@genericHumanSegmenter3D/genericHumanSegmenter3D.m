% Class defining a general parametrized model (could be of anything, not just humans!)
classdef genericHumanSegmenter3D < handle
  properties (Abstract,SetAccess=private, GetAccess=public)
    seg % stores the segmentation, [h x w x nFrames]

    debugOpts % structure with debugLevel, debugDir and debugPrefix
    nFrames % Number of frames being processed
  end

  methods(Abstract)
    initializeFull(obj,img,hModel); % Initializes the segmentation and color models
    % given a human model 3D.
    updateSeg_andColorModel(obj,hModel); % This function is called if the 
    % underlying hModel changes. In this case, the segmentation is first
    % updated and then the color models updated, and then normal grabCut proceeds
    energy=getEnergy_bestSeg(obj,hModel,iFrame); % Computes energy, minimizing wrt seg 
    % Uses existing color models and the passed human model. 
    % Makes a call to dynamic graph cuts
  end
end
