function gmmb=initColorModels(features,labelImg,gmmNmix_bg,initFrames)
% gmmf is a 1x1 structure
% gmmb is a nFrames x 1 structure

% Initializes fg color models from all the initFrames, and intiializes bg color models
% for frames contained in initFrames

[h w nFrames]=size(labelImg);

labelImg(labelImg==3)=1; % Soft labels are used for initializing color models also
labelImg(labelImg==4)=2;

nPixels=h*w;
gmmb=struct('mu',[],'sigma',[],'pi',[]); % The order of the fields is important, as
% assignment of structures only works if the order is correct
gmmb(nFrames).mu=[]; % Just making sure the size of gmmb is correct

for i=initFrames
  tmpFeatures=features(:,(1+(i-1)*nPixels):(i*nPixels));
  tmpLabels=labelImg(:,:,i);
  tmpFeatures=tmpFeatures(:,tmpLabels(:)==2);
  gmmb(i)=gmm.init_gmmBS(tmpFeatures,gmmNmix_bg);
end
