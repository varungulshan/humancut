This package implements segmentation on the entire volume. Supports partial initialization of frames, and implements color models for each part.
