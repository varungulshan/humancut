function posterior=getColorPosterior(obj,iFrame,hModel)

unaryImg_bg=obj.unaryImg_color{iFrame};
unaryImg_fg=hModel.getFgUnary(iFrame);
fgExp=exp(-unaryImg_fg);
bgExp=exp(-unaryImg_bg);

posterior=fgExp./(fgExp+bgExp);
