function initColorModel_fromHumanModel(obj,hModel,initFrames)

obj.labelImg=zeros([obj.h obj.w obj.nFrames],'uint8');
toInitialize=false(1,obj.nFrames);
toInitialize(initFrames)=true;
for i=1:obj.nFrames
  if(obj.constrained(i))
    labelImg=hModel.getColorModel_initLabels('globalModels',[obj.h obj.w],i);
    labelImg=modifyLabels(labelImg,obj.seg(:,:,i));
    obj.labelImg(:,:,i)=labelImg;
  elseif(toInitialize(i))
    obj.labelImg(:,:,i)=hModel.getColorModel_initLabels('globalModels',[obj.h obj.w],i);
  else
    obj.labelImg(:,:,i)=uint8(0);
  end
end

obj.gmmb=humanSegmenters.grabCut_partFG.initColorModels...
         (obj.features,obj.labelImg,...
          obj.opts.gmmNmix_bg,initFrames);

function labelImg=modifyLabels(labelImg,seg)
% Uses to the seg to set the unconstrained regions in labelImg
% to soft initializations

%mask=(labelImg==0);
mask=(labelImg~=5 & labelImg~=6);
labelImg(mask & seg==255)=3; % Soft fg
labelImg(mask & seg==0)=4; % Soft bg
