function updateColorModel(obj,hModel)

opts=obj.opts;
labelImg=obj.labelImg;
mask=(labelImg~=5 & labelImg~=6);
mask=mask(:);

hModel.updatePartColors(obj.seg);

nPixels=obj.h*obj.w;
for i=1:obj.nFrames
  tmpMask=mask((1+(i-1)*nPixels):(i*nPixels));
  tmpFeatures=obj.features(:,(1+(i-1)*nPixels):(i*nPixels));
  tmpSeg=obj.seg(:,:,i);
  tmpFeatures=tmpFeatures(:,tmpSeg(:)==0 & tmpMask);
  obj.gmmb(i)=gmm.updateGmm(tmpFeatures,obj.gmmb(i),opts.gmmUpdate_iters);
end
