function updateSeg_andColorModel(obj,hModel)

% First update segmentation, as the human model has changed.
obj.updateSeg(hModel,[1:obj.nFrames]); 

% Also need to update the labelImg as the human model has changed
for i=1:obj.nFrames
  if(obj.constrained(i))
    labelImg=hModel.getColorModel_initLabels('globalModels',[obj.h obj.w],i);
    labelImg=modifyLabels(labelImg,obj.seg(:,:,i));
    obj.labelImg(:,:,i)=labelImg;
  else
    obj.labelImg(:,:,i)=hModel.getColorModel_initLabels('globalModels',[obj.h obj.w],i);
  end
end

for i=1:obj.opts.numIters_newProxy
  obj.updateColorModel(hModel);
  obj.updateSeg(hModel);
end

function labelImg=modifyLabels(labelImg,seg)
% Uses to the seg to set the unconstrained regions in labelImg
% to soft initializations

mask=(labelImg==0);
labelImg(mask & seg==255)=3; % Soft fg
labelImg(mask & seg==0)=4; % Soft bg
