function delete(obj)
% Destructor function, needs to clear the dynamic graph cut handle
if(obj.debugOpts.debugLevel>0)
  fprintf('Destructor function called for grabCut_partFG\n');
end

for i=1:obj.nFrames
  if(~isempty(obj.graphInfo{i}))
    humanSegmenters.cpp.mex_dgc_cleanUp(obj.graphInfo{i});
    obj.graphInfo{i}=[];
  end
end
