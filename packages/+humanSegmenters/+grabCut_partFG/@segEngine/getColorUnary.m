function bgLikeli=getColorUnary(obj,iFrame)

assert(~obj.constrained(iFrame),'Calling color unary for constrained frame not allowed\n');

opts=obj.opts;
nPixels=obj.h*obj.w;

features=obj.features(:,(1+(iFrame-1)*nPixels):(iFrame*nPixels));
bgLikeli=gmm.computeGmm_likelihood(features,obj.gmmb(iFrame));

bgLikeli=opts.uniform_gamma*opts.uniform_value+(1-opts.uniform_gamma)*bgLikeli;

bgLikeli=-log(bgLikeli);
myUB=opts.unaryBound;
bgLikeli(bgLikeli>myUB)=myUB;

bgLikeli=reshape(bgLikeli,[obj.h obj.w]);
