function updateColorModels(obj,hModel)

opts=obj.opts;
labelImg=obj.labelImg;
mask=(labelImg~=5 & labelImg~=6);
mask=mask(:);

hModel.updatePartColors(obj.seg);

tmpFeatures=obj.features(:,obj.seg(:)==0 & mask);
obj.gmmb=gmm.updateGmm(tmpFeatures,obj.gmmb,opts.gmmUpdate_iters);
