function seg=gcSeg(obj,unaryImg,unaryMask)

import humanSegmenters.cpp.*

objOpts=obj.opts;

opts.gcGamma_e=objOpts.gcGamma_e;
opts.gcGamma_i=objOpts.gcGamma_i;
opts.gcScale=objOpts.gcScale;
opts.beta=obj.beta;
opts.xoffset=obj.coffset';
opts.yoffset=obj.roffset';

if(isempty(obj.graphInfo))
  [seg,flow,graphInfo]=mex_dgcBand2D_init(unaryMask,unaryImg,obj.img,opts);
  obj.graphInfo=graphInfo;
else
  [seg,flow]=mex_dgcBand2D_repeat(unaryMask,unaryImg,obj.graphInfo,opts);
end
