function updateSeg(obj,hModel)

[h w nCh]=size(obj.img);
opts=obj.opts;

shapePosterior=hModel.getShapePosterior();
unaryImg_mask=128*ones([h w],'uint8');
unaryImg_mask(shapePosterior==1)=255;
unaryImg_mask(shapePosterior==0)=0;
unaryImg_color_bg=obj.getColorUnary(unaryImg_mask);
unaryImg_color_fg=hModel.getFgUnary();
unaryImg_color=cat(3,unaryImg_color_fg,unaryImg_color_bg);

unaryImg_shape=zeros([h w 2]);
myUB=opts.unaryBound;
tmp=-log(shapePosterior);
tmp(tmp>myUB)=myUB;
unaryImg_shape(:,:,1)=tmp;
tmp=-log(1-shapePosterior);
tmp(tmp>myUB)=myUB;
unaryImg_shape(:,:,2)=tmp;

obj.unaryImg_color=unaryImg_color;
obj.unaryImg_mask=unaryImg_mask;
unaryImg=opts.wtShape*unaryImg_shape+(1-opts.wtShape)*unaryImg_color;

obj.seg=obj.gcSeg(unaryImg,unaryImg_mask);
