function [unaryImg]=getColorUnary(obj,unaryMask)

opts=obj.opts;

mask=(unaryMask(:)==128);
tmpFeatures=obj.features(:,mask);

bgLikeli=gmm.computeGmm_likelihood(tmpFeatures,obj.gmmb);

bgLikeli=opts.uniform_gamma*opts.uniform_value+(1-opts.uniform_gamma)*bgLikeli;

bgLikeli=-log(bgLikeli);
myUB=opts.unaryBound;
bgLikeli(bgLikeli>myUB)=myUB;

[h w nCh]=size(obj.img);

unaryImg=zeros([h w]);unaryImg(mask)=bgLikeli;
