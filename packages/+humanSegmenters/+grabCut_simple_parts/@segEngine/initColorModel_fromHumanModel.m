function initColorModel_fromHumanModel(obj,hModel)

[h w nCh]=size(obj.img);
labelImg=hModel.getColorModel_initLabels('globalModels',[h w]);
obj.labelImg=labelImg;

obj.gmmb=humanSegmenters.grabCut_simple_parts.initColorModels(obj.features,labelImg,...
                    obj.opts.gmmNmix_bg);
