function posterior=getColorPosterior(obj)

unaryImg=obj.unaryImg_color;
fgExp=exp(-unaryImg(:,:,1));
bgExp=exp(-unaryImg(:,:,2));

posterior=fgExp./(fgExp+bgExp);
unaryImg_mask=obj.unaryImg_mask;
posterior(unaryImg_mask==255)=1;
posterior(unaryImg_mask==0)=0;
