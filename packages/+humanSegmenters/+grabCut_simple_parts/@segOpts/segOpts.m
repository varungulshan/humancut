% Copyright (C) 2010 Varun Gulshan
% This class defines the options for grabCut segmentation
classdef segOpts
  properties (SetAccess=public, GetAccess=public)
    gcGamma_e  % gamma of graph cuts (for contrast dependant term)
    gcGamma_i  % gamma of graph cuts (for ising model term)

    gcScale  % scaling to convert to integers
    gcNbrType % 'nbr4' or 'nbr8'
    gcSigma_c % 'auto' or a numeric value

    gmmNmix_fg % number of gmm mixtures for fg, usually 5
    gmmNmix_bg % number of gmm mixtures for bg, usually 5
    gmmUpdate_iters % number of iterations to update GMM in the EM algorithm

    postProcess % 0 = off, 1 = on
    numIters % Number of iterations to run grabCut for

    uniform_gamma
    uniform_value
    unaryBound

    wtShape % Decide weighing on unary terms, trading off shape vs. color. 
    % shape posterior is given wtShape, and color posterior is given
    % weight of (1-wtShape). 
  end

  methods
    function obj=segOpts(optsString)
      obj=obj.setOptsByString(optsString);
    end
  end

end % of classdef
