function [gmmb]=initColorModels(features,labelImg,gmmNmix_bg)

labelImg(labelImg==3)=1; % Soft labels are used for initializing color models also
labelImg(labelImg==4)=2;

tmpFeatures=features(:,labelImg(:)==2);
gmmb=gmm.init_gmmBS(tmpFeatures,gmmNmix_bg);
