function preProcess(obj,fullVideo)
  import humanSegmenters.*;
  if(~strcmp(obj.state,'created')),
    error('Cant call preProcess from %s state\n',obj.state);
    return;
  end

  if(~strcmp(class(fullVideo),'uint8')),
    error('fullVideo should be of type uint8\n');
  end

  preProcess_GC(obj,fullVideo);
  obj.features=humanSegmenters.helpers.extractPixelsVideo(fullVideo);
  obj.state='pped'; 

function preProcess_GC(obj,fullVideo)

switch(obj.opts.gcNbrType)
  case 'nbr4'
    roffset = int32([ 1,  0 ]);
    coffset = int32([  0, 1 ]);
  case 'nbr8'
    roffset = int32([ 1, 1, 0,-1 ]);
    coffset = int32([ 0, 1, 1, 1 ]);
  otherwise
    error('Invalid nbrhood type %s\n',obj.opts.nbrHoodType);
end

obj.roffset=roffset;
obj.coffset=coffset;

nFrames=size(fullVideo,4);

obj.lEdges=cell(nFrames,1);
obj.rEdges=cell(nFrames,1);
obj.edgeWeights=cell(nFrames,1);
obj.beta=zeros(nFrames,1);

for i=1:nFrames

  img=im2double(fullVideo(:,:,:,i));
  [lEdges,rEdges,colorWeights,spWeights]=humanSegmenters.cpp.mex_setupTransductionGraph...
                                        (img,roffset',coffset');
  if(isnumeric(obj.opts.gcSigma_c))
    D=size(img,3);
    obj.beta(i)=1/(2*D*obj.opts.gcSigma_c^2);
  elseif(strcmp(obj.opts.gcSigma_c,'auto'))
    obj.beta(i)=1/(2*mean(colorWeights));
  end
  
  obj.lEdges{i}=lEdges;
  obj.rEdges{i}=rEdges;
  obj.edgeWeights{i}=obj.opts.gcGamma_e*exp(-obj.beta(i)*colorWeights)+...
                     obj.opts.gcGamma_i*ones(size(colorWeights));

end
