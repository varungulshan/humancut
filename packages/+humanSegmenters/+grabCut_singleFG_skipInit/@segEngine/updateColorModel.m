function updateColorModel(obj)

opts=obj.opts;
labelImg=obj.labelImg;
mask=(labelImg~=5 & labelImg~=6);
mask=mask(:);

tmpFeatures=obj.features(:,obj.seg(:)==255 & mask);
obj.gmmf=gmm.updateGmm(tmpFeatures,obj.gmmf,opts.gmmUpdate_iters);

nPixels=obj.h*obj.w;
for i=1:obj.nFrames
  tmpMask=mask((1+(i-1)*nPixels):(i*nPixels));
  tmpFeatures=obj.features(:,(1+(i-1)*nPixels):(i*nPixels));
  tmpSeg=obj.seg(:,:,i);
  tmpFeatures=tmpFeatures(:,tmpSeg(:)==0 & tmpMask);
  obj.gmmb(i)=gmm.updateGmm(tmpFeatures,obj.gmmb(i),opts.gmmUpdate_iters);
end
