function printMemoryEstimate(obj,hModel)

debugOpts=obj.debugOpts;

outFile=[debugOpts.debugDir debugOpts.debugPrefix 'memoryEstimate.txt'];
fH=fopen(outFile,'w');

varNames=cell(0,1);
varBytes=zeros(0,1);

varNames{end+1}='seg';
varBytes(end+1)=numel(obj.seg);

varNames{end+1}='features';
varBytes(end+1)=numel(obj.features)*8;

idxUnConstrained=find(~obj.constrained,1,'first');

nPixels=obj.h*obj.w;
nEdges=length(obj.lEdges{idxUnConstrained});
varNames{end+1}='graphCut graphs(guess)';
varBytes(end+1)=obj.nFrames*(nPixels*44+58*nEdges);

varNames{end+1}='labelImg';
varBytes(end+1)=numel(obj.labelImg);

varNames{end+1}='unaryImg_color';
varBytes(end+1)=numel(obj.unaryImg_color{idxUnConstrained})*8*obj.nFrames;

varNames{end+1}='edges';
varBytes(end+1)=numel(obj.lEdges{idxUnConstrained})*4*2*obj.nFrames; % *2 because lEdges and rEdges, 2 variables

varNames{end+1}='edgeWeights';
varBytes(end+1)=numel(obj.edgeWeights{idxUnConstrained})*8*obj.nFrames; % *2 because lEdges and rEdges, 2 variables

varNames{end+1}='humanModel_parts';
numBytes=0;
for i=1:length(hModel.allParts)
  partList=hModel.allParts{i}.partList;
  for j=1:numel(partList)
    jPart=partList(j);
    numBytes=numBytes+classBytes(jPart.insidePixels)*numel(jPart.insidePixels)+...
        classBytes(jPart.boundaryPixels)*numel(jPart.boundaryPixels)+...
        classBytes(jPart.shapePrior)*numel(jPart.shapePrior);
  end
end
varBytes(end+1)=numBytes;

for i=1:length(varNames)
  fprintf(fH,'Memory used by variable %s = %.2fMB\n',varNames{i},...
          varBytes(i)/2^20);
end
fprintf(fH,'\nTotal memory used (guess) = %.2fMB\n',sum(varBytes)/2^20);

fclose(fH);

function numBytes=classBytes(x)
switch(class(x))
  case 'double'
    numBytes=8;
  case 'int32'
    numBytes=4;
  case 'uint8'
    numBytes=1;
  case 'logical'
    numBytes=1;
  otherwise
    error('Unexpected class\n');
end
