function energy=getEnergy_bestSeg(obj,hModel,iFrame)

opts=obj.opts;

assert(~obj.constrained(iFrame),'Call to energy of constriained frame not allowed\n');

unaryImg_color=obj.unaryImg_color{iFrame};
shapePosterior=hModel.getShapePosterior([obj.h obj.w],iFrame);

unaryImg_shape=zeros([obj.h obj.w 2]);
myUB=opts.unaryBound;
tmp=-log(shapePosterior);
tmp(tmp>myUB)=myUB;
unaryImg_shape(:,:,1)=tmp;
tmp=-log(1-shapePosterior);
tmp(tmp>myUB)=myUB;
unaryImg_shape(:,:,2)=tmp;

unaryImg=opts.wtShape*unaryImg_shape+(1-opts.wtShape)*unaryImg_color;
seg=obj.gcSeg(unaryImg,iFrame);
energy=obj.computeEnergy_singleFrame(seg,unaryImg,obj.lEdges{iFrame},...
           obj.rEdges{iFrame},obj.edgeWeights{iFrame});
