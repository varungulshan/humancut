function initializePropagated_frame(obj,hModel,iFrame)

assert(~obj.constrained(iFrame),'Cant initialize a constrained frame\n');

tmpLabels=hModel.getColorModel_initLabels('globalModels',[obj.h obj.w],...
                         iFrame);
obj.labelImg(:,:,iFrame)=tmpLabels;

tmpLabels(tmpLabels==3)=1;
tmpLabels(tmpLabels==4)=2;

nPixels=obj.h*obj.w;
tmpFeatures=obj.features(:,(1+(iFrame-1)*nPixels):(iFrame*nPixels));
tmpFeatures=tmpFeatures(:,tmpLabels(:)==2);
obj.gmmb(iFrame)=gmm.init_gmmBS(tmpFeatures,obj.opts.gmmNmix_bg);

obj.updateSeg(hModel,iFrame);

%if(obj.debugOpts.debugLevel>0)
  %for i=1:nFrames
    %miscFns.saveDebug(obj.debugOpts,hModel.getShapePosterior([h w],i),...
            %sprintf('%03d_initShapePosterior.png',i));
    %posterior_color=obj.getColorPosterior();
    %miscFns.saveDebug(obj.debugOpts,posterior_color,sprintf('iter%03d_colorUnaries.jpg',1));
  %end
  %obj.printMemoryEstimate();
%end

%for i=2:obj.opts.numIters
  %obj.updateColorModel(); % updates it using current segmentation, and stored labelImg 
  %% the stored labelImg is a function of hModel
  %obj.updateSeg(hModel); % update segmentation using current color model
                         % and the passed human model
  %if(obj.debugOpts.debugLevel>0)
    %posterior_color=obj.getColorPosterior();
    %miscFns.saveDebug(obj.debugOpts,posterior_color,sprintf('iter%03d_colorUnaries.jpg',i));
  %end
%end
