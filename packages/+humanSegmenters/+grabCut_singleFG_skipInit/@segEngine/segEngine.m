% Copyright (C) 2010 Varun Gulshan
% This class implements grabCut segmentation with a human model embedded in it
classdef segEngine < handle
  properties (SetAccess=private, GetAccess=public)
    state % string = 'created','initialized'
    opts % object of type grabCut_humanModel.segOpts
    debugOpts % structure with debugLevel, debugDir and debugPrefix

    seg % uint8, current segmentation, [h x w x nFrames]

    roffset % internal variables for graphcut
    coffset % internal variables for graphcut
    beta % internal variable for graphCut (nFrames x 1)

    gmmf % variable for color model of fg (single element structure)
    gmmb % variable for color model of bg (nFrames x 1 structure)

    features % internal variable ( nCh x (h*w*nFrames) , double array)
    graphInfo % for saving graph state, nFrames x 1 structure
    labelImg % labelImg used to initialize color models (gives regions where to not
             % learn color models from) 
             % is h x w x nFrames uint8)
    unaryImg_color % cell array of nFrames x 1, each entry is h x w x 2 array

    lEdges % int32, lhs of graph edges, cell array of size nFrames x 1
    rEdges % int32, rhs of graph edges, cell arrya of size nFrames x 1
    edgeWeights % double, penalities to pay when edges break (symettric), cell array of
    % nFrames x 1

    h
    w
    nCh
    nFrames % dimensions of the video
    constrained % 1 x nFrames logical , indicating if a frame is constrained or not
  end

  methods
    function obj=segEngine(segOpts,debugOpts)
      obj.debugOpts=debugOpts;
      assert(strcmp(class(segOpts),...
      'humanSegmenters.grabCut_singleFG_skipInit.segOpts'),'Options should be of proper type\n');
      obj.opts=segOpts;
      obj.state='created';
      obj.seg=[];
    end
    initializeExact_frames(obj,initFrames,hModel,idxC,segC,fullVideo);
    %initializeFull(obj,fullVideo,hModel,idxC,segsC); % Initializes the segmentation and color models
    % given a human model. Initialization is first done for color models (using
    % human model), and then segmentation is initialized. color models and 
    % segmentations are then iterated
    updateSeg_andColorModel(obj,hModel); % This function is called if the 
    % underlying hModel changes. In this case, the segmentation is first
    % updated and then the color models updated, and then normal grabCut proceeds

    energy=getEnergy_bestSeg(obj,hModel,iFrame); % Computes energy, minimizing wrt seg 
    % Uses existing color models and the passed human model. 
    % Makes a call to dynamic graph cuts
    posterior=getColorPosterior(obj,iFrame,hModel);
    initializePropagated_frame(obj,hModel,iFrame);
    printMemoryEstimate(obj,hModel);
    delete(obj);
  end

  methods (Access=private)
    %[unaryImg,unaryMask]=createUnaryImg(obj) % Uses the stored gmm models and labelImg
                         % to compute unaries, also stores information that can be used
                         % to update color models for next iteration
    %seg=gcSeg(obj,unaryImg,unaryMask)
    preProcess(obj,fullVideo) % Call this function first to preprocess image if needed
    % fullVideo should be uint8, between [0,1]
    %start(obj,labelImg) %  Run grabCut given the initial labelImg
    % The number of iterations is given in opts, labelImg is encoded as follows:
    % labelImg=0 is empty
    % labelImg=1 is FG (hard constrained)
    % labelImg=2 is BG (hard contrained)
    % labelImg=3 is FG (soft, used only in first iteration to initialize color model)
    % labelImg=4 is BG (soft, used only in first iteration to initialize color model)
    % labelImg=5 is FG (hard constrained, but not used for color models)
    % labelImg=6 is BG (hard constrained, but not used for color models)

    %iterateOnce(obj) % Performs one more iteration of grabCut, can only be called after 
                     % start has been called

    initColorModel_fromHumanModel(obj,hModel,initFrames);
    updateSeg(obj,hModel,frameNums); % Uses the stored color model and the passed human model
                           % to generate a new segmentation
    unaryImg=getColorUnary(obj,iFrame); % Uses stored color model to compute unary img by color
    seg=gcSeg(obj,unaryImg,iFrame);
    updateColorModel(obj); % Updates color model from the current segmentation
  end

  methods (Static=true)
    energy=computeEnergy_singleFrame(seg,unaryImg,lEdges,rEdges,edgeWeights); % Computes energy
    % of a segmentation given unary and pairwise terms
  end
end
