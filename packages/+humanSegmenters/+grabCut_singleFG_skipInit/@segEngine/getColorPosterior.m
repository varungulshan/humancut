function posterior=getColorPosterior(obj,iFrame,hModel)
% The hModel is not used. just adding it to make this function
% interface compatible with grabCut3D_partFG interface

unaryImg=obj.unaryImg_color{iFrame};
fgExp=exp(-unaryImg(:,:,1));
bgExp=exp(-unaryImg(:,:,2));

posterior=fgExp./(fgExp+bgExp);
