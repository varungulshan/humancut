function obj=setOptsByString(obj,optsString)

switch(optsString)
  case 'iter5_2'
    obj.gcGamma_e=50;
    obj.gcGamma_i=0;

    obj.gcScale=500;
    obj.gcNbrType='nbr8';
    obj.gcSigma_c='auto';

    obj.gmmNmix_fg=5;
    obj.gmmNmix_bg=5;
    obj.postProcess=0;
    obj.numIters=5;
    obj.gmmUpdate_iters=1;

    obj.uniform_gamma=0.05;
    obj.uniform_value=1;
    obj.unaryBound=obj.gcScale*(obj.gcGamma_e+obj.gcGamma_i)*100000;

    obj.wtShape=0.5;
    obj.numIters_newProxy=5;

 otherwise
    error('Invalid joint model options string: %s\n',optsString);
end
