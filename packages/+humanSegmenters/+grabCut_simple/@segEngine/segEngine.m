% Copyright (C) 2010 Varun Gulshan
% This class implements grabCut segmentation with a human model embedded in it
classdef segEngine < handle
  properties (SetAccess=private, GetAccess=public)
    state % string = 'created','initialized'
    opts % object of type grabCut_humanModel.segOpts
    debugOpts % structure with debugLevel, debugDir and debugPrefix

    img % double img, b/w [0,1]
    seg % uint8, current segmentation

    roffset % internal variables for graphcut
    coffset % internal variables for graphcut
    beta % internal variable for graphCut

    gmmf % variable for color model of fg
    gmmb % variable for color model of bg

    features % internal variable
    graphInfo % for saving graph state
    labelImg % labelImg used to initialize color models (gives regions where to not
             % learn color models from)
    unaryImg_color
    unaryImg_mask

  end

  methods
    function obj=segEngine(segOpts,debugOpts)
      obj.debugOpts=debugOpts;
      assert(strcmp(class(segOpts),...
            'humanSegmenters.grabCut_simple.segOpts'),'Options should be of proper type\n');
      obj.opts=segOpts;
      obj.state='created';
      obj.seg=[];
      obj.img=[];
    end
    initializeFull(obj,img,hModel); % Initializes the segmentation and color models
    % given a human model. Initialization is first done for color models (using
    % human model), and then segmentation is initialized. color models and 
    % segmentations are then iterated
    posterior=getColorPosterior(obj);
    delete(obj);
  end

  methods (Access=private)
    %[unaryImg,unaryMask]=createUnaryImg(obj) % Uses the stored gmm models and labelImg
                         % to compute unaries, also stores information that can be used
                         % to update color models for next iteration
    %seg=gcSeg(obj,unaryImg,unaryMask)
    %preProcess(obj,img) % Call this function first to preprocess image if needed
                        % img should be double, between [0,1]
    %start(obj,labelImg) %  Run grabCut given the initial labelImg
    % The number of iterations is given in opts, labelImg is encoded as follows:
    % labelImg=0 is empty
    % labelImg=1 is FG (hard constrained)
    % labelImg=2 is BG (hard contrained)
    % labelImg=3 is FG (soft, used only in first iteration to initialize color model)
    % labelImg=4 is BG (soft, used only in first iteration to initialize color model)
    % labelImg=5 is FG (hard constrained, but not used for color models)
    % labelImg=6 is BG (hard constrained, but not used for color models)

    %iterateOnce(obj) % Performs one more iteration of grabCut, can only be called after 
                     % start has been called

    initColorModel_fromHumanModel(obj,hModel);
    updateSeg(obj,hModel); % Uses the stored color model and the passed human model
                           % to generate a new segmentation
    unaryImg=getColorUnary(obj,unaryMask); % Uses stored color model to compute unary img by color
    seg=gcSeg(obj,unaryImg,unaryMask);
    updateColorModel(obj); % Updates color model from the current segmentation
    
  end

  methods (Static=true)
    %energy=computeEnergy(seg,unaryImg,lEdges,rEdges,edgeWeights); % Computes energy
    % of a segmentation given unary and pairwise terms
  end
end
