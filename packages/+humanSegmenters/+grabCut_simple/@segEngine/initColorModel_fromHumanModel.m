function initColorModel_fromHumanModel(obj,hModel)

[h w nCh]=size(obj.img);
labelImg=hModel.getColorModel_initLabels('globalModels',[h w]);
obj.labelImg=labelImg;
[obj.gmmf,obj.gmmb]=humanSegmenters.grabCut_simple.initColorModels(obj.features,labelImg,...
                    obj.opts.gmmNmix_fg,obj.opts.gmmNmix_bg);
