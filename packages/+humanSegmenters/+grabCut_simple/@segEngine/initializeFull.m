function initializeFull(obj,img,hModel)

obj.preProcess(img);
obj.initColorModel_fromHumanModel(hModel);
obj.updateSeg(hModel);

for i=2:obj.opts.numIters
  obj.updateColorModel(); % updates it using current segmentation, and stored labelImg 
  % the stored labelImg is a function of hModel
  obj.updateSeg(hModel); % update segmentation using current color model
                         % and the passed human model
end
