function [unaryImg]=getColorUnary(obj,unaryMask)

opts=obj.opts;

mask=(unaryMask(:)==128);
tmpFeatures=obj.features(:,mask);

fgLikeli=gmm.computeGmm_likelihood(tmpFeatures,obj.gmmf);
bgLikeli=gmm.computeGmm_likelihood(tmpFeatures,obj.gmmb);

fgLikeli=opts.uniform_gamma*opts.uniform_value+(1-opts.uniform_gamma)*fgLikeli;
bgLikeli=opts.uniform_gamma*opts.uniform_value+(1-opts.uniform_gamma)*bgLikeli;

fgLikeli=-log(fgLikeli);
bgLikeli=-log(bgLikeli);
myUB=opts.unaryBound;
fgLikeli(fgLikeli>myUB)=myUB;
bgLikeli(bgLikeli>myUB)=myUB;

[h w nCh]=size(obj.img);

unaryImg=zeros([h w 2]);
tmp=zeros([h w]);tmp(mask)=fgLikeli;
unaryImg(:,:,1)=tmp;
tmp=zeros([h w]);tmp(mask)=bgLikeli;
unaryImg(:,:,2)=tmp;
