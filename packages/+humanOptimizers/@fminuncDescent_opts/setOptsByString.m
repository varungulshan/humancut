function obj=setOptsByString(obj,optsString)

switch(optsString)
  case 'try1'
    obj.diffMaxChange=5;
    obj.diffMinChange=1;
    obj.maxFunEvals=26*20; % 26 calls are taken to evaluated gradient once
    obj.hessianUpdate='bfgs'; % this is the default for fminunc
  case 'try2'
    obj.diffMaxChange=5;
    obj.diffMinChange=1;
    obj.maxFunEvals=26*20; % 26 calls are taken to evaluated gradient once
    obj.hessianUpdate='steepdesc'; % this makes it a first order method
  case 'try3'
    obj.diffMaxChange=5;
    obj.diffMinChange=1;
    obj.maxFunEvals=26*200; % 26 calls are taken to evaluated gradient once
    obj.hessianUpdate='bfgs'; % this is the default for fminunc
  case 'try4'
    obj.diffMaxChange=5;
    obj.diffMinChange=1;
    obj.maxFunEvals=26*200; % 26 calls are taken to evaluated gradient once
    obj.hessianUpdate='steepdesc'; % this makes it a first order method
 
  otherwise
    error('Invalid joint model options string: %s\n',optsString);
end
