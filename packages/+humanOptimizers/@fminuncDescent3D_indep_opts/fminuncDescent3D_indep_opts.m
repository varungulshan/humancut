% Copyright (C) 2010 Varun Gulshan
% This class defines the options for jointsModel human representation
classdef fminuncDescent3D_indep_opts
  properties (SetAccess=private, GetAccess=public)
    % No options right now!
    diffMaxChange % Maximum change for finite differencing, per entry of x?
    diffMinChange % Minimum change for finite differencing, per entry of x?
    maxFunEvals % maximum number of function evaluations to make
    hessianUpdate % the method to use for updating hessian (or just disable 2nd order
    % method by setting it to 'steepdesc'
  end

  methods
    function obj=fminuncDescent3D_indep_opts(optsString)
      obj=obj.setOptsByString(optsString);
    end
    obj=setOptsByString(obj,optsString);
  end

end % of classdef
