function obj=setOptsByString(obj,optsString)

switch(optsString)
  case 'try1'
    obj.maxFunEvals=26*20; % 26 calls are taken to evaluated gradient once
    obj.tolX=norm(ones(26,1));
  case 'try2'
    obj.maxFunEvals=26*200; % 26 calls are taken to evaluated gradient once
    obj.tolX=norm(ones(26,1));
  otherwise
    error('Invalid joint model options string: %s\n',optsString);
end
