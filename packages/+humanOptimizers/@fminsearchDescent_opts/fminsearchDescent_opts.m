% Copyright (C) 2010 Varun Gulshan
% This class defines the options for fminsearch method descent
classdef fminsearchDescent_opts
  properties (SetAccess=private, GetAccess=public)
    % No options right now!
    maxFunEvals % maximum number of function evaluations to make
    tolX % convergence criteria for modelVector, you need to be aware of dimensionality
    % of the input space you are optimizing over to set it correctly
  end

  methods
    function obj=fminsearchDescent_opts(optsString)
      obj=obj.setOptsByString(optsString);
    end
    obj=setOptsByString(obj,optsString);
  end

end % of classdef
