% Class defining a general parametrized model (could be of anything, not just humans!)
classdef genericHumanOptimizer < handle
  properties (Abstract,SetAccess=private, GetAccess=public)
    debugOpts % structure with debugLevel, debugDir and debugPrefix
  end

  methods(Abstract)
    updateHumanModel(obj,hModel,segH); % Given the current human model and segmentation
    % state, update parameters of the human model
  end
end
