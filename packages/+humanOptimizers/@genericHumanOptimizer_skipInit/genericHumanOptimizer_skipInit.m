% Class defining a general parametrized model (could be of anything, not just humans!)
% It allows for missing initializations
classdef genericHumanOptimizer_skipInit < handle
  properties (Abstract,SetAccess=private, GetAccess=public)
    debugOpts % structure with debugLevel, debugDir and debugPrefix
  end

  methods(Abstract)
    updateHumanModel(obj,hModel,segH); % Given the current human model and segmentation
    % state, update parameters of the human model
    initializeAll(obj,hModel,segH,skipZone,...
             annots,K,hModel_constraintIdx,hModel_constraintVectors,...
             blockVideo,segH_constraintIdx,segH_constraintSegs);
  end
end
