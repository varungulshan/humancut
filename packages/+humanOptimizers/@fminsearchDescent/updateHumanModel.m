function updateHumanModel(obj,hModel,segH)
% Function to update the parametrized human model.
% Input is the current initialization of human model, color model, and segmentation
% It will keep the segmentation and color models fixed, and just optimize
% the human model right now (using gradient descent)

debugOpts=obj.debugOpts;
done=false;

function energy=fOpt(x)
  hModel.setModelVector(x);
  energy=segH.getEnergy_bestSeg(hModel);
end

energyValues=[];

function stop=iterationOutput(x,optimValues,state,fH)
  fprintf(fH,'Energy after %04d iterations: %d (# of fn calls=%d)\n',optimValues.iteration,...
          optimValues.fval,optimValues.funccount);
  energyValues(end+1)=optimValues.fval;
  stop=false;
end

if(debugOpts.debugLevel>0)
  fH=fopen([debugOpts.debugDir debugOpts.debugPrefix 'optimizationLog.txt'],'a');
  fprintf(fH,'\nOptimizing human model, initial energy (before fminsearch) = %d\n',segH.getEnergy_bestSeg(hModel));
  stTime=clock;
end

if(debugOpts.debugLevel>2)
  fprintf(fH,'\n');
  iterationFn=@(x,optimValues,state) iterationOutput(x,optimValues,state,fH);
else
  iterationFn={};
end


opts=obj.opts;
if(debugOpts.debugLevel>0)
  diagnostics='off'; % Diganostics are not very useful right now
else
  diagnostics='off';
end

fminOpts=optimset('MaxFunEvals',opts.maxFunEvals,'Display',diagnostics,...
                  'TolX',opts.tolX,'OutputFcn',iterationFn);
[xNew,fval,exitflag,output]=fminsearch(@fOpt,hModel.modelVector,fminOpts);
hModel.setModelVector(xNew);

if(debugOpts.debugLevel>2)
  fprintf(fH,'\n');
  save([debugOpts.debugDir debugOpts.debugPrefix 'energyValues.mat'],'energyValues');
end

if(debugOpts.debugLevel>0)
  fprintf(fH,'Optimizing human model, final energy (after fminsearch) = %d\n',fval);
  fprintf(fH,'---- Optimization statistics ----:\n');
  fprintf(fH,'Time taken: %.2f minutes\n',etime(clock,stTime)/60);
  fprintf(fH,'Number of iterations: %d\nNumber of function evaluations: %d\nAlgorithm used: %s\nExit message: %s\n',output.iterations,output.funcCount,output.algorithm,output.message);
  fprintf(fH,'---- End opti statistics -------\n');
  fclose(fH);
end

end
