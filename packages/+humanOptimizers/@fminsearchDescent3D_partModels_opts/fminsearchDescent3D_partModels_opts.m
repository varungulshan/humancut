% Copyright (C) 2010 Varun Gulshan
% This class defines the options for jointsModel human representation
classdef fminsearchDescent3D_partModels_opts 
  properties (SetAccess=private, GetAccess=public)
    maxFunEvals % maximum number of function evaluations to make
    tolX % convergence criteria for modelVector, you need to be aware of dimensionality
  end

  methods
    function obj=fminsearchDescent3D_partModels_opts(optsString)
      obj=obj.setOptsByString(optsString);
    end
    obj=setOptsByString(obj,optsString);
  end

end % of classdef
