classdef fminunc_partsAndTracks < handle
  properties (SetAccess=private, GetAccess=public)
    opts % of type fminuncDescent_opts
    debugOpts % structure with debugLevel, debugDir and debugPrefix
  end

  methods
    function obj=fminunc_partsAndTracks(debugOpts,opts)
      assert(isfield(debugOpts,'debugLevel'),'Missing fields from debugOpts\n');
      assert(isfield(debugOpts,'debugDir'),'Missing fields from debugOpts\n');
      assert(isfield(debugOpts,'debugPrefix'),'Missing fields from debugOpts\n');
      obj.debugOpts=debugOpts;
      assert(strcmp(class(opts),'humanOptimizers.fminunc_partsAndTracks_opts'),'Invalid class for opts\n');
      obj.opts=opts;
    end

    updateHumanModel(obj,hModel,segH); % Given the current human model and segmentation
    % state, update parameters of the human model
    updateHumanModel_singleFrame(obj,hModel,segH,iFrame,nbrFrame); 
    initializeAll(obj,hModel,segH,skipZone,...
             annots,K,hModel_constraintIdx,hModel_constraintVectors,...
             blockVideo,blockPtCorr,segH_constraintIdx,segH_constraintSegs);
    propagateInitialization(obj,frameOrder,hModel,segH); % propagates the initialization
    % in the order specified

  end % of methods

  methods(Static=true)
  end
end % of classdef
