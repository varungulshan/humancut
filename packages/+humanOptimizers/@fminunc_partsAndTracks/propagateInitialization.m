function propagateInitialization(obj,frameOrder,hModel,segH)

for i=1:size(frameOrder,2)
  initializer=frameOrder(2,i);
  initializee=frameOrder(1,i);
  hModel.copyFrame(initializee,initializer);
  segH.initializePropagated_frame(hModel,initializee);
  obj.updateHumanModel_singleFrame(hModel,segH,initializee,initializer);
end
