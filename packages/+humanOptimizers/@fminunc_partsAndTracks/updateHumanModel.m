function updateHumanModel(obj,hModel,segH)
% Function to update the parametrized human model.
% Input is the current initialization of human model, color model, and segmentation
% It will keep the segmentation and color models fixed, and just optimize
% the human model right now (using gradient descent)

debugOpts=obj.debugOpts;
constrained=hModel.constrained;
assert(all(constrained==segH.constrained),'Mismatch on constraints\n');

nFrames=hModel.nFrames;
numUnconstrained=nFrames-nnz(constrained);
mapUnconstrained_constrained=find(~constrained);

energyFrame_cache=zeros(1,nFrames);
energyTracks_cache=zeros(1,nFrames-1);
dimensionality=hModel.dimensionality;

for i=1:nFrames
  energyFrame_cache(i)=segH.getEnergy_bestSeg(hModel,i);
end

for i=1:nFrames-1
  energyTracks_cache(i)=hModel.getEnergy_tracks(i,i+1);
end

modelVector_cache=hModel.modelVector(:,~constrained);

function energy=fOpt(x)
    x=reshape(x,[dimensionality numUnconstrained]);
    xChange=(modelVector_cache~=x);
    frameChange=any(xChange,1);
    idxChange=find(frameChange);
    idxChange_orig=mapUnconstrained_constrained(idxChange);
    for i1=1:length(idxChange_orig)
      origIdx=idxChange_orig(i1);
      hModel.setModelVector(x(:,idxChange(i1)),origIdx);
      energyFrame_cache(origIdx)=segH.getEnergy_bestSeg(hModel,origIdx);
    end
    idxChange_tracks=[idxChange_orig idxChange_orig-1];
    idxChange_tracks=idxChange_tracks(idxChange_tracks>0 & ...
                                      idxChange_tracks<nFrames);
    idxChange_tracks=unique(idxChange_tracks);
    for i1=1:length(idxChange_tracks)
      curIdx=idxChange_tracks(i1);
      energyTracks_cache(curIdx)=...
          hModel.getEnergy_tracks(curIdx,curIdx+1);
    end
    %hModel.setModelVector(x,iFrame);
    %energy=segH.getEnergy_bestSeg(hModel,iFrame); 
    %energy=energy+hModel.getEnergy_tracks(iFrame,nbrFrame);
    energy=sum(energyTracks_cache)+sum(energyFrame_cache);
    modelVector_cache=x;
end

if(debugOpts.debugLevel>0)
  fH=fopen([debugOpts.debugDir debugOpts.debugPrefix ...
    sprintf('JointOptimizationLog.txt')],'a');
  fprintf(fH,'Optimizing human model, initial energy (before fminunc) = %d\n',...
          sum(energyTracks_cache)+sum(energyFrame_cache));
  stTime=clock;
end

energyValues=[];

function stop=iterationOutput(x,optimValues,state,fH)
  fprintf(fH,'Energy after %04d iterations: %d (# of fn calls=%d)\n',optimValues.iteration,...
          optimValues.fval,optimValues.funccount);
  energyValues(end+1)=optimValues.fval;
  stop=false;
end

if(debugOpts.debugLevel>2)
  fprintf(fH,'\n');
  iterationFn=@(x,optimValues,state) iterationOutput(x,optimValues,state,fH);
else
  iterationFn={};
end

opts=obj.opts;
if(debugOpts.debugLevel>0)
  diagnostics='off'; % Diganostics are not very useful right now
else
  diagnostics='off';
end

maxFunEvals=opts.maxGradientRounds*hModel.dimensionality*nFrames;

fminOpts=optimset('DiffMaxChange',opts.diffMaxChange,'DiffMinChange',opts.diffMinChange...
                ,'MaxFunEvals',maxFunEvals,'LargeScale','off',...
                'Diagnostics',diagnostics,'HessUpdate',opts.hessianUpdate);
[xNew,fval,exitflag,output]=fminunc(@fOpt,modelVector_cache(:),fminOpts);
xNew=reshape(xNew,[dimensionality numUnconstrained]);
for i=length(mapUnconstrained_constrained)
  hModel.setModelVector(xNew(:,i),mapUnconstrained_constrained(i));
end

if(debugOpts.debugLevel>2)
  fprintf(fH,'\n');
  save([debugOpts.debugDir debugOpts.debugPrefix 'energyValues.mat'],'energyValues');
end

if(debugOpts.debugLevel>0)
  fprintf(fH,'Optimizing human model, final energy (after fminunc) = %d\n',fval);
  fprintf(fH,'--------- Optimization statistics (for above optimization) -------\n');
  fprintf(fH,'Time taken: %.2f minutes\n',etime(clock,stTime)/60);
  fprintf(fH,'Number of iterations: %d\nNumber of function evaluations: %d\nAlgorithm used: %s\nExit message: %s\n',output.iterations,output.funcCount,output.algorithm,output.message);
  fprintf(fH,'---- End opti statistics -------\n');
  fclose(fH);
end

end
