function initializeAll(obj,hModel,segH,skipZone,...
             annots,K,hModel_constraintIdx,hModel_constraintVectors,...
             blockVideo,segH_constraintIdx,segH_constraintSegs)

assert(all(size(hModel_constraintIdx)==size(segH_constraintIdx)),'Constraints dont match\n');
assert(all(hModel_constraintIdx(:)==segH_constraintIdx(:)),'Constraints dont match\n');

nFrames=size(blockVideo,4);

constrained=false(1,nFrames);
constrained(hModel_constraintIdx)=true;

% The constrained frames should only be in the beginning
idxLast_constrained=find(constrained,1,'last');
if(isempty(idxLast_constrained)), idxLast_constrained=0;
end

assert(all(constrained(1:idxLast_constrained)),'Invalid constraints\n');

nUnconstrained=nFrames-idxLast_constrained;
unconstrainedRad=floor(nUnconstrained/2);

skipZone=min(skipZone,unconstrainedRad);
skipDia=2*skipZone+1;

idxJump=idxLast_constrained+1;

exactInit_frames=[];
initFrames_order=zeros(2,0); % First row gives which frame to initialize
% second row gives the initializer for the frame

while(idxJump<=nFrames)
  
  idxEnd=min(nFrames,idxJump+skipDia-1);
  idxCentroid=round((idxJump+idxEnd)/2);

  exactInit_frames(end+1)=idxCentroid; 
  for i=(idxCentroid-1):-1:idxJump
    initFrames_order(:,end+1)=[i;i+1];
  end
  for i=(idxCentroid+1):idxEnd
    initFrames_order(:,end+1)=[i;i-1];
  end

  idxJump=idxJump+skipDia;
end

if(obj.debugOpts.debugLevel>0)
  printInitializationSequence(exactInit_frames,initFrames_order,obj.debugOpts);
end
h=size(blockVideo,1);
w=size(blockVideo,2);
hModel.initialize_exact(exactInit_frames,annots,K,hModel_constraintIdx,...
                        hModel_constraintVectors,nFrames,[h w]);
segH.initializeExact_frames(exactInit_frames,hModel,segH_constraintIdx,...
                            segH_constraintSegs,blockVideo);

obj.propagateInitialization(initFrames_order,hModel,segH);
segH.updateSeg_andColorModel(hModel);

function printInitializationSequence(exactFrames,initFrames,opts)

fH=fopen([opts.debugDir opts.debugPrefix 'initSequence.txt'],'w');
fprintf(fH,'Printing initialization sequence of frames:\n');
for i=exactFrames
  fprintf(fH,'Frame %03d is exactly initialized\n',i);
end
for i=1:size(initFrames,2)
  fprintf(fH,'Frame %03d is initialized from Frame %03d\n',initFrames(1,i),...
          initFrames(2,i));
end
fclose(fH);
