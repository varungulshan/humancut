This class uses the matlab fminunc function to optimize the human model.
It works when initializations for all human models are not specified (the parameter
skipZone defines the radius of the uninitialized zone).
