function updateHumanModel(obj,hModel,segH)
% Function to update the parametrized human model.
% Input is the current initialization of human model, color model, and segmentation
% It will keep the segmentation and color models fixed, and just optimize
% the human model right now (using gradient descent)

constrained=hModel.constrained;
assert(all(constrained==segH.constrained),'Mismatch on constraints\n');

for iFrame=1:segH.nFrames
  if(~constrained(iFrame))
    obj.updateHumanModel_singleFrame(hModel,segH,iFrame);
  end
end
