function updateHumanModel(obj,hModel,segH)
% Function to update the parametrized human model.
% Input is the current initialization of human model, color model, and segmentation
% It will keep the segmentation and color models fixed, and just optimize
% the human model right now (using gradient descent)

debugOpts=obj.debugOpts;
done=false;

function energy=fOpt(x,iFrame)
    hModel.setModelVector(x,iFrame);
    energy=segH.getEnergy_bestSeg(hModel,iFrame); 
end

constrained=hModel.constrained;
assert(all(constrained==segH.constrained),'Mismatch on constraints\n');

for iFrame=1:segH.nFrames

  if(~constrained(iFrame))

    if(debugOpts.debugLevel>0)
      fH=fopen([debugOpts.debugDir debugOpts.debugPrefix ...
        sprintf('%03d_optimizationLog.txt',iFrame)],'a');
      fprintf(fH,'Optimizing human model, initial energy (before fminunc) = %d\n',segH.getEnergy_bestSeg(hModel,iFrame));
    end
    
    opts=obj.opts;
    if(debugOpts.debugLevel>0)
      diagnostics='off'; % Diganostics are not very useful right now
    else
      diagnostics='off';
    end
    
    fminOpts=optimset('DiffMaxChange',opts.diffMaxChange,'DiffMinChange',opts.diffMinChange...
                    ,'MaxFunEvals',opts.maxFunEvals,'LargeScale','off',...
                    'Diagnostics',diagnostics,'HessUpdate',opts.hessianUpdate);
    [xNew,fval,exitflag,output]=fminunc(@(x)fOpt(x,iFrame),hModel.modelVector(:,iFrame),fminOpts);
    hModel.setModelVector(xNew,iFrame);
    
    if(debugOpts.debugLevel>0)
      fprintf(fH,'Optimizing human model, final energy (after fminunc) = %d\n',fval);
      fprintf(fH,'---- Optimization statistics ----:\n');
      fprintf(fH,'Number of iterations: %d\nNumber of function evaluations: %d\nAlgorithm used: %s\nExit message: %s\n',output.iterations,output.funcCount,output.algorithm,output.message);
      fprintf(fH,'---- End opti statistics -------\n');
      fclose(fH);
    end

  end
  
end
  
end
