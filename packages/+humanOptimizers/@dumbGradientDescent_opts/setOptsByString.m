function obj=setOptsByString(obj,optsString)

switch(optsString)
  case 'try1'
    obj.maxIters=2; % 0.2 times the lower arm length
    obj.stepSize=0.03; % times the hand length/width (its a square)
  otherwise
    error('Invalid joint model options string: %s\n',optsString);
end
