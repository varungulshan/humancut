% Copyright (C) 2010 Varun Gulshan
% This class defines the options for jointsModel human representation
classdef dumbGradientDescent_opts
  properties (SetAccess=private, GetAccess=public)
    maxIters % number of iterations to run gradient descent for
    stepSize % stepSize for gradient descent
  end

  methods
    function obj=dumbGradientDescent_opts(optsString)
      obj=obj.setOptsByString(optsString);
    end
    obj=setOptsByString(obj,optsString);
  end

end % of classdef
