function updateHumanModel(obj,hModel,segH)
% Function to update the parametrized human model.
% Input is the current initialization of human model, color model, and segmentation
% It will keep the segmentation and color models fixed, and just optimize
% the human model right now (using gradient descent)

done=false;

% Currently I dont have a termination criteria, just run it for a fix number of iterations
maxIters=obj.opts.maxIters;
stepSize=obj.opts.stepSize;
debugOpts=obj.debugOpts;

numIter=0;

if(debugOpts.debugLevel>0)
  fprintf('Updating human model Iter %03d: Current energy = %d\n',numIter,segH.getEnergy_bestSeg(hModel));
end

while(~done)
  thetaGrad=hModel.computeGradient(segH);
  hModel.updateModel(thetaGrad,stepSize);

  if(debugOpts.debugLevel>0)
    fprintf('Updating human model Iter %03d: Current energy = %d\n',numIter,segH.getEnergy_bestSeg(hModel));
  end

  numIter=numIter+1;
  if(numIter>=maxIters)
    done=true;
  end
end
