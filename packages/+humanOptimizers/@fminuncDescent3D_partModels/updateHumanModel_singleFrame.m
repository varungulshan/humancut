function updateHumanModel_singleFrame(obj,hModel,segH,iFrame)
% Function to update the parametrized human model.
% Input is the current initialization of human model, color model, and segmentation
% It will keep the segmentation and color models fixed, and just optimize
% the human model right now (using gradient descent)

debugOpts=obj.debugOpts;
done=false;

function energy=fOpt(x)
    hModel.setModelVector(x,iFrame);
    energy=segH.getEnergy_bestSeg(hModel,iFrame); 
end

energyValues=[];

function stop=iterationOutput(x,optimValues,state,fH)
  fprintf(fH,'Energy after %04d iterations: %d (# of fn calls=%d)\n',optimValues.iteration,...
          optimValues.fval,optimValues.funccount);
  energyValues(end+1)=optimValues.fval;
  stop=false;
end

constrained=hModel.constrained;
assert(~constrained(iFrame),'Cant optimize constrained frame\n');

if(debugOpts.debugLevel>0)
  fH=fopen([debugOpts.debugDir debugOpts.debugPrefix ...
    sprintf('%03d_optimizationLog.txt',iFrame)],'a');
  fprintf(fH,'Optimizing human model, initial energy (before fminunc) = %d\n',segH.getEnergy_bestSeg(hModel,iFrame));
  stTime=clock;
end


if(debugOpts.debugLevel>2)
  fprintf(fH,'\n');
  iterationFn=@(x,optimValues,state) iterationOutput(x,optimValues,state,fH);
else
  iterationFn={};
end

opts=obj.opts;
if(debugOpts.debugLevel>0)
  diagnostics='off'; % Diganostics are not very useful right now
else
  diagnostics='off';
end

fminOpts=optimset('DiffMaxChange',opts.diffMaxChange,'DiffMinChange',opts.diffMinChange...
                ,'MaxFunEvals',opts.maxFunEvals,'LargeScale','off',...
                'Diagnostics',diagnostics,'HessUpdate',opts.hessianUpdate);
[xNew,fval,exitflag,output]=fminunc(@fOpt,hModel.modelVector(:,iFrame),fminOpts);
hModel.setModelVector(xNew,iFrame);

if(debugOpts.debugLevel>2)
  fprintf(fH,'\n');
  save([debugOpts.debugDir debugOpts.debugPrefix 'energyValues.mat'],'energyValues');
end

if(debugOpts.debugLevel>0)
  fprintf(fH,'Optimizing human model, final energy (after fminunc) = %d\n',fval);
  fprintf(fH,'--------- Optimization statistics (for above optimization) -------\n');
  fprintf(fH,'Time taken: %.2f minutes\n',etime(clock,stTime)/60);
  fprintf(fH,'Number of iterations: %d\nNumber of function evaluations: %d\nAlgorithm used: %s\nExit message: %s\n',output.iterations,output.funcCount,output.algorithm,output.message);
  fprintf(fH,'---- End opti statistics -------\n');
  fclose(fH);
end

end
