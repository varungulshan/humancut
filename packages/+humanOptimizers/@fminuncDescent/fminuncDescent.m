classdef fminuncDescent < humanOptimizers.genericHumanOptimizer
  properties (SetAccess=private, GetAccess=public)
    opts % of type fminuncDescent_opts
    debugOpts % structure with debugLevel, debugDir and debugPrefix
  end

  methods
    function obj=fminuncDescent(debugOpts,opts)
      assert(isfield(debugOpts,'debugLevel'),'Missing fields from debugOpts\n');
      assert(isfield(debugOpts,'debugDir'),'Missing fields from debugOpts\n');
      assert(isfield(debugOpts,'debugPrefix'),'Missing fields from debugOpts\n');
      obj.debugOpts=debugOpts;
      assert(strcmp(class(opts),'humanOptimizers.fminuncDescent_opts'),'Invalid class for opts\n');
      obj.opts=opts;
    end

    updateHumanModel(obj,hModel,segH); % Given the current human model and segmentation
    % state, update parameters of the human model

  end % of methods

  methods(Static=true)
  end
end % of classdef
