function trainAndTest(trainParams,testParams)

cwd=miscFns.extractDirPath(mfilename('fullpath'));

if(~exist('trainParams','var'))
  trainParams.rootOut_dir=[cwd '../../results/grabCut_trained/train/hogLearning/'];
  %trainParams.opts.gtDir=[cwd '../../data/h3d_gtSeg2/'];
  trainParams.opts.evalOpts_string='try1';
  %trainParams.opts.evalOpts_string='kinect_10Feb2011';

  %trainParams.opts.trainMethod='grabCut_edge2_simpleSearch';
  %trainParams.opts.trainMethod_optsString='try1';
  %trainParams.opts.trainMethod_optsString='kovesi_1_soft';

  trainParams.opts.trainMethod='hogLearnLocal';
  %trainParams.opts.trainMethod_optsString='try1';
  trainParams.opts.trainMethod_optsString='kmeans1_small';

  trainParams.opts.visualize=true;
  trainParams.opts.visOpts.debugVisualize=true;
  trainParams.opts.debugLevel=10;
  trainParams.opts.visOpts.imgExt='jpg';
  trainParams.opts.visOpts.segBdryWidth=2;
  trainParams.overWrite=true;

  %trainParams.testBench_cmd='testBench.getTests_grabCut_tmp()';
  %trainParams.testBench_cmd='testBench.getTests_grabCut_small()';
  trainParams.testBench_cmd='testBench.getTests_grabCut_h3d_train_1()';
  %trainParams.testBench_cmd='testBench.getTests_grabCut_kinect10Feb2011_1_train_1()';
end

if(~exist('testParams','var'))
  testParams.rootOut_dir=[cwd '../../results/grabCut_trained/test/hogLearning/'];
  testParams.trainDir=[cwd '../../results/grabCut_trained/train/hogLearning/'];

  testParams.opts.visualize=true;
  testParams.opts.visOpts.debugVisualize=true;
  testParams.opts.debugLevel=10;
  testParams.opts.visOpts.imgExt='jpg';
  testParams.opts.visOpts.segBdryWidth=2;
  testParams.overWrite=true;

  %testParams.evalOpts_string='kinect_firstAttempt';
  testParams.evalOpts_string='try1';
  %testParams.evalOpts_string='kinect_10Feb2011';

  %testParams.testBench_cmd='testBench.getTests_grabCut_small()';
  testParams.testBench_cmd='testBench.getTests_grabCut_h3d_test_1()';
  %testParams.testBench_cmd='testBench.getTests_grabCut_emptyTrain()';
  %testParams.testBench_cmd='testBench.getTests_grabCut_kinect10Feb2011_1_test_1()';
end

grabCut_trained.trainGrabCut(trainParams);
grabCut_trained.testGrabCut(testParams);
