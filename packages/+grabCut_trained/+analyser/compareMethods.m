function compareMethods(dir1,dir2)

cwd=miscFns.extractDirPath(mfilename('fullpath'));

defaultDir1 = ['/data/titus_scratch/varun/iccv2011/results/test/run018/'];
defaultDir2 = ['/data/titus_scratch/varun/iccv2011/results/test/run004/'];

scoreString = 'accuracyOverlap';
numCompare = 25; % number of images, best and worst to compare
outputSegEnable = true; % set to true to output comparisons
outDir = [cwd '../../../junk/compareMethodsOutput/'];
method1_thresh = 95;
method2_thresh = 40; 

% Assign these to over-ride img and label dirs (useful if running on a machine
% different from the one where results are, for e.g your laptop)
%imgDir = '/home/varun/windows/rugbyShared/work/software/h3d_version1.0/data/people/images/';
%labelDir = '/home/varun/windows/rugbyShared/work/videoCut2/git/data/grabCut_boxLabels/h3d/';

if(~exist('dir1','var')),
  dir1 = defaultDir1;
end
if(~exist('dir2','var')),
  dir2 = defaultDir2;
end

if(exist(outDir,'dir')), delete(outDir); end;
if(~exist(outDir,'dir')), mkdir(outDir); end;

fprintf('Printing method 1 options:\n');printOptions(dir1);
fprintf('Printing method 2 options:\n');printOptions(dir2);

fH = fopen([outDir 'info.txt'],'w');
fprintf(fH,'Comparing method1 = %s with method2 = %s\n',dir1,dir2);
fprintf(fH,'Method 1 options:\n');
printOptions(dir1,fH);
fprintf(fH,'Method 2 options:\n');
printOptions(dir2,fH);
fclose(fH);


fieldName = ['stats_' scoreString];
tmpLoad = load([dir1 'stats.mat']);
stats1=getfield(tmpLoad.gStats,fieldName);
tmpLoad = load([dir2 'stats.mat']);
stats2=getfield(tmpLoad.gStats,fieldName);

perf1 = [stats1.score];
perf2 = [stats2.score];

validPerf = (perf1 > method1_thresh & perf2 > method2_thresh);
perf1 = perf1(validPerf);
perf2 = perf2(validPerf);
stats1 = stats1(validPerf);
stats2 = stats2(validPerf);

assert(numel(perf1)==numel(perf2),...
    'Cannot compare methods as they have diff number of images\n');
for i=1:numel(perf1)
  assert(strcmp(stats1(i).imgName,stats2(i).imgName),'Images dont match\n');  
end

diffScores = perf1-perf2;
[xx,sortIdxs] = sort(diffScores,'ascend');

worstIdxs = sortIdxs(1:numCompare);
bestIdxs = sortIdxs(end:-1:(end-numCompare+1));

fprintf('Printing worst performers of method 1 relative to method 2\n');
for i=1:numCompare
  idxWorst = worstIdxs(i);
  fprintf('#%02d: Method1 = %.2f, Method2 = %.2f, (1-2) = %.2f, %s\n',...
      i,perf1(idxWorst),perf2(idxWorst),diffScores(idxWorst),...
      stats1(idxWorst).imgName);
end

fprintf('\nPrinting best performers of method 1 relative to method 2\n');
for i=1:numCompare
  idxBest = bestIdxs(i);
  fprintf('#%02d: Method1 = %.2f, Method2 = %.2f, (1-2) = %.2f, %s\n',...
      i,perf1(idxBest),perf2(idxBest),diffScores(idxBest),...
      stats1(idxBest).imgName);
end

if(outputSegEnable)
  tmpLoad = load([dir1 'testOpts.mat']);
  if(~exist('imgDir','var'))
    imgDir = tmpLoad.testOpts.imgDir;
  end
  if(~exist('labelDir','var'))
    labelDir = tmpLoad.testOpts.labelDir;
  end
  for i=1:numCompare
    idxWorst = worstIdxs(i);
    imgEntry = stats1(idxWorst).imgName;
    imgName = grabCut_trained.dataset.extractImgName(imgEntry);
    img = im2double(imread([imgDir imgName]));
    objectId = grabCut_trained.dataset.extractObjectIdentifier(imgEntry);
    labels = imread([labelDir objectId '.png']);
    img = grabCut_trained.helpers.overlayBox(img,labels);
    seg1 = imread([dir1 'segs/' objectId '.png']);
    seg1 = grabCut_trained.helpers.overlaySeg(img,seg1);
    imwrite(seg1,[outDir sprintf('worst%02d_method1_%s.jpg',i,objectId)]);
    seg2 = imread([dir2 'segs/' objectId '.png']);
    seg2 = grabCut_trained.helpers.overlaySeg(img,seg2);
    imwrite(seg2,[outDir sprintf('worst%02d_method2_%s.jpg',i,objectId)]);
  end

  for i=1:numCompare
    idxBest = bestIdxs(i);
    imgEntry = stats1(idxBest).imgName;
    imgName = grabCut_trained.dataset.extractImgName(imgEntry);
    img = im2double(imread([imgDir imgName]));
    objectId = grabCut_trained.dataset.extractObjectIdentifier(imgEntry);
    labels = imread([labelDir objectId '.png']);
    img = grabCut_trained.helpers.overlayBox(img,labels);
    seg1 = imread([dir1 'segs/' objectId '.png']);
    seg1 = grabCut_trained.helpers.overlaySeg(img,seg1);
    imwrite(seg1,[outDir sprintf('best%02d_method1_%s.jpg',i,objectId)]);
    seg2 = imread([dir2 'segs/' objectId '.png']);
    seg2 = grabCut_trained.helpers.overlaySeg(img,seg2);
    imwrite(seg2,[outDir sprintf('best%02d_method2_%s.jpg',i,objectId)]);
  end

end

function printOptions(dir,fOut)

if(~exist('fOut','var')), fOut=1; end;

fH = fopen([dir 'trainParams.txt']);
while 1
  tline = fgetl(fH);
  if ~ischar(tline), break, end
  fprintf(fOut,'%s\n',tline);
end
fclose(fH);
fprintf('\n');
