function vizMethod(dir1)

cwd=miscFns.extractDirPath(mfilename('fullpath'));

defaultDir1 = ['/data/titus_scratch/varun/iccv2011/results/test/run018/'];

scoreString = 'accuracyOverlap';
numCompare = 100; % number of images, best, worst or random to compare
outputSegEnable = true; % set to true to output comparisons
outDir = [cwd '../../../junk/vizMethodsOutput/'];
ordering = 'random'; % Can be 'worst','best','random'
skipSize = 1; % number of images to skip in the ordering
randSeed = 110; % Change this to generate fresh random numbers

% Assign these to over-ride img and label dirs (useful if running on a machine
% different from the one where results are, for e.g your laptop)

%imgDir = '/home/varun/windows/rugbyShared/work/software/h3d_version1.0/data/people/images/';
%labelDir = '/home/varun/windows/rugbyShared/work/videoCut2/git/data/grabCut_boxLabels/h3d/';

if(~exist('dir1','var')),
  dir1 = defaultDir1;
end

if(exist(outDir,'dir')), delete(outDir); end;
if(~exist(outDir,'dir')), mkdir(outDir); end;

fprintf('Printing method 1 options:\n');printOptions(dir1);

fieldName = ['stats_' scoreString];
tmpLoad = load([dir1 'stats.mat']);
stats1=getfield(tmpLoad.gStats,fieldName);

perf1 = [stats1.score];
idxDisplay = [];
switch(ordering)
  case 'worst'
    [xx,sortIdxs] = sort(perf1,'ascend');
    skipSizeValid = min(skipSize,floor(numel(perf1)/numCompare));
    idxDisplay = sortIdxs(1+skipSizeValid*(0:numCompare-1));
  case 'best'
    [xx,sortIdxs] = sort(perf1,'descend');
    skipSizeValid = min(skipSize,floor(numel(perf1)/numCompare));
    idxDisplay = sortIdxs(1+skipSizeValid*(0:numCompare-1));
  case 'random'
    RandStream.setDefaultStream(RandStream.create('mrg32k3a','Seed',randSeed));
    idxDisplay = grabCut_trained.dataset.COMuniquerand(numCompare,[1 numel(perf1)]);
end

fprintf('Printing %s performers\n',ordering);
fH = fopen([outDir 'tmp.txt'],'w');
for i=1:numCompare
  idxTmp = idxDisplay(i);
  fprintf('#%02d: performance = %.2f, %s\n',...
      i,perf1(idxTmp),stats1(idxTmp).imgName);
  fprintf(fH,'%s\n',stats1(idxTmp).imgName);
end
fclose(fH);
fH = fopen([outDir 'info.txt'],'w');
fprintf(fH,'Result dir used = %s\n',dir1);
printOptions(dir1,fH);
fclose(fH);

if(outputSegEnable)
  tmpLoad = load([dir1 'testOpts.mat']);
  if(~exist('imgDir','var'))
    imgDir = tmpLoad.testOpts.imgDir;
  end
  if(~exist('labelDir','var'))
    labelDir = tmpLoad.testOpts.labelDir;
  end
  for i=1:numCompare
    idxTmp = idxDisplay(i);
    imgEntry = stats1(idxTmp).imgName;
    imgName = grabCut_trained.dataset.extractImgName(imgEntry);
    img = im2double(imread([imgDir imgName]));
    objectId = grabCut_trained.dataset.extractObjectIdentifier(imgEntry);
    labels = imread([labelDir objectId '.png']);
    img = grabCut_trained.helpers.overlayBox(img,labels);
    seg1 = imread([dir1 'segs/' objectId '.png']);
    seg1 = grabCut_trained.helpers.overlaySeg(img,seg1);
    seg1 = miscFns.mex_drawText(im2uint8(seg1),int32(20),sprintf('Score = %.2f',perf1(idxTmp)));
    imwrite(seg1,[outDir sprintf('%s%02d_%s.jpg',ordering,i,objectId)]);
  end
end

function printOptions(dir,fOut)

if(~exist('fOut','var')), fOut=1; end;

fH = fopen([dir 'trainParams.txt']);
while 1
  tline = fgetl(fH);
  if ~ischar(tline), break, end
  fprintf(fOut,'%s\n',tline);
end
fclose(fH);
fprintf('\n');
