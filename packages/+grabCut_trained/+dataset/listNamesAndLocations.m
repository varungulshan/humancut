function listNamesAndLocations()
% Lists the names and locations in acquired images into a text file

cwd=miscFns.extractDirPath(mfilename('fullpath'));
dataDir=[cwd '../../../data/kinectData/10Feb2011/'];
listFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_namesAndLocations3_newTests.txt'];
imgExtension = 'png';
maxImgsPerDir = 100000;

%dataDir=[cwd '../../../data/kinectData/junk/'];
%dataDir=[cwd '../../../data/kinectData/firstAttempt/'];
%dataDir=[cwd '../../../data/kinectData/10Feb2011/'];
%listFile=[cwd '../../../data/grabCutLists/kinectData_firstAttempt.txt'];
%listFile=[cwd '../../../data/grabCutLists/kinectData_junk.txt'];
%gtDir=[cwd '../../../data/gt_kinectData/firstAttempt/'];

if(exist(listFile,'file'))
  msg=sprintf('Do you want to overwrite list file: \n%s?(y/n)[n]: ',listFile);
  inp=input(msg,'s');
  if(~strcmp(inp,'y'))
    return;
  end;
end

dirNames = getDirNames(dataDir);

fH = fopen(listFile,'w');

sscanfPattern = '%s_%s_%s_%d';
for i=1:length(dirNames)
  splits = regexp(dirNames{i},'_','split');
  assert(numel(splits)==4);
  person = splits{1};
  location = splits{2};
  fprintf(fH,'%s %s\n',person,location);
end

fclose(fH);

function dirNames = getDirNames(dataDir)

files = dir(dataDir);
dirNames = {};
for i=1:length(files)
  if(files(i).isdir)
    if(~strcmp(files(i).name,'.') && ~strcmp(files(i).name,'..'))
      dirNames{end+1}=[files(i).name '/'];
    end
  end
end
