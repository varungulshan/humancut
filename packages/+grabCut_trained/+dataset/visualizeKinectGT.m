function visualizeKinectGT()

cwd=miscFns.extractDirPath(mfilename('fullpath'));

imgList=[cwd '../../../data/grabCutLists/tmp.txt'];
imgDir=['/data/adam2/varun/iccv2011/data/kinectData/10Feb2011/'];
gtDir = ['/data/adam2/varun/iccv2011/data/gt_kinectData/10Feb2011/'];
outDir = [cwd '../../../junk/vizGT/'];

%imgList=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_1.txt'];
%imgList=[cwd '../../../data/grabCutLists/kinectData_junk.txt'];
%imgDir=[cwd '../../../data/kinectData/junk/'];
%imgDir=[cwd '../../../data/kinectData/10Feb2011/'];
%gtDir = ['/data/adam2/varun/iccv2011/data/gt_kinectData/firstAttempt/'];
%outDir = ['/data/adam2/varun/iccv2011/data/gt_kinectData/firstAttempt_viz/'];
%outDir = ['/data/adam2/varun/iccv2011/data/gt_kinectData/10Feb2011_viz/'];

if(exist(outDir,'dir')),
  msg=sprintf('Overwrite visualized gt in %s?(y/n)[n]\n',outDir);
  inp=input(msg,'s');
  if(~strcmp(inp,'y')),
    return;
  end
else
  mkdir(outDir);
end

images = textread(imgList,'%s');

cmapLabels = jet(7);

for i = 1:length(images)
  fprintf('Visualizing gt for: %s\n',images{i});
  objectId = grabCut_trained.dataset.extractObjectIdentifier(images{i});
  gtFile = [gtDir objectId '.png'];
  imgFile = [imgDir images{i}];
  overlay = createOverlay(gtFile,imgFile);
  imwrite(overlay,[outDir objectId '.png']);
end

function overlay = createOverlay(gtFile,imgFile)

gt = imread(gtFile);
gt(gt==128)=0;
img = im2double(imread(imgFile));
%overlay = grabCut_trained.helpers.overlaySeg(img,gt);
overlay = grabCut_trained.helpers.overlaySeg_single(img,gt,5);
