function createTrainTestSplit_kinect()
% Creates train test splits by name and location for the kinect dataset


[listFile,trainFile,testFile,trainSpecification,testSpecification,skipSize] = ...
   grabCut_trained.dataset.kinectSplitSettings('setting_4_split3'); 

if(exist(trainFile,'file'))
  msg=sprintf('Do you want to overwrite training list file: \n%s?(y/n)[n]: ',...
              trainFile);
  inp=input(msg,'s');
  if(~strcmp(inp,'y'))
    return;
  end;
end

if(exist(testFile,'file'))
  msg=sprintf('Do you want to overwrite testing list file: \n%s?(y/n)[n]: ',...
              testFile);
  inp=input(msg,'s');
  if(~strcmp(inp,'y'))
    return;
  end;
end

imgNames = textread(listFile,'%s');
imgNames = imgNames(1:skipSize:end);
names = cell(1,numel(imgNames));
locations = cell(1,numel(imgNames));

for i=1:numel(imgNames)
  splits = regexp(imgNames{i},'_','split');
  assert(numel(splits)==5);
  names{i}=splits{1};
  locations{i}=splits{2};
end

[trainNames,trainLocations] = textread(trainSpecification,'%s %s');
[testNames,testLocations] = textread(testSpecification,'%s %s');

fTrain = fopen(trainFile,'w');
fTest = fopen(testFile,'w');
numTrain = 0;
numTest = 0;
numThrown = 0;
for i=1:numel(imgNames)
  if(findNameAndLocation(names{i},locations{i},trainNames,trainLocations))
    fprintf(fTrain,'%s\n',imgNames{i});numTrain=numTrain+1;
  elseif(findNameAndLocation(names{i},locations{i},testNames,testLocations))
    fprintf(fTest,'%s\n',imgNames{i});numTest=numTest+1;
  else
    numThrown = numThrown + 1;
  end
end
fclose(fTrain);
fclose(fTest);

fprintf('Total of %d images scanned, train size = %d, test size = %d, thrown away = %d\n',...
         numel(imgNames),numTrain,numTest,numThrown);

function found = findNameAndLocation(name,location,names,locations)

found = false;
for i=1:length(names)
  if(strcmp(names{i},name) && strcmp(locations{i},location)),
    found = true; return;
  end
end
