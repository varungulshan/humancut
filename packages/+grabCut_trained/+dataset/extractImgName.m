function imgName = extractImgName(fullName)

findColons = strfind(fullName,';');
assert(length(findColons)<=1);

if(isempty(findColons)), imgName = fullName;
else
  imgName = fullName(1:findColons(1)-1);
end
