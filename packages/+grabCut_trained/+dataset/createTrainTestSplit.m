function createTrainTestSplit()

cwd=miscFns.extractDirPath(mfilename('fullpath'));
listFile=[cwd '../../../data/h3d_useCaseLists/type1And2_type1.txt'];
trainFile=[cwd '../../../data/grabCutLists/h3d_train_1.txt'];
testFile=[cwd '../../../data/grabCutLists/h3d_test_1.txt'];

trainFraction = 0.5;

%listFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_1.txt'];

if(exist(trainFile,'file'))
  msg=sprintf('Do you want to overwrite training list file: \n%s?(y/n)[n]: ',...
              trainFile);
  inp=input(msg,'s');
  if(~strcmp(inp,'y'))
    return;
  end;
end

if(exist(testFile,'file'))
  msg=sprintf('Do you want to overwrite testing list file: \n%s?(y/n)[n]: ',...
              testFile);
  inp=input(msg,'s');
  if(~strcmp(inp,'y'))
    return;
  end;
end

imgNames = textread(listFile,'%s');
rand('twister',100);
numTrain = round(trainFraction * length(imgNames));

[trainIds,xx]=grabCut_trained.dataset.COMuniquerand(numTrain,[1 length(imgNames)]);
testIds = setdiff([1:length(imgNames)],trainIds);

fH=fopen(trainFile,'w');
for i=trainIds
  fprintf(fH,'%s\n',imgNames{i}); 
end
fclose(fH);

fH=fopen(testFile,'w');
for i=testIds
  fprintf(fH,'%s\n',imgNames{i}); 
end
fclose(fH);
