function cleanUpGT()

cwd=miscFns.extractDirPath(mfilename('fullpath'));

imgList=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_3_newTests.txt'];
registrationMask = [cwd 'registration.png'];
inGtDir = ['/data/adam2/varun/iccv2011/data/gt_kinectData/10Feb2011/'];
outGtDir = ['/data/adam2/varun/iccv2011/data/gt_kinectData/10Feb2011_cleanUp1/'];
insideFG_border = 1; % width inside fg to declare unknown
outsideFG_border = 1; % width outside fg to declare unknown
outsideFG_unknownCheck = 12; % width outside fg to declare unknown if its depth=0
% is used for getting the hair correctly. would declare background as unknown
% when the background has depth 0

if(exist(outGtDir,'dir')),
  msg=sprintf('Do you want to overwrite gt dir: \n%s?(y/n)[n]: ',outGtDir);
  inp=input(msg,'s');
  if(~strcmp(inp,'y'))
    return;
  end;
else
  mkdir(outGtDir);
end

images = textread(imgList,'%s');
regMask = imread(registrationMask);

for i = 1:length(images)
  fprintf('Cleaning up gt for: %s\n',images{i});
  objectId = grabCut_trained.dataset.extractObjectIdentifier(images{i});
  gtFile = [inGtDir objectId '.png'];
  newGt = cleanUpGt(gtFile,regMask,insideFG_border,outsideFG_border,outsideFG_unknownCheck);
  imwrite(newGt,[outGtDir objectId '.png']);
end

function newGt = cleanUpGt(gtFile,regMask,insideFG_border,outsideFG_border,...
                           outsideFG_unknownCheck)

gt=imread(gtFile);
newGt = regMask;

newGt(gt==255)=255;

tmpMask = (newGt==255);
stEl=strel('disk',insideFG_border);
inner=imerode(tmpMask,stEl);
maskInner = tmpMask & (~inner);

tmpMask = (newGt~=255);
stEl=strel('disk',outsideFG_border);
outer=imerode(tmpMask,stEl);
maskOuter = tmpMask & (~outer);

tmpMask = (newGt~=255);
stEl=strel('disk',outsideFG_unknownCheck);
outer=imerode(tmpMask,stEl);
maskUnknown = tmpMask & (~outer) & (gt==128);

mask = maskInner|maskOuter|maskUnknown;

newGt(mask)=128;
