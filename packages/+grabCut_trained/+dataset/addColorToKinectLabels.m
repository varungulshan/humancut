function addColorToKinectLabels()

cwd=miscFns.extractDirPath(mfilename('fullpath'));
dataDir=[cwd '../../../data/kinectData/10Feb2011/'];
imgExtension = 'png';
colormap=repmat([0 0 1],[256 1]); % Blue everywhere
colormap(1,:)=[0 0 0];
colormap(2,:)=[1 0 0];
colormap(256,:)=[1 1 1];
%dataDir=[cwd '../../../data/kinectData/junk/'];
%dataDir=[cwd '../../../data/kinectData/firstAttempt/'];
%dataDir=[cwd '../../../data/kinectData/10Feb2011/'];
%listFile=[cwd '../../../data/grabCutLists/kinectData_firstAttempt.txt'];
%gtDir=[cwd '../../../data/gt_kinectData/firstAttempt/'];

dirNames = getDirNames(dataDir);

for i=1:length(dirNames)
  curDir = [dataDir dirNames{i}];
  fprintf('Scanning folder: %s\n',dirNames{i});
  idxImg = 0;
  allFiles = dir([curDir 'label*.' imgExtension]);
  for j=1:length(allFiles)
    labelFile = [curDir allFiles(j).name];
    labelImg = imread(labelFile);
    imwrite(labelImg,colormap,labelFile,'png');
  end
end

function dirNames = getDirNames(dataDir)

files = dir(dataDir);
dirNames = {};
for i=1:length(files)
  if(files(i).isdir)
    if(~strcmp(files(i).name,'.') && ~strcmp(files(i).name,'..'))
      dirNames{end+1}=[files(i).name '/'];
    end
  end
end
