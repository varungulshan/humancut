function createKinectDataset()

cwd=miscFns.extractDirPath(mfilename('fullpath'));
dataDir=[cwd '../../../data/kinectData/10Feb2011_newTests/'];
listFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_3_newTests.txt'];
gtDir=['/data/adam2/varun/iccv2011/data/gt_kinectData/10Feb2011/'];
imgExtension = 'png';
maxImgsPerDir = 100000;

%dataDir=[cwd '../../../data/kinectData/junk/'];
%dataDir=[cwd '../../../data/kinectData/firstAttempt/'];
%dataDir=[cwd '../../../data/kinectData/10Feb2011/'];
%listFile=[cwd '../../../data/grabCutLists/kinectData_firstAttempt.txt'];
%listFile=[cwd '../../../data/grabCutLists/kinectData_junk.txt'];
%gtDir=[cwd '../../../data/gt_kinectData/firstAttempt/'];

if(exist(listFile,'file'))
  msg=sprintf('Do you want to overwrite list file: \n%s?(y/n)[n]: ',listFile);
  inp=input(msg,'s');
  if(~strcmp(inp,'y'))
    return;
  end;
end

if(exist(gtDir,'dir'))
  msg=sprintf('Do you want to overwrite gtDir: \n%s?(y/n)[n]: ',gtDir);
  inp=input(msg,'s');
  if(~strcmp(inp,'y'))
    return;
  end;
else
  mkdir(gtDir);
end

dirNames = getDirNames(dataDir);

fH = fopen(listFile,'w');

sscanfPattern = ['image_%d.' imgExtension];
for i=1:length(dirNames)
  curDir = [dataDir dirNames{i}];
  fprintf('Scanning folder: %s\n',dirNames{i});
  idxImg = 0;
  allFiles = dir([curDir 'image*.' imgExtension]);
  uBound = min(maxImgsPerDir,length(allFiles));
  for j=1:uBound
    idxImg = sscanf(allFiles(j).name,sscanfPattern);
    imgFile = [curDir allFiles(j).name];
    labelFile = [curDir sprintf('labels_%05d.png',idxImg)];
    if(exist(labelFile,'file')==0), continue; end
    % If label file has been deleted, then move on
    labelImg = imread(labelFile);
    partialImgName = [dirNames{i} allFiles(j).name];
    writeEntriesAndCreateGT(fH,partialImgName,labelImg,gtDir);
  end
end

fclose(fH);

function dirNames = getDirNames(dataDir)

files = dir(dataDir);
dirNames = {};
for i=1:length(files)
  if(files(i).isdir)
    if(~strcmp(files(i).name,'.') && ~strcmp(files(i).name,'..'))
      dirNames{end+1}=[files(i).name '/'];
    end
  end
end

function writeEntriesAndCreateGT(fH,imgFile,labelImg,gtDir)

uniqueLabels = unique(labelImg(:));
% labelImg = 0 is bg, = 255 is unknown, rest are objects

numUsers = nnz(uniqueLabels~=0 & uniqueLabels~=255);
%assert(numUsers>=1);

mask = zeros(size(labelImg),'uint8');
mask(labelImg==255)=128;
for i=1:numUsers
  objMask = mask;
  objMask(labelImg==i)=255;
  if(i>1), imgSuffix=sprintf(';%d',i-1);
  else, imgSuffix=''; end;
  imgName = [imgFile imgSuffix];
  objectIdentifier = grabCut_trained.dataset.extractObjectIdentifier(imgName);
  fprintf(fH,[imgName '\n']);
  imwrite(objMask,[gtDir objectIdentifier '.png']);
end
