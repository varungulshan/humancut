function visualizeKinectGT()

cwd=miscFns.extractDirPath(mfilename('fullpath'));

trainCmd = 'testBench.getTests_grabCut_kinect10Feb2011_3_train_1()';
testCmd = 'testBench.getTests_grabCut_kinect10Feb2011_3_test_2_newTestsAdd()';

imgDir=['/data2/varun/iccv2011/data/kinectData/10Feb2011/'];
gtDir = ['/data2/varun/iccv2011/data/gt_kinectData/10Feb2011_cleanUp1/'];
outImgDir = ['/data2/varun/luborData/kinectImages/'];
outGtDir = ['/data2/varun/luborData/kinectGT/'];

if(exist(outImgDir,'dir')),
  msg=sprintf('Overwrite %s?(y/n)[n]\n',outImgDir);
  inp=input(msg,'s');
  if(~strcmp(inp,'y')),
    return;
  end
else
  mkdir(outImgDir);
end

if(exist(outGtDir,'dir')),
  msg=sprintf('Overwrite %s?(y/n)[n]\n',outGtDir);
  inp=input(msg,'s');
  if(~strcmp(inp,'y')),
    return;
  end
else
  mkdir(outGtDir);
end

trainOpts = eval(trainCmd);
testOpts = eval(testCmd);

mkdir([outImgDir 'train/']);
mkdir([outImgDir 'test/']);

saveImages(trainOpts,[outImgDir 'train/'],outGtDir,gtDir);
saveImages(testOpts,[outImgDir 'test/'],outGtDir,gtDir);

function saveImages(opts,outImgDir,outGtDir,gtDir)

images = textread(opts.imgList,'%s');
imgDir = opts.imgDir;

for i = 1:length(images)
  fprintf('Copying: %s\r',images{i});
  objectId = grabCut_trained.dataset.extractObjectIdentifier(images{i});
  gtFile = [gtDir objectId '.png'];
  imgFile = [imgDir images{i}];
  copyfile(imgFile,[outImgDir objectId]);
  copyfile(gtFile,[outGtDir objectId]);
end
