function [listFile,trainFile,testFile,trainSpecification,testSpecification,skipSize] = ...
   kinectSplitSettings(settingType)

cwd=miscFns.extractDirPath(mfilename('fullpath'));

switch(settingType)
  case 'setting_1',
    listFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_1.txt'];
    trainFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_1_train_1.txt'];
    testFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_1_test_1.txt'];
    trainSpecification = ...
        [cwd '../../../data/grabCutLists/kinectData_10Feb2011_trainSpecification_1.txt'];
    testSpecification = ...
        [cwd '../../../data/grabCutLists/kinectData_10Feb2011_testSpecification_1.txt'];
    skipSize = 1; % no of images to skip in middle 
    % (useful for creating small debuggable datasets)
  case 'setting_2',
    listFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_1.txt'];
    trainFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_1_train_small.txt'];
    testFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_1_test_small.txt'];
    trainSpecification = ...
        [cwd '../../../data/grabCutLists/kinectData_10Feb2011_trainSpecification_1.txt'];
    testSpecification = ...
        [cwd '../../../data/grabCutLists/kinectData_10Feb2011_testSpecification_1.txt'];
    skipSize = 20; % no of images to skip in middle 
    % (useful for creating small debuggable datasets)

  case 'setting_3',
    listFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_2.txt'];
    trainFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_2_train_1.txt'];
    testFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_2_test_1.txt'];
    trainSpecification = ...
        [cwd '../../../data/grabCutLists/kinectData_10Feb2011_trainSpecification_2.txt'];
    testSpecification = ...
        [cwd '../../../data/grabCutLists/kinectData_10Feb2011_testSpecification_2.txt'];
    skipSize = 1; % no of images to skip in middle 
 
  case 'setting_4',
    listFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_3.txt'];
    trainFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_3_train_1.txt'];
    testFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_3_test_1.txt'];
    trainSpecification = ...
        [cwd '../../../data/grabCutLists/kinectData_10Feb2011_trainSpecification_3.txt'];
    testSpecification = ...
        [cwd '../../../data/grabCutLists/kinectData_10Feb2011_testSpecification_3.txt'];
    skipSize = 1; % no of images to skip in middle 

  case 'setting_4_split1',
    listFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_3_train_1.txt'];
    trainFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_3_train_1_train1.txt'];
    testFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_3_train_1_validate1.txt'];
    trainSpecification = ...
        [cwd '../../../data/grabCutLists/kinectData_10Feb2011_trainSpecification_3_train1.txt'];
    testSpecification = ...
        [cwd '../../../data/grabCutLists/kinectData_10Feb2011_trainSpecification_3_validate1.txt'];
    skipSize = 1; % no of images to skip in middle 

  case 'setting_4_split2',
    listFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_3_train_1.txt'];
    trainFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_3_train_1_train2.txt'];
    testFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_3_train_1_validate2.txt'];
    trainSpecification = ...
        [cwd '../../../data/grabCutLists/kinectData_10Feb2011_trainSpecification_3_train2.txt'];
    testSpecification = ...
        [cwd '../../../data/grabCutLists/kinectData_10Feb2011_trainSpecification_3_validate2.txt'];
    skipSize = 1; % no of images to skip in middle 

  case 'setting_4_split3',
    listFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_3_train_1.txt'];
    trainFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_3_train_1_train3.txt'];
    testFile=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_3_train_1_validate3.txt'];
    trainSpecification = ...
        [cwd '../../../data/grabCutLists/kinectData_10Feb2011_trainSpecification_3_train3.txt'];
    testSpecification = ...
        [cwd '../../../data/grabCutLists/kinectData_10Feb2011_trainSpecification_3_validate3.txt'];
    skipSize = 1; % no of images to skip in middle 
              
  otherwise
    error('Invalid setting %s\n',settingType);
end
