Steps to recreate dataset list files/boxes/gt after acquring new data:

1. Call createKinectDataset, make sure you set the listFile correctly. You can overwrite gtDir as it is exactly a subset of current one (unless you remove the current dataset).
Command: grabCut_trained.dataset.createKinectDataset

2. Create box annotations for new dataset (again overwrite the outLabelDir if you want):
Command: grabCut_trained.dataset.createBoxesFromGT

3. Create cleaned up gt (again overwrite gtDir): 
Command: grabCut_trained.dataset.cleanUpGT

4. List the new names and locations using listNamesAndLocationsCommand (dont overwrite files in this case).

5. Create the train and test specifications manually by editing files in git/data/grabCutLists

6. Use the specifications to generate list files using code createTrainTestSplit_kinect

7. Create files in packages/+testBench for new train and test splits.
