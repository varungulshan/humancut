function createBoxesFromGT()

cwd=miscFns.extractDirPath(mfilename('fullpath'));

imgList=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_3_newTests.txt'];
gtDir = ['/data/adam2/varun/iccv2011/data/gt_kinectData/10Feb2011/'];
outLabelDir = ['/data/adam2/varun/iccv2011/data/boxes_kinectData/10Feb2011_1/'];

%gtDir = ['/data/adam2/varun/iccv2011/data/gt_kinectData/firstAttempt/'];

%outLabelDir = [cwd '../../data/grabCut_boxLabels/h3d/'];
%outLabelDir = ['/data/adam2/varun/iccv2011/data/boxes_kinectData/firstAttempt/'];


boxParams.expandX = 0.02; % as fraction of bounding box size
boxParams.expandY = 0.02; % as fraction of bounding box size
boxParams.borderX = (sqrt(2)-1)/2;
boxParams.borderY = (sqrt(2)-1)/2;


if(exist(outLabelDir,'dir')),
  msg=sprintf('Do you want to overwrite labelDir: \n%s?(y/n)[n]: ',outLabelDir);
  inp=input(msg,'s');
  if(~strcmp(inp,'y'))
    return;
  end;
else
  mkdir(outLabelDir);
end

images = textread(imgList,'%s');

cmapLabels = jet(7);

for i = 1:length(images)
  fprintf('Finding box for: %s\n',images{i});
  objectId = grabCut_trained.dataset.extractObjectIdentifier(images{i});
  gtFile = [gtDir objectId '.png'];
  labels = createLabelsFromGT(gtFile,boxParams);
  imwrite(labels,cmapLabels,[outLabelDir objectId '.png'],'png');
end

function labelImg = createLabelsFromGT(gtFile,boxParams)

gt = imread(gtFile);
labelImg = 6*ones(size(gt),'uint8');

[y,x] = find(gt==255);

xMin = min(x); xMax = max(x);
yMin = min(y); yMax = max(y);

w = xMax-xMin+1;
h = yMax-yMin+1;

boxParams.expandX = round(boxParams.expandX*w);
boxParams.expandY = round(boxParams.expandY*h);

innerX_min = xMin - boxParams.expandX;
innerX_max = xMax + boxParams.expandX;
innerY_min = yMin - boxParams.expandY;
innerY_max = yMax + boxParams.expandY;

boxParams.borderX = round(boxParams.borderX*(innerX_max-innerX_min+1));
boxParams.borderY = round(boxParams.borderY*(innerY_max-innerY_min+1));

outerX_min = innerX_min - boxParams.borderX;
outerX_max = innerX_max + boxParams.borderX;
outerY_min = innerY_min - boxParams.borderY;
outerY_max = innerY_max + boxParams.borderY;

[imgH,imgW]=size(labelImg);

innerX_min = max(1,innerX_min);innerX_max=min(imgW,innerX_max);
innerY_min = max(1,innerY_min);innerY_max=min(imgH,innerY_max);

outerX_min = max(1,outerX_min);outerX_max=min(imgW,outerX_max);
outerY_min = max(1,outerY_min);outerY_max=min(imgH,outerY_max);

labelImg(outerY_min:outerY_max,outerX_min:outerX_max)=2;
labelImg(innerY_min:innerY_max,innerX_min:innerX_max)=3;
