function visualizeBoxes()
% To visualize dataset images with bounding boxes

cwd=miscFns.extractDirPath(mfilename('fullpath'));

imgList=[cwd '../../../data/grabCutLists/h3d_test_1.txt'];
imgDir=[cwd '../../../../../software/h3d_version1.0/data/people/images/'];
labelDir = [cwd '../../../data/grabCut_boxLabels/h3d/'];
outDir = [cwd '../../../results/visualizeBoxes/h3d/'];

%imgList=[cwd '../../../data/grabCutLists/h3d_train_1.txt'];
%imgList=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_1.txt'];
%imgList=[cwd '../../../data/grabCutLists/kinectData_junk.txt'];

%imgDir=[cwd '../../../data/kinectData/junk/'];
%imgDir=[cwd '../../../data/kinectData/10Feb2011/'];
%imgDir=[cwd '../../../../../software/h3d_version1.0/data/people/images/'];

%labelDir = [cwd '../../../data/grabCut_boxLabels/h3d/'];
%labelDir =['/data/adam2/varun/iccv2011/data/boxes_kinectData/10Feb2011_1/'];

%outDir = [cwd '../../../results/visualizeBoxes/kinect10Feb2011_subsample/'];
%outDir = [cwd '../../../results/visualizeBoxes/h3d/'];
%outDir = ['/data/adam2/varun/iccv2011/data/gt_kinectData/firstAttempt_viz/'];
skipImages=1;

if(exist(outDir,'dir')),
  msg=sprintf('Overwrite visualized labels in %s?(y/n)[n]\n',outDir);
  inp=input(msg,'s');
  if(~strcmp(inp,'y')),
    return;
  end
else
  mkdir(outDir);
end

images = textread(imgList,'%s');

cmapLabels = jet(7);

for i = 1:skipImages:length(images)
  fprintf('Visualizing labels for: %s\n',images{i});
  objectId = grabCut_trained.dataset.extractObjectIdentifier(images{i});
  labelFile = [labelDir objectId '.png'];
  imgName = grabCut_trained.dataset.extractImgName(images{i});
  imgFile = [imgDir imgName];
  overlay = createOverlay(labelFile,imgFile);
  imwrite(overlay,[outDir objectId '.jpg']);
end

function overlay = createOverlay(labelFile,imgFile)

labelImg = imread(labelFile);
img = im2double(imread(imgFile));
tmpSeg = zeros(size(labelImg),'uint8');
tmpSeg(labelImg==3)=255;
overlay = grabCut_trained.helpers.overlaySeg(img,tmpSeg);
