function seg = testOnImg(img,labels,trainData,debugOpts)

seg = zeros(size(labels),'uint8');
seg(labels==3)=255;
