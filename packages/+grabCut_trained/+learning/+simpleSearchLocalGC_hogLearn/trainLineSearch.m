function trainLineSearch(opts,trainOutDir,testBenchOpts)

import grabCut_trained.learning.simpleSearchLocalGC_hogLearn.*;

assert(strcmp(opts.trainMethod,'localGrabCut_simpleSearch_hogLearn'));

trainOpts = makeOpts(opts.trainMethod_optsString);
save([trainOutDir 'trainOpts.mat'],'trainOpts');
fH=fopen([trainOutDir 'trainOpts.txt'],'w');
miscFns.printClass(trainOpts.grabCutOpts,fH);
for i=1:length(trainOpts.vary)  
  fprintf(fH,'Vary field %d: %s in %s\n',i,trainOpts.vary(i).fieldName,...
          mat2str(cell2mat(trainOpts.vary(i).fieldValues)));
end
%miscFns.printStructure(trainOpts.vary,fH);
fclose(fH);

currentGcOpts = trainOpts.grabCutOpts;
images = textread(testBenchOpts.imgList,'%s');
imgDir = testBenchOpts.imgDir;
labelDir = testBenchOpts.labelDir;

if(trainOpts.maxTrain > 0)
  numAccept = min(trainOpts.maxTrain,numel(images));
  numReject = numel(images)-numAccept;
  reset(RandStream.getDefaultStream);
  if(numAccept>numReject)
    idxReject = grabCut_trained.dataset.COMuniquerand(numReject,...
                [1 numel(images)]);
    idxAccept = setdiff([1:numel(images)],idxReject);
  else
    idxAccept = grabCut_trained.dataset.COMuniquerand(numAccept,...
                [1 numel(images)]);
  end
else
  idxAccept = [1:numel(images)];
end

images = images(idxAccept);
% Create a new image list for these subset of images
newImgList = [trainOutDir 'imgList_subsetValidated.txt'];
fH = fopen(newImgList,'w');
for i=1:length(images)
  fprintf(fH,'%s\n',images{i});
end
fclose(fH);
testBenchOpts.imgList = newImgList;

fSummary = fopen([trainOutDir 'trainSummary.txt'],'w');
vary = trainOpts.vary;
scoreString = trainOpts.scoreString;
hogLearnData = trainOpts.hogLearnData;

for i=1:length(vary)
  fieldName=vary(i).fieldName;
  fieldValues=vary(i).fieldValues;
  fprintf('\nVarying %s\n\n',fieldName);

  tryOpts=cell(1,numel(fieldValues));
  for j=1:numel(fieldValues)
    tmpOpts = currentGcOpts;
    cmd=['tmpOpts.' fieldName '=fieldValues{j};'];
    eval(cmd);
    tryOpts{j}=tmpOpts;
  end
  tryDir = cell(1,numel(fieldValues));
  for j=1:numel(fieldValues)
    tryDir{j} = [trainOutDir sprintf('vary-%s-%s/',...
                 fieldName,mat2str(fieldValues{j}))];
    mkdir(tryDir{j});
    mkdir([tryDir{j} 'debug/']);
    mkdir([tryDir{j} 'segs/']);
    testOpts = testBenchOpts;
    save([tryDir{j} 'testOpts.mat'],'testOpts');
  end

  [varyIdxs,imgIdxs]=meshgrid([1:numel(fieldValues)],[1:length(images)]);
  parfor j=1:numel(imgIdxs)
    imgIdx = imgIdxs(j);
    varyIdx = varyIdxs(j);
    imgName = grabCut_trained.dataset.extractImgName(images{imgIdx});
    imgPath = [imgDir imgName];
    objectId = grabCut_trained.dataset.extractObjectIdentifier(images{imgIdx});
    labelPath = [labelDir objectId '.png'];
    tmpData=struct();
    tmpData.grabCutOpts = tryOpts{varyIdx};
    tmpData.hogLearnData = hogLearnData;
    debugOpts = struct();
    debugOpts.debugLevel = opts.debugLevel;
    debugOpts.debugDir = [tryDir{varyIdx} 'debug/'];
    debugOpts.debugPrefix = [objectId '_'];
    segOut = testOnImg(imread(imgPath),imread(labelPath),tmpData,debugOpts);
    imwrite(segOut,[tryDir{varyIdx} 'segs/' objectId '.png']);
  end
  
  parfor j=1:numel(fieldValues)
    testBench.computeStats_grabCut(tryDir{j},opts.evalOpts_string);
  end

  performance = zeros(1,numel(fieldValues));
  stdErrs = zeros(1,numel(fieldValues));

  for j=1:numel(fieldValues)
    tmpLoad = load([tryDir{j} 'stats.mat']);
    gStats = tmpLoad.gStats;
    cmd=['gStats.stats_' scoreString];
    stats=eval(cmd);
    scores=[stats.score];
    stdErrs(j)=std(scores,1)/sqrt(length(scores));
    performance(j)=mean(scores);
  end

  [maxPerf,bestJ]=max(performance);
  currentGcOpts = tryOpts{bestJ};

  % --- write out summmary ----
  fprintf(fSummary,'\nVaried: %s \n',fieldName); %in [',fieldName);
  for j=1:numel(fieldValues)
    fprintf(fSummary,'%s = %.2f pm %.2f, %s = %s\n',scoreString,performance(j),...
        stdErrs(j),fieldName,mat2str(fieldValues{j}));
  end
  fprintf(fSummary,'\nBest %s = %s, Performance(%s) = %.2f pm %.2f\n',fieldName,...
        mat2str(fieldValues{bestJ}),... 
        scoreString,performance(bestJ),...
        stdErrs(bestJ));
end

fclose(fSummary);
bestOpts = currentGcOpts;
save([trainOutDir 'bestOpts.mat'],'bestOpts','hogLearnData');
fH = fopen([trainOutDir 'bestOpts.txt'],'w');
miscFns.printClass(bestOpts,fH);
fclose(fH);
