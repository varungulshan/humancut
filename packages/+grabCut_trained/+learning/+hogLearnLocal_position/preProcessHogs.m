function ppInfo = preProcessHogs(hogs,trainOpts)
% TO apply pre-processing such as dimensionality reduction

switch(trainOpts.hogPreProcess)
  case 'dictionarySplit'
    ppInfo = sparseDictionaryProcess_bySplit(hogs,trainOpts.hogPreProcessOpts);
  case {'dictionarySplit_saved','dictionarySingle_saved',...
        'dictionarySingle_saved_normOutput'}
  % these two cases can be handled similiary because all we need
  % to do is load a file, the loaded contents will be different
    ppInfo = singleSparseDictionaryProcess_saved(hogs,trainOpts.hogPreProcessOpts);
  case 'none',
    ppInfo = [];
  otherwise
    error('Invalid pre process method: %s\n',trainOpts.hogPreProcess);
end

function ppInfo = singleSparseDictionaryProcess_saved(hogs,opts)

tmpLoad = load(opts.ppFile);
ppInfo = tmpLoad.ppInfo;

function ppInfo = sparseDictionaryProcess_bySplit(allHogs,opts)

dictionaryParams = opts.dictionaryParams;
reset(RandStream.getDefaultStream); % To reproduce randomness

ppInfo.dictionary = cell(numel(allHogs),1);

for i=1:numel(allHogs)
  hogs = allHogs{i};
  randomHogs = hogs(:,grabCut_trained.dataset.COMuniquerand(dictionaryParams.K,[1 size(hogs,2)]));
  dictionaryParams.D = randomHogs;
  if(opts.useFasterOne)
    ppInfo.dictionary{i} = mexTrainDL_Memory(hogs,dictionaryParams);
  else
    ppInfo.dictionary{i} = mexTrainDL(hogs,dictionaryParams);
  end
end
