function info = learnPredictor(features,segMasks,trainOpts,debugOpts)

switch(trainOpts.predictorType)
  case 'pegasus'
    info = learnPegasus(features,segMasks,trainOpts.predictorOpts,debugOpts);
  case 'pegasus_faster'
    info = learnPegasusFaster(features,segMasks,trainOpts.predictorOpts,debugOpts);
  case 'liblinear'
    info = learnLibLinear(features,segMasks,trainOpts.predictorOpts,debugOpts);
  case 'liblinear_balanced'
    info = learnLibLinear(features,segMasks,trainOpts.predictorOpts,debugOpts,true);
  otherwise
    error('Invalid predictorType: %s\n',trainOpts.predictorType);
end

function info = learnPegasusFaster(featuresAll,segMasksAll,opts,debugOpts)

info = cell(numel(featuresAll),1);
assert(numel(featuresAll)==numel(segMasksAll),'Inconsisent features and segMasks splits\n');
assert(opts.svmBias==-1,'Bias not supported right now\n');

for iSplit = 1:numel(featuresAll)
  features = featuresAll{iSplit};
  segMasks = segMasksAll{iSplit};
  dimensionality = size(features,1);
  if(opts.svmBias>=0)
    dimensionality = dimensionality+1;
  end
  wT = zeros(size(segMasks,1),dimensionality);
  conditioner = ones(1,dimensionality);
  
  for i=1:size(segMasks,1)
    stTime = clock;
    fprintf('Training %d/%d pixel of segmask:',i,size(segMasks,1));
    labelVector = segMasks(i,:)';
    valid = (labelVector~=0.5);
    tmpFeatures = features(:,valid);
    labelVector = 2*labelVector(valid)-1;

    numSamples = size(tmpFeatures,2);
    lambda = 1/(opts.svmC*numSamples);
    numIters = round(opts.numPasses * numSamples);

    [wT(i,:),xx,wScale] = pegasus.sppega_resumable_fast(...
        tmpFeatures,labelVector,lambda,...
        numIters,conditioner);
    wT(i,:) = wT(i,:)*wScale;
    fprintf(' %d seconds\n',etime(clock,stTime));
  end
  info{iSplit}.wT = wT;
end


function info = learnPegasus(featuresAll,segMasksAll,opts,debugOpts)

info = cell(numel(featuresAll),1);
assert(numel(featuresAll)==numel(segMasksAll),'Inconsisent features and segMasks splits\n');
assert(opts.svmBias==-1,'Bias not supported right now\n');

for iSplit = 1:numel(featuresAll)
  features = featuresAll{iSplit};
  segMasks = segMasksAll{iSplit};
  dimensionality = size(features,1);
  if(opts.svmBias>=0)
    dimensionality = dimensionality+1;
  end
  wT = zeros(size(segMasks,1),dimensionality);
  H = {speye(dimensionality)};
  conditioner = ones(1,dimensionality);
  
  for i=1:size(segMasks,1)
    stTime = clock;
    fprintf('Training %d/%d pixel of segmask:',i,size(segMasks,1));
    labelVector = segMasks(i,:)';
    valid = (labelVector~=0.5);
    tmpFeatures = features(:,valid);
    labelVector = 2*labelVector(valid)-1;

    numSamples = size(tmpFeatures,2);
    lambda = 1/(opts.svmC*numSamples);
    tmpH = H;
    tmpH{1}=tmpH{1}*lambda;
    numIters = round(opts.numPasses * numSamples);

    [wT(i,:),xx] = pegasus.sppega(tmpH,tmpFeatures,labelVector,lambda,...
        numIters,conditioner);
    fprintf(' %d seconds\n',etime(clock,stTime));
  end
  info{iSplit}.wT = wT;
end

function info = learnLibLinear(featuresAll,segMasksAll,opts,debugOpts,balanceData)

info = cell(numel(featuresAll),1);
assert(numel(featuresAll)==numel(segMasksAll),'Inconsisent features and segMasks splits\n');

for iSplit = 1:numel(featuresAll)
  origLiblinearOptions = sprintf('-q -s %d -c %f -B %d',opts.svmType,...
                              opts.svmC,opts.svmBias);
  features = featuresAll{iSplit};
  segMasks = segMasksAll{iSplit};
  dimensionality = size(features,1);
  if(opts.svmBias>=0)
    dimensionality = dimensionality+1;
  end
  wT = zeros(size(segMasks,1),dimensionality);
  
  if(~exist('balanceData','var')), balanceData = false; end
  
  for i=1:size(segMasks,1)
    stTime = clock;
    fprintf('Training %d/%d pixel of segmask:',i,size(segMasks,1));
    labelVector = segMasks(i,:)';
    valid = (labelVector~=0.5);
    tmpFeatures = features(:,valid);
    labelVector = 2*labelVector(valid)-1;
    liblinearOptions = origLiblinearOptions;
    if(balanceData)
      n1 = nnz(labelVector==1);
      n2 = numel(labelVector)-n1;
      w1 = n1*(n1+n2)/(n1^2+n2^2);
      w2 = n2*(n1+n2)/(n1^2+n2^2);
      liblinearOptions = [liblinearOptions sprintf(' -w+1 %f -w-1 %f',...
        w1,w2)]; 
    end
  
    model = liblinear.train(labelVector,tmpFeatures,liblinearOptions,'col');
    wT(i,:) = model.Label(1)*model.w; % the multiplication by label takes
    % care of flip
    assert(abs(model.Label(1))==1,'Unexpected model label output\n');
    fprintf(' %d seconds\n',etime(clock,stTime));
  end
  info{iSplit}.wT = wT;
end
