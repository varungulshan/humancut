function [seg,svmConf] = testOnImg(img,labelImg,trainData,debugOpts)

import grabCut_trained.learning.hogLearnLocal_position.*;

% If labelImg has no fg (happens when automatic detection fails)
% then label all as background

if(~any(labelImg(:)==3)), 
  seg=zeros(size(labelImg),'uint8'); 
  return;
end;

img = im2double(img);
ppInfo = trainData.ppInfo;
trainOpts = trainData.opts.trainOpts;
hogOpts = trainData.opts.hogOpts;
predictorInfo = trainData.predictorInfo;

localSegMaskSize = getLocalSegMaskSize(trainOpts);
[bbox,bboxAspect] = getBoxAspectRatio(labelImg);

[bbox,hogBoxSize] = snapBBoxToSize(bbox,trainOpts,hogOpts);
numCellsH = hogBoxSize(1)/hogOpts.cellSize;
numCellsW = hogBoxSize(2)/hogOpts.cellSize;
segCellSizeH = localSegMaskSize(1)/trainOpts.localHogH;
segCellSizeW = localSegMaskSize(2)/trainOpts.localHogW;
segMaskSize = ceil([numCellsH*segCellSizeH numCellsW*segCellSizeW]);

localImg = cpp.mex_extractBBox(img,int32(bbox-1));
localImg = imresize(localImg,[hogBoxSize(1) hogBoxSize(2)],'bilinear');
hogFull = hogCode.mex_getHog(localImg,hogOpts.cellSize);

if(debugOpts.debugLevel>=10)
  visHog = drawHog(hogFull,hogOpts);
  miscFns.saveDebug(debugOpts,visHog,'hog.png');
  miscFns.saveDebug(debugOpts,imresize(localImg,size(visHog)),'img.jpg');
end
% hogFull will be a H x W x (27+4) matrix, H = number of vertical cells, W = horiz cells
hogFull = selectHogType(hogFull,hogOpts);

hogBBoxes = cell(trainOpts.numVerticalSplits,1);
segBBoxes = cell(trainOpts.numVerticalSplits,1);
numRowsPerSplit = (numCellsH-trainOpts.localHogH+1)/trainOpts.numVerticalSplits;

prevEnd = 0;

for i=1:trainOpts.numVerticalSplits

thisEnd = (round(numRowsPerSplit*i));
yStart = [(prevEnd+1):thisEnd];
prevEnd = thisEnd;
xStart = [1:(numCellsW-trainOpts.localHogW+1)];

[x1,y1] = meshgrid(xStart,yStart);
x1=x1(:)';
y1=y1(:)';
hogBBoxes{i} = zeros(4,numel(x1));
hogBBoxes{i}(1,:) = y1;
hogBBoxes{i}(2,:) = y1+trainOpts.localHogH-1;
hogBBoxes{i}(3,:) = x1;
hogBBoxes{i}(4,:) = x1+trainOpts.localHogW-1;

segBBoxes{i} =zeros(4,numel(x1));
segBBoxes{i}(1,:) = round(1+(y1-1)*segCellSizeH);
segBBoxes{i}(2,:) = segBBoxes{i}(1,:)+localSegMaskSize(1)-1;
segBBoxes{i}(3,:) = round(1+(x1-1)*segCellSizeW);
segBBoxes{i}(4,:) = segBBoxes{i}(3,:)+localSegMaskSize(2)-1;

end

% ------- Apply pre-processing to hogs -------------
hogs = cell(trainOpts.numVerticalSplits,1);
for iSplit = 1:trainOpts.numVerticalSplits
  hogs{iSplit} = zeros(getHogDimensionality(hogOpts,trainOpts),...
      size(hogBBoxes{iSplit},2));
  for i=1:size(hogBBoxes{iSplit},2)
    tmpHog = hogFull(hogBBoxes{iSplit}(1,i):hogBBoxes{iSplit}(2,i),...
                     hogBBoxes{iSplit}(3,i):hogBBoxes{iSplit}(4,i),:);
    hogs{iSplit}(:,i) = tmpHog(:);
  end
end
hogs = applyPreProcess(hogs,trainOpts,ppInfo);

% ------ Apply the predictors -------------
[segPredictions,svmOutputs] = applyPredictor(hogs,trainOpts,predictorInfo);
[outSeg,outConf] = combineLocalPredictions(trainOpts,segPredictions,...
    segBBoxes,svmOutputs,localSegMaskSize,segMaskSize);
seg = snapPredictionToBBox(outSeg,bbox,[size(img,1) size(img,2)]);
svmConf = snapConfidenceToBBox(outConf,bbox,[size(img,1) size(img,2)]);

if(debugOpts.debugLevel > 0)
  overlay = grabCut_trained.helpers.overlaySeg(img,seg);
  overlay = grabCut_trained.helpers.overlayBox(overlay,labelImg);
  miscFns.saveDebug(debugOpts,overlay,'seg.jpg'); 
end

function conf = snapConfidenceToBBox(confPrediction,bbox,imgSize)

bboxH = bbox(4)-bbox(2)+1;
bboxW = bbox(3)-bbox(1)+1;
confPrediction = imresize(confPrediction,[bboxH bboxW],'bilinear');
conf = -1*ones(imgSize);

deltaLeft = max(0,1-bbox(1));
deltaRight = max(0,bbox(3)-imgSize(2));
deltaTop = max(0,1-bbox(2));
deltaBottom = max(0,bbox(4)-imgSize(1));

conf( (bbox(2)+deltaTop):(bbox(4)-deltaBottom), ...
     (bbox(1)+deltaLeft):(bbox(3)-deltaRight)) = confPrediction(...
     (1+deltaTop):(end-deltaBottom),(1+deltaLeft):(end-deltaRight)); 

function seg = snapPredictionToBBox(segPrediction,bbox,imgSize)

bboxH = bbox(4)-bbox(2)+1;
bboxW = bbox(3)-bbox(1)+1;
assert(strcmp(class(segPrediction),'uint8'),'Invalid class for seg\n');
segPrediction = imresize(segPrediction,[bboxH bboxW],'nearest');
seg = zeros(imgSize,'uint8');

deltaLeft = max(0,1-bbox(1));
deltaRight = max(0,bbox(3)-imgSize(2));
deltaTop = max(0,1-bbox(2));
deltaBottom = max(0,bbox(4)-imgSize(1));

seg( (bbox(2)+deltaTop):(bbox(4)-deltaBottom), ...
     (bbox(1)+deltaLeft):(bbox(3)-deltaRight)) = segPrediction(...
     (1+deltaTop):(end-deltaBottom),(1+deltaLeft):(end-deltaRight)); 
