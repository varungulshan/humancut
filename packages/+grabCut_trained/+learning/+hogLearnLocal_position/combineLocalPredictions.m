function [seg,confidence] = combineLocalPredictions(trainOpts,...
         segPredictions,segBBoxes,svmOutputs,localSegMaskSize,segMaskSize)

switch(trainOpts.predictorType)
  case {'liblinear','liblinear_balanced','pegasus','pegasus_faster'}
    [seg,confidence] = combineLibLinear(trainOpts,segPredictions,segBBoxes,...
                           svmOutputs,localSegMaskSize,segMaskSize);
  otherwise
    error('Predictor type: %s not yet supported\n',trainOpts.predictorType);
end

function [seg,confidence] = combineLibLinear(trainOpts,...
    segPredictionsAll,segBBoxesAll,svmOutputsAll,localSegMaskSize,segMaskSize)

accumVotes = zeros([segMaskSize(1) segMaskSize(2)]);
countVotes = zeros([segMaskSize(1) segMaskSize(2)]);
confidence = zeros([segMaskSize(1) segMaskSize(2)]);

assert(numel(segBBoxesAll)==numel(segPredictionsAll),...
    'Inconsistent segBBoxes and segPredictions splits\n');
for iSplit = 1:numel(segPredictionsAll)
  segPredictions = segPredictionsAll{iSplit};
  svmOutputs = svmOutputsAll{iSplit};
  segBBoxes = segBBoxesAll{iSplit};
  assert(size(segPredictions,2)==size(segBBoxes,2),...
      'Inconsistent segboxes and seg predictions\n');
  segPredictions = reshape(segPredictions,...
      [localSegMaskSize(1) localSegMaskSize(2) size(segPredictions,2)]);
  svmOutputs = reshape(svmOutputs,...
      [localSegMaskSize(1) localSegMaskSize(2) size(svmOutputs,2)]);
  for i=1:size(segBBoxes,2)
    bbox = segBBoxes(:,i);
    accumVotes(bbox(1):bbox(2),bbox(3):bbox(4)) = ...
        accumVotes(bbox(1):bbox(2),bbox(3):bbox(4))...
        + segPredictions(:,:,i);
    countVotes(bbox(1):bbox(2),bbox(3):bbox(4)) = ...
        countVotes(bbox(1):bbox(2),bbox(3):bbox(4)) + 1;
    confidence(bbox(1):bbox(2),bbox(3):bbox(4)) = ...
        confidence(bbox(1):bbox(2),bbox(3):bbox(4)) + svmOutputs(:,:,i);
  end
end
countVotes(countVotes==0)=1;
confidence = confidence./countVotes;
seg = zeros(size(accumVotes),'uint8');
seg(accumVotes>=0) = 255;
