function [hogs,segMasks] = extractTrainData(imgDir,labelDir,gtDir,...
    images,hogOpts,trainOpts,debugOpts)

import grabCut_trained.learning.hogLearnLocal_position.*;

nImages = length(images);
flipEnable = trainOpts.flipEnable;

N = nImages;
if(flipEnable), N=2*N; end;

hogByImg = cell(1,N);
segMaskByImg = cell(1,N);
hogBBoxesByImg = cell(1,N);
segBBoxesByImg = cell(1,N);

localSegMaskSize = getLocalSegMaskSize(trainOpts);

for i=1:length(images)
  objectId = grabCut_trained.dataset.extractObjectIdentifier(images{i});

  tmpDebugOpts = debugOpts;
  tmpDebugOpts.debugPrefix = [objectId '_'];

  labelFile = [labelDir objectId '.png'];
  labelImg = imread(labelFile);
  [bbox,bboxAspect] = getBoxAspectRatio(labelImg);
  img = imread([imgDir grabCut_trained.dataset.extractImgName(images{i})]);
  img = im2double(img);
  gt = imread([gtDir objectId '.png']);
  [hogByImg{i},segMaskByImg{i},hogBBoxesByImg{i},segBBoxesByImg{i}] ...
      = getLocalHogAndSeg(img,gt,bbox,trainOpts,hogOpts,tmpDebugOpts,localSegMaskSize);

  if(flipEnable)
    tmpDebugOpts = debugOpts;
    tmpDebugOpts.debugPrefix = [objectId '_flip_'];
    labelImg = flipdim(labelImg,2);
    [bbox,bboxAspect] = getBoxAspectRatio(labelImg);
    img = flipdim(img,2);
    gt = flipdim(gt,2);
    [hogByImg{nImages+i},segMaskByImg{nImages+i},...
     hogBBoxesByImg{nImages+i},segBBoxesByImg{nImages+i}] ...
       = getLocalHogAndSeg(img,gt,bbox,trainOpts,hogOpts,tmpDebugOpts,localSegMaskSize);
  end

end

hogs = cell(trainOpts.numVerticalSplits,1);
segMasks = cell(trainOpts.numVerticalSplits,1);

for iSplit = 1:trainOpts.numVerticalSplits
  assert(trainOpts.numVerticalSplits == numel(hogBBoxesByImg{iSplit}),...
      'Inconsistent numsplits with hogBBoxesByImg\n');
  assert(trainOpts.numVerticalSplits == numel(segBBoxesByImg{iSplit}),...
      'Inconsistent numsplits with segBBoxesByImg\n');
  numHogs = 0;
  for i=1:numel(hogByImg)
    numHogs = numHogs + size(hogBBoxesByImg{i}{iSplit},2);
  end
  hogs{iSplit} = zeros(getHogDimensionality(hogOpts,trainOpts),numHogs);
  segMasks{iSplit} = zeros(prod(localSegMaskSize),numHogs);
  idxNext = 1;
  for i=1:numel(hogByImg)
    hogBoxes = hogBBoxesByImg{i}{iSplit};
    segBoxes = segBBoxesByImg{i}{iSplit};
    assert(size(hogBoxes,2)==size(segBoxes,2),'Inconsistent segmasks and hogs\n');
    currentHog = hogByImg{i};
    currentSeg = segMaskByImg{i};
    for j=1:size(hogBoxes,2)
      tmpHog = currentHog(hogBoxes(1,j):hogBoxes(2,j),hogBoxes(3,j):hogBoxes(4,j),:);
      hogs{iSplit}(:,idxNext) = tmpHog(:);
      tmpSeg = currentSeg(segBoxes(1,j):segBoxes(2,j),segBoxes(3,j):segBoxes(4,j));
      segMasks{iSplit}(:,idxNext) = tmpSeg(:);
      idxNext = idxNext+1;
    end
  end
  assert(size(hogs{iSplit},1)==getHogDimensionality(hogOpts,trainOpts), ...
    'Hog size incorrect\n');
  assert(size(segMasks{iSplit},1)==prod(localSegMaskSize),'Incorrect seg output size\n');
end

function [hogFull,segMaskFull,hogBBoxes,segBBoxes] = ...
    getLocalHogAndSeg(img,gt,bbox,trainOpts,hogOpts,...
    debugOpts,localMaskSize)
% hogFull is the hog for the image window, and same for segMaskFull
% hogFull is D x H x W array (D=dimensionality,H,W are number of hog cells)
% hogBBoxes is [4 x N] array, of type [y1 y2 x1 x2] which indicates indices
% to extract from hogFull to get a localHog (ie, hogBBoxes(:,y1:y2,x1:x2)
% same for segMaskBBoxes, segBBoxes(y1:y2,x1:x2) gives a seg

import grabCut_trained.learning.hogLearnLocal_position.*;

useSegForHog = trainOpts.useSegForHog;

[bbox,hogBoxSize] = snapBBoxToSize(bbox,trainOpts,hogOpts);
numCellsH = hogBoxSize(1)/hogOpts.cellSize;
numCellsW = hogBoxSize(2)/hogOpts.cellSize;
segCellSizeH = localMaskSize(1)/trainOpts.localHogH;
segCellSizeW = localMaskSize(2)/trainOpts.localHogW;
segMaskSize = ceil([numCellsH*segCellSizeH numCellsW*segCellSizeW]);

tmpSegMask = cpp.mex_extractBBox(gt,int32(bbox-1));
tmpSegMask = imresize(tmpSegMask,segMaskSize,'nearest');
segMaskFull = zeros(size(tmpSegMask));
segMaskFull(tmpSegMask==128)=0.5;
segMaskFull(tmpSegMask==255)=1;

localImg = cpp.mex_extractBBox(img,int32(bbox-1));
localImg = imresize(localImg,[hogBoxSize(1) hogBoxSize(2)],'bilinear');
if(~useSegForHog)
  hog = hogCode.mex_getHog(localImg,hogOpts.cellSize);
else
  tmpSegMask = cpp.mex_extractBBox(gt,int32(bbox-1));
  tmpSegMask = imresize(tmpSegMask,[hogBoxSize(1) hogBoxSize(2)],'nearest');
  stEl = strel('disk',trainOpts.erodeSegWidth);
  tmpSegMask = imdilate(tmpSegMask==255,stEl);
  hog = hogCode.mex_getHogWithMask(localImg,hogOpts.cellSize,tmpSegMask);
end

if(debugOpts.debugLevel>=10)
  visHog = drawHog(hog,hogOpts);
  miscFns.saveDebug(debugOpts,visHog,'hog.png');
  miscFns.saveDebug(debugOpts,imresize(localImg,size(visHog)),'img.jpg');
  miscFns.saveDebug(debugOpts,imresize(tmpSegMask,size(visHog),'nearest'),'seg.png');
end
% hog will be a H x W x (27+4) matrix, H = number of vertical cells, W = horiz cells
hog = selectHogType(hog,hogOpts);
%hogFull = shiftdim(hog,2); % to make it vertical
hogFull = hog;

hogBBoxes = cell(trainOpts.numVerticalSplits,1);
segBBoxes = cell(trainOpts.numVerticalSplits,1);
numRowsPerSplit = (numCellsH-trainOpts.localHogH+1)/trainOpts.numVerticalSplits;

prevEnd = 0;

for i=1:trainOpts.numVerticalSplits

thisEnd = (round(numRowsPerSplit*i));
yStart = [(prevEnd+1):thisEnd];
prevEnd = thisEnd;
xStart = [1:(numCellsW-trainOpts.localHogW+1)];

[x1,y1] = meshgrid(xStart,yStart);
x1=x1(:)';
y1=y1(:)';
hogBBoxes{i} = zeros(4,numel(x1));
hogBBoxes{i}(1,:) = y1;
hogBBoxes{i}(2,:) = y1+trainOpts.localHogH-1;
hogBBoxes{i}(3,:) = x1;
hogBBoxes{i}(4,:) = x1+trainOpts.localHogW-1;

segBBoxes{i} =zeros(4,numel(x1));
segBBoxes{i}(1,:) = round(1+(y1-1)*segCellSizeH);
segBBoxes{i}(2,:) = segBBoxes{i}(1,:)+localMaskSize(1)-1;
segBBoxes{i}(3,:) = round(1+(x1-1)*segCellSizeW);
segBBoxes{i}(4,:) = segBBoxes{i}(3,:)+localMaskSize(2)-1;

end
