function [segPredictions,svmOuts]=applyPredictor(hogs,trainOpts,predictorInfo)

switch(trainOpts.predictorType)
  case {'liblinear','liblinear_balanced','pegasus','pegasus_faster'}
    [segPredictions,svmOuts] = applyLibLinear(hogs,trainOpts,predictorInfo);
  otherwise
    error('Predictor type: %s not yet supported\n',trainOpts.predictorType);
end

function [segPredictions,svmOutsAll] = applyLibLinear(hogs,trainOpts,info)

segPredictions = cell(numel(hogs),1);
svmOutsAll = cell(numel(hogs),1);
for i=1:numel(hogs)
  wT = info{i}.wT;
  predictorOpts = trainOpts.predictorOpts;
  
  if(predictorOpts.svmBias < 0)
    svmOuts = wT*hogs{i};
  else
    svmOuts = wT(:,1:(end-1))*hogs{i}+wT(:,end)*repmat(predictorOpts.svmBias,[1 size(hogs{i},2)]);
  end
  
  segPredictions{i} = ones(size(svmOuts));
  segPredictions{i}(svmOuts<0) = -1;
  svmOutsAll{i} = svmOuts;
end
