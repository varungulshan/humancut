function opts = makeOpts(optsString)

import grabCut_trained.learning.hogLearnLocal_position.*;

switch(optsString)

  case {'svm1_4levels_1800train_validateC1','svm1_4levels_1800train_validateC2','svm1_4levels_1800train_validateC3','svm1_4levels_1800train_validateC4'}
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.maxTrain = 1800;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmBias = -1;

    switch(optsString)
      case 'svm1_4levels_1800train_validateC1'
        svmC = 0.01;
      case 'svm1_4levels_1800train_validateC2'
        svmC = 0.1;
      case 'svm1_4levels_1800train_validateC3'
        svmC = 1;
      case 'svm1_4levels_1800train_validateC4'
        svmC = 10;
    end
    trainOpts.predictorOpts.svmC = svmC;


  case {'svm1_4levels_1600train_validateC1','svm1_4levels_1600train_validateC2','svm1_4levels_1600train_validateC3','svm1_4levels_1600train_validateC4'}
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.maxTrain = 1600;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmBias = -1;

    switch(optsString)
      case 'svm1_4levels_1600train_validateC1'
        svmC = 0.01;
      case 'svm1_4levels_1600train_validateC2'
        svmC = 0.1;
      case 'svm1_4levels_1600train_validateC3'
        svmC = 1;
      case 'svm1_4levels_1600train_validateC4'
        svmC = 10;
    end
    trainOpts.predictorOpts.svmC = svmC;


  case {'svm1_4levels_1400train_validateC1','svm1_4levels_1400train_validateC2','svm1_4levels_1400train_validateC3','svm1_4levels_1400train_validateC4'}
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.maxTrain = 1400;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmBias = -1;

    switch(optsString)
      case 'svm1_4levels_1400train_validateC1'
        svmC = 0.01;
      case 'svm1_4levels_1400train_validateC2'
        svmC = 0.1;
      case 'svm1_4levels_1400train_validateC3'
        svmC = 1;
      case 'svm1_4levels_1400train_validateC4'
        svmC = 10;
    end
    trainOpts.predictorOpts.svmC = svmC;


  case {'svm1_4levels_1200train_validateC1','svm1_4levels_1200train_validateC2','svm1_4levels_1200train_validateC3','svm1_4levels_1200train_validateC4'}
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.maxTrain = 1200;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmBias = -1;

    switch(optsString)
      case 'svm1_4levels_1200train_validateC1'
        svmC = 0.01;
      case 'svm1_4levels_1200train_validateC2'
        svmC = 0.1;
      case 'svm1_4levels_1200train_validateC3'
        svmC = 1;
      case 'svm1_4levels_1200train_validateC4'
        svmC = 10;
    end
    trainOpts.predictorOpts.svmC = svmC;


  case {'svm1_4levels_1000train_validateC1','svm1_4levels_1000train_validateC2','svm1_4levels_1000train_validateC3','svm1_4levels_1000train_validateC4'}
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.maxTrain = 1000;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmBias = -1;

    switch(optsString)
      case 'svm1_4levels_1000train_validateC1'
        svmC = 0.01;
      case 'svm1_4levels_1000train_validateC2'
        svmC = 0.1;
      case 'svm1_4levels_1000train_validateC3'
        svmC = 1;
      case 'svm1_4levels_1000train_validateC4'
        svmC = 10;
    end
    trainOpts.predictorOpts.svmC = svmC;


  case {'svm1_4levels_800train_validateC1','svm1_4levels_800train_validateC2','svm1_4levels_800train_validateC3','svm1_4levels_800train_validateC4'}
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.maxTrain = 800;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmBias = -1;

    switch(optsString)
      case 'svm1_4levels_800train_validateC1'
        svmC = 0.01;
      case 'svm1_4levels_800train_validateC2'
        svmC = 0.1;
      case 'svm1_4levels_800train_validateC3'
        svmC = 1;
      case 'svm1_4levels_800train_validateC4'
        svmC = 10;
    end
    trainOpts.predictorOpts.svmC = svmC;


  case {'svm1_4levels_600train_validateC1','svm1_4levels_600train_validateC2','svm1_4levels_600train_validateC3','svm1_4levels_600train_validateC4'}
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.maxTrain = 600;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmBias = -1;

    switch(optsString)
      case 'svm1_4levels_600train_validateC1'
        svmC = 0.01;
      case 'svm1_4levels_600train_validateC2'
        svmC = 0.1;
      case 'svm1_4levels_600train_validateC3'
        svmC = 1;
      case 'svm1_4levels_600train_validateC4'
        svmC = 10;
    end
    trainOpts.predictorOpts.svmC = svmC;


  case {'svm1_4levels_400train_validateC1','svm1_4levels_400train_validateC2','svm1_4levels_400train_validateC3','svm1_4levels_400train_validateC4'}
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.maxTrain = 400;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmBias = -1;

    switch(optsString)
      case 'svm1_4levels_400train_validateC1'
        svmC = 0.01;
      case 'svm1_4levels_400train_validateC2'
        svmC = 0.1;
      case 'svm1_4levels_400train_validateC3'
        svmC = 1;
      case 'svm1_4levels_400train_validateC4'
        svmC = 10;
    end
    trainOpts.predictorOpts.svmC = svmC;


  case {'svm1_4levels_2000train_validateC1','svm1_4levels_2000train_validateC2','svm1_4levels_2000train_validateC3','svm1_4levels_2000train_validateC4'}
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.maxTrain = 2000;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmBias = -1;

    switch(optsString)
      case 'svm1_4levels_2000train_validateC1'
        svmC = 0.01;
      case 'svm1_4levels_2000train_validateC2'
        svmC = 0.1;
      case 'svm1_4levels_2000train_validateC3'
        svmC = 1;
      case 'svm1_4levels_2000train_validateC4'
        svmC = 10;
    end
    trainOpts.predictorOpts.svmC = svmC;


  case {'svm1_4levels_200train_validateC1','svm1_4levels_200train_validateC2','svm1_4levels_200train_validateC3','svm1_4levels_200train_validateC4'}
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.maxTrain = 200;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmBias = -1;

    switch(optsString)
      case 'svm1_4levels_200train_validateC1'
        svmC = 0.01;
      case 'svm1_4levels_200train_validateC2'
        svmC = 0.1;
      case 'svm1_4levels_200train_validateC3'
        svmC = 1;
      case 'svm1_4levels_200train_validateC4'
        svmC = 10;
    end
    trainOpts.predictorOpts.svmC = svmC;

  case 'svm1_8levels_validateC1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 8;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.01;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_8levels_validateC2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 8;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_8levels_validateC3'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 8;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_8levels_validateC4'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 8;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'dictionaryOnly_4levels_nonNegative_8threads'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.enableLearner = false;
    trainOpts.useSegForHog= true;
    trainOpts.erodeSegWidth = 2;
    trainOpts.hogPreProcess = 'dictionarySplit';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.posD = 1;
    ppOpts.dictionaryParams.posAlpha = 1;
    ppOpts.dictionaryParams.iter = 300;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 625;
    ppOpts.dictionaryParams.numThreads = 8;
    ppOpts.dictionaryParams.batchSize = 2048;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;
    ppOpts.lassoParams.pos = ppOpts.dictionaryParams.posAlpha;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'dictionaryOnly_4levels_8threads_moreIters'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionarySplit';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 625;
    ppOpts.dictionaryParams.numThreads = 8;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'dictionaryOnly_4levels'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionarySplit';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 600;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_1level_replicateHogLearnLocal'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 1;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_1level_replicateHogLearnLocal_pegasusFaster'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 1;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'pegasus_faster';
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;
    trainOpts.predictorOpts.numPasses = 4;

  case 'svm1_1level_replicateHogLearnLocal_pegasus'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 1;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'pegasus';
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;
    trainOpts.predictorOpts.numPasses = 4;

  case 'svm1_4levels_validateC1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.01;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_4levels_validateC2_allDict_pegasus2'
  % allDict means dictionary learnt on train and test
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_trainAndTest_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'pegasus';
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;
    trainOpts.predictorOpts.numPasses = 1;

  case 'svm1_4levels_validateC2_allDict_pegasus'
  % allDict means dictionary learnt on train and test
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_trainAndTest_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'pegasus';
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;
    trainOpts.predictorOpts.numPasses = 4;

  case 'svm1_4levels_validateC2_allDict_l1loss'
  % allDict means dictionary learnt on train and test
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_trainAndTest_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_4levels_validateC2_allDict'
  % allDict means dictionary learnt on train and test
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_trainAndTest_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_4levels_validateC2_fgbgSeparateDict2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_fgBgSeparatePadding_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;


  case 'svm1_4levels_validateC2_fgbgSeparateDict1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_fgBgSeparate_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_4levels_nonNegative'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_nonNegative_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.posD = 1;
    ppOpts.dictionaryParams.posAlpha = 1;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;
    ppOpts.lassoParams.pos = ppOpts.dictionaryParams.posAlpha;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_4levels_4splitsDict_nonNegative'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySplit_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_4splits_nonNegative_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.posD = 1;
    ppOpts.dictionaryParams.posAlpha = 1;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;
    ppOpts.lassoParams.pos = ppOpts.dictionaryParams.posAlpha;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateLevels_6levels'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 6;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;



  case 'svm1_validateLevels_4levels'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;


  case 'svm1_validateLevels_2levels'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 2;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;


  case 'svm1_4levels_validateC2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_4levels_validateC3'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_4levels_validateC4'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_singleDictionary_4levels_l2loss_balanced'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear_balanced';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_singleDictionary_4levels_l2loss'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_splitDictionary_4levels_l2loss'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySplit_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_flipData_4splits_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 600;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_splitDictionary_4levels_l2loss_C2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySplit_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_flipData_4splits_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 600;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_splitDictionaryMoreIters_4levels_l2loss_C2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySplit_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_4splits_moreIters_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 600;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;


  case 'svm1_singleDictionary_4levels'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_4levels_validate2_C1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.05;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_4levels_validate2_C2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_4levels_validate2_C3'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.35;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_4levels_validate2_C4'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.7;
    trainOpts.predictorOpts.svmBias = -1;



  case 'try1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_4levels_validateC1_normOutput'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved_normOutput';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.01;
    trainOpts.predictorOpts.svmBias = -1;


  case 'svm1_4levels_validateC2_normOutput'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved_normOutput';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_4levels_validateC3_normOutput'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved_normOutput';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;


  case 'svm1_4levels_validateC4_normOutput'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.numVerticalSplits = 4;
    trainOpts.hogPreProcess = 'dictionarySingle_saved_normOutput';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  otherwise
    error('Invalid options string: %s\n',optsString);
end

opts.hogOpts = hogOpts;
opts.trainOpts = trainOpts;
