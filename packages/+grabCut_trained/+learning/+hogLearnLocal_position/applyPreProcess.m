function hogs = applyPreProcess(hogs,trainOpts,ppInfo)
% hogs is a cell array of size numVerticalSplits

switch(trainOpts.hogPreProcess)
  case {'dictionarySingle_saved'}
    hogs = applyDictionarySingle(hogs,trainOpts.hogPreProcessOpts,ppInfo);
  case {'dictionarySingle_saved_normOutput'}
    % normalizes vector to unit norm
    hogs = applyDictionarySingle_normOutput(hogs,trainOpts.hogPreProcessOpts,ppInfo);
  case {'dictionarySplit','dictionarySplit_saved'}
    hogs = applyDictionary_bySplit(hogs,trainOpts.hogPreProcessOpts,ppInfo);
  case 'none',
    hogs=sparse(hogs);
  otherwise
    error('Invalid pre process method: %s\n',trainOpts.hogPreProcess);
end

function hogs = applyDictionary_bySplit(hogs,opts,ppInfo)

assert(numel(hogs)==numel(ppInfo.dictionary),...
    'Incompatible number of dictionary and hog vertical splits\n');
for i=1:numel(hogs)
  hogs{i} = mexLasso(hogs{i},ppInfo.dictionary{i},opts.lassoParams);
end

function hogs = applyDictionarySingle(hogs,opts,ppInfo)

for i=1:numel(hogs)
  hogs{i} = mexLasso(hogs{i},ppInfo.dictionary,opts.lassoParams);
end

function hogs = applyDictionarySingle_normOutput(hogs,opts,ppInfo)

for i=1:numel(hogs)
  sparseFeatures = mexLasso(hogs{i},ppInfo.dictionary,opts.lassoParams);
  for j=1:size(sparseFeatures,2)
    colNorm = norm(sparseFeatures(:,j));
    sparseFeatures(:,j) = sparseFeatures(:,j)/colNorm;
  end
  hogs{i} = sparseFeatures;
end
