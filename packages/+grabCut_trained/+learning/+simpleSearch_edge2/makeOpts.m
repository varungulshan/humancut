function opts = makeGrabCutOpts(optsString)

switch(optsString)
  case 'kovesi_edgeLearn_1'
    gcOpts = grabCut_edgeLearn2.helpers.makeOpts('iter10_2_edgeLearn');
    vary(1).fieldName='gcGamma';
    vary(1).fieldValues=num2cell([10 25 50 100 150]);
    scoreString='accuracyOverlap';
  case 'debug_edgeLearn_1'
    gcOpts = grabCut_edgeLearn2.helpers.makeOpts('iter10_2_edgeLearn');
    vary(1).fieldName='gcGamma';
    vary(1).fieldValues=num2cell([10]);
    scoreString='accuracyOverlap';
  case 'debug_edgeLearn_2'
    gcOpts = grabCut_edgeLearn2.helpers.makeOpts('iter10_2_edgeLearn');
    gcOpts.gmmNmix_yesEdge = 10;
    gcOpts.gmmNmix_noEdge = 14;
    vary(1).fieldName='gcGamma';
    vary(1).fieldValues=num2cell([10]);
    scoreString='accuracyOverlap';
  case 'debug_edgeLearn_3'
    gcOpts = grabCut_edgeLearn2.helpers.makeOpts('iter10_3_edgeLearnWait');
    vary(1).fieldName='gcGamma';
    vary(1).fieldValues=num2cell([gcOpts.gcGamma]);
    scoreString='accuracyOverlap';
  case 'debug_edgeLearn_4'
    gcOpts = grabCut_edgeLearn2.helpers.makeOpts('iter10_3_edgeLearnContinue');
    gcOpts.continuityThresh = 45;
    vary(1).fieldName='gcGamma';
    vary(1).fieldValues=num2cell([gcOpts.gcGamma]);
    scoreString='accuracyOverlap';
  case 'debug_edgeLearn_5'
    gcOpts = grabCut_edgeLearn2.helpers.makeOpts('iter10_3_edgeLearnLong');
    gcOpts.edgeFeatureExtend = 10;
    vary(1).fieldName='gcGamma';
    vary(1).fieldValues=num2cell([gcOpts.gcGamma]);
    scoreString='accuracyOverlap';
  case 'kovesi_1'
    gcOpts = grabCut_edgeLearn2.helpers.makeOpts('iter10_2');
    vary(1).fieldName='gcGamma';
    vary(1).fieldValues=num2cell([10 25 50 100 150]);
    scoreString='accuracyOverlap';
  case 'kovesi_1_soft'
    gcOpts = grabCut_edgeLearn2.helpers.makeOpts('iter10_3_soft');
    vary(1).fieldName='gcGamma';
    vary(1).fieldValues=num2cell([10 25 50 100 150]);
    scoreString='accuracyOverlap';
  case 'kovesi_2'
    gcOpts = grabCut_edgeLearn2.helpers.makeOpts('iter10_2');
    vary(1).fieldName='gcGamma';
    vary(1).fieldValues=num2cell([10 25 50 100 150]);
    vary(2).fieldName='kovesiAutoThresh';
    vary(2).fieldValues=num2cell([0.3 0.5 0.7 0.9]);
    vary(3).fieldName='kovesiSigma';
    vary(3).fieldValues=num2cell([1 2 3]);
    scoreString='accuracyOverlap';
  case 'kovesi_edgeLearn_2'
    gcOpts = grabCut_edgeLearn2.helpers.makeOpts('iter10_2_edgeLearn');
    gcOpts.gcGamma = 10;
    vary(1).fieldName='gmmNmix_noEdge';
    vary(1).fieldValues=num2cell([6 8 10 14]);
    vary(2).fieldName='gmmNmix_yesEdge';
    vary(2).fieldValues=num2cell([6 8 10 14]);
    vary(3).fieldName='gcGamma';
    vary(3).fieldValues=num2cell([10 25 50]);
    scoreString='accuracyOverlap';
  case 'edgeLearn_wait_1'
    gcOpts = grabCut_edgeLearn2.helpers.makeOpts('iter10_3_edgeLearnWait');
    vary(1).fieldName='gcGamma';
    vary(1).fieldValues=num2cell([10 25 50 100 150]);
    scoreString='accuracyOverlap';
  case 'edgeLearnLong_1'
    gcOpts = grabCut_edgeLearn2.helpers.makeOpts('iter10_3_edgeLearnLong');
    vary(1).fieldName='gcGamma';
    vary(1).fieldValues=num2cell([10 25 50 100]);
    vary(2).fieldName='itersBeforeLearning';
    vary(2).fieldValues=num2cell([0 1]);
    vary(3).fieldName='continuityOn';
    vary(3).fieldValues=num2cell([true false]);
    scoreString='accuracyOverlap';
  case 'edgeLearnLongWait_1'
    gcOpts = grabCut_edgeLearn2.helpers.makeOpts('iter10_3_edgeLearnLong');
    gcOpts.itersBeforeLearning = 1;
    vary(1).fieldName='gcGamma';
    vary(1).fieldValues=num2cell([10 25 50]);
    vary(2).fieldName='edgeFeatureExtend';
    vary(2).fieldValues=num2cell([1 4 8 12]);
    scoreString='accuracyOverlap';
  case 'edgeLearnLong_2'
    gcOpts = grabCut_edgeLearn2.helpers.makeOpts('iter10_3_edgeLearnLong');
    vary(1).fieldName='gcGamma';
    vary(1).fieldValues=num2cell([10 25 50 100]);
    vary(2).fieldName='edgeFeatureExtend';
    vary(2).fieldValues=num2cell([1 4 8 12 15]);
    scoreString='accuracyOverlap';
  case 'edgeLearnContinue_1'
    gcOpts = grabCut_edgeLearn2.helpers.makeOpts('iter10_3_edgeLearnContinue');
    vary(1).fieldName='gcGamma';
    vary(1).fieldValues=num2cell([10 25 50 100 150]);
    vary(2).fieldName='continuityThresh';
    vary(2).fieldValues=num2cell([22.5 45 70 91]);
    scoreString='accuracyOverlap';
  case 'smallTestKovesi'
    gcOpts = grabCut_edgeLearn2.helpers.makeOpts('iterQuickLearn');
    vary(1).fieldName='kovesiAutoThresh';
    vary(1).fieldValues=num2cell([0.6 0.7 0.8]);
    vary(2).fieldName='kovesiSigma';
    vary(2).fieldValues=num2cell([1 3]);
    scoreString='accuracyOverlap';
  otherwise
    error('Invalid options string: %s\n',optsString);
end

opts.grabCutOpts = gcOpts;
opts.vary = vary;
opts.scoreString = scoreString;
