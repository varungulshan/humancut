function data = loadTrainingData(trainDir,testDir)
% data will include:
%   data.grabCutOpts: options for grabCut
% testDir is used to output diagnostic information

tmpLoad = load([trainDir 'bestOpts.mat']);
data.grabCutOpts = tmpLoad.bestOpts;
fH = fopen([testDir 'bestGrabCutOpts.txt'],'w');
miscFns.printClass(data.grabCutOpts,fH);
fclose(fH);
