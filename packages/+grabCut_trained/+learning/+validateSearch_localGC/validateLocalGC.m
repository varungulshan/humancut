function validateLocalGC(params)
% Specific function to validate localGC unary scale value

import grabCut_trained.learning.validateSearch_localGC.*;

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('params','var'))
  params.rootOut_dir=[cwd '../../../../results/grabCut_trained/train/gcWithValidation/'];
  params.rootOut_dir_test=[cwd '../../../../results/grabCut_trained/test/gcWithValidation/'];
  params.testEnable = true;
  %params.opts.evalOpts_string='kinect_10Feb2011';
  params.opts.evalOpts_string='try1';
  
  %params.opts.gcOptsStrings={'rad40_validate1','rad40_validate2'};
  params.opts.gcOptsStrings={'smallTest','smallTest2'};
  params.opts.hogLearnOptsStrings={'hogLearnLocal_validate1','hogLearnLocal_validate2'};
  
  params.opts.hogLearnOptsString_fullTrain='hogLearnLocal_3';

  params.opts.visualize=true;
  params.opts.visOpts.debugVisualize=true;
  params.opts.debugLevel=10;
  params.opts.visOpts.imgExt='jpg';
  params.opts.visOpts.segBdryWidth=2;
  params.overWrite=true;

  params.testBench_cmd='testBench.getTests_trainWithValidation_small()';
  params.testBench_cmd_test='testBench.getTests_grabCut_small()';
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',params.rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
end

testBenchOpts=eval(params.testBench_cmd);
save([params.rootOut_dir 'testBenchOpts.mat'],'testBenchOpts');
save([params.rootOut_dir 'params.mat'],'params');

numParams = numel(params.opts.gcOptsStrings);
numFolds = numel(params.opts.hogLearnOptsStrings); 

assert(numFolds == numel(testBenchOpts.trainTestBenchCmds),...
    'inconsistent hoglearn strings and train sets\n');
assert(numFolds == numel(testBenchOpts.validateTestBenchCmds),...
    'inconsistent hoglearn strings and validate sets\n');

validationDir = [params.rootOut_dir 'validations/'];
if(~exist(validationDir,'dir')), mkdir(validationDir); end;

for i = 1:numFolds
  fprintf('Running on fold number: %02d\n',i);
  [hogLearnData,hogLearnType] = localGrabCut_hogLearn.loadHogLearnData...
      (params.opts.hogLearnOptsStrings{i});
  for j = 1:numParams
    fprintf('  Running on parameter setting: %02d\n',j);
    gcOpts = localGrabCut_hogLearn.helpers.makeOpts(...
        params.opts.gcOptsStrings{j});
    gcOpts.hogLearnType = hogLearnType;
    outDir = [validationDir sprintf('optsString_%s_foldNum_%02d/',...
        params.opts.gcOptsStrings{j},i)];
    runLocalGC(hogLearnData,gcOpts,outDir,params,i,testBenchOpts);
  end
end

% Accumuulate all folds and set the best options
scoreString = 'accuracyOverlap';
perf = zeros(numParams,numFolds);
fH = fopen([params.rootOut_dir 'validation_summary.txt'],'w');

for j = 1:numParams
  optsString = params.opts.gcOptsStrings{j};
  for i = 1:numFolds
    statsFile = [params.rootOut_dir 'validations/' ...
        sprintf('optsString_%s_foldNum_%02d/validate/',...
        optsString,i) 'stats.mat'];
    tmpLoad = load(statsFile);
    perf(j,i) = getAvgPerf(tmpLoad.gStats,scoreString);
    fprintf(fH,'Parameter setting #%02d: %s, validation fold: %d, Score = %.2f\n',j,optsString,i,perf(j,i));
  end
  fprintf(fH,'\n');
end

perf = mean(perf,2);
for i=1:numParams
  fprintf(fH,'Mean validation perf for parameter setting #%02d: %s = %.2f\n',i,params.opts.gcOptsStrings{i},perf(i));
end

[bestPerf,bestI] = max(perf);
fprintf(fH,'\nBest parameter setting is #%02d: %s\n',bestI,...
    params.opts.gcOptsStrings{bestI});
fclose(fH);

[hogLearnData,hogLearnType] = localGrabCut_hogLearn.loadHogLearnData...
   (params.opts.hogLearnOptsString_fullTrain);
gcOpts = localGrabCut_hogLearn.helpers.makeOpts(...
    params.opts.gcOptsStrings{bestI});
gcOpts.hogLearnType = hogLearnType;
bestOpts = gcOpts;
saveFullGC(hogLearnData,gcOpts,params);

if(params.testEnable)
  testParams.rootOut_dir = params.rootOut_dir_test;
  testParams.trainDir = [params.rootOut_dir 'bestTrain/'];
  testParams.opts.visualize=params.opts.visualize;
  testParams.opts.visOpts=params.opts.visOpts;
  testParams.opts.debugLevel=params.opts.debugLevel;
  testParams.overWrite=params.overWrite;
  
  testParams.evalOpts_string = params.opts.evalOpts_string; 
  testParams.testBench_cmd = params.testBench_cmd_test;
  
  grabCut_trained.testGrabCut(testParams);

  % Run on train set
  testParams.rootOut_dir = [params.rootOut_dir 'evaluateOnTrain/'];
  testParams.testBench_cmd = testBenchOpts.fullTrainTestBenchCmd;

  grabCut_trained.testGrabCut(testParams);
end

function saveFullGC(hogLearnData,gcOpts,allParams)

params.rootOut_dir = [allParams.rootOut_dir 'bestTrain/'];
params.opts.trainMethod = 'localGrabCut_simpleSearch_hogLearn';

if(~exist(params.rootOut_dir)), mkdir(params.rootOut_dir); end
save([params.rootOut_dir 'params.mat'],'params');

bestOpts = gcOpts;
save([params.rootOut_dir 'bestOpts.mat'],'bestOpts','hogLearnData');
fH = fopen([params.rootOut_dir 'bestOpts.txt'],'w');
miscFns.printClass(bestOpts,fH);
fclose(fH);

function runLocalGC(hogLearnData,gcOpts,outDir,allParams,iFold,testBenchOpts)

% Save in the train dir in the same format as localHogLearn_simpleSearch
% would
params.rootOut_dir = [outDir 'train/'];
params.opts.trainMethod = 'localGrabCut_simpleSearch_hogLearn';

if(~exist(params.rootOut_dir)), mkdir(params.rootOut_dir); end
save([params.rootOut_dir 'params.mat'],'params');

bestOpts = gcOpts;
save([params.rootOut_dir 'bestOpts.mat'],'bestOpts','hogLearnData');
fH = fopen([params.rootOut_dir 'bestOpts.txt'],'w');
miscFns.printClass(bestOpts,fH);
fclose(fH);

testParams.rootOut_dir = [outDir 'validate/'];
testParams.trainDir = params.rootOut_dir;
testParams.opts.visualize=allParams.opts.visualize;
testParams.opts.visOpts=allParams.opts.visOpts;
testParams.opts.debugLevel=allParams.opts.debugLevel;
testParams.overWrite=allParams.overWrite;

testParams.evalOpts_string = allParams.opts.evalOpts_string; 
testParams.testBench_cmd = testBenchOpts.validateTestBenchCmds{iFold};

grabCut_trained.testGrabCut(testParams);

function perf = getAvgPerf(gStats,scoreString)

cmd = ['[gStats.stats_' scoreString '.score]'];
perf = eval(cmd);
perf = mean(perf);


function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);
