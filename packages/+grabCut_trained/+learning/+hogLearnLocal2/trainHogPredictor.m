function trainHogPredictor(params,trainOutDir,testBenchOpts)

import grabCut_trained.learning.hogLearnLocal2.*;

assert(strcmp(params.trainMethod,'hogLearnLocal2'));

fTime = fopen([trainOutDir 'timing.txt'],'w');
stTimeGlobal = clock;

opts = makeOpts(params.trainMethod_optsString);
evalOpts = testBench.evalOpts_grabCut(params.evalOpts_string);
gtDir = evalOpts.gtDir;
save([trainOutDir 'trainOpts.mat'],'opts');

fH = fopen([trainOutDir 'trainOpts.txt'],'w');
miscFns.printClass(opts.trainOpts,fH);
miscFns.printClass(opts.hogOpts,fH);
fclose(fH);

trainingDataDir = [trainOutDir 'trainingData/'];
mkdir(trainingDataDir);

images = textread(testBenchOpts.imgList,'%s');
imgDir = testBenchOpts.imgDir;
labelDir = testBenchOpts.labelDir;

trainOpts = opts.trainOpts;
hogOpts = opts.hogOpts;

if(trainOpts.maxTrain > 0)
  numAccept = min(trainOpts.maxTrain,numel(images));
  numReject = numel(images)-numAccept;
  reset(RandStream.getDefaultStream);
  if(numAccept>numReject)
    idxReject = grabCut_trained.dataset.COMuniquerand(numReject,...
                [1 numel(images)]);
    idxAccept = setdiff([1:numel(images)],idxReject);
  else
    idxAccept = grabCut_trained.dataset.COMuniquerand(numAccept,...
                [1 numel(images)]);
  end
else
  idxAccept = [1:numel(images)];
end

images = images(idxAccept);

debugOpts = struct();
debugOpts.debugLevel = params.debugLevel;
debugOpts.debugDir = [trainOutDir 'debug/'];
debugOpts.debugPrefix = '';
mkdir(debugOpts.debugDir);

[ppInfo,hogByImg,hogBBoxesByImg,segMaskByImg,segBBoxesByImg] = ...
    extractTrainData(imgDir,labelDir,gtDir,images,hogOpts,trainOpts,debugOpts);
save([trainingDataDir 'ppInfo.mat'],'ppInfo');

if(trainOpts.enableLearner)
  stTime = clock;
  fprintf('Extracting features from data\n');
  svmH = extractFeatures(ppInfo,hogByImg,segMaskByImg,hogBBoxesByImg,...
                                      segBBoxesByImg,trainOpts,hogOpts);
  fprintf(fTime,'Time taken to compute features from data  = %.2f minutes (%.1fhrs)\n',etime(clock,stTime)/60,etime(clock,stTime)/3600);
  
  fprintf('Learning predictor ...\n');
  stTime = clock;
  predictorInfo = learnPredictor(svmH,trainOpts,debugOpts);
  fprintf('Time taken to learn predictor = %.2f minutes (%.1fhrs)\n',... 
      etime(clock,stTime)/60,etime(clock,stTime)/3600);
  fprintf(fTime,'Time taken to learn predictor = %.2f minutes (%.1fhrs)\n',... 
      etime(clock,stTime)/60,etime(clock,stTime)/3600);
  %saveLearningDebug(hogs,ppInfo,predictorInfo,hogOpts,trainOpts,debugOpts);
  save([trainingDataDir 'segPredictor.mat'],'predictorInfo');
end
fprintf(fTime,'Total time taken for training: %.1f hrs\n',etime(clock,stTimeGlobal)/3600);
fclose(fTime);
delete(svmH);
