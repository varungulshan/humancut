function info = learnPredictor(svmH,trainOpts,debugOpts)

info = struct();

switch(trainOpts.predictorType)
  case 'liblinear'
    info = svmH.train();
  case 'liblinear_validation'
    info = svmH.trainWithValidation();
  otherwise
    error('Invalid predictorType: %s\n',trainOpts.predictorType);
end

% function info = learnLibLinear(features,labels,opts,debugOpts)
% 
% liblinearOptions = sprintf('-q -s %d -c %f -B %d',opts.svmType,opts.svmC,...
%                            opts.svmBias);
% 
% model = liblinear.train(labels,features,liblinearOptions,'col');
% info.model = model;
% 
% function info = learnLibLinear_validate(features,labels,opts,debugOpts)
% 
% perf = zeros(1,numel(opts.svmC));
% if(debugOpts.debugLevel > 0)
%   fH = fopen([debugOpts.debugDir 'svmValidation.txt'],'w');
%   fprintf(fH,'Starting svm validation\n');
% end
% 
% for i=1:numel(opts.svmC)
%   liblinearOptions = sprintf('-q -s %d -c %f -B %d -v %d',opts.svmType,opts.svmC(i),...
%                              opts.svmBias,opts.numValidationFolds);
%   perf(i) = liblinear.train(labels,features,liblinearOptions,'col');
%   if(debugOpts.debugLevel > 0)
%     fprintf(fH,'%d fold validation completed for C = %d, perf = %.2f\n',...
%                opts.numValidationFolds,opts.svmC(i),perf(i));
%   end
% end
% 
% if(debugOpts.debugLevel > 0)
%   fprintf(fH,'Validation completed\n');
%   fclose(fH);
% end
% 
% [bestPerf,bestC] = max(perf);
% info.bestC = bestC;
% 
% liblinearOptions = sprintf('-q -s %d -c %f -B %d',opts.svmType,bestC,...
%                            opts.svmBias);
% model = liblinear.train(labels,features,liblinearOptions,'col');
% info.model = model;


