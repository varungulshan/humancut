function maskSize = getLocalSegMaskSize(trainOpts)
% Returns the dimensions of the output segmentation mask size
% upon which the predictor should be learned

hogSize = [trainOpts.localHogH trainOpts.localHogW];
maxDim = max(hogSize);

maskSize = round(trainOpts.localSegMaskLongerDimension*hogSize/maxDim);
