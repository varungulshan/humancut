function [box,ratio] = getBoxAspectRatio(labelImg)
% Assuming the labeling is of bounding box type
% this function returns the coordinates of the bounding box
% and its aspect ration (w/h)

minIdx = find(labelImg==3,1,'first');
maxIdx = find(labelImg==3,1,'last');

[y1,x1]=ind2sub(size(labelImg),minIdx);
[y2,x2]=ind2sub(size(labelImg),maxIdx);

box=[x1;y1;x2;y2];
ratio = (x2-x1)/(y2-y1);
