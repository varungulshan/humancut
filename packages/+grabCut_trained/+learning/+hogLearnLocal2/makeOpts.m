function opts = makeOpts(optsString)

import grabCut_trained.learning.hogLearnLocal2.*;

switch(optsString)

  case 'kmeansDictionary_validation_small'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 10;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';

    ppOpts.dictionaryType = 'kmeans_saved';
    ppOpts.dictionaryFile = ...
    '/data/adam2/varun/iccv2011/results/savedDictionaries/kmeans_500_kinect10Feb2011_1.mat';
    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear_validation';
    trainOpts.predictorOpts.numValidationFolds = 3;
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = [0.001 0.01 0.1];
    trainOpts.predictorOpts.svmBias = -1;
  
  case 'kmeansDictionary_saved_svm1_bias'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';

    ppOpts.dictionaryType = 'kmeans_saved';
    ppOpts.dictionaryFile = ...
    '/data2/varun/iccv2011/results/savedDictionaries/kmeans_500_kinect10Feb2011_1.mat';
    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 0.01;
    trainOpts.predictorOpts.svmBias = 1;


  case 'kmeansDictionary_saved_svm1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';

    ppOpts.dictionaryType = 'kmeans_saved';
    ppOpts.dictionaryFile = ...
    '/data/adam2/varun/iccv2011/results/savedDictionaries/kmeans_500_kinect10Feb2011_1.mat';
    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 0.01;
    trainOpts.predictorOpts.svmBias = -1;

  case 'kmeansDictionary_svm2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';

    ppOpts.dictionaryType = 'kmeans';
    ppOpts.numClusters = 500;
    ppOpts.numIters = 100;
    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'kmeansDictionary_svm1_bias_hogMask'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';
    trainOpts.useSegForHogDictionary = true;
    trainOpts.erodeSegWidth = 2;

    ppOpts.dictionaryType = 'kmeans';
    ppOpts.numClusters = 500;
    ppOpts.numIters = 200;
    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = 1;

  case 'kmeansDictionary_svm1_bias_validate'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';

    ppOpts.dictionaryType = 'kmeans_saved';
    ppOpts.dictionaryFile = ...
    '/data2/varun/iccv2011/results/savedDictionaries/kmeans_500_kinect10Feb2011_1.mat';
    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear_validation';
    trainOpts.predictorOpts.numValidationFolds = 3;
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = [0.01 0.1 1 10];
    trainOpts.predictorOpts.svmBias = 1;

  case 'kmeansDictionary_svm1_bias'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';

    ppOpts.dictionaryType = 'kmeans';
    ppOpts.numClusters = 500;
    ppOpts.numIters = 200;
    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = 1;

  case 'kmeansDictionary_svm1_hogMask'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';
    trainOpts.useSegForHogDictionary = true;
    trainOpts.erodeSegWidth = 2;

    ppOpts.dictionaryType = 'kmeans';
    ppOpts.numClusters = 500;
    ppOpts.numIters = 200;
    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;


  case 'kmeansDictionary_svm1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';

    ppOpts.dictionaryType = 'kmeans';
    ppOpts.numClusters = 500;
    ppOpts.numIters = 200;
    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'kmeansDictionary_svm_small2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';

    ppOpts.dictionaryType = 'kmeans';
    ppOpts.numClusters = 500;
    ppOpts.numIters = 1;
    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'hogMask_small'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 10;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';
    trainOpts.useSegForHogDictionary = true;
    trainOpts.erodeSegWidth = 2;

    ppOpts.dictionaryType = 'kmeans';
    ppOpts.numClusters = 100;
    ppOpts.numIters = 10;
    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'kmeansDictionary_svm_small'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 10;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';

    ppOpts.dictionaryType = 'kmeans';
    ppOpts.numClusters = 100;
    ppOpts.numIters = 10;
    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'sparseDictionary_saved_svm1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary_saved';
    ppOpts.dictionaryFile = ...
        '/home/varun/windows/rugbyShared/work/videoCut2/git/junk/ppInfo.mat';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'sparseDictionaryOnly_saved_nonNegative2_hogMask'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary_saved';
    ppOpts.dictionaryFile = '/data2/varun/iccv2011/results/savedDictionaries/sparseDictionary_hogMask_500_kinect10Feb2011_1.mat';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.posD = 1;
    ppOpts.dictionaryParams.posAlpha = 1;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;
    ppOpts.lassoParams.pos = ppOpts.dictionaryParams.posAlpha;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = 1;

  case 'sparseDictionaryOnly_saved_noMask_noPositiveConstraint'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionary';
    trainOpts.useSegForHogDictionary = true;
    trainOpts.erodeSegWidth = 2;
    
    ppOpts.dictionaryType = 'sparseDictionary_saved';
    ppOpts.dictionaryFile = '/data2/varun/iccv2011/results/savedDictionaries/sparseDictionary_500_noMask_noPositiveConstraint_kinect10Feb2011_1.mat';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'sparseDictionaryOnly_nonNegative2_hogMask'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionary';
    trainOpts.useSegForHogDictionary = true;
    trainOpts.erodeSegWidth = 2;
    
    ppOpts.dictionaryType = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.posD = 1;
    ppOpts.dictionaryParams.posAlpha = 1;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.pos = ppOpts.dictionaryParams.posAlpha;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;


  case 'sparseDictionaryOnly_nonNegative2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.posD = 1;
    ppOpts.dictionaryParams.posAlpha = 1;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;
    ppOpts.lassoParams.pos = ppOpts.dictionaryParams.posAlpha;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'sparseDictionaryOnly_saved_unconstrained_max800'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary_saved';
    ppOpts.dictionaryFile = '/data2/varun/iccv2011/results/savedDictionaries/sparseDictionary_unconstrained_500_kinect10Feb2011_3.mat';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = 1;

    trainOpts.maxTrain = 800;

  case 'smallTest_max10'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary_saved';
    ppOpts.dictionaryFile = '/data2/varun/iccv2011/results/savedDictionaries/sparseDictionary_unconstrained_500_kinect10Feb2011_1.mat';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = 1;

    trainOpts.maxTrain = 10;
 
  case 'sparseDictionaryOnly_saved_unconstrained'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary_saved';
    ppOpts.dictionaryFile = '/data2/varun/iccv2011/results/savedDictionaries/sparseDictionary_unconstrained_500_kinect10Feb2011_1.mat';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = 1;

  case 'dictionaryOnly_6'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;


  case 'dictionaryOnly_5'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2000;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;


  case 'dictionaryOnly_4'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 1500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'dictionaryOnly_3'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 1000;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'dictionaryOnly_2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;


  case 'dictionaryOnly_1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 100;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'dictionaryOnly_cellSize_2_localSize5'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 16;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'dictionaryOnly_cellSize_2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 16;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'dictionaryOnly_localSize_3'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 6;
    trainOpts.localHogW = 6;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;


  case 'dictionaryOnly_localSize_2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;


  case 'dictionaryOnly_localSize_1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;


  case 'dictionaryOnly_sparseVary_1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'dictionaryOnly_sparseVary_2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .5;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'dictionaryOnly_sparseVary_3'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 1;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'dictionaryOnly_sparseVary_4'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 10;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'sparseDictionaryOnly_unconstrained'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'sparseDictionaryOnly_nonNegative'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.posD = 1;
    ppOpts.dictionaryParams.posAlpha = 1;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'sparseDictionary_saved_omp2_svm2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary_omp_saved';
    ppOpts.dictionaryFile = ...
    '/data2/varun/iccv2011/results/savedDictionaries/sparseDictionary_omp_500_kinect10Feb2011_2.mat';
    ppOpts.dictionaryParams.mode = 3;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 18; % L0 norm upper bound
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.ompParams.L = ppOpts.dictionaryParams.lambda;
    ppOpts.ompParams.eps = 0;
    ppOpts.ompParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = 1;

  case 'sparseDictionary_saved_omp2_svm1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary_omp_saved';
    ppOpts.dictionaryFile = ...
    '/data2/varun/iccv2011/results/savedDictionaries/sparseDictionary_omp_500_kinect10Feb2011_2.mat';
    ppOpts.dictionaryParams.mode = 3;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 18; % L0 norm upper bound
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.ompParams.L = ppOpts.dictionaryParams.lambda;
    ppOpts.ompParams.eps = 0;
    ppOpts.ompParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = 1;


  case 'sparseDictionary_saved_omp_svm1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary_omp_saved';
    ppOpts.dictionaryFile = ...
    '/data2/varun/iccv2011/results/savedDictionaries/sparseDictionary_omp_500_kinect10Feb2011_1.mat';
    ppOpts.dictionaryParams.mode = 3;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 5; % L0 norm upper bound
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.ompParams.L = ppOpts.dictionaryParams.lambda;
    ppOpts.ompParams.eps = 0;
    ppOpts.ompParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = 1;

  case 'sparseDictionary_omp_svm1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary_omp';
    ppOpts.dictionaryParams.mode = 3;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 5; % L0 norm upper bound
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.ompParams.L = ppOpts.dictionaryParams.lambda;
    ppOpts.ompParams.eps = 0;
    ppOpts.ompParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'sparseDictionaryOnly_omp2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary_omp';
    ppOpts.dictionaryParams.mode = 3;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 18; % L0 norm upper bound
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.ompParams.L = ppOpts.dictionaryParams.lambda;
    ppOpts.ompParams.eps = 0;
    ppOpts.ompParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'sparseDictionaryOnly_omp'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.enableLearner = false;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary_omp';
    ppOpts.dictionaryParams.mode = 3;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 5; % L0 norm upper bound
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.ompParams.L = ppOpts.dictionaryParams.lambda;
    ppOpts.ompParams.eps = 0;
    ppOpts.ompParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'sparseDictionary_svm1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'sparseDictionary_svm_small'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 4;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'dictionary';
    
    ppOpts.dictionaryType = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 100;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  otherwise
    error('Invalid options string: %s\n',optsString);
end

opts.hogOpts = hogOpts;
opts.trainOpts = trainOpts;
