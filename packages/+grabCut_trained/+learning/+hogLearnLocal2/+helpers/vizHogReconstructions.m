function vizHogReconstructions()
% Code to visualize 3 kinds of hogs:
% hog using the ground truth mask
% hog using all the image data
% hog usinga all the image data, but reconstructed from dictionary

import grabCut_trained.learning.hogLearnLocal2.*;

cwd=miscFns.extractDirPath(mfilename('fullpath'));

trainOutDir=[cwd '../../../../../results/grabCut_trained/train/hogViz_nmfWithMask_trainSet/'];

%params.evalOpts_string = 'try1';
params.evalOpts_string = 'kinect_10Feb2011';
params.trainMethod = 'hogLearnLocal2';
params.trainMethod_optsString = 'sparseDictionaryOnly_saved_nonNegative2_hogMask';
params.debugLevel = 10;

testBench_cmd='testBench.getTests_grabCut_kinect10Feb2011_1_train_1()';

%testBench_cmd='testBench.getTests_grabCut_kinect10Feb2011_1_test_1()';
%testBench_cmd='testBench.getTests_grabCut_kinect10Feb2011_1_train_1()';
%testBench_cmd='testBench.getTests_grabCut_small()';
%testBench_cmd='testBench.getTests_grabCut_h3d_train_1()';
%testBench_cmd='testBench.getTests_grabCut_kinect10Feb2011_1_train_small()';

skipSize = 25; % Use this to skip images and generate a small subset output

assert(strcmp(params.trainMethod,'hogLearnLocal2'));
testBenchOpts = eval(testBench_cmd);
if(~exist(trainOutDir,'dir')), mkdir(trainOutDir); end;

opts = makeOpts(params.trainMethod_optsString);
evalOpts = testBench.evalOpts_grabCut(params.evalOpts_string);
gtDir = evalOpts.gtDir;
save([trainOutDir 'trainOpts.mat'],'opts');

fH = fopen([trainOutDir 'trainOpts.txt'],'w');
miscFns.printClass(opts.trainOpts,fH);
miscFns.printClass(opts.hogOpts,fH);
fclose(fH);

trainingDataDir = [trainOutDir 'trainingData/'];
mkdir(trainingDataDir);

images = textread(testBenchOpts.imgList,'%s');
imgDir = testBenchOpts.imgDir;
labelDir = testBenchOpts.labelDir;

trainOpts = opts.trainOpts;
hogOpts = opts.hogOpts;

debugOpts = struct();
debugOpts.debugLevel = params.debugLevel;
debugOpts.debugDir = [trainOutDir 'visualizations/'];
debugOpts.debugPrefix = '';
mkdir(debugOpts.debugDir);

[ppInfo,hogByImg,hogBBoxesByImg,segMaskByImg,segBBoxesByImg] = ...
    extractBothHogs(imgDir,labelDir,gtDir,images,hogOpts,trainOpts,debugOpts,skipSize);
save([trainingDataDir 'ppInfo.mat'],'ppInfo');

reconstructAllHogs(ppInfo,hogByImg,segMaskByImg,hogBBoxesByImg,...
                segBBoxesByImg,trainOpts,hogOpts,debugOpts,images,skipSize);

function reconstructAllHogs(ppInfo,hogByImg,segMaskByImg,hogBBoxesByImg,...
                segBBoxesByImg,trainOpts,hogOpts,debugOpts,images,skipSize)

import grabCut_trained.learning.hogLearnLocal2.*;

localSegMaskSize = getLocalSegMaskSize(trainOpts);
nImages = numel(hogByImg);

hogDim = getHogDimensionality(hogOpts,trainOpts);
for i=1:skipSize:nImages
  objectId = grabCut_trained.dataset.extractObjectIdentifier(images{i});
  hogFull = hogByImg{i};
  hogBoxes = hogBBoxesByImg{i};

  hogs = zeros(hogDim,size(hogBoxes,2));
  for j=1:size(hogBoxes,2)
    tmpHog = hogFull(hogBoxes(1,j):hogBoxes(2,j),hogBoxes(3,j):hogBoxes(4,j),:);
    hogs(:,j) = tmpHog(:);
  end
  
  featuresByBox = applyPreProcess(hogs,trainOpts,ppInfo);
  hogReconstructions = reconstructHogs(featuresByBox,trainOpts,ppInfo);
  
  hogFull2 = zeros(size(hogFull));
  counts = zeros(size(hogFull));

  for j=1:size(hogReconstructions,2)
    tmpHog = hogReconstructions(:,j);
    tmpHog = reshape(tmpHog,[trainOpts.localHogH trainOpts.localHogW ...
                     hogDim/(trainOpts.localHogH*trainOpts.localHogW)]);
    hogFull2(hogBoxes(1,j):hogBoxes(2,j),hogBoxes(3,j):hogBoxes(4,j),:) = ...
        hogFull2(hogBoxes(1,j):hogBoxes(2,j),hogBoxes(3,j):hogBoxes(4,j),:) + ...
        tmpHog; 
    counts(hogBoxes(1,j):hogBoxes(2,j),hogBoxes(3,j):hogBoxes(4,j),:) = ...
        counts(hogBoxes(1,j):hogBoxes(2,j),hogBoxes(3,j):hogBoxes(4,j),:) + 1;
  end
  hogFull2 = hogFull2./counts;
  visHog = drawModifiedHog(hogFull2,hogOpts);
  imwrite(visHog,[debugOpts.debugDir objectId '_hogReconstructed.png']);
  fprintf('Saved reconstructed hog for %04d/%04d img\r',i,length(images));
end


function [ppInfo,hogByImg,hogBBoxesByImg,segMaskByImg,segBBoxesByImg] =...
    extractBothHogs(imgDir,labelDir,gtDir,images,hogOpts,trainOpts,debugOpts,skipSize)

import grabCut_trained.learning.hogLearnLocal2.*;

N = length(images);
hogByImg = cell(1,N);
segMaskByImg = cell(1,N);
hogBBoxesByImg = cell(1,N);
segBBoxesByImg = cell(1,N);

localSegMaskSize = getLocalSegMaskSize(trainOpts);

fprintf('Extracting hogs and segmasks ...\n');
stTime = clock;
for i=1:skipSize:length(images)
  objectId = grabCut_trained.dataset.extractObjectIdentifier(images{i});

  tmpDebugOpts = debugOpts;
  tmpDebugOpts.debugLevel = 0;

  labelFile = [labelDir objectId '.png'];
  labelImg = imread(labelFile);
  [bbox,bboxAspect] = getBoxAspectRatio(labelImg);
  img = imread([imgDir grabCut_trained.dataset.extractImgName(images{i})]);
  img = im2double(img);
  gt = imread([gtDir objectId '.png']);
  [hogByImg{i},segMaskByImg{i},hogBBoxesByImg{i},segBBoxesByImg{i}] ...
      = getLocalHogAndSeg(img,gt,bbox,trainOpts,hogOpts,tmpDebugOpts,localSegMaskSize,...
        true);
  % ----- Draw these hogs ------------
  [bbox,xx] = snapBBoxToSize(bbox,trainOpts,hogOpts);
  localImg = cpp.mex_extractBBox(img,int32(bbox-1));
  visHog = drawModifiedHog(hogByImg{i},hogOpts);
  localImg = imresize(localImg,size(visHog));
  imwrite(visHog,[debugOpts.debugDir objectId '_hogMask.png']);
  imwrite(localImg,[debugOpts.debugDir objectId '_localImg.jpg']);
  fprintf('Saved hog with mask for %04d/%04d img\r',i,length(images));
end
fprintf('Time taken to extract hogs for %d images = %.2f minutes\n',...
    length(images),etime(clock,stTime)/60);

fprintf('Preprocessing hogs ...\n');
stTime = clock;
ppInfo = preProcessHogs(hogByImg,hogBBoxesByImg,trainOpts,hogOpts);
fprintf('Time taken to pre process hogs (dictionary/kmeans) = %.2f minutes\n',...
    etime(clock,stTime)/60);

% Get the hogs again without the gt mask 
hogByImg = cell(1,N);
segMaskByImg = cell(1,N);
hogBBoxesByImg = cell(1,N);
segBBoxesByImg = cell(1,N);
for i=1:skipSize:numel(images)
  objectId = grabCut_trained.dataset.extractObjectIdentifier(images{i});

  tmpDebugOpts = debugOpts;
  tmpDebugOpts.debugLevel = 0; % switch off debugging for this one

  labelFile = [labelDir objectId '.png'];
  labelImg = imread(labelFile);
  [bbox,bboxAspect] = getBoxAspectRatio(labelImg);
  img = imread([imgDir grabCut_trained.dataset.extractImgName(images{i})]);
  img = im2double(img);
  gt = imread([gtDir objectId '.png']);
  [hogByImg{i},segMaskByImg{i},hogBBoxesByImg{i},segBBoxesByImg{i}] ...
      = getLocalHogAndSeg(img,gt,bbox,trainOpts,hogOpts,tmpDebugOpts,localSegMaskSize,...
       false);
   % ----- Draw these hogs ------------
  visHog = drawModifiedHog(hogByImg{i},hogOpts);
  imwrite(visHog,[debugOpts.debugDir objectId '_hogNoMask.png']);
  fprintf('Saved hog without mask for %04d/%04d img\r',i,length(images));
end
