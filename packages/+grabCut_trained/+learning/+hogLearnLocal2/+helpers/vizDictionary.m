function vizDictionary(inDir)

import grabCut_trained.learning.hogLearnLocal2.*;

cwd=miscFns.extractDirPath(mfilename('fullpath'));

%inDir = [cwd '../../../../../results/grabCut_trained/train/hogLearning/'];
defaultInDir = ['/data2/varun/iccv2011/results/train/hogLearnLocal_testing_kinect/'];
%outDir = [cwd '../../../../../results/grabCut_trained/train/hogLearning/debug/dictionary/'];

if(~exist('inDir','var')), inDir = defaultInDir; end;
outDir = [inDir 'debug/dictionary/'];

if(~exist(outDir,'dir')), mkdir(outDir); end;

tmpLoad = load([inDir 'trainOpts.mat']);
hogOpts = tmpLoad.opts.hogOpts;
trainOpts = tmpLoad.opts.trainOpts;

assert(strcmp(trainOpts.hogPreProcess,'sparseDictionary'),'Invalid preprocess\n');

tmpLoad = load([inDir 'trainingData/ppInfo.mat']);
ppInfo = tmpLoad.ppInfo;
basis = ppInfo.sparseDictionary;

assert(size(basis,1)==getHogDimensionality(hogOpts,trainOpts),'Incorrect hog size\n');
localHogH = trainOpts.localHogH;
localHogW = trainOpts.localHogW;
for i=1:size(basis,2)
  hog=basis(:,i);
  assert(mod(length(hog),localHogH*localHogW)==0,'Incorrect hog size\n');
  hog=reshape(hog,[localHogH localHogW length(hog)/(localHogH*localHogW)]);
  visHog = drawModifiedHog(hog,hogOpts);
  imwrite(visHog,[outDir sprintf('basis_%03d.png',i)]);
end

