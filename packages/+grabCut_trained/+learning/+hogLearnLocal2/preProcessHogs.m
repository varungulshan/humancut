function ppInfo = preProcessHogs(hogByImg,hogBBoxesByImg,trainOpts,hogOpts)
% TO apply pre-processing such as dimensionality reduction
% ppInfo.featureSize, gives dimensionality of local hog descriptor

switch(trainOpts.hogPreProcess)
  case 'pca',
    hogs = getAllHogs(hogByImg,hogBBoxesByImg,trainOpts,hogOpts);
    ppInfo = pcaPreprocess(hogs,trainOpts.hogPreProcessOpts);
  case 'dictionary'
    hogs = getAllHogs(hogByImg,hogBBoxesByImg,trainOpts,hogOpts);
    ppInfo = dictionaryProcess(hogs,trainOpts.hogPreProcessOpts);
  case 'none',
    ppInfo = struct();
    ppInfo.featureSize = ...
        grabCut_trained.learning.hogLearnLocal2.getHogDimensionality(hogOpts,trainOpts);
  otherwise
    error('Invalid pre process method: %s\n',trainOpts.hogPreProcess);
end

function ppInfo = dictionaryProcess(hogs,opts)
  switch(opts.dictionaryType)
    case 'kmeans'
      ppInfo = getKmeansDictionary(hogs,opts);
    case 'sparseDictionary'
      ppInfo = getSparseDictionary(hogs,opts);
    case 'sparseDictionary_omp'
      ppInfo = getSparseDictionary_omp(hogs,opts);
    case {'sparseDictionary_saved','kmeans_saved','sparseDictionary_omp_saved'}
      tmpLoad = load(opts.dictionaryFile);
      ppInfo = tmpLoad.ppInfo;
  end
  
function ppInfo = getKmeansDictionary(hogs,opts)

  [xx,meanFeatures,xx,labels,xx] = grabCut_trained.learning.hogLearnLocal2.COMKMeans(...
      hogs,[],opts.numClusters,opts.numIters,1);
  
  ppInfo.dictionary = meanFeatures;
  ppInfo.featureSize = size(ppInfo.dictionary,2);

function ppInfo = getSparseDictionary_omp(hogs,opts)

  dictionaryParams = opts.dictionaryParams;
  reset(RandStream.getDefaultStream); % To reproduce randomness
  randomHogs = hogs(:,grabCut_trained.dataset.COMuniquerand(dictionaryParams.K,[1 size(hogs,2)]));
  dictionaryParams.D = randomHogs;
 
  dictionary = mexTrainDL(hogs,dictionaryParams);

  % Normalize column vectors to have unit norm, needed for omp
  l2NormColumns = sqrt(sum(dictionary.*dictionary,1));
  ppInfo.dictionary = dictionary./repmat(l2NormColumns,[size(dictionary,1) 1]);
  ppInfo.featureSize = size(ppInfo.dictionary,2);
  
function ppInfo = getSparseDictionary(hogs,opts)

  dictionaryParams = opts.dictionaryParams;
  reset(RandStream.getDefaultStream); % To reproduce randomness
  randomHogs = hogs(:,grabCut_trained.dataset.COMuniquerand(dictionaryParams.K,[1 size(hogs,2)]));
  dictionaryParams.D = randomHogs;
  if(opts.useFasterOne)
    ppInfo.dictionary = mexTrainDL_Memory(hogs,dictionaryParams);
  else
    ppInfo.dictionary = mexTrainDL(hogs,dictionaryParams);
  end
  ppInfo.featureSize = size(ppInfo.dictionary,2);

function ppInfo = pcaPreprocess(hogs,opts)

  newDim = opts.dimensionality;
  assert(newDim <= size(hogs,1));
  
  hogs = hogs';
  meanHog = mean(hogs,1)';
  [basis,newHogs] = princomp(hogs);
  
  newHogs = newHogs(:,1:newDim);
  basis = basis(:,1:newDim);
  
  hogs = newHogs';
  ppInfo.meanHog = meanHog;
  ppInfo.basis = basis;
  ppInfo.featureSize = size(basis,2);

function hogs = getAllHogs(hogByImg,hogBBoxesByImg,trainOpts,hogOpts)

  numHogs = 0;
  for i=1:numel(hogByImg)
    numHogs = numHogs + size(hogBBoxesByImg{i},2);
  end
  hogs = zeros(grabCut_trained.learning.hogLearnLocal2.getHogDimensionality(hogOpts,trainOpts),numHogs);
  idxNext = 1;
  for i=1:numel(hogByImg)
    hogBoxes = hogBBoxesByImg{i};
    currentHog = hogByImg{i};
    for j=1:size(hogBoxes,2)
      tmpHog = currentHog(hogBoxes(1,j):hogBoxes(2,j),hogBoxes(3,j):hogBoxes(4,j),:);
      hogs(:,idxNext) = tmpHog(:);
      idxNext = idxNext+1;
    end
  end
