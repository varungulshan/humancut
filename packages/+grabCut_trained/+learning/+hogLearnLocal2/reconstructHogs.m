function hogs = reconstructHogs(features,trainOpts,ppInfo)

switch(trainOpts.hogPreProcess)
  case {'dictionary'}
    hogs = applyDictionary(features,trainOpts.hogPreProcessOpts,ppInfo);
  case 'none',
    % do nothing
  otherwise
    error('Invalid reconstruction method: %s\n',trainOpts.hogPreProcess);
end

function hogs = applyDictionary(features,opts,ppInfo)

  hogs = ppInfo.dictionary*features;
