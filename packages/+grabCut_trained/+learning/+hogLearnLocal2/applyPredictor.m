function svmOut = applyPredictor(features,trainOpts,predictorInfo,segSize)

switch(trainOpts.predictorType)
  case {'liblinear','liblinear_validation'}
    svmOut = applyLibLinear(features,trainOpts,predictorInfo,segSize);
  otherwise
    error('Predictor type: %s not yet supported\n',trainOpts.predictorType);
end

function svmOut = applyLibLinear(features,trainOpts,info,segSize)

[outLabels,xx,svmOut] = liblinear.predict(zeros(size(features,2),1),features,info.model,...
            '','col');
svmOut = info.model.Label(1)*svmOut; % To get the sign correct
assert(prod(segSize)==numel(svmOut),'Invalid prediction size\n');
svmOut = reshape(svmOut,segSize);
