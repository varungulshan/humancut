function featuresStruct = getPixelFeatures(hogFull,hogBoxes,segMask,localSegMaskSize,segBoxes,trainOpts,hogOpts,ppInfo)

import grabCut_trained.learning.hogLearnLocal2.*;

hogs = zeros(getHogDimensionality(hogOpts,trainOpts),size(hogBoxes,2));
for i=1:size(hogBoxes,2)
  tmpHog = hogFull(hogBoxes(1,i):hogBoxes(2,i),hogBoxes(3,i):hogBoxes(4,i),:);
  hogs(:,i) = tmpHog(:);
end

featuresByBox = applyPreProcess(hogs,trainOpts,ppInfo);
% the above can be a sparse vector
assert(ppInfo.featureSize==size(featuresByBox,1),'Incorrect feature size\n');

numEntries = prod(localSegMaskSize)*nnz(featuresByBox);
% numEntries is an upper bound on number of entries
yIdxs = zeros(numEntries,1);
xIdxs = zeros(numEntries,1);
ftrValues = zeros(numEntries,1);

pixelIdxMap = zeros(size(segMask));
validSegMask = (segMask~=0.5);
pixelIdxMap(validSegMask) = [1:nnz(validSegMask)];
localPositionMap = reshape([1:prod(localSegMaskSize)],localSegMaskSize);
idxNext = 1;
for i=1:size(featuresByBox,2)
  pixelIds = pixelIdxMap(segBoxes(1,i):segBoxes(2,i),segBoxes(3,i):segBoxes(4,i));
  localIds = localPositionMap(pixelIds~=0);
  pixelIds = pixelIds(pixelIds~=0);
  curFeature = featuresByBox(:,i);
  [curFtrPositions,xx,curFtrValues]=find(curFeature);
  numFtrValues = numel(curFtrValues);
  for j=1:numel(pixelIds)
    idxRange = idxNext:(idxNext+numFtrValues-1);
    xIdxs(idxRange) = pixelIds(j);
    yIdxs(idxRange) = curFtrPositions+(localIds(j)-1)*ppInfo.featureSize;
    ftrValues(idxRange) = curFtrValues;
    idxNext = idxNext+numFtrValues;
  end
end

numEntriesActual = idxNext-1;
assert(numEntriesActual <= numEntries, 'Bug in estimating numEntries\n');
xIdxs = xIdxs(1:numEntriesActual);
yIdxs = yIdxs(1:numEntriesActual);
ftrValues = ftrValues(1:numEntriesActual);

featuresStruct = struct('xIdxs',[],'yIdxs',[],'ftrValues',[]);
featuresStruct.xIdxs=xIdxs;
featuresStruct.yIdxs=yIdxs;
featuresStruct.ftrValues=ftrValues;
