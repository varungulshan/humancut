function features = applyPreProcess(hogs,trainOpts,ppInfo)

switch(trainOpts.hogPreProcess)
  case 'pca',
    features = applyPCA(hogs,trainOpts.hogPreProcessOpts,ppInfo);
  case {'dictionary'}
    features = applyDictionary(hogs,trainOpts.hogPreProcessOpts,ppInfo);
  case 'none',
    % do nothing
  otherwise
    error('Invalid pre process method: %s\n',trainOpts.hogPreProcess);
end

function features = applyDictionary(hogs,opts,ppInfo)

switch(opts.dictionaryType)
  case {'kmeans','kmeans_saved'}
    features = getKmeansFeatures(hogs,ppInfo.dictionary);
  case {'sparseDictionary','sparseDictionary_saved'}
    features = mexLasso(hogs,ppInfo.dictionary,opts.lassoParams);
  case {'sparseDictionary_omp','sparseDictionary_omp_saved'}
    features = mexOMP(hogs,ppInfo.dictionary,opts.ompParams);
end

function features = getKmeansFeatures(hogs,dictionary)
  import grabCut_trained.learning.hogLearnLocal2.*;
  
  labels = cpp.MEXfindnearestl2(hogs,dictionary);
  features = sparse(size(dictionary,2),size(hogs,2));
  numHogs = size(hogs,2);
  idxOnes = [0:(numHogs-1)]*size(dictionary,2) + double(labels);
  features(idxOnes) = 1;

function hog = applyPCA(hog,opts,ppInfo)

hog = hog - ppInfo.meanHog;
hog = ppInfo.basis'*hog;
