function hogImg = drawHog(hog,hogOpts)

% Doesnt matter what hogOpts are actually

% Add up hog channels
hog = hog(:,:,1:9)+hog(:,:,(1:9)+9)+hog(:,:,(1:9)+18);
hogScale = max(hog(:));
hogImg = hogCode.HOGpicture(hog,hogOpts.visHogBlockSize)/hogScale;
