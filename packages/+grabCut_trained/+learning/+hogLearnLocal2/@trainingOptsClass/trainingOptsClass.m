% Class defines options for training on hogLearnGlobal
classdef trainingOptsClass
  properties (SetAccess=public, GetAccess=public)
    hogCellsInHeight % number of hog cells along the height dimension
    % this implicitly decides the scale of the hogcell
    localSegMaskLongerDimension % number of pixels in longer dimension of segMask
    % decides dimensionality of the output space

    localHogH % number of vertical cells for local hog 
    localHogW % number of horizontal cells for local hog 

    hogPreProcess % set to pca to reduce dimensionality
    hogPreProcessOpts % options for preprocessing (structure)

    predictorType % 'kmeans', 'svmVedaldi'
    predictorOpts % depends on classifierType

    useSegForHogDictionary % uses the segmentation to limit the hog gradients to fg
    % while using the hog to build dictionary elements
    erodeSegWidth % number of pixels to erode segmentation by when limiting
    % hog gradients to segmentation

    enableLearner % To enable svm learning, keep it false to learn dictionary only
    maxTrain
  end

  methods
    function obj=trainingOptsClass()
      obj.hogCellsInHeight = 16;
      obj.localSegMaskLongerDimension = 40;
      obj.hogPreProcess = 'none';
      obj.hogPreProcessOpts = struct();
      obj.localHogH = 4;
      obj.localHogW = 4;
      obj.predictorType = 'kmeans';
      obj.predictorOpts.numClusters = 100;
      obj.predictorOpts.numIters = 200;
      obj.useSegForHogDictionary = false;
      obj.erodeSegWidth = 4;
      obj.enableLearner = true;
      obj.maxTrain = -1; % Use all
    end
  end

end % of classdef
