function [hogFull,segMaskFull,hogBBoxes,segBBoxes] = ...
    getLocalHogAndSeg(img,gt,bbox,trainOpts,hogOpts,...
    debugOpts,localMaskSize,useSegForHog)
% hogFull is the hog for the image window, and same for segMaskFull
% hogFull is D x H x W array (D=dimensionality,H,W are number of hog cells)
% hogBBoxes is [4 x N] array, of type [y1 y2 x1 x2] which indicates indices
% to extract from hogFull to get a localHog (ie, hogBBoxes(:,y1:y2,x1:x2)
% same for segMaskBBoxes, segBBoxes(y1:y2,x1:x2) gives a seg

import grabCut_trained.learning.hogLearnLocal2.*;

if(~exist('useSegForHog','var')), useSegForHog = false; end;

[bbox,hogBoxSize] = snapBBoxToSize(bbox,trainOpts,hogOpts);
numCellsH = hogBoxSize(1)/hogOpts.cellSize;
numCellsW = hogBoxSize(2)/hogOpts.cellSize;
segCellSizeH = localMaskSize(1)/trainOpts.localHogH;
segCellSizeW = localMaskSize(2)/trainOpts.localHogW;

segMaskSize = [localMaskSize(1) localMaskSize(2)] + ...
    round([(numCellsH-trainOpts.localHogH)*segCellSizeH ...
           (numCellsW-trainOpts.localHogW).*segCellSizeW]);

tmpSegMask = cpp.mex_extractBBox(gt,int32(bbox-1));
tmpSegMask = imresize(tmpSegMask,segMaskSize,'nearest');
segMaskFull = zeros(size(tmpSegMask));
segMaskFull(tmpSegMask==128)=0.5;
segMaskFull(tmpSegMask==255)=1;

localImg = cpp.mex_extractBBox(img,int32(bbox-1));
localImg = imresize(localImg,[hogBoxSize(1) hogBoxSize(2)],'bilinear');
if(~useSegForHog)
  hog = hogCode.mex_getHog(localImg,hogOpts.cellSize);
else
  tmpSegMask = cpp.mex_extractBBox(gt,int32(bbox-1));
  tmpSegMask = imresize(tmpSegMask,[hogBoxSize(1) hogBoxSize(2)],'nearest');
  stEl = strel('disk',trainOpts.erodeSegWidth);
  tmpSegMask = imdilate(tmpSegMask==255,stEl);
  hog = hogCode.mex_getHogWithMask(localImg,hogOpts.cellSize,tmpSegMask);
end

if(debugOpts.debugLevel>=10)
  visHog = drawHog(hog,hogOpts);
  miscFns.saveDebug(debugOpts,visHog,'hog.png');
  miscFns.saveDebug(debugOpts,imresize(localImg,size(visHog)),'img.jpg');
  miscFns.saveDebug(debugOpts,imresize(tmpSegMask,size(visHog),'nearest'),'seg.png');
end
% hog will be a H x W x (27+4) matrix, H = number of vertical cells, W = horiz cells
hog = selectHogType(hog,hogOpts);
%hogFull = shiftdim(hog,2); % to make it vertical
hogFull = hog;

yStart = [1:(numCellsH-trainOpts.localHogH+1)];
xStart = [1:(numCellsW-trainOpts.localHogW+1)];

[x1,y1] = meshgrid(xStart,yStart);
x1=x1(:)';
y1=y1(:)';
hogBBoxes = zeros(4,numel(x1));
hogBBoxes(1,:) = y1;
hogBBoxes(2,:) = y1+trainOpts.localHogH-1;
hogBBoxes(3,:) = x1;
hogBBoxes(4,:) = x1+trainOpts.localHogW-1;

segBBoxes =zeros(4,numel(x1));
segBBoxes(1,:) = round(1+(y1-1)*segCellSizeH);
segBBoxes(2,:) = segBBoxes(1,:)+localMaskSize(1)-1;
segBBoxes(3,:) = round(1+(x1-1)*segCellSizeW);
segBBoxes(4,:) = segBBoxes(3,:)+localMaskSize(2)-1;

