function addFeatures(obj,xIdxs,yIdxs,ftrValues)

obj.mexAddFeatures(obj.featureMatrixPtr,obj.numFeatures,xIdxs,yIdxs,...
    ftrValues,obj.svmOpts.svmBias,obj.featureDim);
