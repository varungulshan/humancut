#include "mex.h"
#include <iostream>
#include <linear.h>

using namespace std;

typedef unsigned int uint;
typedef long long int int64;

inline void myAssert(bool condition,char *msg){
  if(!condition){
    mexErrMsgTxt(msg);
  }
}

inline void *myMalloc(size_t numBytes){
  void* ans = mxMalloc(numBytes);
  mexMakeMemoryPersistent(ans);
  return ans;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters
      matrixPtr (int8 class, 8 x 1 , 8 = sizeof(void*))
      numFeatures (int64)
      bias (double)
      featureDim (int64)
     Output: 
      No output, modifications are made to matrixPtr
  */

  myAssert(nrhs == 4, "4 input arguments expected.");
  if (nlhs != 0)
    mexErrMsgTxt("0 output arguments expected.");

  myAssert(mxGetClassID(prhs[0])==mxINT8_CLASS,
      "Invalud class for matrixPtr\n");
  myAssert(mxGetClassID(prhs[1])==mxINT64_CLASS,
      "Invalid class for numFeatures\n");
  myAssert(mxGetClassID(prhs[2])==mxDOUBLE_CLASS,
      "Invalid class for bias\n");
  myAssert(mxGetClassID(prhs[3])==mxINT64_CLASS,
      "Invalid class for featureDim\n");
  myAssert(sizeof(size_t)==sizeof(int64),"This code only works on 64 bit machines\n");

  // Load the variables
  int numBytesPtr = sizeof(feature_node**);
  feature_node **matrix;
  memcpy(&matrix,mxGetData(prhs[0]),numBytesPtr);

  int64 numFeatures = *(int64*)mxGetData(prhs[1]);
  myAssert(mxGetNumberOfElements(prhs[1])==1,"Invalid number of elements in numFeatures\n");
  myAssert(mxGetNumberOfElements(prhs[2])==1,"Invalid number of elements in bias\n");
  myAssert(mxGetNumberOfElements(prhs[3])==1,"Invalid number of elements in featureDim\n");

  double bias = *mxGetPr(prhs[2]);
  int64 featureDim = *(int64*)mxGetData(prhs[3]);
  
  bool anyNull = false;
  for(int64 i=0;i<numFeatures;i++){
    if(matrix[i]==NULL){
      anyNull = true;
      feature_node *lastEntry;
      if(bias>=0){
        matrix[i] = (feature_node*)myMalloc(sizeof(feature_node)*2);
        lastEntry = matrix[i];
        lastEntry->value = bias;
        lastEntry->index = featureDim+1;
        lastEntry++;
      }else{
        matrix[i] = (feature_node*)myMalloc(sizeof(feature_node)*1);
        lastEntry = matrix[i];
      }
      lastEntry->value = 0;
      lastEntry->index = -1;
    }
  }

  if(anyNull){
    mexPrintf("Found alteast one empty column while closing sparse matrix, could be a bug\n");
  }
}
