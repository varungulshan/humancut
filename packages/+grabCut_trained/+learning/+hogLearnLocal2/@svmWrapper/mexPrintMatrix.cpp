#include "mex.h"
#include <iostream>
#include <linear.h>

using namespace std;

typedef unsigned int uint;
typedef long long int int64;

inline void myAssert(bool condition,char *msg){
  if(!condition){
    mexErrMsgTxt(msg);
  }
}

inline void *myMalloc(size_t numBytes){
  void* ans = mxMalloc(numBytes);
  mexMakeMemoryPersistent(ans);
  return ans;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters
     matrixPtr (int8 class, 8 x 1 , 8 = sizeof(void*))
     numFeatures (int64)
     numColumnsRange (1 x 2 array) (double) [optional]
Output: 
No output, modifications are made to matrixPtr
   */
  myAssert(sizeof(size_t)==sizeof(int64),"This code only works on 64 bit machines\n");

  myAssert(nrhs >= 2 && nrhs <=3, "Atleast 2 input arguments expected.");
  if (nlhs != 0)
    mexErrMsgTxt("0 output arguments expected.");

  myAssert(mxGetClassID(prhs[0])==mxINT8_CLASS,
      "Invalid class for matrixPtr\n");
  myAssert(mxGetClassID(prhs[1])==mxINT64_CLASS,
      "Invalid class for numFeatures\n");


  // Load the variables
  int numBytesPtr = sizeof(feature_node**);
  feature_node **matrix;
  memcpy(&matrix,mxGetData(prhs[0]),numBytesPtr);

  int64 numFeatures = *(int64*)mxGetData(prhs[1]);
  int64 colStart = 0;
  int64 colEnd = (numFeatures-1);
  if(nrhs ==3){
    myAssert(mxGetClassID(prhs[2])==mxDOUBLE_CLASS,
      "Invalid class for numColumns\n");
    myAssert(mxGetNumberOfElements(prhs[2])==2,"Incorrect size of range\n");
    double *tmp = mxGetPr(prhs[2]);
    colStart = (int64)(tmp[0]-1);
    colEnd = (int64)(tmp[1]-1);
  }

  mexPrintf("Printing columns [%d,%d] of sparse matrix with %d columns\n",
      (int)colStart+1,(int)colEnd+1,(int)numFeatures);
  for(int64 i=colStart;i<=colEnd;i++){
    feature_node *column = matrix[i];
    while(column->index!=-1){
      mexPrintf("(%d,%d) = %.4f\n",column->index,(int)i+1,column->value);
      column++;
    }
  }

}
