% Class defines options for training on hogLearnGlobal
classdef svmWrapper < handle
  properties (SetAccess=private, GetAccess=public)
    numFeatures
    featureDim
    featureMatrixPtr
    svmOpts
  end

  properties (SetAccess=public, GetAccess=public)
    labels % Needs to be set separately
  end


  methods
    function obj=svmWrapper(numFeatures,featureDim,svmOpts)
      obj.labels = [];
      obj.numFeatures = numFeatures;
      obj.featureDim = featureDim;
      obj.svmOpts = svmOpts;
      obj.featureMatrixPtr = ...
          obj.mexInitSparseMatrix(numFeatures,featureDim);
    end
    delete(obj);
    addFeatures(obj,xIdxs,yIdxs,ftrValues); 
    closeMatrix(obj);
    info = train(obj); 
  end

  methods(Static,Access=public)
    matrixPtr = mexInitSparseMatrix(numFeatures,featureDim);
    mexFreeSparseMatrix(matrixPtr,numFeatures);
    mexAddFeatures(matrixPtr,numFeatures,xIdxs,yIdxs,ftrValues,...
        svmBias,featureDim);
    compile_mex();
    mexCloseMatrix(featureMatrixPtr,numFeatures,...
        svmBias,featureDim);
    mexPrintMatrix(featureMatrixPtr,numFeatures,varargin);
  end

end % of classdef
