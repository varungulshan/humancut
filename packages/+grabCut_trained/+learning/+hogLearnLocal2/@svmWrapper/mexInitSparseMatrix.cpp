#include "mex.h"
#include <iostream>
#include <linear.h>

typedef unsigned int uint;
typedef long long int int64;

inline void myAssert(bool condition,char *msg){
  if(!condition){
    mexErrMsgTxt(msg);
  }
}

inline void *myMalloc(size_t numBytes){
  void* ans = mxMalloc(numBytes);
  mexMakeMemoryPersistent(ans);
  return ans;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters
     numFeatures (int64)
     featureDim (int64)
     Output: 
     ptr to sparse matrix
  */

  myAssert(nrhs == 2, "2 input arguments expected.");
  if (nlhs != 1)
    mexErrMsgTxt("1 output arguments expected.");

  myAssert(mxGetClassID(prhs[0])==mxINT64_CLASS,
      "Invalud class for numFeatures\n");
  myAssert(mxGetClassID(prhs[1])==mxINT64_CLASS,
      "Invalid class for featureDim\n");

  int64 numFeatures = *(int64*)mxGetData(prhs[0]);
  myAssert(sizeof(size_t)==sizeof(int64),"This code only works on 64 bit machines\n");
  
  feature_node **featureMatrix = 
      (feature_node**)myMalloc(numFeatures*sizeof(feature_node*));
  for(int64 i=0;i<numFeatures;i++){
    featureMatrix[i]=NULL;
  }

  int numBytesPtr = sizeof(feature_node**);
  plhs[0] = mxCreateNumericMatrix(numBytesPtr,1,mxINT8_CLASS,mxREAL);
  memcpy(mxGetData(plhs[0]),&featureMatrix,numBytesPtr);

}
