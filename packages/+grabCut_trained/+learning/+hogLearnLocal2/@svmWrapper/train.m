function info = train(obj)

opts = obj.svmOpts;
liblinearOptions = sprintf('-q -s %d -c %f -B %d',opts.svmType,opts.svmC,...
                           opts.svmBias);
spMatrix.featureMatrix = obj.featureMatrixPtr;
spMatrix.featureDim = obj.featureDim;
spMatrix.numFeatures = obj.numFeatures;
model = liblinear.trainPreAllocated(obj.labels,spMatrix,liblinearOptions,'col');
info.model = model;
