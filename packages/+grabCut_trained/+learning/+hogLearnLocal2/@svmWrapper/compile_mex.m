function compile_mex()

cwd=miscFns.extractDirPath(mfilename('fullpath'));

mexCmds=cell(0,1);
mexCmds{end+1} = sprintf('mex -O %s/mexInitSparseMatrix.cpp -I%s -outdir %s/',cwd,cwd,cwd);
mexCmds{end+1} = sprintf('mex -O %s/mexAddFeatures.cpp -I%s -outdir %s/',cwd,cwd,cwd);
mexCmds{end+1} = sprintf('mex -O %s/mexCloseMatrix.cpp -I%s -outdir %s/',cwd,cwd,cwd);
mexCmds{end+1} = sprintf('mex -O %s/mexFreeSparseMatrix.cpp -I%s -outdir %s/',cwd,cwd,cwd);
mexCmds{end+1} = sprintf('mex -O %s/mexPrintMatrix.cpp -I%s -outdir %s/',cwd,cwd,cwd);

for i=1:length(mexCmds)
  fprintf('Executing %s\n',mexCmds{i});
  eval(mexCmds{i});
end

