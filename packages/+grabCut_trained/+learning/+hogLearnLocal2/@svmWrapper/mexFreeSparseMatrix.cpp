#include "mex.h"
#include <iostream>
#include <linear.h>

using namespace std;

typedef unsigned int uint;
typedef long long int int64;

inline void myAssert(bool condition,char *msg){
  if(!condition){
    mexErrMsgTxt(msg);
  }
}

inline void *myMalloc(size_t numBytes){
  void* ans = mxMalloc(numBytes);
  mexMakeMemoryPersistent(ans);
  return ans;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters
      matrixPtr (int8 class, 8 x 1 , 8 = sizeof(void*))
      numFeatures (int64)
     Output: 
      No output, modifications are made to matrixPtr
  */

  myAssert(nrhs == 2, "2 input arguments expected.");
  if (nlhs != 0)
    mexErrMsgTxt("0 output arguments expected.");

  myAssert(mxGetClassID(prhs[0])==mxINT8_CLASS,
      "Invalid class for matrixPtr\n");
  myAssert(mxGetClassID(prhs[1])==mxINT64_CLASS,
      "Invalid class for numFeatures\n");
  myAssert(sizeof(size_t)==sizeof(int64),"This code only works on 64 bit machines\n");

  // Load the variables
  int numBytesPtr = sizeof(feature_node**);
  feature_node **matrix;
  memcpy(&matrix,mxGetData(prhs[0]),numBytesPtr);

  myAssert(mxGetNumberOfElements(prhs[1])==1,"Invalid number of elements in numFeatures\n");
  int64 numFeatures = *(int64*)mxGetData(prhs[1]);

  for(int64 i=0;i<numFeatures;i++){
    if(matrix[i]!=NULL){
      mxFree(matrix[i]);
    }
  }

  mxFree(matrix);
  matrix = NULL;
}
