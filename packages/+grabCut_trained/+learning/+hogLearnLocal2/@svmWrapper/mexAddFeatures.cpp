#include "mex.h"
#include <iostream>
#include <linear.h>
#include <vector>
#include <algorithm>

using namespace std;

typedef unsigned int uint;
typedef long long int int64;

inline void myAssert(bool condition,char *msg){
  if(!condition){
    mexErrMsgTxt(msg);
  }
}

inline void *myMalloc(size_t numBytes){
  void* ans = mxMalloc(numBytes);
  mexMakeMemoryPersistent(ans);
  return ans;
}

class IdxSparse{
  public:
  int64 xIdx;
  int64 yIdx;
  int64 origIdx;
};

bool compareIdxSparse(const IdxSparse& idx1,const IdxSparse& idx2){
  if(idx1.xIdx < idx2.xIdx) return true;
  if(idx1.xIdx > idx2.xIdx) return false;
  return (idx1.yIdx < idx2.yIdx);
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters
      matrixPtr (int8 class, 8 x 1 , 8 = sizeof(void*))
      numFeatures (int64)
      xIdxs [N x 1] (int64) , 1 based idx
      yIdxs [N x 1] (int64) , 1 based idx
      ftrValues [N x 1] (double)
      svmBias (scalar double)
      featureDim (scalar int64)

      xIdxs cannot overwrite previously allocated columns
     Output: 
      No output, modifications are made to matrixPtr
  */

  myAssert(nrhs == 7, "7 input arguments expected.");
  if (nlhs != 0)
    mexErrMsgTxt("0 output arguments expected.");

  myAssert(mxGetClassID(prhs[0])==mxINT8_CLASS,
      "Invalud class for matrixPtr\n");
  myAssert(mxGetClassID(prhs[1])==mxINT64_CLASS,
      "Invalid class for numFeatures\n");
  myAssert(mxGetClassID(prhs[2])==mxINT64_CLASS,
      "Invalid class for xIdxs\n");
  myAssert(mxGetClassID(prhs[3])==mxINT64_CLASS,
      "Invalid class for yIdxs\n");
  myAssert(mxGetClassID(prhs[4])==mxDOUBLE_CLASS,
      "Invalid class for ftrValues\n");
  myAssert(mxGetClassID(prhs[5])==mxDOUBLE_CLASS,
      "Invalid class for bias\n");
  myAssert(mxGetClassID(prhs[6])==mxINT64_CLASS,
      "Invalid class for featureDim\n");
  myAssert(sizeof(size_t)==sizeof(int64),"This code only works on 64 bit machines\n");

  // Load the variables
  int numBytesPtr = sizeof(feature_node**);
  feature_node **matrix;
  memcpy(&matrix,mxGetData(prhs[0]),numBytesPtr);

  int64 numFeatures = *(int64*)mxGetData(prhs[1]);
  myAssert(mxGetNumberOfElements(prhs[1])==1,"Invalid number of elements in numFeatures\n");

  int64 numIdxs = mxGetNumberOfElements(prhs[2]); 
  myAssert(numIdxs==mxGetNumberOfElements(prhs[3]),"Inconsistent number of idxs\n");
  myAssert(numIdxs==mxGetNumberOfElements(prhs[4]),"Inconsistent number of idxs\n");
  myAssert(mxGetNumberOfElements(prhs[5])==1,"Invalid number of elements in bias\n");
  myAssert(mxGetNumberOfElements(prhs[6])==1,"Invalid number of elements in featureDim\n");

  int64 *xIdxs = (int64*)mxGetData(prhs[2]);
  int64 *yIdxs = (int64*)mxGetData(prhs[3]);
  double *ftrValues = mxGetPr(prhs[4]);
  double bias = *mxGetPr(prhs[5]);
  int64 featureDim = *(int64*)mxGetData(prhs[6]);
  
  vector<IdxSparse> allIdxs(numIdxs);
  for(int64 i=0;i<numIdxs;i++){
    allIdxs[i].xIdx = xIdxs[i]-1;
    allIdxs[i].yIdx = yIdxs[i]-1;
    allIdxs[i].origIdx = i;
  }

  sort(allIdxs.begin(),allIdxs.end(),compareIdxSparse);

  int64 idxNext = 0;
  bool addBias = (bias>=0);
  while(idxNext < numIdxs){
    int64 xIdxCurrent = allIdxs[idxNext].xIdx;
    myAssert(xIdxCurrent < numFeatures,"xIdx exceeds matrix dimension\n");
    myAssert(matrix[xIdxCurrent]==NULL,"Cannot overwrite columns\n");

    int64 idxStart = idxNext;
    int64 numY = 1;
    int64 prevY = allIdxs[idxNext].yIdx;
    idxNext++;
    // Count the number of unique yIdxs and allocate memory
    while(idxNext < numIdxs && allIdxs[idxNext].xIdx == xIdxCurrent){
      if(allIdxs[idxNext].yIdx != prevY){
        numY++;prevY = allIdxs[idxNext].yIdx;
      }
      idxNext++;
    }
    int64 idxEnd = idxNext;
    if(addBias){numY++;}
    matrix[xIdxCurrent] = (feature_node*)myMalloc(
        sizeof(feature_node)*(numY+1)); // +1 is for the last empty entry
    feature_node *currentCol = matrix[xIdxCurrent];
    for(int64 i=0;i<numY;i++){
      currentCol[i].index = 0; 
      currentCol[i].value = 0; 
    }

    feature_node *nxtEntry = currentCol;
    prevY = allIdxs[idxStart].yIdx;
    for(int64 i=idxStart;i<idxEnd;i++){
      int64 yIdxCurrent = allIdxs[i].yIdx;
      if(prevY != yIdxCurrent){
        prevY=yIdxCurrent;
        nxtEntry++;
      }
      nxtEntry->value += ftrValues[allIdxs[i].origIdx];
      nxtEntry->index = (int)(yIdxCurrent+1); // liblinear uses 1 based idx
    }
    // assert: nxtEntry points to the last filled entry in col
    nxtEntry++;
    if(addBias){
      nxtEntry->value = bias;nxtEntry->index = (int)(featureDim+1);
      nxtEntry++;
    }
    nxtEntry->index = -1; // End of list entry
  }
  
}
