function closeMatrix(obj)

obj.mexCloseMatrix(obj.featureMatrixPtr,obj.numFeatures,...
    obj.svmOpts.svmBias,obj.featureDim);
