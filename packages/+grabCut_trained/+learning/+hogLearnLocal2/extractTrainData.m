function [ppInfo,hogByImg,hogBBoxesByImg,segMaskByImg,segBBoxesByImg] =...
    extractTrainData(imgDir,labelDir,gtDir,images,hogOpts,trainOpts,debugOpts)

import grabCut_trained.learning.hogLearnLocal2.*;

N = length(images);
hogByImg = cell(1,N);
segMaskByImg = cell(1,N);
hogBBoxesByImg = cell(1,N);
segBBoxesByImg = cell(1,N);

localSegMaskSize = getLocalSegMaskSize(trainOpts);

fprintf('Extracting hogs and segmasks ...\n');
stTime = clock;
for i=1:length(images)
  objectId = grabCut_trained.dataset.extractObjectIdentifier(images{i});

  tmpDebugOpts = debugOpts;
  tmpDebugOpts.debugPrefix = [objectId '_'];

  labelFile = [labelDir objectId '.png'];
  labelImg = imread(labelFile);
  [bbox,bboxAspect] = getBoxAspectRatio(labelImg);
  img = imread([imgDir grabCut_trained.dataset.extractImgName(images{i})]);
  img = im2double(img);
  gt = imread([gtDir objectId '.png']);
  [hogByImg{i},segMaskByImg{i},hogBBoxesByImg{i},segBBoxesByImg{i}] ...
      = getLocalHogAndSeg(img,gt,bbox,trainOpts,hogOpts,tmpDebugOpts,localSegMaskSize,...
        trainOpts.useSegForHogDictionary);
end
fprintf('Time taken to extract hogs for %d images = %.2f minutes\n',...
    length(images),etime(clock,stTime)/60);

fprintf('Preprocessing hogs ...\n');
stTime = clock;
ppInfo = preProcessHogs(hogByImg,hogBBoxesByImg,trainOpts,hogOpts);
fprintf('Time taken to pre process hogs (dictionary/kmeans) = %.2f minutes\n',...
    etime(clock,stTime)/60);

if(trainOpts.useSegForHogDictionary)
  % Get the hogs again without the gt mask for training
  hogByImg = cell(1,N);
  segMaskByImg = cell(1,N);
  hogBBoxesByImg = cell(1,N);
  segBBoxesByImg = cell(1,N);
  for i=1:numel(images)
    objectId = grabCut_trained.dataset.extractObjectIdentifier(images{i});
  
    tmpDebugOpts = debugOpts;
    tmpDebugOpts.debugLevel = 0; % switch off debugging for this one
    tmpDebugOpts.debugPrefix = [objectId '_'];
  
    labelFile = [labelDir objectId '.png'];
    labelImg = imread(labelFile);
    [bbox,bboxAspect] = getBoxAspectRatio(labelImg);
    img = imread([imgDir grabCut_trained.dataset.extractImgName(images{i})]);
    img = im2double(img);
    gt = imread([gtDir objectId '.png']);
    [hogByImg{i},segMaskByImg{i},hogBBoxesByImg{i},segBBoxesByImg{i}] ...
        = getLocalHogAndSeg(img,gt,bbox,trainOpts,hogOpts,tmpDebugOpts,localSegMaskSize,...
          false);
  end
end

