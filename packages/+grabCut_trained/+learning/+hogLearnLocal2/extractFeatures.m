function svmH = extractFeatures(ppInfo,hogByImg,segMaskByImg,hogBBoxesByImg,...
                                    segBBoxesByImg,trainOpts,hogOpts)

import grabCut_trained.learning.hogLearnLocal2.*;

localSegMaskSize = getLocalSegMaskSize(trainOpts);
nImages = numel(hogByImg);

numPixelsToPredict = 0;
for i=1:nImages
  %numPixelsToPredict = numPixelsToPredict + prod(size(segMaskByImg{i}));
  numPixelsToPredict = numPixelsToPredict + nnz(segMaskByImg{i}~=0.5);
end
 
featureDim = int64(ppInfo.featureSize*prod(localSegMaskSize));
svmH = svmWrapper(int64(numPixelsToPredict),featureDim,...
    trainOpts.predictorOpts);
  
labels = zeros(numPixelsToPredict,1);
idxNextFeature = 1;

for i=1:nImages
  iHog = hogByImg{i};
  iSegMask = segMaskByImg{i};
  hogBoxes = hogBBoxesByImg{i};
  segBoxes = segBBoxesByImg{i};
  featuresByImg =  ...
      getPixelFeatures(iHog,hogBoxes,iSegMask,localSegMaskSize,segBoxes,...
      trainOpts,hogOpts,ppInfo);
  featuresByImg.xIdxs = (idxNextFeature-1)+featuresByImg.xIdxs;
  iSegMask = iSegMask(iSegMask~=0.5); % Gives a column major ordering
  numFeatures = numel(iSegMask);
  labels(idxNextFeature:(idxNextFeature+numFeatures-1)) = 2*iSegMask-1;
  idxNextFeature = idxNextFeature + numFeatures;
  svmH.addFeatures(int64(featuresByImg.xIdxs),int64(featuresByImg.yIdxs),...
      featuresByImg.ftrValues);
  fprintf('Extracted features for image: %04d\r',i);
end

svmH.closeMatrix();
svmH.labels = labels;
% closeMatrix means all features have been added

%fprintf('\nTotal memory used to store features = %.2fG\n',...
    %svmWrapper.getTotalMemory()/1e9);
