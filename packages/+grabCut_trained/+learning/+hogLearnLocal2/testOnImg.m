function [seg,svmOut] = testOnImg(img,labelImg,trainData,debugOpts)

import grabCut_trained.learning.hogLearnLocal2.*;

img = im2double(img);
ppInfo = trainData.ppInfo;
trainOpts = trainData.opts.trainOpts;
hogOpts = trainData.opts.hogOpts;
predictorInfo = trainData.predictorInfo;

localSegMaskSize = getLocalSegMaskSize(trainOpts);
[bbox,bboxAspect] = getBoxAspectRatio(labelImg);

[hogFull,segMask,hogBoxes,segBoxes] = getLocalHogAndSeg(img,...
    zeros([size(img,1) size(img,2)],'uint8'),bbox,trainOpts,hogOpts,debugOpts,...
    localSegMaskSize);

% ------- Extract features -----------------
featureIdxs = getPixelFeatures(hogFull,hogBoxes,zeros(size(segMask)),... 
    localSegMaskSize,segBoxes,trainOpts,hogOpts,ppInfo);
features = sparse(featureIdxs.yIdxs,featureIdxs.xIdxs,featureIdxs.ftrValues,...
                  ppInfo.featureSize*prod(localSegMaskSize),prod(size(segMask)));
% ------ Apply the predictors -------------
svmOut = applyPredictor(features,trainOpts,predictorInfo,size(segMask));
svmOut = snapPredictionToBBox(svmOut,bbox,[size(img,1) size(img,2)]);
seg = zeros(size(svmOut),'uint8');
seg(svmOut>0) = 255;

if(debugOpts.debugLevel > 0)
  overlay = grabCut_trained.helpers.overlaySeg(img,seg);
  overlay = grabCut_trained.helpers.overlayBox(overlay,labelImg);
  miscFns.saveDebug(debugOpts,overlay,'seg.jpg'); 
end

function svmOut = snapPredictionToBBox(svmOut_box,bbox,imgSize)

bboxH = bbox(4)-bbox(2)+1;
bboxW = bbox(3)-bbox(1)+1;
svmOut_box = imresize(svmOut_box,[bboxH bboxW],'bilinear');
svmOut = -1*ones(imgSize);

deltaLeft = max(0,1-bbox(1));
deltaRight = max(0,bbox(3)-imgSize(2));
deltaTop = max(0,1-bbox(2));
deltaBottom = max(0,bbox(4)-imgSize(1));

svmOut( (bbox(2)+deltaTop):(bbox(4)-deltaBottom), ...
     (bbox(1)+deltaLeft):(bbox(3)-deltaRight)) = svmOut_box(...
     (1+deltaTop):(end-deltaBottom),(1+deltaLeft):(end-deltaRight)); 
