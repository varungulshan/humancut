function opts = makeGrabCutOpts(optsString)

switch(optsString)
  case 'try2'
    % No training done, just run it on the test with a valu set by hand
    gcOpts = grabCut.helpers.makeOpts('iter10_2');
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([25]);
    scoreString='accuracyOverlap';
    maxTrain = 1;

  case 'try1_max800'
    gcOpts = grabCut.helpers.makeOpts('iter10_2');
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([10 25 50 100]);
    scoreString='accuracyOverlap';
    maxTrain = 800;
 
  case 'try1_max500'
    gcOpts = grabCut.helpers.makeOpts('iter10_2');
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([10 25 50 100 150]);
    scoreString='accuracyOverlap';
    maxTrain = 500;
  case 'try1'
    gcOpts = grabCut.helpers.makeOpts('iter10_2');
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([10 25 50 100 150 200]);
    scoreString='accuracyOverlap';
    maxTrain = -1;
  case 'smallTest2'
    gcOpts = grabCut.helpers.makeOpts('iterQuick');
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([5 10 25]);
    vary(2).fieldName='gcGamma_i';
    vary(2).fieldValues=num2cell([5 10 25]);
    scoreString='accuracyOverlap';
    maxTrain = 2;
   case 'smallTest'
    gcOpts = grabCut.helpers.makeOpts('iterQuick');
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([5 10 25]);
    vary(2).fieldName='gcGamma_i';
    vary(2).fieldValues=num2cell([5 10 25]);
    scoreString='accuracyOverlap';
    maxTrain = -1;
  otherwise
    error('Invalid options string: %s\n',optsString);
end

opts.maxTrain = maxTrain;
opts.grabCutOpts = gcOpts;
opts.vary = vary;
opts.scoreString = scoreString;
