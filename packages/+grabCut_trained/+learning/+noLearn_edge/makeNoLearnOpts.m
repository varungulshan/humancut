function gcOpts = makeGrabCutOpts(optsString)

switch(optsString)
  case 'try1'
    gcOpts = grabCut_edgeLearn.helpers.makeOpts('iter10_2');
  case 'try2'
    gcOpts = grabCut_edgeLearn.helpers.makeOpts('iter10_2_edgeLearn');
  case 'try3'
    gcOpts = grabCut_edgeLearn.helpers.makeOpts('iter10_3_edgeLearn');
  case 'try1_kovesi'
    gcOpts = grabCut_edgeLearn.helpers.makeOpts('iter10_2_kovesi');
  case 'try2_kovesi'
    gcOpts = grabCut_edgeLearn.helpers.makeOpts('iter10_2_edgeLearn_kovesi');
  case 'try3_kovesi'
    gcOpts = grabCut_edgeLearn.helpers.makeOpts('iter10_3_edgeLearn_kovesi');
  otherwise
    error('Invalid options string: %s\n',optsString);
end
