function trainNoLearn(opts,trainOutDir,testBenchOpts)

import grabCut_trained.learning.noLearn_edge.*;

assert(strcmp(opts.trainMethod,'noLearn_edge'));

grabCutOpts = makeNoLearnOpts(opts.trainMethod_optsString);
save([trainOutDir 'grabCutOpts.mat'],'grabCutOpts');
fH=fopen([trainOutDir 'grabCutOpts.txt'],'w');
miscFns.printClass(grabCutOpts,fH);
fclose(fH);
