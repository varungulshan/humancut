function opts = makeGrabCutOpts(optsString)

maxTrain = -1;
switch(optsString)

  case 'hogLearnLocal_bestOptions_1'
    gcOpts = localGrabCut2_hogLearn.helpers.makeOpts('iter10_rad80');
    gcOpts.hogLearnUnary_rescale = 0;
    gcOpts.gcGamma_e = 25; % learnt from run059
    gcOpts.localWin_rad = 40; % learnt from run059
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([25]);
    [hogLearnData,gcOpts.hogLearnType] =...
        localGrabCut2_hogLearn.loadHogLearnData('hogLearnLocal_bestOptions_1');
    % learnt from run072, dictionary 2500, with C=1
    scoreString='accuracyOverlap';
    maxTrain = 1; % no validation is needed, so just do 1

  case 'hogLearnLocal_bestOptions_2'
    gcOpts = localGrabCut2_hogLearn.helpers.makeOpts('iter10_rad80');
    gcOpts.hogLearnUnary_rescale = 1;
    gcOpts.gcGamma_e = 25; % learnt from run059
    gcOpts.localWin_rad = 40; % learnt from run059
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([25]);
    [hogLearnData,gcOpts.hogLearnType] =...
        localGrabCut2_hogLearn.loadHogLearnData('hogLearnLocal_bestOptions_1');
    % learnt from run072, dictionary 2500, with C=1
    scoreString='accuracyOverlap';
    maxTrain = 1; % no validation is needed, so just do 1

  case 'hogLearnLocal_bestOptions2_2'
    gcOpts = localGrabCut2_hogLearn.helpers.makeOpts('iter10_rad80');
    gcOpts.hogLearnUnary_rescale = 1;
    gcOpts.gcGamma_e = 25; % learnt from run059
    gcOpts.localWin_rad = 40; % learnt from run059
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([25]);
    [hogLearnData,gcOpts.hogLearnType] =...
        localGrabCut2_hogLearn.loadHogLearnData('hogLearnLocal_bestOptions_2');
    % learnt from run072, dictionary 2500, with C=1
    scoreString='accuracyOverlap';
    maxTrain = 1; % no validation is needed, so just do 1

  case 'hogLearnLocal_bestOptions2_2_5iters'
    gcOpts = localGrabCut2_hogLearn.helpers.makeOpts('iter10_rad80');
    gcOpts.hogLearnUnary_rescale = 1;
    gcOpts.gcGamma_e = 25; % learnt from run059
    gcOpts.localWin_rad = 40; % learnt from run059
    gcOpts.numIters=5;
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([25]);
    [hogLearnData,gcOpts.hogLearnType] =...
        localGrabCut2_hogLearn.loadHogLearnData('hogLearnLocal_bestOptions_2');
    % learnt from run072, dictionary 2500, with C=1
    scoreString='accuracyOverlap';
    maxTrain = 1; % no validation is needed, so just do 1

  case 'hogLearnLocal_bestOptions2_2_3iters'
    gcOpts = localGrabCut2_hogLearn.helpers.makeOpts('iter10_rad80');
    gcOpts.hogLearnUnary_rescale = 1;
    gcOpts.gcGamma_e = 25; % learnt from run059
    gcOpts.localWin_rad = 40; % learnt from run059
    gcOpts.numIters=3;
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([25]);
    [hogLearnData,gcOpts.hogLearnType] =...
        localGrabCut2_hogLearn.loadHogLearnData('hogLearnLocal_bestOptions_2');
    % learnt from run072, dictionary 2500, with C=1
    scoreString='accuracyOverlap';
    maxTrain = 1; % no validation is needed, so just do 1

  case 'hogLearnLocal_bestOptions_3_4iters'
    gcOpts = localGrabCut2_hogLearn.helpers.makeOpts('iter10_rad80');
    gcOpts.hogLearnUnary_rescale = 1;
    gcOpts.gcGamma_e = 25; % learnt from run059
    gcOpts.localWin_rad = 40; % learnt from run059
    gcOpts.numIters=4;
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([25]);
    [hogLearnData,gcOpts.hogLearnType] =...
        localGrabCut2_hogLearn.loadHogLearnData('hogLearnLocal_bestOptions_3');
    % learnt from run072, dictionary 2500, with C=1
    scoreString='accuracyOverlap';
    maxTrain = 1; % no validation is needed, so just do 1

  case 'hogLearnLocal_bestOptions_4_4iters'
    gcOpts = localGrabCut2_hogLearn.helpers.makeOpts('iter10_rad80');
    gcOpts.hogLearnUnary_rescale = 1;
    gcOpts.gcGamma_e = 25; % learnt from run059
    gcOpts.localWin_rad = 40; % learnt from run059
    gcOpts.numIters=4;
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([25]);
    [hogLearnData,gcOpts.hogLearnType] =...
        localGrabCut2_hogLearn.loadHogLearnData('hogLearnLocal_bestOptions_4');
    % learnt from run072, dictionary 2500, with C=1
    scoreString='accuracyOverlap';
    maxTrain = 1; % no validation is needed, so just do 1

  case 'hogLearnLocal_position_run103_4iters_2'
    gcOpts = localGrabCut2_hogLearn.helpers.makeOpts('iter10_rad80');
    gcOpts.hogLearnUnary_rescale = 1;
    gcOpts.gcGamma_e = 25; % learnt from run059
    gcOpts.localWin_rad = 40; % learnt from run059
    gcOpts.numIters=4;
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([20 25 30 35]);
    vary(2).fieldName='localWin_rad';
    vary(2).fieldValues=num2cell([30 40 50 60]);
    [hogLearnData,gcOpts.hogLearnType] =...
        localGrabCut2_hogLearn.loadHogLearnData('hogLearnLocal_position_run103');
    % learnt from run103, single dictionary 2500, with C=0.1
    scoreString='accuracyOverlap';
    maxTrain = 800;

  case 'hogLearnLocal_position_run103_4iters_3'
    gcOpts = localGrabCut2_hogLearn.helpers.makeOpts('iter10_rad80');
    gcOpts.hogLearnUnary_rescale = 1;
    gcOpts.gcGamma_e = 20; % learnt from run115
    gcOpts.localWin_rad = 30; % learnt from run115
    gcOpts.numIters=4;
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([20]);
    [hogLearnData,gcOpts.hogLearnType] =...
        localGrabCut2_hogLearn.loadHogLearnData('hogLearnLocal_position_run103');
    % learnt from run103, single dictionary 2500, with C=0.1
    scoreString='accuracyOverlap';
    maxTrain = 1;

  case 'hogLearnLocal_position_run103_4iters_4'
    gcOpts = localGrabCut2_hogLearn.helpers.makeOpts('iter10_rad80');
    gcOpts.hogLearnUnary_rescale = 1;
    gcOpts.gcGamma_e = 20; % learnt from run115
    gcOpts.localWin_rad = 30; % learnt from run115
    gcOpts.numIters=4;
    gcOpts.radRatioForOverlaying=0.5;
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([20]);
    [hogLearnData,gcOpts.hogLearnType] =...
        localGrabCut2_hogLearn.loadHogLearnData('hogLearnLocal_position_run103');
    % learnt from run103, single dictionary 2500, with C=0.1
    scoreString='accuracyOverlap';
    maxTrain = 1;


  case 'hogLearnLocal_position_run103_4iters_pp'
    gcOpts = localGrabCut2_hogLearn.helpers.makeOpts('iter10_rad80');
    gcOpts.hogLearnUnary_rescale = 1;
    gcOpts.gcGamma_e = 20; % learnt from run115
    gcOpts.localWin_rad = 30; % learnt from run115
    gcOpts.numIters=4;
    gcOpts.postProcessHogOutput = true;
    gcOpts.postProcessConnectivity = 4;
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([20]);
    [hogLearnData,gcOpts.hogLearnType] =...
        localGrabCut2_hogLearn.loadHogLearnData('hogLearnLocal_position_run103');
    % learnt from run103, single dictionary 2500, with C=0.1
    scoreString='accuracyOverlap';
    maxTrain = 1;

  case 'hogLearnLocal_position_run103_4iters_confidence1'
    gcOpts = localGrabCut2_hogLearn.helpers.makeOpts('iter10_rad80');
    gcOpts.hogLearnUnary_rescale = 1;
    gcOpts.gcGamma_e = 25; % learnt from run059
    gcOpts.localWin_rad = 40; % learnt from run059
    gcOpts.numIters=4;
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([25]);
    [hogLearnData,gcOpts.hogLearnType] =...
        localGrabCut2_hogLearn.loadHogLearnData('hogLearnLocal_position_run103_confidence');
    % learnt from run103, single dictionary 2500, with C=0.1
    scoreString='accuracyOverlap';
    maxTrain = 1; % no validation is needed, so just do 1

  case 'hogLearnLocal_position_run103_4iters_confidence2'
    gcOpts = localGrabCut2_hogLearn.helpers.makeOpts('iter10_rad80');
    gcOpts.hogLearnUnary_rescale = 0;
    gcOpts.gcGamma_e = 25; % learnt from run059
    gcOpts.localWin_rad = 40; % learnt from run059
    gcOpts.numIters=4;
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([25]);
    [hogLearnData,gcOpts.hogLearnType] =...
        localGrabCut2_hogLearn.loadHogLearnData('hogLearnLocal_position_run103_confidence');
    % learnt from run103, single dictionary 2500, with C=0.1
    scoreString='accuracyOverlap';
    maxTrain = 1; % no validation is needed, so just do 1

  case 'hogLearnLocal_position_run103_4iters'
    gcOpts = localGrabCut2_hogLearn.helpers.makeOpts('iter10_rad80');
    gcOpts.hogLearnUnary_rescale = 1;
    gcOpts.gcGamma_e = 25; % learnt from run059
    gcOpts.localWin_rad = 40; % learnt from run059
    gcOpts.numIters=4;
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([25]);
    [hogLearnData,gcOpts.hogLearnType] =...
        localGrabCut2_hogLearn.loadHogLearnData('hogLearnLocal_position_run103');
    % learnt from run103, single dictionary 2500, with C=0.1
    scoreString='accuracyOverlap';
    maxTrain = 1; % no validation is needed, so just do 1

  case 'noHogLearn_largeSet800_1'
    gcOpts = localGrabCut2_hogLearn.helpers.makeOpts('iter10_rad80');
    gcOpts.hogLearnUnary_rescale = 0;
    gcOpts.hogLearnType = 'none';
    hogLearnData = [];
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([10 25 50 100]);
    vary(2).fieldName='localWin_rad';
    vary(2).fieldValues=num2cell([40 80 120 160]);
    scoreString='accuracyOverlap';
    maxTrain = 800;

  case 'hogLearnLocal_largeSet800_1'
    gcOpts = localGrabCut2_hogLearn.helpers.makeOpts('iter10_rad80');
    gcOpts.hogLearnUnary_rescale = 0;
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([10 25 50 100]);
    vary(2).fieldName='localWin_rad';
    vary(2).fieldValues=num2cell([40 80 120 160]);
    [hogLearnData,gcOpts.hogLearnType] =...
        localGrabCut2_hogLearn.loadHogLearnData('hogLearnLocal_2');
    scoreString='accuracyOverlap';
    maxTrain = 800;

  case 'hogLearnLocal_largeSet800_2'
    gcOpts = localGrabCut2_hogLearn.helpers.makeOpts('iter10_rad80');
    gcOpts.hogLearnUnary_rescale = 1;
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([10 25 50 100]);
    vary(2).fieldName='localWin_rad';
    vary(2).fieldValues=num2cell([40 80 120 160]);
    [hogLearnData,gcOpts.hogLearnType] =...
        localGrabCut2_hogLearn.loadHogLearnData('hogLearnLocal_2');
    scoreString='accuracyOverlap';
    maxTrain = 800;

 case 'smallTest_noHog'
    gcOpts = localGrabCut2_hogLearn.helpers.makeOpts('smallTest');
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([10 50]);
    hogLearnData = [];
    gcOpts.hogLearnType = 'none';
    scoreString='accuracyOverlap';
    maxTrain = 3;
 
 case 'smallTest'
    gcOpts = localGrabCut2_hogLearn.helpers.makeOpts('iter10_rad80');
    vary(1).fieldName='hogLearnUnary_rescale';
    vary(1).fieldValues=num2cell([0 0.1]);
    vary(2).fieldName='gcGamma_e';
    vary(2).fieldValues=num2cell([10 50]);
    [hogLearnData,gcOpts.hogLearnType] =...
        localGrabCut2_hogLearn.loadHogLearnData('try1');
    scoreString='accuracyOverlap';
    maxTrain = 2;
 
 otherwise
    error('Invalid options string: %s\n',optsString);
end

opts.grabCutOpts = gcOpts;
opts.vary = vary;
opts.scoreString = scoreString;
opts.hogLearnData = hogLearnData;
opts.maxTrain = maxTrain;
