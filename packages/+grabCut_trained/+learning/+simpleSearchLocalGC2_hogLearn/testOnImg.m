function seg = testOnImg(img,labels,trainData,debugOpts)

gcOpts = trainData.grabCutOpts;
hogLearnData = trainData.hogLearnData;

img = im2double(img);
segH = localGrabCut2_hogLearn.segEngine(debugOpts,gcOpts,hogLearnData);
segH.preProcess(img);
segH.start(labels);
seg = segH.seg;

if(debugOpts.debugLevel > 0)
  overlay = grabCut_trained.helpers.overlaySeg(img,seg);
  overlay = grabCut_trained.helpers.overlayBox(overlay,labels);
  miscFns.saveDebug(debugOpts,overlay,'seg.jpg'); 
end

delete(segH);
