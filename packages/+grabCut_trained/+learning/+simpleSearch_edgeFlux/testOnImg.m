function seg = testOnImg(img,labels,trainData,debugOpts)

gcOpts = trainData.grabCutOpts;

img = im2double(img);
segH = grabCut_edgeLearnFlux.segEngine(debugOpts,gcOpts);
segH.preProcess(img);
segH.start(labels);
seg = segH.seg;

if(debugOpts.debugLevel > 0)
  overlay = grabCut_trained.helpers.overlaySeg(img,seg);
  overlay = grabCut_trained.helpers.overlayBox(overlay,labels);
  miscFns.saveDebug(debugOpts,overlay,'seg.jpg'); 
end

delete(segH);
