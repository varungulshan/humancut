function opts = makeGrabCutOpts(optsString)

switch(optsString)
  case 'edgeLearn_1'
    gcOpts = grabCut_edgeLearnFlux.helpers.makeOpts('iter10_3_edgeLearn');
    vary(1).fieldName='gcGamma';
    vary(1).fieldValues=num2cell([10 25 50 100]);
    vary(2).fieldName='fluxStrength';
    vary(2).fieldValues=num2cell([0 10 50 100]);
    scoreString='accuracyOverlap';
  case 'edgeLearn_2'
    gcOpts = grabCut_edgeLearnFlux.helpers.makeOpts('iter10_3_edgeLearn');
    vary(1).fieldName='gcGamma';
    vary(1).fieldValues=num2cell([10 25 50 100]);
    vary(2).fieldName='fluxStrength';
    vary(2).fieldValues=num2cell([0 5 10 15 20]);
    vary(3).fieldName='fluxBlurSigma';
    vary(3).fieldValues=num2cell([1 2 3 4]);
    scoreString='accuracyOverlap';
  case 'debug_edgeLearn_1'
    gcOpts = grabCut_edgeLearnFlux.helpers.makeOpts('iter10_2_edgeLearn');
    gcOpts.fluxStrength = 10;
    vary(1).fieldName='gcGamma';
    vary(1).fieldValues=num2cell([10]);
    scoreString='accuracyOverlap';
 otherwise
    error('Invalid options string: %s\n',optsString);
end

opts.grabCutOpts = gcOpts;
opts.vary = vary;
opts.scoreString = scoreString;
