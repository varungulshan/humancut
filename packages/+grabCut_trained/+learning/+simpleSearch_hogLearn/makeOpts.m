function opts = makeGrabCutOpts(optsString)

maxTrain = -1;

switch(optsString)
  case 'hogLearnLocal_1'
    gcOpts = grabCut_hogLearn.helpers.makeOpts('iter10_2');
    vary(1).fieldName='hogLearnUnary_rescale';
    vary(1).fieldValues=num2cell([0 0.5 1 10]);
    vary(2).fieldName='gcGamma_e';
    vary(2).fieldValues=num2cell([10 25 50 100]);
    [hogLearnData,gcOpts.hogLearnType] =...
        grabCut_hogLearn.loadHogLearnData('hogLearnLocal_1');
    scoreString='accuracyOverlap';

  case 'hogLearnLocal_2'
    gcOpts = grabCut_hogLearn.helpers.makeOpts('iter10_2');
    gcOpts.hogLearnUnary_rescale = 0.5;
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([10 25 50 100]);
    [hogLearnData,gcOpts.hogLearnType] =...
        grabCut_hogLearn.loadHogLearnData('hogLearnLocal_1');
    scoreString='accuracyOverlap';

  case 'hogLearnLocal_3'
    gcOpts = grabCut_hogLearn.helpers.makeOpts('iter10_2');
    gcOpts.hogLearnUnary_rescale = 0;
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([10 25 50 100]);
    [hogLearnData,gcOpts.hogLearnType] =...
        grabCut_hogLearn.loadHogLearnData('hogLearnLocal_1');
    scoreString='accuracyOverlap';

  case 'hogLearnLocal_largeSet800_1'
    gcOpts = grabCut_hogLearn.helpers.makeOpts('iter10_2');
    gcOpts.hogLearnUnary_rescale = 0;
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([10 25 50 100]);
    [hogLearnData,gcOpts.hogLearnType] =...
        grabCut_hogLearn.loadHogLearnData('hogLearnLocal_2');
    scoreString='accuracyOverlap';
    maxTrain = 800;

  case 'hogLearnLocal_largeSet800_2'
    gcOpts = grabCut_hogLearn.helpers.makeOpts('iter10_2');
    gcOpts.hogLearnUnary_rescale = 1;
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([10 25 50 100]);
    [hogLearnData,gcOpts.hogLearnType] =...
        grabCut_hogLearn.loadHogLearnData('hogLearnLocal_2');
    scoreString='accuracyOverlap';
    maxTrain = 800;

  case 'hogLearnLocal_position_103'
    gcOpts = grabCut_hogLearn.helpers.makeOpts('iter10_2');
    gcOpts.hogLearnUnary_rescale = 1;
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([25]);
    [hogLearnData,gcOpts.hogLearnType] =...
        grabCut_hogLearn.loadHogLearnData('hogLearnLocal_position_run103');
    scoreString='accuracyOverlap';
    maxTrain = 1;

  case 'hogLearnLocal2_3'
    gcOpts = grabCut_hogLearn.helpers.makeOpts('iter10_2');
    gcOpts.hogLearnUnary_rescale = 0;
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([10 25 50 100]);
    [hogLearnData,gcOpts.hogLearnType] =...
        grabCut_hogLearn.loadHogLearnData('try1');
    scoreString='accuracyOverlap';
   
  case 'hogLearnLocal2_2'
    gcOpts = grabCut_hogLearn.helpers.makeOpts('iter10_2');
    gcOpts.hogLearnUnary_rescale = 0.5;
    vary(1).fieldName='gcGamma_e';
    vary(1).fieldValues=num2cell([10 25 50 100]);
    [hogLearnData,gcOpts.hogLearnType] =...
        grabCut_hogLearn.loadHogLearnData('try1');
    scoreString='accuracyOverlap';
 
  case 'hogLearnLocal2_1'
    gcOpts = grabCut_hogLearn.helpers.makeOpts('iter10_2');
    vary(1).fieldName='hogLearnUnary_rescale';
    vary(1).fieldValues=num2cell([0 0.1 0.5 1 10]);
    vary(2).fieldName='gcGamma_e';
    vary(2).fieldValues=num2cell([10 25 50 100]);
    [hogLearnData,gcOpts.hogLearnType] =...
        grabCut_hogLearn.loadHogLearnData('try1');
    scoreString='accuracyOverlap';

  case 'smallTest_2'
    gcOpts = grabCut_hogLearn.helpers.makeOpts('iter10_2');
    vary(1).fieldName='hogLearnUnary_rescale';
    vary(1).fieldValues=num2cell([0 0.1]);
    vary(2).fieldName='gcGamma_e';
    vary(2).fieldValues=num2cell([10 50]);
    [hogLearnData,gcOpts.hogLearnType] =...
        grabCut_hogLearn.loadHogLearnData('try1');
    scoreString='accuracyOverlap';
    maxTrain = 2;
 
  case 'smallTest'
    gcOpts = grabCut_hogLearn.helpers.makeOpts('iter10_2');
    vary(1).fieldName='hogLearnUnary_rescale';
    vary(1).fieldValues=num2cell([0 0.1]);
    vary(2).fieldName='gcGamma_e';
    vary(2).fieldValues=num2cell([10 50]);
    [hogLearnData,gcOpts.hogLearnType] =...
        grabCut_hogLearn.loadHogLearnData('try1');
    scoreString='accuracyOverlap';
  otherwise
    error('Invalid options string: %s\n',optsString);
end

opts.grabCutOpts = gcOpts;
opts.vary = vary;
opts.scoreString = scoreString;
opts.hogLearnData = hogLearnData;
opts.maxTrain = maxTrain;
