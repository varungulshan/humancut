function data = loadTrainingData(trainDir,testDir)
% data will include:

trainingDataDir = [trainDir 'trainingData/'];
tmpLoad = load([trainDir 'trainOpts.mat']);
opts = tmpLoad.opts;
tmpLoad = load([trainingDataDir 'ppInfo.mat']);
ppInfo = tmpLoad.ppInfo;
tmpLoad = load([trainingDataDir 'segPredictor.mat']);
predictorInfo = tmpLoad.predictorInfo;

data.opts = opts;
data.ppInfo = ppInfo;
data.predictorInfo = predictorInfo;
