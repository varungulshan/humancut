function compile_mex()

cwd=miscFns.extractDirPath(mfilename('fullpath'));

mexCmds=cell(0,1);
mexCmds{end+1} = sprintf('mex -O %s+cpp/mex_extractBBox.cpp -outdir %s+cpp/',cwd,cwd);
mexCmds{end+1} = sprintf('mex -O %s+cpp/MEXkmeans_faster2.cpp -outdir %s+cpp/',cwd,cwd);
mexCmds{end+1} = sprintf('mex -O %s+cpp/MEXfindnearestl2.cpp -outdir %s+cpp/',cwd,cwd);

for i=1:length(mexCmds)
  fprintf('Executing %s\n',mexCmds{i});
  eval(mexCmds{i});
end

