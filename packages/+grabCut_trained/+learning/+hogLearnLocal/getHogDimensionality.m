function dim = getHogDimensionality(hogOpts,trainOpts)

numCells = (trainOpts.localHogW*trainOpts.localHogH);

switch(hogOpts.hogType)
  case '180degree'
    singleHogDim = 9; % this can depend on hog type    
  case '180degreeAndNormalizers'
    singleHogDim = 13; % this can depend on hog type    
  case '360degree'
    singleHogDim = 18; % this can depend on hog type    
  case 'pedro'
    singleHogDim = 27+4; % this can depend on hog type    
  otherwise
    error('Invalid hog type:%s\n',hogOpts.hogType);
end

dim = singleHogDim * numCells;
