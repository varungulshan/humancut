#include "mex.h"
#include <float.h>
#include <memory.h>
#include <math.h>
#include "matrix.h"
#include <iostream>
#include <vector>

typedef unsigned int uint;

inline void myAssert(bool condition,char *msg){
  if(!condition){
    mexErrMsgTxt(msg);
  }
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters
     img       [ h x w x D] (double or uint8)
     bbox      [4 x 1] (int32)
     Output: cropped image to bbox (repeating stuff outside it)
     img bbox sizes image
  */

  myAssert(nrhs == 2, "2 input arguments expected.");
  if (nlhs != 1)
    mexErrMsgTxt("1 output arguments expected.");

  myAssert(mxGetClassID(prhs[0])==mxDOUBLE_CLASS ||
      mxGetClassID(prhs[0])==mxUINT8_CLASS,"Invalud class for img\n");
  myAssert(mxGetClassID(prhs[1])==mxINT32_CLASS,"Invalid class for bbox\n");

  int numDims=mxGetNumberOfDimensions(prhs[0]);
  
  int h=mxGetDimensions(prhs[0])[0];
  int w=mxGetDimensions(prhs[0])[1];
  int N=h*w;
  int D;
  if(numDims==3)
    D=mxGetDimensions(prhs[0])[2];
  else
    D=1;

  myAssert(mxGetNumberOfElements(prhs[1])==4,"bbox should be 4 elements\n");
  int *bbox = (int*)mxGetData(prhs[1]);
  int hOut = bbox[3]-bbox[1]+1;
  int wOut = bbox[2]-bbox[0]+1;

  if(mxGetClassID(prhs[0])==mxDOUBLE_CLASS){
    double *ptrImg = (double*)mxGetData(prhs[0]);
    int dims[3]={hOut,wOut,D};
    plhs[0] = mxCreateNumericArray(3,dims,mxDOUBLE_CLASS,mxREAL);
    double *outImg = (double*)mxGetData(plhs[0]);

    for(int d=0;d<D;d++){
      double *ptrImgD = ptrImg + N*d;
      for(int x=bbox[0];x<=bbox[2];x++){
        int xImg = std::max(x,0);
        xImg = std::min(xImg,w-1);
        for(int y=bbox[1];y<=bbox[3];y++){
          int yImg = std::max(y,0);
          yImg = std::min(yImg,h-1);
          *outImg = ptrImgD[yImg+xImg*h];
          outImg++;
        }
      }
    }
   }
   else if(mxGetClassID(prhs[0])==mxUINT8_CLASS){
    typedef unsigned char uchar;
    uchar *ptrImg = (uchar*)mxGetData(prhs[0]);
    int dims[3]={hOut,wOut,D};
    plhs[0] = mxCreateNumericArray(3,dims,mxUINT8_CLASS,mxREAL);
    uchar *outImg = (uchar*)mxGetData(plhs[0]);

    for(int d=0;d<D;d++){
      uchar *ptrImgD = ptrImg + N*d;
      for(int x=bbox[0];x<=bbox[2];x++){
        int xImg = std::max(x,0);
        xImg = std::min(xImg,w-1);
        for(int y=bbox[1];y<=bbox[3];y++){
          int yImg = std::max(y,0);
          yImg = std::min(yImg,h-1);
          *outImg = ptrImgD[yImg+xImg*h];
          outImg++;
        }
      }
    }
   }
}
