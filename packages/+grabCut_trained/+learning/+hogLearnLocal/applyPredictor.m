function segPredictions = applyPredictor(hogs,trainOpts,predictorInfo)

switch(trainOpts.predictorType)
  case {'liblinear','liblinear_balanced'}
    segPredictions = applyLibLinear(hogs,trainOpts,predictorInfo);
  case 'kmeans'
    segPredictions = applyKmeans(hogs,trainOpts,predictorInfo);
  otherwise
    error('Predictor type: %s not yet supported\n',trainOpts.predictorType);
end

function segPredictions = applyLibLinear(hogs,trainOpts,info)

wT = info.wT;
predictorOpts = trainOpts.predictorOpts;

if(predictorOpts.svmBias < 0)
  svmOuts = wT*hogs;
else
  svmOuts = wT(:,1:(end-1))*hogs+wT(:,end)*repmat(predictorOpts.svmBias,[1 size(hogs,2)]);
end

segPredictions = ones(size(svmOuts));
segPredictions(svmOuts<0) = -1;


function segPredictions = applyKmeans(hogs,trainOpts,predictorInfo)

import grabCut_trained.learning.hogLearnLocal.*;

labels = cpp.MEXfindnearestl2(hogs,predictorInfo.meanFeatures);
segPredictions = predictorInfo.meanSegMasks(:,labels);
