function ppInfo = preProcessHogs(hogs,trainOpts)
% TO apply pre-processing such as dimensionality reduction

switch(trainOpts.hogPreProcess)
  case 'pca',
    ppInfo = pcaPreprocess(hogs,trainOpts.hogPreProcessOpts);
  case 'sparseDictionary'
    ppInfo = sparseDictionaryProcess(hogs,trainOpts.hogPreProcessOpts);
  case 'sparseDictionary_saved'
    ppInfo = sparseDictionaryProcess_saved(hogs,trainOpts.hogPreProcessOpts);
  case 'none',
    ppInfo = [];
  otherwise
    error('Invalid pre process method: %s\n',trainOpts.hogPreProcess);
end

function ppInfo = sparseDictionaryProcess_saved(hogs,opts)

tmpLoad = load(opts.ppFile);
ppInfo = tmpLoad.ppInfo;

function ppInfo = sparseDictionaryProcess(hogs,opts)

dictionaryParams = opts.dictionaryParams;
reset(RandStream.getDefaultStream); % To reproduce randomness
randomHogs = hogs(:,grabCut_trained.dataset.COMuniquerand(dictionaryParams.K,[1 size(hogs,2)]));
dictionaryParams.D = randomHogs;
if(opts.useFasterOne)
  ppInfo.dictionary = mexTrainDL_Memory(hogs,dictionaryParams);
else
  ppInfo.dictionary = mexTrainDL(hogs,dictionaryParams);
end

function ppInfo = pcaPreprocess(hogs,opts)

newDim = opts.dimensionality;
assert(newDim <= size(hogs,1));

hogs = hogs';
meanHog = mean(hogs,1)';
[basis,newHogs] = princomp(hogs);

basis = basis(:,1:newDim);

ppInfo.meanHog = meanHog;
ppInfo.basis = basis;
