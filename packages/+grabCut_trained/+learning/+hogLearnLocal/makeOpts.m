function opts = makeOpts(optsString)

import grabCut_trained.learning.hogLearnLocal.*;

switch(optsString)

  case 'try1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'none';
    trainOpts.predictorType = 'kmeans';
    trainOpts.predictorOpts.numClusters = 100;
    trainOpts.predictorOpts.numIters = 100;

  case 'svm1_validateC_5'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_500_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 100;
    trainOpts.predictorOpts.svmBias = -1;


  case 'svm1_dict2_validateC_1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_1500_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_dict2_validateC_2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_1500_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;


  case 'svm1_dict2_validateC_3'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_1500_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;


  case 'svm1_dict2_validateC_4'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_1500_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 0.01;
    trainOpts.predictorOpts.svmBias = -1;


  case 'svm1_validateC_4'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_500_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 0.01;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateC_3'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_500_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateC_2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_500_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateC_1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_500_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateDict_1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_100_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateDict_2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_500_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateDict_3'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_1000_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateDict_4'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_1500_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateDict_5'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2000_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;


  case 'svm1_validateDict_6'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateCellSize_1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateCellSize_2_bugFix'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 16;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_cellSize16_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_cellSize8_localSize5'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;


  case 'svm1_cellSize16_localSize5'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 16;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_cellSize16_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateCellSize_2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 16;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateSparsity_1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_sparse_0.15_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateSparsity_2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_sparse_0.5_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .5;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateSparsity_3'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_sparse_1_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 1;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateSparsity_4'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_sparse_10_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 10;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateLocalSize_1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_4_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateLocalSize_2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateC3_flipEnable_l2loss_2_fullDict'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 5;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateC2_flipEnable_l2loss_2_fullDict'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateC1_flipEnable_l2loss_2_fullDict'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;


  case 'svm1_validateLocalSize_flipEnable_l2loss_2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateLocalSize_flipEnable_2_fullDict'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_flipData_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;


  case 'svm1_validateLocalSize_flipEnable_2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'dictionaryOnly_localSize5_flipEnable_8threads_iterFix'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.enableLearner = false;
    trainOpts.flipEnable = true;
    trainOpts.hogPreProcess = 'sparseDictionary';
    
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 800; 
    % 800 iters = approx 4 passes over data, assuming you are learning
    % dictionary over both train and test
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 8;
    ppOpts.dictionaryParams.batchSize = 2048;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'dictionaryOnly_nonNegative_8threads'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.enableLearner = false;
    trainOpts.flipEnable = true;
    trainOpts.useSegForHog= true;
    trainOpts.erodeSegWidth = 2;
    trainOpts.hogPreProcess = 'sparseDictionary';
    
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.posD = 1;
    ppOpts.dictionaryParams.posAlpha = 1;
    ppOpts.dictionaryParams.iter = 300;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 8;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;
    ppOpts.lassoParams.pos = ppOpts.dictionaryParams.posAlpha;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'dictionaryOnly_localSize5_flipEnable_8threads'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.enableLearner = false;
    trainOpts.flipEnable = true;
    trainOpts.hogPreProcess = 'sparseDictionary';
    
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 2000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 8;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateLocalSize_2_balanced'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_5_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear_balanced';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_validateLocalSize_3'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 6;
    trainOpts.localHogW = 6;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/data2/varun/iccv2011/results/savedDictionaries/dictionary_2500_size_6_kinect_3.mat';

    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 0.15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 2500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;


  case 'svm1_savedDictionary'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/home/varun/windows/rugbyShared/work/videoCut2/git/results/grabCut_trained/train/hogLearning/trainingData/ppInfo.mat';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;


  case 'svm_smallTest'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 4;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 100;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm3_bias'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 2;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = 1;


  case 'svm2_bias'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = 1;

  case 'svm1_noDictionary_l2loss_validateC1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.hogPreProcess = 'none';
    ppOpts = [];

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_noDictionary_l2loss_validateC2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.hogPreProcess = 'none';
    ppOpts = [];

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_noDictionary_l2loss_validateC3'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.flipEnable = true;
    trainOpts.hogPreProcess = 'none';
    ppOpts = [];

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 2;
    trainOpts.predictorOpts.svmC = 0.01;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_noDictionary'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'none';
    ppOpts = [];

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1_noDictionary_bias'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'none';
    ppOpts = [];

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 1;
    trainOpts.predictorOpts.svmBias = 1;

  case 'svm1_bias_moreIter'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = 1;

  case 'svm1_moreIter'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

   case 'kmeans1_small'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'none';
    trainOpts.predictorType = 'kmeans';
    trainOpts.predictorOpts.numClusters = 100;
    trainOpts.predictorOpts.numIters = 10;


   case 'kmeans1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'none';
    trainOpts.predictorType = 'kmeans';
    trainOpts.predictorOpts.numClusters = 500;
    trainOpts.predictorOpts.numIters = 200;

  case 'tryDictionary2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;
    trainOpts.predictorType = 'kmeans';
    trainOpts.predictorOpts.numClusters = 100;
    trainOpts.predictorOpts.numIters = 10;
 
  case 'tryDictionary1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary';
    ppOpts.useFasterOne = false;
    ppOpts.dictionaryParams.mode = 0;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = 10;
    ppOpts.dictionaryParams.iter = 1;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 100;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;
    trainOpts.predictorType = 'kmeans';
    trainOpts.predictorOpts.numClusters = 100;
    trainOpts.predictorOpts.numIters = 10;
 
  otherwise
    error('Invalid options string: %s\n',optsString);
end

opts.hogOpts = hogOpts;
opts.trainOpts = trainOpts;
