function trainHogPredictor(params,trainOutDir,testBenchOpts)

import grabCut_trained.learning.dictionaryLearn.*;

assert(strcmp(params.trainMethod,'dictionaryLearn'));

fTime = fopen([trainOutDir 'timing.txt'],'w');
stTimeGlobal = clock;

opts = makeOpts(params.trainMethod_optsString);
evalOpts = testBench.evalOpts_grabCut(params.evalOpts_string);
gtDir = evalOpts.gtDir;
save([trainOutDir 'trainOpts.mat'],'opts');
trainingDataDir = [trainOutDir 'trainingData/'];
mkdir(trainingDataDir);

images = textread(testBenchOpts.imgList,'%s');
imgDir = testBenchOpts.imgDir;
labelDir = testBenchOpts.labelDir;

trainOpts = opts.trainOpts;
hogOpts = opts.hogOpts;

debugOpts = struct();
debugOpts.debugLevel = params.debugLevel;
debugOpts.debugDir = [trainOutDir 'debug/'];
debugOpts.debugPrefix = '';
mkdir(debugOpts.debugDir);
stTime = clock;
fprintf('Extracting training data hogs\n');
[hogsFG,hogsBG]=extractTrainData(imgDir,labelDir,gtDir, ...
                images,hogOpts,trainOpts,debugOpts);
fprintf(fTime,'Time taken to extract features from data  = %.2f minutes (%.1fhrs)\n',etime(clock,stTime)/60,etime(clock,stTime)/3600);
% hogs = D1 x N vector, segMasks = D2 x N vector, N = number of images
fprintf('Preprocessing hogs\n');
stTime = clock;
ppInfo = preProcessHogs(hogsFG,hogsBG,trainOpts);
fprintf(fTime,'Time taken to pre-process hogs = %.2f minutes (%.1fhrs)\n',etime(clock,stTime)/60,etime(clock,stTime)/3600);
save([trainingDataDir 'ppInfo.mat'],'ppInfo');

fprintf(fTime,'Total time taken for training: %.1f hrs\n',etime(clock,stTimeGlobal)/3600);
fclose(fTime);
