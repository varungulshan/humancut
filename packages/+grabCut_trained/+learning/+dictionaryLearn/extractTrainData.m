function [hogsFG,hogsBG] = extractTrainData(imgDir,labelDir,gtDir,...
    images,hogOpts,trainOpts,debugOpts)

import grabCut_trained.learning.dictionaryLearn.*;

nImages = length(images);
flipEnable = trainOpts.flipEnable;

N = nImages;
if(flipEnable), N=2*N; end;

hogByImgFG = cell(1,N);
hogByImgBG = cell(1,N);
hogBBoxesByImg = cell(1,N);

for i=1:length(images)
  objectId = grabCut_trained.dataset.extractObjectIdentifier(images{i});

  tmpDebugOpts = debugOpts;
  tmpDebugOpts.debugPrefix = [objectId '_'];

  labelFile = [labelDir objectId '.png'];
  labelImg = imread(labelFile);
  [bbox,bboxAspect] = getBoxAspectRatio(labelImg);
  img = imread([imgDir grabCut_trained.dataset.extractImgName(images{i})]);
  img = im2double(img);
  gt = imread([gtDir objectId '.png']);
  [hogByImgFG{i},hogByImgBG{i},hogBBoxesByImg{i}] ...
      = getLocalHogAndSeg(img,gt,bbox,trainOpts,hogOpts,tmpDebugOpts);

  if(flipEnable)
    tmpDebugOpts = debugOpts;
    tmpDebugOpts.debugPrefix = [objectId '_flip_'];
    labelImg = flipdim(labelImg,2);
    [bbox,bboxAspect] = getBoxAspectRatio(labelImg);
    img = flipdim(img,2);
    gt = flipdim(gt,2);
    [hogByImgFG{nImages+i},hogByImgBG{nImages+i},...
     hogBBoxesByImg{nImages+i}] ...
       = getLocalHogAndSeg(img,gt,bbox,trainOpts,hogOpts,tmpDebugOpts);
  end

end

numHogs = 0;
for i=1:numel(hogByImgFG)
  numHogs = numHogs + size(hogBBoxesByImg{i},2);
end

hogsFG = zeros(getHogDimensionality(hogOpts,trainOpts),numHogs);
hogsBG = zeros(getHogDimensionality(hogOpts,trainOpts),numHogs);

idxNext = 1;
for i=1:numel(hogByImgFG)
  hogBoxes = hogBBoxesByImg{i};
  currentHogFG = hogByImgFG{i};
  currentHogBG = hogByImgBG{i};
  for j=1:size(hogBoxes,2)
    tmpHog = currentHogFG(hogBoxes(1,j):hogBoxes(2,j),hogBoxes(3,j):hogBoxes(4,j),:);
    hogsFG(:,idxNext) = tmpHog(:);
    tmpHog = currentHogBG(hogBoxes(1,j):hogBoxes(2,j),hogBoxes(3,j):hogBoxes(4,j),:);
    hogsBG(:,idxNext) = tmpHog(:);
    idxNext = idxNext+1;
  end
end
assert(size(hogsFG,1)==getHogDimensionality(hogOpts,trainOpts), ...
    'Hog size incorrect\n');

function [hogFullFG,hogFullBG,hogBBoxes] = ...
    getLocalHogAndSeg(img,gt,bbox,trainOpts,hogOpts,...
    debugOpts)
% hogFull is the hog for the image window, and same for segMaskFull
% hogFull is D x H x W array (D=dimensionality,H,W are number of hog cells)
% hogBBoxes is [4 x N] array, of type [y1 y2 x1 x2] which indicates indices
% to extract from hogFull to get a localHog (ie, hogBBoxes(:,y1:y2,x1:x2)
% same for segMaskBBoxes, segBBoxes(y1:y2,x1:x2) gives a seg

import grabCut_trained.learning.dictionaryLearn.*;

[bbox,hogBoxSize] = snapBBoxToSize(bbox,trainOpts,hogOpts,...
    [size(img,1) size(img,2)]);
numCellsH = hogBoxSize(1)/hogOpts.cellSize;
numCellsW = hogBoxSize(2)/hogOpts.cellSize;

tmpSegMask = cpp.mex_extractBBox(gt,int32(bbox-1));
tmpSegMask = imresize(tmpSegMask,[hogBoxSize(1) hogBoxSize(2)],'nearest');
stEl = strel('disk',trainOpts.erodeSegWidth);
tmpSegMask = imdilate(tmpSegMask==255,stEl);

localImg = cpp.mex_extractBBox(img,int32(bbox-1));
localImg = imresize(localImg,[hogBoxSize(1) hogBoxSize(2)],'bilinear');

hog = hogCode.mex_getHogWithMask(localImg,hogOpts.cellSize,tmpSegMask);
if(debugOpts.debugLevel>=10)
  visHog = drawHog(hog,hogOpts);
  miscFns.saveDebug(debugOpts,visHog,'hogFG.png');
  miscFns.saveDebug(debugOpts,imresize(localImg,size(visHog)),'img.jpg');
  miscFns.saveDebug(debugOpts,imresize(tmpSegMask,size(visHog),'nearest'),'seg.png');
end

hogFullFG = selectHogType(hog,hogOpts);

hog = hogCode.mex_getHogWithMask(localImg,hogOpts.cellSize,~tmpSegMask);
if(debugOpts.debugLevel>=10)
  visHog = drawHog(hog,hogOpts);
  miscFns.saveDebug(debugOpts,visHog,'hogBG.png');
end
hogFullBG = selectHogType(hog,hogOpts);

yStart = [1:(numCellsH-trainOpts.localHogH+1)];
xStart = [1:(numCellsW-trainOpts.localHogW+1)];

[x1,y1] = meshgrid(xStart,yStart);
x1=x1(:)';
y1=y1(:)';
hogBBoxes = zeros(4,numel(x1));
hogBBoxes(1,:) = y1;
hogBBoxes(2,:) = y1+trainOpts.localHogH-1;
hogBBoxes(3,:) = x1;
hogBBoxes(4,:) = x1+trainOpts.localHogW-1;
