function opts = makeOpts(optsString)

import grabCut_trained.learning.dictionaryLearn.*;

switch(optsString)

  case 'dictionaryOnly_localSize5_flipEnable_8threads'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.enableLearner = false;
    trainOpts.flipEnable = true;
    trainOpts.erodeSegWidth = 2;
    trainOpts.extraCellsH = 0;
    trainOpts.extraCellsW = 0;
    trainOpts.hogPreProcess = 'sparseDictionary';
    
    ppOpts.useFasterOne = true;
    ppOpts.fgDictionaryParams.mode = 2;
    ppOpts.fgDictionaryParams.modeD = 0;
    ppOpts.fgDictionaryParams.lambda = .15;
    ppOpts.fgDictionaryParams.iter = 600; 
    ppOpts.fgDictionaryParams.modeParam = 0;
    ppOpts.fgDictionaryParams.K = 1500;
    ppOpts.fgDictionaryParams.numThreads = 8;
    ppOpts.fgDictionaryParams.batchSize = 2048;

    ppOpts.bgDictionaryParams.mode = 2;
    ppOpts.bgDictionaryParams.modeD = 0;
    ppOpts.bgDictionaryParams.lambda = .15;
    ppOpts.bgDictionaryParams.iter = 600; 
    ppOpts.bgDictionaryParams.modeParam = 0;
    ppOpts.bgDictionaryParams.K = 1000;
    ppOpts.bgDictionaryParams.numThreads = 8;
    ppOpts.bgDictionaryParams.batchSize = 2048;

    ppOpts.lassoParams.mode = ppOpts.fgDictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.fgDictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.fgDictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;


  case 'dictionaryOnly_localSize5_flipEnable_8threads_padding'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInHeight = 16;
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 5;
    trainOpts.localHogW = 5;
    trainOpts.enableLearner = false;
    trainOpts.flipEnable = true;
    trainOpts.erodeSegWidth = 2;
    trainOpts.extraCellsH = 2;
    trainOpts.extraCellsW = 2;
    trainOpts.hogPreProcess = 'sparseDictionary';
    
    ppOpts.useFasterOne = true;
    ppOpts.fgDictionaryParams.mode = 2;
    ppOpts.fgDictionaryParams.modeD = 0;
    ppOpts.fgDictionaryParams.lambda = .15;
    ppOpts.fgDictionaryParams.iter = 600; 
    ppOpts.fgDictionaryParams.modeParam = 0;
    ppOpts.fgDictionaryParams.K = 1500;
    ppOpts.fgDictionaryParams.numThreads = 8;
    ppOpts.fgDictionaryParams.batchSize = 2048;

    ppOpts.bgDictionaryParams.mode = 2;
    ppOpts.bgDictionaryParams.modeD = 0;
    ppOpts.bgDictionaryParams.lambda = .15;
    ppOpts.bgDictionaryParams.iter = 600; 
    ppOpts.bgDictionaryParams.modeParam = 0;
    ppOpts.bgDictionaryParams.K = 1000;
    ppOpts.bgDictionaryParams.numThreads = 8;
    ppOpts.bgDictionaryParams.batchSize = 2048;

    ppOpts.lassoParams.mode = ppOpts.fgDictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.fgDictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.fgDictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

  otherwise
    error('Invalid options string: %s\n',optsString);
end

opts.hogOpts = hogOpts;
opts.trainOpts = trainOpts;
