function hogImg = drawModifiedHog(hog,hogOpts)
% Function to visualize hog, after it subset has been chosen
% depending on the hogType

switch(hogOpts.hogType)
  case '180degree'
    % Nothing to do
  case '180degreeAndNormalizers'
    % Also contains the 4 normalizers used to normalize the block
    hog = hog(:,:,1:9);
  case '360degree'
    hog = hog(:,:,1:9)+hog(:,:,10:18);
  case 'pedro'
    hog = hog(:,:,1:9)+hog(:,:,(1:9)+9)+hog(:,:,(1:9)+18);
  otherwise
    error('Invalid hog type: %s\n',hogOpts.hogType);
end

hogScale = max(hog(:));
hogImg = hogCode.HOGpicture(hog,hogOpts.visHogBlockSize)/hogScale;
