function saveLearningDebug(features,ppInfo,predictorInfo,hogOpts,trainOpts,debugOpts)
% Function to save debugging info about learning

switch(trainOpts.predictorType)
  case 'kmeans'
    saveKmeansInfo(features,ppInfo,predictorInfo,hogOpts,trainOpts,debugOpts);
end

function saveKmeansInfo(features,ppInfo,predictorInfo,hogOpts,trainOpts,debugOpts)

import grabCut_trained.learning.dictionaryLearn.*;

debugOpts.debugDir = [debugOpts.debugDir 'clusters/'];
if(~exist(debugOpts.debugDir,'dir')), mkdir(debugOpts.debugDir); end;

if(debugOpts.debugLevel>=10)
  %assert(strcmp(trainOpts.hogPreProcess,'none'),'Preprocessing not yet supported\n');
  means = predictorInfo.meanFeatures;
  localHogH = trainOpts.localHogH;
  localHogW = trainOpts.localHogW;
  
  for i=1:size(means,2)
    segMask=predictorInfo.meanSegMasks(:,i);
    segMaskSize = getLocalSegMaskSize(trainOpts);
    assert(prod(segMaskSize)==numel(segMask),'Incorrect segmasks\n');
    segMask = reshape(segMask,segMaskSize);
    visSize = hogOpts.visHogBlockSize*[localHogH localHogW];
    segMask = imresize(segMask,visSize);
    miscFns.saveDebug(debugOpts,segMask,sprintf('cluster_%03d_meanSeg.png',i));
  end
  
  if(strcmp(trainOpts.hogPreProcess,'none'))
    for i=1:size(means,2)
      hog=means(:,i);
      assert(length(hog)==getHogDimensionality(hogOpts,trainOpts),'Incorrect hog size\n');
      assert(mod(length(hog),localHogH*localHogW)==0,'Incorrect hog size\n');
      hog=reshape(hog,[localHogH localHogW length(hog)/(localHogH*localHogW)]);
      visHog = drawModifiedHog(hog,hogOpts);
      miscFns.saveDebug(debugOpts,visHog,sprintf('cluster_%03d_hog.png',i));
    end
  end
    
end
