% Class defines options for training on hogLearnGlobal
classdef trainingOptsClass
  properties (SetAccess=public, GetAccess=public)
    hogCellsInHeight % number of hog cells along the height dimension
    % this implicitly decides the scale of the hogcell
    localSegMaskLongerDimension % number of pixels in longer dimension of segMask
    % decides dimensionality of the output space

    localHogH % number of vertical cells for local hog 
    localHogW % number of horizontal cells for local hog 

    hogPreProcess % set to pca to reduce dimensionality
    hogPreProcessOpts % options for preprocessing (structure)

    predictorType % 'kmeans', 'svmVedaldi'
    predictorOpts % depends on classifierType

    flipEnable
    enableLearner
    erodeSegWidth

    extraCellsH
    extraCellsW
  end

  methods
    function obj=trainingOptsClass()
      obj.hogCellsInHeight = 16;
      obj.localSegMaskLongerDimension = 40;
      obj.hogPreProcess = 'none';
      obj.hogPreProcessOpts = struct();
      obj.localHogH = 4;
      obj.localHogW = 4;
      obj.predictorType = 'kmeans';
      obj.predictorOpts.numClusters = 100;
      obj.predictorOpts.numIters = 200;
      obj.flipEnable = false;
      obj.enableLearner = true;
      obj.erodeSegWidth = 2;
      obj.extraCellsH = 0;
      obj.extraCellsW = 0;
    end
  end

end % of classdef
