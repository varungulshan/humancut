function hog = selectHogType(hog,hogOpts)

switch(hogOpts.hogType)
  case '180degree'
    hog = hog(:,:,18+(1:9));
  case '180degreeAndNormalizers'
    % Also contains the 4 normalizers used to normalize the block
    hog = hog(:,:,18+(1:13));
  case '360degree'
    hog = hog(:,:,1:18);
  case 'pedro'
    % nothing to do
  otherwise
    error('Invalid hog type: %s\n',hogOpts.hogType);
end

