function [bbox,hogBoxSize] = snapBBoxToSize(bbox,trainOpts,hogOpts,imgSize)
% Snaps bbox to fit integral number of hog cells

imgH = imgSize(1);imgW=imgSize(2);

hBoxOrig = bbox(4)-bbox(2)+1;
wBoxOrig = bbox(3)-bbox(1)+1;

hogBoxSize(1) = hogOpts.cellSize * trainOpts.hogCellsInHeight;
rescaleHeight = hogBoxSize(1)/hBoxOrig;
newWidth = rescaleHeight*wBoxOrig;
numCellsHoriz = ceil(newWidth/hogOpts.cellSize);
numCellsHoriz = max(numCellsHoriz,trainOpts.localHogW);
hogBoxSize(2) = hogOpts.cellSize * numCellsHoriz;

hBox = hBoxOrig;
wBox = wBoxOrig;

aspectHog = hogBoxSize(2)/hogBoxSize(1);

if(wBox/hBox < aspectHog)
  wBox = hBox * aspectHog;
else
  hBox = wBox/aspectHog;
end

wBoxDelta = (wBox - wBoxOrig)/2;
hBoxDelta = (hBox - hBoxOrig)/2;
assert(wBoxDelta>=0);
assert(hBoxDelta>=0);

bbox(1) = (bbox(1)-wBoxDelta);
bbox(2) = (bbox(2)-hBoxDelta);
bbox(3) = (bbox(3)+wBoxDelta);
bbox(4) = (bbox(4)+hBoxDelta);

% Add the padding cells if there is space
cellSize_origResolution = hBox/trainOpts.hogCellsInHeight;
padCellsLeft = min(max(0,floor( (bbox(1)-1)/cellSize_origResolution)), ...
                    trainOpts.extraCellsW);
padCellsRight = min(max(0,floor( (imgW-bbox(2))/cellSize_origResolution)), ...
                    trainOpts.extraCellsW);
padCellsTop = min(max(0,floor( (bbox(3)-1)/cellSize_origResolution)), ...
                    trainOpts.extraCellsH);
padCellsBottom = min(max(0,floor((imgH-bbox(4))/cellSize_origResolution)),...
                    trainOpts.extraCellsH);

hogBoxSize(1) = hogBoxSize(1)+hogOpts.cellSize*(padCellsTop+padCellsBottom);
hogBoxSize(2) = hogBoxSize(2)+hogOpts.cellSize*(padCellsLeft+padCellsRight);

bbox(1) = bbox(1)-padCellsLeft*cellSize_origResolution;
bbox(2) = bbox(2)-padCellsTop*cellSize_origResolution;
bbox(3) = bbox(3)+padCellsRight*cellSize_origResolution;
bbox(4) = bbox(4)+padCellsBottom*cellSize_origResolution;

bbox = round(bbox);
