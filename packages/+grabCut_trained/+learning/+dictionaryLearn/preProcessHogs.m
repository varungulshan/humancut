function ppInfo = preProcessHogs(hogsFG,hogsBG,trainOpts)
% TO apply pre-processing such as dimensionality reduction

switch(trainOpts.hogPreProcess)
  case 'sparseDictionary'
    ppInfo = sparseDictionaryProcess(hogsFG,hogsBG,trainOpts.hogPreProcessOpts);
  otherwise
    error('Invalid pre process method: %s\n',trainOpts.hogPreProcess);
end

function ppInfo = sparseDictionaryProcess(hogsFG,hogsBG,opts)

dictionaryParams = opts.fgDictionaryParams;
hogs = hogsFG;
reset(RandStream.getDefaultStream); % To reproduce randomness
randomHogs = hogs(:,grabCut_trained.dataset.COMuniquerand(dictionaryParams.K,[1 size(hogs,2)]));
dictionaryParams.D = randomHogs;
if(opts.useFasterOne)
  dictionaryFG = mexTrainDL_Memory(hogs,dictionaryParams);
else
  dictionaryFG = mexTrainDL(hogs,dictionaryParams);
end

dictionaryParams = opts.bgDictionaryParams;
hogs = hogsBG;
reset(RandStream.getDefaultStream); % To reproduce randomness
randomHogs = hogs(:,grabCut_trained.dataset.COMuniquerand(dictionaryParams.K,[1 size(hogs,2)]));
dictionaryParams.D = randomHogs;
if(opts.useFasterOne)
  dictionaryBG = mexTrainDL_Memory(hogs,dictionaryParams);
else
  dictionaryBG = mexTrainDL(hogs,dictionaryParams);
end

ppInfo.dictionary = [dictionaryFG dictionaryBG];
