function info = learnPredictor(features,segMasks,trainOpts,debugOpts)

info = struct();

switch(trainOpts.predictorType)
  case 'kmeans',
    info = learnKmeans(features,segMasks,trainOpts.predictorOpts,debugOpts);
  case 'liblinear'
    info = learnLibLinear(features,segMasks,trainOpts.predictorOpts,debugOpts);
  otherwise
    error('Invalid predictorType: %s\n',trainOpts.predictorType);
end

function info = learnLibLinear(features,segMasks,opts,debugOpts)

liblinearOptions = sprintf('-q -s %d -c %f -B %d',opts.svmType,opts.svmC,...
                           opts.svmBias);
dimensionality = size(features,1);
if(opts.svmBias>=0)
  dimensionality = dimensionality+1;
end
wT = zeros(size(segMasks,1),dimensionality);

for i=1:size(segMasks,1)
  stTime = clock;
  fprintf('Training %d/%d pixel of segmask:',i,size(segMasks,1));
  labelVector = segMasks(i,:)';
  valid = (labelVector~=0.5);
  tmpFeatures = features(:,valid);
  labelVector = 2*labelVector(valid)-1;
  model = liblinear.train(labelVector,tmpFeatures,liblinearOptions,'col');
  wT(i,:) = model.Label(1)*model.w; % the multiplication by label takes
  % care of flip
  assert(abs(model.Label(1))==1,'Unexpected model label output\n');
  fprintf(' %d seconds\n',etime(clock,stTime));
end
info.wT = wT;

function info = learnKmeans(features,segMasks,opts,debugOpts)

features = full(features); % In case features are sparse
[xx,meanFeatures,xx,labels,xx] = grabCut_trained.learning.hogLearnLocalScaled.COMKMeans(...
    features,[],opts.numClusters,opts.numIters,1);

info.meanFeatures = meanFeatures;
meanSegMasks = zeros(size(segMasks,1),opts.numClusters);
for i=1:opts.numClusters
  meanSegMasks(:,i) = mean(segMasks(:,labels==i),2);
end
info.meanSegMasks = meanSegMasks;
