function trainHogPredictor(params,trainOutDir,testBenchOpts)

import grabCut_trained.learning.hogLearnLocalScaled.*;

assert(strcmp(params.trainMethod,'hogLearnLocalScaled'));

opts = makeOpts(params.trainMethod_optsString);
evalOpts = testBench.evalOpts_grabCut(params.evalOpts_string);
gtDir = evalOpts.gtDir;
save([trainOutDir 'trainOpts.mat'],'opts');
trainingDataDir = [trainOutDir 'trainingData/'];
mkdir(trainingDataDir);

images = textread(testBenchOpts.imgList,'%s');
imgDir = testBenchOpts.imgDir;
labelDir = testBenchOpts.labelDir;

trainOpts = opts.trainOpts;
hogOpts = opts.hogOpts;

debugOpts = struct();
debugOpts.debugLevel = params.debugLevel;
debugOpts.debugDir = [trainOutDir 'debug/'];
debugOpts.debugPrefix = '';
mkdir(debugOpts.debugDir);
fprintf('Extracting training data (hogs and segmasks)\n');
[hogs,segMasks]=extractTrainData(imgDir,labelDir,gtDir, ...
                images,hogOpts,trainOpts,debugOpts);
% hogs = D1 x N vector, segMasks = D2 x N vector, N = number of images
fprintf('Preprocessing hogs\n');
[hogs,ppInfo] = preProcessHogs(hogs,trainOpts);
save([trainingDataDir 'ppInfo.mat'],'ppInfo');

fprintf('Learning predictor\n');
predictorInfo = learnPredictor(hogs,segMasks,trainOpts,debugOpts);
saveLearningDebug(hogs,ppInfo,predictorInfo,hogOpts,trainOpts,debugOpts);
save([trainingDataDir 'segPredictor.mat'],'predictorInfo');
