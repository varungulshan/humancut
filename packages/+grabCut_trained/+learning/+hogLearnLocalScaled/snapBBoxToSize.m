function [bbox,hogBoxSize] = snapBBoxToSize(bbox,trainOpts,hogOpts,...
    upperBodyFile)

tmpLoad = load(upperBodyFile);
detections = tmpLoad.bbox;

if(size(detections,1)==0)
  upperBodyHeight = trainOpts.defaultUpperBodyHeight;
else
  [xx,bestId] = max(detections(:,5));
  bestDetection = detections(bestId,:);
  upperBodyHeight = bestDetection(4)-bestDetection(2)+1;
end

hBoxOrig = bbox(4)-bbox(2)+1;
wBoxOrig = bbox(3)-bbox(1)+1;

hogCellSizeInImg = upperBodyHeight / trainOpts.hogCellsInUpperBody;

numCellsInHeight = ceil(hBoxOrig/hogCellSizeInImg);
numCellsInWidth = ceil(wBoxOrig/hogCellSizeInImg);

numCellsInHeight = max(trainOpts.localHogH,numCellsInHeight);
numCellsInWidth = max(trainOpts.localHogW,numCellsInWidth);

hogBoxSize(1) = hogOpts.cellSize * numCellsInHeight;
hogBoxSize(2) = hogOpts.cellSize * numCellsInWidth;

hBox = hogCellSizeInImg * numCellsInHeight;
wBox = hogCellSizeInImg * numCellsInWidth;

wBoxDelta = (wBox - wBoxOrig)/2;
hBoxDelta = (hBox - hBoxOrig)/2;
assert(wBoxDelta>=0);
assert(hBoxDelta>=0);

bbox(1) = round(bbox(1)-wBoxDelta);
bbox(2) = round(bbox(2)-hBoxDelta);
bbox(3) = round(bbox(3)+wBoxDelta);
bbox(4) = round(bbox(4)+hBoxDelta);
