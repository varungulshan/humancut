function [hogs,ppInfo]=preProcessHogs(hogs,trainOpts)
% TO apply pre-processing such as dimensionality reduction

switch(trainOpts.hogPreProcess)
  case 'pca',
    [hogs,ppInfo] = pcaPreprocess(hogs,trainOpts.hogPreProcessOpts);
  case 'sparseDictionary'
    [hogs,ppInfo] = sparseDictionaryProcess(hogs,trainOpts.hogPreProcessOpts);
  case 'sparseDictionary_saved'
    [hogs,ppInfo] = sparseDictionaryProcess_saved(hogs,trainOpts.hogPreProcessOpts);
  case 'none',
    ppInfo = [];
  otherwise
    error('Invalid pre process method: %s\n',trainOpts.hogPreProcess);
end

function [hogs,ppInfo] = sparseDictionaryProcess_saved(hogs,opts)

tmpLoad = load(opts.ppFile);
ppInfo = tmpLoad.ppInfo;
hogs = mexLasso(hogs,ppInfo.sparseDictionary,opts.lassoParams);

function [hogs,ppInfo] = sparseDictionaryProcess(hogs,opts)

dictionaryParams = opts.dictionaryParams;
reset(RandStream.getDefaultStream); % To reproduce randomness
randomHogs = hogs(:,grabCut_trained.dataset.COMuniquerand(dictionaryParams.K,[1 size(hogs,2)]));
dictionaryParams.D = randomHogs;
if(opts.useFasterOne)
  ppInfo.sparseDictionary = mexTrainDL_Memory(hogs,dictionaryParams);
else
  ppInfo.sparseDictionary = mexTrainDL(hogs,dictionaryParams);
end
hogs = mexLasso(hogs,ppInfo.sparseDictionary,opts.lassoParams);

function [hogs,ppInfo] = pcaPreprocess(hogs,opts)

newDim = opts.dimensionality;
assert(newDim <= size(hogs,1));

hogs = hogs';
meanHog = mean(hogs,1)';
[basis,newHogs] = princomp(hogs);

newHogs = newHogs(:,1:newDim);
basis = basis(:,1:newDim);

hogs = newHogs';
ppInfo.meanHog = meanHog;
ppInfo.basis = basis;
