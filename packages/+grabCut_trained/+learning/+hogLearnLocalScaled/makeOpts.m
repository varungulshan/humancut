function opts = makeOpts(optsString)

import grabCut_trained.learning.hogLearnLocalScaled.*;

switch(optsString)

  case 'svm1_savedDictionary'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInUpperBody = 4;
    trainOpts.upperBodyDir = '/data2/varun/iccv2011/pp/upperBodyDetections_manuel/';
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary_saved';
    ppOpts.ppFile = '/home/varun/windows/rugbyShared/work/videoCut2/git/results/grabCut_trained/train/hogLearning/trainingData/ppInfo.mat';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 100;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'svm3_bias'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInUpperBody = 4;
    trainOpts.upperBodyDir = '/data2/varun/iccv2011/pp/upperBodyDetections_manuel/';
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 2;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = 1;


  case 'svm2_bias'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInUpperBody = 4;
    trainOpts.upperBodyDir = '/data2/varun/iccv2011/pp/upperBodyDetections_manuel/';
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = 1;

  case 'svm1_bias_moreIter'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInUpperBody = 4;
    trainOpts.upperBodyDir = '/data2/varun/iccv2011/pp/upperBodyDetections_manuel/';
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = 1;

  case 'svm1_moreIter'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInUpperBody = 4;
    trainOpts.upperBodyDir = '/data2/varun/iccv2011/pp/upperBodyDetections_manuel/';
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 1000;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 10;
    trainOpts.predictorOpts.svmBias = -1;

  case 'smallTest'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.hogCellsInUpperBody = 4;
    trainOpts.upperBodyDir = '/data2/varun/iccv2011/pp/upperBodyDetections_manuel/';
    trainOpts.localSegMaskLongerDimension = 40;
    trainOpts.localHogH = 4;
    trainOpts.localHogW = 4;
    trainOpts.hogPreProcess = 'sparseDictionary';
    ppOpts.useFasterOne = true;
    ppOpts.dictionaryParams.mode = 2;
    ppOpts.dictionaryParams.modeD = 0;
    ppOpts.dictionaryParams.lambda = .15;
    ppOpts.dictionaryParams.iter = 10;
    ppOpts.dictionaryParams.modeParam = 0;
    ppOpts.dictionaryParams.K = 500;
    ppOpts.dictionaryParams.numThreads = 1;

    ppOpts.lassoParams.mode = ppOpts.dictionaryParams.mode;
    ppOpts.lassoParams.lambda = ppOpts.dictionaryParams.lambda;
    ppOpts.lassoParams.numThreads = ppOpts.dictionaryParams.numThreads;

    trainOpts.hogPreProcessOpts = ppOpts;

    trainOpts.predictorType = 'liblinear';
    trainOpts.predictorOpts.svmType = 3;
    trainOpts.predictorOpts.svmC = 0.1;
    trainOpts.predictorOpts.svmBias = -1;

  otherwise
    error('Invalid options string: %s\n',optsString);
end

opts.hogOpts = hogOpts;
opts.trainOpts = trainOpts;
