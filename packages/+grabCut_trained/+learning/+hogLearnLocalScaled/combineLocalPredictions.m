function seg = combineLocalPredictions(trainOpts,segPredictions,segBBoxes,...
         localSegMaskSize,segMaskSize)

switch(trainOpts.predictorType)
  case 'liblinear'
    seg = combineLibLinear(trainOpts,segPredictions,segBBoxes,...
                           localSegMaskSize,segMaskSize);
  case 'kmeans'
    seg = combineKmeans(trainOpts,segPredictions,segBBoxes,...
                           localSegMaskSize,segMaskSize);
  otherwise
    error('Predictor type: %s not yet supported\n',trainOpts.predictorType);
end

function seg = combineKmeans(trainOpts,segPredictions,segBBoxes,...
                           localSegMaskSize,segMaskSize)

accumCounts = zeros([segMaskSize(1) segMaskSize(2)]);
accumMasks = zeros([segMaskSize(1) segMaskSize(2)]);
assert(size(segPredictions,2)==size(segBBoxes,2),...
    'Inconsistent segboxes and seg predictions\n');
segPredictions = reshape(segPredictions,[localSegMaskSize(1) localSegMaskSize(2) ...
                         size(segPredictions,2)]);
for i=1:size(segBBoxes,2)
  bbox = segBBoxes(:,i);
  accumCounts(bbox(1):bbox(2),bbox(3):bbox(4)) = ...
      accumCounts(bbox(1):bbox(2),bbox(3):bbox(4))+1;
  accumMasks(bbox(1):bbox(2),bbox(3):bbox(4)) = ...
      accumMasks(bbox(1):bbox(2),bbox(3):bbox(4)) + segPredictions(:,:,i);
end
accumMasks = accumMasks./accumCounts;
seg = zeros(size(accumMasks),'uint8');
seg(accumMasks>=0.5) = 255;

function seg = combineLibLinear(trainOpts,segPredictions,segBBoxes,...
                           localSegMaskSize,segMaskSize)

accumVotes = zeros([segMaskSize(1) segMaskSize(2)]);
assert(size(segPredictions,2)==size(segBBoxes,2),...
    'Inconsistent segboxes and seg predictions\n');
segPredictions = reshape(segPredictions,[localSegMaskSize(1) localSegMaskSize(2) ...
                         size(segPredictions,2)]);
for i=1:size(segBBoxes,2)
  bbox = segBBoxes(:,i);
  accumVotes(bbox(1):bbox(2),bbox(3):bbox(4)) = accumVotes(bbox(1):bbox(2),bbox(3):bbox(4))...
      + segPredictions(:,:,i);
end

seg = zeros(size(accumVotes),'uint8');
seg(accumVotes>=0) = 255;
