% Class defines options for training on hogLearnGlobal
classdef hogOptsClass
  properties (SetAccess=public, GetAccess=public)
    cellSize % number of pixels in one square cell of hog
    hogType % different types: '180degree','360degree','pedro'
    % 'pedro' is pedro's implementation where he adds additional feature
    % '180degree' is 9 dimensional (orientations b/w 0-180)
    % '360degree' is 18 dimensional (orientations b/w 0-360)
    visHogBlockSize % Used for visualizing hog
  end

  methods
    function obj=hogOptsClass()
      obj.cellSize = 8;
      obj.hogType = '180degree';
      obj.visHogBlockSize = 30;
    end
  end

end % of classdef
