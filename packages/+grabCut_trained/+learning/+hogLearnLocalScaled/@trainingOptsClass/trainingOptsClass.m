% Class defines options for training on hogLearnGlobal
classdef trainingOptsClass
  properties (SetAccess=public, GetAccess=public)
    hogCellsInUpperBody % number of hog cells along the height dimension
    % of the uppder body, this implicitly decides the scale of the hogcell
    localSegMaskLongerDimension % number of pixels in longer dimension of segMask
    % decides dimensionality of the output space

    localHogH % number of vertical cells for local hog 
    localHogW % number of horizontal cells for local hog 

    hogPreProcess % set to pca to reduce dimensionality
    hogPreProcessOpts % options for preprocessing (structure)

    predictorType % 'kmeans', 'svmVedaldi'
    predictorOpts % depends on classifierType

    defaultUpperBodyHeight
    upperBodyDir % directory where upper body detections are stored
  end

  methods
    function obj=trainingOptsClass()
      obj.hogCellsInUpperBody = 4; % Computed approximately to match the setting
      % for hog learn local where 16 cells = height
      obj.defaultUpperBodyHeight = 100; % For images where upper body does not fire
      obj.localSegMaskLongerDimension = 40;
      obj.hogPreProcess = 'none';
      obj.hogPreProcessOpts = struct();
      obj.localHogH = 4;
      obj.localHogW = 4;
      obj.predictorType = 'kmeans';
      obj.predictorOpts.numClusters = 100;
      obj.predictorOpts.numIters = 200;
    end
  end

end % of classdef
