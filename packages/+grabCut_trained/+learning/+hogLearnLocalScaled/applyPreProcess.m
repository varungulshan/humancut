function hogs = applyPreProcess(hogs,trainOpts,ppInfo)

switch(trainOpts.hogPreProcess)
  case 'pca',
    hogs = applyPCA(hogs,trainOpts.hogPreProcessOpts,ppInfo);
  case {'sparseDictionary','sparseDictionary_saved'}
    hogs = applyDictionary(hogs,trainOpts.hogPreProcessOpts,ppInfo);
  case 'none',
    % do nothing
  otherwise
    error('Invalid pre process method: %s\n',trainOpts.hogPreProcess);
end

function hogs = applyDictionary(hogs,opts,ppInfo)

hogs = mexLasso(hogs,ppInfo.sparseDictionary,opts.lassoParams);

function hog = applyPCA(hog,opts,ppInfo)

hog = hog - ppInfo.meanHog;
hog = ppInfo.basis'*hog;
