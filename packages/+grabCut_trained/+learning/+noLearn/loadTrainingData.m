function data = loadTrainingData(trainDir,testDir)
% data will include:
%   data.grabCutOpts: options for grabCut
% testDir is used to output diagnostic information

data = load([trainDir 'grabCutOpts.mat']);
fH = fopen([testDir 'grabCutOpts.txt'],'w');
miscFns.printClass(data.grabCutOpts,fH);
fclose(fH);
