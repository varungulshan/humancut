function gcOpts = makeGrabCutOpts(optsString)

switch(optsString)
  case 'try1'
    gcOpts = grabCut.helpers.makeOpts('iter10_2');
  case 'try2'
    gcOpts = grabCut.helpers.makeOpts('iter10_ising');
  case 'iter10_3'
    gcOpts = grabCut.helpers.makeOpts('iter10_3');
  otherwise
    error('Invalid options string: %s\n',optsString);
end
