function opts = makeGrabCutOpts(optsString)

switch(optsString)
  case 'kovesi_edgeLearn_1'
    gcOpts = grabCut_edgeLearn.helpers.makeOpts('iter10_2_edgeLearn_kovesi');
    vary(1).fieldName='gcGamma';
    vary(1).fieldValues=num2cell([10 25 50 100 150]);
    scoreString='accuracyOverlap';
  case 'kovesi_1'
    gcOpts = grabCut_edgeLearn.helpers.makeOpts('iter10_2_kovesi');
    vary(1).fieldName='gcGamma';
    vary(1).fieldValues=num2cell([10 25 50 100 150]);
    scoreString='accuracyOverlap';
  case 'kovesi_2'
    gcOpts = grabCut_edgeLearn.helpers.makeOpts('iter10_2_kovesi');
    vary(1).fieldName='gcGamma';
    vary(1).fieldValues=num2cell([10 25 50 100 150]);
    vary(2).fieldName='kovesiAutoThresh';
    vary(2).fieldValues=num2cell([0.3 0.5 0.7 0.9]);
    vary(3).fieldName='kovesiSigma';
    vary(3).fieldValues=num2cell([1 2 3]);
    scoreString='accuracyOverlap';
  case 'smallTestKovesi'
    gcOpts = grabCut_edgeLearn.helpers.makeOpts('iterQuickKovesi');
    vary(1).fieldName='kovesiAutoThresh';
    vary(1).fieldValues=num2cell([0.6 0.7 0.8]);
    vary(2).fieldName='kovesiSigma';
    vary(2).fieldValues=num2cell([1 3]);
    scoreString='accuracyOverlap';
  case 'smallTest'
    gcOpts = grabCut_edgeLearn.helpers.makeOpts('iterQuick');
    vary(1).fieldName='gcGamma';
    vary(1).fieldValues=num2cell([5 10 25]);
    vary(2).fieldName='gmmNmix_fg';
    vary(2).fieldValues=num2cell([3 5]);
    scoreString='accuracyOverlap';
  otherwise
    error('Invalid options string: %s\n',optsString);
end

opts.grabCutOpts = gcOpts;
opts.vary = vary;
opts.scoreString = scoreString;
