function [means,labels]=clusterAspectRatios(ratios,numClusters,numIters)

[xx,means,xx,labels,xx]=grabCut_trained.learning.hogLearnGlobal.COMKMeans(...
    ratios,[],numClusters,numIters,1);
