function opts = makeOpts(optsString)

import grabCut_trained.learning.hogLearnGlobal.*;

switch(optsString)
  case 'try1'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degree';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.numAspects = 3;
    trainOpts.numItersClustering = 200;
    trainOpts.hogCellsInLongerDim = 16;
    trainOpts.segMaskLongerDimension = 128;

  case 'singleAspect'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 8;
    hogOpts.hogType = '180degree';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.numAspects = 1;
    trainOpts.numItersClustering = 200;
    trainOpts.hogCellsInLongerDim = 16;
    trainOpts.segMaskLongerDimension = 128;

  case '2AspectsBigCell_PCA2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 20;
    hogOpts.hogType = '180degree';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.numAspects = 2;
    trainOpts.numItersClustering = 200;
    trainOpts.hogCellsInLongerDim = 16;
    trainOpts.segMaskLongerDimension = 128;
    trainOpts.hogPreProcess = 'pca';
    trainOpts.hogPreProcessOpts.dimensionality = 150;

  case '2AspectsBigCell_PCA2_hog2'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 20;
    hogOpts.hogType = '180degreeAndNormalizers';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.numAspects = 2;
    trainOpts.numItersClustering = 200;
    trainOpts.hogCellsInLongerDim = 16;
    trainOpts.segMaskLongerDimension = 128;
    trainOpts.hogPreProcess = 'pca';
    trainOpts.hogPreProcessOpts.dimensionality = 150;


  case '3AspectsBigCell_PCA'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 20;
    hogOpts.hogType = '180degree';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.numAspects = 3;
    trainOpts.numItersClustering = 200;
    trainOpts.hogCellsInLongerDim = 16;
    trainOpts.segMaskLongerDimension = 128;
    trainOpts.hogPreProcess = 'pca';
    trainOpts.hogPreProcessOpts.dimensionality = 30;

  case 'singleAspectBigCell'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 20;
    hogOpts.hogType = '180degree';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.numAspects = 1;
    trainOpts.numItersClustering = 200;
    trainOpts.hogCellsInLongerDim = 16;
    trainOpts.segMaskLongerDimension = 128;

  case '3AspectsBigCell'
    hogOpts = hogOptsClass();
    hogOpts.cellSize = 20;
    hogOpts.hogType = '180degree';
    hogOpts.visHogBlockSize = 20;

    trainOpts = trainingOptsClass();
    trainOpts.numAspects = 3;
    trainOpts.numItersClustering = 200;
    trainOpts.hogCellsInLongerDim = 16;
    trainOpts.segMaskLongerDimension = 128;
  
  otherwise
    error('Invalid options string: %s\n',optsString);
end

opts.hogOpts = hogOpts;
opts.trainOpts = trainOpts;
