function seg = testOnImg(img,labelImg,trainData,debugOpts)

import grabCut_trained.learning.hogLearnGlobal.*;

img = im2double(img);
aspectInfo = trainData.aspectInfo;
opts = trainData.opts;
aspectData = trainData.aspectData;

[bbox,bboxAspect] = getBoxAspectRatio(labelImg);
[xx,aspectNum] = min(abs(aspectInfo.aspectMeansQuantized-bboxAspect));

hogBoxSize = aspectInfo.bboxSizes(:,aspectNum);
segMaskSize = getSegMaskSize(hogBoxSize,opts.trainOpts.segMaskLongerDimension);

bbox = snapBBoxToSize(bbox,hogBoxSize);
localImg = cpp.mex_extractBBox(img,int32(bbox-1));
localImg = imresize(localImg,hogBoxSize','bilinear');
hog = hogCode.mex_getHog(localImg,opts.hogOpts.cellSize);

if(debugOpts.debugLevel>=10)
  visHog = drawHog(hog,opts.hogOpts);
  miscFns.saveDebug(debugOpts,visHog,'hog.png');
  miscFns.saveDebug(debugOpts,imresize(localImg,size(visHog)),'img.jpg');
end

hog = selectHogType(hog,opts.hogOpts);
hog = hog(:);

segPrediction = predictSeg(hog,opts.trainOpts,aspectData(aspectNum));
segPrediction = reshape(segPrediction,[segMaskSize(1) segMaskSize(2)]);

if(debugOpts.debugLevel>=10)
  tmpSeg = segPrediction;
  tmpSeg(segPrediction<0)=0;
  tmpSeg(segPrediction>1)=1;
  miscFns.saveDebug(debugOpts,imresize(tmpSeg,size(visHog)),'segPredicted.jpg');
end

seg = snapPredictionToBBox(segPrediction,bbox,[size(img,1) size(img,2)]);

if(debugOpts.debugLevel > 0)
  overlay = grabCut_trained.helpers.overlaySeg(img,seg);
  overlay = grabCut_trained.helpers.overlayBox(overlay,labelImg);
  miscFns.saveDebug(debugOpts,overlay,'seg.jpg'); 
end

function seg = snapPredictionToBBox(segPrediction,bbox,imgSize)

bboxH = bbox(4)-bbox(2)+1;
bboxW = bbox(3)-bbox(1)+1;
segPrediction = imresize(segPrediction,[bboxH bboxW],'bilinear');
segPrediction(segPrediction<0.5)=0;
segPrediction(segPrediction>=0.5)=1;
segPrediction = im2uint8(segPrediction);
seg = zeros(imgSize,'uint8');

deltaLeft = max(0,1-bbox(1));
deltaRight = max(0,bbox(3)-imgSize(2));
deltaTop = max(0,1-bbox(2));
deltaBottom = max(0,bbox(4)-imgSize(1));

seg( (bbox(2)+deltaTop):(bbox(4)-deltaBottom), ...
     (bbox(1)+deltaLeft):(bbox(3)-deltaRight)) = segPrediction(...
     (1+deltaTop):(end-deltaBottom),(1+deltaLeft):(end-deltaRight)); 

function segPrediction = predictSeg(hog,trainOpts,trainData)
% Apply pre-processing if needed
switch(trainOpts.hogPreProcess)
  case 'pca'
    hog = applyPCA(hog,trainData.ppInfo);
end

hog = [hog;1];
segPrediction = trainData.w'*hog;

function hog = applyPCA(hog,ppInfo)

hog = hog - ppInfo.meanHog;
hog = ppInfo.basis'*hog;
