function bbox = snapBBoxToSize(bbox,hogBoxSize)
% Snaps bbox to aspectRatio of hogBox size, by only increasing its size

hBoxOrig = bbox(4)-bbox(2)+1;
wBoxOrig = bbox(3)-bbox(1)+1;

hBox = hBoxOrig;
wBox = wBoxOrig;

aspectHog = hogBoxSize(2)/hogBoxSize(1);

if(wBox/hBox < aspectHog)
  wBox = hBox * aspectHog;
else
  hBox = wBox/aspectHog;
end

wBoxDelta = (wBox - wBoxOrig)/2;
hBoxDelta = (hBox - hBoxOrig)/2;
assert(wBoxDelta>=0);
assert(hBoxDelta>=0);

bbox(1) = round(bbox(1)-wBoxDelta);
bbox(2) = round(bbox(2)-hBoxDelta);
bbox(3) = round(bbox(3)+wBoxDelta);
bbox(4) = round(bbox(4)+hBoxDelta);


