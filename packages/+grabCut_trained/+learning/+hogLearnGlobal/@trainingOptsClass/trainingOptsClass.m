% Class defines options for training on hogLearnGlobal
classdef trainingOptsClass
  properties (SetAccess=public, GetAccess=public)
    numAspects % number of clusters for aspect ratio
    numItersClustering % number of iterations for clustering aspect ratio

    hogCellsInLongerDim % number of hog cells along the longer dimension
    % decides the resolution at which to make hog cells (along with the 
    % hog cell size)
    segMaskLongerDimension % number of pixels in longer dimension of segMask
    % decides dimensionality of the output space (together with the aspect ratio
    % of the box)

    visCanny % set to true to visualize extracted canny edges, though canny edges
    % are not used currently, just visualizing them to see if they are useful

    hogPreProcess % set to pca to reduce dimensionality
    hogPreProcessOpts % options for preprocessing (structure)
    lambda % regularizer for least squares
  end

  methods
    function obj=trainingOptsClass()
      obj.numAspects = 1;
      obj.numItersClustering = 1;
      obj.hogCellsInLongerDim = 16;
      obj.segMaskLongerDimension = 128;
      obj.visCanny = false;
      obj.hogPreProcess = 'none';
      obj.hogPreProcessOpts = struct();
      obj.lambda = 1e-9;
    end
  end

end % of classdef
