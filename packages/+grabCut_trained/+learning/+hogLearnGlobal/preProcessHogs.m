function [hogs,ppInfo]=preProcessHogs(hogs,trainOpts)
% TO apply pre-processing such as dimensionality reduction

switch(trainOpts.hogPreProcess)
  case 'pca',
    [hogs,ppInfo] = pcaPreprocess(hogs,trainOpts.hogPreProcessOpts);
  otherwise
    ppInfo = [];
end

function [hogs,ppInfo] = pcaPreprocess(hogs,opts)

newDim = opts.dimensionality;
assert(newDim <= size(hogs,1));

hogs = hogs';
meanHog = mean(hogs,1)';
[basis,newHogs] = princomp(hogs);

newHogs = newHogs(:,1:newDim);
basis = basis(:,1:newDim);

hogs = newHogs';
ppInfo.meanHog = meanHog;
ppInfo.basis = basis;
