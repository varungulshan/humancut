function [hogs,segMasks,segMaskSize] = extractTrainData(imgDir,labelDir,gtDir,...
    images,hogBoxSize,hogOpts,trainOpts,debugOpts)

import grabCut_trained.learning.hogLearnGlobal.*;

N = length(images);
hogs = zeros(getHogDimensionality(hogOpts,hogBoxSize),N);
segMaskSize = getSegMaskSize(hogBoxSize,trainOpts.segMaskLongerDimension);
segMasks = zeros(prod(segMaskSize),N);

for i=1:length(images)
  objectId = grabCut_trained.dataset.extractObjectIdentifier(images{i});

  tmpDebugOpts = debugOpts;
  tmpDebugOpts.debugPrefix = [objectId '_'];

  labelFile = [labelDir objectId '.png'];
  labelImg = imread(labelFile);
  [bbox,bboxAspect] = getBoxAspectRatio(labelImg);
  img = imread([imgDir grabCut_trained.dataset.extractImgName(images{i})]);
  img = im2double(img);
  gt = imread([gtDir objectId '.png']);
  [hogs(:,i),segMasks(:,i)] = getHogAndSeg(img,gt,bbox,hogBoxSize,segMaskSize,hogOpts,...
      tmpDebugOpts);
end

function [hog,segMask] = getHogAndSeg(img,gt,bbox,hogBoxSize,segMaskSize,hogOpts,...
    debugOpts)

import grabCut_trained.learning.hogLearnGlobal.*;

bbox = snapBBoxToSize(bbox,hogBoxSize);

tmpSegMask = cpp.mex_extractBBox(gt,int32(bbox-1));
tmpSegMask = imresize(tmpSegMask,segMaskSize','nearest');
segMask = zeros(size(tmpSegMask));
segMask(tmpSegMask==128)=0.5;
segMask(tmpSegMask==255)=1;
if(debugOpts.debugLevel>=10)
  copySegMask = segMask;
end
segMask = segMask(:);
localImg = cpp.mex_extractBBox(img,int32(bbox-1));
localImg = imresize(localImg,hogBoxSize','bilinear');
hog = hogCode.mex_getHog(localImg,hogOpts.cellSize);
if(debugOpts.debugLevel>=10)
  visHog = drawHog(hog,hogOpts);
  miscFns.saveDebug(debugOpts,visHog,'hog.png');
  miscFns.saveDebug(debugOpts,imresize(localImg,size(visHog)),'img.jpg');
  miscFns.saveDebug(debugOpts,imresize(copySegMask,size(visHog),'nearest'),'seg.png');
end
% hog will be a H x W x (27+4) matrix, H = number of vertical cells, W = horiz cells
hog = selectHogType(hog,hogOpts);

%hog = shiftdim(hog,2); % to make it vertical, not needed right now
% note: hog is H x W x D matrix, so doing hog(:) puts different hog cells together
% so right now it doesnt matter, but you might want to do the shiftdim(hog,2) later
hog = hog(:);

function dim = getHogDimensionality(hogOpts,hogBoxSize)

cellSize = hogOpts.cellSize;
assert(mod(hogBoxSize(1),cellSize)==0);
assert(mod(hogBoxSize(2),cellSize)==0);

numCells = (hogBoxSize(1)/cellSize)*(hogBoxSize(2)/cellSize);

switch(hogOpts.hogType)
  case '180degree'
    singleHogDim = 9; % this can depend on hog type    
  case '180degreeAndNormalizers'
    singleHogDim = 13; % this can depend on hog type    
  case '360degree'
    singleHogDim = 18; % this can depend on hog type    
  case 'pedro'
    singleHogDim = 27+4; % this can depend on hog type    
  otherwise
    error('Invalid hog type:%s\n',hogOpts.hogType);
end

dim = singleHogDim * numCells;
