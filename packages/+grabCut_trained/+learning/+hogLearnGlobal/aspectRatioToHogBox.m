function boxSize = aspectRatioToHogBox(ratio,cellSize,maxCellsInOneDim)
% Takes the aspect ratio and returns a box size which can fit
% integral number of hog cells, while being as close as possible to the
% aspect ratio
% boxSize is [h;w]

biggerDim = cellSize * maxCellsInOneDim;
smallerRatio = min(ratio,1/ratio);
numCellsInSmaller = round(smallerRatio * maxCellsInOneDim);
smallerDim = cellSize * numCellsInSmaller;

if(ratio>1)
  boxSize = [smallerDim;biggerDim];
else
  boxSize = [biggerDim;smallerDim];
end
