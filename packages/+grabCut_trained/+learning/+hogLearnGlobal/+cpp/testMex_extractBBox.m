function testMex_extractBBox()

import grabCut_trained.learning.hogLearnGlobal.cpp.*;
cwd=miscFns.extractDirPath(mfilename('fullpath'));

img=imread([cwd 'cropTest.jpg']);

[h w nCh] = size(img);

bbox1 = [1-30 1-30 w+30 h+30];
crop1 = mex_extractBBox(im2double(img),int32(bbox1-1));
figure;imshow(crop1);
crop2 = mex_extractBBox(im2uint8(rgb2gray(img)),int32(bbox1-1));
figure;imshow(crop2);
