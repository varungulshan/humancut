function trainHogRegression(params,trainOutDir,testBenchOpts)

import grabCut_trained.learning.hogLearnGlobal.*;

assert(strcmp(params.trainMethod,'hogLearnGlobal'));

opts = makeOpts(params.trainMethod_optsString);
evalOpts = testBench.evalOpts_grabCut(params.evalOpts_string);
gtDir = evalOpts.gtDir;
save([trainOutDir 'trainOpts.mat'],'opts');
trainingDataDir = [trainOutDir 'trainingData/'];
mkdir(trainingDataDir);

images = textread(testBenchOpts.imgList,'%s');
imgDir = testBenchOpts.imgDir;
labelDir = testBenchOpts.labelDir;

trainOpts = opts.trainOpts;
hogOpts = opts.hogOpts;
% ---- First cluster the boxes by aspect ratio ------
aspectRatios = zeros(1,length(images));
boxes = zeros(4,length(images)); % each box is column vector [x1;y1;x2;y2]
% (x1,y1) = top left, (x2,y2) = bottom right
for i=1:length(images)
  %imgName = grabCut_trained.dataset.extractImgName(images{i});
  objectId = grabCut_trained.dataset.extractObjectIdentifier(images{i});
  labelFile = [labelDir objectId '.png'];
  labelImg = imread(labelFile);
  [boxes(:,i),aspectRatios(i)] = getBoxAspectRatio(labelImg);
end

[aspectMeans,aspectLabels] = clusterAspectRatios(aspectRatios,...
                             trainOpts.numAspects,trainOpts.numItersClustering);
listByAspect = cell(1,trainOpts.numAspects);
bboxSizes = zeros(2,trainOpts.numAspects);
aspectMeansQuantized = aspectMeans;
for i=1:trainOpts.numAspects
  listByAspect{i} = find(aspectLabels==i);
  bboxSizes(:,i) = aspectRatioToHogBox(aspectMeans(i),hogOpts.cellSize,...
                  trainOpts.hogCellsInLongerDim);
  aspectMeansQuantized(i) = bboxSizes(2,i)/bboxSizes(1,i);
end

save([trainingDataDir 'aspectRatioInfo.mat'],'aspectMeansQuantized',... 
    'bboxSizes','listByAspect');

for i=1:trainOpts.numAspects
  debugOpts = struct();
  debugOpts.debugLevel = params.debugLevel;
  debugOpts.debugDir = [trainOutDir sprintf('aspect%d_debug/',i)];
  debugOpts.debugPrefix = '';
  mkdir(debugOpts.debugDir);
  [hogs,segMasks,segMaskSize]=extractTrainData(imgDir,labelDir,gtDir, ...
                  images(listByAspect{i}),bboxSizes(:,i),...
                  hogOpts,trainOpts,debugOpts);
  % hogs = D x N vector, segMasks = D2 x N vector, N = number of images
  % Add a constant to hog
  [hogs,ppInfo] = preProcessHogs(hogs,trainOpts);
  hogs = [hogs;ones(1,size(hogs,2))];
  w = trainLinearRegressor(hogs,segMasks,trainOpts);
  if(debugOpts.debugLevel>=10)
    visHogSize = hogOpts.visHogBlockSize * bboxSizes(:,i)/hogOpts.cellSize;
    saveReconstructions(hogs,w,debugOpts,segMaskSize,images(listByAspect{i}),visHogSize);
  end
  save([trainingDataDir sprintf('aspect_%d_regressor.mat',i)],...
        'w','ppInfo');
end

%fSummary = fopen([trainOutDir 'trainSummary.txt'],'w');
%fclose(fSummary);

function saveReconstructions(hogs,w,debugOpts,segMaskSize,images,newSize)

tmpSegs = w'*hogs;
tmpSegs(tmpSegs<0)=0;
tmpSegs(tmpSegs>1)=1;
for i=1:size(tmpSegs,2)
  curSeg = reshape(tmpSegs(:,i),[segMaskSize(1) segMaskSize(2)]);
  objectId = grabCut_trained.dataset.extractObjectIdentifier(images{i});
  debugOpts.debugPrefix = [objectId '_'];
  miscFns.saveDebug(debugOpts,imresize(curSeg,[newSize(1) newSize(2)])...
     ,'segPredicted.jpg');
end

function W = trainLinearRegressor(X,Y,trainOpts)
% X is a D1 x N matrix, D1 = dimensonality, N = data points
% Y is a D2 x N matrix, D2 = dimensonality, N =  data points
% W is a D1 x D2 matrix, W'X is a segmentation mask

Y=Y';
% Equation is minimising (1/N)|X'W-Y|^2 + \lambda |W|^2(l2 norm)
% think of 1D output space (i.e 1 pixel of the output) first, and.
% then of the whole segmentation
N = size(X,2);
D = size(X,1);
lambda = trainOpts.lambda;
W = inv(X*X'+N*lambda*eye(D))*X*Y;
