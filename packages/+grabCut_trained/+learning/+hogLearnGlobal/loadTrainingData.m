function data = loadTrainingData(trainDir,testDir)
% data will include:
%   data.grabCutOpts: options for grabCut
% testDir is used to output diagnostic information

trainingDataDir = [trainDir 'trainingData/'];
tmpLoad = load([trainDir 'trainOpts.mat']);
opts = tmpLoad.opts;
aspectInfo = load([trainingDataDir 'aspectRatioInfo.mat']);
aspectData = load([trainingDataDir sprintf('aspect_%d_regressor.mat',1)]);
for i=2:numel(aspectInfo.aspectMeansQuantized)
  aspectData(i) =  load([trainingDataDir sprintf('aspect_%d_regressor.mat',i)]);
end

data.opts = opts;
data.aspectInfo = aspectInfo;
data.aspectData = aspectData;
