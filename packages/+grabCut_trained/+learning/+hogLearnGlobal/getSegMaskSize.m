function segMaskSize = getSegMaskSize(hogBoxSize,longerDimension)
% Returns the dimensions of the segmask upon which regression is done
% depends on aspect ratio of hog box and the maximum segmask dimension

longerDimHog = max(hogBoxSize);
rescale = longerDimension/longerDimHog;
segMaskSize = round(hogBoxSize * rescale);
