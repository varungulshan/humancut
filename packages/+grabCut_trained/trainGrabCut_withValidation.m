function trainGrabCut_withValidation(params)
% Learns a model for the head given training data

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('params','var'))
  params.rootOut_dir=[cwd '../../results/grabCut_trained/train/gcWithValidation/'];
  params.rootOut_dir_test=[cwd '../../results/grabCut_trained/test/gcWithValidation/'];
  params.testEnable = true;
  params.opts.evalOpts_string='kinect_10Feb2011';
  %params.opts.evalOpts_string='try1';
  
  params.opts.trainMethod='trivialSeg';
  %params.opts.trainMethod='grabCut_simpleSearch';
  %params.opts.trainMethod_optsString='tryDictionary1';
  %params.opts.trainMethod_optsString='kmeansDictionary_saved_svm1_bias';
  params.opts.trainMethod_optsStrings={'try1','try2'};

  params.opts.visualize=true;
  params.opts.visOpts.debugVisualize=true;
  params.opts.debugLevel=10;
  params.opts.visOpts.imgExt='jpg';
  params.opts.visOpts.segBdryWidth=2;
  params.overWrite=true;

  %params.testBench_cmd='testBench.getTests_h3d_headTrain()';
  %params.testBench_cmd='testBench.getTests_grabCut_small()';
  %params.testBench_cmd='testBench.getTests_grabCut_h3d_train_1()';
  %params.testBench_cmd='testBench.getTests_grabCut_kinect10Feb2011_1_train_small()';
  params.testBench_cmd='testBench.getTests_grabCut_kinect10Feb2011_3_trainWithValidation_1()';
  params.testBench_cmd_test='testBench.getTests_grabCut_kinect10Feb2011_3_test_1()';
  %params.testBench_cmd='testBench.getTests_grabCut_tmp()';
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',params.rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
end

testBenchOpts=eval(params.testBench_cmd);
save([params.rootOut_dir 'testBenchOpts.mat'],'testBenchOpts');
save([params.rootOut_dir 'params.mat'],'params');

paramArray = makeParameterArray(params,testBenchOpts);
% Each row is a validation fold for one parameter setting

paramsDir = [params.rootOut_dir 'allParams/'];
if(~exist(paramsDir,'dir')), mkdir(paramsDir); end
validationDir = [params.rootOut_dir 'validations/'];
if(~exist(validationDir,'dir')), mkdir(validationDir); end

jobIds = cell(1,numel(paramArray));

for paramNum = 1:size(paramArray,1)
  for foldNum = 1:size(paramArray,2)
    tmpParams = paramArray(paramNum,foldNum);
    paramFile = [paramsDir sprintf('params_paramNum%d_fold%d.mat',...
        paramNum,foldNum)];
    save(paramFile,'tmpParams');
    curJobId = submitTrainTestJob(paramFile);
    jobIds{paramNum+(foldNum-1)*size(paramArray,1)} = curJobId;
    fprintf('Submitted fold number = %d, for parameter setting %d with job ID = %s\n',foldNum,paramNum,curJobId);
  end
end

accumulateJobId = submitAccumulateJob(params.rootOut_dir,jobIds);
fprintf('Submitted accumulate job: %s\n',accumulateJobId);

function jobId = submitTrainTestJob(paramFile)

scriptFile = 'trainTestJob';
jobId = grabCut_trained.helpers.qsubScript(scriptFile,{},paramFile);

function jobId = submitAccumulateJob(rootDir,dependencyJobs)
% Also runs the training on the full dataset after accumulation
scriptFile = 'accumulateValidations';
jobId = grabCut_trained.helpers.qsubScript(scriptFile,dependencyJobs,...
    rootDir);

function paramArray = makeParameterArray(params,testBenchOpts)

numFolds = numel(testBenchOpts.trainTestBenchCmds);
assert(numFolds==numel(testBenchOpts.validateTestBenchCmds));

numParameters = numel(params.opts.trainMethod_optsStrings);

paramArray = struct('rootOut_dir',[],...
                    'opts',[],'overWrite',[],'testBench_cmd_train',[],...
                    'testBench_cmd_validate',[]);
tmpParams = paramArray;

tmpParams.opts = rmfield(params.opts,'trainMethod_optsStrings');
tmpParams.overWrite = params.overWrite;

for i=1:numParameters
  for j=1:numFolds
    tmpParams.rootOut_dir = [params.rootOut_dir 'validations/' ...
        sprintf('optsString_%s_foldNum_%d/',...
        params.opts.trainMethod_optsStrings{i},j)];
    tmpParams.opts.trainMethod_optsString = ...
        params.opts.trainMethod_optsStrings{i};
    tmpParams.testBench_cmd_train = testBenchOpts.trainTestBenchCmds{j};
    tmpParams.testBench_cmd_validate = testBenchOpts.validateTestBenchCmds{j};
    paramArray(i,j) = tmpParams;
  end
end

function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);
