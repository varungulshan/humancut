function img=overlayBox(img,labels,lineRad)
% Shows local windows in previous frame which were used to propagate segmentation
% to the current frame

if(~exist('lineRad','var'))
  lineRad = ceil(max(size(img))*0.0015);
end

boxColor = [1 0 0]; % FG box
%boxColor = [1 1 1]; % FG box
fgBox = (labels==3);
if(any(fgBox(:)))
  topLeft = find(fgBox,1,'first');
  bottomRight = find(fgBox,1,'last');
  [yMin,xMin] = ind2sub(size(labels),topLeft);
  [yMax,xMax] = ind2sub(size(labels),bottomRight);
  img = drawBox(img,boxColor,lineRad,xMin,xMax,yMin,yMax);
end

boxColor = [0 1 0]; % BG box
bgBox = (labels==2);
if(any(bgBox(:)))
  topLeft = find(bgBox,1,'first');
  bottomRight = find(bgBox,1,'last');
  [yMin,xMin] = ind2sub(size(labels),topLeft);
  [yMax,xMax] = ind2sub(size(labels),bottomRight);
  img = drawBox(img,boxColor,lineRad,xMin,xMax,yMin,yMax);
end

function img=drawBox(img,winClr,penRad,xMin,xMax,yMin,yMax)

[h w nCh]=size(img);

% Clockwise: left bar, top bar, right bar, bottom bar
xL=[xMin-penRad xMin-penRad xMax-penRad xMin-penRad];
xR=[xMin+penRad xMax+penRad xMax+penRad xMax+penRad];
yT=[yMin-penRad yMin-penRad yMin-penRad yMax-penRad];
yB=[yMax+penRad yMin+penRad yMax+penRad yMax+penRad];

xL=max(1,xL);
xR=min(w,xR);
yT=max(1,yT);
yB=min(h,yB);

for j=1:4
  x_left=xL(j);
  x_right=xR(j);
  y_top=yT(j);
  y_bottom=yB(j);

  for i=1:3
    img(y_top:y_bottom,x_left:x_right,i)=winClr(i);
  end
end
