function img=overlaySeg(img,seg,lineWidth)

if(~exist('lineWidth','var'))
  lineWidth = ceil(max(size(img))*0.0045);
end

[segBdryMask,segBdryColors]=miscFns.getSegBoundary_twoColors(seg,[0 0 1],...
                            [1 1 0],lineWidth,lineWidth);
img(segBdryMask)=segBdryColors;
