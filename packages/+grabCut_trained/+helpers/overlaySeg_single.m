function img=overlaySeg_single(img,seg,lineWidth)
% Single colored boundary

if(~exist('lineWidth','var'))
  lineWidth = ceil(max(size(img))*0.0045);
end

%color = [1 1 1];
%color = [1 0 0];
color = [0 1 1];
%[segBdryMask,segBdryColors]=miscFns.getSegBoundary_twoColors(seg,[1 0 0],...
                            %[1 1 0],lineWidth,0);
lineWidth = round(lineWidth/2);

[segBdryMask,segBdryColors]=miscFns.getSegBoundary_twoColors(seg,color,...
                            color,lineWidth,lineWidth);
img(segBdryMask)=segBdryColors;
