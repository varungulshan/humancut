function img=overlaySeg_single(img,seg,lineWidth)
% Single colored boundary

if(~exist('lineWidth','var'))
  lineWidth = ceil(max(size(img))*0.0045);
end

[segBdryMask,segBdryColors]=miscFns.getSegBoundary_twoColors(seg,[0 1 1],...
                            [0 1 1],0,lineWidth);
img(segBdryMask)=segBdryColors;
