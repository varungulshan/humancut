function jobId = qsubScript(scriptFile,dependencyJobs,varargin)
% Code to submit a matlab qsub job that runs scriptFile
% with arguments in varargin

memoryLimit = 4; % In units of gigs
qsubCmd = 'qsub -cwd ';
qsubCmd = [qsubCmd sprintf('-l vf=%dG ',memoryLimit)];

numDeps = numel(dependencyJobs);
if(numDeps>0)
  qsubCmd = [qsubCmd '-hold_jid '];
  for i=1:(numDeps-1)
    qsubCmd = [qsubCmd dependencyJobs{i} ','];
  end
  qsubCmd = [qsubCmd dependencyJobs{end} ' '];
end

qsubCmd = [qsubCmd 'mats2 '];
qsubCmd = [qsubCmd '"' makeScriptCall(scriptFile,varargin{:}) '"'];

[xx,qsubOut] = system(qsubCmd);
jobId = parseQsubOut(qsubOut);

function scriptCall = makeScriptCall(scriptFile,varargin)

numArgs = numel(varargin);

argString = '';
if(numArgs>0)
  for i=1:(numArgs-1)
    argString = [argString putArg(varargin{i}) ','];  
  end
  argString = [argString putArg(varargin{end})];  
end

scriptCall = [scriptFile '(' argString ')'];

function strArg = putArg(inArg)

if(isnumeric(inArg))
  strArg = mat2str(inArg);
elseif(ischar(inArg))
  strArg = ['''' inArg ''''];
else
  error('Invalid class %s for inArg\n',class(inArg));
end

function jobId = parseQsubOut(qsubOut)

regex = 'Your job (\d+) \(.*';
tokens = regexp(qsubOut,regex,'tokens');
assert(numel(tokens)>=1,'No match found for qsub out\n');
assert(numel(tokens{1})>=1,'No match found for qsub out\n');
jobId = tokens{1}{1};
