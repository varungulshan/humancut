function posterior=visualize_unary(unaryEnergy,unaryMask,betaVisualize)

if(~exist('unaryMask','var')),
  unaryMask=128*ones([size(unaryEnergy,1) size(unaryEnergy,2)],'uint8');
end

if(~exist('betaVisualize','var')),
  betaVisualize=1;
end
fgE=betaVisualize*unaryEnergy(:,:,1);
bgE=betaVisualize*unaryEnergy(:,:,2);
posterior=exp(-fgE)./(exp(-fgE)+exp(-bgE));

posterior(unaryMask==255)=1;
posterior(unaryMask==0)=0;

%figure;imshow(posterior);
