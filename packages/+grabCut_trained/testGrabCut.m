function testGrabCut(params)
% Runs segmentation, using trained parameters for grabCut

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('params','var'))
  %params.rootOut_dir=[cwd '../../results/grabCut_trained/test/noLearning/'];
  %params.rootOut_dir=[cwd '../../results/grabCut_trained/test/hogLearning/'];
  params.rootOut_dir=[cwd '../../results/grabCut_trained/test/gcWithHogLearning/'];
  %params.rootOut_dir=[cwd '../../results/grabCut_trained/test/debugGC_random50/'];

  %params.trainDir=[cwd '../../results/grabCut_trained/train/noLearning/'];
  %params.trainDir=['/data/titus_scratch/varun/iccv2011/results/train/run012/'];
  %params.trainDir=[cwd '../../results/grabCut_trained/train/hogLearning/'];
  %params.trainDir=[cwd '../../results/grabCut_trained/train/gcWithHogLearning/'];
  %params.trainDir='/data1/varun/iccv2011/results/train/run045/';
  %params.trainDir='/data1/varun/iccv2011/results/train/run103/bestTrain/';
  params.trainDir='/data2/varun/iccv2011/results/train/run115/';
  %params.trainDir='/data2/varun/iccv2011/results/train/run013/';
  
  %params.opts.trainMethod='noLearn';
  %params.opts.trainMethod_optsString='try1';

  params.opts.visualize=true;
  params.opts.visOpts.debugVisualize=true;
  params.opts.debugLevel=10;
  params.opts.visOpts.imgExt='jpg';
  params.opts.visOpts.segBdryWidth=2;
  params.overWrite=true;

  %params.evalOpts_string='try1';
  params.evalOpts_string='kinect_10Feb2011';

  params.testBench_cmd='testBench.getTests_grabCut_tmp()';
  %params.testBench_cmd='testBench.getTests_grabCut_small()';
  %params.testBench_cmd='testBench.getTests_grabCut_h3d_test_1()';
  %params.testBench_cmd='testBench.getTests_grabCut_kinect10Feb2011_1_test_1()';
  %params.testBench_cmd='testBench.getTests_grabCut_kinect10Feb2011_3_test_1()';
  %params.testBench_cmd='testBench.getTests_grabCut_kinect10Feb2011_1_test_small()';
  %params.testBench_cmd='testBench.getTests_grabCut_kinect10Feb2011_1_train_small()';
  
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',params.rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
end

testOpts=eval(params.testBench_cmd);
save([params.rootOut_dir 'testOpts.mat'],'testOpts');
tmpLoad = load([params.trainDir 'params.mat']);
trainParams = tmpLoad.params;

saveTrainingParams_file(params,trainParams);
 
segOutDir=[params.rootOut_dir 'segs/'];
debugDir = [params.rootOut_dir 'debug/'];

if(~exist(segOutDir,'dir')), mkdir(segOutDir); end;
if(~exist(debugDir,'dir')), mkdir(debugDir); end;

assert(strcmp(params.trainDir,trainParams.rootOut_dir));
%cmd=['ln -s ' params.trainDir ' ' params.rootOut_dir 'trainingDir_link'];
%system(cmd);

fTime = fopen([params.rootOut_dir 'timing.txt'],'w');
stTime = clock;
testModel(testOpts,params.opts,params.trainDir,params.rootOut_dir,...
          segOutDir,trainParams,debugDir);
fprintf(fTime,'Time taken to test on data  = %.2f minutes (%.1fhrs)\n',etime(clock,stTime)/60,etime(clock,stTime)/3600);
stTime = clock;
testBench.computeStats_grabCut(params.rootOut_dir,params.evalOpts_string);
fprintf(fTime,'Time taken to compute stats  = %.2f minutes (%.1fhrs)\n',etime(clock,stTime)/60,etime(clock,stTime)/3600);
fclose(fTime);

function testModel(testOpts,opts,trainDir,testDir,segOutDir,trainParams,debugDir)

trainData = loadTrainData(trainParams,testDir);
testFunction = getTestFunction(trainParams.opts.trainMethod);

images = textread(testOpts.imgList,'%s');
imgDir = testOpts.imgDir;
labelDir = testOpts.labelDir;

parfor i = 1:length(images)
%for i = 1:length(images)
  fprintf('Running on %s\n',images{i});
  imgName = grabCut_trained.dataset.extractImgName(images{i});
  imgPath = [imgDir imgName];
  objectId = grabCut_trained.dataset.extractObjectIdentifier(images{i});
  labelPath = [labelDir objectId '.png'];
  debugOpts = struct();
  debugOpts.debugLevel = opts.debugLevel;
  debugOpts.debugDir = debugDir;
  debugOpts.debugPrefix = [objectId '_'];
  segOut = testFunction(imread(imgPath),imread(labelPath),trainData,debugOpts);
  imwrite(segOut,[segOutDir objectId '.png']);
end

function data = loadTrainData(trainParams,testDir)

switch(trainParams.opts.trainMethod)
  case 'trivialSeg'
    data = grabCut_trained.learning.trivialSeg.loadTrainingData(trainParams.rootOut_dir,...
                                                             testDir);
  case 'noLearn'
    data = grabCut_trained.learning.noLearn.loadTrainingData(trainParams.rootOut_dir,...
                                                             testDir);
  case 'noLearn_edge'
    data = grabCut_trained.learning.noLearn_edge.loadTrainingData(trainParams.rootOut_dir,...
                                                             testDir);
  case 'grabCut_edgeFlux_simpleSearch'
    data = grabCut_trained.learning.simpleSearch_edgeFlux.loadTrainingData(...
               trainParams.rootOut_dir,testDir);
  case 'hogLearnLocal'
    data = grabCut_trained.learning.hogLearnLocal.loadTrainingData(...
               trainParams.rootOut_dir,testDir);
  case 'hogLearnLocal_position'
    data = grabCut_trained.learning.hogLearnLocal_position.loadTrainingData(...
               trainParams.rootOut_dir,testDir);
  case 'hogLearnLocalScaled'
    data = grabCut_trained.learning.hogLearnLocalScaled.loadTrainingData(...
               trainParams.rootOut_dir,testDir);
  case 'hogLearnLocal2'
    data = grabCut_trained.learning.hogLearnLocal2.loadTrainingData(...
               trainParams.rootOut_dir,testDir);
  case 'hogLearnGlobal'
    data = grabCut_trained.learning.hogLearnGlobal.loadTrainingData(...
               trainParams.rootOut_dir,testDir);
  case 'grabCut_edge2_simpleSearch'
    data = grabCut_trained.learning.simpleSearch_edge2.loadTrainingData(...
               trainParams.rootOut_dir,testDir);
  case 'grabCut_edge_simpleSearch'
    data = grabCut_trained.learning.simpleSearch_edge.loadTrainingData(...
               trainParams.rootOut_dir,testDir);
  case 'grabCut_simpleSearch_hogLearn'
    data = grabCut_trained.learning.simpleSearch_hogLearn.loadTrainingData(...
        trainParams.rootOut_dir,testDir);
  case 'localGrabCut2_simpleSearch_hogLearn'
    data = grabCut_trained.learning.simpleSearchLocalGC2_hogLearn.loadTrainingData(...
        trainParams.rootOut_dir,testDir);
  case 'localGrabCut_simpleSearch_hogLearn'
    data = grabCut_trained.learning.simpleSearchLocalGC_hogLearn.loadTrainingData(...
        trainParams.rootOut_dir,testDir);
  case 'grabCut_simpleSearch'
    data = grabCut_trained.learning.simpleSearch.loadTrainingData(trainParams.rootOut_dir,...
                                                             testDir);
  otherwise
    error('Invalid training method: %s\n',trainParams.opts.trainMethod);
end

function testFn = getTestFunction(trainMethod)

switch(trainMethod)
  case 'trivialSeg'
    testFn = @grabCut_trained.learning.trivialSeg.testOnImg;
  case 'noLearn'
    testFn = @grabCut_trained.learning.noLearn.testOnImg;
  case 'noLearn_edge'
    testFn = @grabCut_trained.learning.noLearn_edge.testOnImg;
  case 'grabCut_simpleSearch_hogLearn'
    testFn = @grabCut_trained.learning.simpleSearch_hogLearn.testOnImg;
  case 'localGrabCut2_simpleSearch_hogLearn'
    testFn = @grabCut_trained.learning.simpleSearchLocalGC2_hogLearn.testOnImg;
  case 'localGrabCut_simpleSearch_hogLearn'
    testFn = @grabCut_trained.learning.simpleSearchLocalGC_hogLearn.testOnImg;
  case 'grabCut_simpleSearch'
    testFn = @grabCut_trained.learning.simpleSearch.testOnImg;
  case 'grabCut_edge_simpleSearch'
    testFn = @grabCut_trained.learning.simpleSearch_edge.testOnImg;
  case 'grabCut_edge2_simpleSearch'
    testFn = @grabCut_trained.learning.simpleSearch_edge2.testOnImg;
  case 'grabCut_edgeFlux_simpleSearch'
    testFn = @grabCut_trained.learning.simpleSearch_edgeFlux.testOnImg;
  case 'hogLearnGlobal'
    testFn = @grabCut_trained.learning.hogLearnGlobal.testOnImg;
  case 'hogLearnLocal'
    testFn = @grabCut_trained.learning.hogLearnLocal.testOnImg;
  case 'hogLearnLocal_position'
    testFn = @grabCut_trained.learning.hogLearnLocal_position.testOnImg;
  case 'hogLearnLocalScaled'
    testFn = @grabCut_trained.learning.hogLearnLocalScaled.testOnImg;
  case 'hogLearnLocal2'
    testFn = @grabCut_trained.learning.hogLearnLocal2.testOnImg;
end

function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);

function saveTrainingParams_file(params,trainingParams);

fH=fopen([params.rootOut_dir 'trainParams.txt'],'w');
miscFns.printStructure(trainingParams,fH);
fclose(fH);

%opts=params.opts;
%if(opts.visualize & opts.visOpts.debugVisualize)
  %debugOpts.debugLevel=opts.debugLevel;
%else
  %debugOpts.debugLevel=0;
%end
%debugOpts.debugDir='';
%debugOpts.debugPrefix='';
%
%fH=fopen([params.rootOut_dir 'opts_humanModel.txt'],'w');
%[hModel,hModel_opts]=h3dCut.createHumanModel(opts,debugOpts);
%miscFns.printClass(hModel_opts,fH);
%delete(hModel);
%fclose(fH);
%
%fH=fopen([params.rootOut_dir 'opts_segModel.txt'],'w');
%[segH,segH_opts]=h3dCut.createSegObject(opts,debugOpts);
%miscFns.printClass(segH_opts,fH);
%delete(segH);
%fclose(fH);

