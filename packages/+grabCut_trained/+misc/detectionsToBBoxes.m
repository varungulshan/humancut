function detectionsToBBoxes()

% Function to take detections from either an upperbody or
% pedro's detector, and convert them into bounding box annotations

cwd=miscFns.extractDirPath(mfilename('fullpath'));
%params.trainList=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_1_train_small.txt'];
params.trainList=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_3_train_1.txt'];
%params.allList=params.trainList;
%params.allList=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_3_trainAndTest_1.txt'];
params.allList=[cwd '../../../data/grabCutLists/kinectData_10Feb2011_3_newTests.txt'];
params.detectionDir='/data2/varun/iccv2011/pp/pedroDetections/';
%params.detectionDir='/data2/varun/iccv2011/pp/upperBodyDetections_manuel/';
params.gtLabelDir = ['/data/adam2/varun/iccv2011/data/boxes_kinectData/10Feb2011_1/'];
params.outLabelDir = ['/data/adam2/varun/iccv2011/junk/10Feb2011_3_pedroBoxes1/'];
%params.outLabelDir = ['/data/adam2/varun/iccv2011/pp/10Feb2011_3_manuelBoxes1/'];
params.boxSelectMethod = 'highestConf';
params.boxRepositionMethod = 'trainSetMedian';

params.boxParams.borderX = (sqrt(2)-1)/2;
params.boxParams.borderY = (sqrt(2)-1)/2;

if(exist(params.outLabelDir,'dir')),
  msg=sprintf('Do you want to overwrite labelDir: \n%s?(y/n)[n]: ',params.outLabelDir);
  inp=input(msg,'s');
  if(~strcmp(inp,'y'))
    return;
  end;
else
  mkdir(params.outLabelDir);
end

saveParams_file(params);
trainDetections = loadDetections(params.trainList,params.detectionDir,...
                  params.boxSelectMethod);
trainGTBoxes = loadGTBoxes(params.trainList,params.gtLabelDir);
trainInfo = trainBoxReposition(trainDetections,trainGTBoxes,...
    params.boxRepositionMethod);

convertDetectionsToBoxes(params,trainInfo);

function convertDetectionsToBoxes(params,trainInfo)

images = textread(params.allList,'%s');
detections = loadDetections(params.allList,params.detectionDir,...
                  params.boxSelectMethod);
labelDir = params.gtLabelDir;
cmapLabels = jet(7);
for i=1:numel(images)
  fprintf('Converting detection for: %s\n',images{i});
  objectId = grabCut_trained.dataset.extractObjectIdentifier(images{i});
  labelImg_orig = imread([labelDir objectId '.png']);
  labelImg_detected = detectionToLabels(detections{i},...
      params.boxRepositionMethod,trainInfo,size(labelImg_orig),...
      params.boxParams);
  imwrite(labelImg_detected,cmapLabels,[params.outLabelDir objectId '.png']);
end

function labelImg=detectionToLabels(detection,repositionMethod,info,...
    imgSize,boxParams)

labelImg = 6*ones(imgSize,'uint8');
if(numel(detection)==0), return; end;

bbox = detectionToBox(detection,info,repositionMethod);

xMin = bbox(1); xMax = bbox(3);
yMin = bbox(2); yMax = bbox(4);

w = xMax-xMin+1;
h = yMax-yMin+1;

innerX_min = xMin; 
innerX_max = xMax; 
innerY_min = yMin; 
innerY_max = yMax; 

boxParams.borderX = round(boxParams.borderX*(innerX_max-innerX_min+1));
boxParams.borderY = round(boxParams.borderY*(innerY_max-innerY_min+1));

outerX_min = innerX_min - boxParams.borderX;
outerX_max = innerX_max + boxParams.borderX;
outerY_min = innerY_min - boxParams.borderY;
outerY_max = innerY_max + boxParams.borderY;

imgH = imgSize(1);
imgW = imgSize(2);

innerX_min = max(1,innerX_min);innerX_max=min(imgW,innerX_max);
innerY_min = max(1,innerY_min);innerY_max=min(imgH,innerY_max);

outerX_min = max(1,outerX_min);outerX_max=min(imgW,outerX_max);
outerY_min = max(1,outerY_min);outerY_max=min(imgH,outerY_max);

labelImg(outerY_min:outerY_max,outerX_min:outerX_max)=2;
labelImg(innerY_min:innerY_max,innerX_min:innerX_max)=3;

function bbox = detectionToBox(detection,info,repositionMethod)

switch(repositionMethod)
  case 'trainSetMedian'
    midPointDetX = mean([detection(1) detection(3)]);
    midPointDetY = mean([detection(2) detection(4)]);
    detW = detection(3) - detection(1);
    detH = detection(4) - detection(2);

    midPointDetX = midPointDetX + info.deltaX;
    midPointDetY = midPointDetY + info.deltaY;

    detW = detW * info.scaleW;
    detH = detH * info.scaleH;
  
    bbox = round([midPointDetX-detW/2 midPointDetY-detH/2 ...
                  midPointDetX+detW/2 midPointDetY+detH/2]);
  otherwise
    error('Invalid reposition method: %s\n',repositionMethod);
end

function trainInfo = trainBoxReposition(detections,gtBoxes,...
    repositionMethod)

switch(repositionMethod)
  case 'trainSetMedian'
    trainInfo = trainMedianReposition(detections,gtBoxes);
  otherwise
    error('Unsupported reposition method: %s\n',repositionMethod);
end

function info = trainMedianReposition(detections,gtBoxes)

nBoxes = numel(detections);
valid = false(nBoxes,1);
deltaX = zeros(nBoxes,1);
deltaY = zeros(nBoxes,1);
scaleH = zeros(nBoxes,1);
scaleW = zeros(nBoxes,1);

for i=1:nBoxes
  tmpDet = detections{i};
  if(numel(tmpDet)==0), continue; end;
  tmpBox = gtBoxes{i};
  valid(i) = true;
  midPointDetX = mean([tmpDet(1) tmpDet(3)]);
  midPointDetY = mean([tmpDet(2) tmpDet(4)]);
  detW = tmpDet(3) - tmpDet(1);
  detH = tmpDet(4) - tmpDet(2);
  midPointGtX = mean([tmpBox(1) tmpBox(3)]);
  midPointGtY = mean([tmpBox(2) tmpBox(4)]);
  gtW = tmpBox(3) - tmpBox(1);
  gtH = tmpBox(4) - tmpBox(2);

  deltaX(i) = midPointGtX - midPointDetX;
  deltaY(i) = midPointGtY - midPointDetY;
  scaleH(i) = gtH/detH;
  scaleW(i) = gtW/detW;
end

deltaX = deltaX(valid);
deltaY = deltaY(valid);
scaleH = scaleH(valid);
scaleW = scaleW(valid);

info.deltaX = median(deltaX);
info.deltaY = median(deltaY);
info.scaleH = median(scaleH);
info.scaleW = median(scaleW);

function gtBoxes = loadGTBoxes(imgList, labelDir)

images = textread(imgList,'%s');
gtBoxes = cell(numel(images),1);
for i=1:numel(images)
  fprintf('Finding gtBox for: %s\n',images{i});
  objectId = grabCut_trained.dataset.extractObjectIdentifier(images{i});
  labelImg = imread([labelDir objectId '.png']);
  gtBoxes{i} = grabCut_trained.misc.getBoxFromLabels(labelImg);
end

function detections = loadDetections(imgList,detectionDir,...
    detectionSelectMethod)

images = textread(imgList,'%s');
detections = cell(numel(images),1);
for i=1:numel(images)
  fprintf('Finding detection for: %s\n',images{i});
  objectId = grabCut_trained.dataset.extractObjectIdentifier(images{i});
  detFile = [detectionDir objectId '.mat'];
  detections{i} = loadDetectionSingle(detFile,detectionSelectMethod);
end

function detection = loadDetectionSingle(detFile,detectionSelectMethod)

tmpLoad = load(detFile);
bboxes = tmpLoad.bbox;

if(size(bboxes,1)==0)
  detection = [];
  return;
end

switch(detectionSelectMethod)
  case 'highestConf'
    [maxConf,maxId] = max(bboxes(:,5));
    detection = bboxes(maxId,:);
  otherwise
    error('Unsupported detection selection method %s\n',...
        detectionSelectMethod);
end

function saveParams_file(params)

fH=fopen([params.outLabelDir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);
