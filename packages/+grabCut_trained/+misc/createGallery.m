function createGallery()
% Function to create qualitative results gallery

cwd=miscFns.extractDirPath(mfilename('fullpath'));
outDir='/home/varun/windows/vgg/bmvc2011_paper/images/pixelisedGallery/';
%outDir='/home/varun/windows/vgg/bmvc2011_paper/images/externalData_gallery/';
latexDirPrefix = 'images/pixelisedGallery/';
%latexDirPrefix = 'images/externalData_gallery/';
imgList=[cwd '../../../data/grabCutLists/tmp.txt'];
%imgList=[cwd '../../../data/grabCutLists/externalImages.txt'];
%imgDir='/home/varun/windows/rugbyShared/work/videoCut2/git/data/bmvc_outsideDataset_images/';
%labelDir='/home/varun/windows/rugbyShared/work/videoCut2/git/data/bmvc_outsideDataset_labelsSaved/';
imgDir=['/data2/varun/iccv2011/data/kinectData/10Feb2011/'];
labelDir=['/data2/varun/iccv2011/data/boxes_kinectData/10Feb2011_1/'];
gtDir=['/data/adam2/varun/iccv2011/data/gt_kinectData/10Feb2011_cleanUp1/'];
pixeliseFaces=false;
bdryWidth = 5;
drawBox = true;
showScore = false;
concatenate = false;
cropToBox = true; % sometimes for hog prediction, seg goes slightly outside
% box due to hog cell quantization, fix it for visualization only
latexW = 0.3;
latexH = 0.2;

dirs = cell(0,1);
%dirs{end+1} = '/data1/varun/iccv2011/results/test/run132/';
%dirs{end+1} = '/data2/varun/iccv2011/results/test/run130/';
%dirs{end+1} = '/data1/varun/iccv2011/results/test/run131/';
dirs{end+1} = [cwd '../../../results/grabCut_trained/externalData/spSVM-pos/'];
dirs{end+1} = [cwd '../../../results/grabCut_trained/externalData/spSVM-pos+localGC/'];
dirs{end+1} = [cwd '../../../results/grabCut_trained/externalData/gc/'];

methodNames = cell(0,1);
methodNames{end+1} = 'spSvm-pos';
methodNames{end+1} = 'spSvm-pos+localGC';
methodNames{end+1} = 'gc';

sortByIdx=2; % performance of spSvm-pos+localGC

assert(numel(methodNames)==numel(dirs),'Invalid methodnames\n');
if(~exist(outDir,'dir')), mkdir(outDir); end;
images=textread(imgList,'%s');

if(concatenate)
  fH=fopen([outDir 'galleryLatex.tex'],'a');
else
  fH=fopen([outDir 'galleryLatex.tex'],'w');
end
perf = zeros(1,numel(images));

for i=1:numel(images)
  fprintf('Saving: %s\n',images{i});
  fullImgName = images{i};
  img = imread([imgDir fullImgName]);
  img = im2double(img);

  if(size(img,1)>size(img,2)),
    orientation='vertical';
    sizeString=sprintf('[height=%.2f\\textheight]',latexH);
  else
    sizeString=sprintf('[width=%.2f\\textwidth]',latexW);
    orientation='horizontal';
  end
  
  imgName = grabCut_trained.dataset.extractImgName(fullImgName); 
  objectId = grabCut_trained.dataset.extractObjectIdentifier(fullImgName);
  if(pixeliseFaces)
    img = grabCut_trained.misc.pixeliseBBox(img);
  end
  for j = 1:numel(dirs)
    segFile = [dirs{j} 'segs/' objectId '.png'];
    seg = imread(segFile);
    if(cropToBox)
      labelImg = imread([labelDir objectId '.png']);
      seg(labelImg~=3) = 0;
    end
    overlay = img;
    if(drawBox)
      labelImg = imread([labelDir objectId '.png']);
      labelImg(labelImg==2)=6;
      overlay = grabCut_trained.helpers.overlayBox(overlay,labelImg);
    end
    overlay = grabCut_trained.helpers.overlaySeg_single(overlay,seg,...
        bdryWidth);
    if(showScore)
      gt = imread([gtDir objectId '.png']);
      [overlay,score] = printScore(seg,gt,overlay);
      if(j==sortByIdx), perf(i)=score; end
    end
    imgName = [methodNames{j} '_' objectId];
    imwrite(overlay,[outDir imgName '.png']);
    %imwrite(overlay,[outDir methodNames{j} '_' objectId '.jpg'],...
        %'Quality',100);
    imgName = replaceDots(imgName);
    if(mod(j,numel(dirs))~=0)
      fprintf(fH,'\\includegraphics%s{%s} & \n',...
          sizeString,[latexDirPrefix imgName]);
    else
      fprintf(fH,'\\includegraphics%s{%s} \\tabularnewline \n',...
          sizeString,[latexDirPrefix imgName]);
    end
  end
end

[perf,sortIds]=sort(perf,'descend');
images = images(sortIds);

fprintf('Images in sorted performance order:\n');
for i=1:numel(images)
  fprintf('%s\n',images{i});
end

fclose(fH);

grabCut_trained.misc.convertJPG(outDir);

function s = replaceDots(s)

s = strrep(s,'.','\lyxdot ');

function [img,score] = printScore(seg,gt,img)

score = testBench.evaluateSeg(seg,gt,'accuracyOverlap');
img = miscFns.mex_drawTextBig(im2uint8(img),[5 40],sprintf('%.2f',score),...
      [255 255 0]);
