function img = pixeliseBBox(img)

imshow(img);
bbox = getBBox(img); 
img = do_pixeliseBBox(img,bbox,10);

function img = do_pixeliseBBox(img,bbox,pixelSize)

cropImg = img(bbox(2):bbox(4),bbox(1):bbox(3),:);
[origH origW nCh] = size(cropImg);
cropImg = imresize(imresize(cropImg,1/pixelSize),[origH origW],'nearest');
img(bbox(2):bbox(4),bbox(1):bbox(3),:) = cropImg;

function bbox = getBBox(img)

k = waitforbuttonpress;
point1 = get(gca,'CurrentPoint');    % button down detected
finalRect = rbbox;                   % return figure units
point2 = get(gca,'CurrentPoint');    % button up detected
point1 = point1(1,1:2);              % extract x and y
point2 = point2(1,1:2);
p1 = round(min(point1,point2));             
p2 = round(max(point1,point2));

bbox = [p1(1) p1(2) p2(1) p2(2)];
