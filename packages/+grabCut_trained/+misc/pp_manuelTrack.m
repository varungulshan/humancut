function pp_manuelTrack(params)

% Runs detector trained by manuel on a video

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('params','var'))
  params.rootOut_dir=[cwd '../../../junk/manuelBody_tracks/'];
  params.modelFile=[cwd '../../../data/manuelTrained/ub-gen-M1_final.mat'];
  params.vocDir='/home/varun/windows/rugbyShared/work/software/voc-release3.1/';
  params.testBench_cmd='testBench.getTests_grabCut_kinect10Feb2011_1_train_1()';
  params.overWrite=true;
  
  testBenchOpts=eval(params.testBench_cmd);
else
  testBenchOpts=eval(params.testBench_cmd);
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',params.rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
end

images = textread(testBenchOpts.imgList,'%s');
imgDir = testBenchOpts.imgDir;

addpath(params.vocDir);
runOnImages(imgDir,images,params.rootOut_dir,params.modelFile,...
    cwd);
rmpath(params.vocDir);

function runOnImages(imgDir,images,rootOutDir,modelFile,cwd)

tmpLoad = load(modelFile); % provides the variable model
vocModel = tmpLoad.model;
outDir=rootOutDir;

parfor i=1:length(images)
  imgName = grabCut_trained.dataset.extractImgName(images{i});
  img = imread([imgDir imgName]);
  objectId = grabCut_trained.dataset.extractObjectIdentifier(images{i});
  fprintf('Running detections on: %s\n',objectId);
  bboxes = saveDetections(img,vocModel,objectId,outDir);
  img = drawBoxes(img,bboxes);
  imwrite(img,[outDir objectId '_detections.jpg']);
end

function img = drawBoxes(img,bboxes)

boxColor = uint8(255*[1 0 0]); % FG box
%bboxes(:,3) = bboxes(:,1)+bboxes(:,3);
%bboxes(:,4) = bboxes(:,2)+bboxes(:,4);

for i=1:size(bboxes,1)
  img = drawBox(img,boxColor,2,bboxes(i,1),bboxes(i,3), ...
      bboxes(i,2),bboxes(i,4));
  idx = sub2ind([size(img,1) size(img,2)],bboxes(i,2),bboxes(i,1));
  img = miscFns.mex_drawText(img,int32(idx),sprintf('%.2f',bboxes(i,5)));
end

function img=drawBox(img,winClr,penRad,xMin,xMax,yMin,yMax)

[h w nCh]=size(img);

% Clockwise: left bar, top bar, right bar, bottom bar
xL=[xMin-penRad xMin-penRad xMax-penRad xMin-penRad];
xR=[xMin+penRad xMax+penRad xMax+penRad xMax+penRad];
yT=[yMin-penRad yMin-penRad yMin-penRad yMax-penRad];
yB=[yMax+penRad yMin+penRad yMax+penRad yMax+penRad];

xL=max(1,xL);
xR=min(w,xR);
yT=max(1,yT);
yB=min(h,yB);

for j=1:4
  x_left=xL(j);
  x_right=xR(j);
  y_top=yT(j);
  y_bottom=yB(j);

  for i=1:3
    img(y_top:y_bottom,x_left:x_right,i)=winClr(i);
  end
end
function bbox=saveDetections(curFrame,model,objectId,outDir)

bbox=process(curFrame,model,0);
if(length(bbox)==0), bbox=zeros(0,5); end
bbox(:,1:4)=round(bbox(:,1:4));
save([outDir objectId '.mat'],'bbox');
if 0
  bbox(:,3)=bbox(:,3)-bbox(:,1);
  bbox(:,4)=bbox(:,4)-bbox(:,2);
  scales=sqrt(bbox(:,3).*bbox(:,4)/(128*128));
  for i=1:size(bbox,1)
    fprintf(fH_detections,'%s %d %d %d %d %.6f %.6f\n',objectId,bbox(i,1),bbox(i,2)...
            ,bbox(i,3),bbox(i,4),scales(i),bbox(i,5));
  end
end

function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);
