function pixeliseFaces(params)
% pixelises faces in a image list

cwd=miscFns.extractDirPath(mfilename('fullpath'));
%outDir='/home/varun/windows/vgg/bmvc2011_paper/images/pixelisedFaces/';
outDir='/home/varun/windows/vgg/bmvc2011_paper/images/automationImages_pixelised/';
inDir='/home/varun/windows/vgg/bmvc2011_paper/images/automationImages/';
listFile=[inDir 'tmpPixeliseList.txt'];

images=textread(listFile,'%s');

if(~exist(outDir,'dir')), mkdir(outDir); end;

figure;
for i=1:numel(images)
  fprintf('Pixelising: %s\n',images{i});
  fullImgName = images{i};
  img = imread([inDir fullImgName]);
  imgName = miscFns.extractImgName(fullImgName); 
  imshow(img);
  img = grabCut_trained.misc.pixeliseBBox(img);
  imwrite(img,[outDir imgName '.png']);
end
