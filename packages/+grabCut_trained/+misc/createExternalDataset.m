function createExternalDataset()
% Function to create qualitative results gallery

cwd=miscFns.extractDirPath(mfilename('fullpath'));
%imgDir='/home/varun/windows/rugbyShared/work/software/h3d_version1.0/data/people/images/';
imgDir='/data/adam3/pascalData/VOCdevkit_2010/VOC2010/JPEGImages/';
inLabelDir = '/home/varun/windows/rugbyShared/work/videoCut2/git/data/bmvc_outsideDataset_labels/';
imgList=[cwd '../../../data/grabCutLists/tmp.txt'];
outLabelDir = '/home/varun/windows/rugbyShared/work/videoCut2/git/data/bmvc_outsideDataset_labelsSaved/';
outImgDir = '/home/varun/windows/rugbyShared/work/videoCut2/git/data/bmvc_outsideDataset_images/';
maxDim = 640;

images = textread(imgList,'%s');

if(~exist(outLabelDir,'dir')), mkdir(outLabelDir); end;

for i=1:numel(images)
  imgName = images{i};
  img = imread([imgDir imgName]);
  rescale = maxDim/max(size(img,2),size(img,1));
  img = imresize(img,rescale);
  imwrite(img,[outImgDir imgName]);
  copyfile([inLabelDir imgName '.png'],[outLabelDir imgName '.png']);
end
