function convertJPG(imgDir)
% Function to create qualitative results gallery

cwd=miscFns.extractDirPath(mfilename('fullpath'));
defImgDir='/home/varun/windows/vgg/bmvc2011_paper/images/dataset_pixelised/';

if(~exist('imgDir','var')), imgDir=defImgDir; end;

%images = dir([imgDir '*.png']);
images = dir([imgDir '*.jpg']);

for i=1:numel(images)
  imgName = images(i).name;
  stem = miscFns.extractImgName(imgName);
  cmd = ['convert -quality 70 ' imgDir imgName ' ' imgDir stem '.jpg'];
  %cmd = ['convert -quality 90 ' imgDir imgName ' ' imgDir stem '.jpg'];
  fprintf('Executing: %s\n',cmd);
  system(cmd);
  %delete([imgDir imgName]);
end
