function createGallery()
% Function to create qualitative results gallery

cwd=miscFns.extractDirPath(mfilename('fullpath'));
outDir='/home/varun/windows/vgg/bmvc2011_paper/images/trainData_gallery/';
latexDirPrefix = 'images/trainData_gallery/';
imgList=[cwd '../../../data/grabCutLists/tmp.txt'];

imgDir=['/data2/varun/iccv2011/data/kinectData/10Feb2011/'];
labelDir=['/data2/varun/iccv2011/data/boxes_kinectData/10Feb2011_1/'];
gtDir=['/data/adam2/varun/iccv2011/data/gt_kinectData/10Feb2011_cleanUp1/'];

pixeliseFaces=true;
bdryWidth = 5;
drawBox = false;

latexW = 0.3;
latexH = 0.2;

if(~exist(outDir,'dir')), mkdir(outDir); end;
images=textread(imgList,'%s');

fH=fopen([outDir 'galleryLatex.tex'],'w');

for i=1:numel(images)
  fprintf('Saving: %s\n',images{i});
  fullImgName = images{i};
  img = imread([imgDir fullImgName]);
  img = im2double(img);

  if(size(img,1)>size(img,2)),
    orientation='vertical';
    sizeString=sprintf('[height=%.2f\\textheight]',latexH);
  else
    sizeString=sprintf('[width=%.2f\\textwidth]',latexW);
    orientation='horizontal';
  end
  
  %imgName = grabCut_trained.dataset.extractImgName(fullImgName); 
  objectId = grabCut_trained.dataset.extractObjectIdentifier(fullImgName);
  if(pixeliseFaces)
    img = grabCut_trained.misc.pixeliseBBox(img);
  end

  gt = imread([gtDir objectId '.png']);
  gt(gt==128)=0;
  overlay = grabCut_trained.helpers.overlaySeg_single(img,gt,...
        bdryWidth);
  imwrite(overlay,[outDir objectId '.png']);
  imgName = replaceDots(objectId);
  if(mod(i,3)~=0)
    fprintf(fH,'\\includegraphics%s{%s} & \n',...
            sizeString,[latexDirPrefix imgName]);
  else
    fprintf(fH,'\\includegraphics%s{%s} \\tabularnewline \n',...
        sizeString,[latexDirPrefix imgName]);
  end
end

fclose(fH);

grabCut_trained.misc.convertJPG(outDir);

function s = replaceDots(s)

s = strrep(s,'.','\lyxdot ');

function [img,score] = printScore(seg,gt,img)

score = testBench.evaluateSeg(seg,gt,'accuracyOverlap');
img = miscFns.mex_drawTextBig(im2uint8(img),[5 40],sprintf('%.2f',score),...
      [255 255 0]);
