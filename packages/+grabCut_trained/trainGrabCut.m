function trainGrabCut(params)
% Learns a model for the head given training data

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('params','var'))
  %params.rootOut_dir=[cwd '../../results/grabCut_trained/train/hogLearning/'];
  params.rootOut_dir=[cwd '../../results/grabCut_trained/train/gcWithHogLearning/'];
  %params.rootOut_dir=[cwd '../../results/grabCut_trained/train/hogLearning_kmeans/'];
  %params.rootOut_dir=[cwd '../../results/grabCut_trained/testDebug/'];
  %params.opts.gtDir=[cwd '../../data/h3d_gtSeg2/'];
  params.opts.evalOpts_string='kinect_10Feb2011';
  %params.opts.evalOpts_string='try1';
  
  %params.opts.trainMethod='grabCut_edge2_simpleSearch';
  %params.opts.trainMethod_optsString='debug_edgeLearn_4';
  %params.opts.trainMethod='hogLearnLocal2';
  %params.opts.trainMethod='hogLearnLocal';
  params.opts.trainMethod='hogLearnLocal_position';
  %params.opts.trainMethod='grabCut_simpleSearch_hogLearn';
  %params.opts.trainMethod='grabCut_simpleSearch';
  %params.opts.trainMethod_optsString='tryDictionary1';
  %params.opts.trainMethod_optsString='kmeansDictionary_saved_svm1_bias';
  %params.opts.trainMethod_optsString='svm1_cellSize8_localSize5';
  %params.opts.trainMethod_optsString='hogLearnLocal_position_103';
  params.opts.trainMethod_optsString='svm1_4levels_validateC2_allDict_pegasus';

  params.opts.visualize=true;
  params.opts.visOpts.debugVisualize=true;
  params.opts.debugLevel=10;
  params.opts.visOpts.imgExt='jpg';
  params.opts.visOpts.segBdryWidth=2;
  params.overWrite=true;

  %params.testBench_cmd='testBench.getTests_h3d_headTrain()';
  %params.testBench_cmd='testBench.getTests_grabCut_small()';
  %params.testBench_cmd='testBench.getTests_grabCut_h3d_train_1()';
  params.testBench_cmd='testBench.getTests_grabCut_kinect10Feb2011_1_train_small()';
  %params.testBench_cmd='testBench.getTests_grabCut_kinect10Feb2011_1_train_1()';
  %params.testBench_cmd='testBench.getTests_grabCut_kinect10Feb2011_3_train_1()';
  %params.testBench_cmd='testBench.getTests_grabCut_tmp()';
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',params.rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
end

testBenchOpts=eval(params.testBench_cmd);
save([params.rootOut_dir 'testBenchOpts.mat'],'testBenchOpts');
save([params.rootOut_dir 'params.mat'],'params');
 
trainModel(testBenchOpts,params.opts,params.rootOut_dir);

function trainModel(testBenchOpts,opts,trainOutDir)
switch(opts.trainMethod)
  case 'hogLearnGlobal'
    grabCut_trained.learning.hogLearnGlobal.trainHogRegression(opts,...
        trainOutDir,testBenchOpts);
  case 'hogLearnLocal'
    grabCut_trained.learning.hogLearnLocal.trainHogPredictor(opts,...
        trainOutDir,testBenchOpts);
  case 'dictionaryLearn'
    grabCut_trained.learning.dictionaryLearn.trainHogPredictor(opts,...
        trainOutDir,testBenchOpts);
  case 'hogLearnLocal_position'
    grabCut_trained.learning.hogLearnLocal_position.trainHogPredictor(opts,...
        trainOutDir,testBenchOpts);
  case 'hogLearnLocalScaled'
    grabCut_trained.learning.hogLearnLocalScaled.trainHogPredictor(opts,...
        trainOutDir,testBenchOpts);
  case 'hogLearnLocal2'
    grabCut_trained.learning.hogLearnLocal2.trainHogPredictor(opts,...
        trainOutDir,testBenchOpts);
  case 'trivialSeg'
    grabCut_trained.learning.trivialSeg.trainTrivial(opts,trainOutDir,testBenchOpts);
  case 'noLearn'
    grabCut_trained.learning.noLearn.trainNoLearn(opts,trainOutDir,testBenchOpts);
  case 'grabCut_simpleSearch_hogLearn'
    grabCut_trained.learning.simpleSearch_hogLearn.trainLineSearch(...
        opts,trainOutDir,testBenchOpts);
  case 'localGrabCut_simpleSearch_hogLearn'
    grabCut_trained.learning.simpleSearchLocalGC_hogLearn.trainLineSearch(...
        opts,trainOutDir,testBenchOpts);
  case 'localGrabCut2_simpleSearch_hogLearn'
    grabCut_trained.learning.simpleSearchLocalGC2_hogLearn.trainLineSearch(...
        opts,trainOutDir,testBenchOpts);
  case 'grabCut_simpleSearch'
    grabCut_trained.learning.simpleSearch.trainLineSearch(opts,trainOutDir,testBenchOpts);
  case 'grabCut_edge_simpleSearch'
    grabCut_trained.learning.simpleSearch_edge.trainLineSearch(opts,trainOutDir,testBenchOpts);
  case 'grabCut_edgeFlux_simpleSearch'
    grabCut_trained.learning.simpleSearch_edgeFlux.trainLineSearch...
        (opts,trainOutDir,testBenchOpts);
  case 'grabCut_edge2_simpleSearch'
    grabCut_trained.learning.simpleSearch_edge2.trainLineSearch(opts,trainOutDir,testBenchOpts);
  case 'noLearn_edge'
    grabCut_trained.learning.noLearn_edge.trainNoLearn(opts,trainOutDir,testBenchOpts);
  otherwise
    error('Invalid training method: %s\n',opts.trainMethod);
end

function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);
