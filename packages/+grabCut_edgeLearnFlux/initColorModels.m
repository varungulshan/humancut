function [gmmf,gmmb]=initColorModels(features,labelImg,gmmNmix_fg,gmmNmix_bg)

labelImg(labelImg==3)=1; % Soft labels are used for initializing color models also
labelImg(labelImg==4)=2;

tmpFeatures=features(:,labelImg(:)==1);
gmmf=gmm.init_gmmBS(tmpFeatures,gmmNmix_fg);

tmpFeatures=features(:,labelImg(:)==2);
gmmb=gmm.init_gmmBS(tmpFeatures,gmmNmix_bg);
