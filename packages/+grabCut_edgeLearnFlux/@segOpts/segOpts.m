% Copyright (C) 2010 Varun Gulshan
% This class defines the options for grabCut_edgeLearnFlux segmentation
classdef segOpts
  properties (SetAccess=public, GetAccess=public)
    fluxStrength
    gcGamma  % gamma of graph cuts 
    gcScale  % scaling to convert to integers
    gcNbrType % 'nbr4' or 'nbr8'

    gmmNmix_fg % number of gmm mixtures for fg, usually 5
    gmmNmix_bg % number of gmm mixtures for bg, usually 5
    gmmUpdate_iters % number of iterations to update GMM in the EM algorithm

    gmmNmix_yesEdge % number of gmm mixtures for fg, usually 5
    gmmNmix_noEdge % number of gmm mixtures for bg, usually 5

    postProcess % 0 = off, 1 = on
    numIters % Number of iterations to run grabCut_edgeLearnFlux for
    cannyThresh % 'auto' or a number

    enableEdgePosterior
    kovesiSigma
    kovesiAutoThresh % threshold for setting auto threshold, declares the number
    % of pixels that are not edges (0.7 is matlabs setting)

    edgeFeatureExtend % number of pixels on either side of the edge to average
    fluxBlurSigma
  end

  methods
    function obj=segOpts()
      % Set the default options
      obj.gcGamma=140;
      obj.fluxStrength=5;
      obj.gcScale=50;
      obj.gcNbrType='nbr8';

      obj.gmmNmix_fg=5;
      obj.gmmNmix_bg=5;
      obj.postProcess=0;
      obj.numIters=10;
      
      obj.gmmUpdate_iters=1;
      obj.cannyThresh = 'auto';
      obj.enableEdgePosterior = true;
      
      obj.kovesiSigma = 1;
      obj.kovesiAutoThresh = 0.7;
      obj.edgeFeatureExtend = 4;
      obj.fluxBlurSigma = 2;
    end
  end

end % of classdef
