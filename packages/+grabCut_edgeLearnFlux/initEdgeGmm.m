function [gmmNo,gmm10,gmm01]=initEdgeGmm(features,edgeLabels,gmmOpts)

assert(strcmp(class(edgeLabels),'int32'),'EdgeLabels should be of class int32\n');

tmpFeatures=features(:,edgeLabels==0);
gmmNo=gmm.init_gmmBS(tmpFeatures,gmmOpts.nMix_noEdge);

tmpFeatures=features(:,edgeLabels==1);
gmm10=gmm.init_gmmBS(tmpFeatures,gmmOpts.nMix_yesEdge);

tmpFeatures=features(:,edgeLabels==2);
gmm01=gmm.init_gmmBS(tmpFeatures,gmmOpts.nMix_yesEdge);
