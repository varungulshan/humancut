function updateEdgeModel(obj)

opts = obj.opts;
if(opts.enableEdgePosterior)
  seg = obj.seg;
  
  edgeLabels = zeros(size(obj.lEdgesLearn),'int32');
  edges10Mask = seg(obj.lEdgesLearn)==255 & seg(obj.rEdgesLearn)==0;
  edges01Mask = seg(obj.lEdgesLearn)==0 & seg(obj.rEdgesLearn)==255;
  edgeLabels(edges10Mask) = 1;
  edgeLabels(edges01Mask) = 2;

  gmmOpts.nMix_yesEdge = opts.gmmNmix_yesEdge;
  gmmOpts.nMix_noEdge = opts.gmmNmix_noEdge;
  [obj.gmmNoEdge,obj.gmm10Edge,obj.gmm01Edge] =...
      grabCut_edgeLearnFlux.initEdgeGmm(obj.edgeFeatures,...
      edgeLabels,gmmOpts);

end
