function preProcess(obj,img)
  if(~strcmp(obj.state,'init')),
    error('Cant call preProcess from %s state\n',obj.state);
    return;
  end

  if(~strcmp(class(img),'double')),
    error('img should be of type double\n');
  end

  [obj.cannyEdgeMap,obj.cannyOrient]=getCanny(img,obj.opts);
  miscFns.saveDebug(obj.debugOpts,obj.cannyEdgeMap,'canny.png');
  preProcess_GC(obj,img);
  obj.smoothFilterX = fspecial('gaussian',[1 round(4*obj.opts.fluxBlurSigma+1)],... 
      obj.opts.fluxBlurSigma);
  obj.smoothFilterY = fspecial('gaussian',[round(4*obj.opts.fluxBlurSigma+1) 1],... 
      obj.opts.fluxBlurSigma);
  obj.dyFilter = -fspecial('sobel'); % - sign is needed to correct the axis direction
  obj.dxFilter = obj.dyFilter';
  obj.features=grabCut_edgeLearnFlux.extractPixels(img);
  obj.img=img;
  obj.state='pped'; 


function [edgeMap,edgeOrient] = getCanny(img,opts)

[edgeMap,edgeOrient] = getKovesiEdgeMap(img,opts.cannyThresh, ...
                       opts.kovesiSigma,opts.kovesiAutoThresh);

function preProcess_GC(obj,img)

switch(obj.opts.gcNbrType)
  case 'nbr4'
    roffset = int32([ 1,  0 ]);
    coffset = int32([  0, 1 ]);
  case 'nbr8'
    roffset = int32([ 1, 1, 0,-1 ]);
    coffset = int32([ 0, 1, 1, 1 ]);
  otherwise
    error('Invalid nbrhood type %s\n',obj.opts.nbrHoodType);
end

obj.roffset=roffset;
obj.coffset=coffset;

orientIdxMap = binOrientations(obj.cannyOrient,roffset,coffset);
[obj.lEdgesLearn,obj.rEdgesLearn,obj.vectorFieldX,obj.vectorFieldY] = ...
    grabCut_edgeLearnFlux.cpp.mex_getLearningEdges(img,obj.cannyEdgeMap,...
                                               orientIdxMap,roffset',coffset');

edgeFtrIdx = [obj.lEdgesLearn;obj.rEdgesLearn];

% The sorting below is done in the mex file now
%edgeFtrIdx = sort(edgeFtrIdx,1,'ascend'); % To orient edges in one particular direction
%obj.lEdgesLearn = edgeFtrIdx(1,:);
%obj.rEdgesLearn = edgeFtrIdx(2,:);

[obj.lEdgesCanny,obj.rEdgesCanny] = ...
    grabCut_edgeLearnFlux.cpp.mex_setupGraph(img,obj.cannyEdgeMap,roffset',coffset');

function idxMap = binOrientations(orient,roffset,coffset)

thetas = -90+atan2(double(coffset),double(roffset))*180/pi; 
% Rotating all thetas by -90 because orients is actually orientation
% along the canny edge (and not perpendicular to the canny edge)
thetas(thetas<0)=thetas(thetas<0)+180; % all angles between [0,180]

thetaCopy = thetas;
thetaCopy(thetas>90) = thetaCopy(thetas>90)-180;
thetaCopy(thetas<=90) = thetaCopy(thetas<=90)+180;
thetaIdx = repmat(int32([1:numel(roffset)]),[1 2]);
thetas = [thetas thetaCopy]';

assert(all(orient(:)>=0 & orient(:)<=180));
deltas = abs(repmat(thetas,[1 numel(orient)])-repmat(orient(:)',[numel(thetas) 1]));
[deltas,idxs] = min(deltas,[],1);

idxMap = reshape(thetaIdx(idxs),[size(orient,1) size(orient,2)]);

function [edgeMap,orient] = getKovesiEdgeMap(img,thresh,sigma,autoThresh)

img=rgb2gray(img);
[derivativeMag,orient] = canny(img,sigma);

maxMag = max(derivativeMag(:));
derivativeMag = derivativeMag/maxMag;

upperThresh = 0;
if(isnumeric(thresh)), upperThresh = min(1,thresh);
elseif(strcmp(thresh,'auto')), upperThresh = getAutoThresh(derivativeMag,autoThresh);
else error('Invalid threshold for kovesi: %s\n',mat2str(thresh));
end

lowerThresh = 0.4 * upperThresh;
allEdges = nonmaxsup(derivativeMag,orient,1.35);
edgeMap = hysthresh(allEdges,upperThresh,lowerThresh);

function highThresh = getAutoThresh(mag,PercentOfPixelsNotEdges)

[m,n] = size(mag);
%PercentOfPixelsNotEdges = 0.7; % copied from matlab
counts=imhist(mag, 64);
highThresh = find(cumsum(counts) > PercentOfPixelsNotEdges*m*n,...
                  1,'first') / 64;
