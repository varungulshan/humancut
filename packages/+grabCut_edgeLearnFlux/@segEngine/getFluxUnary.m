function fluxUnary = getFluxUnary(obj,edgePosteriors)

vectorFieldX = obj.vectorFieldX;
vectorFieldY = obj.vectorFieldY;

diffPosterior = edgePosteriors(1,:) - edgePosteriors(2,:);
fieldMag = obj.opts.fluxStrength*diffPosterior;

vectorFieldX(obj.lEdgesLearn) = vectorFieldX(obj.lEdgesLearn).*fieldMag;
vectorFieldX(obj.rEdgesLearn) = vectorFieldX(obj.rEdgesLearn).*fieldMag;
vectorFieldY(obj.lEdgesLearn) = vectorFieldY(obj.lEdgesLearn).*fieldMag;
vectorFieldY(obj.rEdgesLearn) = vectorFieldY(obj.rEdgesLearn).*fieldMag;

vectorFieldX_dx = imfilter(imfilter(imfilter(vectorFieldX,obj.smoothFilterX),...
    obj.smoothFilterY),obj.dxFilter);
vectorFieldY_dy = imfilter(imfilter(imfilter(vectorFieldY,obj.smoothFilterX),...
    obj.smoothFilterY),obj.dyFilter);

fluxUnary = -(vectorFieldY_dy+vectorFieldX_dx);
