function initEdgeModel(obj)

opts = obj.opts;
if(opts.enableEdgePosterior)
  labelImg = obj.labelImg;

  % Remove canny edges that lie inside cropped out areas
  mask = (labelImg <=4);

  obj.vectorFieldX(~mask)=0;
  obj.vectorFieldY(~mask)=0;

  cannyEdgeMask = mask(obj.lEdgesLearn);
  cannyEdgeMask = cannyEdgeMask | mask(obj.rEdgesLearn);
  obj.lEdgesLearn = obj.lEdgesLearn(cannyEdgeMask);
  obj.rEdgesLearn = obj.rEdgesLearn(cannyEdgeMask);

  cannyEdgeMask = mask(obj.lEdgesCanny);
  cannyEdgeMask = cannyEdgeMask | mask(obj.rEdgesCanny);
  obj.lEdgesCanny = obj.lEdgesCanny(cannyEdgeMask);
  obj.rEdgesCanny = obj.rEdgesCanny(cannyEdgeMask);

  edgeFtrIdx = [obj.lEdgesLearn;obj.rEdgesLearn];
  %edgeFtrIdx = sort(edgeFtrIdx,1,'ascend'); % To orient edges in one particular direction
  
  %edgeFtrIdx = edgeFtrIdx(:);
  %D = size(obj.features,1);
  %obj.edgeFeatures = obj.features(:,edgeFtrIdx);
  %obj.edgeFeatures = reshape(obj.edgeFeatures,[2*size(obj.edgeFeatures,1) ...
                                               %size(obj.edgeFeatures,2)/2]);

  obj.edgeFeatures = grabCut_edgeLearnFlux.cpp.mex_getEdgeFeatures(...
      obj.features,int32(edgeFtrIdx),int32(obj.opts.edgeFeatureExtend),...
      int32(size(obj.img,1)),int32(size(obj.img,2)));

end
