function seg=gcSeg(obj,unaryImg,unaryMask,edgePosteriors)

import grabCut_edgeLearnFlux.cpp.*

objOpts=obj.opts;

opts.gcGamma=objOpts.gcGamma;
opts.gcScale=objOpts.gcScale;
opts.xoffset=obj.coffset';
opts.yoffset=obj.roffset';

% Add the flux to the unary terms, only altering the fg unaries
% (what matters is the difference unary anyways)
% Note: we cany use unaryImg(idx) because only altering fg unaries, unaryImg
% is actually a [h x w x 2] matrix, but the first plane is fg unaries
unaryFlux = obj.getFluxUnary(edgePosteriors);
unaryImg(:,:,1) = unaryImg(:,:,1)+unaryFlux;

if(isempty(obj.graphInfo))
  [seg,flow,graphInfo]=mex_dgcBand2D_init(unaryMask,unaryImg,obj.img,opts,...
                                          obj.lEdgesCanny,obj.rEdgesCanny);
  obj.graphInfo=graphInfo;
else
  [seg,flow]=mex_dgcBand2D_repeat(unaryMask,unaryImg,obj.graphInfo,opts,...
                                  obj.lEdgesCanny,obj.rEdgesCanny);
end
obj.nextIterationNum = obj.nextIterationNum+1;
