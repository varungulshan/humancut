% Copyright (C) 2010 Varun Gulshan
% This class implements grabCut_edgeLearnFlux segmentation
classdef segEngine < handle
  properties (SetAccess=private, GetAccess=public)
    state % string = 'init','pped','started'
    opts % object of type grabCut_edgeLearnFlux.segOpts
    img % double img, b/w [0,1]
    posteriorImage % data terms for graph cut, only if debugLevel > 0
    debugOpts % 
    seg % uint8, current segmentation
    labelImg % saves the annotation provided, as it gets used across iterations

    roffset % internal variables for graphcut
    coffset % internal variables for graphcut
    beta % internal variable for graphCut

    gmmf % internal variable for color model
    gmmb % internal variable for color model

    gmmNoEdge % internal variable for edge model
    gmm10Edge % internal variable for edge model
    gmm01Edge % internal variable for edge model

    features % internal variable
    edgeFeatures
    graphInfo % internal variable
    cannyEdgeMap % internal variable, type = logical
    cannyOrient % orientations of edges
    lEdgesCanny % 1 indexed
    rEdgesCanny % 1 indexed
    lEdgesLearn % 1 indexed
    rEdgesLearn % 1 indexed

    smoothFilterX
    smoothFilterY
    dxFilter
    dyFilter

    vectorFieldX % direction of unit normal canny edges, x component
    vectorFieldY % direction of unit normal canny edges, y component
    
    nextIterationNum % gets updated in gcSeg function
  end

  methods
    function obj=segEngine(debugOpts,segOpts)
      obj.debugOpts=debugOpts;
      assert(strcmp(class(segOpts),'grabCut_edgeLearnFlux.segOpts'));
      obj.opts=segOpts;
      obj.state='init';
      obj.seg=[];
      obj.img=[];
      obj.posteriorImage=[];
      obj.graphInfo=[];
      obj.cannyEdgeMap = [];
      obj.cannyOrient = [];
      obj.lEdgesCanny = [];
      obj.rEdgesCanny = [];
      obj.lEdgesLearn = [];
      obj.rEdgesLearn = [];
      obj.gmmNoEdge = [];
      obj.gmm10Edge = [];
      obj.gmm01Edge = [];
      obj.edgeFeatures = [];
      obj.nextIterationNum = 0;
    end
    preProcess(obj,img) % Call this function first to preprocess image if needed
                        % img should be double, between [0,1]
    start(obj,labelImg) %  Run grabCut_edgeLearnFlux given the initial labelImg
    % The number of iterations is given in opts, labelImg is encoded as follows:
    % labelImg=0 is empty
    % labelImg=1 is FG (hard constrained)
    % labelImg=2 is BG (hard contrained)
    % labelImg=3 is FG (soft, used only in first iteration to initialize color model)
    % labelImg=4 is BG (soft, used only in first iteration to initialize color model)
    % labelImg=5 is FG (hard constrained, but not used for color models)
    % labelImg=6 is BG (hard constrained, but not used for color models)
    iterateOnce(obj) % Performs one more iteration of grabCut_edge, can only be called after 
                     % start has been called
    delete(obj) % Destructor to clear graph cuts
  end

  methods (Access=private)
    [unaryImg,unaryMask]=createUnaryImg(obj) % Uses the stored gmm models and labelImg
                         % to compute unaries, also stores information that can be used
                         % to update color models for next iteration
    seg=gcSeg(obj,unaryImg,unaryMask,edgePosteriors)
    initEdgeFeatures(obj) % Initializes the edge features and edge gmm's
    updateEdgeModel(obj) % Initializes the edge features and edge gmm's
    edgePosteriors=getEdgePosterior(obj) % Returns edge classifier posterior
    img=drawEdgePosteriors(obj,edgePosteriors) % Returns edge classifier posterior
    unaryFlux = getFluxUnary(obj,edgePosteriors);
  end
end
