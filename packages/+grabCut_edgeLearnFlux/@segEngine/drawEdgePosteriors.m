function img=drawEdgePosteriors(obj,edgePosteriors)

img=zeros([size(obj.img,1) size(obj.img,2)]);

%edgesIdx = obj.lEdgesCanny;
%rEdgesMask = obj.cannyEdgeMap(obj.rEdgesCanny);
%edgesIdx(rEdgesMask) = obj.rEdgesCanny(rEdgesMask);

%img(edgesIdx) = edgePosteriors;
diffPosteriors = edgePosteriors(1,:)-edgePosteriors(2,:);
img(obj.lEdgesLearn) = 0.5*(1+diffPosteriors);
img(obj.rEdgesLearn) = 0.5*(1-diffPosteriors);
