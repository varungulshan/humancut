function edgePosteriors = getEdgePosterior(obj)

if(obj.opts.enableEdgePosterior & obj.nextIterationNum>=1)
  edge10_likeli = gmm.computeGmm_likelihood(obj.edgeFeatures,obj.gmm10Edge);
  edge01_likeli = gmm.computeGmm_likelihood(obj.edgeFeatures,obj.gmm01Edge);
  noEdge_likeli = gmm.computeGmm_likelihood(obj.edgeFeatures,obj.gmmNoEdge);
  
  sumLikeli = edge10_likeli+edge01_likeli+noEdge_likeli;
  sumLikeli(sumLikeli==0)=1;

  edgePosteriors = zeros([2 numel(obj.lEdgesLearn)]);
  edgePosteriors(1,:) = edge10_likeli./sumLikeli;
  edgePosteriors(2,:) = edge01_likeli./sumLikeli;
else
  edgePosteriors = zeros([2 numel(obj.lEdgesLearn)]);
end
