#include "mex.h"
#include <float.h>
#include <memory.h>
#include <math.h>
#include "matrix.h"
#include <iostream>
#include <vector>

#define SQR(x) (x)*(x)

inline void myAssert(bool condition,char *msg){
  if(!condition){
    mexErrMsgTxt(msg);
  }
}


typedef unsigned int uint;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters
     C         [ h x w x D] (double)
     edge      [ h x w] (logical)
     orientIdxs [h x w] (int32)
     roffset   [ K x 1] (int32)
     coffset   [ K x 1] (int32)
     Output:
     lEdges, rEdges [1 x N] (int32), 1 indexed edge indices
     vectorFieldX, vectorFieldY [h x w] (double)
  */

  myAssert(nrhs == 5, "5 input arguments expected.");
  if (nlhs != 4)
    mexErrMsgTxt("4 output arguments expected.");

  if(mxGetClassID(prhs[0])!=mxDOUBLE_CLASS) mexErrMsgTxt("prhs[0] (image) shd be of type double\n");
  myAssert(mxGetClassID(prhs[1])==mxLOGICAL_CLASS,"prhs[1] (edge) shd be of type logical\n");
  myAssert(mxGetClassID(prhs[2])==mxINT32_CLASS,"prhs[2] (orientIdxs) shd be of type logical\n");
  if(mxGetClassID(prhs[3])!=mxINT32_CLASS) mexErrMsgTxt("prhs[3] (roffset) shd be of type int32\n");
  if(mxGetClassID(prhs[4])!=mxINT32_CLASS) mexErrMsgTxt("prhs[4] (coffset) shd be of type int32\n");

  int numDims=mxGetNumberOfDimensions(prhs[0]);
  
  int h=mxGetDimensions(prhs[0])[0];
  int w=mxGetDimensions(prhs[0])[1];
  int N=h*w;
  int D;
  if(numDims==3)
    D=mxGetDimensions(prhs[0])[2];
  else
    D=1;

  myAssert(mxGetM(prhs[1])==h && mxGetN(prhs[1])==w,"Invalid edge map dimensions\n");
  myAssert(mxGetM(prhs[2])==h && mxGetN(prhs[2])==w,"Invalid orientIdx dimensions\n");

  if(mxGetN(prhs[3])!=1 || mxGetN(prhs[4])!=1)
      mexErrMsgTxt("Invalid size for roffset or coffset\n");

  int K=mxGetM(prhs[3]);
  if(mxGetM(prhs[4])!=K)
    mexErrMsgTxt("Invalid size for roffset or coffset\n");

  bool *edgeMap = (bool*)mxGetData(prhs[1]);
  int *orientIdxs = (int*)mxGetData(prhs[2]);
  int *roffsets=(int*)mxGetData(prhs[3]);
  int *coffsets=(int*)mxGetData(prhs[4]);

  double *C=mxGetPr(prhs[0]);

  int *indexOffsets=(int*)mxMalloc(K*sizeof(int));
  double *vectorFieldX=(double*)mxMalloc(K*sizeof(double));
  double *vectorFieldY=(double*)mxMalloc(K*sizeof(double));

  for(int i=0;i<K;i++) {
    indexOffsets[i]=coffsets[i]*h+roffsets[i];
    double norm=sqrt(SQR(coffsets[i])+SQR(roffsets[i]));
    vectorFieldX[i]=coffsets[i]/norm;
    vectorFieldY[i]=roffsets[i]/norm;
  }

  plhs[2]=mxCreateNumericMatrix(h,w,mxDOUBLE_CLASS,mxREAL);
  plhs[3]=mxCreateNumericMatrix(h,w,mxDOUBLE_CLASS,mxREAL);
  double *outVectorFieldX = mxGetPr(plhs[2]);
  double *outVectorFieldY = mxGetPr(plhs[3]);
  memset(outVectorFieldX,0,h*w*sizeof(double));
  memset(outVectorFieldY,0,h*w*sizeof(double));

  std::vector<int> lEdges;
  std::vector<int> rEdges;

  int x,y;
  bool *ptrEdges = edgeMap;
  int *ptrOrient = orientIdxs;
  for(x=0;x<w;x++){
    for(y=0;y<h;y++,ptrEdges++,ptrOrient++){
      if(!(*ptrEdges)){continue;}
      int lEdge=y+x*h;
      {
        int i = *ptrOrient - 1;
        double gradient[2]={-1,-1};
        int r[2];
        int c[2];
        int rEdge[2];
        
        r[0]=y+roffsets[i];
        r[1]=y-roffsets[i];

        if(r[0]>=0 && r[0]<h){
          c[0]=x+coffsets[i];
          if(c[0]>=0 && c[0]<w){
            int idxOffset = indexOffsets[i];
            rEdge[0]=lEdge+idxOffset;
            double colorW=0;
            for(int d=0;d<D;d++){
              double diff=(C[lEdge+d*N]-C[rEdge[0]+d*N]);
              colorW+=diff*diff;
            }
            gradient[0]=colorW;
          }
        }

        if(r[1]>=0 && r[1]<h){
          c[1]=x-coffsets[i];
          if(c[1]>=0 && c[1]<w){
            int idxOffset = -indexOffsets[i];
            rEdge[1]=lEdge+idxOffset;
            double colorW=0;
            for(int d=0;d<D;d++){
              double diff=(C[lEdge+d*N]-C[rEdge[1]+d*N]);
              colorW+=diff*diff;
            }
            gradient[1]=colorW;
          }
        }

        int maxIdx = -1;
        maxIdx = (gradient[0]>gradient[1]) ? 0:1;
        int signVfield[2] = {+1,-1};
        if(gradient[maxIdx]!=-1){
          int signField = signVfield[maxIdx];
          int lIdx = std::min(lEdge,rEdge[maxIdx]);
          int rIdx = std::max(lEdge,rEdge[maxIdx]);

          lEdges.push_back(lIdx);
          rEdges.push_back(rIdx);
          
          if(lEdge!=lIdx) {signField=-signField;}

          double vFieldX = signField*vectorFieldX[i];
          double vFieldY = signField*vectorFieldY[i];
          outVectorFieldX[lIdx] = vFieldX;
          outVectorFieldY[lIdx] = vFieldY;
          outVectorFieldX[rIdx] = vFieldX;
          outVectorFieldY[rIdx] = vFieldY;

          //lEdges.push_back(lEdge);
          //rEdges.push_back(rEdge[maxIdx]);
          // the min max ensures edges have particular direction
        }
      }
    }
  }

  int numEdges=lEdges.size();

  plhs[0]=mxCreateNumericMatrix(1,numEdges,mxINT32_CLASS,mxREAL);
  plhs[1]=mxCreateNumericMatrix(1,numEdges,mxINT32_CLASS,mxREAL);

  int *lEdges_out=(int*)mxGetData(plhs[0]);
  int *rEdges_out=(int*)mxGetData(plhs[1]);

  for(int i=0;i<numEdges;i++){
    lEdges_out[i]=lEdges[i]+1;
    rEdges_out[i]=rEdges[i]+1;
  }

  // --- To free ---
  mxFree(indexOffsets);
  mxFree(vectorFieldX);
  mxFree(vectorFieldY);

}
