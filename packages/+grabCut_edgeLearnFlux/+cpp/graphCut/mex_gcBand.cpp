#include "mex.h"
#include "graph.h"
#include <cmath>
#include <iostream>
#include <climits>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters combinations
     rhs[0] = mask [h x w] (255 for fg, 0 for bg, 128 for where to run graph cut), uint8
     rhs[1] = negLog_likelihood for fg (only used in mask==128), double
     rhs[2] = negLog_likelihood for bg (only used in mask==128), double
     rhs[3] = gamma, double
     rhs[4] = beta, double
     rhs[5] = roffset, [L x 1] int32
     rhs[6] = coffset, [L x 1] int32
     rhs[7] = gcScale, double
     rhs[8] = gamma_ising, double
     rhs[9] = img, [h x w x nCh] double

     lhs[0] -> returns the seg labels of all nodes (array of type hxw uint8)
     lhs[1] -> returns the flow
   */

  if (nrhs != 10)
    mexErrMsgTxt("10 inputs required");

  if(mxGetClassID(prhs[0])!=mxUINT8_CLASS)
    mexErrMsgTxt("prhs[0] (mask) should be of type uint8\n");
  if(mxGetClassID(prhs[1])!=mxDOUBLE_CLASS)
    mexErrMsgTxt("prhs[1] (negLogLikeli fg) should be of type double\n");
  if(mxGetClassID(prhs[2])!=mxDOUBLE_CLASS)
    mexErrMsgTxt("prhs[2] (negLogLikeli bg) should be of type double\n");
  if(mxGetClassID(prhs[3])!=mxDOUBLE_CLASS)
    mexErrMsgTxt("prhs[3] (gamma) should be of type double\n");
  if(mxGetClassID(prhs[4])!=mxDOUBLE_CLASS)
    mexErrMsgTxt("prhs[4] (beta) should be of type double\n");
  if(mxGetClassID(prhs[5])!=mxINT32_CLASS)
    mexErrMsgTxt("prhs[5] (roffset) should be of type int32\n");
  if(mxGetClassID(prhs[6])!=mxINT32_CLASS)
    mexErrMsgTxt("prhs[6] (coffset) should be of type int32\n");
  if(mxGetClassID(prhs[7])!=mxDOUBLE_CLASS)
    mexErrMsgTxt("prhs[7] (gcScale) should be of type double\n");
  if(mxGetClassID(prhs[8])!=mxDOUBLE_CLASS)
    mexErrMsgTxt("prhs[8] (gamma_ising) should be of type double\n");
  if(mxGetClassID(prhs[9])!=mxDOUBLE_CLASS)
    mexErrMsgTxt("prhs[9] (img) should be of type double\n");


  typedef Graph<int,int,int> GraphInt;
  int h=mxGetM(prhs[0]);
  int w=mxGetN(prhs[0]);
  int n=h*w;
  int nOffset=mxGetM(prhs[5]);

  if(mxGetM(prhs[1])!=h || mxGetN(prhs[1])!=w) mexErrMsgTxt("prhs[1] (fg Log likeli) incorrect dimensions\n");
  if(mxGetM(prhs[2])!=h || mxGetN(prhs[2])!=w) mexErrMsgTxt("prhs[2] (bg Log likeli) incorrect dimensions\n");

  if(mxGetN(prhs[3])!=1 || mxGetM(prhs[3])!=1) mexErrMsgTxt("prhs[3] (gamma) shd be 1x1\n");
  if(mxGetN(prhs[4])!=1 || mxGetM(prhs[4])!=1) mexErrMsgTxt("prhs[4] (beta) shd be 1x1\n");
  if(mxGetN(prhs[5])!=1) mexErrMsgTxt("prhs[5] (roffset) incorrect dim\n");
  if(mxGetN(prhs[6])!=1 || mxGetM(prhs[6])!=nOffset) mexErrMsgTxt("prhs[6] (coffset) incorrect dim\n");
  if(mxGetN(prhs[7])!=1 || mxGetM(prhs[7])!=1) mexErrMsgTxt("prhs[7] (gcScale) shd be 1x1\n");
  if(mxGetN(prhs[8])!=1 || mxGetM(prhs[8])!=1) mexErrMsgTxt("prhs[8] (gamma_ising) shd be 1x1\n");
  if(mxGetDimensions(prhs[9])[0]!=h || mxGetDimensions(prhs[9])[1]!=w) mexErrMsgTxt("prhs[9] (img) incorrect dimensions\n");

  if(sizeof(int)<4)
    mexErrMsgTxt("Code assumes int is > 4 bytes, it isnt\n");
  // ---- Data verified ok, now set up all the pointers
  int numDims=mxGetNumberOfDimensions(prhs[9]);
  int D;
  if(numDims==3)
    D=mxGetDimensions(prhs[9])[2];
  else
    D=1;

  unsigned char *mask=(unsigned char*)mxGetData(prhs[0]);
  double *fgL=mxGetPr(prhs[1]);
  double *bgL=mxGetPr(prhs[2]);
  double gamma=*mxGetPr(prhs[3]);
  double beta=*mxGetPr(prhs[4]);
  double gamma_ising=*mxGetPr(prhs[8]);
  double gcScale=*mxGetPr(prhs[7]);
  double *img=mxGetPr(prhs[9]);
  int *idxMap=(int*)mxMalloc(h*w*sizeof(int));

  // -- scan the mask to index the pixels ----
  unsigned char *it_mask=mask;
  int *it_idxMap=idxMap;
  int idx=0;
  for(int i=0;i<n;i++,it_mask++,it_idxMap++){
    if(*it_mask==128){*it_idxMap=idx;idx++;}
    else{*it_idxMap=-1;}
  }

  // -- setup the graph now ---
  int nNodes=idx;
  int mBound=nNodes*nOffset; // Upper bound on number of edges in graph

  GraphInt *g=newGraph<int,int,int>(nNodes,mBound,NULL); 
  g->add_node(nNodes);

  // -- add pairwise terms and alter unaries (for pixels nxt to hard constraints) --

  int *roffsets=(int*)mxGetData(prhs[5]);
  int *coffsets=(int*)mxGetData(prhs[6]);

  int *indexOffsets=(int*)mxMalloc(nOffset*sizeof(int));
  for(int i=0;i<nOffset;i++) {
    indexOffsets[i]=coffsets[i]*h+roffsets[i];
  }

  it_mask=mask;
  it_idxMap=idxMap;
  for(int x=0;x<w;x++){
    for(int y=0;y<h;y++,it_mask++,it_idxMap++){
      int lEdge=y+x*h;
      for(int i=0;i<nOffset;i++){
        int r=y+roffsets[i];
        if(r>=0 && r<h){
          int c=x+coffsets[i];
          if(c>=0 && c<w){
            int nbrOffset=indexOffsets[i];
            int rEdge=lEdge+nbrOffset;
            unsigned char nxt_mask=*(it_mask+nbrOffset);
            if(*it_mask==128){
              // compute edge weight
              double colorW=0;
              for(int d=0;d<D;d++){
                double diff=(img[lEdge+d*n]-img[rEdge+d*n]);
                colorW+=diff*diff;
              }
              double edgeW_dbl=(gamma_ising+gamma*exp(-beta*colorW));
              if(nxt_mask==128){
                // add edge to graph
                int edgeW=(int)(gcScale*edgeW_dbl);
                g->add_edge(*it_idxMap,*(it_idxMap+nbrOffset),edgeW,edgeW); 
              }
              else if(nxt_mask==255){
                double *lEdge_bgL=bgL+lEdge;
                *lEdge_bgL=*lEdge_bgL+edgeW_dbl;
              }
              else if(nxt_mask==0){
                double *lEdge_fgL=fgL+lEdge;
                *lEdge_fgL=*lEdge_fgL+edgeW_dbl;
              }
              else{
                mexErrMsgTxt("Invalid value in likelihood mask\n");
              }
            } // end if *it_mask==128
            else if(nxt_mask==128){
              double colorW=0;
              for(int d=0;d<D;d++){
                double diff=(img[lEdge+d*n]-img[rEdge+d*n]);
                colorW+=diff*diff;
              }
              double edgeW_dbl=(gamma_ising+gamma*exp(-beta*colorW));
              switch(*it_mask){
                case 0 :
                  {double *rEdge_fgL=fgL+rEdge;
                  *rEdge_fgL=*rEdge_fgL+edgeW_dbl;}
                  break;
                case 255 :
                  {double *rEdge_bgL=bgL+rEdge;
                  *rEdge_bgL=*rEdge_bgL+edgeW_dbl;}
                  break;
                default:
                   mexErrMsgTxt("Unexpected mask value in likeli_mask\n");
              }
            } // end if nxtMask==128
          }
        }
      }
    }
  }

  it_mask=mask;
  it_idxMap=idxMap;
  int intMax=(1<<30);

  for(int i=0;i<n;i++,it_mask++,it_idxMap++){
    if(*it_mask==128){
      int curIdx=*it_idxMap;
      double fgLikeli=fgL[i];
      double bgLikeli=bgL[i];
      double diff=gcScale*(fgLikeli-bgLikeli);
      double dbl_intMax=(double)(intMax);
      double dbl_intMin=(double)(-intMax);
      int fgUnary;
      if(diff>dbl_intMax){fgUnary=intMax;}
      else if(diff<dbl_intMin){fgUnary=-intMax;}
      else{fgUnary=(int)diff;}
      g->add_tweights(curIdx,0,fgUnary);
    }
  }

  int flow=g->maxflow();

  // --- Now prepare the output ------------
  if(nlhs < 2) {mexErrMsgTxt("Atleast two outputs required\n");}
  int dims[2];dims[0]=h;dims[1]=w;

  plhs[0]=mxCreateNumericArray(2,dims,mxUINT8_CLASS,mxREAL);
  plhs[1]=mxCreateDoubleScalar((double)flow);

  unsigned char *it_seg=(unsigned char*)mxGetData(plhs[0]);
  it_mask=mask;
  it_idxMap=idxMap;
  for(int i=0;i<n;i++,it_seg++,it_mask++,it_idxMap++){
    if(*it_mask==128){
      int curIdx=*it_idxMap;
      *it_seg=(g->what_segment(curIdx)==GraphInt::SOURCE?255:0); // 255 for source, 0 for sink
    }
    else if(*it_mask==255){*it_seg=255;}
    else{*it_seg=0;}
  }

  // ------- To free -------------
  mxFree(idxMap);
  mxFree(indexOffsets);
  deleteGraph<int,int,int>(&g);
}
