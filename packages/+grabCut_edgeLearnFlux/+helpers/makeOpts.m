function opts=makeOpts(optsString)

switch(optsString)

  case 'iterQuickLearn'
    opts=grabCut_edgeLearnFlux.segOpts();
    opts.gcGamma=50;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=1;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.kovesiSigma = 1;
    opts.kovesiAutoThresh = 0.7;
    opts.enableEdgePosterior = true;

    opts.gmmNmix_yesEdge = 6;
    opts.gmmNmix_noEdge = 8;
    opts.edgeFeatureExtend = 4;
    opts.fluxStrength = 5;

  case 'iterQuick'
    opts=grabCut_edgeLearnFlux.segOpts();
    opts.gcGamma=50;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=1;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.kovesiSigma = 1;
    opts.kovesiAutoThresh = 0.7;
    opts.enableEdgePosterior = false;
    opts.edgeFeatureExtend = 4;
    opts.fluxStrength = 5;

   case 'iter10_2'
    opts=grabCut_edgeLearnFlux.segOpts();
    opts.gcGamma=50;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.kovesiSigma = 1;
    opts.kovesiAutoThresh = 0.7;
    opts.enableEdgePosterior = false;
    opts.fluxStrength = 5;

   case 'iter10_2_edgeLearn'
    opts=grabCut_edgeLearnFlux.segOpts();
    opts.gcGamma=50;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.kovesiAutoThresh = 0.7;
    opts.enableEdgePosterior = true;

    opts.gmmNmix_yesEdge = 6;
    opts.gmmNmix_noEdge = 8;
    opts.kovesiSigma = 1;
    opts.fluxStrength = 5;
    
   case 'iter10_3'
    opts=grabCut_edgeLearnFlux.segOpts();
    opts.gcGamma=25;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.kovesiSigma = 1;
    opts.kovesiAutoThresh = 0.7;
    opts.enableEdgePosterior = false;
    opts.fluxStrength = 5;

   case 'iter10_3_edgeLearn'
    opts=grabCut_edgeLearnFlux.segOpts();
    opts.gcGamma=25;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;

    opts.cannyThresh = 'auto';
    opts.kovesiAutoThresh = 0.7;
    opts.enableEdgePosterior = true;

    opts.gmmNmix_yesEdge = 6;
    opts.gmmNmix_noEdge = 8;
    opts.kovesiSigma = 1;
    opts.fluxStrength = 5;
    
  otherwise
    error('Invalid options string %s\n',optsString);
end
