function opts=evalOpts_humanCut(optsString)

cwd=miscFns.extractDirPath(mfilename('fullpath'));

switch(optsString)

  case 'try1'
    opts.gtDir=[cwd '../../data/h3d_gtSeg/'];
    opts.scores={'accuracyOverlap'};
  case 'try2'
    opts.gtDir=[cwd '../../data/h3d_gtSeg2/'];
    opts.scores={'accuracyOverlap'};
   otherwise
    error('Invalid option string %s\n',optsString);
end
