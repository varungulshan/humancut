function opts=evalOpts_grabCut(optsString)

cwd=miscFns.extractDirPath(mfilename('fullpath'));

switch(optsString)
  case 'try1'
    opts.gtDir=[cwd '../../data/h3d_gtSeg2/'];
    opts.scores={'accuracyOverlap'};
  case 'kinect_firstAttempt'
    opts.gtDir=['/data2/varun/iccv2011/data/gt_kinectData/firstAttempt/'];
    opts.scores={'accuracyOverlap'};
  case 'kinect_10Feb2011'
    opts.gtDir=['/data2/varun/iccv2011/data/gt_kinectData/10Feb2011_cleanUp1/'];
    opts.scores={'accuracyOverlap'};
  case 'kinect_10Feb2011_newGT'
    opts.gtDir=['/data2/varun/iccv2011/data/gt_kinectData/10Feb2011_cleanUp2/'];
         opts.scores={'accuracyOverlap'};
   otherwise
    error('Invalid option string %s\n',optsString);
end
