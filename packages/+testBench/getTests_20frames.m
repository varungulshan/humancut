function [paths,files]=getTests_20frames()

paths=cell(0,1);
files=cell(0,1);

paths{end+1}='../../data/cuts/love_actually_1-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/footballer-28to48/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/surfer-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/buffy-s5e02_1-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/maninhat-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/buffy-s5e03_2-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/buffy-s5e10_1-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/gym-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/my_cousin_vinny_1-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/horse-1to21/';
files{end+1}='00001.png';

cwd=miscFns.extractDirPath(mfilename('fullpath'));
for i=1:length(paths)
  paths{i}=[cwd paths{i}];
end
