function opts=getTests_h3d1()

cwd=miscFns.extractDirPath(mfilename('fullpath'));

opts.imgList=[cwd '../+testBench/h3dList_small.txt'];
opts.imgDir=[cwd '../../../../software/h3d_version1.0/data/people/images/'];
opts.annoDir=[cwd '../../../../software/h3d_version1.0/data/people/info/'];
