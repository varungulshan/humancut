function opts=getTests_grabCut_bmvc_externalImages()

cwd=miscFns.extractDirPath(mfilename('fullpath'));

opts.imgList=[cwd '../../data/grabCutLists/externalImages.txt'];
opts.imgDir=[cwd '../../data/bmvc_outsideDataset_images/'];
opts.labelDir=[cwd '../../data/bmvc_outsideDataset_labelsSaved/'];
