function plotStats(resDir)
labelFont_size=30;

statsFile=[resDir 'stats.mat'];
load(statsFile); % Provides gStats

maxFrames=gStats.maxFrames;
numVideos=length(gStats.stats);

maskValid=false(numVideos,maxFrames);
scores=zeros(numVideos,maxFrames);

stats=gStats.stats;
for i=1:numVideos
  frameNums=stats(i).frameNums;
  scores(i,frameNums)=stats(i).scores;
  maskValid(i,frameNums)=true;
end

mask_frameNum=any(maskValid,1);
valid_fNums=find(mask_frameNum); % should be sorted
means=zeros(size(valid_fNums));
stdErrs=zeros(size(valid_fNums));

for i=1:length(valid_fNums)
  frameNum=valid_fNums(i);
  validScores=scores(maskValid(:,frameNum),frameNum);
  numScores=length(validScores);
  means(i)=mean(validScores);
  stdErrs(i)=std(validScores,1)/sqrt(numScores);
end

bar(valid_fNums,means);
hold on;
errorbar(valid_fNums,means,stdErrs,'linestyle','none','linewidth',2);
xlabel('Frame number','fontsize',labelFont_size);
ylabel(gStats.opts.scoreString,'fontsize',labelFont_size);
cwd=miscFns.extractDirPath(mfilename('fullpath'));
load([cwd 'position_plot']); % Provides pos
set(gcf,'outerposition',pos);
cmd=['print -dpng ' resDir 'plot.png'];
eval(cmd);
