function computeStats_grabCut(resDir,evalOpts_string)
% Computes segmentation scores for a particular dir

  opts=testBench.evalOpts_grabCut(evalOpts_string);
  if(~exist(resDir,'dir')), error('results dir %s does not exist\n',resDir); end
  fH=fopen([resDir 'evalOpts.txt'],'w');
  miscFns.printStructure(opts,fH);
  fclose(fH);

  tmpLoad=load([resDir 'testOpts.mat']);
  testOpts=tmpLoad.testOpts;

  gStats.evalOpts_string=evalOpts_string;
  gStats.opts=opts;
  gStats.resDir=resDir;
  gStats.testOpts=testOpts;

  for iScore=1:length(opts.scores)
    stats=getStats_byScore([resDir 'segs/'],opts,opts.scores{iScore},testOpts);
    cmd=['gStats.stats_' opts.scores{iScore} '=stats;'];
    eval(cmd);
  end
  printStats([resDir 'stats.txt'],gStats,opts);
  save([resDir 'stats.mat'],'gStats');

function printStats(statsFile,gStats,opts)

fH=fopen(statsFile,'w');
fprintf(fH,'Reporting segmentation scores on dir: %s\nEvaluation string: %s\n\n',...
        gStats.resDir,gStats.evalOpts_string);

for i=1:length(opts.scores)
  iScore=opts.scores{i};
  cmd=['gStats.stats_' iScore];
  stats=eval(cmd);
  scores=[stats.score];
  stdErr_scores=std(scores,1)/sqrt(length(scores));
  fprintf(fH,'Mean %s score: %.2f stdErr %.2f (over %d humans)\n',...
          iScore,mean(scores),stdErr_scores,length(scores));
end
fprintf(fH,'\n\n');
for i=1:length(opts.scores)
  iScore=opts.scores{i};
  cmd=['gStats.stats_' iScore];
  stats=eval(cmd);
  scores=[stats.score];
  [scores,sortIdx]=sort(scores);
  stats=stats(sortIdx);
  fprintf(fH,'---- Printing detailed *%s* scores (ascending) per human -----\n\n',iScore);
  fprintf(fH,'%35s %7s\n','Img name','Score');

  for j=1:length(stats)
    fprintf(fH,'%35s %7s\n',stats(j).imgName,...
            sprintf('%.2f',stats(j).score));
  end
  fprintf(fH,'\n\n');
end

fclose(fH);

function stats=getStats_byScore(resDir,opts,scoreType,testOpts)

images=textread(testOpts.imgList,'%s');

for i=1:length(images)
  objectId = grabCut_trained.dataset.extractObjectIdentifier(images{i});
  segFile=[resDir objectId '.png'];
  clear seg;
  seg = imread(segFile);
  gtFile=[opts.gtDir objectId '.png'];
  gt=imread(gtFile);
  score=testBench.evaluateSeg(seg,gt,scoreType);
  stats(i).score=score;
  stats(i).imgName=images{i};
end
