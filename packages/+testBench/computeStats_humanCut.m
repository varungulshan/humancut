function computeStats_humanCut(resDir,evalOpts_string)
% Computes segmentation scores for a particular dir
% this evaluation is done for humanCut where you have a segmentation
% for each track. There may be more than one GT per video (each representing
% a person). Also there may be >=0 tracks per video. 
% The evaluation can compute many kinds of scores, see evalOpts_humanCut for
% various options.

  opts=testBench.evalOpts_humanCut(evalOpts_string);
  if(~exist(resDir,'dir')), error('results dir %s does not exist\n',resDir); end
  fH=fopen([resDir 'evalOpts.txt'],'w');
  miscFns.printStructure(opts,fH);
  fclose(fH);

  gStats.evalOpts_string=evalOpts_string;
  gStats.opts=opts;
  gStats.resDir=resDir;

  for iScore=1:length(opts.scores)
    stats=getStats_byScore(resDir,opts,opts.scores{iScore});
    cmd=['gStats.stats_' opts.scores{iScore} '=stats;'];
    eval(cmd);
  end
  printStats([resDir 'stats.txt'],gStats,opts);
  save([resDir 'stats.mat'],'gStats');

function printStats(statsFile,gStats,opts)

fH=fopen(statsFile,'w');
fprintf(fH,'Reporting segmentation scores on dir: %s\nEvaluation string: %s\n\n',...
        gStats.resDir,gStats.evalOpts_string);

for i=1:length(opts.scores)
  iScore=opts.scores{i};
  cmd=['gStats.stats_' iScore];
  stats=eval(cmd);
  scores=[stats.score];
  stdErr_scores=std(scores,1)/sqrt(length(scores));
  fprintf(fH,'Mean %s score: %.2f stdErr %.2f (over %d tracks)\n',...
          iScore,mean(scores),stdErr_scores,length(scores));
end
fprintf(fH,'\n\n');
for i=1:length(opts.scores)
  iScore=opts.scores{i};
  cmd=['gStats.stats_' iScore];
  stats=eval(cmd);
  fprintf(fH,'---- Printing detailed *%s* scores per track -----\n\n',iScore);
  fprintf(fH,'%35s %9s %7s %12s %6s\n','Video name','Track num','Score','Track length',...
                                  'GT num');
  for j=1:length(stats)
    fprintf(fH,'%35s %9s %7s %12s %6s\n',stats(j).videoName,...
            sprintf('%03d',stats(j).trackNum),...
            sprintf('%.2f',stats(j).score),sprintf('%03d',stats(j).trackL),...
            sprintf('%02d',stats(j).gtNum));
  end
  fprintf(fH,'\n\n');
end

fclose(fH);

function stats=getStats_byScore(resDir,opts,scoreType)

[videoPaths,videoFiles]=eval(opts.datasetCmd);
videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);

switch(scoreType)
  case 'scoreAll'
    stats=getStats_scoreAll(resDir,videoNames,opts.gtDir,opts.scoreString);
  case 'scoreFull'
    stats=getStats_scoreFull(resDir,videoNames,opts.gtDir,opts.scoreString);
end

function stats=getStats_scoreFull(resDir,videoNames,gtDir,scoreString)

%stats=struct('score',[],'trackDir',[],'gtNum',[],'trackL',[]);
stats=struct([]);
iStat=1;

for i=1:length(videoNames)
  iGtDir=[gtDir videoNames{i} '/'];
  iResDir=[resDir videoNames{i} '/'];
  [scoreMatrix,trackDirs,trackLs,videoLs]=getScoreMatrix(iGtDir,iResDir,scoreString);
  trackNums=[1:length(trackLs)];
  validTracks=(trackLs==videoLs);
  trackLs=trackLs(validTracks);
  trackDirs=trackDirs(validTracks);
  videoLs=videoLs(validTracks);
  trackNums=trackNums(validTracks);
  scoreMatrix=scoreMatrix(validTracks,:);
  [numTrack,numGt]=size(scoreMatrix);
  [gtIds,trackIds]=meshgrid([1:numGt],[1:numTrack]);
  valid=logical(ones(size(scoreMatrix)));
  while(any(valid(:)))
    [bestScore,id]=max(scoreMatrix(valid)); 
    validTrackIds=trackIds(valid);
    validGtIds=gtIds(valid);
    curTrack_id=validTrackIds(id);
    curGt_id=validGtIds(id);
    valid(:,curGt_id)=false;
    valid(curTrack_id,:)=false;
    stats(iStat).score=bestScore;
    stats(iStat).trackDir=trackDirs{curTrack_id};
    stats(iStat).gtNum=curGt_id;
    stats(iStat).trackL=trackLs(curTrack_id);
    stats(iStat).videoName=videoNames{i};
    stats(iStat).trackNum=trackNums(curTrack_id);
    iStat=iStat+1;
  end
end

function stats=getStats_scoreAll(resDir,videoNames,gtDir,scoreString)

%stats=struct('score',[],'trackDir',[],'gtNum',[],'trackL',[]);
stats=struct([]);
iStat=1;

for i=1:length(videoNames)
  iGtDir=[gtDir videoNames{i} '/'];
  iResDir=[resDir videoNames{i} '/'];
  [scoreMatrix,trackDirs,trackLs,xx]=getScoreMatrix(iGtDir,iResDir,scoreString);
  [numTrack,numGt]=size(scoreMatrix);
  [gtIds,trackIds]=meshgrid([1:numGt],[1:numTrack]);
  valid=logical(ones(size(scoreMatrix)));
  gtAvailable=logical(ones(numGt,1));
  for j=1:numGt
    if(~any(valid(:)))
      id=find(gtAvailable,1,'first');
      gtAvailable(id)=false;
      stats(iStat).score=0;
      stats(iStat).trackDir='emptyTrack';
      stats(iStat).gtNum=id;
      stats(iStat).trackL=0;
      stats(iStat).videoName=videoNames{i};
      stats(iStat).trackNum=-1;
    else
      [bestScore,id]=max(scoreMatrix(valid)); 
      validTrackIds=trackIds(valid);
      validGtIds=gtIds(valid);
      curTrack_id=validTrackIds(id);
      curGt_id=validGtIds(id);
      valid(:,curGt_id)=false;
      valid(curTrack_id,:)=false;
      gtAvailable(curGt_id)=false;
      stats(iStat).score=bestScore;
      stats(iStat).trackDir=trackDirs{curTrack_id};
      stats(iStat).gtNum=curGt_id;
      stats(iStat).trackL=trackLs(curTrack_id);
      stats(iStat).videoName=videoNames{i};
      stats(iStat).trackNum=curTrack_id;
    end
    iStat=iStat+1;
  end
end

function [scoreMat,trackDirs,trackLs,videoLs]=getScoreMatrix(gtDir,resDir,scoreString)
  trackDirs={};
  trackLs=[];

  done=false;
  iTrackDir=1;
  while(~done)
    testDir=[resDir sprintf('track%03d_files/',iTrackDir)];
    if(exist(testDir,'dir'))
      trackDirs{iTrackDir}=testDir;
      iTrackDir=iTrackDir+1;
    else
      done=true;
    end
  end

  numGtDir=0;
  while(true)
    testDir=[gtDir sprintf('human_%03d/',numGtDir+1)];
    if(~exist(testDir,'dir'))
      break;
    else
      numGtDir=numGtDir+1;
    end
  end

  numTracks=length(trackDirs);
  scoreMat=zeros(numTracks,numGtDir);
  trackLs=zeros(numTracks,1);
  videoLs=zeros(numTracks,1);

  for i=1:numTracks
    segFile=[trackDirs{i} 'seg.mat'];
    clear seg trackSt trackEnd;
    load(segFile); % provides the variable seg,trackSt,trackEnd
    trackLs(i)=trackEnd-trackSt+1;
    videoLs(i)=size(seg,3);
    for iGt=1:numGtDir
      iGt_dir=[gtDir sprintf('human_%03d/',iGt)];
      scoreSum=0;
      numGt=0;
      for j=1:size(seg,3)
        gtFile=[iGt_dir sprintf('%05d.png',j)];
        if(exist(gtFile,'file'))
          numGt=numGt+1;
          if(j>=trackSt & j<=trackEnd)
            gt=imread(gtFile);
            scoreSum=scoreSum+testBench.evaluateSeg(seg(:,:,j),gt,scoreString);
          end % otherwise a score of 0 is given effectively, because numGt still gets
              % incremented
        end 
      end % end loop over frames
      scoreMat(i,iGt)=scoreSum/numGt;
    end % end loop over different GTs
  end % end loop over tracks
