function hBars=plotBars_withErrs(xLabels,yMeans,yErrs,barColors,markerColors)
% xLabels -> 1 x N point on x axis
% yMeans -> M x N y values, Each row is considered a different method
% yErrs -> M x N err bar values
% barColors and marker colors are optional

% ---- Internal settings ------------
barWidth=1;
barColors_default=[0 179 91;255 23 27;  96 192 255;255 201 54;137 113 218]/255;
markerColors_default=[ 150 216 183; 255 87 107;176 190 205;255 255 51;178 154 255]/255;

if(~exist('barColors','var')),
  barColors=repmat(barColors_default,[10 1]);
end
if(~exist('markerColors','var')),
  markerColors=repmat(markerColors_default,[10 1]);
end
% ----- End internal settings --------

figure;
numX=size(xLabels,2);
hBars=bar(yMeans');
for i=1:numX
  xLabels_string{i}=num2str(xLabels(i));
end
%set(get(hBars,'parent'),'xticklabel',xLabels_string);
set(gca,'xticklabel',xLabels_string);

for i=1:size(yMeans,1), set(hBars(i),'facecolor',barColors(i,:)); end;
set(hBars,'barwidth',barWidth);

numBars=size(yMeans,1);
xPosErrBars=zeros(numX,numBars);
allBarsWidth=(numBars-1)/(numBars+1); % Guess work on position of bars

if numBars==1, allBarsWidth=0; gap=0;
else , gap=allBarsWidth/(numBars-1); end;

offsets(1)=-allBarsWidth/2;
for i=2:numBars, offsets(i)=offsets(i-1)+gap; end;

for j=1:numX
  for i=1:numBars, 
    xPosErrBars(j,i)=j+offsets(i);
  end
end;

hold on;
hErrBars=errorbar(xPosErrBars,yMeans',yErrs','linestyle','none','linewidth',2);
for i=1:numBars, set(hErrBars(i),'color',markerColors(i,:)); end;
hold off;
