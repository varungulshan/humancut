function opts=getTests_grabCut_emptyTrain()

cwd=miscFns.extractDirPath(mfilename('fullpath'));

opts.imgList=[cwd '../../data/grabCutLists/empty.txt'];
opts.imgDir=[cwd '../../../../software/h3d_version1.0/data/people/images/'];
opts.labelDir=[cwd '../../data/grabCut_boxLabels/h3d/'];
%opts.imgList=[cwd '../../data/h3d_useCaseLists/type1And2_type1.txt'];
%opts.imgDir=[cwd '../../../../software/h3d_version1.0/data/people/images/'];
