function opts=evalOpts(optsString)

cwd=miscFns.extractDirPath(mfilename('fullpath'));

switch(optsString)
  case 'try1'
    opts.gtDir=[cwd '../../data/manualGT/'];
    opts.scoreString='accuracyOverlap';
    opts.datasetCmd='testBench.getTests_20frames';
    opts.singleScore='areaUnderCurve'; % takes average of area under curve for each video
  case 'try2'
    opts.gtDir=[cwd '../../data/manualGT/'];
    opts.scoreString='accuracyOverlap';
    opts.datasetCmd='testBench.getTests_20frames_2';
    opts.singleScore='areaUnderCurve'; % takes average of area under curve for each video
  case 'try3'
    opts.gtDir=[cwd '../../data/manualGT/'];
    opts.scoreString='accuracyOverlap';
    opts.datasetCmd='testBench.getTests_20frames_3';
    opts.singleScore='areaUnderCurve'; % takes average of area under curve for each video
  case 'try4'
    opts.gtDir=[cwd '../../data/manualGT/'];
    opts.scoreString='accuracyOverlap';
    opts.datasetCmd='testBench.getTests_20frames_4';
    opts.singleScore='areaUnderCurve'; % takes average of area under curve for each video
   otherwise
    error('Invalid option string %s\n',optsString);
end
