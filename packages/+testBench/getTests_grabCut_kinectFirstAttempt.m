function opts=getTests_grabCut_kinectFirstAttempt()

cwd=miscFns.extractDirPath(mfilename('fullpath'));

opts.imgList=[cwd '../../data/grabCutLists/kinectData_firstAttempt.txt'];
opts.imgDir=['/data2/varun/iccv2011/data/kinectData/firstAttempt/'];
opts.labelDir=['/data2/varun/iccv2011/data/boxes_kinectData/firstAttempt/'];
