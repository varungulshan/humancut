function opts=getTests_grabCut_h3dset()

cwd=miscFns.extractDirPath(mfilename('fullpath'));

%opts.imgList=[cwd '../../../../software/h3d_version1.0/data/people/training_list.txt'];
opts.imgList=[cwd '../../data/grabCutLists/smallTest_h3d.txt'];
opts.imgDir=[cwd '../../../../software/h3d_version1.0/data/people/images/'];
opts.labelDir=[cwd '../../data/grabCut_boxLabels/h3d/'];
