function computeStats(resDir,evalOpts_string)
% Computes segmentation scores for a particular dir
% extra arguments which are redundant at the moment!)

  opts=testBench.evalOpts(evalOpts_string);
  if(~exist(resDir,'dir')), error('results dir %s does not exist\n',resDir); end
  fH=fopen([resDir 'evalOpts.txt'],'w');
  miscFns.printStructure(opts,fH);
  fclose(fH);

  [videoPaths,videoFiles]=eval(opts.datasetCmd);
  videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);

  gStats.evalOpts_string=evalOpts_string;
  gStats.opts=opts;
  gStats.resDir=resDir;
  stats=struct([]);
  maxFrames=0;
  for i=1:length(videoNames)
    curResDir=[resDir videoNames{i} '/'];
    [frameNums,scores]=evalVideo(curResDir,videoNames{i},opts); 
    maxFrames=max(maxFrames,max(frameNums));
    stats(i).frameNums=frameNums;
    stats(i).scores=scores;
    stats(i).videoName=videoNames{i};
  end
  gStats.stats=stats;
  gStats.maxFrames=maxFrames;
  printStats([resDir 'stats.txt'],gStats,opts);
  save([resDir 'stats.mat'],'gStats');

function printStats(statsFile,gStats,opts)
fH=fopen(statsFile,'w');
fprintf(fH,'Reporting segmentation scores on dir: %s\nEvaluation string: %s\n\n',...
        gStats.resDir,gStats.evalOpts_string);
stats=gStats.stats;
switch(opts.singleScore)
  case 'areaUnderCurve'
    [avgArea,stdErrArea,avgLength,stdLength]=testBench.getSingleScore_area(stats);
    fprintf(fH,'Avg. area under curve: %.2f stdErr %.3f (units of overlap score)\n',avgArea,stdErrArea);
    fprintf(fH,'Avg. length of sequence: %.2f stdDev %.2f frames\n',avgLength,stdLength);
    fprintf(fH,'\nNote that the area under curve is divided by the length of the sequence.\nIts units are units of overlap score.\n\n');
  otherwise
    fprintf(fH,'Found invalid single score string: %s\n\n',opts.singleScore);
end
for i=1:length(stats)
  fprintf(fH,'Video name: %s\n',stats(i).videoName);
  fprintf(fH,'%20s:','Frame number');
  for j=1:length(stats(i).frameNums)
    fprintf(fH,' %5s ',sprintf('%05d',stats(i).frameNums(j)));
  end
  fprintf(fH,'\n');
  fprintf(fH,'%20s:',gStats.opts.scoreString);
  for j=1:length(stats(i).scores)
    fprintf(fH,' %5s ',sprintf('%.2f',stats(i).scores(j)));
  end
  fprintf(fH,'\n');
  fprintf(fH,'%20s:','Area under curve');
  [area,L]=testBench.getAreaUnder_curve(stats(i).frameNums,stats(i).scores);
  fprintf(fH,' %5s ',sprintf('%.2f',area));
  fprintf(fH,'\n\n');
end
fclose(fH);
  
function [fNums,scores]=evalVideo(resDir,videoName,opts)
gtDir=[opts.gtDir videoName '/'];
segFile=[resDir 'seg.mat'];
load(segFile); % provides variable seg

fNums=[];scores=[];
for i=1:size(seg,3)
  gtFile=[gtDir sprintf('%05d.png',i)];
  if(exist(gtFile,'file')),
    fNums(end+1)=i;
    gt=imread(gtFile);
    scores(end+1)=testBench.evaluateSeg(seg(:,:,i),gt,opts.scoreString);
  end
end
