function opts=evalOpts_humanCut(optsString)

cwd=miscFns.extractDirPath(mfilename('fullpath'));

switch(optsString)
  case 'try_proxy1'
    % Options for trying out the human proxy based evaluation
    opts.gtDir=[cwd '../../data/manualGT_humanCut/'];
    opts.scoreString='accuracyOverlap';
    opts.datasetCmd='testBench.getTests_proxy';
    opts.scores={'scoreFull'};

  case 'try_proxy2'
    % Options for trying out the human proxy based evaluation
    opts.gtDir=[cwd '../../data/manualGT_humanCut/'];
    opts.scoreString='accuracyOverlap';
    opts.datasetCmd='testBench.getTests_proxy2';
    opts.scores={'scoreFull'};

  case 'try_proxy2_single'
    % Options for trying out the human proxy based evaluation
    opts.gtDir=[cwd '../../data/manualGT_humanCut/'];
    opts.scoreString='accuracyOverlap';
    opts.datasetCmd='testBench.getTests_proxy2_single';
    opts.scores={'scoreFull'};
   
  case 'try1'
    opts.gtDir=[cwd '../../data/manualGT_humanCut/'];
    opts.scoreString='accuracyOverlap';
    opts.datasetCmd='testBench.getTests_20frames_humans';
    opts.scores={'scoreAll','scoreFull'};
    % 'scoreAll' means that all tracks are evaluated even if they dont
    % span entire video. Note that even though all tracks are evaluated,
    % only the best tracks are chosen (as many tracks as number of GT segmentation
    % available are chosen. We dont penalise having more tracks than GT segmentations
    % because our GT is incomplete right now.
    % If number of tracks less than GT, empty tracks are initialized and given 0  score.

    % 'scoreFull' means only evaluate tracks than span entire video
    % Again, care is taken if number of tracks exceeds number of GTs, then best 
    % tracks are chosen. If number of tracks is less then number of GTs, it is not
    % penalised. So this score essentially measures: 'given a detection that spans
    % entire video, how good is its segmentation quality' (note that even a detection
    % that spans an entire video could be actually false
    % in which case it will report 0 segmentation then).
   otherwise
    error('Invalid option string %s\n',optsString);
end
