function [avgArea,stdErrArea,avgL,stdDevL]=getSingleScore_area(stats)
% Function to compute a single score for video segmentation algorithm on a dataset.
% the single score is based on area under overlap score vs time curve
% Inputs:
% stats -> structure obtained in computeStats function 
% 
% For every entry in stats (which corresponds to segmentation of a particular video),
% it computes area under overlap score vs time curve, normalizes the area by the length
% of the sequence. Then it averages this quantity over all the videos to get avgArea and
% stdError of Area. 
% avgL and stdL measure the average and std-dev of the length of the sequences
% This function makes most sense only when lenght of the sequences is the same, it
% is not so good if you average one long sequence with one short sequence

areas=zeros(length(stats),1);
lengths=zeros(length(stats),1);
N=length(stats);

for i=1:length(stats)
  [areas(i),lengths(i)]=testBench.getAreaUnder_curve(stats(i).frameNums,stats(i).scores);
end

avgArea=mean(areas);
stdErrArea=std(areas,1)/sqrt(N);
avgL=mean(lengths);
stdDevL=std(lengths,1);
