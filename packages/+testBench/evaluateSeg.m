function score=evaluateSeg(seg,gtSeg,scoreString)

if(size(gtSeg,1)~=size(seg,1) | size(gtSeg,2)~=size(seg,2))
  fprintf('Ground truth seg not of same size as orig image\n');error('As above\n');
end;

if(~exist('scoreString','var')),
  scoreString='accuracyOverlap';
end

evalMask=(gtSeg~=128);

tpMask=(seg==255)&(gtSeg==255)&evalMask;
tp=sum(tpMask(:));
clear tpMask;
tnMask=(seg==0)&(gtSeg==0)&evalMask;
tn=sum(tnMask(:));
clear tnMask;
fpMask=(seg==255)&(gtSeg==0)&evalMask;
fp=sum(fpMask(:));
clear fpMask;
fnMask=(seg==0)&(gtSeg==255)&evalMask;
fn=sum(fnMask(:));
clear fnMask;

switch(scoreString)
  case 'accuracyOverlap'
    score=100*tp/(tp+fn+fp);
  case 'accuracyHamming'
    score=(tn+tp)*100/(tn+tp+fn+fp);
  otherwise
    error('Invalid score string: %s\n',scoreString);
end

