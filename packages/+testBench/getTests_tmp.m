function [paths,files]=getTests_tmp()
% Scratch file for testing on specific videos

paths=cell(0,1);
files=cell(0,1);

%paths{end+1}='../../data/tinyTests/seinfield_1-1to3/';
%files{end+1}='00001.png';

%paths{end+1}='../../data/snapCut_videos/footballer_cut2/';
%files{end+1}='00001.png';

%paths{end+1}='../../data/cuts/gandhi_2-1to21/';
%files{end+1}='00001.png';

%paths{end+1}='../../data/cuts/my_cousin_vinny_1-1to21/';
%files{end+1}='00001.png';

%paths{end+1}='../../data/cuts/vinny_biologicalClock_small-1to21/';
%files{end+1}='00001.png';
%
%paths{end+1}='../../data/cuts/gandhi_1-1to21/';
%files{end+1}='00001.png';
%
%paths{end+1}='../../data/cuts/seinfield_1-1to21/';
%files{end+1}='00001.png';
%
%paths{end+1}='../../data/cuts/seinfield_2-1to21/';
%files{end+1}='00001.png';

%paths{end+1}='../../data/cuts/seinfield_3-1to21/';
%files{end+1}='00001.png';

%paths{end+1}='../../data/cuts/seinfield_4-1to21/';
%files{end+1}='00001.png';

%paths{end+1}='../../data/cuts/seinfield_5-1to21/';
%files{end+1}='00001.png';
%
%paths{end+1}='../../data/cuts/seinfield_6-1to21/';
%files{end+1}='00001.png';
%
paths{end+1}='../../data/cuts/seinfield_7-1to21/';
files{end+1}='00001.png';

%paths{end+1}='../../data/cuts/seinfield_8-1to21/';
%files{end+1}='00001.png';

%paths{end+1}='../../data/cuts/seinfield_9-1to21/';
%files{end+1}='00001.png';
%
%paths{end+1}='../../data/cuts/seinfield_10-1to21/';
%files{end+1}='00001.png';

%paths{end+1}='../../data/myVideos/';
%files{end+1}='seinfield_1.avi';
%
%paths{end+1}='../../data/myVideos/';
%files{end+1}='seinfield_2.avi';
%
%paths{end+1}='../../data/myVideos/';
%files{end+1}='seinfield_3.avi';
%
%paths{end+1}='../../data/myVideos/';
%files{end+1}='seinfield_4.avi';
%
%paths{end+1}='../../data/myVideos/';
%files{end+1}='seinfield_5.avi';
%
%paths{end+1}='../../data/myVideos/';
%files{end+1}='seinfield_6.avi';
%
%paths{end+1}='../../data/myVideos/';
%files{end+1}='seinfield_7.avi';
%
%paths{end+1}='../../data/myVideos/';
%files{end+1}='seinfield_8.avi';
%
%paths{end+1}='../../data/myVideos/';
%files{end+1}='seinfield_9.avi';
%
%paths{end+1}='../../data/myVideos/';
%files{end+1}='seinfield_10.avi';

cwd=miscFns.extractDirPath(mfilename('fullpath'));
for i=1:length(paths)
  paths{i}=[cwd paths{i}];
end
