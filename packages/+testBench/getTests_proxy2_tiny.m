function [paths,files]=getTests_proxy()
% Creating a new test bench on 04 Oct 2010
% videos for testing out human proxy based segmentation
% Removed videos with major occlusions of the person

paths=cell(0,1);
files=cell(0,1);

paths{end+1}='../../data/tinyTests_h3d/seinfield_1-1to21-1to3/';
files{end+1}='00001.png';

paths{end+1}='../../data/tinyTests_h3d/seinfield_3-1to21-1to3/';
files{end+1}='00001.png';
%
paths{end+1}='../../data/tinyTests_h3d/seinfield_4-1to21-1to3/';
files{end+1}='00001.png';

paths{end+1}='../../data/tinyTests_h3d/seinfield_5-1to21-1to3/';
files{end+1}='00001.png';

paths{end+1}='../../data/tinyTests_h3d/seinfield_6-1to21-1to3/';
files{end+1}='00001.png';

paths{end+1}='../../data/tinyTests_h3d/seinfield_7-1to21-1to3/';
files{end+1}='00001.png';

paths{end+1}='../../data/tinyTests_h3d/seinfield_8-1to21-1to3/';
files{end+1}='00001.png';

paths{end+1}='../../data/tinyTests_h3d/seinfield_9-1to21-1to3/';
files{end+1}='00001.png';

paths{end+1}='../../data/tinyTests_h3d/seinfield_10-1to21-1to3/';
files{end+1}='00001.png';

paths{end+1}='../../data/tinyTests_h3d/buffy_1-1to21-1to3/';
files{end+1}='00001.png';

paths{end+1}='../../data/tinyTests_h3d/buffy_3-1to21-1to3/';
files{end+1}='00001.png';

paths{end+1}='../../data/tinyTests_h3d/my_cousin_vinny_1-1to21-1to3/';
files{end+1}='00001.png';

paths{end+1}='../../data/tinyTests_h3d/gandhi_2-1to21-1to3/';
files{end+1}='00001.png';


cwd=miscFns.extractDirPath(mfilename('fullpath'));
for i=1:length(paths)
  paths{i}=[cwd paths{i}];
end
