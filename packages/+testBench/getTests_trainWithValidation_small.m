function opts=getTests_grabCut_kinect10Feb2011_2_train_1()

cwd=miscFns.extractDirPath(mfilename('fullpath'));

opts.trainTestBenchCmds{1} = ...
    'testBench.getTests_grabCut_small()';
opts.trainTestBenchCmds{2} = ...
    'testBench.getTests_grabCut_small()';

opts.validateTestBenchCmds{1} = ...
    'testBench.getTests_grabCut_small()';
opts.validateTestBenchCmds{2} = ...
    'testBench.getTests_grabCut_small()';

opts.fullTrainTestBenchCmd = 'testBench.getTests_grabCut_small()';
