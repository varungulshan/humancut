function opts=getTests_h3d2()

cwd=miscFns.extractDirPath(mfilename('fullpath'));

opts.imgList=[cwd '../../../../software/h3d_version1.0/data/people/training_list.txt'];
opts.imgDir=[cwd '../../../../software/h3d_version1.0/data/people/images/'];
opts.annoDir=[cwd '../../../../software/h3d_version1.0/data/people/info/'];
