function [paths,files]=getTests_all()

paths=cell(0,1);
files=cell(0,1);

paths{end+1}='../../data/myVideos/';
files{end+1}='buffy_s5e02_1.avi';

paths{end+1}='../../data/myVideos/';
files{end+1}='buffy_s5e03_2.avi';

paths{end+1}='../../data/myVideos/';
files{end+1}='buffy_s5e10_1.avi';

paths{end+1}='../../data/myVideos/withBlackBars/';
files{end+1}='love_actually_1-withBlackBars.avi';

paths{end+1}='../../data/myVideos/withBlackBars/';
files{end+1}='my_cousin_vinny_1-withBlackBars.avi';

paths{end+1}='../../data/snapCut_videos/';
files{end+1}='horse.mov';

paths{end+1}='../../data/snapCut_videos/';
files{end+1}='surfer.avi';

paths{end+1}='../../data/snapCut_videos/';
files{end+1}='maninhat.mov';

paths{end+1}='../../data/snapCut_videos/';
files{end+1}='gym.mov';

paths{end+1}='../../data/snapCut_videos/footballer/';
files{end+1}='00001.png';

cwd=miscFns.extractDirPath(mfilename('fullpath'));
for i=1:length(paths)
  paths{i}=[cwd paths{i}];
end
