function opts=getTests_grabCut_h3dset()

cwd=miscFns.extractDirPath(mfilename('fullpath'));

opts.imgList=[cwd '../../data/grabCutLists/h3d_train_1.txt'];
opts.imgDir=[cwd '../../../../software/h3d_version1.0/data/people/images/'];
opts.labelDir=[cwd '../../data/grabCut_boxLabels/h3d/'];
