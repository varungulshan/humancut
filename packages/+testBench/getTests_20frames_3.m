function [paths,files]=getTests_20frames_3()
% Creating a new test bench on 19 Jul 2010
% removing the hard buffy and footballer video and adding new videos

paths=cell(0,1);
files=cell(0,1);

paths{end+1}='../../data/cuts/love_actually_1-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/surfer-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/buffy-s5e02_1-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/maninhat-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/buffy-s5e03_2-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/gym-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/my_cousin_vinny_1-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/horse-1to21/';
files{end+1}='00001.png';

% --- new videos added below ---------
paths{end+1}='../../data/cuts/vinny_biologicalClock_small-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/gandhi_1-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/seinfield_1-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/seinfield_2-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/seinfield_3-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/seinfield_4-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/seinfield_5-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/seinfield_6-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/seinfield_7-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/seinfield_8-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/seinfield_9-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/seinfield_10-1to21/';
files{end+1}='00001.png';

cwd=miscFns.extractDirPath(mfilename('fullpath'));
for i=1:length(paths)
  paths{i}=[cwd paths{i}];
end
