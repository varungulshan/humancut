function plotStats_many(resDirs,methodNames)

% Variation of plotStats, plots different methods side by side
labelFont_size=30;

if(~exist('methodNames','var')),
  for i=1:length(resDirs)
    methodNames{i}=sprintf('Method%02d',i);
  end
end

evalOpts_string='';
prev_validFnums=[];

for j=1:length(resDirs)
  resDir=resDirs{j};
  methodName=methodNames{j};
  statsFile=[resDir 'stats.mat'];
  load(statsFile); % Provides gStats
  
  % Check consistency
  if(isempty(evalOpts_string)),
    evalOpts_string=gStats.evalOpts_string;
  else
    if(~strcmp(evalOpts_string,gStats.evalOpts_string))
      error('Evaluation string %s is inconsisten with original evaluation string %s\n',... 
             gStats.evalOpts_string,evalOpts_string);
    end
  end
  maxFrames=gStats.maxFrames;
  numVideos=length(gStats.stats);
  
  maskValid=false(numVideos,maxFrames);
  scores=zeros(numVideos,maxFrames);
  
  stats=gStats.stats;
  for i=1:numVideos
    frameNums=stats(i).frameNums;
    scores(i,frameNums)=stats(i).scores;
    maskValid(i,frameNums)=true;
  end
  
  mask_frameNum=any(maskValid,1);
  valid_fNums=find(mask_frameNum); % should be sorted
  means=zeros(size(valid_fNums));
  stdErrs=zeros(size(valid_fNums));
  
  for i=1:length(valid_fNums)
    frameNum=valid_fNums(i);
    validScores=scores(maskValid(:,frameNum),frameNum);
    numScores=length(validScores);
    means(i)=mean(validScores);
    stdErrs(i)=std(validScores,1)/sqrt(numScores);
  end
  
  if(j>1)
    if(~all(valid_fNums==prev_validFnums))
      error('Inconsistency in aggregating results\n');
    end
  else
    prev_validFnums=valid_fNums;
  end
  allMeans(j,:)=means;
  allStdErrs(j,:)=stdErrs;
end
% --- Printing out some info -----------
fprintf('Processed %d videos\n',length(stats));
for i=1:length(stats)
  fprintf('Video %d: %s\n',i,stats(i).videoName);
end
% ------- end printing info ---------

plotH=testBench.plotBars_withErrs(valid_fNums,allMeans,allStdErrs);

xlabel('Frame number','fontsize',labelFont_size);
ylabel(gStats.opts.scoreString,'fontsize',labelFont_size);
title(['Evaluation on ' evalOpts_string],'fontsize',labelFont_size);
cwd=miscFns.extractDirPath(mfilename('fullpath'));
load([cwd 'position_plot']); % Provides pos
set(gcf,'outerposition',pos);
legend(methodNames);
%cmd=['print -dpng ' resDir 'plot.png'];
%eval(cmd);
