function [paths,files]=getTests_20frames_ps()
% Creating a new test bench on 19 Jul 2010
% some videos on which to test pictorial structures code on

paths=cell(0,1);
files=cell(0,1);

paths{end+1}='../../data/cuts/buffy_1-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/buffy_3-1to21/';
files{end+1}='00001.png';

% --- new videos added below ---------
paths{end+1}='../../data/cuts/gandhi_2-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/seinfield_3-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/seinfield_4-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/seinfield_6-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/seinfield_7-1to21/';
files{end+1}='00001.png';

paths{end+1}='../../data/cuts/seinfield_9-1to21/';
files{end+1}='00001.png';

cwd=miscFns.extractDirPath(mfilename('fullpath'));
for i=1:length(paths)
  paths{i}=[cwd paths{i}];
end
