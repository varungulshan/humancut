function plotStats_byVideo2(resDir)
% Shows performance on per video basis
% draws line plots instead of bar graphs. Line plots are better to see

labelFont_size=30;

statsFile=[resDir 'stats.mat'];
load(statsFile); % Provides gStats

maxFrames=gStats.maxFrames;
numVideos=length(gStats.stats);

maskValid=false(numVideos,maxFrames);
scores=zeros(numVideos,maxFrames);
perfByVideo=zeros(numVideos,1);
videoNames=cell(numVideos,1);

stats=gStats.stats;
for i=1:numVideos
  frameNums=stats(i).frameNums;
  scores(i,frameNums)=stats(i).scores;
  maskValid(i,frameNums)=true;
  perfByVideo(i)=mean(stats(i).scores);
  videoNames{i}=stats(i).videoName;
end

[perfByVideo,sortIds]=sort(perfByVideo,'descend');
scores=scores(sortIds,:);
stats=stats(sortIds);
maskValid=maskValid(sortIds,:);
videoNames=videoNames(sortIds);

fprintf('Sorted by mean score in descending order:\n');
for i=1:numVideos
  fprintf('%03d: %s\n',i,stats(i).videoName);
end

colors=colorcube(3*numVideos);
colors=colors([1:3:end],:);
plotH=testBench.plotLines_perVideo(scores,maskValid,colors);

xlabel('Frame number','fontsize',labelFont_size);
ylabel(gStats.opts.scoreString,'fontsize',labelFont_size);
evalOpts_string=gStats.evalOpts_string;
title(['Evaluation on ' evalOpts_string],'fontsize',labelFont_size);
cwd=miscFns.extractDirPath(mfilename('fullpath'));
load([cwd 'position_plot']); % Provides pos
set(gcf,'outerposition',pos);
legend(videoNames,'interpreter','none');
%cmd=['print -dpng ' resDir 'plot.png'];
%eval(cmd);
