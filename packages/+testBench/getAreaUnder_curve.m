function [area,L]=getAreaUnder_curve(frameNums,scores)
% Computes area under score vs. frameNum curve (using linear interpolation)

frameNums=frameNums(:);scores=scores(:);
frameNumsShift=[1;frameNums(1:(end-1))];
trapezBase=frameNums-frameNumsShift;

scoresShift=[scores(1);scores(1:(end-1))];

area=sum(0.5*trapezBase.*(scoresShift+scores));
L=frameNums(end)-1;
area=area/L;
