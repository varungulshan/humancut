function [paths,files]=getTests_proxy()
% Creating a new test bench on 04 Oct 2010
% videos for testing out human proxy based segmentation
% Removed videos with major occlusions of the person

paths=cell(0,1);
files=cell(0,1);

paths{end+1}='../../data/cuts/seinfield_4-1to21/';
files{end+1}='00001.png';

cwd=miscFns.extractDirPath(mfilename('fullpath'));
for i=1:length(paths)
  paths{i}=[cwd paths{i}];
end
