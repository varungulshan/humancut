function opts=getTests_h3d3()

cwd=miscFns.extractDirPath(mfilename('fullpath'));

%opts.imgList=[cwd '../../../../software/h3d_version1.0/data/people/training_list.txt'];
%opts.imgList=[cwd '../../data/h3d_useCaseLists/type1And2_type1.txt'];
opts.imgList=[cwd '../../data/h3d_useCaseLists/tmp.txt'];
opts.imgDir=[cwd '../../../../software/h3d_version1.0/data/people/images/'];
opts.annoDir=[cwd '../../../../software/h3d_version1.0/data/people/info/'];
