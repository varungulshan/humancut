function [paths,files]=getTests_bird()

paths=cell(0,1);
files=cell(0,1);

paths{end+1}='../../data/birds/bird1/';
files{end+1}='00001.bmp';

cwd=miscFns.extractDirPath(mfilename('fullpath'));
for i=1:length(paths)
  paths{i}=[cwd paths{i}];
end
