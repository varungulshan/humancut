function [paths,files]=getTests_1()

paths=cell(0,1);
files=cell(0,1);

paths{end+1}='../../data/cuts/vinny_biologicalClock_small-1to243/';
files{end+1}='00001.png';
%paths{end+1}='../../data/cuts/maninhat-1to21/';
%files{end+1}='00001.png';

cwd=miscFns.extractDirPath(mfilename('fullpath'));
for i=1:length(paths)
  paths{i}=[cwd paths{i}];
end
