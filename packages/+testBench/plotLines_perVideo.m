function plotH=plotLines_perVideo(scores,maskValid,colors)
% Plots a linePlot of performance for each video
% scores -> [M x N] matrix of performances, M=num of videos, N=max number of frames
% maskValid -> M x N logical matrix, entry denotes whether segmentation was evaluated
%              at that frame or not.
% colors -> colors for line plots

% ---- Internal settings ------------
lineWidth=3;
markerSize=7;
lineStyle='--';
markers={'o','s','d','p'};
xTicks=[1 5 11 16 21];
ylimits=[70 100]; % leave empty for default

colors_default=[0 179 91;255 23 27;  96 192 255;255 201 54;137 113 218]/255;

if(~exist('colors','var')),
  colors=repmat(colors_default,[10 1]);
end
% ----- End internal settings --------

figure;

hold on;
nMarkers=length(markers);
xLabels=[1:size(scores,2)];
for i=1:size(scores,1)
  valid=maskValid(i,:);
  x=xLabels(valid);
  y=scores(i,valid);
  plot(x,y,'lineStyle',lineStyle,'marker',markers{mod((i-1),nMarkers)+1},...
      'markerEdgeColor',colors(i,:),'markerFaceColor',colors(i,:),...
      'markerSize',markerSize,'linewidth',lineWidth,...
      'color',colors(i,:));
end
hold off;

numX=size(xTicks,2);
set(gca,'xtick',xTicks);
for i=1:numX
  xLabels_string{i}=num2str(xTicks(i));
end
if(~isempty(ylimits))
  ylim(ylimits);
end
set(gca,'xticklabel',xLabels_string);
plotH=gca;
