classdef flowPropagatorEdgeLearn1 < videoSegmenter
% Uses a pre-processed edge map for graph cut edges
% The pre-processed edge map is thresholded at edgeThresh to give a 
% binary edge map
% No edge learning actually happens, I have put that in other classes
% (see flowPropagatorEdgeLearn2 and so on)
% SEEMS LIKE THIS CLASS IS REDUNDANT, is same as flowPropagatorEdgeMap 
% i left coding this in between and when i resumed i build flowPropagatorEdgeLearn2!!
  properties (SetAccess=private, GetAccess=public)
    gamma
    debugLevel
    flowDir
    edgeDir
    gtFile % gt for first frame
    vH
    gtSeg_firstFrame
    h
    w
    nCh
    nFrames
    state % 'initialized','pped','segStarted'
    seg % uint8, seg variable for the entire volume
    frameNum
    framesSegmented
    video % only if debugLevel > 0
    gcOpts % contains edgeThresh, gamma, gamma_ising, unaryConstant, gcScale, nbrHoodType
    roffset %int32
    coffset %int32
  end

  methods
    function obj=flowPropagatorEdgeLearn1(debugLevel,flowDir,edgeDir,gtFile,vH,gcOpts)
      obj.debugLevel=debugLevel;
      obj.flowDir=flowDir;
      obj.edgeDir=edgeDir;
      obj.gtFile=gtFile; 
      obj.vH=vH;
      [obj.h obj.w obj.nCh obj.nFrames]=obj.vH.videoSize();
      obj.state='initialized';
      obj.seg=zeros([obj.h obj.w obj.nFrames],'uint8');
      obj.framesSegmented=0;
      obj.frameNum=1;
      obj.video=[];
      obj.gcOpts=gcOpts;
      beta=[];
    end
  end

  methods (Access=private)
    flowVector=getNextFlow(obj)
    %beta=getBetaAuto_GC(obj)
    preProcess_GC(obj)
    seg=graphCutSeg(obj,flowSeg,labels)
  end

end % of classdef
