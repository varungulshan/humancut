function preProcess_GC(obj)

switch(obj.gcOpts.nbrHoodType)
  case 'nbr4'
    roffset = int32([ 1,  0 ]);
    coffset = int32([  0, 1 ]);
  case 'nbr8'
    roffset = int32([ 1, 1, 0,-1 ]);
    coffset = int32([ 0, 1, 1, 1 ]);
  otherwise
    error('Invalid nbrhood type %s\n',obj.gcOpts.nbrHoodType);
end

obj.roffset=roffset;
obj.coffset=coffset;

