% Copyright (C) 2010 Varun Gulshan
% This class defines the options for grabCut segmentation
classdef segOpts
  properties (SetAccess=public, GetAccess=public)
    gcGamma_e  % gamma of graph cuts (for contrast dependant term)
    gcGamma_i  % gamma of graph cuts (for ising model term)
    gcScale  % scaling to convert to integers
    gcNbrType % 'nbr4' or 'nbr8'
    gcSigma_c % 'auto' or a numeric value

    gmmNmix_fg % number of gmm mixtures for fg, usually 5
    gmmNmix_bg % number of gmm mixtures for bg, usually 5
    gmmUpdate_iters % number of iterations to update GMM in the EM algorithm

    postProcess % 0 = off, 1 = on
    numIters % Number of iterations to run grabCut for

    uniform_gamma
    uniform_value
    unaryBound

    wtShape % Decide weighing on unary terms, trading off shape vs. color. 
    % shape posterior is given wtShape, and color posterior is given
    % weight of (1-wtShape). 
    numIters_newProxy % number of iterations to run when the proxy is updated
  end

  methods
    function obj=segOpts()
      % Set the default options
      obj.gcGamma_e=140;
      obj.gcGamma_i=5;
      obj.gcScale=500;
      obj.gcNbrType='nbr8';
      obj.gcSigma_c='auto';

      obj.gmmNmix_fg=5;
      obj.gmmNmix_bg=5;
      obj.postProcess=0;
      obj.numIters=10;
      
      obj.gmmUpdate_iters=1;

      obj.uniform_gamma=0.05;
      obj.uniform_value=1; % if image lies between [0,1]
      obj.unaryBound=obj.gcScale*(obj.gcGamma_e+obj.gcGamma_i)*100000;

      obj.wtShape=0.5;
      obj.numIters_newProxy=10;
    end
  end

end % of classdef
