function delete(obj)
% Destructor function, needs to clear the dynamic graph cut handle
if(~isempty(obj.graphInfo))
  grabCut_humanModel.cpp.mex_dgc_cleanUp(obj.graphInfo);
  obj.graphInfo=[];
end
