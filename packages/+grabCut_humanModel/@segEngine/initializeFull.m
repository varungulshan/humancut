function initializeFull(obj,img,hModel)

obj.preProcess(img);
obj.initColorModel_fromHumanModel(hModel);
obj.updateSeg(hModel);

if(obj.debugOpts.debugLevel>0)
  [h w nCh]=size(img);
  miscFns.saveDebug(obj.debugOpts,hModel.getShapePosterior([h w]),'initShapePosterior.png');
  posterior_color=obj.getColorPosterior();
  miscFns.saveDebug(obj.debugOpts,posterior_color,sprintf('iter%03d_colorUnaries.jpg',1));
end

for i=2:obj.opts.numIters
  obj.updateColorModel(); % updates it using current segmentation, and stored labelImg 
  % the stored labelImg is a function of hModel
  obj.updateSeg(hModel); % update segmentation using current color model
                         % and the passed human model
  if(obj.debugOpts.debugLevel>0)
    posterior_color=obj.getColorPosterior();
    miscFns.saveDebug(obj.debugOpts,posterior_color,sprintf('iter%03d_colorUnaries.jpg',i));
  end
end
