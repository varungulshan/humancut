function energy=getEnergy_fixedSeg(obj,hModel)

[h w nCh]=size(obj.img);
opts=obj.opts;

unaryImg_color=obj.unaryImg_color;
shapePosterior=hModel.getShapePosterior([h w]);

unaryImg_shape=zeros([h w 2]);
myUB=opts.unaryBound;
tmp=-log(shapePosterior);
tmp(tmp>myUB)=myUB;
unaryImg_shape(:,:,1)=tmp;
tmp=-log(1-shapePosterior);
tmp(tmp>myUB)=myUB;
unaryImg_shape(:,:,2)=tmp;

unaryImg=opts.wtShape*unaryImg_shape+(1-opts.wtShape)*unaryImg_color;
energy=obj.computeEnergy(obj.seg,unaryImg,obj.lEdges,obj.rEdges,obj.edgeWeights);
