function unaryImg=getColorUnary(obj)

opts=obj.opts;
fgLikeli=gmm.computeGmm_likelihood(obj.features,obj.gmmf);
bgLikeli=gmm.computeGmm_likelihood(obj.features,obj.gmmb);

fgLikeli=opts.uniform_gamma*opts.uniform_value+(1-opts.uniform_gamma)*fgLikeli;
bgLikeli=opts.uniform_gamma*opts.uniform_value+(1-opts.uniform_gamma)*bgLikeli;

fgLikeli=-log(fgLikeli);
bgLikeli=-log(bgLikeli);
myUB=opts.unaryBound;
fgLikeli(fgLikeli>myUB)=myUB;
bgLikeli(bgLikeli>myUB)=myUB;

[h w nCh]=size(obj.img);
fgLikeli=reshape(fgLikeli,[h w]);
bgLikeli=reshape(bgLikeli,[h w]);

unaryImg=zeros([h w 2]);
unaryImg(:,:,1)=fgLikeli;
unaryImg(:,:,2)=bgLikeli;
