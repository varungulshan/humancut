function preProcess(obj,img)
  if(~strcmp(obj.state,'created')),
    error('Cant call preProcess from %s state\n',obj.state);
    return;
  end

  if(~strcmp(class(img),'double')),
    error('img should be of type double\n');
  end

  preProcess_GC(obj,img);
  obj.features=grabCut_humanModel.extractPixels(img);
  obj.img=img;
  obj.state='pped'; 

function preProcess_GC(obj,img)

switch(obj.opts.gcNbrType)
  case 'nbr4'
    roffset = int32([ 1,  0 ]);
    coffset = int32([  0, 1 ]);
  case 'nbr8'
    roffset = int32([ 1, 1, 0,-1 ]);
    coffset = int32([ 0, 1, 1, 1 ]);
  otherwise
    error('Invalid nbrhood type %s\n',obj.opts.nbrHoodType);
end

obj.roffset=roffset;
obj.coffset=coffset;

[lEdges,rEdges,colorWeights,spWeights]=grabCut_humanModel.cpp.mex_setupTransductionGraph(img,roffset',coffset');
if(isnumeric(obj.opts.gcSigma_c))
  D=size(img,3);
  obj.beta=1/(2*D*obj.opts.gcSigma_c^2);
elseif(strcmp(obj.opts.gcSigma_c,'auto'))
  obj.beta=1/(2*mean(colorWeights));
end

obj.lEdges=lEdges;
obj.rEdges=rEdges;
obj.edgeWeights=obj.opts.gcGamma_e*exp(-obj.beta*colorWeights)+...
                obj.opts.gcGamma_i*ones(size(colorWeights));
