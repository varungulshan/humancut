function updateColorModels(obj)

opts=obj.opts;
labelImg=obj.labelImg;
mask=(labelImg~=5 & labelImg~=6);
mask=mask(:);

tmpFeatures=obj.features(:,obj.seg(:)==255 & mask);
obj.gmmf=gmm.updateGmm(tmpFeatures,obj.gmmf,opts.gmmUpdate_iters);

tmpFeatures=obj.features(:,obj.seg(:)==0 & mask);
obj.gmmb=gmm.updateGmm(tmpFeatures,obj.gmmb,opts.gmmUpdate_iters);
