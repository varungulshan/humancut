function posterior=getColorPosterior(obj)

unaryImg=obj.unaryImg_color;
fgExp=exp(-unaryImg(:,:,1));
bgExp=exp(-unaryImg(:,:,2));

posterior=fgExp./(fgExp+bgExp);
