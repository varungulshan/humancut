function updateSeg_andColorModel(obj,hModel)

% First update segmentation, as the human model has changed.
obj.updateSeg(hModel); 

% Also need to update the labelImg as the human model has changed
[h w nCh]=size(obj.img);
obj.labelImg=hModel.getColorModel_initLabels('globalModels',[h w]);

for i=1:obj.opts.numIters_newProxy
  obj.updateColorModel();
  obj.updateSeg(hModel);
end
