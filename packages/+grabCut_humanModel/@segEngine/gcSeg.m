function seg=gcSeg(obj,unaryImg)

import grabCut_humanModel.cpp.*

objOpts=obj.opts;

opts.gcScale=objOpts.gcScale;

if(isempty(obj.graphInfo))
  [seg,flow,graphInfo]=mex_dgc_init(unaryImg,obj.lEdges,obj.rEdges,obj.edgeWeights,opts);
  obj.graphInfo=graphInfo;
else
  [seg,flow]=mex_dgc_repeat(unaryImg,obj.graphInfo,opts);
end
