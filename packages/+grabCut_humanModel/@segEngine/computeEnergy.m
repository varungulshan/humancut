function energy=computeEnergy(seg,unaryImg,lEdges,rEdges,edgeW)

energy=0;
mask=cat(3,seg==255,seg==0);

energy=energy+sum(unaryImg(mask));
energy=energy+sum(edgeW(seg(lEdges)~=seg(rEdges)));

assert(energy<Inf,'Energy is hitting limit of double, check!\n');
