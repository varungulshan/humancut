function opts=makeOpts(optsString)

switch(optsString)
   case 'iter10_2'
    opts=grabCut_humanModel.segOpts();
    opts.gcGamma_e=50;
    opts.gcGamma_i=0;

    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=5;
    opts.gmmUpdate_iters=1;

    opts.uniform_gamma=0.05;
    opts.uniform_value=1;
    opts.unaryBound=opts.gcScale*(opts.gcGamma_e+opts.gcGamma_i)*100000;

    opts.wtShape=0.5;
    opts.numIters_newProxy=5;

  otherwise
    error('Invalid options string %s\n',optsString);
end
