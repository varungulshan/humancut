function demo()

boxMargin=30;
maxDim = 640;
cwd=miscFns.extractDirPath(mfilename('fullpath'));
[filename,pathname]=uigetfile({'*.*'},'Open image to segment',[cwd '../../data/cuts/']);

img=im2double(imread([pathname filename]));
rescale = maxDim/max(size(img,1),size(img,2));
img = imresize(img,rescale);
figure;imshow(img);
fprintf('Draw bounding box\n');

k = waitforbuttonpress;
point1 = get(gca,'CurrentPoint');    % button down detected
finalRect = rbbox;                   % return figure units
point2 = get(gca,'CurrentPoint');    % button up detected
point1 = point1(1,1:2);              % extract x and y
point2 = point2(1,1:2);
p1 = round(min(point1,point2));             
p2 = round(max(point1,point2));

[h w nCh]=size(img);
labelImg=6*ones([h w],'uint8');

xT=max(1,p1(1)-boxMargin);yT=max(1,p1(2)-boxMargin);
xB=min(w,p2(1)+boxMargin);yB=min(h,p2(2)+boxMargin);
labelImg(yT:yB,xT:xB)=2;

xT=max(1,p1(1));yT=max(1,p1(2));
xB=min(w,p2(1));yB=min(h,p2(2));

labelImg(yT:yB,xT:xB)=3;

stTime=clock;
segOpts=localGrabCut_hogLearn.helpers.makeOpts('bestOpts_bmvc');
[hogLearnData,segOpts.hogLearnType] =...
        localGrabCut_hogLearn.loadHogLearnData('hogLearnLocal_position_run103');

debugOpts.debugLevel=1;
debugOpts.debugPrefix='demo';
debugOpts.debugDir=[cwd '../../junk/'];
segH=localGrabCut_hogLearn.segEngine(debugOpts,segOpts,hogLearnData);
segH.preProcess(img);
segH.start(labelImg);
fprintf('LocalGrabCut completed in %.2f seconds\n',etime(clock,stTime));

[segBoundaryMask,segBoundaryColors]= ...
miscFns.getSegBoundary_twoColors(segH.seg,[0 1 0],[1 0 0],2,2);
img(segBoundaryMask)=segBoundaryColors;

figure;imshow(img);
delete(segH);
