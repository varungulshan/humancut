function [data,learnType] = loadHogLearnData(optsString)

switch(optsString)
  case 'try1'
    learnType = 'hogLearnLocal2';
    data = grabCut_trained.learning.hogLearnLocal2.loadTrainingData(...
        '/data2/varun/iccv2011/results/train/run027/','');

  case 'hogLearnLocal_1'
    learnType = 'hogLearnLocal';
    data = grabCut_trained.learning.hogLearnLocal.loadTrainingData(...
        '/data2/varun/iccv2011/results/train/run015/','');

  case 'hogLearnLocal_2'
    learnType = 'hogLearnLocal';
    data = grabCut_trained.learning.hogLearnLocal.loadTrainingData(...
        '/data1/varun/iccv2011/results/train/run051/','');

  case 'hogLearnLocal_3'
    learnType = 'hogLearnLocal';
    data = grabCut_trained.learning.hogLearnLocal.loadTrainingData(...
        '/data1/varun/iccv2011/results/train/run065/bestTrain/','');

  case 'hogLearnLocal_bestOptions_1'
    learnType = 'hogLearnLocal';
    data = grabCut_trained.learning.hogLearnLocal.loadTrainingData(...
        '/data1/varun/iccv2011/results/train/run072/bestTrain/','');

  case 'hogLearnLocal_bestOptions_2'
    learnType = 'hogLearnLocal';
    data = grabCut_trained.learning.hogLearnLocal.loadTrainingData(...
        '/data1/varun/iccv2011/results/train/run083/bestTrain/','');

  case 'hogLearnLocal_bestOptions_3'
    learnType = 'hogLearnLocal';
    data = grabCut_trained.learning.hogLearnLocal.loadTrainingData(...
        '/data1/varun/iccv2011/results/train/run095/','');

  case 'hogLearnLocal_bestOptions_4'
    learnType = 'hogLearnLocal';
    data = grabCut_trained.learning.hogLearnLocal.loadTrainingData(...
        '/data1/varun/iccv2011/results/train/run092/','');

  case 'hogLearnLocal_position_run103'
    learnType = 'hogLearnLocal_position';
    data = grabCut_trained.learning.hogLearnLocal_position.loadTrainingData(...
        '/data1/varun/iccv2011/results/train/run103/bestTrain/','');

  case 'hogLearnLocal_position_run103_confidence'
    learnType = 'hogLearnLocal_position_confidence';
    data = grabCut_trained.learning.hogLearnLocal_position.loadTrainingData(...
        '/data1/varun/iccv2011/results/train/run103/bestTrain/','');

  case 'hogLearnLocal_position_run103_validate1'
    learnType = 'hogLearnLocal_position';
    data = grabCut_trained.learning.hogLearnLocal_position.loadTrainingData(...
        '/data1/varun/iccv2011/results/train/run103/validations/optsString_svm1_4levels_validateC2_foldNum_1/train/','');

  case 'hogLearnLocal_position_run103_validate2'
    learnType = 'hogLearnLocal_position';
    data = grabCut_trained.learning.hogLearnLocal_position.loadTrainingData(...
        '/data1/varun/iccv2011/results/train/run103/validations/optsString_svm1_4levels_validateC2_foldNum_2/train/','');

  case 'hogLearnLocal_position_run103_validate3'
    learnType = 'hogLearnLocal_position';
    data = grabCut_trained.learning.hogLearnLocal_position.loadTrainingData(...
        '/data1/varun/iccv2011/results/train/run103/validations/optsString_svm1_4levels_validateC2_foldNum_3/train/','');

  case 'hogLearnLocal_validate1'
    learnType = 'hogLearnLocal';
    data = grabCut_trained.learning.hogLearnLocal.loadTrainingData(...
        '/data1/varun/iccv2011/results/train/run065/validations/optsString_svm1_validateC_1_foldNum_1/train/','');

  case 'hogLearnLocal_validate2'
    learnType = 'hogLearnLocal';
    data = grabCut_trained.learning.hogLearnLocal.loadTrainingData(...
        '/data1/varun/iccv2011/results/train/run065/validations/optsString_svm1_validateC_1_foldNum_2/train/','');

  case 'hogLearnLocal_validate3'
    learnType = 'hogLearnLocal';
    data = grabCut_trained.learning.hogLearnLocal.loadTrainingData(...
        '/data1/varun/iccv2011/results/train/run065/validations/optsString_svm1_validateC_1_foldNum_3/train/','');

  otherwise
    error('HogLearnData option: %s is not supported\n',optsString);
end
