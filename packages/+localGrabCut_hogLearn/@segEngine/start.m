function start(obj,labelImg)

if(~strcmp(obj.state,'pped')),
  error('Can only call start if state is pped\n');
end
[h,w]=size(labelImg);

obj.state='started';
initSeg = preProcessHogOutput(obj,labelImg);
obj.labelImg=labelImg;

% Modify the labelImg for initialization
softInitMask = (labelImg==3 | labelImg==4);
labelImg(softInitMask & initSeg==255) = 3;
labelImg(softInitMask & initSeg==0) = 4;

seg=128*ones(size(labelImg),'uint8');
seg(labelImg==1 | labelImg==3 | labelImg==5)=255;
seg(labelImg==2 | labelImg==4 | labelImg==6)=0;
obj.seg=seg;

[unaryImg,unaryMask]=obj.createUnaryImg();
obj.seg=obj.gcSeg(unaryImg,unaryMask);

% One iteration is over already
for i=2:obj.opts.numIters
  obj.iterateOnce();
end

function initSeg = preProcessHogOutput(obj,labelImg)

opts = obj.opts;
switch(opts.hogLearnType)
  case 'hogLearnLocal'
    % Right now, hogLearnLocal does not output confidence masks
    initSeg = grabCut_trained.learning.hogLearnLocal.testOnImg(...
        obj.img,labelImg,obj.hogLearnData,obj.debugOpts);
    hogLearnOutput = ones(size(initSeg));
    hogLearnOutput(initSeg==0) = -1;
    obj.hogLearnOutput = opts.hogLearnUnary_rescale*hogLearnOutput;

  case 'hogLearnLocal_position'
    % Right now, hogLearnLocal does not output confidence masks
    initSeg = grabCut_trained.learning.hogLearnLocal_position.testOnImg(...
        obj.img,labelImg,obj.hogLearnData,obj.debugOpts);
    hogLearnOutput = ones(size(initSeg));
    hogLearnOutput(initSeg==0) = -1;
    obj.hogLearnOutput = opts.hogLearnUnary_rescale*hogLearnOutput;

  case 'hogLearnLocal_position_confidence'
    [tmpSeg,conf] = grabCut_trained.learning.hogLearnLocal_position.testOnImg(...
        obj.img,labelImg,obj.hogLearnData,obj.debugOpts);
    initSeg = zeros(size(tmpSeg),'uint8');
    initSeg(conf>0) = 255;
    hogLearnOutput = conf;
    obj.hogLearnOutput = opts.hogLearnUnary_rescale*hogLearnOutput;

  case 'hogLearnLocal2'
    [initSeg,hogLearnOutput] = grabCut_trained.learning.hogLearnLocal2.testOnImg(...
        obj.img,labelImg,obj.hogLearnData,obj.debugOpts);
    obj.hogLearnOutput = opts.hogLearnUnary_rescale*hogLearnOutput;
  case 'none'
    initSeg = zeros(size(labelImg),'uint8');
    initSeg(labelImg ==1 | labelImg==3|labelImg==5) = 255;
    obj.hogLearnOutput = zeros(size(initSeg));
end

if(opts.postProcessHogOutput)
  [components,numComponents]=bwlabel(logical(initSeg),...
      opts.postProcessConnectivity);
  compSizes = histc(components(:),[0:numComponents]); 
  if(numComponents>0)
    compSizes = compSizes(2:end);
    [maxSize,maxId] = max(compSizes);
    deleteMask = components~=maxId;
    initSeg(deleteMask)=0;
    obj.hogLearnOutput(deleteMask) = -1*opts.hogLearnUnary_rescale;
  end
end
