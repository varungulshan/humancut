function seg=gcSeg(obj,unaryImg,unaryMask)

import localGrabCut_hogLearn.cpp.*

opts=obj.opts;
beta=obj.beta;

[seg,flow]=mex_gcBand(unaryMask,unaryImg(:,:,1),unaryImg(:,:,2),opts.gcGamma_e,beta,...
                      obj.roffset',obj.coffset',opts.gcScale,opts.gcGamma_i,...
                      obj.img);
