function runOffline(img,labelImg)

img=im2double(img);
cwd=miscFns.extractDirPath(mfilename('fullpath'));

stTime=clock;
segOpts=localGrabCut_hogLearn.helpers.makeOpts('bestOpts_bmvc');
[hogLearnData,segOpts.hogLearnType] =...
        localGrabCut_hogLearn.loadHogLearnData('hogLearnLocal_position_run103');

debugOpts.debugLevel=1;
debugOpts.debugPrefix='demo';
debugOpts.debugDir=[cwd '../../junk/'];
segH=localGrabCut_hogLearn.segEngine(debugOpts,segOpts,hogLearnData);
segH.preProcess(img);
segH.start(labelImg);
fprintf('LocalGrabCut completed in %.2f seconds\n',etime(clock,stTime));

[segBoundaryMask,segBoundaryColors]= ...
miscFns.getSegBoundary_twoColors(segH.seg,[0 1 0],[1 0 0],2,2);
img(segBoundaryMask)=segBoundaryColors;

figure;imshow(img);
delete(segH);
