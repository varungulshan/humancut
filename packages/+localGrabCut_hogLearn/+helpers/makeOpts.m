function opts=makeOpts(optsString)

switch(optsString)

   case 'bestOpts_bmvc'
    opts=localGrabCut_hogLearn.segOpts();
    opts.gcGamma_e=20;
    opts.gcGamma_i=0;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=4;
    
    opts.min_validFraction=0.02;
    opts.localWin_rad=30;
    opts.hogLearnUnary_rescale = 1;
    opts.hogLearnType = 'hogLearnLocal';

   case 'iter10_global'
    opts=localGrabCut_hogLearn.segOpts();
    opts.gcGamma_e=50;
    opts.gcGamma_i=0;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=10;
    
    opts.min_validFraction=0.02;
    opts.localWin_rad=10000;
    opts.hogLearnUnary_rescale = 1;
    opts.hogLearnType = 'hogLearnLocal';

  case 'iter10_rad150'
    opts=localGrabCut_hogLearn.segOpts();
    opts.gcGamma_e=50;
    opts.gcGamma_i=0;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=10;
    
    opts.min_validFraction=0.02;
    opts.localWin_rad=150;
    opts.hogLearnUnary_rescale = 1;
    opts.hogLearnType = 'hogLearnLocal';

  case 'iter10_rad80'
    opts=localGrabCut_hogLearn.segOpts();
    opts.gcGamma_e=50;
    opts.gcGamma_i=0;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=10;
    
    opts.min_validFraction=0.02;
    opts.localWin_rad=80;
    opts.hogLearnUnary_rescale = 1;
    opts.hogLearnType = 'hogLearnLocal';

  case 'rad40_validateNew_1'
    opts=localGrabCut_hogLearn.segOpts();
    opts.gcGamma_e=25;
    opts.gcGamma_i=0;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=4;
    
    opts.min_validFraction=0.02;
    opts.localWin_rad=40;
    opts.hogLearnUnary_rescale = 0.2;
    opts.hogLearnType = 'hogLearnLocal';

  case 'rad40_validateNew_2'
    opts=localGrabCut_hogLearn.segOpts();
    opts.gcGamma_e=25;
    opts.gcGamma_i=0;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=4;
    
    opts.min_validFraction=0.02;
    opts.localWin_rad=40;
    opts.hogLearnUnary_rescale = 0.4;
    opts.hogLearnType = 'hogLearnLocal';

  case 'rad40_validateNew_3'
    opts=localGrabCut_hogLearn.segOpts();
    opts.gcGamma_e=25;
    opts.gcGamma_i=0;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=4;
    
    opts.min_validFraction=0.02;
    opts.localWin_rad=40;
    opts.hogLearnUnary_rescale = 0.6;
    opts.hogLearnType = 'hogLearnLocal';

  case 'rad40_validateNew_4'
    opts=localGrabCut_hogLearn.segOpts();
    opts.gcGamma_e=25;
    opts.gcGamma_i=0;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=4;
    
    opts.min_validFraction=0.02;
    opts.localWin_rad=40;
    opts.hogLearnUnary_rescale = 0.8;
    opts.hogLearnType = 'hogLearnLocal';

  case 'rad40_validate1'
    opts=localGrabCut_hogLearn.segOpts();
    opts.gcGamma_e=25;
    opts.gcGamma_i=0;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=10;
    
    opts.min_validFraction=0.02;
    opts.localWin_rad=40;
    opts.hogLearnUnary_rescale = 0;
    opts.hogLearnType = 'hogLearnLocal';

  case 'rad40_validate2'
    opts=localGrabCut_hogLearn.segOpts();
    opts.gcGamma_e=25;
    opts.gcGamma_i=0;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=10;
    
    opts.min_validFraction=0.02;
    opts.localWin_rad=40;
    opts.hogLearnUnary_rescale = 0.1;
    opts.hogLearnType = 'hogLearnLocal';

  case 'rad40_validate3'
    opts=localGrabCut_hogLearn.segOpts();
    opts.gcGamma_e=25;
    opts.gcGamma_i=0;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=10;
    
    opts.min_validFraction=0.02;
    opts.localWin_rad=40;
    opts.hogLearnUnary_rescale = 0.5;
    opts.hogLearnType = 'hogLearnLocal';

  case 'rad40_validate4'
    opts=localGrabCut_hogLearn.segOpts();
    opts.gcGamma_e=25;
    opts.gcGamma_i=0;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=10;
    
    opts.min_validFraction=0.02;
    opts.localWin_rad=40;
    opts.hogLearnUnary_rescale = 1;
    opts.hogLearnType = 'hogLearnLocal';

  case 'rad40_validate5'
    opts=localGrabCut_hogLearn.segOpts();
    opts.gcGamma_e=25;
    opts.gcGamma_i=0;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=10;
    
    opts.min_validFraction=0.02;
    opts.localWin_rad=40;
    opts.hogLearnUnary_rescale = 5;
    opts.hogLearnType = 'hogLearnLocal';

  case 'smallTest2'
    opts=localGrabCut_hogLearn.segOpts();
    opts.gcGamma_e=25;
    opts.gcGamma_i=0;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=1;
    
    opts.min_validFraction=0.02;
    opts.localWin_rad=40;
    opts.hogLearnUnary_rescale = 1;
    opts.hogLearnType = 'hogLearnLocal';
 
  case 'smallTest'
    opts=localGrabCut_hogLearn.segOpts();
    opts.gcGamma_e=25;
    opts.gcGamma_i=0;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=3;
    opts.gmmNmix_bg=3;
    opts.postProcess=0;
    opts.numIters=1;
    
    opts.min_validFraction=0.02;
    opts.localWin_rad=80;
    opts.hogLearnUnary_rescale = 1;
    opts.hogLearnType = 'hogLearnLocal';
                                                 
  otherwise
    error('Invalid options string %s\n',optsString);
end
