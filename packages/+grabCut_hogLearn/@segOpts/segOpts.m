% Copyright (C) 2010 Varun Gulshan
% This class defines the options for grabCut_hogLearn segmentation
classdef segOpts
  properties (SetAccess=public, GetAccess=public)
    gcGamma_e  % gamma of graph cuts (for contrast dependant term)
    gcGamma_i  % gamma of graph cuts (for ising model term)
    gcScale  % scaling to convert to integers
    gcNbrType % 'nbr4' or 'nbr8'
    gcSigma_c % 'auto' or a numeric value

    gmmNmix_fg % number of gmm mixtures for fg, usually 5
    gmmNmix_bg % number of gmm mixtures for bg, usually 5
    gmmUpdate_iters % number of iterations to update GMM in the EM algorithm

    postProcess % 0 = off, 1 = on
    numIters % Number of iterations to run grabCut_hogLearn for

    hogLearnUnary_rescale
    hogLearnType
  end

  methods
    function obj=segOpts()
      % Set the default options
      obj.gcGamma_e=140;
      obj.gcGamma_i=5;
      obj.gcScale=50;
      obj.gcNbrType='nbr8';
      obj.gcSigma_c='auto';

      obj.gmmNmix_fg=5;
      obj.gmmNmix_bg=5;
      obj.postProcess=0;
      obj.numIters=10;
      
      obj.gmmUpdate_iters=1;
      obj.hogLearnUnary_rescale = 1;
      obj.hogLearnType = 'hogLearnLocal2';
    end
  end

end % of classdef
