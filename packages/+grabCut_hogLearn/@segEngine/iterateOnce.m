function iterateOnce(obj)
% Run one iteration of grabCut_hogLearn
% first update color model, then update segmentation

if(~strcmp(obj.state,'started')),
  error('Can only call iterateOnce, once state is started\n');
end

labelImg=obj.labelImg;
ftrMask=(labelImg~=5 & labelImg~=6);
[obj.gmmf,obj.gmmb]=grabCut_hogLearn.updateColorModels(obj.features,obj.seg,obj.gmmf,obj.gmmb,...
                    obj.opts.gmmUpdate_iters,ftrMask);
[unaryImg,unaryMask]=obj.createUnaryImg();
obj.seg=obj.gcSeg(unaryImg,unaryMask);
