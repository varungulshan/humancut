function start(obj,labelImg)

if(~strcmp(obj.state,'pped')),
  error('Can only call start if state is pped\n');
end
[h,w]=size(labelImg);

obj.state='started';
initSeg = preProcessHogOutput(obj,labelImg);
obj.labelImg = labelImg;
% Modify the labelImg for initialization
softInitMask = (labelImg==3 | labelImg==4);
labelImg(softInitMask & initSeg==255) = 3;
labelImg(softInitMask & initSeg==0) = 4;
[obj.gmmf,obj.gmmb] = grabCut_hogLearn.initColorModels(obj.features,labelImg,obj.opts.gmmNmix_fg,...
                                            obj.opts.gmmNmix_bg);
[unaryImg,unaryMask]=obj.createUnaryImg();
obj.seg=obj.gcSeg(unaryImg,unaryMask);

% One iteration is over already
for i=2:obj.opts.numIters
  obj.iterateOnce();
end

function initSeg = preProcessHogOutput(obj,labelImg)

opts = obj.opts;
switch(opts.hogLearnType)
  case 'hogLearnLocal'
    % Right now, hogLearnLocal does not output confidence masks
    initSeg = grabCut_trained.learning.hogLearnLocal.testOnImg(...
        obj.img,labelImg,obj.hogLearnData,obj.debugOpts);
    hogLearnOutput = ones(size(initSeg));
    hogLearnOutput(initSeg==0) = -1;
    obj.hogLearnOutput = opts.hogLearnUnary_rescale*hogLearnOutput;
  case 'hogLearnLocal2'
    [initSeg,hogLearnOutput] = grabCut_trained.learning.hogLearnLocal2.testOnImg(...
        obj.img,labelImg,obj.hogLearnData,obj.debugOpts);
    obj.hogLearnOutput = opts.hogLearnUnary_rescale*hogLearnOutput;
  case 'hogLearnLocal_position'
    % Right now, hogLearnLocal does not output confidence masks
    initSeg = grabCut_trained.learning.hogLearnLocal_position.testOnImg(...
        obj.img,labelImg,obj.hogLearnData,obj.debugOpts);
    hogLearnOutput = ones(size(initSeg));
    hogLearnOutput(initSeg==0) = -1;
    obj.hogLearnOutput = opts.hogLearnUnary_rescale*hogLearnOutput;
end
