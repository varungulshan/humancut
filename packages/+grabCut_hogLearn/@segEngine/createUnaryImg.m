function [unaryImg,unaryMask]=createUnaryImg(obj)

labelImg=obj.labelImg;
features=obj.features;

labelImg(labelImg==5)=1; % For this function, these labels are equivalent
labelImg(labelImg==6)=2;

ftrMask=(labelImg~=1 & labelImg~=2);
features=features(:,ftrMask(:));

fgLikeli=gmm.computeGmm_likelihood(features,obj.gmmf);
bgLikeli=gmm.computeGmm_likelihood(features,obj.gmmb);

fgLikeli=-log(fgLikeli);
bgLikeli=-log(bgLikeli);
myUB=realmax/(obj.opts.gcScale*100);
fgLikeli(fgLikeli>myUB)=myUB;
bgLikeli(bgLikeli>myUB)=myUB;

[h w]=size(labelImg);
unaryImg=zeros([h w 2]);
tmp=zeros([h w]);tmp(ftrMask)=fgLikeli;
unaryImg(:,:,1)=tmp;
tmp=zeros([h w]);tmp(ftrMask)=bgLikeli;
unaryImg(:,:,2)=tmp;

unaryImg(:,:,2)=unaryImg(:,:,2)+obj.hogLearnOutput;

unaryMask=128*ones([h w],'uint8');
unaryMask(labelImg==1)=255;
unaryMask(labelImg==2)=0;
