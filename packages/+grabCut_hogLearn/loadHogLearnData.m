function [data,learnType] = loadHogLearnData(optsString)

switch(optsString)
  case 'try1'
    learnType = 'hogLearnLocal2';
    data = grabCut_trained.learning.hogLearnLocal2.loadTrainingData(...
        '/data2/varun/iccv2011/results/train/run027/','');
  case 'hogLearnLocal_1'
    learnType = 'hogLearnLocal';
    data = grabCut_trained.learning.hogLearnLocal.loadTrainingData(...
        '/data2/varun/iccv2011/results/train/run015/','');
  case 'hogLearnLocal_2'
    learnType = 'hogLearnLocal';
    data = grabCut_trained.learning.hogLearnLocal.loadTrainingData(...
        '/data1/varun/iccv2011/results/train/run051/','');
  case 'hogLearnLocal_position_run103'
    learnType = 'hogLearnLocal_position';
    data = grabCut_trained.learning.hogLearnLocal_position.loadTrainingData(...
        '/data1/varun/iccv2011/results/train/run103/bestTrain/','');
  otherwise
    error('HogLearnData option: %s is not supported\n',optsString);
end
