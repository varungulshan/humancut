function opts=makeOpts(optsString)

switch(optsString)

  case 'iterQuick'
    opts=grabCut_hogLearn.segOpts();
    opts.gcGamma_e=50;
    opts.gcGamma_i=0;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=1;
    opts.gmmUpdate_iters=1;
    opts.hogLearnUnary_rescale = 1;
    opts.hogLearnType = 'hogLearnLocal2';
    
   case 'iter10_2'
    opts=grabCut_hogLearn.segOpts();
    opts.gcGamma_e=50;
    opts.gcGamma_i=0;
    opts.gcScale=500;
    opts.gcNbrType='nbr8';
    opts.gcSigma_c='auto';

    opts.gmmNmix_fg=5;
    opts.gmmNmix_bg=5;
    opts.postProcess=0;
    opts.numIters=10;
    opts.gmmUpdate_iters=1;
    opts.hogLearnUnary_rescale = 1;
    opts.hogLearnType = 'hogLearnLocal2';
  
  otherwise
    error('Invalid options string %s\n',optsString);
end
