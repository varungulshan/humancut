#include <XnOpenNI.h>
#include <XnCppWrapper.h>
#include <GL/glut.h>
#include <demoConfig.h>
#include <iostream>

#define CHECK_RC(nRetVal, what)                    \
  if (nRetVal != XN_STATUS_OK)                  \
  {                                \
    printf("%s failed: %s\n", what, xnGetStatusString(nRetVal));\
    return nRetVal;                        \
  }

#define CONFIG_XML_PATH DEMO_CONFIG_FILE

//---------------------------------------------------------------------------
// Globals
//---------------------------------------------------------------------------
xn::Context g_Context;
xn::DepthGenerator g_DepthGenerator;
xn::ImageGenerator g_ImageGenerator;
xn::UserGenerator g_UserGenerator;

XnBool g_bNeedPose = FALSE;
XnChar g_strPose[20] = "";
XnBool g_bDrawBackground = TRUE;
XnBool g_bDrawPixels = TRUE;
XnBool g_bDrawSkeleton = TRUE;
XnBool g_bPrintID = TRUE;
XnBool g_bPrintState = TRUE;

#if (XN_PLATFORM == XN_PLATFORM_MACOSX)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#define GL_WIN_SIZE_X 640
#define GL_WIN_SIZE_Y 480

XnBool g_bPause = false;
XnBool g_bRecord = true;
xn::Recorder recorder;
#define RECORDER_DESTINATION "../../recordings/sampleRecord_highRes1.oni"

XnBool g_bQuit = false;
int winDepthMap,winImg;

void DrawDepthMap(const xn::DepthMetaData& dmd, const xn::SceneMetaData& smd);
void DrawImgMap(const xn::ImageMetaData& imd, const xn::SceneMetaData& smd);

//---------------------------------------------------------------------------
// Code
//---------------------------------------------------------------------------

void CleanupExit()
{
  g_Context.Shutdown();

  exit (1);
}

// Callback: New user was detected
void XN_CALLBACK_TYPE User_NewUser(xn::UserGenerator& generator, XnUserID nId, void* pCookie)
{
  printf("New User %d\n", nId);
  // New user found
  if (g_bNeedPose)
  {
    g_UserGenerator.GetPoseDetectionCap().StartPoseDetection(g_strPose, nId);
  }
  else
  {
    g_UserGenerator.GetSkeletonCap().RequestCalibration(nId, TRUE);
  }
}
// Callback: An existing user was lost
void XN_CALLBACK_TYPE User_LostUser(xn::UserGenerator& generator, XnUserID nId, void* pCookie)
{
  printf("Lost user %d\n", nId);
}
// Callback: Detected a pose
void XN_CALLBACK_TYPE UserPose_PoseDetected(xn::PoseDetectionCapability& capability, const XnChar* strPose, XnUserID nId, void* pCookie)
{
  printf("Pose %s detected for user %d\n", strPose, nId);
  g_UserGenerator.GetPoseDetectionCap().StopPoseDetection(nId);
  g_UserGenerator.GetSkeletonCap().RequestCalibration(nId, TRUE);
}
// Callback: Started calibration
void XN_CALLBACK_TYPE UserCalibration_CalibrationStart(xn::SkeletonCapability& capability, XnUserID nId, void* pCookie)
{
  printf("Calibration started for user %d\n", nId);
}
// Callback: Finished calibration
void XN_CALLBACK_TYPE UserCalibration_CalibrationEnd(xn::SkeletonCapability& capability, XnUserID nId, XnBool bSuccess, void* pCookie)
{
  if (bSuccess)
  {
    // Calibration succeeded
    printf("Calibration complete, start tracking user %d\n", nId);
    g_UserGenerator.GetSkeletonCap().StartTracking(nId);
  }
  else
  {
    // Calibration failed
    printf("Calibration failed for user %d\n", nId);
    if (g_bNeedPose)
    {
      g_UserGenerator.GetPoseDetectionCap().StartPoseDetection(g_strPose, nId);
    }
    else
    {
      g_UserGenerator.GetSkeletonCap().RequestCalibration(nId, TRUE);
    }
  }
}

// this function is called each frame
void glutDisplay (void)
{
  glutSetWindow(winDepthMap);
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Setup the OpenGL viewpoint
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();

  xn::SceneMetaData sceneMD;
  xn::DepthMetaData depthMD;
  g_DepthGenerator.GetMetaData(depthMD);
  //glOrtho(0, depthMD.XRes(), depthMD.YRes(), 0, -1.0, 1.0);
  glOrtho(0, depthMD.XRes(), depthMD.YRes(), 0, -1.0, 1.0);

  glDisable(GL_TEXTURE_2D);

  if (!g_bPause)
  {
    // Read next available data
    g_Context.WaitAndUpdateAll();
  }

    // Process the data
    g_DepthGenerator.GetMetaData(depthMD);
    g_UserGenerator.GetUserPixels(0, sceneMD);
    DrawDepthMap(depthMD, sceneMD);

  glutSwapBuffers();
}

// this function is called each frame
void glutDisplay_img (void)
{

  glutSetWindow(winImg);
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Setup the OpenGL viewpoint
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();

  xn::SceneMetaData sceneMD;
  xn::ImageMetaData imageMD;
  g_ImageGenerator.GetMetaData(imageMD);
  //glOrtho(0, depthMD.XRes(), depthMD.YRes(), 0, -1.0, 1.0);
  glOrtho(0, imageMD.XRes(), imageMD.YRes(), 0, -1.0, 1.0);

  glDisable(GL_TEXTURE_2D);

  g_ImageGenerator.GetMetaData(imageMD);
  g_UserGenerator.GetUserPixels(0, sceneMD);
  DrawImgMap(imageMD, sceneMD);

  glutSwapBuffers();
}


void glutIdle (void)
{
  if (g_bQuit) {
    CleanupExit();
  }

  // Display the frame
  glutSetWindow(winDepthMap);
  glutPostRedisplay();
  glutSetWindow(winImg);
  glutPostRedisplay();
  if(g_bRecord){
    recorder.Record();
  }
}

void glutKeyboard (unsigned char key, int x, int y)
{
  switch (key)
  {
  case 27:
    CleanupExit();
  case 'b':
    // Draw background?
    g_bDrawBackground = !g_bDrawBackground;
    break;
  case 'x':
    // Draw pixels at all?
    g_bDrawPixels = !g_bDrawPixels;
    break;
  case 's':
    // Draw Skeleton?
    g_bDrawSkeleton = !g_bDrawSkeleton;
    break;
  case 'i':
    // Print label?
    g_bPrintID = !g_bPrintID;
    break;
  case 'l':
    // Print ID & state as label, or only ID?
    g_bPrintState = !g_bPrintState;
    break;
  case'p':
    g_bPause = !g_bPause;
    break;
  }
}


void glInit (int * pargc, char ** argv)
{
  glutInit(pargc, argv);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
  //glutInitWindowSize(GL_WIN_SIZE_X, GL_WIN_SIZE_Y);
  glutInitWindowSize(640, 960);
  int mainWindow = glutCreateWindow ("Prime Sense User Tracker Viewer");
  winDepthMap = glutCreateSubWindow(mainWindow,0,0,640,480);
  glutSetWindow(winDepthMap);
  //glutFullScreen();
  glutSetCursor(GLUT_CURSOR_NONE);

  glutKeyboardFunc(glutKeyboard);
  glutDisplayFunc(glutDisplay);
  glutIdleFunc(glutIdle);

  glDisable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);
  glEnableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);

  winImg = glutCreateSubWindow(mainWindow,0,480,640,480);
  glutSetWindow(winImg);
  glutDisplayFunc(glutDisplay_img);
  glDisable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);
  glEnableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);

}

int main(int argc, char **argv)
{
  XnStatus nRetVal = XN_STATUS_OK;

  xn::EnumerationErrors errors;
  nRetVal = g_Context.InitFromXmlFile(CONFIG_XML_PATH, &errors);
  if (nRetVal == XN_STATUS_NO_NODE_PRESENT)
  {
    XnChar strError[1024];
    errors.ToString(strError, 1024);
    printf("%s\n", strError);
    return (nRetVal);
  }
  else if (nRetVal != XN_STATUS_OK)
  {
    printf("Open failed: %s\n", xnGetStatusString(nRetVal));
    return (nRetVal);
  }

  nRetVal = g_Context.FindExistingNode(XN_NODE_TYPE_DEPTH, g_DepthGenerator);
  CHECK_RC(nRetVal, "Find depth generator");
  nRetVal = g_Context.FindExistingNode(XN_NODE_TYPE_IMAGE, g_ImageGenerator);
  CHECK_RC(nRetVal, "Find image generator");
  nRetVal = g_Context.FindExistingNode(XN_NODE_TYPE_USER, g_UserGenerator);
  if (nRetVal != XN_STATUS_OK)
  {
    nRetVal = g_UserGenerator.Create(g_Context);
    CHECK_RC(nRetVal, "Find user generator");
  }

  XnCallbackHandle hUserCallbacks, hCalibrationCallbacks, hPoseCallbacks;
  if (!g_UserGenerator.IsCapabilitySupported(XN_CAPABILITY_SKELETON))
  {
    printf("Supplied user generator doesn't support skeleton\n");
    return 1;
  }
  g_UserGenerator.RegisterUserCallbacks(User_NewUser, User_LostUser, NULL, hUserCallbacks);
  g_UserGenerator.GetSkeletonCap().RegisterCalibrationCallbacks(UserCalibration_CalibrationStart, UserCalibration_CalibrationEnd, NULL, hCalibrationCallbacks);

  if (g_UserGenerator.GetSkeletonCap().NeedPoseForCalibration())
  {
    g_bNeedPose = TRUE;
    if (!g_UserGenerator.IsCapabilitySupported(XN_CAPABILITY_POSE_DETECTION))
    {
      printf("Pose required, but not supported\n");
      return 1;
    }
    g_UserGenerator.GetPoseDetectionCap().RegisterToPoseCallbacks(UserPose_PoseDetected, NULL, NULL, hPoseCallbacks);
    g_UserGenerator.GetSkeletonCap().GetCalibrationPose(g_strPose);
  }

  g_UserGenerator.GetSkeletonCap().SetSkeletonProfile(XN_SKEL_PROFILE_ALL);

  if(g_bRecord){
    recorder.Create(g_Context);
    recorder.SetDestination(XN_RECORD_MEDIUM_FILE,RECORDER_DESTINATION);
    recorder.AddNodeToRecording(g_DepthGenerator);
    recorder.AddNodeToRecording(g_ImageGenerator);
    //recorder.AddNodeToRecording(g_UserGenerator);
    printf("Saving recording to %s\n",RECORDER_DESTINATION);
  }

  assert(g_DepthGenerator.IsCapabilitySupported(XN_CAPABILITY_ALTERNATIVE_VIEW_POINT));
  assert(g_DepthGenerator.IsCapabilitySupported(XN_CAPABILITY_FRAME_SYNC));
  assert(g_DepthGenerator.GetFrameSyncCap().CanFrameSyncWith(g_ImageGenerator));
  //assert(g_UserGenerator.IsCapabilitySupported(XN_CAPABILITY_FRAME_SYNC));
  //assert(g_UserGenerator.GetFrameSyncCap().CanFrameSyncWith(g_ImageGenerator));
  //assert(g_ImageGenerator.IsCapabilitySupported(XN_CAPABILITY_FRAME_SYNC));
  //assert(g_ImageGenerator.GetFrameSyncCap().CanFrameSyncWith(g_DepthGenerator));

  //g_DepthGenerator.GetAlternativeViewPointCap().SetViewPoint(g_ImageGenerator);
  g_DepthGenerator.GetFrameSyncCap().FrameSyncWith(g_ImageGenerator);
  nRetVal = g_Context.StartGeneratingAll();
  CHECK_RC(nRetVal, "StartGenerating");

  glInit(&argc, argv);
  glutMainLoop();

}

#define MAX_DEPTH 10000
float g_pDepthHist[MAX_DEPTH];
unsigned int getClosestPowerOfTwo(unsigned int n)
{
	unsigned int m = 2;
	while(m < n) m<<=1;

	return m;
}

GLuint initTexture(void** buf, int& width, int& height)
{
	GLuint texID = 0;
	glGenTextures(1,&texID);

	width = getClosestPowerOfTwo(width);
	height = getClosestPowerOfTwo(height); 
	*buf = new unsigned char[width*height*4];
	glBindTexture(GL_TEXTURE_2D,texID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	return texID;
}

GLfloat texcoords[8],texcoords2[8];
void DrawRectangle(float topLeftX, float topLeftY, float bottomRightX, float bottomRightY)
{
	GLfloat verts[8] = {	topLeftX, topLeftY,
		topLeftX, bottomRightY,
		bottomRightX, bottomRightY,
		bottomRightX, topLeftY
	};
	glVertexPointer(2, GL_FLOAT, 0, verts);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	//TODO: Maybe glFinish needed here instead - if there's some bad graphics crap
	glFlush();
}

void DrawTexture(float topLeftX, float topLeftY, float bottomRightX, float bottomRightY,
                 GLfloat* texcoordsLocal)
{
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2, GL_FLOAT, 0, texcoordsLocal);

	DrawRectangle(topLeftX, topLeftY, bottomRightX, bottomRightY);

	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

XnFloat Colors[][3] =
{
	{0,1,1},
	{0,0,1},
	{0,1,0},
	{1,1,0},
	{1,0,0},
	{1,.5,0},
	{.5,1,0},
	{0,.5,1},
	{.5,0,1},
	{1,1,.5},
	{1,1,1}
};
XnUInt32 nColors = 10;

void glPrintString(void *font, char *str)
{
	int i,l = strlen(str);

	for(i=0; i<l; i++)
	{
		glutBitmapCharacter(font,*str++);
	}
}

void DrawLimb(XnUserID player, XnSkeletonJoint eJoint1, XnSkeletonJoint eJoint2)
{
	if (!g_UserGenerator.GetSkeletonCap().IsTracking(player))
	{
		printf("not tracked!\n");
		return;
	}

	XnSkeletonJointPosition joint1, joint2;
	g_UserGenerator.GetSkeletonCap().GetSkeletonJointPosition(player, eJoint1, joint1);
	g_UserGenerator.GetSkeletonCap().GetSkeletonJointPosition(player, eJoint2, joint2);

	if (joint1.fConfidence < 0.5 || joint2.fConfidence < 0.5)
	{
		return;
	}

	XnPoint3D pt[2];
	pt[0] = joint1.position;
	pt[1] = joint2.position;

	g_DepthGenerator.ConvertRealWorldToProjective(2, pt, pt); 
	glVertex3i(pt[0].X, pt[0].Y, 0);
	glVertex3i(pt[1].X, pt[1].Y, 0);
}

void DrawDepthMap(const xn::DepthMetaData& dmd, const xn::SceneMetaData& smd)
{
  static bool bInitialized = false;  
  static GLuint depthTexID;
  static unsigned char* pDepthTexBuf;
  static int texWidth, texHeight;

  float topLeftX;
  float topLeftY;
  float bottomRightY;
  float bottomRightX;
  float texXpos;
  float texYpos;

  if(!bInitialized)
  {

    texWidth =  getClosestPowerOfTwo(dmd.XRes());
    texHeight = getClosestPowerOfTwo(dmd.YRes());

//    printf("Initializing depth texture: width = %d, height = %d\n", texWidth, texHeight);
    depthTexID = initTexture((void**)&pDepthTexBuf,texWidth, texHeight);

//    printf("Initialized depth texture: width = %d, height = %d\n", texWidth, texHeight);
    bInitialized = true;

    topLeftX = dmd.XRes();
    topLeftY = 0;
    bottomRightY = dmd.YRes();
    bottomRightX = 0;
    texXpos =(float)dmd.XRes()/texWidth;
    texYpos  =(float)dmd.YRes()/texHeight;

    memset(texcoords, 0, 8*sizeof(float));
    texcoords[0] = texXpos, texcoords[1] = texYpos, texcoords[2] = texXpos, texcoords[7] = texYpos;

  }
  unsigned int nValue = 0;
  unsigned int nHistValue = 0;
  unsigned int nIndex = 0;
  unsigned int nX = 0;
  unsigned int nY = 0;
  unsigned int nNumberOfPoints = 0;
  XnUInt16 g_nXRes = dmd.XRes();
  XnUInt16 g_nYRes = dmd.YRes();

  unsigned char* pDestImage = pDepthTexBuf;

  const XnDepthPixel* pDepth = dmd.Data();
  const XnLabel* pLabels = smd.Data();

  // Calculate the accumulative histogram
  memset(g_pDepthHist, 0, MAX_DEPTH*sizeof(float));
  for (nY=0; nY<g_nYRes; nY++)
  {
    for (nX=0; nX<g_nXRes; nX++)
    {
      nValue = *pDepth;

      if (nValue != 0)
      {
        g_pDepthHist[nValue]++;
        nNumberOfPoints++;
      }

      pDepth++;
    }
  }

  for (nIndex=1; nIndex<MAX_DEPTH; nIndex++)
  {
    g_pDepthHist[nIndex] += g_pDepthHist[nIndex-1];
  }
  if (nNumberOfPoints)
  {
    for (nIndex=1; nIndex<MAX_DEPTH; nIndex++)
    {
      g_pDepthHist[nIndex] = (unsigned int)(256 * (1.0f - (g_pDepthHist[nIndex] / nNumberOfPoints)));
    }
  }

  pDepth = dmd.Data();
  if (g_bDrawPixels)
  {
    XnUInt32 nIndex = 0;
    // Prepare the texture map
    for (nY=0; nY<g_nYRes; nY++)
    {
      for (nX=0; nX < g_nXRes; nX++, nIndex++)
      {

        pDestImage[0] = 0;
        pDestImage[1] = 0;
        pDestImage[2] = 0;
        if (g_bDrawBackground || *pLabels != 0)
        {
          nValue = *pDepth;
          XnLabel label = *pLabels;
          XnUInt32 nColorID = label % nColors;
          if (label == 0)
          {
            nColorID = nColors;
          }

          if (nValue != 0)
          {
            nHistValue = g_pDepthHist[nValue];

            pDestImage[0] = nHistValue * Colors[nColorID][0]; 
            pDestImage[1] = nHistValue * Colors[nColorID][1];
            pDestImage[2] = nHistValue * Colors[nColorID][2];
          }
        }

        pDepth++;
        pLabels++;
        pDestImage+=3;
      }

      pDestImage += (texWidth - g_nXRes) *3;
    }

  }
  else
  {
    xnOSMemSet(pDepthTexBuf, 0, 3*2*g_nXRes*g_nYRes);
  }

  glBindTexture(GL_TEXTURE_2D, depthTexID);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texWidth, texHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, pDepthTexBuf);

  // Display the OpenGL texture map
  glColor4f(0.75,0.75,0.75,1);

  glEnable(GL_TEXTURE_2D);
  DrawTexture(dmd.XRes(),dmd.YRes(),0,0,texcoords);  
  glDisable(GL_TEXTURE_2D);

  char strLabel[50] = "";
  XnUserID aUsers[15];
  XnUInt16 nUsers = 15;
  g_UserGenerator.GetUsers(aUsers, nUsers);
  for (int i = 0; i < nUsers; ++i)
  {
    if (g_bPrintID)
    {
      XnPoint3D com;
      g_UserGenerator.GetCoM(aUsers[i], com);
      g_DepthGenerator.ConvertRealWorldToProjective(1, &com, &com);

      xnOSMemSet(strLabel, 0, sizeof(strLabel));
      if (!g_bPrintState)
      {
        // Tracking
        sprintf(strLabel, "%d", aUsers[i]);
      }
      else if (g_UserGenerator.GetSkeletonCap().IsTracking(aUsers[i]))
      {
        // Tracking
        sprintf(strLabel, "%d - Tracking", aUsers[i]);
      }
      else if (g_UserGenerator.GetSkeletonCap().IsCalibrating(aUsers[i]))
      {
        // Calibrating
        sprintf(strLabel, "%d - Calibrating...", aUsers[i]);
      }
      else
      {
        // Nothing
        sprintf(strLabel, "%d - Looking for pose", aUsers[i]);
      }


      glColor4f(1-Colors[i%nColors][0], 1-Colors[i%nColors][1], 1-Colors[i%nColors][2], 1);

      glRasterPos2i(com.X, com.Y);
      glPrintString(GLUT_BITMAP_HELVETICA_18, strLabel);
    }

    if (g_bDrawSkeleton && g_UserGenerator.GetSkeletonCap().IsTracking(aUsers[i]))
    {
      glBegin(GL_LINES);
      glColor4f(1-Colors[aUsers[i]%nColors][0], 1-Colors[aUsers[i]%nColors][1], 1-Colors[aUsers[i]%nColors][2], 1);
      DrawLimb(aUsers[i], XN_SKEL_HEAD, XN_SKEL_NECK);

      DrawLimb(aUsers[i], XN_SKEL_NECK, XN_SKEL_LEFT_SHOULDER);
      DrawLimb(aUsers[i], XN_SKEL_LEFT_SHOULDER, XN_SKEL_LEFT_ELBOW);
      DrawLimb(aUsers[i], XN_SKEL_LEFT_ELBOW, XN_SKEL_LEFT_HAND);

      DrawLimb(aUsers[i], XN_SKEL_NECK, XN_SKEL_RIGHT_SHOULDER);
      DrawLimb(aUsers[i], XN_SKEL_RIGHT_SHOULDER, XN_SKEL_RIGHT_ELBOW);
      DrawLimb(aUsers[i], XN_SKEL_RIGHT_ELBOW, XN_SKEL_RIGHT_HAND);

      DrawLimb(aUsers[i], XN_SKEL_LEFT_SHOULDER, XN_SKEL_TORSO);
      DrawLimb(aUsers[i], XN_SKEL_RIGHT_SHOULDER, XN_SKEL_TORSO);

      DrawLimb(aUsers[i], XN_SKEL_TORSO, XN_SKEL_LEFT_HIP);
      DrawLimb(aUsers[i], XN_SKEL_LEFT_HIP, XN_SKEL_LEFT_KNEE);
      DrawLimb(aUsers[i], XN_SKEL_LEFT_KNEE, XN_SKEL_LEFT_FOOT);

      DrawLimb(aUsers[i], XN_SKEL_TORSO, XN_SKEL_RIGHT_HIP);
      DrawLimb(aUsers[i], XN_SKEL_RIGHT_HIP, XN_SKEL_RIGHT_KNEE);
      DrawLimb(aUsers[i], XN_SKEL_RIGHT_KNEE, XN_SKEL_RIGHT_FOOT);

      DrawLimb(aUsers[i], XN_SKEL_LEFT_HIP, XN_SKEL_RIGHT_HIP);

      glEnd();
    }
  }
}

void DrawImgMap(const xn::ImageMetaData& imd, const xn::SceneMetaData& smd)
{
  static bool bInitialized = false;  
  static GLuint imgTexID;
  static unsigned char* pImgTexBuf;
  static int imgTexHeight,imgTexWidth;

  float topLeftX;
  float topLeftY;
  float bottomRightY;
  float bottomRightX;
  float texXpos;
  float texYpos;


  if(!bInitialized)
  {

    imgTexWidth =  getClosestPowerOfTwo(imd.XRes());
    imgTexHeight = getClosestPowerOfTwo(imd.YRes());

//    printf("Initializing depth texture: width = %d, height = %d\n", texWidth, texHeight);
    imgTexID = initTexture((void**)&pImgTexBuf,imgTexWidth, imgTexHeight);

//    printf("Initialized depth texture: width = %d, height = %d\n", texWidth, texHeight);
    bInitialized = true;

    topLeftX = imd.XRes();
    topLeftY = 0;
    bottomRightY = imd.YRes();
    bottomRightX = 0;
    texXpos =(float)imd.XRes()/imgTexWidth;
    texYpos  =(float)imd.YRes()/imgTexHeight;

    memset(texcoords2, 0, 8*sizeof(float));
    texcoords2[0] = texXpos, texcoords2[1] = texYpos, texcoords2[2] = texXpos,
    texcoords2[7] = texYpos;
  }

  unsigned char* pDestImage2 = pImgTexBuf;

  if (g_bDrawPixels)
  {
    const XnRGB24Pixel* pImg = imd.RGB24Data();
    for(int nY = 0; nY < imd.YRes(); nY++){
      for(int nX = 0; nX < imd.XRes(); nX++){
        pDestImage2[0] = pImg->nRed;
        pDestImage2[1] = pImg->nGreen;
        pDestImage2[2] = pImg->nBlue;
        pDestImage2+=3;
        pImg++;
      }
      pDestImage2 += (imgTexWidth - imd.XRes())*3;
    }

  }

  glBindTexture(GL_TEXTURE_2D, imgTexID);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imgTexWidth, imgTexHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, pImgTexBuf);

  // Display the OpenGL texture map
  glColor4f(0.75,0.75,0.75,1);

  glEnable(GL_TEXTURE_2D);
  DrawTexture(imd.XRes(),imd.YRes(),0,0,texcoords2);  
  glDisable(GL_TEXTURE_2D);

}
