#ifndef CREATE_DATASET_H
#define CREATE_DATASET_H

#ifndef NDEBUG
  #define printDebug(...) printf(__VA_ARGS__)
#else
  #define printDebug(...)
#endif

#endif
