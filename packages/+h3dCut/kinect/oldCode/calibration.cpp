// Code to breakpoint and see what happend in alternative viewpoint
#include <XnOpenNI.h>
#include <XnCppWrapper.h>
#include <GL/glut.h>
#include <demoConfig.h>
#include <iostream>
#include <assert.h>
#include <createDataset.h>
#include <cv.h>
#include <highgui.h>

#define CHECK_RC(nRetVal, what)                    \
  if (nRetVal != XN_STATUS_OK)                  \
  {                                \
    printf("%s failed: %s\n", what, xnGetStatusString(nRetVal));\
    return nRetVal;                        \
  }

#define CONFIG_XML_PATH DEMO_CONFIG_FILE
#define DOT_RADIUS 8
#define DOT_COLOR CV_RGB(255,0,0)
#define LINE_THICKNESS 4
#define LINE_COLOR CV_RGB(0,255,0)

//---------------------------------------------------------------------------
// Globals
//---------------------------------------------------------------------------
xn::Context g_Context;
xn::DepthGenerator g_DepthGenerator;
xn::ImageGenerator g_ImageGenerator;
xn::UserGenerator g_UserGenerator;
XnBool g_bDrawBackground = TRUE;
XnBool g_bDrawPixels = TRUE;
XnBool g_bDrawSkeleton = TRUE;
XnBool g_bPrintID = TRUE;
XnBool g_bPrintState = TRUE;
XnChar g_strPose[20] = "";
bool outputVideo = false;

#if (XN_PLATFORM == XN_PLATFORM_MACOSX)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#define GL_WIN_SIZE_X 1280
#define GL_WIN_SIZE_Y 480

XnBool g_bPause = false;
XnBool g_bRecord = false;
xn::Recorder recorder;
#define RECORDER_DESTINATION "../../recordings/sampleRecord2.oni"
#define OUTPUT_VIDEO_PATH "../../recordings/outputSave7.avi"

XnBool g_bQuit = false;
XnBool g_bNeedPose = FALSE;

int winDepthMap,winImg,winMain;
cv::Mat *cvImg=NULL;
cv::Mat *outputImg=NULL;
unsigned char *tmpDepthMap,*tmpImg;
cv::VideoWriter* vw=NULL;
int imgW,imgH,depthW,depthH;
void DrawDepthMap(const xn::DepthMetaData& dmd, const xn::SceneMetaData& smd);
void DrawStickManOpenCV(const xn::ImageMetaData& imd);
void DrawImgMap();

//---------------------------------------------------------------------------
// Code
//---------------------------------------------------------------------------

void addOutputFrame(){
  glutSetWindow(winDepthMap);
  glReadPixels(0,0,depthW,depthH,GL_BGR,GL_UNSIGNED_BYTE,tmpDepthMap);
  glutSetWindow(winImg);
  glReadPixels(0,0,imgW,imgH,GL_BGR,GL_UNSIGNED_BYTE,tmpImg);

  unsigned char* ptrOutput = outputImg->ptr();
  for(int y=0;y<depthH;y++){
    unsigned char *ptrDepth = tmpDepthMap+(depthH-1-y)*3*depthW;
    for(int x=0;x<depthW;x++){
      ptrOutput[0]=ptrDepth[0];
      ptrOutput[1]=ptrDepth[1];
      ptrOutput[2]=ptrDepth[2];
      ptrOutput+=3;ptrDepth+=3;
    }
    ptrOutput+=3*(outputImg->cols-depthW);
  }

  ptrOutput = outputImg->ptr()+depthW*3;
  for(int y=0;y<imgH;y++){
    unsigned char *ptrImg = tmpImg+(imgH-1-y)*3*imgW;
    for(int x=0;x<imgW;x++){
      ptrOutput[0]=ptrImg[0];
      ptrOutput[1]=ptrImg[1];
      ptrOutput[2]=ptrImg[2];
      ptrOutput+=3;ptrImg+=3;
    }
    ptrOutput+=3*(outputImg->cols-imgW);
  }

  (*vw)<<(*outputImg);
  //printDebug("Added output frame\n");
}

void CleanupExit()
{
  g_Context.Shutdown();
  delete(cvImg);
  if(vw!=NULL){delete(vw);}
  if(outputVideo){delete(tmpDepthMap);delete(tmpImg);}
  exit (1);
}

// Callback: New user was detected
void XN_CALLBACK_TYPE User_NewUser(xn::UserGenerator& generator, XnUserID nId, void* pCookie)
{
  printf("New User %d\n", nId);
  // New user found
  if (g_bNeedPose)
  {
    g_UserGenerator.GetPoseDetectionCap().StartPoseDetection(g_strPose, nId);
  }
  else
  {
    g_UserGenerator.GetSkeletonCap().RequestCalibration(nId, TRUE);
  }
}

// Callback: An existing user was lost
void XN_CALLBACK_TYPE User_LostUser(xn::UserGenerator& generator, XnUserID nId, void* pCookie)
{
  printf("Lost user %d\n", nId);
}

// Callback: Detected a pose
void XN_CALLBACK_TYPE UserPose_PoseDetected(xn::PoseDetectionCapability& capability, const XnChar* strPose, XnUserID nId, void* pCookie)
{
  printf("Pose %s detected for user %d\n", strPose, nId);
  g_UserGenerator.GetPoseDetectionCap().StopPoseDetection(nId);
  g_UserGenerator.GetSkeletonCap().RequestCalibration(nId, TRUE);
}

// Callback: Started calibration
void XN_CALLBACK_TYPE UserCalibration_CalibrationStart(xn::SkeletonCapability& capability, XnUserID nId, void* pCookie)
{
  printf("Calibration started for user %d\n", nId);
}

// Callback: Finished calibration
void XN_CALLBACK_TYPE UserCalibration_CalibrationEnd(xn::SkeletonCapability& capability, XnUserID nId, XnBool bSuccess, void* pCookie)
{
  if (bSuccess)
  {
    // Calibration succeeded
    printf("Calibration complete, start tracking user %d\n", nId);
    g_UserGenerator.GetSkeletonCap().StartTracking(nId);
  }
  else
  {
    // Calibration failed
    printf("Calibration failed for user %d\n", nId);
    if (g_bNeedPose)
    {
      g_UserGenerator.GetPoseDetectionCap().StartPoseDetection(g_strPose, nId);
    }
    else
    {
      g_UserGenerator.GetSkeletonCap().RequestCalibration(nId, TRUE);
    }
  }
}

// this function is called each frame
void glutDisplay (void)
{
  glutSetWindow(winDepthMap);
  if (!g_bPause)
  {
    // Read next available data
    //g_Context.WaitAndUpdateAll();
    g_Context.WaitOneUpdateAll(g_DepthGenerator);
  }

  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Setup the OpenGL viewpoint
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();

  xn::SceneMetaData sceneMD;
  xn::DepthMetaData depthMD;
  xn::ImageMetaData imageMD;
  g_DepthGenerator.GetMetaData(depthMD);
  g_ImageGenerator.GetMetaData(imageMD);
  glOrtho(0, depthMD.XRes(), depthMD.YRes(), 0, -1.0, 1.0);
  glDisable(GL_TEXTURE_2D);
  g_UserGenerator.GetUserPixels(0, sceneMD);

  imgW = imageMD.XRes();imgH = imageMD.YRes();
  depthW = depthMD.XRes();depthH = depthMD.YRes();

  if(cvImg==NULL){
    cvImg = new cv::Mat(imgH,imgW,CV_8UC3);
  }
  if(outputVideo && outputImg==NULL){
    outputImg = new cv::Mat(std::max(imgH,depthH),imgW+depthW,CV_8UC3);
    vw = new cv::VideoWriter();
    vw->open(OUTPUT_VIDEO_PATH,CV_FOURCC('P','I','M','1'),30,outputImg->size());
    assert(vw->isOpened());
    tmpImg = new unsigned char[imgH*imgW*3];
    tmpDepthMap = new unsigned char[depthH*depthW*3];
  }
  DrawDepthMap(depthMD, sceneMD);
  glutSwapBuffers();

  glutSetWindow(winImg);
  DrawStickManOpenCV(imageMD);
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Setup the OpenGL viewpoint
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();

  glOrtho(0, imageMD.XRes(), imageMD.YRes(), 0, -1.0, 1.0);
  glDisable(GL_TEXTURE_2D);

  DrawImgMap();

  glutSwapBuffers();

  if(outputVideo){
    addOutputFrame();
  }
}

void glutIdle (void)
{
  if (g_bQuit) {
    CleanupExit();
  }

  // Display the frame
  glutSetWindow(winDepthMap);
  glutPostRedisplay();
}

void glutKeyboard (unsigned char key, int x, int y)
{
  switch (key)
  {
  case 27:
    CleanupExit();
  case 'b':
    // Draw background?
    g_bDrawBackground = !g_bDrawBackground;
    break;
  case 'x':
    // Draw pixels at all?
    g_bDrawPixels = !g_bDrawPixels;
    break;
  case 's':
    // Draw Skeleton?
    g_bDrawSkeleton = !g_bDrawSkeleton;
    break;
  case 'i':
    // Print label?
    g_bPrintID = !g_bPrintID;
    break;
  case 'l':
    // Print ID & state as label, or only ID?
    g_bPrintState = !g_bPrintState;
    break;
  case'p':
    g_bPause = !g_bPause;
    break;
  }
}

void glInit (int * pargc, char ** argv)
{
  glutInit(pargc, argv);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(GL_WIN_SIZE_X, GL_WIN_SIZE_Y);
  winMain = glutCreateWindow ("Prime Sense User Tracker Viewer");
  
  winDepthMap = glutCreateSubWindow(winMain,0,0,640,480);
  glutSetWindow(winDepthMap);
  glutSetCursor(GLUT_CURSOR_NONE);

  glutKeyboardFunc(glutKeyboard);
  glutDisplayFunc(glutDisplay);
  glutIdleFunc(glutIdle);

  glDisable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);
  glEnableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);

  winImg = glutCreateSubWindow(winMain,640,0,640,480);
  glutSetWindow(winImg);
  glutDisplayFunc(glutDisplay);
  glDisable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);
  glEnableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);

}

int main(int argc, char **argv)
{
  XnStatus nRetVal = XN_STATUS_OK;

	if (argc > 1){
		nRetVal = g_Context.Init();
		CHECK_RC(nRetVal, "Init");
		nRetVal = g_Context.OpenFileRecording(argv[1]);
		if (nRetVal != XN_STATUS_OK)
		{
			printf("Can't open recording %s: %s\n", argv[1], xnGetStatusString(nRetVal));
			return 1;
		}
  }else{
    xn::EnumerationErrors errors;
    nRetVal = g_Context.InitFromXmlFile(CONFIG_XML_PATH, &errors);
    if (nRetVal == XN_STATUS_NO_NODE_PRESENT)
    {
      XnChar strError[1024];
      errors.ToString(strError, 1024);
      printf("%s\n", strError);
      return (nRetVal);
    }
    else if (nRetVal != XN_STATUS_OK)
    {
      printf("Open failed: %s\n", xnGetStatusString(nRetVal));
      return (nRetVal);
    }
  }

  nRetVal = g_Context.FindExistingNode(XN_NODE_TYPE_DEPTH, g_DepthGenerator);
  //nRetVal = g_DepthGenerator.Create(g_Context);
  CHECK_RC(nRetVal, "Find depth generator");
  nRetVal = g_Context.FindExistingNode(XN_NODE_TYPE_IMAGE, g_ImageGenerator);
  CHECK_RC(nRetVal, "Find image generator");
  nRetVal = g_Context.FindExistingNode(XN_NODE_TYPE_USER, g_UserGenerator);
  if (nRetVal != XN_STATUS_OK)
  {
    nRetVal = g_UserGenerator.Create(g_Context);
    CHECK_RC(nRetVal, "Find user generator");
  }

  XnCallbackHandle hUserCallbacks, hCalibrationCallbacks, hPoseCallbacks;
  if (!g_UserGenerator.IsCapabilitySupported(XN_CAPABILITY_SKELETON))
  {
    printf("Supplied user generator doesn't support skeleton\n");
    return 1;
  }
  g_UserGenerator.RegisterUserCallbacks(User_NewUser, User_LostUser, NULL, hUserCallbacks);
  g_UserGenerator.GetSkeletonCap().RegisterCalibrationCallbacks(UserCalibration_CalibrationStart, UserCalibration_CalibrationEnd, NULL, hCalibrationCallbacks);

  if (g_UserGenerator.GetSkeletonCap().NeedPoseForCalibration())
  {
    g_bNeedPose = TRUE;
    if (!g_UserGenerator.IsCapabilitySupported(XN_CAPABILITY_POSE_DETECTION))
    {
      printf("Pose required, but not supported\n");
      return 1;
    }
    g_UserGenerator.GetPoseDetectionCap().RegisterToPoseCallbacks(UserPose_PoseDetected, NULL, NULL, hPoseCallbacks);
    g_UserGenerator.GetSkeletonCap().GetCalibrationPose(g_strPose);
  }

  g_UserGenerator.GetSkeletonCap().SetSkeletonProfile(XN_SKEL_PROFILE_ALL);
  

  nRetVal = g_Context.StartGeneratingAll();
  CHECK_RC(nRetVal, "StartGenerating");
  //g_DepthGenerator_mock.CreateBasedOn(g_DepthGenerator);
  //assert(g_DepthGenerator_mock.IsCapabilitySupported(XN_CAPABILITY_ALTERNATIVE_VIEW_POINT));
  //g_DepthGenerator_mock.GetAlternativeViewPointCap().SetViewPoint(g_ImageGenerator);
  g_DepthGenerator.GetAlternativeViewPointCap().SetViewPoint(g_ImageGenerator);
  
  glInit(&argc, argv);
  glutMainLoop();

}

#define MAX_DEPTH 10000
float g_pDepthHist[MAX_DEPTH];
unsigned int getClosestPowerOfTwo(unsigned int n)
{
	unsigned int m = 2;
	while(m < n) m<<=1;

	return m;
}

GLuint initTexture(void** buf, int& width, int& height)
{
	GLuint texID = 0;
	glGenTextures(1,&texID);

	width = getClosestPowerOfTwo(width);
	height = getClosestPowerOfTwo(height); 
	*buf = new unsigned char[width*height*4];
	glBindTexture(GL_TEXTURE_2D,texID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	return texID;
}

GLfloat texcoords[8],texcoords2[8];
void DrawRectangle(float topLeftX, float topLeftY, float bottomRightX, float bottomRightY)
{
	GLfloat verts[8] = {	topLeftX, topLeftY,
		topLeftX, bottomRightY,
		bottomRightX, bottomRightY,
		bottomRightX, topLeftY
	};
	glVertexPointer(2, GL_FLOAT, 0, verts);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	//TODO: Maybe glFinish needed here instead - if there's some bad graphics crap
	glFlush();
}

void DrawTexture(float topLeftX, float topLeftY, float bottomRightX, float bottomRightY,
                 GLfloat* texcoordsLocal)
{
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2, GL_FLOAT, 0, texcoordsLocal);

	DrawRectangle(topLeftX, topLeftY, bottomRightX, bottomRightY);

	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

XnFloat Colors[][3] =
{
	{0,1,1},
	{0,0,1},
	{0,1,0},
	{1,1,0},
	{1,0,0},
	{1,.5,0},
	{.5,1,0},
	{0,.5,1},
	{.5,0,1},
	{1,1,.5},
	{1,1,1}
};
XnUInt32 nColors = 10;

void glPrintString(void *font, char *str)
{
	int i,l = strlen(str);

	for(i=0; i<l; i++)
	{
		glutBitmapCharacter(font,*str++);
	}
}

void DrawLimb(XnUserID player, XnSkeletonJoint eJoint1, XnSkeletonJoint eJoint2)
{
	if (!g_UserGenerator.GetSkeletonCap().IsTracking(player))
	{
		printf("not tracked!\n");
		return;
	}

	XnSkeletonJointPosition joint1, joint2;
	g_UserGenerator.GetSkeletonCap().GetSkeletonJointPosition(player, eJoint1, joint1);
	g_UserGenerator.GetSkeletonCap().GetSkeletonJointPosition(player, eJoint2, joint2);

	if (joint1.fConfidence < 0.5 || joint2.fConfidence < 0.5)
	{
		return;
	}

	XnPoint3D pt[2];
	pt[0] = joint1.position;
	pt[1] = joint2.position;

	g_DepthGenerator.ConvertRealWorldToProjective(2, pt, pt); 
	glVertex3i(pt[0].X, pt[0].Y, 0);
	glVertex3i(pt[1].X, pt[1].Y, 0);
}

void DrawDepthMap(const xn::DepthMetaData& dmd, const xn::SceneMetaData& smd)
{
  static bool bInitialized = false;  
  static GLuint depthTexID;
  static unsigned char* pDepthTexBuf;
  static int texWidth, texHeight;

  float topLeftX;
  float topLeftY;
  float bottomRightY;
  float bottomRightX;
  float texXpos;
  float texYpos;

  if(!bInitialized)
  {

    texWidth =  getClosestPowerOfTwo(dmd.XRes());
    texHeight = getClosestPowerOfTwo(dmd.YRes());

//    printf("Initializing depth texture: width = %d, height = %d\n", texWidth, texHeight);
    depthTexID = initTexture((void**)&pDepthTexBuf,texWidth, texHeight);

//    printf("Initialized depth texture: width = %d, height = %d\n", texWidth, texHeight);
    bInitialized = true;

    topLeftX = dmd.XRes();
    topLeftY = 0;
    bottomRightY = dmd.YRes();
    bottomRightX = 0;
    texXpos =(float)dmd.XRes()/texWidth;
    texYpos  =(float)dmd.YRes()/texHeight;

    memset(texcoords, 0, 8*sizeof(float));
    texcoords[0] = texXpos, texcoords[1] = texYpos, texcoords[2] = texXpos, texcoords[7] = texYpos;

  }
  unsigned int nValue = 0;
  unsigned int nHistValue = 0;
  unsigned int nIndex = 0;
  unsigned int nX = 0;
  unsigned int nY = 0;
  unsigned int nNumberOfPoints = 0;
  XnUInt16 g_nXRes = dmd.XRes();
  XnUInt16 g_nYRes = dmd.YRes();

  unsigned char* pDestImage = pDepthTexBuf;

  const XnDepthPixel* pDepth = dmd.Data();
  const XnLabel* pLabels = smd.Data();

  // Calculate the accumulative histogram
  memset(g_pDepthHist, 0, MAX_DEPTH*sizeof(float));
  for (nY=0; nY<g_nYRes; nY++)
  {
    for (nX=0; nX<g_nXRes; nX++)
    {
      nValue = *pDepth;

      if (nValue != 0)
      {
        g_pDepthHist[nValue]++;
        nNumberOfPoints++;
      }

      pDepth++;
    }
  }

  for (nIndex=1; nIndex<MAX_DEPTH; nIndex++)
  {
    g_pDepthHist[nIndex] += g_pDepthHist[nIndex-1];
  }
  if (nNumberOfPoints)
  {
    for (nIndex=1; nIndex<MAX_DEPTH; nIndex++)
    {
      g_pDepthHist[nIndex] = (unsigned int)(256 * (1.0f - (g_pDepthHist[nIndex] / nNumberOfPoints)));
    }
  }

  pDepth = dmd.Data();
  if (g_bDrawPixels)
  {
    XnUInt32 nIndex = 0;
    // Prepare the texture map
    for (nY=0; nY<g_nYRes; nY++)
    {
      for (nX=0; nX < g_nXRes; nX++, nIndex++)
      {

        pDestImage[0] = 0;
        pDestImage[1] = 0;
        pDestImage[2] = 0;
        if (g_bDrawBackground || *pLabels != 0)
        {
          nValue = *pDepth;
          XnLabel label = *pLabels;
          XnUInt32 nColorID = label % nColors;
          if (label == 0)
          {
            nColorID = nColors;
          }

          if (nValue != 0)
          {
            nHistValue = g_pDepthHist[nValue];

            pDestImage[0] = nHistValue * Colors[nColorID][0]; 
            pDestImage[1] = nHistValue * Colors[nColorID][1];
            pDestImage[2] = nHistValue * Colors[nColorID][2];
          }
        }

        pDepth++;
        pLabels++;
        pDestImage+=3;
      }

      pDestImage += (texWidth - g_nXRes) *3;
    }

  }
  else
  {
    xnOSMemSet(pDepthTexBuf, 0, 3*2*g_nXRes*g_nYRes);
  }

  glBindTexture(GL_TEXTURE_2D, depthTexID);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texWidth, texHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, pDepthTexBuf);

  // Display the OpenGL texture map
  glColor4f(0.75,0.75,0.75,1);

  glEnable(GL_TEXTURE_2D);
  DrawTexture(dmd.XRes(),dmd.YRes(),0,0,texcoords);  
  glDisable(GL_TEXTURE_2D);

  char strLabel[50] = "";
  XnUserID aUsers[15];
  XnUInt16 nUsers = 15;
  g_UserGenerator.GetUsers(aUsers, nUsers);
  for (int i = 0; i < nUsers; ++i)
  {
    if (g_bPrintID)
    {
      XnPoint3D com;
      g_UserGenerator.GetCoM(aUsers[i], com);
      g_DepthGenerator.ConvertRealWorldToProjective(1, &com, &com);

      xnOSMemSet(strLabel, 0, sizeof(strLabel));
      if (!g_bPrintState)
      {
        // Tracking
        sprintf(strLabel, "%d", aUsers[i]);
      }
      else if (g_UserGenerator.GetSkeletonCap().IsTracking(aUsers[i]))
      {
        // Tracking
        sprintf(strLabel, "%d - Tracking", aUsers[i]);
      }
      else if (g_UserGenerator.GetSkeletonCap().IsCalibrating(aUsers[i]))
      {
        // Calibrating
        sprintf(strLabel, "%d - Calibrating...", aUsers[i]);
      }
      else
      {
        // Nothing
        sprintf(strLabel, "%d - Looking for pose", aUsers[i]);
      }


      glColor4f(1-Colors[i%nColors][0], 1-Colors[i%nColors][1], 1-Colors[i%nColors][2], 1);

      glRasterPos2i(com.X, com.Y);
      glPrintString(GLUT_BITMAP_HELVETICA_18, strLabel);
    }

    if (g_bDrawSkeleton && g_UserGenerator.GetSkeletonCap().IsTracking(aUsers[i]))
    {
      glBegin(GL_LINES);
      glColor4f(1-Colors[aUsers[i]%nColors][0], 1-Colors[aUsers[i]%nColors][1], 1-Colors[aUsers[i]%nColors][2], 1);
      DrawLimb(aUsers[i], XN_SKEL_HEAD, XN_SKEL_NECK);

      DrawLimb(aUsers[i], XN_SKEL_NECK, XN_SKEL_LEFT_SHOULDER);
      DrawLimb(aUsers[i], XN_SKEL_LEFT_SHOULDER, XN_SKEL_LEFT_ELBOW);
      DrawLimb(aUsers[i], XN_SKEL_LEFT_ELBOW, XN_SKEL_LEFT_HAND);

      DrawLimb(aUsers[i], XN_SKEL_NECK, XN_SKEL_RIGHT_SHOULDER);
      DrawLimb(aUsers[i], XN_SKEL_RIGHT_SHOULDER, XN_SKEL_RIGHT_ELBOW);
      DrawLimb(aUsers[i], XN_SKEL_RIGHT_ELBOW, XN_SKEL_RIGHT_HAND);

      DrawLimb(aUsers[i], XN_SKEL_LEFT_SHOULDER, XN_SKEL_TORSO);
      DrawLimb(aUsers[i], XN_SKEL_RIGHT_SHOULDER, XN_SKEL_TORSO);

      DrawLimb(aUsers[i], XN_SKEL_TORSO, XN_SKEL_LEFT_HIP);
      DrawLimb(aUsers[i], XN_SKEL_LEFT_HIP, XN_SKEL_LEFT_KNEE);
      DrawLimb(aUsers[i], XN_SKEL_LEFT_KNEE, XN_SKEL_LEFT_FOOT);

      DrawLimb(aUsers[i], XN_SKEL_TORSO, XN_SKEL_RIGHT_HIP);
      DrawLimb(aUsers[i], XN_SKEL_RIGHT_HIP, XN_SKEL_RIGHT_KNEE);
      DrawLimb(aUsers[i], XN_SKEL_RIGHT_KNEE, XN_SKEL_RIGHT_FOOT);

      DrawLimb(aUsers[i], XN_SKEL_LEFT_HIP, XN_SKEL_RIGHT_HIP);

      glEnd();
    }
  }
}

void drawJoints(XnUserID uid,cv::Mat &img){
  XnSkeletonJoint joints[]={XN_SKEL_HEAD,XN_SKEL_NECK,
                            XN_SKEL_LEFT_SHOULDER,XN_SKEL_RIGHT_SHOULDER,
                            XN_SKEL_LEFT_ELBOW,XN_SKEL_RIGHT_ELBOW,
                            XN_SKEL_LEFT_HAND,XN_SKEL_RIGHT_HAND,
                            XN_SKEL_LEFT_HIP,XN_SKEL_RIGHT_HIP,
                            XN_SKEL_LEFT_KNEE,XN_SKEL_RIGHT_KNEE,
                            XN_SKEL_LEFT_FOOT,XN_SKEL_RIGHT_FOOT
                           };
  
  int numJoints = sizeof(joints)/sizeof(XnSkeletonJoint);
  for(int i=0;i<numJoints;i++){
    XnSkeletonJointPosition jPos;
    g_UserGenerator.GetSkeletonCap().GetSkeletonJointPosition(uid,joints[i],jPos);
    if(jPos.fConfidence < 0.5){continue;}
    XnPoint3D pt = jPos.position;
    g_DepthGenerator.ConvertRealWorldToProjective(1,&pt,&pt);
    cv::Point cvPoint(pt.X,pt.Y);
    cv::circle(img,cvPoint,DOT_RADIUS,DOT_COLOR,-1);
  }

}

void drawLines(XnUserID uid,cv::Mat &img){
  XnSkeletonJoint limbs[][2] = {{XN_SKEL_HEAD, XN_SKEL_NECK},
                                {XN_SKEL_NECK, XN_SKEL_LEFT_SHOULDER},
                                {XN_SKEL_LEFT_SHOULDER, XN_SKEL_LEFT_ELBOW},
                                {XN_SKEL_LEFT_ELBOW, XN_SKEL_LEFT_HAND},
                                {XN_SKEL_NECK, XN_SKEL_RIGHT_SHOULDER},
                                {XN_SKEL_RIGHT_SHOULDER, XN_SKEL_RIGHT_ELBOW},
                                {XN_SKEL_RIGHT_ELBOW, XN_SKEL_RIGHT_HAND},
                                {XN_SKEL_LEFT_SHOULDER, XN_SKEL_TORSO},
                                {XN_SKEL_RIGHT_SHOULDER, XN_SKEL_TORSO},
                                {XN_SKEL_TORSO, XN_SKEL_LEFT_HIP},
                                {XN_SKEL_LEFT_HIP, XN_SKEL_LEFT_KNEE},
                                {XN_SKEL_LEFT_KNEE, XN_SKEL_LEFT_FOOT},
                                {XN_SKEL_TORSO, XN_SKEL_RIGHT_HIP},
                                {XN_SKEL_RIGHT_HIP, XN_SKEL_RIGHT_KNEE},
                                {XN_SKEL_RIGHT_KNEE, XN_SKEL_RIGHT_FOOT},
                                {XN_SKEL_LEFT_HIP, XN_SKEL_RIGHT_HIP}
                                };
  int numLimbs = sizeof(limbs)/(2*sizeof(XnSkeletonJoint));
  for(int i=0;i<numLimbs;i++){
    XnSkeletonJointPosition j1,j2;
    g_UserGenerator.GetSkeletonCap().GetSkeletonJointPosition(uid,limbs[i][0],j1);
    g_UserGenerator.GetSkeletonCap().GetSkeletonJointPosition(uid,limbs[i][1],j2);
    if(j1.fConfidence < 0.5 || j2.fConfidence < 0.5){continue;}
    XnPoint3D pt[2];
    pt[0] = j1.position;
    pt[1] = j2.position;
    g_DepthGenerator.ConvertRealWorldToProjective(2,pt,pt);
    cv::line(img,cv::Point(pt[0].X,pt[0].Y),cv::Point(pt[1].X,pt[1].Y),
             LINE_COLOR,LINE_THICKNESS);
  }
}

void overlayStickMan(cv::Mat &img){
  XnUserID aUsers[15];
  XnUInt16 nUsers = 15;
  g_UserGenerator.GetUsers(aUsers, nUsers);
  for (int i = 0; i < nUsers; ++i)
  {
    if (g_bDrawSkeleton && g_UserGenerator.GetSkeletonCap().IsTracking(aUsers[i]))
    {
      drawJoints(aUsers[i],img);
      drawLines(aUsers[i],img);
    }
  } 
}


void DrawStickManOpenCV(const xn::ImageMetaData& imd)
{
  const XnRGB24Pixel* pImg = imd.RGB24Data();
  assert(cvImg->isContinuous());
  unsigned char* ptrImg = cvImg->ptr();
  for(int nY = 0; nY < imd.YRes(); nY++){
    for(int nX = 0; nX < imd.XRes(); nX++){
      *ptrImg=pImg->nBlue;ptrImg++;
      *ptrImg=pImg->nGreen;ptrImg++;
      *ptrImg=pImg->nRed;ptrImg++;
      pImg++;
    }
  }

  overlayStickMan(*cvImg);
  //cv::imshow("rgbImg",cvImg); 
  //cv::waitKey(1);
}

void DrawImgMap()
{
  static bool bInitialized = false;  
  static GLuint imgTexID;
  static unsigned char* pImgTexBuf;
  static int imgTexHeight,imgTexWidth;

  float topLeftX;
  float topLeftY;
  float bottomRightY;
  float bottomRightX;
  float texXpos;
  float texYpos;

  if(!bInitialized)
  {

    imgTexWidth =  getClosestPowerOfTwo(cvImg->cols);
    imgTexHeight = getClosestPowerOfTwo(cvImg->rows);

//    printf("Initializing depth texture: width = %d, height = %d\n", texWidth, texHeight);
    imgTexID = initTexture((void**)&pImgTexBuf,imgTexWidth, imgTexHeight);

//    printf("Initialized depth texture: width = %d, height = %d\n", texWidth, texHeight);
    bInitialized = true;

    topLeftX = cvImg->cols;
    topLeftY = 0;
    bottomRightY = cvImg->rows;
    bottomRightX = 0;
    texXpos =(float)cvImg->cols/imgTexWidth;
    texYpos  =(float)cvImg->rows/imgTexHeight;

    memset(texcoords2, 0, 8*sizeof(float));
    texcoords2[0] = texXpos, texcoords2[1] = texYpos, texcoords2[2] = texXpos,
    texcoords2[7] = texYpos;
  }

  unsigned char* pDestImage = pImgTexBuf;

  if (g_bDrawPixels)
  {
    unsigned char* ptrImg = cvImg->ptr();
    for(int nY = 0; nY < cvImg->rows; nY++){
      for(int nX = 0; nX < cvImg->cols; nX++){
        pDestImage[0] = ptrImg[2];
        pDestImage[1] = ptrImg[1];
        pDestImage[2] = ptrImg[0];
        pDestImage+=3;
        ptrImg+=3;
      }
      pDestImage += (imgTexWidth - cvImg->cols)*3;
    }
  }

  glBindTexture(GL_TEXTURE_2D, imgTexID);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imgTexWidth, imgTexHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, pImgTexBuf);

  // Display the OpenGL texture map
  glColor4f(0.75,0.75,0.75,1);

  glEnable(GL_TEXTURE_2D);
  DrawTexture(cvImg->cols,cvImg->rows,0,0,texcoords2);  
  glDisable(GL_TEXTURE_2D);

}
