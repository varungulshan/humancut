// Code to list all the production nodes available 

#include <XnOpenNI.h>
#include <XnCppWrapper.h>
#include <GL/glut.h>
#include <demoConfig.h>
#include <iostream>
#include <assert.h>
#include <createDataset.h>
#include <cv.h>
#include <highgui.h>

#define CHECK_RC(nRetVal, what)                    \
  if (nRetVal != XN_STATUS_OK)                  \
  {                                \
    printf("%s failed: %s\n", what, xnGetStatusString(nRetVal));\
    return nRetVal;                        \
  }

#define CONFIG_XML_PATH DEMO_CONFIG_FILE
#define DOT_RADIUS 8
#define DOT_COLOR CV_RGB(255,0,0)
#define LINE_THICKNESS 4
#define LINE_COLOR CV_RGB(0,255,0)

//---------------------------------------------------------------------------
// Globals
//---------------------------------------------------------------------------
xn::Context g_Context;

#if (XN_PLATFORM == XN_PLATFORM_MACOSX)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#define GL_WIN_SIZE_X 1280
#define GL_WIN_SIZE_Y 480

XnBool g_bRecord = false;
xn::Recorder recorder;
#define RECORDER_DESTINATION "../../recordings/sampleRecord2.oni"
#define OUTPUT_VIDEO_PATH "../../recordings/outputSave7.avi"


//---------------------------------------------------------------------------
// Code
//---------------------------------------------------------------------------

void CleanupExit()
{
  g_Context.Shutdown();
  exit (1);
}

int main(int argc, char **argv)
{
  XnStatus nRetVal = XN_STATUS_OK;
	nRetVal = g_Context.Init();
	CHECK_RC(nRetVal, "Init");

  xn::Query query;
  xn::NodeInfoList possibleChains;
  int nodeCount;

  possibleChains.Clear();
  printf("\nListing nodes of type user\n");

  nRetVal = g_Context.EnumerateProductionTrees(XN_NODE_TYPE_USER,&query,
      possibleChains,NULL);
  CHECK_RC(nRetVal, "Enumerate user nodes");

  nodeCount = 0;
  for(xn::NodeInfoList::Iterator itNode = possibleChains.Begin(); 
      itNode != possibleChains.End();itNode++){
    xn::NodeInfo curNode = *itNode;
    const XnProductionNodeDescription& nodeInfo= curNode.GetDescription();
    XnVersion ver = nodeInfo.Version;
    printf("-------------------\n");
    printf("Node #%02d:\nName: %s\n",nodeCount,curNode.GetInstanceName());
    printf("Creation info: %s\n",curNode.GetCreationInfo());
    printf("Vendor: %s\n",nodeInfo.strVendor);
    printf("Name from Vendor: %s\n",nodeInfo.strName);
    printf("Version: %d.%d.%d.%d\n",(int)ver.nMajor,(int)ver.nMinor,
                                    (int)ver.nMaintenance,
                                    (int)ver.nBuild);
    xn::NodeInfoList dependencies = curNode.GetNeededNodes();

    printf("Listing dependencies:\n");
    int depCount=0;
    for(xn::NodeInfoList::Iterator itNode2 = dependencies.Begin(); 
        itNode2 != dependencies.End();itNode2++){
      xn::NodeInfo childNode = *itNode2;
      printf("  Dependency #%d\n",depCount);depCount++;
      printf("  Creation info: %s\n",childNode.GetCreationInfo());
      const XnProductionNodeDescription& nodeInfo = childNode.GetDescription();
      printf("  Vendor: %s\n",nodeInfo.strVendor);
      printf("  Name from Vendor: %s\n",nodeInfo.strName);
    }
    
    nodeCount++;
  }

  possibleChains.Clear();
  printf("Listing nodes of type depth\n");

  nRetVal = g_Context.EnumerateProductionTrees(XN_NODE_TYPE_DEPTH,&query,
      possibleChains,NULL);
  CHECK_RC(nRetVal, "Enumerate depth nodes");

  nodeCount = 0;
  for(xn::NodeInfoList::Iterator itNode = possibleChains.Begin(); 
      itNode != possibleChains.End();itNode++){
    xn::NodeInfo curNode = *itNode;
    const XnProductionNodeDescription& nodeInfo= curNode.GetDescription();
    XnVersion ver = nodeInfo.Version;
    printf("-------------------\n");
    printf("Node #%02d:\nName: %s\n",nodeCount,curNode.GetInstanceName());
    printf("Creation info: %s\n",curNode.GetCreationInfo());
    printf("Vendor: %s\n",nodeInfo.strVendor);
    printf("Name from Vendor: %s\n",nodeInfo.strName);
    printf("Version: %d.%d.%d.%d\n",(int)ver.nMajor,(int)ver.nMinor,
                                    (int)ver.nMaintenance,
                                    (int)ver.nBuild);
    nodeCount++;
  }

  possibleChains.Clear();
  printf("\nListing nodes of type image\n");

  nRetVal = g_Context.EnumerateProductionTrees(XN_NODE_TYPE_IMAGE,&query,
      possibleChains,NULL);
  CHECK_RC(nRetVal, "Enumerate image nodes");

  nodeCount = 0;
  for(xn::NodeInfoList::Iterator itNode = possibleChains.Begin(); 
      itNode != possibleChains.End();itNode++){
    xn::NodeInfo curNode = *itNode;
    const XnProductionNodeDescription& nodeInfo= curNode.GetDescription();
    XnVersion ver = nodeInfo.Version;
    printf("-------------------\n");
    printf("Node #%02d:\nName: %s\n",nodeCount,curNode.GetInstanceName());
    printf("Creation info: %s\n",curNode.GetCreationInfo());
    printf("Vendor: %s\n",nodeInfo.strVendor);
    printf("Name from Vendor: %s\n",nodeInfo.strName);
    printf("Version: %d.%d.%d.%d\n",(int)ver.nMajor,(int)ver.nMinor,
                                    (int)ver.nMaintenance,
                                    (int)ver.nBuild);
    nodeCount++;
  }

  possibleChains.Clear();
  printf("\nListing nodes of type device\n");

  nRetVal = g_Context.EnumerateProductionTrees(XN_NODE_TYPE_DEVICE,&query,
      possibleChains,NULL);
  CHECK_RC(nRetVal, "Enumerate device nodes");

  nodeCount = 0;
  for(xn::NodeInfoList::Iterator itNode = possibleChains.Begin(); 
      itNode != possibleChains.End();itNode++){
    xn::NodeInfo curNode = *itNode;
    const XnProductionNodeDescription& nodeInfo= curNode.GetDescription();
    XnVersion ver = nodeInfo.Version;
    printf("-------------------\n");
    printf("Node #%02d:\nName: %s\n",nodeCount,curNode.GetInstanceName());
    printf("Creation info: %s\n",curNode.GetCreationInfo());
    printf("Vendor: %s\n",nodeInfo.strVendor);
    printf("Name from Vendor: %s\n",nodeInfo.strName);
    printf("Version: %d.%d.%d.%d\n",(int)ver.nMajor,(int)ver.nMinor,
                                    (int)ver.nMaintenance,
                                    (int)ver.nBuild);
    nodeCount++;
  }

  possibleChains.Clear();
  printf("\nListing existing nodes\n");

  nRetVal = g_Context.EnumerateExistingNodes(possibleChains);
  CHECK_RC(nRetVal, "Enumerate all nodes");

  nodeCount = 0;
  for(xn::NodeInfoList::Iterator itNode = possibleChains.Begin(); 
      itNode != possibleChains.End();itNode++){
    xn::NodeInfo curNode = *itNode;
    const XnProductionNodeDescription& nodeInfo= curNode.GetDescription();
    XnVersion ver = nodeInfo.Version;
    printf("-------------------\n");
    printf("Node #%02d:\nName: %s\n",nodeCount,curNode.GetInstanceName());
    printf("Creation info: %s\n",curNode.GetCreationInfo());
    printf("Vendor: %s\n",nodeInfo.strVendor);
    printf("Name from Vendor: %s\n",nodeInfo.strName);
    printf("Version: %d.%d.%d.%d\n",(int)ver.nMajor,(int)ver.nMinor,
                                    (int)ver.nMaintenance,
                                    (int)ver.nBuild);
    nodeCount++;
  }


  return 0;

	if (argc > 1){
		nRetVal = g_Context.Init();
		CHECK_RC(nRetVal, "Init");
		nRetVal = g_Context.OpenFileRecording(argv[1]);
		if (nRetVal != XN_STATUS_OK)
		{
			printf("Can't open recording %s: %s\n", argv[1], xnGetStatusString(nRetVal));
			return 1;
		}
  }else{
    xn::EnumerationErrors errors;
    nRetVal = g_Context.InitFromXmlFile(CONFIG_XML_PATH, &errors);
    if (nRetVal == XN_STATUS_NO_NODE_PRESENT)
    {
      XnChar strError[1024];
      errors.ToString(strError, 1024);
      printf("%s\n", strError);
      return (nRetVal);
    }
    else if (nRetVal != XN_STATUS_OK)
    {
      printf("Open failed: %s\n", xnGetStatusString(nRetVal));
      return (nRetVal);
    }
  }

}

