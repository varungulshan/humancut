function convertLabels(dirName)

imgExtension = 'png';
colormap=repmat([0 0 1],[256 1]); % Blue everywhere
colormap(1,:)=[0 0 0];
colormap(2,:)=[1 0 0];
colormap(256,:)=[1 1 1];

dirNames = {dirName};

for i=1:length(dirNames)
  curDir = dirNames{i};
  fprintf('Scanning folder: %s\n',dirNames{i});
  idxImg = 0;
  allFiles = dir([curDir 'label*.' imgExtension]);
  for j=1:length(allFiles)
    labelFile = [curDir allFiles(j).name];
    labelImg = imread(labelFile);
    imwrite(labelImg,colormap,labelFile,'png');
  end
end
