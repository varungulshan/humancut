#include <segProcessor.h>

XnFloat Colors[][3] = {
  {0,1,1},
  {0,0,1},
  {0,1,0},
  {1,1,0},
  {1,0,0},
  {1,.5,0},
  {.5,1,0},
  {0,.5,1},
  {.5,0,1},
  {1,1,.5},
  {1,1,1}
};
XnUInt32 nColors = 10;

unsigned int segProcessor::getClosestPowerOfTwo(unsigned int n)
{
	unsigned int m = 2;
	while(m < n) m<<=1;
	return m;
}


segProcessor::segProcessor(xn::DepthGenerator &dG,xn::ImageGenerator &imgG,
                 xn::UserGenerator &uG) : depthG(dG), imageG(imgG), userG(uG){

  //depthG = dG;imageG = imgG;userG = uG;

  depthG.GetMetaData(dmd);
  imageG.GetMetaData(imd);
  userG.GetUserPixels(0,smd);

  // Initialize the depth texture information
  depthTexW =  getClosestPowerOfTwo(dmd.XRes());
  depthTexH = getClosestPowerOfTwo(dmd.YRes());
  depthTexBuffer = new unsigned char[depthTexW*depthTexH*4];

  float topLeftX;
  float topLeftY;
  float bottomRightY;
  float bottomRightX;
  float texXpos;
  float texYpos;

  topLeftX = dmd.XRes();
  topLeftY = 0;
  bottomRightY = dmd.YRes();
  bottomRightX = 0;
  texXpos =(float)dmd.XRes()/depthTexW;
  texYpos  =(float)dmd.YRes()/depthTexH;

  memset(texcoordsDepth, 0, 8*sizeof(float));
  texcoordsDepth[0] = texXpos, texcoordsDepth[1] = texYpos,
  texcoordsDepth[2] = texXpos, texcoordsDepth[7] = texYpos;
  
  // Initialize the image texture information
  imgTexWidth =  getClosestPowerOfTwo(imd.XRes());
  imgTexHeight = getClosestPowerOfTwo(imd.YRes());
  imageTexBuffer = new unsigned char[imgTexWidth*imgTexHeight*4];

  topLeftX = imd.XRes();
  topLeftY = 0;
  bottomRightY = imd.YRes();
  bottomRightX = 0;
  texXpos =(float)imd.XRes()/imgTexWidth;
  texYpos  =(float)imd.YRes()/imgTexHeight;

  memset(texcoordsImage, 0, 8*sizeof(float));
  texcoordsImage[0] = texXpos, texcoordsImage[1] = texYpos, texcoordsImage[2] = texXpos,
  texcoordsImage[7] = texYpos;

  // Initialize labelimages
  double xScale = (double)imd.XRes()/dmd.XRes();
  double yScale = (double)imd.YRes()/dmd.YRes();
  assert(yScale>=xScale);
  assert(round(xScale)==xScale);
  assert(xScale>=1);
  //printf("dmd.YRes()=%d\n",dmd.YRes());
  //labelImg_big = cv::Mat::zeros(dmd.YRes()*(unsigned int)xScale,imd.XRes(),CV_16UC1);
  labelImg_big = new cv::Mat(dmd.YRes()*(unsigned int)xScale,imd.XRes(),CV_16UC1);
  short int *tmpPtr;
  tmpPtr = (short int*)labelImg_big->ptr();
  for(int i=0;i<=labelImg_big->rows*labelImg_big->cols;i++,tmpPtr++)
      {*tmpPtr=0;}
  //printf("labelImg_big W=%d,H=%d\n",labelImg_big.cols,labelImg_big.rows);
  //labelImg = cv::Mat::zeros(imd.YRes(),imd.XRes(),CV_16UC1);
  labelImg = new cv::Mat(imd.YRes(),imd.XRes(),CV_16UC1);
  tmpPtr = (short int*)labelImg->ptr();
  for(int i=0;i<=labelImg->rows*labelImg->cols;i++,tmpPtr++)
      {*tmpPtr=0;}
  assert(sizeof(unsigned short int)==2);

  imgH = imd.YRes();imgW = imd.XRes(); depthH = dmd.YRes(); depthW = dmd.XRes();

  debayeredImg = new cv::Mat(imgH,imgW,CV_8UC3);
  //debayeredImg = cv::Mat::zeros(imgH,imgW,CV_8UC3);
  //imd.Free();
  //dmd.Free();
}

segProcessor::~segProcessor()
{
  delete(depthTexBuffer);
  delete(imageTexBuffer);
  delete(labelImg_big);
  delete(labelImg);
  delete(debayeredImg);
}

cv::Mat segProcessor::getImageOpenCV(){
  cv::Mat img(imgH,imgW,CV_8UC3);  
  assert(img.isContinuous());
  unsigned char *texBuffer = imageTexBuffer;
  unsigned char *ptrImg = img.ptr();
  for(int y=0;y<imgH;y++){
    for(int x=0;x<imgW;x++){
      ptrImg[2]=texBuffer[0];
      ptrImg[1]=texBuffer[1];
      ptrImg[0]=texBuffer[2];
      ptrImg+=3;texBuffer+=3;
    }
    texBuffer += (imgTexWidth - imgW)*3;
  }
  return img;
}

unsigned char* segProcessor::getImageTexture(){

  const XnLabel* userLabels = smd.Data(); // It is uint16 type
  const cv::Mat labelImg_orig(smd.YRes(),smd.XRes(),CV_16UC1,(void*)userLabels);

  //printf("labelImg_big W=%d,H=%d\n",labelImg_big.cols,labelImg_big.rows);
  cv::resize(labelImg_orig,*labelImg_big,labelImg_big->size(),0,0,cv::INTER_NEAREST);
  assert(labelImg_orig.ptr()!=labelImg_big->ptr());

  assert(labelImg_big->cols == imd.XRes());
  assert(labelImg_big->cols == labelImg->cols);
  assert(labelImg_big->rows <= labelImg->rows);

  unsigned short int* ptrLabelImg = (unsigned short int*)labelImg->ptr();
  unsigned short int* ptrLabelImg_big = (unsigned short int*)labelImg_big->ptr();
  for(int i=0;i<labelImg_big->cols * labelImg_big->rows;i++){
    *ptrLabelImg = *ptrLabelImg_big;
    ptrLabelImg_big++;ptrLabelImg++;
  }

  ptrLabelImg = (unsigned short int*)labelImg->ptr();
  //const XnRGB24Pixel* pImg = imd.RGB24Data();
  const unsigned char* pImg = debayeredImg->ptr();
  unsigned char* pDestImage2 = imageTexBuffer;

  assert(debayeredImg->rows == imd.YRes() && debayeredImg->cols == imd.XRes() );
  for(int nY = 0; nY < imd.YRes(); nY++){
    for(int nX = 0; nX < imd.XRes(); nX++){
      if(*ptrLabelImg==0){
        pDestImage2[0] = pImg[2];
        pDestImage2[1] = pImg[1];
        pDestImage2[2] = pImg[0];
      }else{
        double r=pImg[2],g=pImg[1],b=pImg[0];
        int colorId = (*ptrLabelImg)%nColors;
        r = (1-LABEL_ALPHA)*r + 255*LABEL_ALPHA*Colors[colorId][0];
        g = (1-LABEL_ALPHA)*g + 255*LABEL_ALPHA*Colors[colorId][1];
        b = (1-LABEL_ALPHA)*b + 255*LABEL_ALPHA*Colors[colorId][2];
        pDestImage2[0] = (unsigned char)r;
        pDestImage2[1] = (unsigned char)g;
        pDestImage2[2] = (unsigned char)b;
      }
      pDestImage2+=3;
      pImg+=3;ptrLabelImg++;
    }
    pDestImage2 += (imgTexWidth - imd.XRes())*3;
  }

  return imageTexBuffer;
}

void segProcessor::newFrameArrived(){
  dmd.Free();
  imd.Free();
  smd.Free();
  depthG.GetMetaData(dmd);
  imageG.GetMetaData(imd);
  userG.GetUserPixels(0,smd);
  updateDebayeredImg();
}

#define AVG(a,b) (((int)(a) + (int)(b)) >> 1)
#define AVG3(a,b,c) (((int)(a) + (int)(b) + (int)(c)) / 3)
#define AVG4(a,b,c,d) (((int)(a) + (int)(b) + (int)(c) + (int)(d)) >> 2)
#define WAVG4(a,b,c,d,x,y)  ( ( ((int)(a) + (int)(b)) * (int)(x) + ((int)(c) + (int)(d)) * (int)(y) ) / ( 2 * ((int)(x) + (int(y))) ) )

void segProcessor::updateDebayeredImg()
{
  if (imd.XRes() == debayeredImg->cols && imd.YRes() == debayeredImg->rows)
  {
    register const XnUInt8 *bayer_pixel = imd.Data();
    register unsigned yIdx, xIdx;

    int line_step = debayeredImg->cols;
    int line_step2 = debayeredImg->cols << 1;

    int rgb_line_step  = line_step * 3;             // previous color line
    register unsigned char *rgb_pixel = (unsigned char *)debayeredImg->ptr();

    if (true)
    {
      // first two pixel values for first two lines
      // Bayer         0 1 2
      //         0     G r g
      // line_step     b g b
      // line_step2    g r g

      rgb_pixel[3] = rgb_pixel[0] = bayer_pixel[1];    // red pixel
      rgb_pixel[1] = bayer_pixel[0];    // green pixel
      rgb_pixel[rgb_line_step + 2] = rgb_pixel[2] = bayer_pixel[line_step]; // blue;

      // Bayer         0 1 2
      //         0     g R g
      // line_step     b g b
      // line_step2    g r g
      //rgb_pixel[3] = bayer_pixel[1];
      rgb_pixel[4] = AVG3( bayer_pixel[0], bayer_pixel[2], bayer_pixel[line_step+1] );
      rgb_pixel[rgb_line_step + 5] = rgb_pixel[5] = AVG( bayer_pixel[line_step], bayer_pixel[line_step+2] );

      // BGBG line
      // Bayer         0 1 2
      //         0     g r g
      // line_step     B g b
      // line_step2    g r g
      rgb_pixel[rgb_line_step + 3] = rgb_pixel[rgb_line_step    ] = AVG( bayer_pixel[1] , bayer_pixel[line_step2+1] );
      rgb_pixel[rgb_line_step + 1] = AVG3( bayer_pixel[0] , bayer_pixel[line_step+1] , bayer_pixel[line_step2] );
      //rgb_pixel[rgb_line_step + 2] = bayer_pixel[line_step];

      // pixel (1, 1)  0 1 2
      //         0     g r g
      // line_step     b G b
      // line_step2    g r g
      //rgb_pixel[rgb_line_step + 3] = AVG( bayer_pixel[1] , bayer_pixel[line_step2+1] );
      rgb_pixel[rgb_line_step + 4] = bayer_pixel[line_step+1];
      //rgb_pixel[rgb_line_step + 5] = AVG( bayer_pixel[line_step] , bayer_pixel[line_step+2] );

      rgb_pixel += 6;
      bayer_pixel += 2;
      // rest of the first two lines
      for (xIdx = 2; xIdx < debayeredImg->cols - 2; xIdx += 2, rgb_pixel += 6, bayer_pixel += 2)
      {
        // GRGR line
        // Bayer        -1 0 1 2
        //           0   r G r g
        //   line_step   g b g b
        // line_step2    r g r g
        rgb_pixel[0] = AVG( bayer_pixel[1], bayer_pixel[-1]);
        rgb_pixel[1] = bayer_pixel[0];
        rgb_pixel[2] = bayer_pixel[line_step + 1];

        // Bayer        -1 0 1 2
        //          0    r g R g
        //  line_step    g b g b
        // line_step2    r g r g
        rgb_pixel[3] = bayer_pixel[1];
        rgb_pixel[4] = AVG3( bayer_pixel[0], bayer_pixel[2], bayer_pixel[line_step+1] );
        rgb_pixel[rgb_line_step + 5] = rgb_pixel[5] = AVG( bayer_pixel[line_step], bayer_pixel[line_step+2] );

        // BGBG line
        // Bayer         -1 0 1 2
        //         0      r g r g
        // line_step      g B g b
        // line_step2     r g r g
        rgb_pixel[rgb_line_step    ] = AVG4( bayer_pixel[1] , bayer_pixel[line_step2+1], bayer_pixel[-1] , bayer_pixel[line_step2-1] );
        rgb_pixel[rgb_line_step + 1] = AVG4( bayer_pixel[0] , bayer_pixel[line_step2], bayer_pixel[line_step-1], bayer_pixel[line_step+1] );
        rgb_pixel[rgb_line_step + 2] = bayer_pixel[line_step];

        // Bayer         -1 0 1 2
        //         0      r g r g
        // line_step      g b G b
        // line_step2     r g r g
        rgb_pixel[rgb_line_step + 3] = AVG( bayer_pixel[1] , bayer_pixel[line_step2+1] );
        rgb_pixel[rgb_line_step + 4] = bayer_pixel[line_step+1];
        //rgb_pixel[rgb_line_step + 5] = AVG( bayer_pixel[line_step] , bayer_pixel[line_step+2] );
      }

      // last two pixel values for first two lines
      // GRGR line
      // Bayer        -1 0 1
      //           0   r G r
      //   line_step   g b g
      // line_step2    r g r
      rgb_pixel[0] = AVG( bayer_pixel[1], bayer_pixel[-1]);
      rgb_pixel[1] = bayer_pixel[0];
      rgb_pixel[rgb_line_step + 5] = rgb_pixel[rgb_line_step + 2] = rgb_pixel[5] = rgb_pixel[2] = bayer_pixel[line_step];

      // Bayer        -1 0 1
      //          0    r g R
      //  line_step    g b g
      // line_step2    r g r
      rgb_pixel[3] = bayer_pixel[1];
      rgb_pixel[4] = AVG( bayer_pixel[0], bayer_pixel[line_step+1] );
      //rgb_pixel[5] = bayer_pixel[line_step];

      // BGBG line
      // Bayer        -1 0 1
      //          0    r g r
      //  line_step    g B g
      // line_step2    r g r
      rgb_pixel[rgb_line_step    ] = AVG4( bayer_pixel[1] , bayer_pixel[line_step2+1], bayer_pixel[-1] , bayer_pixel[line_step2-1] );
      rgb_pixel[rgb_line_step + 1] = AVG4( bayer_pixel[0] , bayer_pixel[line_step2], bayer_pixel[line_step-1], bayer_pixel[line_step+1] );
      //rgb_pixel[rgb_line_step + 2] = bayer_pixel[line_step];

      // Bayer         -1 0 1
      //         0      r g r
      // line_step      g b G
      // line_step2     r g r
      rgb_pixel[rgb_line_step + 3] = AVG( bayer_pixel[1] , bayer_pixel[line_step2+1] );
      rgb_pixel[rgb_line_step + 4] = bayer_pixel[line_step+1];
      //rgb_pixel[rgb_line_step + 5] = bayer_pixel[line_step];

      bayer_pixel += line_step + 2;
      rgb_pixel += rgb_line_step + 6;

      // main processing
      for (yIdx = 2; yIdx < debayeredImg->cols-2; yIdx += 2)
      {
        // first two pixel values
        // Bayer         0 1 2
        //        -1     b g b
        //         0     G r g
        // line_step     b g b
        // line_step2    g r g

        rgb_pixel[3] = rgb_pixel[0] = bayer_pixel[1];    // red pixel
        rgb_pixel[1] = bayer_pixel[0];    // green pixel
        rgb_pixel[2] = AVG( bayer_pixel[line_step], bayer_pixel[-line_step] ); // blue;

        // Bayer         0 1 2
        //        -1     b g b
        //         0     g R g
        // line_step     b g b
        // line_step2    g r g
        //rgb_pixel[3] = bayer_pixel[1];
        rgb_pixel[4] = AVG4( bayer_pixel[0], bayer_pixel[2], bayer_pixel[line_step+1], bayer_pixel[1-line_step] );
        rgb_pixel[5] = AVG4( bayer_pixel[line_step], bayer_pixel[line_step+2], bayer_pixel[-line_step], bayer_pixel[2-line_step]);

        // BGBG line
        // Bayer         0 1 2
        //         0     g r g
        // line_step     B g b
        // line_step2    g r g
        rgb_pixel[rgb_line_step + 3] = rgb_pixel[rgb_line_step    ] = AVG( bayer_pixel[1] , bayer_pixel[line_step2+1] );
        rgb_pixel[rgb_line_step + 1] = AVG3( bayer_pixel[0] , bayer_pixel[line_step+1] , bayer_pixel[line_step2] );
        rgb_pixel[rgb_line_step + 2] = bayer_pixel[line_step];

        // pixel (1, 1)  0 1 2
        //         0     g r g
        // line_step     b G b
        // line_step2    g r g
        //rgb_pixel[rgb_line_step + 3] = AVG( bayer_pixel[1] , bayer_pixel[line_step2+1] );
        rgb_pixel[rgb_line_step + 4] = bayer_pixel[line_step+1];
        rgb_pixel[rgb_line_step + 5] = AVG( bayer_pixel[line_step] , bayer_pixel[line_step+2] );

        rgb_pixel += 6;
        bayer_pixel += 2;
        // continue with rest of the line
        for (xIdx = 2; xIdx < debayeredImg->cols - 2; xIdx += 2, rgb_pixel += 6, bayer_pixel += 2)
        {
          // GRGR line
          // Bayer        -1 0 1 2
          //          -1   g b g b
          //           0   r G r g
          //   line_step   g b g b
          // line_step2    r g r g
          rgb_pixel[0] = AVG( bayer_pixel[1], bayer_pixel[-1]);
          rgb_pixel[1] = bayer_pixel[0];
          rgb_pixel[2] = AVG( bayer_pixel[line_step], bayer_pixel[-line_step] );

          // Bayer        -1 0 1 2
          //          -1   g b g b
          //          0    r g R g
          //  line_step    g b g b
          // line_step2    r g r g
          rgb_pixel[3] = bayer_pixel[1];
          rgb_pixel[4] = AVG4( bayer_pixel[0], bayer_pixel[2], bayer_pixel[line_step+1], bayer_pixel[1-line_step] );
          rgb_pixel[5] = AVG4( bayer_pixel[-line_step], bayer_pixel[2-line_step], bayer_pixel[line_step], bayer_pixel[line_step+2]);

          // BGBG line
          // Bayer         -1 0 1 2
          //         -1     g b g b
          //          0     r g r g
          // line_step      g B g b
          // line_step2     r g r g
          rgb_pixel[rgb_line_step    ] = AVG4( bayer_pixel[1], bayer_pixel[line_step2+1], bayer_pixel[-1], bayer_pixel[line_step2-1] );
          rgb_pixel[rgb_line_step + 1] = AVG4( bayer_pixel[0], bayer_pixel[line_step2], bayer_pixel[line_step-1], bayer_pixel[line_step+1] );
          rgb_pixel[rgb_line_step + 2] = bayer_pixel[line_step];

          // Bayer         -1 0 1 2
          //         -1     g b g b
          //          0     r g r g
          // line_step      g b G b
          // line_step2     r g r g
          rgb_pixel[rgb_line_step + 3] = AVG( bayer_pixel[1] , bayer_pixel[line_step2+1] );
          rgb_pixel[rgb_line_step + 4] = bayer_pixel[line_step+1];
          rgb_pixel[rgb_line_step + 5] = AVG( bayer_pixel[line_step] , bayer_pixel[line_step+2] );
        }

        // last two pixels of the line
        // last two pixel values for first two lines
        // GRGR line
        // Bayer        -1 0 1
        //           0   r G r
        //   line_step   g b g
        // line_step2    r g r
        rgb_pixel[0] = AVG( bayer_pixel[1], bayer_pixel[-1]);
        rgb_pixel[1] = bayer_pixel[0];
        rgb_pixel[rgb_line_step + 5] = rgb_pixel[rgb_line_step + 2] = rgb_pixel[5] = rgb_pixel[2] = bayer_pixel[line_step];

        // Bayer        -1 0 1
        //          0    r g R
        //  line_step    g b g
        // line_step2    r g r
        rgb_pixel[3] = bayer_pixel[1];
        rgb_pixel[4] = AVG( bayer_pixel[0], bayer_pixel[line_step+1] );
        //rgb_pixel[5] = bayer_pixel[line_step];

        // BGBG line
        // Bayer        -1 0 1
        //          0    r g r
        //  line_step    g B g
        // line_step2    r g r
        rgb_pixel[rgb_line_step    ] = AVG4( bayer_pixel[1] , bayer_pixel[line_step2+1], bayer_pixel[-1] , bayer_pixel[line_step2-1] );
        rgb_pixel[rgb_line_step + 1] = AVG4( bayer_pixel[0] , bayer_pixel[line_step2], bayer_pixel[line_step-1], bayer_pixel[line_step+1] );
        //rgb_pixel[rgb_line_step + 2] = bayer_pixel[line_step];

        // Bayer         -1 0 1
        //         0      r g r
        // line_step      g b G
        // line_step2     r g r
        rgb_pixel[rgb_line_step + 3] = AVG( bayer_pixel[1] , bayer_pixel[line_step2+1] );
        rgb_pixel[rgb_line_step + 4] = bayer_pixel[line_step+1];
        //rgb_pixel[rgb_line_step + 5] = bayer_pixel[line_step];

        bayer_pixel += line_step + 2;
        rgb_pixel += rgb_line_step + 6;
      }

      //last two lines
      // Bayer         0 1 2
      //        -1     b g b
      //         0     G r g
      // line_step     b g b

      rgb_pixel[rgb_line_step + 3] = rgb_pixel[rgb_line_step    ] = rgb_pixel[3] = rgb_pixel[0] = bayer_pixel[1];    // red pixel
      rgb_pixel[1] = bayer_pixel[0];    // green pixel
      rgb_pixel[rgb_line_step + 2] = rgb_pixel[2] = bayer_pixel[line_step]; // blue;

      // Bayer         0 1 2
      //        -1     b g b
      //         0     g R g
      // line_step     b g b
      //rgb_pixel[3] = bayer_pixel[1];
      rgb_pixel[4] = AVG4( bayer_pixel[0], bayer_pixel[2], bayer_pixel[line_step+1], bayer_pixel[1-line_step] );
      rgb_pixel[5] = AVG4( bayer_pixel[line_step], bayer_pixel[line_step+2], bayer_pixel[-line_step], bayer_pixel[2-line_step]);

      // BGBG line
      // Bayer         0 1 2
      //        -1     b g b
      //         0     g r g
      // line_step     B g b
      //rgb_pixel[rgb_line_step    ] = bayer_pixel[1];
      rgb_pixel[rgb_line_step + 1] = AVG( bayer_pixel[0] , bayer_pixel[line_step+1] );
      rgb_pixel[rgb_line_step + 2] = bayer_pixel[line_step];

      // Bayer         0 1 2
      //        -1     b g b
      //         0     g r g
      // line_step     b G b
      //rgb_pixel[rgb_line_step + 3] = AVG( bayer_pixel[1] , bayer_pixel[line_step2+1] );
      rgb_pixel[rgb_line_step + 4] = bayer_pixel[line_step+1];
      rgb_pixel[rgb_line_step + 5] = AVG( bayer_pixel[line_step] , bayer_pixel[line_step+2] );

      rgb_pixel += 6;
      bayer_pixel += 2;
      // rest of the last two lines
      for (xIdx = 2; xIdx < debayeredImg->cols - 2; xIdx += 2, rgb_pixel += 6, bayer_pixel += 2)
      {
        // GRGR line
        // Bayer       -1 0 1 2
        //        -1    g b g b
        //         0    r G r g
        // line_step    g b g b
        rgb_pixel[0] = AVG( bayer_pixel[1], bayer_pixel[-1]);
        rgb_pixel[1] = bayer_pixel[0];
        rgb_pixel[2] = AVG( bayer_pixel[line_step], bayer_pixel[-line_step]);

        // Bayer       -1 0 1 2
        //        -1    g b g b
        //         0    r g R g
        // line_step    g b g b
        rgb_pixel[rgb_line_step + 3] = rgb_pixel[3] = bayer_pixel[1];
        rgb_pixel[4] = AVG4( bayer_pixel[0], bayer_pixel[2], bayer_pixel[line_step+1], bayer_pixel[1-line_step] );
        rgb_pixel[5] = AVG4( bayer_pixel[line_step], bayer_pixel[line_step+2], bayer_pixel[-line_step], bayer_pixel[-line_step+2] );

        // BGBG line
        // Bayer       -1 0 1 2
        //        -1    g b g b
        //         0    r g r g
        // line_step    g B g b
        rgb_pixel[rgb_line_step    ] = AVG( bayer_pixel[-1], bayer_pixel[1] );
        rgb_pixel[rgb_line_step + 1] = AVG3( bayer_pixel[0], bayer_pixel[line_step-1], bayer_pixel[line_step+1] );
        rgb_pixel[rgb_line_step + 2] = bayer_pixel[line_step];


        // Bayer       -1 0 1 2
        //        -1    g b g b
        //         0    r g r g
        // line_step    g b G b
        //rgb_pixel[rgb_line_step + 3] = bayer_pixel[1];
        rgb_pixel[rgb_line_step + 4] = bayer_pixel[line_step+1];
        rgb_pixel[rgb_line_step + 5] = AVG( bayer_pixel[line_step] , bayer_pixel[line_step+2] );
      }

      // last two pixel values for first two lines
      // GRGR line
      // Bayer       -1 0 1
      //        -1    g b g
      //         0    r G r
      // line_step    g b g
      rgb_pixel[rgb_line_step    ] = rgb_pixel[0] = AVG( bayer_pixel[1], bayer_pixel[-1]);
      rgb_pixel[1] = bayer_pixel[0];
      rgb_pixel[5] = rgb_pixel[2] = AVG( bayer_pixel[line_step], bayer_pixel[-line_step]);

      // Bayer       -1 0 1
      //        -1    g b g
      //         0    r g R
      // line_step    g b g
      rgb_pixel[rgb_line_step + 3] = rgb_pixel[3] = bayer_pixel[1];
      rgb_pixel[4] = AVG3( bayer_pixel[0], bayer_pixel[line_step+1], bayer_pixel[-line_step+1] );
      //rgb_pixel[5] = AVG( bayer_pixel[line_step], bayer_pixel[-line_step] );

      // BGBG line
      // Bayer       -1 0 1
      //        -1    g b g
      //         0    r g r
      // line_step    g B g
      //rgb_pixel[rgb_line_step    ] = AVG2( bayer_pixel[-1], bayer_pixel[1] );
      rgb_pixel[rgb_line_step + 1] = AVG3( bayer_pixel[0] , bayer_pixel[line_step-1], bayer_pixel[line_step+1] );
      rgb_pixel[rgb_line_step + 5] = rgb_pixel[rgb_line_step + 2] = bayer_pixel[line_step];

      // Bayer       -1 0 1
      //        -1    g b g
      //         0    r g r
      // line_step    g b G
      //rgb_pixel[rgb_line_step + 3] = bayer_pixel[1];
      rgb_pixel[rgb_line_step + 4] = bayer_pixel[line_step+1];
      //rgb_pixel[rgb_line_step + 5] = bayer_pixel[line_step];
    }
  }
  else
  {
    printf("This debayering not supported\n");
    exit(-1);
  }
  cv::cvtColor(*debayeredImg,*debayeredImg,CV_RGB2BGR);
}
 
unsigned char* segProcessor::getDepthTexture(){

  unsigned int nValue = 0;
  unsigned int nHistValue = 0;
  unsigned int nIndex = 0;
  unsigned int nX = 0;
  unsigned int nY = 0;
  unsigned int nNumberOfPoints = 0;
  XnUInt16 g_nXRes = dmd.XRes();
  XnUInt16 g_nYRes = dmd.YRes();

  const XnDepthPixel* pDepth = dmd.Data();
  const XnLabel* pLabels = smd.Data();

  // Calculate the accumulative histogram
  memset(g_pDepthHist, 0, MAX_DEPTH*sizeof(float));
  for (nY=0; nY<g_nYRes; nY++)
  {
    for (nX=0; nX<g_nXRes; nX++)
    {
      nValue = *pDepth;

      if (nValue != 0)
      {
        g_pDepthHist[nValue]++;
        nNumberOfPoints++;
      }
      pDepth++;
    }
  }

  for (nIndex=1; nIndex<MAX_DEPTH; nIndex++)
  {
    g_pDepthHist[nIndex] += g_pDepthHist[nIndex-1];
  }
  if (nNumberOfPoints)
  {
    for (nIndex=1; nIndex<MAX_DEPTH; nIndex++)
    {
      g_pDepthHist[nIndex] = (unsigned int)(256 * (1.0f - (g_pDepthHist[nIndex] / nNumberOfPoints)));
    }
  }

  pDepth = dmd.Data();
  unsigned char* pDestImage = depthTexBuffer;

  nIndex = 0;
  // Prepare the texture map
  for (nY=0; nY<g_nYRes; nY++)
  {
    for (nX=0; nX < g_nXRes; nX++, nIndex++)
    {

      pDestImage[0] = 0;
      pDestImage[1] = 0;
      pDestImage[2] = 0;
      if (true)
      {
        nValue = *pDepth;
        XnLabel label = *pLabels;
        XnUInt32 nColorID = label % nColors;
        if (label == 0)
        {
          nColorID = nColors;
        }

        if (nValue != 0)
        {
          nHistValue = g_pDepthHist[nValue];

          pDestImage[0] = nHistValue * Colors[nColorID][0]; 
          pDestImage[1] = nHistValue * Colors[nColorID][1];
          pDestImage[2] = nHistValue * Colors[nColorID][2];
        }
      }

      pDepth++;
      pLabels++;
      pDestImage+=3;
    }
    pDestImage += (imgTexWidth - g_nXRes) *3;
  }

  return depthTexBuffer;
}
