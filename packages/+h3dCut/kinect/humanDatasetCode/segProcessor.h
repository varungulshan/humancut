#ifndef SEG_PROCESSOR_H
#define SEG_PROCESSOR_H

#include <XnOpenNI.h>
#include <XnCppWrapper.h>
#include <iostream>
#include <assert.h>
#include <cv.h>

#define MAX_DEPTH 10000
#define LABEL_ALPHA 0.5

extern XnFloat Colors[][3];
extern XnUInt32 nColors;

class segProcessor{
  private:
    xn::DepthGenerator &depthG;
    xn::ImageGenerator &imageG;
    xn::UserGenerator &userG;
    unsigned char* depthTexBuffer;
    unsigned char* imageTexBuffer;
    float g_pDepthHist[MAX_DEPTH];
    unsigned int getClosestPowerOfTwo(unsigned int n);
    cv::Mat *labelImg_big; // To store the resized labels
    cv::Mat *labelImg; // To store the labels at the resolution of the img
    void updateDebayeredImg(); // Debayers the recently arrived image

  public:
    xn::ImageMetaData imd;
    xn::DepthMetaData dmd;
    xn::SceneMetaData smd;
    cv::Mat *debayeredImg;
    float texcoordsDepth[8];
    int depthTexH,depthTexW;
    float texcoordsImage[8];
    int imgTexWidth,imgTexHeight;
    int imgH,imgW,depthH,depthW;
    segProcessor(xn::DepthGenerator &dG,xn::ImageGenerator &imgG,
                 xn::UserGenerator &uG);
    // Returns the depth texture to render for openGL
    void newFrameArrived(); // Updates the meta datas
    unsigned char* getDepthTexture();
    unsigned char* getImageTexture();
    cv::Mat getImageOpenCV();
    ~segProcessor();
};

#endif
