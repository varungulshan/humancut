#include <datasetManager.h>
#include <string>
#include <iostream>
#include <fstream>
#include <yaml.h>
#include <cstdlib>
#include <sys/stat.h>
//#include <curses.h>

//#define DATASET_CONFIG_FILE "../../configs/datasetConfig.xml"
#define DATASET_CONFIG_FILE "../../configs/datasetConfig.yaml"
#define MATLAB_SCRIPT "../../scripts/convertLabels.sh "
#define ENABLE_CONFIG_CHECK true
#define MAX_DIR_STRING_LENGTH 5000
#define NUM_RECORDED_TICK 10
#define NUM_BURNT_TICK 5

datasetManager::datasetManager() 
{
  initConfigFromYaml();
}

void datasetManager::initConfigFromYaml(){
  std::ifstream fin(DATASET_CONFIG_FILE);
  YAML::Parser parser(fin);

  YAML::Node doc;
  parser.GetNextDocument(doc);
  doc["name"]>>name;
  doc["location"]>>location;
  doc["datasetDir"]>>datasetDir;
  std::string tmp;
  doc["framesPerRecording"]>>tmp;
  framesPerRecording = std::atoi(tmp.c_str());
  doc["maxObjects"]>>tmp;
  maxObjects = std::atoi(tmp.c_str());
  doc["drawTimeStamp"]>>tmp;
  drawTimeStamp = (tmp=="true");
  doc["syncEnable"]>>tmp;
  syncEnable = (tmp=="true");
  doc["syncTolerance"]>>tmp;
  syncTolerance = std::atoi(tmp.c_str());
  doc["burnIn"]>>tmp;
  burnInFrames = std::atoi(tmp.c_str());
  confirmConfig();
  fin.close();
}

void datasetManager::confirmConfig(){
  printf("Name: %s\n",name.c_str());
  printf("Location: %s\n",location.c_str());
  printf("Dataset dir: %s\n",datasetDir.c_str());
  printf("Frames/recording: %d\n",framesPerRecording);
  printf("Max objects: %d\n",maxObjects);
  printf("Sync enable: %s, ",syncEnable?"true":"false");
  printf("Sync tolerance: %d\n",syncTolerance);
  printf("Burn in frames: %d\n",burnInFrames);
  if(ENABLE_CONFIG_CHECK){
    //initscr();
    //timeout(-1);
    printf("Are the above settings correct? (y/n)[n]\n");
    char userResp = (char)getchar();
    //endwin();
    if(userResp != 'y'){exit(-1);} 
    printf("Initializing recording ...\n");
  }
}

void datasetManager::startManager(const xn::ImageMetaData &imd,
                                  const xn::SceneMetaData &smd){

  imageW = imd.XRes(); imageH = imd.YRes();
  labelsW = smd.XRes(); labelsH = smd.YRes();

  setupRecording();
  tmpLabels = new cv::Mat(labelsH,labelsW,CV_8UC1);
  startTime = time(NULL);
  //tmpLabels = cv::Mat::zeros(labelsH,labelsW,CV_8UC1);
}

void datasetManager::setupRecording(){
  numBurnt = 0;
  numRecorded = 0; 
  if(maxObjects>1){numPeople="multiple";}
  else{numPeople="individual";}
  std::string dir_prefix = datasetDir+name+"_"+location+"_"+numPeople+"_";
  char dirStr[MAX_DIR_STRING_LENGTH];
  int dirCount = 1;
  struct stat sb; 
  while(true){
    snprintf(dirStr,MAX_DIR_STRING_LENGTH,"%s%02d",dir_prefix.c_str(),dirCount);
    if(stat(dirStr,&sb) == 0){
      dirCount++;
      if(dirCount>=100){
        printf("Too many directories exist, exiting\n");
        exit(-1);
      }
    }
    else{break;}
  }
  if(mkdir(dirStr,0755) != 0){
    printf("Could not create output directory: %s\n",dirStr);
    exit(-1);
  }
  outputDir = dirStr;
  outputDir += "/";
  imgName_prefix = outputDir+"image_";
  labelName_prefix = outputDir+"labels_"; 
}

int datasetManager::getNumberOfUsers(xn::UserGenerator &userG){

  XnUserID userIds[10];
  XnUInt16 nUsers = 10;
  userG.GetUsers(userIds,nUsers);

  int visibleUsers = nUsers; 

  for(int i=0;i<nUsers;i++){
    XnPoint3D com;
    userG.GetCoM(userIds[i],com);
    /*
    printf("User at: %.2f,%.2f,%.2f\n",(double)com.X,(double)com.Y,
                                       (double)com.Z);
    */
    if(com.X==0 && com.Y==0 && com.Z==0){visibleUsers--;};
  }

  return visibleUsers;
}

bool datasetManager::newFrameArrived(const xn::DepthMetaData &dmd,
                                     cv::Mat &debayeredImg,
                                     xn::UserGenerator &userG,
                                     XnUInt64 depthTimeStamp,
                                     XnUInt64 imgTimeStamp){

  /*
  XnUserID userIds[10];
  XnUInt16 nUsers = 10;
  userG.GetUsers(userIds,nUsers);
  */

  int nUsers = getNumberOfUsers(userG);
  bool done = false;

  assert(maxObjects <= 1); // This code is for one human at a time only
                           // right now.

  if(nUsers > 0 && nUsers<= maxObjects && numRecorded < framesPerRecording){
    if(numBurnt > burnInFrames){
      xn::SceneMetaData smd;
      userG.GetUserPixels(0,smd);
      const XnLabel* userLabels = smd.Data(); // uint16
      const XnDepthPixel* ptrDepth = dmd.Data();
      assert(smd.XRes()==labelsW && smd.YRes()==labelsH);
      assert(dmd.XRes()==labelsW && dmd.YRes()==labelsH);
      assert(tmpLabels->isContinuous());
      unsigned char *ptrLabels = tmpLabels->ptr();
      bool atleastOnePerson = true;
      for(int i=0;i<labelsW * labelsH;i++){
        //*ptrLabels=(*ptrDepth==0)?255:(unsigned char)*userLabels;
        unsigned char curLabel = (unsigned char)*userLabels;
        if(curLabel > 0){
          //atleastOnePerson = true;
          curLabel = 1;}
        *ptrLabels=(*ptrDepth==0)?255:curLabel;
        ptrLabels++;userLabels++;
        ptrDepth++;
      }

      assert(imageH == debayeredImg.rows && imageW == debayeredImg.cols);
      /*
         const XnRGB24Pixel *ptrImd = imd.RGB24Data();
         unsigned char *ptrImage = tmpImg.ptr();
         assert(tmpImg.isContinuous());
         for(int i=0;i<imageH * imageW;i++){
       *ptrImage=ptrImd->nBlue;ptrImage++;
       *ptrImage=ptrImd->nGreen;ptrImage++;
       *ptrImage=ptrImd->nRed;ptrImage++;
       ptrImd++;
       }
       */
      //bayerToRGB(imd,tmpImg);
      //cv::cvtColor(tmpImg,tmpImg,CV_RGB2BGR);
      if(atleastOnePerson){
        if(drawTimeStamp){
          int textHeight = 20;
          int textOrigin = 20;
          int maxChars = 100;
          char tmpStr[maxChars];
          snprintf(tmpStr,maxChars,"Time(depth img)=%lld micro sec",depthTimeStamp);
          cv::putText(debayeredImg,tmpStr,cv::Point(10,textOrigin),cv::FONT_HERSHEY_PLAIN,
              1.2,CV_RGB(255,0,0),2);
          snprintf(tmpStr,maxChars,"Time(rgb   img)=%lld micro sec",imgTimeStamp);
          cv::putText(debayeredImg,tmpStr,cv::Point(10,textOrigin+textHeight),
              cv::FONT_HERSHEY_PLAIN,
              1.2,CV_RGB(255,0,0),2);
          double diffTime = ((double)imgTimeStamp-(double)depthTimeStamp)/1000;
          snprintf(tmpStr,maxChars,"Time(rgb-depth)=%.1f milli sec",diffTime);
          cv::putText(debayeredImg,tmpStr,cv::Point(10,textOrigin+2*textHeight),
              cv::FONT_HERSHEY_PLAIN,
              1.2,CV_RGB(255,0,0),2);
        }
        char suffix[50];
        assert(numRecorded<1e5);
        snprintf(suffix,50,"%05d.png",numRecorded);
        bool written;
        written = cv::imwrite(imgName_prefix+suffix,debayeredImg);
        assert(written);
        written = cv::imwrite(labelName_prefix+suffix,*tmpLabels);
        assert(written);

        numRecorded++; 
        if(numRecorded%NUM_RECORDED_TICK==0){
          printf("Recorded: %d/%d, Time taken = %d seconds\n",
              numRecorded,framesPerRecording,(int)difftime(time(NULL),startTime));
        }
      }
    }
    else{
      numBurnt++;
      if(numBurnt%NUM_BURNT_TICK==0){
        printf("Burnt: %d/%d frames\n",numBurnt,burnInFrames);
      }
    }
  }
  else if(nUsers > maxObjects){
    printf("Not recording, as there are %d people being tracked (max limit = %d)\n",
            nUsers,maxObjects);         
  }

  if(numRecorded >= framesPerRecording){
    done = true;
  }
  return done;
}

void datasetManager::addColormapToLabels(){

  printf("Adding colormap to label images using matlab call\n");
  std::string command = MATLAB_SCRIPT+outputDir;
  system(command.c_str());

}

datasetManager::~datasetManager(){
  addColormapToLabels();
  if(numRecorded == 0){
    printf("Nothing recorded in %s, removing dir\n",outputDir.c_str());
    rmdir(outputDir.c_str());
    return;
  }

  printf("Saving configuration to: %s\n",outputDir.c_str());

  YAML::Emitter out;
  char framesRecorded[10];
  snprintf(framesRecorded,10,"%d",numRecorded);
  
  out << YAML::BeginMap;
  out << YAML::Key << "name";
  out << YAML::Value << name;
  out << YAML::Key << "location";
  out << YAML::Value << location;
  out << YAML::Key << "numPeople";
  out << YAML::Value << numPeople;
  out << YAML::Key << "recordedFrames";
  out << YAML::Value << framesRecorded;
  out << YAML::EndMap;

  std::ofstream fout((outputDir+"config.yaml").c_str());
  fout<< out.c_str();
  fout.close();
  tmpLabels->release();
  delete(tmpLabels);
}
