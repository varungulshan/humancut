#include <XnOpenNI.h>
#include <XnCppWrapper.h>
#include <GL/glut.h>
#include <config.h>
#include <iostream>
#include <assert.h>
#include <cv.h>
#include <segProcessor.h>
#include <highgui.h>
#include <datasetManager.h>
#include <cstring>

#ifndef NDEBUG
  #define printDebug(...) printf(__VA_ARGS__)
#else
  #define printDebug(...)
#endif

#define CHECK_RC(nRetVal, what)                    \
  if (nRetVal != XN_STATUS_OK)                  \
  {                                \
    printf("%s failed: %s\n", what, xnGetStatusString(nRetVal));\
    return nRetVal;                        \
  }

#define CONFIG_XML_PATH DEMO_CONFIG_FILE

#if (XN_PLATFORM == XN_PLATFORM_MACOSX)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif


//---------------------------------------------------------------------------
// Globals
//---------------------------------------------------------------------------
#define RECORDER_DESTINATION "../../recordings/latestRecording.oni"
#define OUTPUT_VIDEO_PATH "../../recordings/latestVideo.avi"

xn::Context g_Context;
xn::DepthGenerator g_DepthGenerator;
xn::ImageGenerator g_ImageGenerator;
xn::UserGenerator g_UserGenerator;
segProcessor *segP;
datasetManager *datasetM;

bool isRunningRecording = false;
XnBool g_bPause = false;
bool addFrame; // to indicate current frames are synced
bool enableRecording = true;
xn::Recorder recorder;
bool saveVideo = false;
cv::VideoWriter *vw=NULL;
bool softwareSyncEnable = false; // Does not use WaitAndUpdateAll, enabling this
// makes the user generator flicker
bool softwareSyncEnable2 = true; // Uses WaitAndUpdateAll
double syncTolerance = 11; // In milli seconds
// Good settings till now are:
// softwareSyncEnable = false
// softwareSyncEnable2 = true
// syncTolerance = 11

XnBool g_bQuit = false;
int winDepthMap,winImg;

void DrawDepthMap();
void DrawImgMap();
int setupDevices();

//---------------------------------------------------------------------------
// Code
//---------------------------------------------------------------------------

void CleanupExit()
{
  g_Context.Shutdown();
  delete(segP);
  delete(datasetM);
  if(vw!=NULL){delete(vw);}
  exit (1);
}

// Callback: New user was detected
void XN_CALLBACK_TYPE User_NewUser(xn::UserGenerator& generator, XnUserID nId, void* pCookie)
{
  printf("New User %d\n", nId);
}

// Callback: An existing user was lost
void XN_CALLBACK_TYPE User_LostUser(xn::UserGenerator& generator, XnUserID nId, void* pCookie)
{
  printf("Lost user %d\n", nId);
}

// this function is called each frame
void glutDisplay_depth(void)
{
  glutSetWindow(winDepthMap);
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Setup the OpenGL viewpoint
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();

  xn::DepthMetaData depthMD;
  g_DepthGenerator.GetMetaData(depthMD);

  glOrtho(0, depthMD.XRes(), depthMD.YRes(), 0, -1.0, 1.0);
  glDisable(GL_TEXTURE_2D);

  // Process the data
  DrawDepthMap();
  glutSwapBuffers();
}

void glutDisplay_img (void)
{
  glutSetWindow(winImg);
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Setup the OpenGL viewpoint
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();

  xn::ImageMetaData imageMD;
  g_ImageGenerator.GetMetaData(imageMD);

  glOrtho(0, imageMD.XRes(), imageMD.YRes(), 0, -1.0, 1.0);
  glDisable(GL_TEXTURE_2D);

  DrawImgMap();
  if(saveVideo){
    (*vw)<<segP->getImageOpenCV();
  }

  glutSwapBuffers();
}

void glutIdle (void)
{
  if (g_bQuit) {
    CleanupExit();
  }

  /*
  printDebug("Frame id depth frame = %d, time stamp = %lld\n",
      g_DepthGenerator.GetFrameID(),g_DepthGenerator.GetTimestamp());
  printDebug("Frame id image frame = %d, time stamp = %lld\n",
      g_ImageGenerator.GetFrameID(),g_ImageGenerator.GetTimestamp());
  */

  addFrame = false;
  if(softwareSyncEnable){
    bool newData = false;
    if(!g_bPause){
      if(g_UserGenerator.IsNewDataAvailable()){
        g_UserGenerator.WaitAndUpdateData();
        newData = true;
        assert(g_UserGenerator.GetTimestamp()==g_DepthGenerator.GetTimestamp());
        // assumes the depth node is updated also
      }
      if(g_ImageGenerator.IsNewDataAvailable()){
        g_ImageGenerator.WaitAndUpdateData();
        newData = true;
      }
    }
    if(newData){
      if(enableRecording){
        recorder.Record(); 
      }
      double timeDiff = (double)g_UserGenerator.GetTimestamp() - 
                        (double)g_ImageGenerator.GetTimestamp();
      timeDiff/=1000;
      if(abs(timeDiff) > syncTolerance) addFrame = false;
      else addFrame = true;
    }
  }
  else{
    if (!g_bPause)
    {
      // Read next available data
      g_Context.WaitAndUpdateAll();
    }
    if(enableRecording){
      recorder.Record(); 
    }
    if(softwareSyncEnable2){
      double timeDiff = (double)g_UserGenerator.GetTimestamp() - 
                        (double)g_ImageGenerator.GetTimestamp();
      timeDiff/=1000;
      if(abs(timeDiff) > syncTolerance) addFrame = false;
      else addFrame = true;
    }
    else{
      addFrame = true;
    }
  }

  segP->newFrameArrived();
  if(addFrame){
    assert(g_DepthGenerator.GetTimestamp()==g_UserGenerator.GetTimestamp());
    bool done = datasetM->newFrameArrived(segP->dmd,*(segP->debayeredImg),
        g_UserGenerator,
        g_DepthGenerator.GetTimestamp(),g_ImageGenerator.GetTimestamp());
    if(done){
      CleanupExit();
    }
 }
 // Display the frame
 glutSetWindow(winImg);
 glutPostRedisplay();
 glutSetWindow(winDepthMap);
 glutPostRedisplay();
 
}

void glutKeyboard (unsigned char key, int x, int y)
{
  switch (key)
  {
  case 27:
    CleanupExit();
  case'p':
    g_bPause = !g_bPause;
    break;
  }
}


void glInit (int * pargc, char ** argv)
{
  glutInit(pargc, argv);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);

  // Create depth map window
  xn::DepthMetaData dmd;
  g_DepthGenerator.GetMetaData(dmd);
  glutInitWindowSize(dmd.XRes(), dmd.YRes());
  winDepthMap = glutCreateWindow ("Depth map");
  glutSetWindow(winDepthMap);
  //glutSetCursor(GLUT_CURSOR_NONE);

  glutKeyboardFunc(glutKeyboard);
  glutDisplayFunc(glutDisplay_depth);
  glutIdleFunc(glutIdle);

  glDisable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);
  glEnableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);

  // Create image window
  xn::ImageMetaData imd;
  g_ImageGenerator.GetMetaData(imd);
  glutInitWindowSize(imd.XRes(), imd.YRes());
  winImg = glutCreateWindow("RBG image");
  glutSetWindow(winImg);

  glutKeyboardFunc(glutKeyboard);
  glutDisplayFunc(glutDisplay_img);
  glDisable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);
  glEnableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);
}

int main(int argc, char **argv)
{
  datasetM = new datasetManager();
  softwareSyncEnable2 = datasetM->syncEnable;
  syncTolerance = datasetM->syncTolerance;
  XnStatus nRetVal = XN_STATUS_OK;

  xn::EnumerationErrors errors;

  if(argc>1){
    nRetVal = g_Context.Init();
    CHECK_RC(nRetVal, "Init");
    const char *fileName = argv[1];
    nRetVal = g_Context.OpenFileRecording(fileName);
    if (nRetVal != XN_STATUS_OK)
    {
      printf("Can't open recording %s: %s\n", argv[1], xnGetStatusString(nRetVal));
      return 1;
    }
    isRunningRecording = true;
  }else{

    nRetVal = g_Context.InitFromXmlFile(CONFIG_XML_PATH, &errors);
    if (nRetVal == XN_STATUS_NO_NODE_PRESENT)
    {
      XnChar strError[1024];
      errors.ToString(strError, 1024);
      printf("%s\n", strError);
      return (nRetVal);
    }
    else if (nRetVal != XN_STATUS_OK)
    {
      printf("Open failed: %s\n", xnGetStatusString(nRetVal));
      return (nRetVal);
    }
    isRunningRecording = false;
  }

  nRetVal = g_Context.FindExistingNode(XN_NODE_TYPE_DEPTH, g_DepthGenerator);
  //nRetVal = g_DepthGenerator.Create(g_Context);
  CHECK_RC(nRetVal, "Find depth generator");
  nRetVal = g_Context.FindExistingNode(XN_NODE_TYPE_IMAGE, g_ImageGenerator);
  CHECK_RC(nRetVal, "Find image generator");
  nRetVal = g_Context.FindExistingNode(XN_NODE_TYPE_USER, g_UserGenerator);
  if (nRetVal != XN_STATUS_OK)
  {
    nRetVal = g_UserGenerator.Create(g_Context);
    CHECK_RC(nRetVal, "Find user generator");
  }

  XnCallbackHandle hUserCallbacks;
  g_UserGenerator.RegisterUserCallbacks(User_NewUser, User_LostUser, NULL, hUserCallbacks);

  if(enableRecording){
    recorder.Create(g_Context);
    recorder.SetDestination(XN_RECORD_MEDIUM_FILE,RECORDER_DESTINATION);
    recorder.AddNodeToRecording(g_DepthGenerator);
    recorder.AddNodeToRecording(g_ImageGenerator);
    recorder.AddNodeToRecording(g_UserGenerator);
    printf("Saving recording to %s\n",RECORDER_DESTINATION);
  }

  setupDevices();

  nRetVal = g_Context.StartGeneratingAll();
  CHECK_RC(nRetVal, "StartGenerating");

  segP = new segProcessor(g_DepthGenerator,g_ImageGenerator,g_UserGenerator);
  if(saveVideo){
    vw = new cv::VideoWriter();
    vw->open(OUTPUT_VIDEO_PATH,CV_FOURCC('P','I','M','1'),30,
        cv::Size(segP->imgW,segP->imgH));
    assert(vw->isOpened());
  }
  datasetM->startManager(segP->imd,segP->smd); 
  glInit(&argc, argv);
  glutMainLoop();
}

int setupDevices(){
  if(isRunningRecording) return 0;
  // ---- Begin copy from ROS code -----------------
  XnStatus retVal;
  // InputFormat should be 6 = uncompressed Bayer for Kinect
  retVal = g_ImageGenerator.SetIntProperty ("InputFormat", 6);
  CHECK_RC(retVal,"Error setting the image input format to Uncompressed 8-bit BAYER!");

  // RegistrationType should be 2 (software) for Kinect, 1 (hardware) for PS
  retVal = g_DepthGenerator.SetIntProperty ("RegistrationType", 2);
  CHECK_RC(retVal,"[OpenNIDriver] Error enabling registration!");

  // Grayscale: bypass debayering -> gives us bayer pattern!
  retVal = g_ImageGenerator.SetPixelFormat(XN_PIXEL_FORMAT_GRAYSCALE_8_BIT );
  CHECK_RC(retVal,"[OpenNIDriver] Failed to set image pixel format to 8bit-grayscale");
  // ----- End copy from ROS code ------------------

  assert(g_DepthGenerator.IsCapabilitySupported(XN_CAPABILITY_ALTERNATIVE_VIEW_POINT));
  assert(g_DepthGenerator.IsCapabilitySupported(XN_CAPABILITY_FRAME_SYNC));
  assert(g_DepthGenerator.GetFrameSyncCap().CanFrameSyncWith(g_ImageGenerator));
  //retVal = g_DepthGenerator.GetFrameSyncCap().FrameSyncWith(g_ImageGenerator);
  //CHECK_RC(nRetVal, "Frame sync problem");

  retVal = g_DepthGenerator.GetAlternativeViewPointCap().SetViewPoint(g_ImageGenerator);
  CHECK_RC(retVal, "Alternative viewpoint problem");
}

GLuint initTexture()
{
	GLuint texID = 0;
	glGenTextures(1,&texID);

	glBindTexture(GL_TEXTURE_2D,texID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	return texID;
}

void DrawRectangle(float topLeftX, float topLeftY, float bottomRightX, float bottomRightY)
{
	GLfloat verts[8] = {	topLeftX, topLeftY,
		topLeftX, bottomRightY,
		bottomRightX, bottomRightY,
		bottomRightX, topLeftY
	};
	glVertexPointer(2, GL_FLOAT, 0, verts);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	//TODO: Maybe glFinish needed here instead - if there's some bad graphics crap
	glFlush();
}

void DrawTexture(float topLeftX, float topLeftY, float bottomRightX, float bottomRightY,
                 GLfloat* texcoordsLocal)
{
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2, GL_FLOAT, 0, texcoordsLocal);

	DrawRectangle(topLeftX, topLeftY, bottomRightX, bottomRightY);

	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

void glPrintString(void *font, char *str)
{
	int i,l = strlen(str);

	for(i=0; i<l; i++)
	{
		glutBitmapCharacter(font,*str++);
	}
}

void DrawDepthMap()
{
  static GLuint depthTexID;
  static bool isInitialized = false;

  if(!isInitialized){
    depthTexID = initTexture();
    isInitialized = true;
  }
  unsigned char* pDepthTexBuf = segP->getDepthTexture();
  glBindTexture(GL_TEXTURE_2D, depthTexID);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, segP->depthTexW, segP->depthTexH,
               0, GL_RGB, GL_UNSIGNED_BYTE, pDepthTexBuf);

  // Display the OpenGL texture map
  glColor4f(0.75,0.75,0.75,1);

  glEnable(GL_TEXTURE_2D);
  DrawTexture(segP->depthW,segP->depthH,0,0,segP->texcoordsDepth);  
  glDisable(GL_TEXTURE_2D);

  char strLabel[50] = "";
  XnUserID aUsers[15];
  XnUInt16 nUsers = 15;
  g_UserGenerator.GetUsers(aUsers, nUsers);
  for (int i = 0; i < nUsers; ++i)
  {
    if (true)
    {
      XnPoint3D com;
      g_UserGenerator.GetCoM(aUsers[i], com);
      g_DepthGenerator.ConvertRealWorldToProjective(1, &com, &com);

      xnOSMemSet(strLabel, 0, sizeof(strLabel));
      sprintf(strLabel, "%d", aUsers[i]);

      glColor4f(1-Colors[i%nColors][0], 1-Colors[i%nColors][1], 1-Colors[i%nColors][2], 1);
      glRasterPos2i(com.X, com.Y);
      glPrintString(GLUT_BITMAP_HELVETICA_18, strLabel);
    }
  }
}

void DrawImgMap()
{
  static bool isInitialized = false;  
  static GLuint imgTexID;

  if(!isInitialized){
    imgTexID = initTexture();
    isInitialized = true;
  }

  unsigned char* pImgTexBuf = segP->getImageTexture();
  glBindTexture(GL_TEXTURE_2D, imgTexID);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, segP->imgTexWidth, segP->imgTexHeight,
               0, GL_RGB, GL_UNSIGNED_BYTE, pImgTexBuf);

  // Display the OpenGL texture map
  glColor4f(0.75,0.75,0.75,1);

  glEnable(GL_TEXTURE_2D);
  DrawTexture(segP->imgW,segP->imgH,0,0,segP->texcoordsImage);  
  glDisable(GL_TEXTURE_2D);

  if(addFrame == false){
    std::string tmpStr = "Out of sync";
    glColor4f(1, 0, 0, 1);
    glRasterPos2i(0, 20);
    glPrintString(GLUT_BITMAP_HELVETICA_18, (char*)tmpStr.c_str());
  }
}
