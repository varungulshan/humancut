#include <XnOpenNI.h>
#include <XnCppWrapper.h>
#include <assert.h>
#include <config.h>

#ifndef NDEBUG
  #define printDebug(...) printf(__VA_ARGS__)
#else
  #define printDebug(...)
#endif

#define CHECK_RC(nRetVal, what)                    \
  if (nRetVal != XN_STATUS_OK)                  \
  {                                \
    printf("%s failed: %s\n", what, xnGetStatusString(nRetVal));\
    return nRetVal;                        \
  }

#define CONFIG_XML_PATH DEMO_CONFIG_FILE

//---------------------------------------------------------------------------
// Globals
//---------------------------------------------------------------------------

xn::Context g_Context;
xn::ImageGenerator g_ImageGenerator;
void CleanupExit()
{
  g_Context.Shutdown();
  exit (1);
}

int main(int argc, char **argv)
{
  XnStatus nRetVal = XN_STATUS_OK;

  xn::EnumerationErrors errors;

  g_Context.InitFromXmlFile(CONFIG_XML_PATH);
  nRetVal = g_Context.FindExistingNode(XN_NODE_TYPE_IMAGE, g_ImageGenerator);
  CHECK_RC(nRetVal, "Find image generator");
 
  g_ImageGenerator.SetIntProperty("InputFormat",6);
  nRetVal = g_Context.StartGeneratingAll();
  CHECK_RC(nRetVal, "StartGenerating");
  printf("RGB Camera input mode reset\n");
  CleanupExit();
}
