#include <XnOpenNI.h>
#include <XnCppWrapper.h>
#include <GL/glut.h>
#include <config.h>
#include <iostream>
#include <assert.h>
#include <cv.h>
#include <segProcessor.h>

#ifndef NDEBUG
  #define printDebug(...) printf(__VA_ARGS__)
#else
  #define printDebug(...)
#endif

#define CHECK_RC(nRetVal, what)                    \
  if (nRetVal != XN_STATUS_OK)                  \
  {                                \
    printf("%s failed: %s\n", what, xnGetStatusString(nRetVal));\
    return nRetVal;                        \
  }

#define CONFIG_XML_PATH DEMO_CONFIG_FILE

#if (XN_PLATFORM == XN_PLATFORM_MACOSX)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif


//---------------------------------------------------------------------------
// Globals
//---------------------------------------------------------------------------

#define DEFAULT_RECORDED_FILE "../../recordings/sampleRecord_highRes3.oni"
#define RECORDING_TMP_FILE "../../recordings/tmp.oni"

xn::Context g_Context;
xn::DepthGenerator g_DepthGenerator;
xn::ImageGenerator g_ImageGenerator;
xn::UserGenerator g_UserGenerator;
segProcessor *segP;
bool recordingHack = true;
xn::Recorder recorder;

XnBool g_bPause = false;

XnBool g_bQuit = false;
int winDepthMap,winImg;

void DrawDepthMap();
void DrawImgMap();

//---------------------------------------------------------------------------
// Code
//---------------------------------------------------------------------------

void CleanupExit()
{
  g_Context.Shutdown();
  delete(segP);
  exit (1);
}

// Callback: New user was detected
void XN_CALLBACK_TYPE User_NewUser(xn::UserGenerator& generator, XnUserID nId, void* pCookie)
{
  printf("New User %d\n", nId);
}

// Callback: An existing user was lost
void XN_CALLBACK_TYPE User_LostUser(xn::UserGenerator& generator, XnUserID nId, void* pCookie)
{
  printf("Lost user %d\n", nId);
}

// this function is called each frame
void glutDisplay_depth(void)
{
  glutSetWindow(winDepthMap);
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Setup the OpenGL viewpoint
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();

  xn::DepthMetaData depthMD;
  g_DepthGenerator.GetMetaData(depthMD);

  glOrtho(0, depthMD.XRes(), depthMD.YRes(), 0, -1.0, 1.0);
  glDisable(GL_TEXTURE_2D);

  // Process the data
  DrawDepthMap();
  glutSwapBuffers();
}

void glutDisplay_img (void)
{
  glutSetWindow(winImg);
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Setup the OpenGL viewpoint
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();

  xn::ImageMetaData imageMD;
  g_ImageGenerator.GetMetaData(imageMD);

  glOrtho(0, imageMD.XRes(), imageMD.YRes(), 0, -1.0, 1.0);
  glDisable(GL_TEXTURE_2D);

  DrawImgMap();

  glutSwapBuffers();
}

void glutIdle (void)
{
  if (g_bQuit) {
    CleanupExit();
  }

  if (!g_bPause)
  {
    // Read next available data
    g_Context.WaitAndUpdateAll();
  }
  segP->newFrameArrived();
  // Display the frame
  glutSetWindow(winImg);
  glutPostRedisplay();
  glutSetWindow(winDepthMap);
  glutPostRedisplay();
  //if(recordingHack){
    //recorder.Record();
  //}
}

void glutKeyboard (unsigned char key, int x, int y)
{
  switch (key)
  {
  case 27:
    CleanupExit();
  case'p':
    g_bPause = !g_bPause;
    break;
  }
}


void glInit (int * pargc, char ** argv)
{
  glutInit(pargc, argv);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);

  // Create image window
  xn::ImageMetaData imd;
  g_ImageGenerator.GetMetaData(imd);
  glutInitWindowSize(imd.XRes(), imd.YRes());
  winImg = glutCreateWindow("RBG image");
  glutSetWindow(winImg);

  glutKeyboardFunc(glutKeyboard);
  glutDisplayFunc(glutDisplay_img);
  glDisable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);
  glEnableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);

  // Create depth map window
  xn::DepthMetaData dmd;
  g_DepthGenerator.GetMetaData(dmd);
  glutInitWindowSize(dmd.XRes(), dmd.YRes());
  winDepthMap = glutCreateWindow ("Depth map");
  glutSetWindow(winDepthMap);
  //glutSetCursor(GLUT_CURSOR_NONE);

  glutKeyboardFunc(glutKeyboard);
  glutDisplayFunc(glutDisplay_depth);
  glutIdleFunc(glutIdle);

  glDisable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);
  glEnableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);
}

int main(int argc, char **argv)
{
  XnStatus nRetVal = XN_STATUS_OK;

  nRetVal = g_Context.Init();
  CHECK_RC(nRetVal, "Init");
  const char *fileName = (argc > 1)? argv[1]:DEFAULT_RECORDED_FILE;
	nRetVal = g_Context.OpenFileRecording(fileName);
	if (nRetVal != XN_STATUS_OK)
	{
		printf("Can't open recording %s: %s\n", argv[1], xnGetStatusString(nRetVal));
		return 1;
	}
  
  nRetVal = g_Context.FindExistingNode(XN_NODE_TYPE_DEPTH, g_DepthGenerator);
  CHECK_RC(nRetVal, "Find depth generator");
  nRetVal = g_Context.FindExistingNode(XN_NODE_TYPE_IMAGE, g_ImageGenerator);
  CHECK_RC(nRetVal, "Find image generator");
  nRetVal = g_Context.FindExistingNode(XN_NODE_TYPE_USER, g_UserGenerator);
  if (nRetVal != XN_STATUS_OK)
  {
    printf("User generator not found recorded, there will be diff between recorded segmentation and the one computed now!\n");
    nRetVal = g_UserGenerator.Create(g_Context);
    CHECK_RC(nRetVal, "Find user generator");
  }

  XnCallbackHandle hUserCallbacks;
  g_UserGenerator.RegisterUserCallbacks(User_NewUser, User_LostUser, NULL, hUserCallbacks);

  if(recordingHack){
    recorder.Create(g_Context);
    recorder.SetDestination(XN_RECORD_MEDIUM_FILE,RECORDING_TMP_FILE);
    recorder.AddNodeToRecording(g_DepthGenerator);
    recorder.AddNodeToRecording(g_ImageGenerator);
    recorder.AddNodeToRecording(g_UserGenerator);
    printf("Hack: saving recording to %s\n",RECORDING_TMP_FILE);
  }

  nRetVal = g_Context.StartGeneratingAll();
  CHECK_RC(nRetVal, "StartGenerating");

  segP = new segProcessor(g_DepthGenerator,g_ImageGenerator,g_UserGenerator);
  glInit(&argc, argv);
  glutMainLoop();
}

GLuint initTexture()
{
	GLuint texID = 0;
	glGenTextures(1,&texID);

	glBindTexture(GL_TEXTURE_2D,texID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	return texID;
}

void DrawRectangle(float topLeftX, float topLeftY, float bottomRightX, float bottomRightY)
{
	GLfloat verts[8] = {	topLeftX, topLeftY,
		topLeftX, bottomRightY,
		bottomRightX, bottomRightY,
		bottomRightX, topLeftY
	};
	glVertexPointer(2, GL_FLOAT, 0, verts);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	//TODO: Maybe glFinish needed here instead - if there's some bad graphics crap
	glFlush();
}

void DrawTexture(float topLeftX, float topLeftY, float bottomRightX, float bottomRightY,
                 GLfloat* texcoordsLocal)
{
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2, GL_FLOAT, 0, texcoordsLocal);

	DrawRectangle(topLeftX, topLeftY, bottomRightX, bottomRightY);

	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

void glPrintString(void *font, char *str)
{
	int i,l = strlen(str);

	for(i=0; i<l; i++)
	{
		glutBitmapCharacter(font,*str++);
	}
}

void DrawDepthMap()
{
  static GLuint depthTexID;
  static bool isInitialized = false;

  if(!isInitialized){
    depthTexID = initTexture();
    isInitialized = true;
  }
  unsigned char* pDepthTexBuf = segP->getDepthTexture();
  glBindTexture(GL_TEXTURE_2D, depthTexID);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, segP->depthTexW, segP->depthTexH,
               0, GL_RGB, GL_UNSIGNED_BYTE, pDepthTexBuf);

  // Display the OpenGL texture map
  glColor4f(0.75,0.75,0.75,1);

  glEnable(GL_TEXTURE_2D);
  DrawTexture(segP->depthW,segP->depthH,0,0,segP->texcoordsDepth);  
  glDisable(GL_TEXTURE_2D);

  char strLabel[50] = "";
  XnUserID aUsers[15];
  XnUInt16 nUsers = 15;
  g_UserGenerator.GetUsers(aUsers, nUsers);
  for (int i = 0; i < nUsers; ++i)
  {
    if (true)
    {
      XnPoint3D com;
      g_UserGenerator.GetCoM(aUsers[i], com);
      g_DepthGenerator.ConvertRealWorldToProjective(1, &com, &com);

      xnOSMemSet(strLabel, 0, sizeof(strLabel));
      sprintf(strLabel, "%d", aUsers[i]);

      glColor4f(1-Colors[i%nColors][0], 1-Colors[i%nColors][1], 1-Colors[i%nColors][2], 1);
      glRasterPos2i(com.X, com.Y);
      glPrintString(GLUT_BITMAP_HELVETICA_18, strLabel);
    }
  }
}

void DrawImgMap()
{
  static bool isInitialized = false;  
  static GLuint imgTexID;

  if(!isInitialized){
    imgTexID = initTexture();
    isInitialized = true;
  }

  unsigned char* pImgTexBuf = segP->getImageTexture();
  glBindTexture(GL_TEXTURE_2D, imgTexID);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, segP->imgTexWidth, segP->imgTexHeight,
               0, GL_RGB, GL_UNSIGNED_BYTE, pImgTexBuf);

  // Display the OpenGL texture map
  glColor4f(0.75,0.75,0.75,1);

  glEnable(GL_TEXTURE_2D);
  DrawTexture(segP->imgW,segP->imgH,0,0,segP->texcoordsImage);  
  glDisable(GL_TEXTURE_2D);

}
