#ifndef DATASET_MANAGER_H
#define DATASET_MANAGER_H

#include <string>
#include <XnOpenNI.h>
#include <XnCppWrapper.h>
#include <cv.h>
#include <highgui.h>
#include <time.h>

class datasetManager{

  private:
    void initConfigFromYaml();
    void initVideoStreams();
    // dataset settings
    std::string name,location,numPeople,datasetDir;
    std::string outputDir,imgName_prefix,labelName_prefix;
    int maxObjects;
    int framesPerRecording;
    void confirmConfig();
    int imageW,imageH,labelsW,labelsH;
    void setupRecording();
    int numRecorded;
    cv::Mat *tmpLabels;
    bool drawTimeStamp;
    int getNumberOfUsers(xn::UserGenerator &userG);
    time_t startTime;
    void addColormapToLabels();
    int burnInFrames;
    int numBurnt;
  public:
    int syncTolerance;
    bool syncEnable;
    datasetManager();
    void startManager(const xn::ImageMetaData &imd,const xn::SceneMetaData &smd);
    bool newFrameArrived(const xn::DepthMetaData &dmd,
                         cv::Mat &debayeredImg,
                         xn::UserGenerator &userG,
                         XnUInt64 depthTimeStamp = 0,
                         XnUInt64 imgTimeStamp = 0);
    ~datasetManager();

};

#endif
