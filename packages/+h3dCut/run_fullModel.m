function run_fullModel(params)
% Runs tests on a dataset , 
% All three parameters (color model,segmentation and puppet) are optimized
% in an alternating fashion

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('params','var'))
  params.rootOut_dir=[cwd '../../results/h3dCut/grabCut/'];

  params.opts.method='simpleJointModel_parts';
  %params.opts.method='simpleJointModel';

  params.opts.humanModel_type='jointsModel_partColors';
  %params.opts.humanModel_type='jointsModel';
  params.opts.humanModel_optsString='try1';
  params.opts.segMethod='grabCut_humanModelParts';
  %params.opts.segMethod='grabCut_humanModel';
  params.opts.segMethod_optsString='iter5_2';

  params.opts.visualize=true;
  params.opts.visOpts.debugVisualize=true;
  params.opts.debugLevel=10;
  params.opts.visOpts.imgExt='jpg';
  params.opts.visOpts.segBdryWidth=2;
  params.overWrite=true;

  params.evalOpts_string='try2';
  params.testBench_cmd='testBench.getTests_h3d_tmp()';
  %params.gtDir=[cwd '../../data/h3d_gtSeg/'];
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',params.rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
end

segOutDir=[params.rootOut_dir 'segs/'];
if(~exist(segOutDir,'dir')), mkdir(segOutDir); end;

timingFile=[params.rootOut_dir 'timing.txt'];
fH_time=fopen(timingFile,'w');

testOpts=eval(params.testBench_cmd);
save([params.rootOut_dir 'testOpts.mat'],'testOpts');
[annots,K]=h3dAnno.loadAnnotations_full(testOpts.annoDir,testOpts.imgDir,testOpts.imgList,...
                                    false);
stTime_global=clock;

parfor i=1:length(annots)
%for i=1:length(annots)  
  fprintf('\nNow running on %s\n',annots(i).fullImgName);
  runOnImg(annots(i),K,testOpts,params.opts,segOutDir);
end

fprintf(fH_time,'\n\nTotal time to run on all videos = %.2f hours\n',...
        etime(clock,stTime_global)/3600);
fclose(fH_time);

testBench.computeStats_h3dCut(params.rootOut_dir,params.evalOpts_string);

function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);

opts=params.opts;
if(opts.visualize & opts.visOpts.debugVisualize)
  debugOpts.debugLevel=opts.debugLevel;
else
  debugOpts.debugLevel=0;
end
debugOpts.debugDir='';
debugOpts.debugPrefix='';

fH=fopen([params.rootOut_dir 'opts_humanModel.txt'],'w');
[hModel,hModel_opts]=h3dCut.createHumanModel(opts,debugOpts);
miscFns.printClass(hModel_opts,fH);
delete(hModel);
fclose(fH);

fH=fopen([params.rootOut_dir 'opts_segModel.txt'],'w');
[segH,segH_opts]=h3dCut.createSegObject(opts,debugOpts);
miscFns.printClass(segH_opts,fH);
delete(segH);
fclose(fH);

function runOnImg(anno,K,testOpts,opts,outDir)

switch(opts.method)
  case 'simpleJointModel'
    h3dCut.runSimpleJointModel(anno,K,outDir,opts,testOpts);
  case 'simpleJointModel_parts'
    h3dCut.runSimpleJointModel_parts(anno,K,outDir,opts,testOpts);
  otherwise
    error('Invalid full model method %s\n',opts.method);
end

