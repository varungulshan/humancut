function trainHeadModel(params)
% Learns a model for the head given training data

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('params','var'))
  params.rootOut_dir=[cwd '../../results/h3dCut/trainedHeadModel/kmeans/'];
  params.opts.gtDir=[cwd '../../data/h3d_gtSeg2/'];
  
  %params.opts.trainMethod='linearRegression';
  %params.opts.trainMethod_optsString='try1';
  params.opts.trainMethod='kmeans';
  params.opts.trainMethod_optsString='try1';
  %params.opts.method='simpleJointModel';

  params.opts.humanModel_type='jointsModel_partColors_trainHead';
  %params.opts.humanModel_type='jointsModel';
  params.opts.humanModel_optsString='try1';

  params.opts.visualize=true;
  params.opts.visOpts.debugVisualize=true;
  params.opts.debugLevel=10;
  params.opts.visOpts.imgExt='jpg';
  params.opts.visOpts.segBdryWidth=2;
  params.overWrite=true;

  params.testBench_cmd='testBench.getTests_h3d_headTrain()';
  %params.testBench_cmd='testBench.getTests_h3d_tmp()';
  
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',params.rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
end

testBenchOpts=eval(params.testBench_cmd);
save([params.rootOut_dir 'testBenchOpts.mat'],'testBenchOpts');
[annots,K]=h3dAnno.loadAnnotations_full(testBenchOpts.annoDir,testBenchOpts.imgDir,...
    testBenchOpts.imgList,false);
 
trainModel(annots,K,testBenchOpts,params.opts,params.rootOut_dir);

function trainModel(annots,K,testBenchOpts,opts,trainOutDir)
switch(opts.trainMethod)
  case 'linearRegression'
    h3dCut.learning.linearReg.trainLinearRegression(annots,K,opts,trainOutDir);
  case 'kmeans'
    h3dCut.learning.kmeans.trainKmeans(annots,K,opts,trainOutDir);
end

function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);

opts=params.opts;
if(opts.visualize & opts.visOpts.debugVisualize)
  debugOpts.debugLevel=opts.debugLevel;
else
  debugOpts.debugLevel=0;
end
debugOpts.debugDir='';
debugOpts.debugPrefix='';

fH=fopen([params.rootOut_dir 'opts_humanModel.txt'],'w');
[hModel,hModel_opts]=h3dCut.createHumanModel(opts,debugOpts);
miscFns.printClass(hModel_opts,fH);
delete(hModel);
fclose(fH);

