function createUseCase_lists()
% Function to display annotations from the H3D datset, and use
% the keyboard to classify them into different use cases

cwd=miscFns.extractDirPath(mfilename('fullpath'));

listDir=[cwd '../../data/h3d_useCaseLists/'];
labelDir=[cwd '../../data/h3d_gtLabels/'];
%imgList=[cwd '../../../../software/h3d_version1.0/data/people/image_list_original.txt'];
%imgList=[cwd '../+testBench/h3dList_small.txt'];
imgList=[cwd '../../data/h3d_useCaseLists/type1And2.txt'];
imgDir=[cwd '../../../../software/h3d_version1.0/data/people/images/'];
annoDir=[cwd '../../../../software/h3d_version1.0/data/people/info/'];
startIdx=1;

typeListNames={};
typeListNames{end+1}='type1And2_type1.txt';
typeListNames{end+1}='type1And2_type3.txt';
typeListNames{end+1}='type1And2_type4.txt';
typeListNames{end+1}='type1And2_undecided.txt';
%typeListNames{end+1}='typeUndecided.txt';

typeList_keys={};
typeList_keys{end+1}='1';
%typeList_keys{end+1}='2';
typeList_keys{end+1}='3';
typeList_keys{end+1}='4';
typeList_keys{end+1}='5';

[annots,K]=h3dAnno.loadAnnotations_full(annoDir,imgDir,imgList,false);

assert(numel(typeListNames)==numel(typeList_keys));
numTypes=length(typeListNames);

showLabelLegend(K,annots,labelDir);

if(~exist(listDir,'dir'))
  mkdir(listDir);
end

fH=[];
for i=1:numTypes
  fH(i)=fopen([listDir typeListNames{i}],'a');
end

gData=struct();
gData.figImg=figure;
gData.figLabels=figure;
gData.annots=annots;
gData.K=K;
gData.imgDir=imgDir;
gData.labelDir=labelDir;
gData.idx=startIdx;
gData.typeList_keys=typeList_keys;
gData.typeListNames=typeListNames;
gData.fH=fH;
gData.ptH={};

set(gData.figImg,'KeyPressFcn',@OnKeyPress);
gData=loadStuff(gData);
guidata(gData.figImg,gData);
uiwait(gData.figImg);

for i=1:numTypes
  fclose(fH(i));
  imgNames=textread([listDir typeListNames{i}],'%s');
  imgNames=unique(imgNames);
  tmpFh=fopen([listDir typeListNames{i}],'w');
  for j=1:length(imgNames)
    fprintf(tmpFh,'%s\n',imgNames{j});
  end
  fclose(tmpFh);
end

%close(gData.figImg);
close(gData.figLabels);

function showLabelLegend(K,annots,labelDir)
  labelFile=[labelDir annots(1).fullImgName '.png'];
  [labelImg,cmap]=imread(labelFile);
  cmap=uint8(255*cmap);
  legendNames={'A_Occluder', ...
               'A_Face', ...
               'A_Hair', ...
               'A_UpperClothes', ...
               'A_LeftArm', ...
               'A_RightArm', ...
               'A_LowerClothes', ...
               'A_LeftLeg', ...
               'A_RightLeg', ...
               'A_LeftShoe', ...
               'A_RightShoe', ...
               'A_Neck', ...
               'A_Bag', ...
               'A_Hat', ...
               'A_LeftGlove', ...
               'A_RightGlove', ...
               'A_LeftSock', ...
               'A_RightSock', ...
               'A_Sunglasses', ...
               'A_Dress', ...
               'A_BadSegment'...
            };
  legendWidth=150;
  boxHeight=20;
  legendHeight=boxHeight*numel(legendNames);
  
  legendImg=zeros([legendHeight legendWidth 3],'uint8');
  for i=1:numel(legendNames)
    idxPart=eval(['K.' legendNames{i}]);
    partColor=reshape(cmap(idxPart+1,:),[1 1 3]);
    partBox=repmat(partColor,[boxHeight legendWidth 1]);
    legendImg((1+(i-1)*boxHeight):(i*boxHeight),:,:)=partBox;
    legendImg=miscFns.mex_drawText(legendImg,int32(3*legendHeight+(i-1)*boxHeight+15),...
                                   legendNames{i});
  end

  figure;imshow(legendImg);


function OnKeyPress(h,eventData)
  k=lower(eventData.Character);
  gData=guidata(h);

  typeKeys=gData.typeList_keys;

  for i=1:length(typeKeys)
    if(strcmp(typeKeys{i},k))
      gData=saveTypeAndLoadNext(i,gData);
      guidata(h,gData);
      break;
    end
  end


function gData=saveTypeAndLoadNext(typeIdx,gData)
  if(gData.idx>length(gData.annots))
    return;
  end
  idx=gData.idx;
  fH=gData.fH(typeIdx);
  annots=gData.annots;
  fprintf(fH,'%s\n',annots(idx).fullImgName);
  fprintf('Saved human #%04d/%04d in file: %s\n',idx,length(annots),...
          gData.typeListNames{typeIdx});
  gData.idx=idx+1;
  gData=loadStuff(gData);

function gData=loadStuff(gData)
  if(gData.idx>length(gData.annots))
    fprintf('All humans done, nothing left to load!\n');
    return;
  end
  for i=1:length(gData.ptH)
    delete(gData.ptH{i}); % clear existing drawn points
  end
  figImgPosition=get(gData.figImg,'position');
  figLabelsPosition=get(gData.figLabels,'position');
  idx=gData.idx;
  labelDir=gData.labelDir;
  imgDir=gData.imgDir;
  annots=gData.annots;
  labelFile=[labelDir annots(idx).fullImgName '.png'];
  [labelImg,cmap]=imread(labelFile);
  figure(gData.figLabels);
  imshow(labelImg,cmap);
  set(gData.figLabels,'position',figLabelsPosition);
  figure(gData.figImg);
  imgFile=annots(idx).imgPath;
  img=imread(imgFile);
  imshow(img);
  set(gData.figImg,'position',figImgPosition);
  gData.ptH=drawJoints(img,annots(idx),gData.K);
  fprintf('Loaded human #%04d/%04d\n',idx,length(annots));

function ptH=drawJoints(img,anno,K)
  jointIdxs=[K.R_Eye K.L_Eye K.R_Shoulder K.L_Shoulder K.R_Hip K.L_Hip ...
             K.R_Elbow K.L_Elbow K.R_Wrist K.L_Wrist K.R_Knee K.L_Knee ...
             K.R_Ankle K.L_Ankle];
  marked=anno.marked;
  visible=anno.visible;
  coords=anno.coords;
  ptH={};
  for i=jointIdxs
    if(marked(i))
      pt=impoint(gca,coords(i,1:2));
      ptH{end+1}=pt;
      if(visible(i))
        setColor(pt,'b');
      else
        setColor(pt,'g');
      end
    end
  end
