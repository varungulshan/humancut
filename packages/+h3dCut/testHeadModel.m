function testHeadModel(params)
% Runs segmentation, using trained head model

cwd=miscFns.extractDirPath(mfilename('fullpath'));
if(~exist('params','var'))
  params.rootOut_dir=[cwd '../../results/h3dCut/grabCut/'];
  params.trainDir=[cwd '../../results/h3dCut/trainedHeadModel/kmeans/'];
  
  %params.opts.trainMethod='linearRegression';
  %params.opts.trainMethod_optsString='try1';
  params.opts.trainMethod='kmeans';
  params.opts.trainMethod_optsString='try1';
  %params.opts.method='simpleJointModel';

  params.opts.humanModel_type='jointsModel_partColors_trainHead';
  %params.opts.humanModel_type='jointsModel';
  params.opts.humanModel_optsString='try1';

  params.opts.segMethod='grabCut_humanModelParts';
  %params.opts.segMethod='grabCut_humanModel';
  params.opts.segMethod_optsString='iter5_2';

  params.opts.visualize=true;
  params.opts.visOpts.debugVisualize=true;
  params.opts.debugLevel=10;
  params.opts.visOpts.imgExt='jpg';
  params.opts.visOpts.segBdryWidth=2;
  params.overWrite=true;

  params.evalOpts_string='try2';
  params.testBench_cmd='testBench.getTests_h3d3()';
  %params.testBench_cmd='testBench.getTests_h3d_tmp()';
  
end

if(exist(params.rootOut_dir,'dir'))
  if(params.overWrite)
    saveParams_file(params);
  else
    error('Cant overwrite on output dir: %s\n',params.rootOut_dir);
  end
else
  mkdir(params.rootOut_dir);
  saveParams_file(params);
end

testOpts=eval(params.testBench_cmd);
save([params.rootOut_dir 'testOpts.mat'],'testOpts');
[annots,K]=h3dAnno.loadAnnotations_full(testOpts.annoDir,testOpts.imgDir,...
    testOpts.imgList,false);
 
segOutDir=[params.rootOut_dir 'segs/'];
testModel(annots,K,testOpts,params.opts,params.trainDir,segOutDir);

testBench.computeStats_h3dCut(params.rootOut_dir,params.evalOpts_string);

function testModel(annots,K,testOpts,opts,trainDir,segOutDir)
switch(opts.trainMethod)
  case 'linearRegression'
    h3dCut.learning.linearReg.testLinearRegression(annots,K,opts,trainDir,segOutDir);
  case 'kmeans'
    h3dCut.learning.kmeans.testKmeans(annots,K,opts,trainDir,segOutDir);
end

function saveParams_file(params)

fH=fopen([params.rootOut_dir 'params.txt'],'w');
miscFns.printStructure(params,fH);
fclose(fH);

opts=params.opts;
if(opts.visualize & opts.visOpts.debugVisualize)
  debugOpts.debugLevel=opts.debugLevel;
else
  debugOpts.debugLevel=0;
end
debugOpts.debugDir='';
debugOpts.debugPrefix='';

fH=fopen([params.rootOut_dir 'opts_humanModel.txt'],'w');
[hModel,hModel_opts]=h3dCut.createHumanModel(opts,debugOpts);
miscFns.printClass(hModel_opts,fH);
delete(hModel);
fclose(fH);

fH=fopen([params.rootOut_dir 'opts_segModel.txt'],'w');
[segH,segH_opts]=h3dCut.createSegObject(opts,debugOpts);
miscFns.printClass(segH_opts,fH);
delete(segH);
fclose(fH);

