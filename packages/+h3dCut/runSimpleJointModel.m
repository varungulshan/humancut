function runSimpleJointModel(anno,K,outDir,opts,testOpts)
% K is the h3d structure that stores joint indexes by name

img=im2double(imread(anno.imgPath));
[h w nCh]=size(img);

if(opts.visualize & opts.visOpts.debugVisualize)
  debugOpts.debugLevel=opts.debugLevel;
  debugOpts.debugDir=[outDir 'debug/'];
  if(~exist(debugOpts.debugDir,'dir')), mkdir(debugOpts.debugDir); end;
  debugOpts.debugPrefix=sprintf('%s_',anno.fullImgName);
else
  debugOpts.debugLevel=0;
  debugOpts.debugDir='';
  debugOpts.debugPrefix='';
end

[hModel,hModel_opts]=h3dCut.createHumanModel(opts,debugOpts);
[segH,segH_opts]=h3dCut.createSegObject(opts,debugOpts);
hModel.initialize(anno,K,[h w]);
segH.initializeFull(img,hModel);

if(debugOpts.debugLevel>0)
  segOverlay=humanCut.overlaySeg(img,segH.seg,opts.visOpts.segBdryWidth);
  humanOverlay=hModel.drawModel(segOverlay);
  miscFns.saveDebug(debugOpts,humanOverlay,sprintf('seg.%s',opts.visOpts.imgExt));
  posterior=segH.getColorPosterior();
  miscFns.saveDebug(debugOpts,posterior,sprintf('colorUnary.%s',opts.visOpts.imgExt));
  shapePosterior=hModel.getShapePosterior();
  miscFns.saveDebug(debugOpts,shapePosterior,sprintf('shapePosterior.%s',opts.visOpts.imgExt));
end

seg=segH.seg;

delete(hModel);
delete(segH);

save([outDir anno.fullImgName '.mat'],'seg'); 
