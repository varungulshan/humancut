function [segH,segOpts]=createSegObject(opts,debugOpts)

switch(opts.segMethod)
  case 'grabCut_humanModel'
    segOpts=humanSegmenters.grabCut_simple.segOpts(opts.segMethod_optsString);
    segH=humanSegmenters.grabCut_simple.segEngine(segOpts,debugOpts);
  case 'grabCut_humanModelParts'
    segOpts=humanSegmenters.grabCut_simple_parts.segOpts(opts.segMethod_optsString);
    segH=humanSegmenters.grabCut_simple_parts.segEngine(segOpts,debugOpts);
  otherwise
    error('Invalid segmentation method: %s\n',opts.segMethod);
end
