function opts=makeRegressionOpts(optsString)

opts=struct();
switch(optsString)
  case 'try1'
    opts.maskResolutionX=200;
    opts.maskResolutionY=200;
    opts.headBBoxHeight_scale=1.5; % size of head bounding box compared to torso length
    % Aspect ratio is maintained
  otherwise
    error('Invalid option string: %s\n',optsString);
end
