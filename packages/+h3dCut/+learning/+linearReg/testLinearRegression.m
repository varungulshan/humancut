function testLinearRegression(annots,K,opts,trainDir,outDir)

import h3dCut.learning.linearReg.*;

% First build the human models for each human in annots
% and load the corresponding segmentation?

numHumans=numel(annots);

rOpts=makeRegressionOpts(opts.trainMethod_optsString); % regression opts
[X,Y]=initializeXY(rOpts,numHumans);

if(opts.visualize & opts.visOpts.debugVisualize)
  debugOpts.debugLevel=opts.debugLevel;
  debugOpts.debugDir=[outDir 'debug/'];
  if(~exist(debugOpts.debugDir,'dir')), mkdir(debugOpts.debugDir); end;
  debugOpts.debugPrefix='';
else
  debugOpts.debugLevel=0;
  debugOpts.debugDir='';
  debugOpts.debugPrefix='';
end

tmpLoad=load([trainDir 'weights.mat']); % Provides variable W

parfor i=1:numHumans
%for i=1:numHumans
  anno=annots(i);
  imgName=anno.fullImgName;
  img=im2double(imread(anno.imgPath));
  processImg(anno,img,opts,debugOpts,rOpts,outDir,K,tmpLoad.W);
end
 
function processImg(anno,img,opts,debugOpts,rOpts,outDir,K,W)
  [hModel,hModel_opts]=h3dCut.createHumanModel(opts,debugOpts);
  [segH,segH_opts]=h3dCut.createSegObject(opts,debugOpts);
  hModel.initialize(anno,K); % Only intializes joint positions
  X=hModel.getHeadX();
  predY=X'*W;
  putHeadMask(predY,rOpts,hModel,img); % Initializes complete shape prior of human model, 
  %hModel.initializeColors(img);
  % And not just the head, this completes initialization of hModel
  segH.initializeFull(img,hModel);
  if(debugOpts.debugLevel>0)
    debugOpts.debugPrefix=sprintf('%s_',anno.fullImgName);
    segOverlay=humanCut.overlaySeg(img,segH.seg,opts.visOpts.segBdryWidth);
    humanOverlay=hModel.drawModel(segOverlay);
    miscFns.saveDebug(debugOpts,humanOverlay,sprintf('seg.%s',opts.visOpts.imgExt));
    posterior=segH.getColorPosterior();
    miscFns.saveDebug(debugOpts,posterior,sprintf('colorUnary.%s',opts.visOpts.imgExt));
    shapePosterior=hModel.getShapePosterior();
    miscFns.saveDebug(debugOpts,shapePosterior,sprintf('shapePosterior.%s',opts.visOpts.imgExt));
  end
  seg=segH.seg;
  delete(hModel);
  delete(segH);
  save([outDir anno.fullImgName '.mat'],'seg'); 

function putHeadMask(yPred,opts,hModel,img)

resolutionX=opts.maskResolutionX;
resolutionY=opts.maskResolutionY;

assert(numel(yPred)==resolutionY*resolutionX);
yPred=reshape(yPred,[resolutionY resolutionX]);

referenceLength=hModel.getTorsoScale();
boxH=referenceLength * opts.headBBoxHeight_scale;

resizeBox=boxH/resolutionY;

yImgRes=imresize(yPred,resizeBox,'method','bilinear');
hModel.initializeShape(yImgRes,hModel.getEyesCenter(),img);

function [X,Y]=initializeXY(rOpts,numHumans)

%X=zeros(13,numHumans); % 6*2=12 joint coordinates + 1 bias term
X=zeros(11,numHumans); % 6*2=12 joint coordinates + 1 bias term, remove 1 joint of torso
% because origin is set to centroid, hence causes rank deficiecy of X, so becomes
% 11 dimensional
Y=zeros(numHumans,rOpts.maskResolutionY*rOpts.maskResolutionX);
