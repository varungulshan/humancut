function trainLinearRegression(annots,K,opts,trainOutDir)

import h3dCut.learning.linearReg.*;

% First build the human models for each human in annots
% and load the corresponding segmentation?

numHumans=numel(annots);

gtDir=opts.gtDir;
rOpts=makeRegressionOpts(opts.trainMethod_optsString); % regression opts
[X,Y]=initializeXY(rOpts,numHumans);

if(opts.visualize & opts.visOpts.debugVisualize)
  debugOpts.debugLevel=opts.debugLevel;
  debugOpts.debugDir=[trainOutDir 'debug/'];
  if(~exist(debugOpts.debugDir,'dir')), mkdir(debugOpts.debugDir); end;
  debugOpts.debugPrefix='';
else
  debugOpts.debugLevel=0;
  debugOpts.debugDir='';
  debugOpts.debugPrefix='';
end

for i=1:numHumans
  anno=annots(i);
  imgName=anno.fullImgName;
  gt=imread([gtDir imgName '.png']);
  [hModel,hModel_opts]=h3dCut.createHumanModel(opts,debugOpts);
  hModel.initialize(anno,K);
  X(:,i)=hModel.getHeadX();
  Y(i,:)=getHeadMask(gt,rOpts,hModel.getTorsoScale(),hModel.getEyesCenter());
  if(debugOpts.debugLevel>0)
    debugOpts.debugPrefix=[imgName '_gt_'];
    saveTrainingMask(Y(i,:),rOpts,debugOpts);
  end
end

% W is the regression vector, for the solution of:
% |X'W-Y| (l2 norm)
W=inv(X*X')*X*Y;

if(debugOpts.debugLevel>0)
  Ypred=X'*W;
  for i=1:numHumans
    anno=annots(i);
    imgName=anno.fullImgName;
    debugOpts.debugPrefix=[imgName '_pred_'];
    saveTrainingMask(Ypred(i,:),rOpts,debugOpts);
  end
end

save([trainOutDir 'weights.mat'],'W');

function saveTrainingMask(mask,opts,debugOpts)

h=opts.maskResolutionY;
w=opts.maskResolutionX;
assert(h*w==numel(mask));
mask=reshape(mask,[h w]);

miscFns.saveDebug(debugOpts,mask,'headMask.jpg');

function [X,Y]=initializeXY(rOpts,numHumans)

%X=zeros(13,numHumans); % 6*2=12 joint coordinates + 1 bias term
X=zeros(11,numHumans); % 6*2=12 joint coordinates + 1 bias term, remove 1 joint of torso
% because origin is set to centroid, hence causes rank deficiecy of X, so becomes
% 11 dimensional
Y=zeros(numHumans,rOpts.maskResolutionY*rOpts.maskResolutionX);

function Y=getHeadMask(gt,opts,referenceLength,center)

resolutionX=opts.maskResolutionX;
resolutionY=opts.maskResolutionY;

aspectRatio=resolutionX/resolutionY;

boxH=referenceLength * opts.headBBoxHeight_scale;
boxW=aspectRatio * boxH;
[h w]=size(gt);

boxH=round(boxH);
boxW=round(boxW);

assert(strcmp(class(gt),'uint8'));
cropY=zeros([boxH boxW],'uint8');

xLeft = round(center(1) - boxW/2);
xRight = xLeft + boxW -1;
xLeftOffset = max(0,1-xLeft);
xRightOffset = max(0,xRight-w);
xLeft=xLeft+xLeftOffset;
xRight=xRight-xRightOffset;

yTop = round(center(2) - boxH/2);
yBottom = yTop + boxH -1;
yTopOffset = max(0,1-yTop);
yBottomOffset = max(0,yBottom-h);
yTop=yTop+yTopOffset;
yBottom=yBottom-yBottomOffset;

cropY((1+yTopOffset):(boxH-yBottomOffset),(1+xLeftOffset):(boxW-xRightOffset))= ...
    gt(yTop:yBottom,xLeft:xRight);

cropY=im2double(cropY);
cropY=imresize(cropY,[resolutionX resolutionY],'method','bilinear');
Y=cropY(:)';
