function testLinearRegression(annots,K,opts,trainDir,outDir)

import h3dCut.learning.kmeans.*;

% First build the human models for each human in annots
% and load the corresponding segmentation?

numHumans=numel(annots);

kOpts=makeKmeansOpts(opts.trainMethod_optsString); % regression opts
[X,Y]=initializeXY(kOpts,numHumans);

if(opts.visualize & opts.visOpts.debugVisualize)
  debugOpts.debugLevel=opts.debugLevel;
  debugOpts.debugDir=[outDir 'debug/'];
  if(~exist(debugOpts.debugDir,'dir')), mkdir(debugOpts.debugDir); end;
  debugOpts.debugPrefix='';
else
  debugOpts.debugLevel=0;
  debugOpts.debugDir='';
  debugOpts.debugPrefix='';
end

tmpLoad=load([trainDir 'means.mat']); % Provides variable Xmeans and Ymeans

parfor i=1:numHumans
%for i=1:numHumans
  anno=annots(i);
  imgName=anno.fullImgName;
  img=im2double(imread(anno.imgPath));
  processImg(anno,img,opts,debugOpts,kOpts,outDir,K,tmpLoad);
end
 
function processImg(anno,img,opts,debugOpts,kOpts,outDir,K,tmpLoad)
  [hModel,hModel_opts]=h3dCut.createHumanModel(opts,debugOpts);
  [segH,segH_opts]=h3dCut.createSegObject(opts,debugOpts);
  hModel.initialize(anno,K); % Only intializes joint positions
  X=hModel.getHeadX_kmeans();
  predY=findNearestCluster(X,tmpLoad.Xmeans,tmpLoad.Ymeans);
  putHeadMask(predY,kOpts,hModel,img); % Initializes complete shape prior of human model, 
  %hModel.initializeColors(img);
  % And not just the head, this completes initialization of hModel
  segH.initializeFull(img,hModel);
  if(debugOpts.debugLevel>0)
    debugOpts.debugPrefix=sprintf('%s_',anno.fullImgName);
    segOverlay=humanCut.overlaySeg(img,segH.seg,opts.visOpts.segBdryWidth);
    humanOverlay=hModel.drawModel(segOverlay);
    miscFns.saveDebug(debugOpts,humanOverlay,sprintf('seg.%s',opts.visOpts.imgExt));
    posterior=segH.getColorPosterior();
    miscFns.saveDebug(debugOpts,posterior,sprintf('colorUnary.%s',opts.visOpts.imgExt));
    shapePosterior=hModel.getShapePosterior();
    miscFns.saveDebug(debugOpts,shapePosterior,sprintf('shapePosterior.%s',opts.visOpts.imgExt));
  end
  seg=segH.seg;
  delete(hModel);
  delete(segH);
  save([outDir anno.fullImgName '.mat'],'seg'); 

function yPred=findNearestCluster(X,Xmeans,Ymeans)

assert(size(X,1)==size(Xmeans,1));
dist=Xmeans-repmat(X,[1 size(Xmeans,2)]);
dist=dist.*dist;
dist=sum(dist,1);
[minDist,minIdx]=min(dist);
yPred=Ymeans(minIdx,:);

function putHeadMask(yPred,opts,hModel,img)

resolutionX=opts.maskResolutionX;
resolutionY=opts.maskResolutionY;

assert(numel(yPred)==resolutionY*resolutionX);
yPred=reshape(yPred,[resolutionY resolutionX]);

referenceLength=hModel.getTorsoScale();
boxH=referenceLength * opts.headBBoxHeight_scale;

resizeBox=boxH/resolutionY;

yImgRes=imresize(yPred,resizeBox,'method','bilinear');
hModel.initializeShape(yImgRes,hModel.getEyesCenter(),img);

function [X,Y]=initializeXY(kOpts,numHumans)

X=zeros(12,numHumans); % 6*2=12 joint coordinates + 1 bias term
Y=zeros(numHumans,kOpts.maskResolutionY*kOpts.maskResolutionX);
