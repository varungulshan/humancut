function trainKmeans(annots,K,opts,trainOutDir)

import h3dCut.learning.kmeans.*;

% First build the human models for each human in annots
% and load the corresponding segmentation?

numHumans=numel(annots);

gtDir=opts.gtDir;
kOpts=makeKmeansOpts(opts.trainMethod_optsString); % regression opts
[X,Y]=initializeXY(kOpts,numHumans);

if(opts.visualize & opts.visOpts.debugVisualize)
  debugOpts.debugLevel=opts.debugLevel;
  debugOpts.debugDir=[trainOutDir 'debug/'];
  if(~exist(debugOpts.debugDir,'dir')), mkdir(debugOpts.debugDir); end;
  debugOpts.debugPrefix='';
else
  debugOpts.debugLevel=0;
  debugOpts.debugDir='';
  debugOpts.debugPrefix='';
end

for i=1:numHumans
  anno=annots(i);
  imgName=anno.fullImgName;
  gt=imread([gtDir imgName '.png']);
  [hModel,hModel_opts]=h3dCut.createHumanModel(opts,debugOpts);
  hModel.initialize(anno,K);
  X(:,i)=hModel.getHeadX_kmeans();
  Y(i,:)=getHeadMask(gt,kOpts,hModel.getTorsoScale(),hModel.getEyesCenter());
  if(debugOpts.debugLevel>0)
    debugOpts.debugPrefix=[imgName '_gt_'];
    saveTrainingMask(Y(i,:),kOpts,debugOpts);
  end
end

numClusters=kOpts.numClusters;
[clusterIdx,clusterMean]=kmeans(X',numClusters);

Ymeans=zeros(numClusters,size(Y,2));
clusterSizes=zeros(1,numClusters);
for i=1:numHumans
  clusterId=clusterIdx(i);
  Ymeans(clusterId,:)=Ymeans(clusterId,:)+Y(i,:);
  clusterSizes(clusterId)=clusterSizes(clusterId)+1;
end

for i=1:numClusters
  Ymeans(i,:)=Ymeans(i,:)/clusterSizes(i);
end

Xmeans=clusterMean';

save([trainOutDir 'means.mat'],'Xmeans','Ymeans');

if(debugOpts.debugLevel>0)
  for i=1:size(Ymeans,1)
    debugOpts.debugPrefix=sprintf('Cluster_%02d_',i);
    saveTrainingMask(Ymeans(i,:),kOpts,debugOpts);
  end
end

function saveTrainingMask(mask,opts,debugOpts)

h=opts.maskResolutionY;
w=opts.maskResolutionX;
assert(h*w==numel(mask));
mask=reshape(mask,[h w]);

miscFns.saveDebug(debugOpts,mask,'headMask.jpg');

function [X,Y]=initializeXY(kOpts,numHumans)

X=zeros(12,numHumans); % 6*2=12 joint coordinates
Y=zeros(numHumans,kOpts.maskResolutionY*kOpts.maskResolutionX);

function Y=getHeadMask(gt,opts,referenceLength,center)

resolutionX=opts.maskResolutionX;
resolutionY=opts.maskResolutionY;

aspectRatio=resolutionX/resolutionY;

boxH=referenceLength * opts.headBBoxHeight_scale;
boxW=aspectRatio * boxH;
[h w]=size(gt);

boxH=round(boxH);
boxW=round(boxW);

assert(strcmp(class(gt),'uint8'));
cropY=zeros([boxH boxW],'uint8');

xLeft = round(center(1) - boxW/2);
xRight = xLeft + boxW -1;
xLeftOffset = max(0,1-xLeft);
xRightOffset = max(0,xRight-w);
xLeft=xLeft+xLeftOffset;
xRight=xRight-xRightOffset;

yTop = round(center(2) - boxH/2);
yBottom = yTop + boxH -1;
yTopOffset = max(0,1-yTop);
yBottomOffset = max(0,yBottom-h);
yTop=yTop+yTopOffset;
yBottom=yBottom-yBottomOffset;

cropY((1+yTopOffset):(boxH-yBottomOffset),(1+xLeftOffset):(boxW-xRightOffset))= ...
    gt(yTop:yBottom,xLeft:xRight);

cropY=im2double(cropY);
cropY=imresize(cropY,[resolutionX resolutionY],'method','bilinear');
Y=cropY(:)';
