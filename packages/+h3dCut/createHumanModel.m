function [hModel,hModel_opts]=createHumanModel(opts,debugOpts)
% Function to create an empty human model (whose interface is
% defined by humanModels.genericModel.
% opts.humanModel_type specifies the type of human model to create
% opts.humanModel_optsString specified option string for the human model

switch(opts.humanModel_type)
  case 'jointsModel'
    hModel_opts=humanModels.jointsModel2_opts(opts.humanModel_optsString);
    hModel=humanModels.jointsModel2(debugOpts,hModel_opts);
  case 'jointsModel_partColors'
    hModel_opts=humanModels.jointsModel2_parts_opts(opts.humanModel_optsString);
    hModel=humanModels.jointsModel2_parts(debugOpts,hModel_opts);
  case 'jointsModel_partColors_trainHead'
    hModel_opts=humanModels.jointsModel_parts_trainedHead_opts(opts.humanModel_optsString);
    hModel=humanModels.jointsModel_parts_trainedHead(debugOpts,hModel_opts);
  otherwise
    error('Invalid model type: %s in createHumanModel\n',modelType);
end
