function compile_mex()

cwd=miscFns.extractDirPath(mfilename('fullpath'));

mexCmds=cell(0,1);
mexCmds{end+1} = sprintf('mex -O -largeArrayDims %ssppega_resumable_fast.cpp -outdir %s',cwd,cwd);
mexCmds{end+1} = sprintf('mex -O -largeArrayDims %ssppega.c -outdir %s',cwd,cwd);
mexCmds{end+1} = sprintf('mex -O -largeArrayDims %ssppega_resumable.cpp -outdir %s',cwd,cwd);

for i=1:length(mexCmds)
  fprintf('Executing %s\n',mexCmds{i});
  eval(mexCmds{i});
end

