// Modified sppega resumable to use sparsity in the regularization
// factor also
#include "mex.h"
#include <math.h>
#include <strings.h>
#include <string.h>

#define max(x,y) (((x)<=(y))?(y):(x))
#define min(x,y) (((x)>=(y))?(y):(x))


inline void myAssert(bool condition,char *msg){
  if(!condition){
    mexErrMsgTxt(msg);
  }
}

/* ---------------------------------------------------------------- */
double
product(mxArray const * X_array,
        double const * w,
        mwIndex i)
{
  double const * X = mxGetPr(X_array) ;
  mwIndex const * Jc = mxGetJc(X_array) ;
  mwIndex const * Ir = mxGetIr(X_array) ;
  double acc = 0 ;
  /*
  if (!mxIsSparse(X_array)) {
    mexErrMsgTxt("X is not sparse") ;
  }
  */
  {
    int index = Jc[i] ;
    int stop = Jc[i+1] ;
    while(index < stop) {
      acc += w[Ir[index]] * X[index] ;
      index++ ;
    }
  }
  return acc ;
}

/* ---------------------------------------------------------------- */

void
accumulate(double * w,
           mxArray const * X_array,
           double const * diagonal,
           double mult,
           mwIndex i)
{
  double const * X = mxGetPr(X_array) ;
  mwIndex const * Jc = mxGetJc(X_array) ;
  mwIndex const * Ir = mxGetIr(X_array) ;
  /*
  if (!mxIsSparse(X_array)) {
    mexErrMsgTxt("X is not sparse") ;
  }
  */
  {
    int index = Jc[i] ;
    int stop = Jc[i+1] ;
    while(index < stop) {
      int j = Ir[index] ;
      //w[j] += X[index] * diagonal[j] * mult ;
      w[j] += X[index] * mult ; // Conditioning removed for time being
      index++ ;
    }
  }
}


/* ---------------------------------------------------------------- */
void
calc_objective(double * en,
               mxArray const * data_array, /* dense */
               mxArray const * labels_array,
               mxArray const * model_array,
               mxArray const * Hmodel_array)
{
  int i ;
  int numPos = 0 ;
  int numNeg = 0 ;
  int numData = mxGetN(data_array) ;
  double const * labels = mxGetPr(labels_array) ;
  double const * model = mxGetPr(model_array) ;
  double const * Hmodel = mxGetPr(Hmodel_array) ;

  en[0] = 0 ; /* tot */
  en[1] = 0 ; /* reg */
  en[2] = 0 ; /* pos */
  en[3] = 0 ; /* neg */

  /* compute loss */
  for (i = 0 ; i < numData ; ++i) {
    double score = product(data_array, model, i) ;
    if (labels[i] > 0) {
      en[2] += max(0, 1 - score) ;
      numPos ++ ;
    } else {
      en[3] += max(0, 1 + score) ;
      numNeg ++ ;
    }
  }
  en[2] = en[2] / numPos ;
  en[3] = en[3] / numPos ;

  /* compute regularizer */
  {
    double acc = 0 ;
    int dimension = mxGetNumberOfElements(model_array) ;
    for (i = 0 ; i < dimension ; ++i) acc += model[i] * Hmodel[i] ;
    en[1] = 0.5 * acc ;
  }
  en[0] = en[1] + en[2] + en[3] ;
}

enum {
  IN_DATA = 0,
  IN_LABELS,
  IN_LAMBDA,
  IN_NUMITER,
  IN_PRECOND,
  OPT_IN_W_INIT,
  OPT_IN_ITERNUM,
  OPT_IN_A
} ;

/* ---------------------------------------------------------------- */
void randperm(int * perm, int dimension)
{
  int i, j, k ;
  for (i = 0; i < dimension ; i++) perm[i] = i;
  for (i = 0; i < dimension ; i++) {
    j = (int)(drand48()*(dimension - i)) + i ;
    j = min(j, dimension-1) ;
    k = perm[i] ; perm[i] = perm[j] ; perm[j] = k ;
  }
}

/* ---------------------------------------------------------------- */
void mexFunction (int nout, mxArray * out [],
                  int nin, const mxArray * in [])
{
  if (nin < 5) {
    mexErrMsgTxt("Atleast 5 arguments please.") ;
  }

  mxArray const * data_array = in[IN_DATA] ;
  mxArray const * labels_array = in[IN_LABELS] ;
  mxArray const * lambda_array = in[IN_LAMBDA] ;
  mxArray const * numIter_array = in[IN_NUMITER] ;
  mxArray const * precond_array = in[IN_PRECOND] ;

  int i, k, j ;

  {
    int dimension = mxGetM(data_array) ;
    int numData = mxGetN(data_array) ;
    int t, pass, ent ;
    double a = 1; // rescaling of w
    int maxNumIterations = (int)mxGetScalar(numIter_array) ;
    mxArray * model_array = mxCreateDoubleMatrix(dimension, 1, mxREAL) ;
    double * model = mxGetPr(model_array) ;
    double const * precond = mxGetPr(precond_array) ;
    double const * labels = mxGetPr(labels_array) ;
    double lambda = mxGetScalar(lambda_array) ;
    int * perm = (int*)mxMalloc(numData * sizeof(int)) ;
    int t0 = (int)max(ceil(10 / lambda),1) ;
    //double xrate = 0 ;
    //int const regulInterval = 10 ;
    //int const objectiveInterval = (int)max(1,ceil(maxNumIterations / 50)) ;
    //mxArray * objective_array = mxCreateDoubleMatrix(4, maxNumIterations / objectiveInterval + 1, mxREAL) ;
    //double * objective = mxGetPr(objective_array) ;

    if (mxGetNumberOfElements(labels_array) != numData) {
      mexErrMsgTxt("LABELS has not the right size.") ;
    }
    if (mxGetNumberOfElements(precond_array) != dimension) {
      mexErrMsgTxt("PRECOND has not the right size.") ;
    }
    if (mxGetClassID(precond_array) != mxDOUBLE_CLASS) {
      mexErrMsgTxt("PRECOND has not the right class.") ;
    }
    if (mxGetClassID(labels_array) != mxDOUBLE_CLASS) {
      mexErrMsgTxt("LABELS has not the right class.") ;
    }

    mexPrintf("Note: preconditioning is currently disabled in this code\n");
    mexPrintf("sppega: maxNumIterations = %d\n", maxNumIterations) ;
    mexPrintf("sppega: dimension = %d\n", dimension) ;
    mexPrintf("sppega: numData = %d\n", numData) ;
    mexPrintf("sppega: lambda = %f\n", lambda) ;
    mexPrintf("sppega: t0 = %f\n", t0) ;
    //mexPrintf("sppega: regulInterval = %d\n", regulInterval) ;
    //mexPrintf("sppega: objectiveInterval = %d\n", objectiveInterval) ;

    pass = 0 ;
    t = 0 ;
    ent = 0 ;
    //xrate = 0;
    // Initialize from optional arguments if passed

    if(nin>OPT_IN_W_INIT){
      mexPrintf("Initial w passed\n");
      mxArray const *tmpW = in[OPT_IN_W_INIT];
      myAssert(mxGetNumberOfElements(tmpW)==dimension,
          "Incorrect dimension for w init\n");
      memcpy(model,mxGetPr(tmpW),sizeof(double)*dimension);
    }

    if(nin>OPT_IN_ITERNUM){
      mexPrintf("Initial iternum passed\n");
      mxArray const *tmp = in[OPT_IN_ITERNUM];
      myAssert(mxGetNumberOfElements(tmp)==1,"Iter num should be scalar\n");
      t = (int)mxGetScalar(tmp);
      maxNumIterations+=t;
    }

    if(nin>OPT_IN_A){
      mexPrintf("Initial a passed\n");
      mxArray const *tmp = in[OPT_IN_A];
      myAssert(mxGetNumberOfElements(tmp)==1,"a should be scalar\n");
      a = mxGetScalar(tmp);
    }

    for (; t < maxNumIterations; ++pass) {
      /*mexPrintf("sppega: pass %d\n", pass+1) ;*/
      randperm(perm, numData) ;

      for (i = 0 ; i < numData && t < maxNumIterations; ++i, ++t) {
        int label ;
        double score, rate = 1 / (lambda * (t + t0)) ;
        double aPrev = a;
        a = a - a/(t+t0);
        //xrate += rate ; 

        /* subgradient step */
        score = aPrev*product(data_array, model, perm[i]) ;
        label = (int)labels[perm[i]] ;
        if (score*label < 1) {
          accumulate(model, data_array, precond, (rate/a) * label, perm[i]) ;
        }

        /* compute the product H * model */
        /*
        if (t % regulInterval == 0 || t % objectiveInterval == 0) {
          memcpy(Hmodel, model, sizeof(double)*dimension) ;
          for (k = 0 ; k < mxGetNumberOfElements(H_array) ; ++k) {
            mxArray const * factor_array = mxGetCell(H_array, k) ;
            if (!mxIsSparse(factor_array)) {
              mexErrMsgTxt("H contains a non-sparse martrix.") ;
            }
            if (mxGetM(factor_array) != dimension ||
                mxGetN(factor_array) != dimension) {
              mexErrMsgTxt("H constains a factor of incorrect size.") ;
            }
            memcpy(tmp, Hmodel, sizeof(double)*dimension) ;
            for (j = 0 ; j < dimension ; ++j) {
              Hmodel[j] = product(factor_array, tmp, j) ;
            }
          }
        }
        */

        /* check objective */
        /*
        if (t % objectiveInterval == 0) {
          calc_objective(objective + 4 * (ent++),
                         data_array,
                         labels_array,
                         model_array,
                         Hmodel_array) ;
          mexPrintf("sppega: %05d: t/r/p/n %5.2g %5.2g %5.2g %5.2g; iter: %d\n",
                    ent, objective[0],objective[1],
                    objective[2],objective[3],t) ;
        }
        */

        /* regularizer step */
        /*
        if (t % regulInterval == 0) {
          int j ;
          for (j = 0 ; j < dimension ; ++j) {
            model[j] -= Hmodel[j] * xrate ;
          }
          xrate = 0 ;
        }
        */

      }
    }

    out[0] = model_array ;
    //out[1] = objective_array ;
    //mxDestroyArray(Hmodel_array) ;
    out[1] = mxCreateDoubleMatrix(1,1,mxREAL);
    out[2] = mxCreateDoubleMatrix(1,1,mxREAL);
    *mxGetPr(out[1]) = (double)t;
    *mxGetPr(out[2]) = a;
    //mxFree(tmp) ;
  }
}
