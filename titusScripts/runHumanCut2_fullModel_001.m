function runHumanCut2_fullModel_001()

cd ..;
setup;

expNum=1; % ---- DONT FORGET TO CHANGE ME !! ---------------

cwd=miscFns.extractDirPath(mfilename('fullpath'));

params.rootOut_dir=sprintf('/data/titus_scratch/varun/humanCut2/fullModel_grabCut/run%03d/',expNum);
params.opts.h3dAnno_dir=[cwd '../data/h3dAnnotations_2/'];

params.opts.method='frameByFrame_exactInit';
params.opts.numIter_proxyUpdates=5;

params.opts.humanModel_type='jointsModel';
params.opts.humanModel_optsString='jointsModel_try1';
params.opts.segMethod='grabCut_humanModel';
params.opts.segMethod_optsString='iter10_2';
params.opts.humanModel_optimizeMethod='fminsearchDescent';
params.opts.humanModel_optimizeMethod_opts='try1';

params.opts.visualize=true;
params.opts.visOpts.debugVisualize=true;
params.opts.visOpts.imgExt='jpg';
params.opts.visOpts.segBdryWidth=2;
params.overWrite=false;

params.evalOpts_string='try_proxy2';
params.testBench_cmd='testBench.getTests_proxy2()';

humanCut2.run_fullModel(params);
