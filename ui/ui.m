function varargout = ui(varargin)
% UI M-file for ui.fig
%      UI, by itself, creates a new UI or raises the existing
%      singleton*.
%
%      H = UI returns the handle to a new UI or the handle to
%      the existing singleton*.
%
%      UI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in UI.M with the given input arguments.
%
%      UI('Property','Value',...) creates a new UI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ui_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ui

% Last Modified by GUIDE v2.5 08-Apr-2010 17:57:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ui_OpeningFcn, ...
                   'gui_OutputFcn',  @ui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ui is made visible.
function ui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ui (see VARARGIN)

% --- Initializating the state machine of the UI -----

handles.data=[];

set(handles.canvas,'Units','pixels');
handles.data=uidata();
handles.data.baseLineCanvasPosition=get(handles.canvas,'Position');
handles.data.uiState='started';
handles=stateTransition_function(handles,'started');
% Choose default command line output for ui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ui wait for user response (see UIRESUME)
% uiwait(handles.figure1);

function brush=makeBrush(sz)
  [x,y] = meshgrid(-sz:sz,-sz:sz);
  r = x.^2 + y.^2;
  brush = r<=(sz*sz);
  brush=uint8(brush);

% --- Outputs from this function are returned to the command line.
function varargout = ui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in button_loadVideo.
function button_loadVideo_Callback(hObject, eventdata, handles)
% hObject    handle to button_loadVideo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[handles,allOk]=stateTransition_function(handles,'videoLoaded');
guidata(hObject,handles);

function refreshCanvas(handles)
switch(handles.data.canvasView)
  case 'gt'
    refreshLabelImg_gt(handles.data);
    %refreshLabelImg(handles.data);
  case 'drawing_segBoundary'
    refreshLabelImg_boundary(handles.data);
  case 'orig'
    showOrig(handles);
  case 'fg'
    showSegmentation(handles,'fg');
  case 'bg'
    showSegmentation(handles,'bg');
  case 'misc_1'
    showMisc(handles,1);
  case 'misc_2'
    showMisc(handles,2);
  case 'misc_3'
    showMisc(handles,3);
  case 'misc_4'
    showMisc(handles,4);
  case 'misc_5'
    showMisc(handles,5);
  case 'misc_6'
    showMisc(handles,6);
  case 'misc_7'
    showMisc(handles,7);
  case 'misc_8'
    showMisc(handles,8);
  case 'misc_9'
    showMisc(handles,9);
  case 'misc_10'
    showMisc(handles,10);
  otherwise
    error('Unknown view state %s\n',handles.data.canvasView);
end

function refreshLabelImg_gt(data)
  if(isempty(data.gtBoundaryMask))
    fprintf('No GT available\n');
    [h w nChannels]=size(data.curImg);
    img=zeros([h w nChannels]);
    set(data.labelImgHandle,'Cdata',img);

    txtH=text(10,20,'No GT available');
    set(txtH,'color','w','fontsize',20,'fontweight','bold');
    snapShot=getframe;
    snapShot.cdata=snapShot.cdata(1:h,1:w,:);
    set(data.labelImgHandle,'Cdata',snapShot.cdata);
    delete(txtH);

    drawnow expose;
    return;
  end

  labelImg=data.labelImg(:,:,data.frameNum);
  backImg=data.curImg;
  backImg(data.gtBoundaryMask)=data.gtBoundaryColors;
  alphaImage=zeros(size(labelImg));
  alphaImage(labelImg==1 | labelImg==2)=1;

  cmap=data.cmap;
  cmapR=cmap(:,1);
  cmapG=cmap(:,2);
  cmapB=cmap(:,3);
  labelLayer(:,:,1)=cmapR(labelImg+1);
  labelLayer(:,:,2)=cmapG(labelImg+1);
  labelLayer(:,:,3)=cmapB(labelImg+1);

  alphaImage=repmat(alphaImage,[1 1 3]);
  cdata=labelLayer.*alphaImage+backImg.*(1-alphaImage);
  set(data.labelImgHandle,'CData',cdata);
  drawnow expose;

function showMisc(handles,miscNum)
% Function to draw a custom view
% on the canvas

data=handles.data;
try
  evalCmd=sprintf('handles.data.segmenterH.view_%d()',miscNum);
  miscImg=eval(evalCmd);
catch
  fprintf('No misc view %d available\n',miscNum);
  [h w nChannels]=size(data.curImg);
  img=zeros([h w nChannels]);
  set(data.labelImgHandle,'Cdata',img);

  txtH=text(10,20,'No misc view available');
  set(txtH,'color','w','fontsize',20,'fontweight','bold');
  snapShot=getframe;
  snapShot.cdata=snapShot.cdata(1:h,1:w,:);
  set(data.labelImgHandle,'Cdata',snapShot.cdata);
  delete(txtH);
  drawnow expose;

  return;
end

img=imresize(miscImg,[handles.data.hCanvas handles.data.wCanvas],'nearest');
if(size(img,3)==1),
  img=repmat(img,[1 1 3]);
end
set(data.labelImgHandle,'Cdata',img);
%drawnow expose;

function showOrig(handles)
backImg=handles.data.curImg;
set(handles.data.labelImgHandle,'Cdata',backImg);

function showSegmentation(handles,showWhat)
data=handles.data;
seg=data.curSeg;
%seg=imresize(seg,[data.hCanvas data.wCanvas],'nearest');

switch(showWhat)
  case 'fg'
    mask=(seg==255);
  case 'bg'
    mask=(seg==0);
  otherwise
    error('Unknown case in showSegmentation\n');
end

backImg=data.curImg;
img=backImg;
canvasColor=[0 0 1];
for i=1:3
  tmpImg=img(:,:,i);
  tmpImg(~mask)=canvasColor(i);
  img(:,:,i)=tmpImg;
end
%img=(backImg).*repmat(double(mask),[1 1 size(backImg,3)]);
set(data.labelImgHandle,'Cdata',img);
drawnow expose;

function seg=makeSegFromLabel(labelImg)
  seg=128*ones(size(labelImg),'uint8');
  seg(labelImg==1 | labelImg==3|labelImg==6)=255;
  seg(labelImg==2 | labelImg==4|labelImg==7)=0;

function refreshLabelImg(data)
  amap=data.amap;
  labelImg=data.labelImg(:,:,data.frameNum);
  backImg=data.inVideo(:,:,:,data.frameNum);
  alphaImage=amap(labelImg+1);
  cmap=data.cmap;
  cmapR=cmap(:,1);
  cmapG=cmap(:,2);
  cmapB=cmap(:,3);
  labelLayer(:,:,1)=cmapR(labelImg+1);
  labelLayer(:,:,2)=cmapG(labelImg+1);
  labelLayer(:,:,3)=cmapB(labelImg+1);

  alphaImage=repmat(alphaImage,[1 1 3]);
  cdata=labelLayer.*alphaImage+backImg.*(1-alphaImage);
  set(data.labelImgHandle,'CData',cdata);
  drawnow expose;

function refreshLabelImg_boundary(data)
  labelImg=data.labelImg;
  backImg=data.curImg;
  %stTime=clock;
  backImg(data.segBoundaryMask)=data.segBoundaryColors;
  %fprintf('Took %.3f seconds to drawSegboundary\n',etime(clock,stTime));
  %stTime=clock;
  mask=(labelImg~=0);
  labels=labelImg(mask);
  clrs=data.cmap(labels+1,:);
  clrs=clrs(:);
  backImg(repmat(mask,[1 1 3]))=clrs;
  %fprintf('Took %.3f seconds to draw labels\n',etime(clock,stTime));
  %backImg(data.labelMask)=data.labelBoundaryColors;
  %alphaImage=zeros(size(labelImg));
  %%alphaImage(labelImg==1 | labelImg==2|labelImg==6|labelImg==7|labelImg==5)=1;
  %alphaImage=data.amap(labelImg+1);
%
  %stTime=clock;
  %cmap=data.cmap;
  %cmapR=cmap(:,1);
  %cmapG=cmap(:,2);
  %cmapB=cmap(:,3);
  %labelLayer(:,:,1)=cmapR(labelImg+1);
  %labelLayer(:,:,2)=cmapG(labelImg+1);
  %labelLayer(:,:,3)=cmapB(labelImg+1);
%
  %alphaImage=repmat(alphaImage,[1 1 3]);
  %cdata=labelLayer.*alphaImage+backImg.*(1-alphaImage);
  %fprintf('Took %.3f seconds to drawLabels\n',etime(clock,stTime));
  %stTime=clock;
  %set(data.labelImgHandle,'CData',cdata);
  set(data.labelImgHandle,'CData',backImg);
  %drawnow expose;
  %fprintf('Took %.3f seconds to set cdata and expose\n',etime(clock,stTime));

function [handles,allOk]=stateTransition_function(handles,nxtState)
% Function to implement the state machine for the user interface
% See your notebook #3 for the state machine diagram, i dont
% have a image of it (If only i had a tablet)

curState=handles.data.uiState;
switch(nxtState)
  case 'started'
    %error('You cant go request to go back to started state\n');
    [handles,allOk]=resetUI(handles);
  case 'videoLoaded'
    if(strcmp(curState,'video_reload'))
      % Do nothing
      allOk=true;
    else
      switch(curState)
        case {'videoPPed','segStarted'}
          handles=cleanUp(handles);
      end
      [handles,allOk]=loadVideo(handles);
    end
  case 'videoPPed'
    % You can only come to this state from videoLoaded, any other state
    % raise error
    if(~strcmp(curState,'videoLoaded'))
      error('Going to state videoPPed from %s state not allowed, bug\n',curState);
    end
    [handles,allOk]=ppVideo(handles);
  case 'segStarted'
    % You can only come to this state from videoPPed or itself, any other state
    % raise error
    if(~(strcmp(curState,'videoPPed')|strcmp(curState,'segStarted')|...
         strcmp(curState,'videoQsub_loaded')))
      error('Going to state videoSegmented from %s state not allowed, bug\n',curState);
    end
    if(strcmp(curState,'videoQsub_loaded'))
      %[handles,allOk]=segmentVideo_qsubLoaded(handles);
      % Nothing to do !!
      allOk=true;
    else
      [handles,allOk]=segmentVideo(handles);
    end
  case 'video_reload'
    if(~(strcmp(curState,'videoPPed')|strcmp(curState,'segStarted')|...
         strcmp(curState,'videoLoaded')))
      error('Going to state videoReloaded from %s state not allowed, bug\n',curState);
    end
    [handles,allOk]=reloadVideo(handles);

  otherwise
    error('Invalid state %s requested, bug\n',nxtState);
end

if(allOk)
  handles.data.uiState=nxtState;
else
  fprintf('UI could not make state transition to %s\n',nxtState);
end

function [handles,allOk]=resetUI(handles)

set(handles.canvas,'Units','pixels');
set(handles.panel_stroke,'SelectedObject',handles.radio_strokeFG_hard);

set(handles.panel_view,'visible','off');
set(handles.button_startSeg,'enable','off');
set(handles.button_saveLabels,'enable','off');
set(handles.button_loadLabels,'enable','off');
set(handles.button_preProcess,'enable','off');
set(handles.button_clearFrame,'enable','off');
set(handles.button_preProcess,'enable','off');
set(handles.chkbox_autoEdit,'Value',1);
set(handles.button_updateSeg,'enable','off');
set(handles.button_saveViewAvi,'enable','off');
set(handles.button_diagnose,'enable','off');
set(handles.button_reloadVideo,'enable','off');
set(handles.button_popOut,'enable','off');
set(handles.slider_gamma,'enable','off');
set(handles.popup_videoMethod,'String',{...
   'motionCut2',...
   'motionCut',...
   'snapCut2',...
   'flowProp_try1','flowProp_gc','flowProp_edgePb',...
   'flowProp_edgePbNN','Use text file'});
set(handles.popup_videoMethod,'Value',1);

set(handles.figure1,'KeyPressFcn',@figure1_MyKeyPressFcn);

data=handles.data;
%data.baseLineCanvasPosition=handles.data.baseLineCanvasPosition;
data.debugLevel=2;
data.drag=[];
data.thin_firstClick=[];
data.thin_secondClick=[];
data.strokeType=1; % 1 is for fgHard
set(handles.panel_view,'SelectedObject',handles.radio_boundaryView);
data.canvasView='drawing_segBoundary';
data.uiState='started';
data.brushRad=2;
data.brushMask=makeBrush(data.brushRad);
data.brushType=1;
set(handles.panel_strokeType,'SelectedObject',handles.radio_brushBrush);
data.boxStarted=false;
data.videoReaderH=[];

data.scroll_value=0;
data.scroll_step=2;
data.segBoundaryColors=[];
data.segBoundaryMask=[];
data.segBoundaryColor_inside=[1 0 0];
data.segBoundaryColor_outside=[0 1 0];
data.segBoundaryWidth=2;
data.gtDir='./data/gt/';
data.gamma=300;
data.geoGamma=0.9;

data.origCanvasPosition=data.baseLineCanvasPosition;
set(handles.canvas,'Position',data.baseLineCanvasPosition);
cla(handles.canvas,'reset');

segMethodStrings=get(handles.popup_videoMethod,'String');
data.segMethod=segMethodStrings{get(handles.popup_videoMethod,'Value')};

set(handles.canvas,'xlimmode','manual','ylimmode','manual','zlimmode','manual');
set(handles.canvas,'XTick',[],'YTick',[]);
set(handles.slider_canvasSize,'enable','on');
set(handles.slider_canvasSize,'value',1.0);
set(handles.slider_brushSize,'min',0);
set(handles.slider_brushSize,'max',10);
set(handles.slider_brushSize,'sliderstep',[1/9 1/9]);
set(handles.slider_brushSize,'value',data.brushRad);
set(handles.text_brushSize,'string',sprintf('Brush size: %d',data.brushRad));
set(handles.text_frameNumber,'string',sprintf('Frame number: '));

set(handles.slider_gamma,'value',data.gamma);
set(handles.text_gamma,'string',sprintf('Gamma: %.2f',data.gamma));
set(handles.text_geoGamma,'string',sprintf('Geo Gamma: %.2f',data.geoGamma));
set(handles.textEdit_geoGamma,'string',sprintf('%.3f',data.geoGamma));
set(handles.text_scrollValue,'string',sprintf('ScrollSize: %.5f',data.scroll_step));
set(handles.textEdit_scrollValue,'string',sprintf('%.5f',data.scroll_step));

handles.data=data;
allOk=true;

function handles=cleanUp(handles)
try
  if(~isempty(handles.data.videoReaderH)),
    delete(handles.data.videoReaderH);
  end
  if(~isempty(handles.data.videoReaderH_copy)),
    delete(handles.data.videoReaderH_copy);
  end
  if(~isempty(handles.data.segmenterH)),
    delete(handles.data.segmenterH);
  end
  handles.data.videoReaderH=[];
  handles.data.videoReaderH_copy=[];
  handles.data.segmenterH=[];
catch
  fprintf('No cleaning up to do\n');
end

function qFG=getThinStructures(data)
if(data.origResolution)
  error('Sorry, original resolution option only works with images at the moment, sorry for informing so late\n');
end
stTime=clock;
fprintf('Computing shortest paths\n');
pp=data.pp;
opts=data.opts.segOpts;
labelImg=zeros(size(data.labelImg),'uint8');
clickPt=data.thin_firstClick;
labelImg(clickPt(2),clickPt(1))=6;
[xx,starInfo]=getEnergies_starShape_principled(pp.inImage,labelImg,opts.contourOpts,pp.starPP);
qFG=starInfo.qFG;
fprintf('Shortest paths computed in %.2f seconds\n',etime(clock,stTime));

function [handles,allOk]=segmentVideo(handles)
  allOk=false;
  allOk=handles.data.segmenterH.start(handles.data.labelImg_video,handles.data.frameNum);
  if(~allOk), return; end;
  curSeg=imresize(handles.data.segmenterH.curFrame_seg(), ...
                     [handles.data.hCanvas handles.data.wCanvas],'nearest');
  %curSeg=myresize(handles.data.mapToCanvas,handles.data.segmenterH.curFrame_seg(), ...
                     %[handles.data.hCanvas handles.data.wCanvas]);
  %handles.data.labelImg_lastSegmentation=handles.data.labelImg;
  handles.data.curSeg=curSeg;

  [handles.data.segBoundaryMask,handles.data.segBoundaryColors]= ...
  miscFns.getSegBoundary_twoColors(curSeg,handles.data.segBoundaryColor_outside, ...
                           handles.data.segBoundaryColor_inside...
                          ,handles.data.segBoundaryWidth,handles.data.segBoundaryWidth);

  refreshCanvas(handles);
  set(handles.panel_stroke,'SelectedObject',handles.radio_brushAuto);
  guidata(handles.radio_brushAuto,handles);
  panel_stroke_SelectionChangeFcn(handles.radio_brushAuto,[],handles);
  handles=guidata(handles.radio_brushAuto);
  set(handles.button_updateSeg,'enable','on');
  %set(handles.panel_strokeType,'SelectedObject',handles.radio_brushBrush);
  %panel_strokeType_SelectionChangeFcn(handles.radio_brushBrush, [], handles)
  %handles=guidata(handles.radio_brushAuto);
  allOk=true;

function [handles,allOk]=ppVideo(handles)
  allOk=false;
  clear videoOptions;
  handles.data.segmenterH=videoOptions(handles.data);
  handles.data.segmenterH.preProcess();

  set(handles.button_startSeg,'enable','on');
  set(handles.button_preProcess,'enable','off');
  set(handles.slider_gamma,'enable','off');
  set(handles.popup_videoMethod,'enable','off');
  allOk=true;

function labelImg=overlaySegmentation(labelImg,seg)
  if(any(seg(:)==1 & (labelImg(:)==2|labelImg(:)==7)))
    warning('Hard constrained BG pixels segmented as FG, something wrong (can happen if run in origResolution)\n');
  elseif(any(seg(:)==0 & (labelImg(:)==1|labelImg(:)==6) ))
    warning('Hard constrained FG pixels segmented as BG, something wrong (can happen if run in origResolution)\n');
  end
  unknownMask=(labelImg==0|labelImg==3|labelImg==4);
  labelImg(unknownMask & seg==1)=3;
  labelImg(unknownMask & seg==0)=4;

function [handles,allOk]=reloadVideo(handles)

allOk=false;

handles=cleanUp(handles);
[handles,allOk]=loadVideo(handles,true);

function [handles,allOk]=loadVideo(handles,reload)

if(~exist('reload','var')), reload=false; end
allOk=false;
if(~reload)
  %[filename, pathname] = uigetfile({'*.*';'*.mat';'*.avi'},'Open video to segment',...
  %                      './data/myVideos/');
  [filename, pathname] = uigetfile({'*.*';'*.mat';'*.avi'},'Open video to segment',...
                        './data/');
                      
  if isequal(filename,0),  return; end;
else
  filename=handles.data.inFileName;
  pathname=handles.data.pathname;
end

[handles.data.videoReaderH]=myVideoReader(pathname,filename);
[handles.data.videoReaderH_copy]=myVideoReader(pathname,filename);

allOk=handles.data.videoReaderH.loadOk;

if(~allOk),
  delete(handles.data.videoReaderH);
  delete(handles.data.videoReaderH_copy);
  handles.data.videoReaderH=[];
  handles.data.videoReaderH_copy=[];
  warning('Could not read video properly\n');
  return;
end

[h,w,nCh,nFrames]=handles.data.videoReaderH.videoSize();

gtFile=[handles.data.gtDir miscFns.extractImgName(filename) '.mat'];
if(~exist(gtFile,'file')),
  handles.data.gtBoundaryMask=[];
  handles.data.gtBoundaryColors=[];
  handles.data.gtSeg_video=[];
else
  warning('Not yet implemented gt loading\n');
  handles.data.gtBoundaryMask=[];
  handles.data.gtBoundaryColors=[];
  handles.data.gtSeg_video=[];
end

set(handles.slider_frameNum,'min',1);
set(handles.slider_frameNum,'max',nFrames+eps); % the eps is to prevent crashing?
set(handles.slider_frameNum,'value',1);
set(handles.slider_frameNum,'sliderstep',[1/nFrames 1/nFrames]);
set(handles.button_updateSeg,'enable','off');
set(handles.button_clearFrame,'enable','on');
set(handles.button_preProcess,'enable','on');
set(handles.button_loadLabels,'enable','on');
set(handles.button_saveLabels,'enable','on');
set(handles.button_saveViewAvi,'enable','on');
set(handles.button_diagnose,'enable','on');
set(handles.button_reloadVideo,'enable','on');
set(handles.button_popOut,'enable','on');
set(handles.slider_canvasSize,'enable','off');
set(handles.slider_gamma,'enable','on');
set(handles.popup_videoMethod,'enable','on');
set(handles.button_startSeg,'enable','off');

set(handles.panel_stroke,'SelectedObject',handles.radio_strokeFG_hard);
panel_stroke_SelectionChangeFcn(handles.radio_strokeFG_hard,[],handles);
handles=guidata(handles.radio_strokeFG_hard);

set(handles.panel_strokeType,'SelectedObject',handles.radio_brushBrush);
panel_strokeType_SelectionChangeFcn(handles.radio_brushBrush,[],handles);
handles=guidata(handles.radio_brushBrush);

set(handles.panel_view,'visible','on');

handles.data.scroll_value=30; % Set this to a good intial value
handles.data.segBoundaryMask=[];
handles.data.segBoundaryColors=[];
handles.data.inFileName=filename;
handles.data.pathname=pathname;
handles.data.frameNum=1; % Frame number is 1 indexed
set(handles.text_frameNumber,'String',sprintf('%s: Frame number: %d',handles.data.inFileName,handles.data.frameNum));

axes(handles.canvas);
set(gcf,'DoubleBuffer','on');
set(handles.canvas,'xlimmode','manual','ylimmode','manual','zlimmode','manual');
set(handles.canvas,'XTick',[],'YTick',[]);

numLabels=length(get(handles.panel_stroke,'Children')); 

%alpha=get(handles.slider_labelsOpacity,'Value');
alpha=1;
amap=alpha*ones(numLabels,1);
amap(2:3)=1;amap(1)=0;
handles.data.amap=amap;

% -- Set up the color map ---
cmap=colorcube(10*numLabels);
cmap=cmap([1:10:10*numLabels],:);

% Make the colors bright:
cmap=brighten(cmap,0.7);

handles.data.cmap=cmap;
origPos=handles.data.origCanvasPosition;
hScale=w/origPos(3);
vScale=h/origPos(4);
if(hScale>vScale), 
  handles.data.imgReScale=hScale;
  newCanvasHeight=floor(h/hScale);
  newY=floor((origPos(4)-newCanvasHeight)/2+origPos(2));
  set(handles.canvas,'Position',[origPos(1) newY origPos(3) newCanvasHeight]);
else
  handles.data.imgReScale=vScale;
  %newCanvasWidth=floor(w/vScale);
  newCanvasWidth=8*(round((w/vScale)/8)); % Hack to make width multiple of 8
  newX=floor((origPos(3)-newCanvasWidth)/2+origPos(1));
  set(handles.canvas,'Position',[newX origPos(2) newCanvasWidth origPos(4)]);
end;
p=get(handles.canvas,'Position');p=round(p);

fprintf('Original video resolution = %d x %d, nFrames = %d\n',w,h,nFrames);
fprintf('Canvas resolution %d x %d\n',p(3),p(4));
handles.data.hCanvas=p(4);
handles.data.wCanvas=p(3);

handles.data.mapToCanvas=makeMapToCanvas([p(4) p(3)],[h w]);

handles.data.labelImg=repmat(uint8(0),[p(4) p(3)]);
handles.data.labelImgHandle=imshow(handles.data.labelImg(:,:,1)); 
handles.data.curImg=im2double(imresize(handles.data.videoReaderH.curFrame,[p(4) p(3)],'nearest'));
%handles.data.curImg=im2double(myresize(handles.data.mapToCanvas,handles.data.videoReaderH.curFrame,[p(4) p(3) 3]));
handles.data.labelImg_video=zeros([h w nFrames],'uint8');
handles.data.curSeg=zeros([p(4) p(3)],'uint8');

handles.data.defSaveLocation=['data/savedLabels/' filename(1:end-4) '-labels.mat'];
handles.data.defSaveLocation_view=['savedResults/' filename(1:end-4) sprintf('-%dx%d-view%s.avi'...
                             ,p(4),p(3),'%s')];
refreshCanvas(handles);
allOk=true;

function [allOk,video]=readVideo_usingReader(vr)
  h=get(vr,'height');
  w=get(vr,'width');
  nCh=3; % This is default for videoReader
  nFrames=get(vr,'numFrames');
  memUsage=h*w*nCh*nFrames*8/(1024*1024);
  memLimit=1024;
  if(memUsage>memLimit ),
    allOk=false;video=[];
    fprintf('Original video is using > %dM of memory, so not loading\n',memLimit);
    return;
  end;
  video=zeros([h w nCh nFrames],'uint8');
  fprintf('Skipping first frame, usually seems to be blank!\n');
  nFramesActual=nFrames-1;
  next(vr);
  for i=1:(nFrames-1)
    if(~next(vr)),
      fprintf('Can only read %d frames of the %d claimed frames\n',...
               i-1,nFrames);
      nFramesActual=i-1;
      break;
    end
    video(:,:,:,i)=getframe(vr);
  end
  video=video(:,:,:,1:nFramesActual);
  fprintf('\n----- Successfully read avi file using videoReader -----\n');
  allOk=true;

function allOk=sanityCheckVideo(inVideo)
  allOk=false;
  [h,w,nCh,nFrames]=size(inVideo);
  if(nCh~=3),
    fprintf('Channels in video ~=3, something wrong\n');
    return;
  end;
  if(~strcmp(class(inVideo),'uint8')),
    fprintf('Expected video to be uint8\n');
    return;
  end;
  memUsage=h*w*nCh*nFrames*8/(1024*1024);
  fprintf('Original video dimensions are: %dx%d, nChannels=%d,length=%d frames\n',w,h,nCh,nFrames);
  fprintf('Memory to store input video in double (in original resolution)=%.2fMB\n\n',memUsage);
  allOk=true;

function outVideo=resizeVideo(newDim,inVideo)
  [h,w,nCh,nFrames]=size(inVideo);
  if(h==newDim(2) & w==newDim(1)),
    outVideo=inVideo;
  else
    outVideo=zeros([newDim(1) newDim(2) nCh nFrames],'uint8');
    for i=1:nFrames
      outVideo(:,:,:,i)=imresize(inVideo(:,:,:,i),newDim);
    end
  end
  outVideo=double(outVideo)/255;
  [h,w,nCh,nFrames]=size(outVideo);
  memUsage=h*w*nCh*nFrames*8/(1024*1024);
  fprintf('Resized video dimensions are: %dx%d, nChannels=%d,length=%d frames\n',w,h,nCh,nFrames);
  fprintf('Memory to store resized video in double: %.2fMB\n\n',memUsage);

% --- Executes on button press in button_startSeg.
function button_startSeg_Callback(hObject, eventdata, handles)
% hObject    handle to button_startSeg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles=stateTransition_function(handles,'segStarted');
guidata(hObject,handles);

% --- Executes on button press in button_saveLabels.
function button_saveLabels_Callback(hObject, eventdata, handles)
% hObject    handle to button_saveLabels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[filename, pathname, filIndex] = uiputfile({'*.png'},'Save labels',handles.data.defSaveLocation);
if(isequal(filename,0)), return; end;
handles.data.defSaveLocation=[pathname filename];
labelImg=handles.data.labelImg;
labelImg(labelImg==3|labelImg==4)=0;
cmap=handles.data.cmap;
cmap(1,:)=0;
%save([pathname filename],'labelImg');
imwrite(labelImg,cmap,[pathname filename]);
guidata(hObject,handles);

% --- Executes on button press in button_loadLabels.
function button_loadLabels_Callback(hObject, eventdata, handles)
% hObject    handle to button_loadLabels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[filename, pathname] = uigetfile({'*.mat';'*.png'},'Select labels to load',handles.data.defSaveLocation);
if(isequal(filename,0)), return; end;

switch(filename(end-2:end))
  case 'mat'
    tmpLoad=load([pathname filename]); % Provides the variable labelImg
    if(~isfield(tmpLoad,'labelImg')),
      msgbox('Invalid .mat file, doesnt contain labels\n','Wassup!','error');
      return;
    end
    labelImg=tmpLoad.labelImg;
  case 'png'
    labelImg=imread([pathname filename]);
  otherwise
    error('Unexpected extensions\n');
end

% TEMPORARY HACK!!!
labelImg(labelImg==1)=6;

if(~all(size(labelImg)==size(handles.data.labelImg)))
  %msgbox('Loaded labels not the same size as loaded video\n','Wassup!','error');
  fprintf('Loaded labels not the same size as loaded video, resizing\n');
  [h,w,nFrames]=size(handles.data.labelImg);
  if(nFrames>1)
    msgbox('Sorry, resizing of annotations works with images right now, videos not supported\n','Duh','error');
    return;
  end
  labelImg=imresize(labelImg,size(handles.data.labelImg),'method','nearest');
end
if(~strcmp(class(labelImg),'uint8'))
  msgbox('Label image should be of type uint8, not loading\n','Wassup!','error');
  return;
end;
handles.data.labelImg=labelImg;
refreshCanvas(handles);
guidata(hObject,handles);

% --- Executes on button press in button_preProcess.
function button_preProcess_Callback(hObject, eventdata, handles)
% hObject    handle to button_preProcess (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles=stateTransition_function(handles,'videoPPed');
guidata(hObject,handles);

% --- Executes when selected object is changed in panel_stroke.
function panel_stroke_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in panel_stroke 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

switch get(hObject,'tag')
  case 'radio_strokeNone'
    handles.data.strokeType=0;
  case 'radio_strokeFG_hard'
    handles.data.strokeType=1;
  case 'radio_strokeBG_hard'
    handles.data.strokeType=2;
  case 'radio_brushAuto'
    handles.data.strokeType=3;
  otherwise
    fprintf('Warning: unknown stroke type selected\n');
end;
guidata(hObject,handles);

% -------- Documenting the numbers in the label image ---
%  label 0:  no user brush
%  label 1: hard fg user brush
%  label 2: hard bg user brush
%  label 3: Segmented by algorithm fg
%  label 4: Segmented by algorithm bg
%  label 5: Auto brush stroke, this will only
%  remain temporarlily while the stroke is under construction
%  should not be part of the label img usually.

% --- Executes when selected object is changed in panel_view.
function panel_view_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in panel_view 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
switch get(hObject,'tag')
  case 'radio_canvasGT'
    handles.data.canvasView='gt';
  case 'radio_boundaryView'
    handles.data.canvasView='drawing_segBoundary';
  case 'radio_viewOrig'
    handles.data.canvasView='orig';
  case 'radio_canvasFG'
    handles.data.canvasView='fg';
  case 'radio_canvasBG'
    handles.data.canvasView='bg';
  case 'radio_viewMisc_1'
    handles.data.canvasView='misc_1';
  case 'radio_viewMisc_2'
    handles.data.canvasView='misc_2';
  case 'radio_viewMisc_3'
    handles.data.canvasView='misc_3';
  case 'radio_viewMisc_4'
    handles.data.canvasView='misc_4';
  case 'radio_viewMisc_5'
    handles.data.canvasView='misc_5';
  case 'radio_viewMisc_6'
    handles.data.canvasView='misc_6';
  case 'radio_viewMisc_7'
    handles.data.canvasView='misc_7';
  case 'radio_viewMisc_8'
    handles.data.canvasView='misc_8';
  case 'radio_viewMisc_9'
    handles.data.canvasView='misc_9';
  case 'radio_viewMisc_10'
    handles.data.canvasView='misc_10';
  otherwise
    fprintf('Warning: unknown view type selected\n');
end;
refreshCanvas(handles);
guidata(hObject,handles);

% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure1_WindowButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(strcmp('started',handles.data.uiState)) return; end;
if(~(strcmp('drawing_segBoundary',handles.data.canvasView)|strcmp('fg',handles.data.canvasView)|strcmp('bg',handles.data.canvasView))) return; end;

pt1= get(handles.canvas,'CurrentPoint'); pt1=pt1(1,1:2);
if(pt1(1) <= 0 || pt1(1)>size(handles.data.labelImg,2) || pt1(2) <= 0 || pt1(2)>size(handles.data.labelImg,1))
 return;
end;

switch(handles.data.brushType)
  case 0 %Box
    % nothing 
    return;
  case 1 %Brush
    handles.data.drag = pt1;
    %if(handles.data.strokeType==3)
      %handles.data.labelImg_snapShot=handles.data.labelImg;
    %end
  case 2 %thin structures
    return;
  otherwise
    return;
end;

guidata(hObject, handles);

% --- Executes on mouse motion over figure - except title and menu.
function figure1_WindowButtonMotionFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
switch(handles.data.brushType)
  case 0 %BBbox
    if(handles.data.boxStarted)
      pt2=get(handles.canvas,'CurrentPoint');pt2=pt2(1,1:2);
      pt1=handles.data.boxStart;
      xMin=min(pt1(1),pt2(1));
      yMin=min(pt1(2),pt2(2));
      xMax=max(pt1(1),pt2(1));
      yMax=max(pt1(2),pt2(2));
      w=max(1,xMax-xMin);
      h=max(1,yMax-yMin);
      set(handles.data.boxHandle,'Position',[xMin yMin w h]);
      guidata(hObject, handles);
    end
  case 1 %Brush
    if isempty(handles.data.drag), return,end;
    pt1 = handles.data.drag;
    pt2 = get(handles.canvas,'CurrentPoint');pt2=pt2(1,1:2);
    handles.data.drag = pt2;
    handles.data = drawBrush(handles.data,pt1,pt2);
    refreshCanvas(handles);
    guidata(hObject, handles);
  case 2 %Thin structures
    % NOT SUPPORTED RIGHT NOW
    return;
    if(~isempty(handles.data.thin_firstClick))
      pt2 = get(handles.canvas,'CurrentPoint');pt2=pt2(1,1:2);
      handles.data = drawThin(handles.data,round(pt2));
      refreshCanvas(handles);
      guidata(hObject, handles);
    end
  otherwise
    return;
end

function data = drawThin(data,curPoint)
labelImg=data.labelImg_snapShot;
[h w]=size(labelImg);
if(curPoint(2)<=0 | curPoint(2)>h | curPoint(1) <= 0 | curPoint(1)>w)
  return;
end
strokeLabel=data.thin_label;
origin=data.thin_firstClick;
curPoint=sub2ind(size(labelImg),curPoint(2),curPoint(1));
endPoint=sub2ind(size(labelImg),origin(2),origin(1));
while(curPoint~=endPoint)
  labelImg(curPoint)=strokeLabel;
  curPoint=data.qFG_thin(curPoint);
end
data.labelImg=labelImg;

function data=drawThin_endPoint(data,pt1)
pt2=pt1;
diff = pt2-pt1;
[H W nFrames]=size(data.labelImg);
r = ceil(sqrt(sum(diff.^2))+1E-10);
col=uint8(data.thin_label);
for i=0:r
    pt = round(pt1+diff*(i/r));
    xLeftOffset=max(1-(pt(1)-data.brushRad),0);
    xRightOffset=min(0,W-(pt(1)+data.brushRad));
    yLeftOffset=max(1-(pt(2)-data.brushRad),0);
    yRightOffset=min(0,H-(pt(2)+data.brushRad));
    xrng = pt(1)-data.brushRad+xLeftOffset:pt(1)+data.brushRad+xRightOffset;
    yrng = pt(2)-data.brushRad+yLeftOffset:pt(2)+data.brushRad+yRightOffset;
    brmask = data.brushMask(1+yLeftOffset:2*data.brushRad+1+yRightOffset,1+xLeftOffset:2*data.brushRad+1+xRightOffset);
    data.labelImg(yrng,xrng,data.frameNum) = data.labelImg(yrng,xrng,data.frameNum).*(1-brmask) + brmask*col;
end

function data = drawBrush(data,pt1,pt2)
diff = pt2-pt1;
[H W nFrames]=size(data.labelImg);
r = ceil(sqrt(sum(diff.^2))+1E-10);
col=uint8(data.strokeType);
for i=0:r
    pt = round(pt1+diff*(i/r));
    xLeftOffset=max(1-(pt(1)-data.brushRad),0);
    xRightOffset=min(0,W-(pt(1)+data.brushRad));
    yLeftOffset=max(1-(pt(2)-data.brushRad),0);
    yRightOffset=min(0,H-(pt(2)+data.brushRad));
    xrng = pt(1)-data.brushRad+xLeftOffset:pt(1)+data.brushRad+xRightOffset;
    yrng = pt(2)-data.brushRad+yLeftOffset:pt(2)+data.brushRad+yRightOffset;
    brmask = data.brushMask(1+yLeftOffset:2*data.brushRad+1+yRightOffset,1+xLeftOffset:2*data.brushRad+1+xRightOffset);
    data.labelImg(yrng,xrng) = data.labelImg(yrng,xrng).*(1-brmask) + brmask*col;
end

% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure1_WindowButtonUpFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(strcmp('started',handles.data.uiState)) return; end;
if(~(strcmp('drawing_segBoundary',handles.data.canvasView)|strcmp('fg',handles.data.canvasView)|strcmp('bg',handles.data.canvasView))) return; end;
pt1= get(handles.canvas,'CurrentPoint'); pt1=pt1(1,1:2);
if(pt1(1) <= 0 || pt1(1)>size(handles.data.labelImg,2) || pt1(2) <= 0 || pt1(2)>size(handles.data.labelImg,1))
  switch(handles.data.brushType)
    case 1
      if(~isempty(handles.data.drag))
        handles.data.drag = [];
        if(handles.data.strokeType==3)
          handles.data=drawAutoBrush(handles.data);
          handles.data=remapLabelImg(handles.data);
          refreshCanvas(handles);
        end
        guidata(hObject,handles);
      end
  end
return;
end

figure1_WindowButtonMotionFcn(hObject,eventdata,handles);
handles=guidata(hObject);
switch(handles.data.brushType)
  case 0 %BBox
    if(~handles.data.boxStarted)
       handles.data.boxStart=pt1;
       handles.data.boxStarted=true;
       handles.data.boxHandle=rectangle('Position',[pt1 1 1],'LineStyle','--');
    else
       handles.data.boxEnd=pt1;
       handles.data = drawBox(handles.data); 
       refreshCanvas(handles);
       handles.data.boxStarted=false;
       delete(handles.data.boxHandle);
    end;
    handles.data=remapLabelImg(handles.data);
    refreshCanvas(handles);
  case 1
    handles.data.drag = [];
    if(handles.data.strokeType==3)
      handles.data=drawAutoBrush(handles.data);
    end
    handles.data=remapLabelImg(handles.data);
    refreshCanvas(handles);
  case 2
    % NOT SUPPORTED RIGHT NOW
    error('Thin struct not supported yet\n');
    if(isempty(handles.data.thin_firstClick))
      handles.data.thin_firstClick=round(pt1); 
      handles.data.thin_label=getThinStroke_label(handles.data);
      handles.data=drawThin_endPoint(handles.data,handles.data.thin_firstClick);
      handles.data.qFG_thin=getThinStructures(handles.data);
      handles.data.labelImg_snapShot=handles.data.labelImg;
    else
      handles.data.thin_secondClick=round(pt1);
      handles.data=drawThin_endPoint(handles.data,handles.data.thin_secondClick);
      handles.data.thin_firstClick=[];
      handles.data.thin_secondClick=[];
      handles.data.labelImg_snapShot=[];
    end
end;

if(get(handles.chkbox_autoEdit,'Value')==1 & handles.data.brushType~=2),
  if(strcmp('segStarted',handles.data.uiState)),
    fprintf('Real time edit enabled: Calling update seg\n');
    button_updateSeg_Callback(hObject,[],handles);
    handles=guidata(hObject);
  end;
end

if(get(handles.chkbox_autoEdit,'Value')==1 & handles.data.brushType==2),
  if(isempty(handles.data.thin_firstClick) & isempty(handles.data.thin_secondClick)),
    if(strcmp('segStarted',handles.data.uiState)),
      fprintf('Real time edit enabled: Calling update seg\n');
      button_updateSeg_Callback_Callback(hObject,[],handles);
      handles=guidata(hObject);
    end;
  end
end

guidata(hObject,handles);

function data=remapLabelImg(data)
labelImg=imresize(data.labelImg,[size(data.labelImg_video,1) size(data.labelImg_video,2)],...
         'nearest');
data.labelImg_video(:,:,data.frameNum)=labelImg;
data.labelImg=imresize(labelImg,[data.hCanvas data.wCanvas],'nearest');
%data.labelImg=myresize(data.mapToCanvas,labelImg,[data.hCanvas data.wCanvas]);

function data=drawBox(data)
  xMin=round(min(data.boxStart(1),data.boxEnd(1)));
  yMin=round(min(data.boxStart(2),data.boxEnd(2)));
  xMax=round(max(data.boxStart(1),data.boxEnd(1)));
  yMax=round(max(data.boxStart(2),data.boxEnd(2)));
  
  [h w]=size(data.labelImg);
  data.labelImg(1:yMin,:)=uint8(data.strokeType);
  data.labelImg(yMin:yMax,1:xMin)=uint8(data.strokeType);
  data.labelImg(yMin:yMax,xMax:w)=uint8(data.strokeType);
  data.labelImg(yMax:h,:)=uint8(data.strokeType);

function strokeType=getThinStroke_label(data)
if(data.strokeType==3)
  clickIndex=data.thin_firstClick;
  clickLabel=data.labelImg(clickIndex(2),clickIndex(1));
  switch(clickLabel)
    case {1,3}
      strokeType=2;
    case {2,4}
      strokeType=6;
    otherwise
      strokeType=0; %empty stroke
  end
else
  strokeType=data.strokeType;
end

function data=drawAutoBrush(data)
autoBrushMask=(data.labelImg==3);
curSeg=data.curSeg;
autoBrushMask=imresize(autoBrushMask,size(curSeg),'nearest');
numLabels=zeros(2,1);
% numLabels(1) is the number of fg pixels in curSeg under autoBrush
% numLabels(2) is the number of bg pixels in curSeg under autoBrush
autoBrushLabels=curSeg(autoBrushMask);
numLabels(1)=nnz(autoBrushLabels==255);
numLabels(2)=nnz(autoBrushLabels==0);

[xx,maxInd]=max(numLabels);

switch(maxInd)
  case 1
    data.labelImg(autoBrushMask)=2;
  case 2
    data.labelImg(autoBrushMask)=1;
end

% --- Executes on slider movement.
function slider_brushSize_Callback(hObject, eventdata, handles)
% hObject    handle to slider_brushSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

handles.data.brushRad=round(get(hObject,'Value'));
handles.data.brushMask=makeBrush(handles.data.brushRad);
set(handles.text_brushSize,'String',sprintf('Brush size: %d',handles.data.brushRad));
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function slider_brushSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_brushSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_canvasSize_Callback(hObject, eventdata, handles)
% hObject    handle to slider_canvasSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
scale=get(hObject,'Value');
baseLine=handles.data.baseLineCanvasPosition;
centerX=baseLine(1)+baseLine(3)/2;
centerY=baseLine(2)+baseLine(4)/2;
newW=8*(round(baseLine(3)*scale/8));
newH=baseLine(4)*scale;

handles.data.origCanvasPosition=[centerX-newW/2 centerY-newH/2 newW newH];
fprintf('Canvas size =%dx%d\n',...
        round(newW),round(newH));
set(handles.canvas,'Position',handles.data.origCanvasPosition);
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function slider_canvasSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_canvasSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on key press with focus on figure1 and no controls selected.
function figure1_MyKeyPressFcn(h, evnt)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles=guidata(h);
%fprintf('Character: %c\nModifier: %s\nKey: %s\n',evnt.Character,'No modifier key',evnt.Key);
switch evnt.Key
  case 'uparrow'
    if(strcmp(get(handles.slider_canvasSize,'enable'),'off')),
      return;
    end
    sliderMax=get(handles.slider_canvasSize,'max');
    sliderValue=min(sliderMax,get(handles.slider_canvasSize,'value')+0.1);
    set(handles.slider_canvasSize,'value',sliderValue);
    slider_canvasSize_Callback(handles.slider_canvasSize, [], handles)
  case 'downarrow'
    if(strcmp(get(handles.slider_canvasSize,'enable'),'off')),
      return;
    end
    sliderMin=get(handles.slider_canvasSize,'min');
    sliderValue=max(sliderMin,get(handles.slider_canvasSize,'value')-0.1);
    set(handles.slider_canvasSize,'value',sliderValue);
    slider_canvasSize_Callback(handles.slider_canvasSize, [], handles)
end;

switch evnt.Key
  case 'rightarrow'
    sliderMax=get(handles.slider_frameNum,'max');
    %sliderMin=get(handles.slider_frameNum,'min');
    %sliderStep=(sliderMax-sliderMin)*get(handles.slider_frameNum,'sliderstep');
    sliderValue=min(sliderMax,get(handles.slider_frameNum,'value') + 1);
    set(handles.slider_frameNum,'value',sliderValue);
    slider_frameNum_Callback(handles.slider_frameNum, [], handles)
  case 'leftarrow'
    %sliderMax=get(handles.slider_frameNum,'max');
    sliderMin=get(handles.slider_frameNum,'min');
    %sliderStep=(sliderMax-sliderMin)*get(handles.slider_frameNum,'sliderstep');
    sliderValue=max(sliderMin,get(handles.slider_frameNum,'value') - 1);
    set(handles.slider_frameNum,'value',sliderValue);
    slider_frameNum_Callback(handles.slider_frameNum, [], handles)
end;

if(strcmp(get(handles.panel_stroke,'visible'),'on'))
  switch(evnt.Key)
    case '1'
      set(handles.panel_stroke,'SelectedObject',handles.radio_strokeFG_hard);
      panel_stroke_SelectionChangeFcn(handles.radio_strokeFG_hard,[],handles);
      handles=guidata(h);
    case '2'
      set(handles.panel_stroke,'SelectedObject',handles.radio_strokeBG_hard);
      panel_stroke_SelectionChangeFcn(handles.radio_strokeBG_hard,[],handles);
      handles=guidata(h);
  end
end

if(strcmp(get(handles.panel_view,'visible'),'on'))
  switch(evnt.Key)
    case 'd'
      set(handles.panel_view,'SelectedObject',handles.radio_canvasGT);
      panel_view_SelectionChangeFcn(handles.radio_canvasGT,[],handles);
      handles=guidata(h);
    case 's'
      set(handles.panel_view,'SelectedObject',handles.radio_boundaryView);
      panel_view_SelectionChangeFcn(handles.radio_boundaryView,[],handles);
      handles=guidata(h);
    case 'f'
      set(handles.panel_view,'SelectedObject',handles.radio_canvasFG);
      panel_view_SelectionChangeFcn(handles.radio_canvasFG,[],handles);
      handles=guidata(h);
    case 'b'
      set(handles.panel_view,'SelectedObject',handles.radio_canvasBG);
      panel_view_SelectionChangeFcn(handles.radio_canvasBG,[],handles);
      handles=guidata(h);
    case 'c'
      set(handles.panel_view,'SelectedObject',handles.radio_viewMisc_1);
      panel_view_SelectionChangeFcn(handles.radio_viewMisc_1,[],handles);
      handles=guidata(h);
  end
end
% --- Executes on button press in button_reset.
function button_reset_Callback(hObject, eventdata, handles)
% hObject    handle to button_reset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles=stateTransition_function(handles,'started');
guidata(hObject,handles);

% --- Executes on button press in button_clearFrame.
function button_clearFrame_Callback(hObject, eventdata, handles)
% hObject    handle to button_clearFrame (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[h,w,nFrames]=size(handles.data.labelImg);
handles.data.labelImg(:,:,handles.data.frameNum)=zeros(h,w,'uint8');
refreshCanvas(handles);
guidata(hObject,handles);

% --- Executes on slider movement.
function slider_labelsOpacity_Callback(hObject, eventdata, handles)
% hObject    handle to slider_labelsOpacity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

numLabels=length(handles.data.amap);
oldAmap=handles.data.amap;
alpha=get(hObject,'Value');
amap=alpha*ones(numLabels,1);
amap(1)=0;
amap(2:3)=oldAmap(2:3); % Hacky way to not change the transparency level of user strokes
amap(6)=oldAmap(6); % Hacky way to not change the transparency level of user strokes
handles.data.amap=amap;
refreshCanvas(handles);
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function slider_labelsOpacity_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_labelsOpacity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in button_updateSeg.
function button_updateSeg_Callback(hObject, eventdata, handles)
% hObject    handle to button_updateSeg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%diffLabelImg=checkAnd_diffLabels(handles.data);
handles.data.segmenterH.updateSeg(handles.data.labelImg_video,handles.data.frameNum);
handles.data.curSeg=imresize(handles.data.segmenterH.curFrame_seg(), ...
                   [handles.data.hCanvas handles.data.wCanvas],'nearest');
%handles.data.curSeg=myresize(handles.data.mapToCanvas,handles.data.segmenterH.curFrame_seg(), ...
                   %[handles.data.hCanvas handles.data.wCanvas],'nearest');
curSeg=handles.data.curSeg;

%handles.data.labelImg_lastSegmentation=handles.data.labelImg;
[handles.data.segBoundaryMask,handles.data.segBoundaryColors]=...
  miscFns.getSegBoundary_twoColors(curSeg,handles.data.segBoundaryColor_outside, ...
                          handles.data.segBoundaryColor_inside...
                          ,handles.data.segBoundaryWidth,handles.data.segBoundaryWidth);
refreshCanvas(handles);
guidata(hObject,handles);

% --- Executes on button press in button_saveViewAvi.
function button_saveViewAvi_Callback(hObject, eventdata, handles)
% hObject    handle to button_saveViewAvi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[h,w,nCh,nFr]=size(handles.data.inVideo);
if(nFr>1)
  fprintf('Video writing doesnt work in Windows at the moment\n');
  return;
else
  [filename, pathname, filIndex] = uiputfile({'*.png'},'Save view',...
              sprintf(handles.data.defSaveLocation_view,handles.data.canvasView));
  if(isequal(filename,0)), return; end;
  fullName=[pathname filename];
  
  outVideo=generateView_video(handles);
  imwrite(outVideo,fullName);
  fprintf('Image saved\n');
end

function video=generateView_video(handles)
  curFrame=handles.data.frameNum;
  video=zeros(size(handles.data.inVideo));

  for i=1:size(handles.data.inVideo,4)
    handles.data.frameNum=i;
    refreshCanvas(handles);
    video(:,:,:,i)=get(handles.data.labelImgHandle,'Cdata');
  end
  handles.data.frameNum=curFrame;
  refreshCanvas(handles);

% --- Executes on button press in button_diagnose.
function button_diagnose_Callback(hObject, eventdata, handles)
% hObject    handle to button_diagnose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

varList={};
tryList={'labelImg','segmenterH','frameNum'};
nameList={'labelImg','segH','frameNum'};

for i=1:length(tryList)
  try
    eval(['handles.data.' tryList{i} ';']);
    cmd=[nameList{i} '=handles.data.' tryList{i} ';'];
    eval(cmd);
    varList{end+1}=nameList{i};
  catch
    continue;
  end
end

fprintf('\n------- Diagnosing -------\nVariables for you to analyse:\n');
for i=1:length(varList)
  fprintf([varList{i} '\n']);
end
keyboard;


% --- Executes on button press in button_reloadVideo.
function button_reloadVideo_Callback(hObject, eventdata, handles)
% hObject    handle to button_reloadVideo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[handles,allOk]=stateTransition_function(handles,'video_reload');
if(allOk)
  [handles,allOk]=stateTransition_function(handles,'videoLoaded');
end
guidata(hObject,handles);


% --- Executes on slider movement.
function slider_gamma_Callback(hObject, eventdata, handles)
% hObject    handle to slider_gamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

handles.data.gamma=get(hObject,'Value');
set(handles.text_gamma,'string',sprintf('Gamma: %.2f',handles.data.gamma));
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function slider_gamma_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_gamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in button_popOut.
function button_popOut_Callback(hObject, eventdata, handles)
% hObject    handle to button_popOut (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

frame=get(handles.data.labelImgHandle,'Cdata');
figure;imshow(frame);
title(handles.data.canvasView);

% --- Executes on button press in chkbox_autoEdit.
function chkbox_autoEdit_Callback(hObject, eventdata, handles)
% hObject    handle to chkbox_autoEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkbox_autoEdit


% --- Executes on button press in radio_brushBrush.
function radio_brushBrush_Callback(hObject, eventdata, handles)
% hObject    handle to radio_brushBrush (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radio_brushBrush


% --- Executes when selected object is changed in panel_strokeType.
function panel_strokeType_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in panel_strokeType 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)

switch get(hObject,'tag')
  case 'radio_brushBrush'
    handles.data.brushType=1;
  case 'radio_brushBox'
    handles.data.brushType=0;
  case 'radio_brushThin'
    handles.data.brushType=2;
    handles.data.thin_firstClick=[];
    handles.data.thin_secondClick=[];
  otherwise
    fprintf('Warning: unknown stroke type selected\n');
end;
guidata(hObject,handles);


% --- Executes on button press in chkbox_origResolution.
function chkbox_origResolution_Callback(hObject, eventdata, handles)
% hObject    handle to chkbox_origResolution (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkbox_origResolution

fprintf('\nChange in origResolution flag will only take effect on loading a new video\n');
%handles.data.origResolution=get(handles.chkbox_origResolution,'Value');
%guidata(hObject,handles);


% --- Executes on scroll wheel click while the figure is in focus.
function figure1_WindowScrollWheelFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	VerticalScrollCount: signed integer indicating direction and number of clicks
%	VerticalScrollAmount: number of lines scrolled for each click
% handles    structure with handles and user data (see GUIDATA)

if(strcmp(handles.data.segMethod,'scrollCut')|strcmp(handles.data.segMethod,'scrollCut_seq')...
  |strcmp(handles.data.segMethod,'touchCut')|strcmp(handles.data.segMethod,'touchCut_seq'))
  pt1= get(handles.canvas,'CurrentPoint'); pt1=round(pt1(1,1:2));
  if(pt1(1) <= 0 || pt1(1)>size(handles.data.labelImg,2) || pt1(2) <= 0 || pt1(2)>size(handles.data.labelImg,1))
  return;
  end;
  
  scrollCount=eventdata.VerticalScrollCount;
  delta=handles.data.scroll_step*(-scrollCount);
  handles.data.scroll_value=handles.data.scroll_value+delta;
  fprintf('Balooning force is now = %.5f\n',handles.data.scroll_value);

  if(strcmp('videoSegmented',handles.data.uiState)),
    cmd=['handles.data.opts.segOpts.lambda=handles.data.scroll_value;'];
    eval(cmd);
    if(get(handles.chkbox_autoEdit,'Value')==1),
      fprintf('Real time edit enabled: Calling quick edit\n');
      button_quickEdit_Callback(hObject,[],handles);
      handles=guidata(hObject);
    end;
  end
  
  guidata(hObject,handles);
  
end


% --- Executes on selection change in popup_starShape.
function popup_starShape_Callback(hObject, eventdata, handles)
% hObject    handle to popup_starShape (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popup_starShape contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_starShape

starMethodStrings=get(handles.popup_starShape,'String');
handles.data.starMethod=starMethodStrings{get(handles.popup_starShape,'Value')};
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function popup_starShape_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popup_starShape (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function textEdit_geoGamma_Callback(hObject, eventdata, handles)
% hObject    handle to textEdit_geoGamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of textEdit_geoGamma as text
%        str2double(get(hObject,'String')) returns contents of textEdit_geoGamma as a double
str=get(hObject,'string');
handles.data.geoGamma=str2num(str);
if(isempty(handles.data.geoGamma)),
  handles.data.geoGamma=1;
end
set(handles.text_geoGamma,'string',sprintf('Geo Gamma: %.5f',handles.data.geoGamma));
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function textEdit_geoGamma_CreateFcn(hObject, eventdata, handles)
% hObject    handle to textEdit_geoGamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on key press with focus on textEdit_geoGamma and none of its controls.
function textEdit_geoGamma_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to textEdit_geoGamma (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)



function textEdit_scrollValue_Callback(hObject, eventdata, handles)
% hObject    handle to textEdit_scrollValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of textEdit_scrollValue as text
%        str2double(get(hObject,'String')) returns contents of textEdit_scrollValue as a double

str=get(hObject,'string');
handles.data.scroll_step=str2num(str);
if(isempty(handles.data.scroll_step)),
  handles.data.scroll_step=0.01;
end
set(handles.text_scrollValue,'string',sprintf('ScrollSize: %.5f',handles.data.scroll_step));
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function textEdit_scrollValue_CreateFcn(hObject, eventdata, handles)
% hObject    handle to textEdit_scrollValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider_frameNum_Callback(hObject, eventdata, handles)
% hObject    handle to slider_frameNum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

if(strcmp(handles.data.uiState,'started')), return; end;

nxtFrame=round(get(hObject,'Value'));
diff=nxtFrame-handles.data.frameNum;

switch(diff)
  case 1
    [handles,moveOk]=forward_oneFrame(handles);
  case -1
    [handles,moveOk]=backward_oneFrame(handles);
  case 0
    moveOk=true;
    % do nothing
  otherwise
    [handles,moveOk]=jumpToFrame(handles,nxtFrame);
end

set(hObject,'Value',handles.data.frameNum);
if(moveOk)
  set(handles.text_frameNumber,'String',sprintf('%s: Frame number: %d',handles.data.inFileName,handles.data.frameNum));
  guidata(hObject,handles);
end

function [handles,moveOk]=jumpToFrame(handles,nxtFrame)
  fprintf('Jump to frame not yet implemented\n');
  moveOk=false;
  keyboard;

function [handles,moveOk]=backward_oneFrame(handles)
  moveOk=true;
  data=handles.data;
  if(strcmp(data.uiState,'segStarted')),
    moveOk=data.segmenterH.moveBackward(data.labelImg_video);
    if(~moveOk), return; end;
    curSeg=data.segmenterH.curFrame_seg();
    curSeg=imresize(curSeg,[data.hCanvas data.wCanvas],'nearest');
    %curSeg=myresize(handles.data.mapToCanvas,curSeg,[data.hCanvas data.wCanvas]);
    data.curSeg=curSeg;
    [data.segBoundaryMask,data.segBoundaryColors]= ...
    miscFns.getSegBoundary_twoColors(curSeg,data.segBoundaryColor_outside, ...
                           data.segBoundaryColor_inside...
                          ,data.segBoundaryWidth,data.segBoundaryWidth);
  end
  data.frameNum=data.frameNum-1;
  data.videoReaderH.moveBackward();
  data.curImg=im2double(...
              imresize(data.videoReaderH.curFrame,[data.hCanvas data.wCanvas],'nearest'));
  %data.curImg=im2double(...
              %myresize(handles.data.mapToCanvas,data.videoReaderH.curFrame,[data.hCanvas data.wCanvas 3]));
  data.labelImg=imresize(data.labelImg_video(:,:,data.frameNum),[data.hCanvas data.wCanvas]...
                         ,'nearest');
  %data.labelImg=myresize(handles.data.mapToCanvas,data.labelImg_video(:,:,data.frameNum),[data.hCanvas data.wCanvas]);

  handles.data=data;
  refreshCanvas(handles);

function [handles,moveOk]=forward_oneFrame(handles)
  moveOk=true;
  data=handles.data;
  if(strcmp(data.uiState,'segStarted')),
    moveOk=data.segmenterH.moveForward(data.labelImg_video);
    if(~moveOk), return; end;
    curSeg=data.segmenterH.curFrame_seg();
    curSeg=imresize(curSeg,[data.hCanvas data.wCanvas],'nearest');
    %curSeg=myresize(handles.data.mapToCanvas,curSeg,[data.hCanvas data.wCanvas]);
    data.curSeg=curSeg;
    [data.segBoundaryMask,data.segBoundaryColors]= ...
    miscFns.getSegBoundary_twoColors(curSeg,data.segBoundaryColor_outside, ...
                           data.segBoundaryColor_inside...
                          ,data.segBoundaryWidth,data.segBoundaryWidth);
  end
  data.frameNum=data.frameNum+1;
  %stTime=clock;
  data.videoReaderH.moveForward();
  %fprintf('Took %.3f secs to decode video\n',etime(clock,stTime));
  %stTime=clock;
  data.curImg=im2double(...
              imresize(data.videoReaderH.curFrame,[data.hCanvas data.wCanvas],'nearest'));
  data.labelImg=imresize(data.labelImg_video(:,:,data.frameNum),[data.hCanvas data.wCanvas]...
                         ,'nearest');
  %data.curImg=im2double(...
              %myresize(handles.data.mapToCanvas,data.videoReaderH.curFrame,[data.hCanvas data.wCanvas 3]));
  %data.labelImg=myresize(handles.data.mapToCanvas,data.labelImg_video(:,:,data.frameNum),[data.hCanvas data.wCanvas]);
  %fprintf('Took %.3f secs to imresize!\n',etime(clock,stTime));

  %stTime=clock;
  handles.data=data;
  refreshCanvas(handles);
  %fprintf('Took %.3f secs to refreshCanvas!\n',etime(clock,stTime));

% --- Executes during object creation, after setting all properties.
function slider_frameNum_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_frameNum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes on selection change in popup_videoMethod.
function popup_videoMethod_Callback(hObject, eventdata, handles)
% hObject    handle to popup_videoMethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popup_videoMethod contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_videoMethod

vidMethodStrings=get(handles.popup_videoMethod,'String');
handles.data.segMethod=vidMethodStrings{get(handles.popup_videoMethod,'Value')};
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function popup_videoMethod_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popup_videoMethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
fprintf('Cleaning up ui\n');
handles=cleanUp(handles);

delete(hObject);

