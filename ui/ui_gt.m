function varargout = ui(varargin)
% UI M-file for ui.fig
%      UI, by itself, creates a new UI or raises the existing
%      singleton*.
%
%      H = UI returns the handle to a new UI or the handle to
%      the existing singleton*.
%
%      UI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in UI.M with the given input arguments.
%
%      UI('Property','Value',...) creates a new UI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ui_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ui

% Last Modified by GUIDE v2.5 23-May-2010 22:23:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ui_OpeningFcn, ...
                   'gui_OutputFcn',  @ui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ui is made visible.
function ui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ui (see VARARGIN)

% --- Initializating the state machine of the UI -----

fprintf('Usage:\n');
fprintf('Brush size change: Up/down arrows\n');
fprintf('Next frame/previous frame: Left/right arrows\n');
fprintf('Undo (1 level only): u\n');
fprintf('Clear all annotation: c\n');
fprintf('Toggle annotation overlay: a (useful for finding leaks in boundary)\n');
fprintf('Draw boundary brush: b\n');
fprintf('Fill boundary: f (and then click any point inside closed boundary)\n');
fprintf('Brush label unknown: 0\n');
fprintf('Brush label fg: 1\n');
fprintf('Brush label bg: 2\n');


handles.data=[];

set(handles.canvas,'Units','pixels');
handles.data=ui_gtData();
handles.data.baseLineCanvasPosition=get(handles.canvas,'Position');
handles.data.uiState='opened';
handles=stateTransition_function(handles,'started');
% Choose default command line output for ui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ui wait for user response (see UIRESUME)
% uiwait(handles.figure1);

function brush=makeBrush(sz)
  [x,y] = meshgrid(-sz:sz,-sz:sz);
  r = x.^2 + y.^2;
  brush = r<=(sz*sz);
  brush=uint8(brush);

% --- Outputs from this function are returned to the command line.
function varargout = ui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function refreshCanvas(handles)
switch(handles.data.canvasView)
  case 'gtOverlay'
    refreshOverlay(handles.data);
  otherwise
    error('Unknown view state %s\n',handles.data.canvasView);
end

function refreshOverlay(data)
  amap=data.amap;
  labelImg=data.labelImg;
  backImg=data.curImg;
  alphaImage=amap(labelImg+1);
  cmap=data.cmap;
  cmapR=cmap(:,1);
  cmapG=cmap(:,2);
  cmapB=cmap(:,3);
  labelLayer(:,:,1)=cmapR(labelImg+1);
  labelLayer(:,:,2)=cmapG(labelImg+1);
  labelLayer(:,:,3)=cmapB(labelImg+1);

  alphaImage=repmat(alphaImage,[1 1 3]);
  cdata=labelLayer.*alphaImage+backImg.*(1-alphaImage);
  set(data.labelImgHandle,'CData',cdata);
  %drawnow expose;

function [handles,allOk]=stateTransition_function(handles,nxtState)
% Function to implement the state machine for the user interface
% See your notebook #3 for the state machine diagram, i dont
% have a image of it (If only i had a tablet)

curState=handles.data.uiState;
switch(nxtState)
  case 'started'
    %error('You cant go request to go back to started state\n');
    [handles,allOk]=resetUI(handles);
  case 'gtStarted'
    % You can only come to this state from videoPPed or itself, any other state
    % raise error
    if(~strcmp(curState,'started'))
      handles=cleanUp(handles);
      %error('Going to state gtStarted from %s state not allowed, bug\n',curState);
    end
    [handles,allOk]=startGT(handles);
  otherwise
    error('Invalid state %s requested, bug\n',nxtState);
end

if(allOk)
  handles.data.uiState=nxtState;
else
  fprintf('UI could not make state transition to %s\n',nxtState);
end

function [handles,allOk]=resetUI(handles)

set(handles.canvas,'Units','pixels');
set(handles.panel_stroke,'SelectedObject',handles.radio_strokeUnknown);

set(handles.button_startGT,'enable','on');
set(handles.figure1,'pointer','cross');
set(handles.figure1,'KeyPressFcn',@figure1_MyKeyPressFcn);

data=handles.data;
data.debugLevel=1;
data.drag=[];
data.strokeType=0; % 0 is for unknown
data.canvasView='gtOverlay';
data.uiState='started';
data.brushRad=1;
data.brushMask=makeBrush(data.brushRad);
data.brushType=1;
set(handles.panel_brushType,'SelectedObject',handles.radio_brushBrush);
data.videoReaderH=[];

cwd=miscFns.extractDirPath(mfilename('fullpath'));
data.gtDir_out=[cwd '../data/manualGT/'];
data.cmd_dataset='testBench.getTests_tmp()';
data.skipSize=4;

data.origCanvasPosition=data.baseLineCanvasPosition;
set(handles.canvas,'Position',data.baseLineCanvasPosition);
cla(handles.canvas,'reset');

set(handles.canvas,'xlimmode','manual','ylimmode','manual','zlimmode','manual');
set(handles.canvas,'XTick',[],'YTick',[]);
set(handles.slider_brushSize,'min',0);
set(handles.slider_brushSize,'max',10);
set(handles.slider_brushSize,'sliderstep',[1/9 1/9]);
set(handles.slider_brushSize,'value',data.brushRad);
set(handles.text_brushSize,'string',sprintf('Brush size: %d',data.brushRad));
set(handles.text_status,'string',sprintf('Video Num: Frame num: '));

handles.data=data;
allOk=true;

function handles=cleanUp(handles)
try
  if(~isempty(handles.data.videoReaderH)),
    delete(handles.data.videoReaderH);
  end
  if(~isempty(handles.data.videoReaderH_copy)),
    delete(handles.data.videoReaderH_copy);
  end
  if(~isempty(handles.data.segmenterH)),
    delete(handles.data.segmenterH);
  end
  handles.data.videoReaderH=[];
  handles.data.videoReaderH_copy=[];
  handles.data.segmenterH=[];
catch
  fprintf('No cleaning up to do\n');
end

function [handles,allOk]=startGT(handles)
  allOk=false;
  [videoPaths,videoFiles]=eval(handles.data.cmd_dataset);
  videoNames=miscFns.extractVideoNames(videoPaths,videoFiles);
  handles.data.videoNames=videoNames;
  handles.data.videoPaths=videoPaths;
  handles.data.videoFiles=videoFiles;
  handles.data.videoNum=1;
  handles=loadVideo(handles);
  handles=setupFrame(handles);
  %set(handles.button_startGT,'enable','off');
  allOk=true;

function handles=setupFrame(handles)

data=handles.data;
vidName=data.videoNames{data.videoNum};
gtDir=[data.gtDir_out vidName '/'];
if(~exist(gtDir)), mkdir(gtDir); end;
gtFile=[gtDir sprintf('%05d.png',data.frameNum)];
if(~exist(gtFile,'file')), curSeg=zeros(data.hImg,data.wImg,'uint8');
else, curSeg=imread(gtFile);
end

numVideos=length(data.videoNames);
numFrames=data.videoReaderH.nFrames;
statusString=sprintf('Video num: %d/%d, Frame num: %d/%d\n',data.videoNum,...
                     numVideos,data.frameNum,numFrames);
set(handles.text_status,'string',statusString);

handles.data.strokeType=0;
set(handles.panel_stroke,'SelectedObject',handles.radio_strokeUnknown);
handles.data.brushType=1;
set(handles.panel_brushType,'SelectedObject',handles.radio_brushBrush);
handles.data.brushRad=1;
set(handles.slider_brushSize,'value',handles.data.brushRad);
set(handles.text_brushSize,'String',sprintf('Brush size: %d',handles.data.brushRad));
handles.data.labelImg=segToLabel(curSeg);
handles.data.labelImg_prev=handles.data.labelImg;
handles.data.curImg=im2double(data.videoReaderH.curFrame);
handles.data.labelImgHandle=image(handles.data.curImg); 
set(handles.canvas,'xlimmode','manual','ylimmode','manual','zlimmode','manual');
set(handles.canvas,'XTick',[],'YTick',[]);
handles.data.curSeg=curSeg;
handles.data.gtFile=gtFile;
refreshCanvas(handles);

function labelImg=segToLabel(seg)
[h w]=size(seg);
labelImg=zeros([h w],'uint8');
labelImg(seg==0)=2;
labelImg(seg==128)=0;
labelImg(seg==255)=1;

function [handles,allOk]=loadVideo(handles)

data=handles.data;
pathname=data.videoPaths{data.videoNum};
filename=data.videoFiles{data.videoNum};
[handles.data.videoReaderH]=myVideoReader(pathname,filename);

allOk=handles.data.videoReaderH.loadOk;

if(~allOk),
  delete(handles.data.videoReaderH);
  handles.data.videoReaderH=[];
  error('Could not read video properly\n');
  return;
end

[h,w,nCh,nFrames]=handles.data.videoReaderH.videoSize();
handles.data.hImg=h;
handles.data.wImg=w;

handles.data.frameNum=1; % Frame number is 1 indexed

axes(handles.canvas);
set(gcf,'DoubleBuffer','on');

numLabels=length(get(handles.panel_stroke,'Children')); 

%alpha=get(handles.slider_labelsOpacity,'Value');
alpha=0.5;
amap=alpha*ones(numLabels,1);
amap(3)=0;
handles.data.amap=amap;
handles.data.amap_other=ones(numLabels,1);

% -- Set up the color map ---
cmap=colorcube(10*numLabels);
cmap=cmap([1:10:10*numLabels],:);

% Make the colors bright:
cmap=brighten(cmap,0.7);

handles.data.cmap=cmap;
origPos=handles.data.origCanvasPosition;

hScale=w/origPos(3);
vScale=h/origPos(4);
if(hScale>vScale), 
  newCanvasHeight=floor(h/hScale);
  newY=floor((origPos(4)-newCanvasHeight)/2+origPos(2));
  set(handles.canvas,'Position',[origPos(1) newY origPos(3) newCanvasHeight]);
else
  newCanvasWidth=8*(round((w/vScale)/8)); % Hack to make width multiple of 8
  newX=floor((origPos(3)-newCanvasWidth)/2+origPos(1));
  set(handles.canvas,'Position',[newX origPos(2) newCanvasWidth origPos(4)]);
end;
p=get(handles.canvas,'Position');p=round(p);

allOk=true;

% --- Executes on button press in button_startGT.
function button_startGT_Callback(hObject, eventdata, handles)
% hObject    handle to button_startGT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles=stateTransition_function(handles,'gtStarted');
guidata(hObject,handles);

% --- Executes when selected object is changed in panel_stroke.
function panel_stroke_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in panel_stroke 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

switch get(hObject,'tag')
  case 'radio_strokeUnknown'
    handles.data.strokeType=0;
  case 'radio_strokeFG'
    handles.data.strokeType=1;
  case 'radio_strokeBG'
    handles.data.strokeType=2;
  otherwise
    fprintf('Warning: unknown stroke type selected\n');
end;
guidata(hObject,handles);

% -------- Documenting the numbers in the label image ---
%  label 0: unknown region (seg==128)
%  label 1: fg ground truth (seg==255)
%  label 2: bg ground truth (seg==0)

% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure1_WindowButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(strcmp('started',handles.data.uiState)) return; end;

pt1= get(handles.canvas,'CurrentPoint'); pt1=pt1(1,1:2);
if(pt1(1) <= 0 || pt1(1)>size(handles.data.labelImg,2) || pt1(2) <= 0 || pt1(2)>size(handles.data.labelImg,1))
 return;
end;

switch(handles.data.brushType)
  case 1 %Brush
    handles.data.drag = pt1;
    handles.data.labelImg_prev=handles.data.labelImg;
  otherwise
    return;
end;

guidata(hObject, handles);

% --- Executes on mouse motion over figure - except title and menu.
function figure1_WindowButtonMotionFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
switch(handles.data.brushType)
  case 1 %Brush
    if isempty(handles.data.drag), return,end;
    pt1 = handles.data.drag;
    pt2 = get(handles.canvas,'CurrentPoint');pt2=pt2(1,1:2);
    handles.data.drag = pt2;
    handles.data = drawBrush(handles.data,pt1,pt2);
    refreshCanvas(handles);
    guidata(hObject, handles);
  otherwise
    return;
end

function data = drawBrush(data,pt1,pt2)
diff = pt2-pt1;
[H W nFrames]=size(data.labelImg);
r = ceil(sqrt(sum(diff.^2))+1E-10);
col=uint8(data.strokeType);
for i=0:r
    pt = round(pt1+diff*(i/r));
    xLeftOffset=max(1-(pt(1)-data.brushRad),0);
    xRightOffset=min(0,W-(pt(1)+data.brushRad));
    yLeftOffset=max(1-(pt(2)-data.brushRad),0);
    yRightOffset=min(0,H-(pt(2)+data.brushRad));
    xrng = pt(1)-data.brushRad+xLeftOffset:pt(1)+data.brushRad+xRightOffset;
    yrng = pt(2)-data.brushRad+yLeftOffset:pt(2)+data.brushRad+yRightOffset;
    brmask = data.brushMask(1+yLeftOffset:2*data.brushRad+1+yRightOffset,1+xLeftOffset:2*data.brushRad+1+xRightOffset);
    data.labelImg(yrng,xrng) = data.labelImg(yrng,xrng).*(1-brmask) + brmask*col;
end

% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure1_WindowButtonUpFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(strcmp('started',handles.data.uiState)) return; end;
pt1= get(handles.canvas,'CurrentPoint'); pt1=pt1(1,1:2);
if(pt1(1) <= 0 || pt1(1)>size(handles.data.labelImg,2) || pt1(2) <= 0 || pt1(2)>size(handles.data.labelImg,1))
  switch(handles.data.brushType)
    case 1
      if(~isempty(handles.data.drag))
        handles.data.drag = [];
        guidata(hObject,handles);
      end
  end
return;
end

figure1_WindowButtonMotionFcn(hObject,eventdata,handles);
handles=guidata(hObject);
switch(handles.data.brushType)
  case 1
    handles.data.drag = [];
    refreshCanvas(handles);
  case 2
    handles.data.labelImg_prev=handles.data.labelImg;
    handles.data=floodFill(handles.data,pt1);
    refreshCanvas(handles);
end;

guidata(hObject,handles);

function data=floodFill(data,pt)
pt=round(pt);
labelImg=data.labelImg;
strokeType=data.strokeType;
x=pt(1);
y=pt(2);
%fprintf('Flood filling (%d,%d) with objNum=%d\n',x,y,strokeType);
mask=logical(zeros(size(labelImg)));
mask(labelImg==0)=1;
newmask=imfill(mask,[y x],4);
labelImg(newmask~=mask)=strokeType;
data.labelImg=labelImg;

% --- Executes on slider movement.
function slider_brushSize_Callback(hObject, eventdata, handles)
% hObject    handle to slider_brushSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

handles.data.brushRad=round(get(hObject,'Value'));
handles.data.brushMask=makeBrush(handles.data.brushRad);
set(handles.text_brushSize,'String',sprintf('Brush size: %d',handles.data.brushRad));
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function slider_brushSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_brushSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on key press with focus on figure1 and no controls selected.
function figure1_MyKeyPressFcn(h, evnt)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles=guidata(h);
%fprintf('Character: %c\nModifier: %s\nKey: %s\n',evnt.Character,'No modifier key',evnt.Key);
switch evnt.Key
  case 'uparrow'
    sliderMax=get(handles.slider_brushSize,'max');
    sliderValue=min(sliderMax,get(handles.slider_brushSize,'value')+1);
    set(handles.slider_brushSize,'value',sliderValue);
    slider_brushSize_Callback(handles.slider_brushSize, [], handles)
  case 'downarrow'
    sliderMin=get(handles.slider_brushSize,'min');
    sliderValue=max(sliderMin,get(handles.slider_brushSize,'value')-1);
    set(handles.slider_brushSize,'value',sliderValue);
    slider_brushSize_Callback(handles.slider_brushSize, [], handles)
end;

switch evnt.Key
  case 'rightarrow'
    button_next_Callback(handles.button_next,[],handles);
  case 'leftarrow'
    button_prev_Callback(handles.button_prev,[],handles);
  case 'u'
    button_undo_Callback(handles.button_prev,[],handles);
  case 'c'
    button_clear_Callback(handles.button_clear,[],handles);
  case 'a'
    tmp=handles.data.amap;
    handles.data.amap=handles.data.amap_other;
    handles.data.amap_other=tmp;
    refreshCanvas(handles);
    guidata(h,handles);
end;

if(strcmp(get(handles.panel_brushType,'visible'),'on'))
  switch(evnt.Key)
    case 'b'
      set(handles.panel_brushType,'SelectedObject',handles.radio_brushBrush);
      panel_brushType_SelectionChangeFcn(handles.radio_brushBrush,[],handles);
      handles=guidata(h);
    case 'f'
      set(handles.panel_brushType,'SelectedObject',handles.radio_brushFill);
      panel_brushType_SelectionChangeFcn(handles.radio_brushFill,[],handles);
      handles=guidata(h);
      set(handles.panel_stroke,'SelectedObject',handles.radio_strokeFG);
      panel_stroke_SelectionChangeFcn(handles.radio_strokeFG,[],handles);
      handles=guidata(h);
  end
end

if(strcmp(get(handles.panel_stroke,'visible'),'on'))
  switch(evnt.Key)
    case '1'
      set(handles.panel_stroke,'SelectedObject',handles.radio_strokeUnknown);
      panel_stroke_SelectionChangeFcn(handles.radio_strokeUnknown,[],handles);
      handles=guidata(h);
    case '2'
      set(handles.panel_stroke,'SelectedObject',handles.radio_strokeFG);
      panel_stroke_SelectionChangeFcn(handles.radio_strokeFG,[],handles);
      handles=guidata(h);
    case '3'
      set(handles.panel_stroke,'SelectedObject',handles.radio_strokeBG);
      panel_stroke_SelectionChangeFcn(handles.radio_strokeBG,[],handles);
      handles=guidata(h);
  end
end

% --- Executes on slider movement.
function slider_frameNum_Callback(hObject, eventdata, handles)
% hObject    handle to slider_frameNum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

if(strcmp(handles.data.uiState,'started')), return; end;

nxtFrame=round(get(hObject,'Value'));
diff=nxtFrame-handles.data.frameNum;

switch(diff)
  case 1
    [handles,moveOk]=forward_oneFrame(handles);
  case -1
    [handles,moveOk]=backward_oneFrame(handles);
  case 0
    moveOk=true;
    % do nothing
  otherwise
    [handles,moveOk]=jumpToFrame(handles,nxtFrame);
end

set(hObject,'Value',handles.data.frameNum);
if(moveOk)
  set(handles.text_status,'String',sprintf('%s: Frame number: %d',handles.data.inFileName,handles.data.frameNum));
  guidata(hObject,handles);
end

function [handles,moveOk]=backward_oneFrame(handles)
  moveOk=true;
  data=handles.data;
  if(strcmp(data.uiState,'segStarted')),
    moveOk=data.segmenterH.moveBackward(data.labelImg_video);
    if(~moveOk), return; end;
    curSeg=data.segmenterH.curFrame_seg();
    curSeg=imresize(curSeg,[data.hCanvas data.wCanvas],'nearest');
    %curSeg=myresize(handles.data.mapToCanvas,curSeg,[data.hCanvas data.wCanvas]);
    data.curSeg=curSeg;
    [data.segBoundaryMask,data.segBoundaryColors]= ...
    miscFns.getSegBoundary_twoColors(curSeg,data.segBoundaryColor_outside, ...
                           data.segBoundaryColor_inside...
                          ,data.segBoundaryWidth,data.segBoundaryWidth);
  end
  data.frameNum=data.frameNum-1;
  data.videoReaderH.moveBackward();
  data.curImg=im2double(...
              imresize(data.videoReaderH.curFrame,[data.hCanvas data.wCanvas],'nearest'));
  %data.curImg=im2double(...
              %myresize(handles.data.mapToCanvas,data.videoReaderH.curFrame,[data.hCanvas data.wCanvas 3]));
  data.labelImg=imresize(data.labelImg_video(:,:,data.frameNum),[data.hCanvas data.wCanvas]...
                         ,'nearest');
  %data.labelImg=myresize(handles.data.mapToCanvas,data.labelImg_video(:,:,data.frameNum),[data.hCanvas data.wCanvas]);

  handles.data=data;
  refreshCanvas(handles);

function [handles,moveOk]=forward_oneFrame(handles)
  moveOk=true;
  data=handles.data;
  if(strcmp(data.uiState,'segStarted')),
    moveOk=data.segmenterH.moveForward(data.labelImg_video);
    if(~moveOk), return; end;
    curSeg=data.segmenterH.curFrame_seg();
    curSeg=imresize(curSeg,[data.hCanvas data.wCanvas],'nearest');
    %curSeg=myresize(handles.data.mapToCanvas,curSeg,[data.hCanvas data.wCanvas]);
    data.curSeg=curSeg;
    [data.segBoundaryMask,data.segBoundaryColors]= ...
    miscFns.getSegBoundary_twoColors(curSeg,data.segBoundaryColor_outside, ...
                           data.segBoundaryColor_inside...
                          ,data.segBoundaryWidth,data.segBoundaryWidth);
  end
  data.frameNum=data.frameNum+1;
  %stTime=clock;
  data.videoReaderH.moveForward();
  %fprintf('Took %.3f secs to decode video\n',etime(clock,stTime));
  %stTime=clock;
  data.curImg=im2double(...
              imresize(data.videoReaderH.curFrame,[data.hCanvas data.wCanvas],'nearest'));
  data.labelImg=imresize(data.labelImg_video(:,:,data.frameNum),[data.hCanvas data.wCanvas]...
                         ,'nearest');
  %data.curImg=im2double(...
              %myresize(handles.data.mapToCanvas,data.videoReaderH.curFrame,[data.hCanvas data.wCanvas 3]));
  %data.labelImg=myresize(handles.data.mapToCanvas,data.labelImg_video(:,:,data.frameNum),[data.hCanvas data.wCanvas]);
  %fprintf('Took %.3f secs to imresize!\n',etime(clock,stTime));

  %stTime=clock;
  handles.data=data;
  refreshCanvas(handles);
  %fprintf('Took %.3f secs to refreshCanvas!\n',etime(clock,stTime));


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
fprintf('Cleaning up ui\n');
handles=cleanUp(handles);

delete(hObject);



% --- Executes on button press in button_next.
function button_next_Callback(hObject, eventdata, handles)
% hObject    handle to button_next (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles=loadNext(handles);
guidata(hObject,handles);

function seg=labelToSeg(labels)
[h w]=size(labels);
seg=zeros([h w],'uint8');
seg(labels==0)=128;
seg(labels==1)=255;
seg(labels==2)=0;

function handles=loadNext(handles)
% save the current segmentation
data=handles.data;
curSeg=labelToSeg(data.labelImg);
imwrite(curSeg,data.gtFile);

if(data.videoReaderH.nFrames>=(data.frameNum+1+data.skipSize)),
  for i=1:(data.skipSize+1)
    data.videoReaderH.moveForward();
  end
  handles.data.frameNum=data.frameNum+1+data.skipSize;
  handles=setupFrame(handles);
else
  maxVideos=length(data.videoNames);
  if(data.videoNum>=maxVideos),
    fprintf('All videos done!!\n');
  else
    handles.data.videoNum=data.videoNum+1;
    delete(data.videoReaderH);
    handles=loadVideo(handles);
    handles=setupFrame(handles);
  end
end

% --- Executes on button press in button_prev.
function button_prev_Callback(hObject, eventdata, handles)
% hObject    handle to button_prev (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

fprintf('Sorry, only moving forward is supported right now, go back to start if you want to go back again.\n');

% --- Executes during object creation, after setting all properties.
function button_prev_CreateFcn(hObject, eventdata, handles)
% hObject    handle to button_prev (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on key press with focus on figure1 and none of its controls.
function figure1_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function text_brushSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_brushSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function button_next_CreateFcn(hObject, eventdata, handles)
% hObject    handle to button_next (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function button_startGT_CreateFcn(hObject, eventdata, handles)
% hObject    handle to button_startGT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes when selected object is changed in panel_brushType.
function panel_brushType_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in panel_brushType 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)

switch get(hObject,'tag')
  case 'radio_brushBrush'
    handles.data.brushType=1;
  case 'radio_brushFill'
    handles.data.brushType=2;
  otherwise
    fprintf('Warning: unknown brush type selected\n');
end;
guidata(hObject,handles);


% --- Executes on button press in button_undo.
function button_undo_Callback(hObject, eventdata, handles)
% hObject    handle to button_undo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

tmp=handles.data.labelImg;
handles.data.labelImg=handles.data.labelImg_prev;
handles.data.labelImg_prev=tmp;
refreshCanvas(handles);
guidata(hObject,handles);


% --- Executes on button press in button_clear.
function button_clear_Callback(hObject, eventdata, handles)
% hObject    handle to button_clear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.data.labelImg_prev=handles.data.labelImg;
handles.data.labelImg(:)=2;
refreshCanvas(handles);
guidata(hObject,handles);


% --- Executes on scroll wheel click while the figure is in focus.
function figure1_WindowScrollWheelFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	VerticalScrollCount: signed integer indicating direction and number of clicks
%	VerticalScrollAmount: number of lines scrolled for each click
% handles    structure with handles and user data (see GUIDATA)


