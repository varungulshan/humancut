classdef ui_gtData
  properties (SetAccess = public, GetAccess = public)
    baseLineCanvasPosition % original position of canvas
    uiState % string denoting current state of UI
    drag % variable to denote if mouse is dragging
    strokeType
    canvasView % string to denote current view setting of canvas, it is one of:
    % 'drawing_segBoundary', 'gt', 'fg', 'bg', 'posterior'
    brushRad
    brushMask
    brushType
    boxStarted
    videoReaderH
    gtDir_out % dir where gt's should be saved
    gtFile
    cmd_dataset
    origCanvasPosition
    frameNum
    videoNum
    amap
    amap_other
    cmap
    labelImg % resolution = canvas
    labelImg_prev % resolution = canvas, for undo purposes
    labelImgHandle
    curImg % resolution = canvas
    curSeg % resolution = canvas
    hImg
    wImg
    videoPaths
    videoFiles
    videoNames
    debugLevel
    skipSize % no of frames to skip for GT
  end

  methods
    function obj=ui_gtData()
      % Not initializing anything here, i suppose everything gets set to [] as default
    end
  end
end
