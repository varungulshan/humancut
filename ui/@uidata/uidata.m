classdef uidata
  properties (SetAccess = public, GetAccess = public)
    baseLineCanvasPosition % original position of canvas
    uiState % string denoting current state of UI
    drag % variable to denote if mouse is dragging
    thin_firstClick
    thin_secondClick
    strokeType
    canvasView % string to denote current view setting of canvas, it is one of:
    % 'drawing_segBoundary', 'gt', 'fg', 'bg', 'posterior'
    brushRad
    brushMask
    brushType
    boxStarted
    videoReaderH
    videoReaderH_copy % to pass to the segmentation algo
    scroll_value
    scroll_step
    segBoundaryColors
    segBoundaryMask
    segBoundaryColor_inside
    segBoundaryColor_outside
    segBoundaryWidth
    gtDir
    gamma
    geoGamma
    origCanvasPosition
    segMethod
    gtBoundaryMask % in the resolution of the canvas
    gtBoundaryColors % in the resolution of the canvas
    gtSeg_video % in the resolution of the video
    frameNum
    amap
    cmap
    imgReScale % scaling factor from orignal size to canvas
    labelImg % resolution = canvas
    labelImgHandle
    curImg % resolution = canvas
    curSeg % resolution = canvas
    labelImg_video % resolution = original video
    defSaveLocation
    defSaveLocation_view
    hCanvas
    wCanvas
    segmenterH % handle to a video segmentation class
    inFileName
    pathname
    debugLevel
    labelMask
    labelBoundaryColors
    mapToCanvas
  end

  methods
    function obj=uidata()
      % Not initializing anything here, i suppose everything gets set to [] as default
    end
  end
end
