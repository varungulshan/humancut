function img=myresize(mapping,origImg,newSize)
if(length(newSize)<3),
  nCh=1;
else
  nCh=3;
  if(newSize(3)~=3), error('3rd dimension not 3\n'); end
end

if(nCh==3)
  img=reshape(origImg(mapping),newSize);
else
  N=prod(newSize);
  img=reshape(origImg(mapping(1:N)),newSize);
end
%fprintf('woo');
