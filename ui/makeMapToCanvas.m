function map=makeMaptToCanvas(canvasSize,imgSize)

tmpImg=zeros(imgSize);
N=prod(imgSize);
tmpImg(:)=[1:N];

resizedImg=imresize(tmpImg,canvasSize,'nearest');
map=resizedImg(:);
map=[map;map+N;map+2*N];
