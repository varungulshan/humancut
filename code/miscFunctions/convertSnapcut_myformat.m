function convertSnapcut_myformat()
% Renamed files from snap cut into proper format

inDir='data/snapCut_videos/walkingman/';
outDir='data/snapCut_videos/walkman/';

inFormat='frame%03d.png';
outFormat='%06d.png';

i=1;
mkdir(outDir);
while true
inFile=[inDir sprintf(inFormat,i)];
if(~exist(inFile,'file')),
  break;
end
outFile=[outDir sprintf(outFormat,i)];
copyfile(inFile,outFile);
i=i+1;
end
