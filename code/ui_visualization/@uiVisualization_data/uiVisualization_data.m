classdef uiVisualization_data
  properties (SetAccess = public, GetAccess = public)
    baseLinePosition_orig % original position of canvas
    baseLinePosition_crop1 % original position of canvas
    baseLinePosition_crop2 % original position of canvas
    origCanvasPosition
    uiState % string denoting current state of UI
    drag % variable to denote if mouse is dragging
    rootresDir % dir where to look for results
    resDir % dir where results for loaded video are found
    saveDir % dir where to save views
    videoPath
    videoFile
    videoName
    frameNum % the current frame number
    nFrames
    hImg_orig % the height of original video frame
    wImg_orig % the width of original video frame
    hAxis_orig % the height of the canvas showing the original frame
    wAxis_orig % the width of the canvas showing the original frame
    hAxis_crop1
    wAxis_crop1
    hAxis_crop2
    wAxis_crop2
    
    prevCropAsp % Aspect ratio of previous box
    crop_xT % The top left corner of crop box (in img coordinates)
    crop_yT % The top left corner of crop box (in img coordinates)
    crop_xB % The bottom right corner of crop box (in img coordinates)
    crop_yB % The bottom right corner of crop box (in img coordinates)

    origImg % the current frame
    viewRegex % regex for finding the view by its viewIndex and frameNum
              % will eb called as sprintf(viewRegex,<viewIndex>,<frameNum>)
    numViews % total number of views to search for
    views % h x w x nCh x numViews array storing the views

    origView_idx
    cropView1_idx
    cropView2_idx
    enable_cropView1
    enable_cropView2

    hFig % handle to the figure itself
    curFocus_axis % denotes which axis is in focus: one of 'origView','cropView1','cropView2'
    vH % handle to video reader
    rectH % handle to the cropping rectangle
    
    imgH_origView % handle to img in axis_origView
    imgH_cropView1 % handle to img in axis_origView
    imgH_cropView2 % handle to img in axis_origView

    lineH_cropView1
    ptH_cropView1
    annotations_colorCube
  end

  methods
    function obj=uidata()
      % Not initializing anything here, i suppose everything gets set to [] as default
    end
  end
end
