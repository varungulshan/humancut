function varargout = uiVisualization(varargin)
% UIVISUALIZATION M-file for uiVisualization.fig
%      UIVISUALIZATION, by itself, creates a new UIVISUALIZATION or raises the existing
%      singleton*.
%
%      H = UIVISUALIZATION returns the handle to a new UIVISUALIZATION or the handle to
%      the existing singleton*.
%
%      UIVISUALIZATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in UIVISUALIZATION.M with the given input arguments.
%
%      UIVISUALIZATION('Property','Value',...) creates a new UIVISUALIZATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before uiVisualization_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to uiVisualization_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help uiVisualization

% Last Modified by GUIDE v2.5 29-Jul-2010 14:26:14

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @uiVisualization_OpeningFcn, ...
                   'gui_OutputFcn',  @uiVisualization_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before uiVisualization is made visible.
function uiVisualization_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to uiVisualization (see VARARGIN)

% Choose default command line output for uiVisualization
handles.output = hObject;
handles.data=[];
set(handles.axis_origView,'Units','pixels');
set(handles.axis_cropView1,'Units','pixels');
set(handles.axis_cropView2,'Units','pixels');
handles.data=uiVisualization_data();
handles.data.hFig=hObject;
handles.data.baseLinePosition_orig=get(handles.axis_origView,'Position');
handles.data.baseLinePosition_crop1=get(handles.axis_cropView1,'Position');
handles.data.baseLinePosition_crop2=get(handles.axis_cropView2,'Position');
%handles.data.rootresDir=[cwd '../../results/motionCut_visualize/rad40/'];
handles.data.rootresDir=['/data/adam2/varun/motionCut/run001/'];
%handles.data.rootresDir=[cwd '../../results/snapCut2_visualize/try_rad30/'];
handles.data.uiState='started';
handles=stateTransition_function(handles,'started');
%handles=stateTransition_function(handles,'firstLoad');
handles.output = hObject;

fprintf('Some usage:\n1. You can use arrow keys (left and right) to go forward and backward.\n2. You can use numeric keys 1,2,...,9,0 to change the view\n3. To change the view of a particular panel, click anywhere on the corresponding axis to give it focus\n');

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes uiVisualization wait for user response (see UIRESUME)
% uiwait(handles.figure1);

function [handles,allOk]=stateTransition_function(handles,nxtState)
% Function to implement the state machine for the user interface
% See your notebook #3 for the state machine diagram, i dont
% have a image of it (If only i had a tablet)

curState=handles.data.uiState;
switch(nxtState)
  case 'started'
    %error('You cant go request to go back to started state\n');
    [handles,allOk]=resetUI(handles);
  case 'firstLoad'
    if(strcmp(curState,'started'))
      [handles,allOk]=firstLoad(handles);
    else
      handles=cleanUp(handles);
      [handles,allOk]=firstLoad(handles);
      %error('firstLoad can only be reached from started, bug\n');
    end
  otherwise
    error('Invalid state %s requested, bug\n',nxtState);
end

if(allOk)
  handles.data.uiState=nxtState;
else
  fprintf('UI could not make state transition to %s\n',nxtState);
end

function [handles,allOk]=firstLoad(handles)

data=handles.data;
videoPath=data.videoPath;
videoFile=data.videoFile;
rootresDir=data.rootresDir;
viewRegex=data.viewRegex;
numViews=data.numViews;

videoName=miscFns.extractVideoNames({videoPath},{videoFile});
videoName=videoName{1};
cwd=miscFns.extractDirPath(mfilename('fullpath'));
resDir=[rootresDir videoName '/'];

fprintf('Using result dir: %s\nView Regex=%s\nNum view=%d\nSave dir=%s\n',...
        resDir,viewRegex,numViews,data.saveDir);

data.videoName=videoName;
data.resDir=resDir;
data.vH=myVideoReader(videoPath,videoFile);
allOk=data.vH.loadOk;
data.frameNum=1;
data.nFrames=data.vH.nFrames;
[h w nCh]=size(data.vH.curFrame);

setViewText(resDir,handles.txt_viewDescription);
set(gcf,'DoubleBuffer','on');
set(handles.axis_origView,'xlimmode','manual','ylimmode','manual','zlimmode','manual');
set(handles.axis_origView,'XTick',[],'YTick',[]);
set(handles.axis_cropView1,'xlimmode','manual','ylimmode','manual','zlimmode','manual');
set(handles.axis_cropView1,'XTick',[],'YTick',[]);
set(handles.axis_cropView2,'xlimmode','manual','ylimmode','manual','zlimmode','manual');
set(handles.axis_cropView2,'XTick',[],'YTick',[]);

origPos=data.baseLinePosition_orig;
[data.hAxis_orig,data.wAxis_orig]=resizeAxis(origPos,h,w,handles.axis_origView);
axes(handles.axis_origView);
data.imgH_origView=imshow(zeros(data.hAxis_orig,data.wAxis_orig));
data.hImg_orig=h;
data.wImg_orig=w;

data.rectH=imrect(handles.axis_origView,[1 1 80 80]);
fcn=makeConstrainToRectFcn('imrect',get(handles.axis_origView,'XLim'),...
                                    get(handles.axis_origView,'YLim'));
setPositionConstraintFcn(data.rectH,fcn);
addNewPositionCallback(data.rectH,@cropChange_callback);
data.prevCropAsp=0;

handles.data=data;
handles=updateCropBox(handles);
handles=loadViews(handles);
updateViews(handles);

function cropChange_callback(newPos)
  global figHandle;
  handles=guidata(figHandle);
  handles=updateCropBox(handles);
  updateViews(handles);
  guidata(figHandle,handles);

function setViewText(resDir,txtH)
  txtFile=[resDir 'views.txt'];
  if(exist(txtFile,'file'))
    txtString=readFileToString(txtFile);
  else
    txtString=sprintf('Orig: Original frame at time t\nView 1: Local windows at time t-1. \nView 2: Motion vectors from t-1 to t. (yellow BG, blue FG)\nView 3: FG motion relative to BG from t-1 to t.\nView 4: Local windows at time t-1 (with box numbers)\nView 5: Unaries visualized at time t\nView 6-9: Empty\nView 10: Segmentation at time t.');
  end

  set(txtH,'string',txtString);

function txtString=readFileToString(txtFile)
  fH=fopen(txtFile,'r');
  txtString=fread(fH,'*char')';
  fclose(fH);

function updateViews(handles)

updateMainView(handles);
updateCropViews(handles);

function updateMainView(handles)

hCanvas=handles.data.hAxis_orig;
wCanvas=handles.data.wAxis_orig;

origView_idx=handles.data.origView_idx;
if(origView_idx==0),
  img=handles.data.origImg;
else
  img=handles.data.views(:,:,:,origView_idx);
end
imCanvas=imresize(img,[hCanvas wCanvas],'method','nearest');
%axes(handles.axis_origView);
set(handles.data.imgH_origView,'Cdata',imCanvas);

function updateCropViews(handles)

if(handles.data.enable_cropView1)
  hCanvas=handles.data.hAxis_crop1;
  wCanvas=handles.data.wAxis_crop1;
  
  viewIdx=handles.data.cropView1_idx;
  if(viewIdx==0),
    img=handles.data.origImg;
  else
    img=handles.data.views(:,:,:,viewIdx);
  end
  data=handles.data;
  img=img(data.crop_yT:data.crop_yB,data.crop_xT:data.crop_xB,:);
  imCanvas=imresize(img,[hCanvas wCanvas],'method','nearest');
  set(handles.data.imgH_cropView1,'Cdata',imCanvas);
end

if(handles.data.enable_cropView2)
  hCanvas=handles.data.hAxis_crop2;
  wCanvas=handles.data.wAxis_crop2;
  
  viewIdx=handles.data.cropView2_idx;
  if(viewIdx==0),
    img=handles.data.origImg;
  else
    img=handles.data.views(:,:,:,viewIdx);
  end
  data=handles.data;
  img=img(data.crop_yT:data.crop_yB,data.crop_xT:data.crop_xB,:);
  imCanvas=imresize(img,[hCanvas wCanvas],'method','nearest');
  set(handles.data.imgH_cropView2,'Cdata',imCanvas);
end

function handles=loadViews(handles)

handles.data.origImg=handles.data.vH.curFrame;
frameNum=handles.data.frameNum;
viewRegex=handles.data.viewRegex;
numViews=handles.data.numViews;

[h w nCh]=size(handles.data.origImg);
views=zeros([h w nCh numViews]);
for i=1:numViews
  imgPath=[handles.data.resDir sprintf(viewRegex,i,frameNum)];
  if(exist(imgPath,'file'))
    curView=imread(imgPath);
    if(size(curView,3)==1), curView=repmat(curView,[1 1 3]); end;
    views(:,:,:,i)=im2double(curView);
  else
    views(:,:,:,i)=absentView(h,w); 
  end
end
handles.data.views=views;

function img=absentView(h,w)
img=zeros([h w 3]);
img(:,:,1)=0.5;
img(:,:,2)=0.5;
img(:,:,3)=0;

function handles=updateCropBox(handles)
cropBox_pos=getPosition(handles.data.rectH);
xT=cropBox_pos(1);
yT=cropBox_pos(2);
wBox=cropBox_pos(3);
hBox=cropBox_pos(4);

xScale=handles.data.wImg_orig/handles.data.wAxis_orig;
yScale=handles.data.hImg_orig/handles.data.hAxis_orig;

xT=xT*xScale;wBox=wBox*xScale;
yT=yT*yScale;hBox=hBox*yScale;

handles.data.crop_xT=max(1,round(xT));
handles.data.crop_yT=max(1,round(yT));
handles.data.crop_xB=min(handles.data.wImg_orig,round(xT+wBox));
handles.data.crop_yB=min(handles.data.hImg_orig,round(yT+hBox));

cropH=handles.data.crop_yB-handles.data.crop_yT+1;
cropW=handles.data.crop_xB-handles.data.crop_xT+1;
cropAsp=cropW/cropH;
if(~(abs(cropAsp-handles.data.prevCropAsp)<0.05))
  [hAxis_crop1,wAxis_crop1]=resizeAxis(...
   handles.data.baseLinePosition_crop1,cropH,cropW,handles.axis_cropView1);
   axes(handles.axis_cropView1);
   handles.data.imgH_cropView1=imshow(zeros(hAxis_crop1,wAxis_crop1));
   handles.data.hAxis_crop1=hAxis_crop1;
   handles.data.wAxis_crop1=wAxis_crop1;
  [hAxis_crop2,wAxis_crop2]=resizeAxis(...
   handles.data.baseLinePosition_crop2,cropH,cropW,handles.axis_cropView2);
   axes(handles.axis_cropView2);
   handles.data.imgH_cropView2=imshow(zeros(hAxis_crop2,wAxis_crop2));
   handles.data.hAxis_crop2=hAxis_crop2;
   handles.data.wAxis_crop2=wAxis_crop2;
   fprintf('Prev crop aspect=%.4f, current=%.4f\n',handles.data.prevCropAsp,cropAsp);
   handles.data.prevCropAsp=cropAsp;
end

function [hCanvas,wCanvas]=resizeAxis(origPos,h,w,axisH)
% origPos is the current position vector of the canvas
% h w is the aspect ratio to be resize into (scaling of h,w does not matter)

hScale=w/origPos(3);
vScale=h/origPos(4);
if(hScale>vScale), 
  newCanvasHeight=floor(h/hScale);
  newY=floor((origPos(4)-newCanvasHeight)/2+origPos(2));
  set(axisH,'Position',[origPos(1) newY origPos(3) newCanvasHeight]);
else
  newCanvasWidth=8*(round((w/vScale)/8)); % Hack to make width multiple of 8
  newX=floor((origPos(3)-newCanvasWidth)/2+origPos(1));
  set(axisH,'Position',[newX origPos(2) newCanvasWidth origPos(4)]);
end;
p=get(axisH,'Position');p=round(p);

%fprintf('Original video resolution = %d x %d, nFrames = %d\n',w,h,nFrames);
%fprintf('Canvas resolution %d x %d\n',p(3),p(4));
hCanvas=p(4);
wCanvas=p(3);

function [handles,allOk]=resetUI(handles)

global figHandle;

figHandle=handles.data.hFig;

handles.data.origView_idx=0;
set(handles.panel_origView,'SelectedObject',handles.origView_orig);
handles.data.cropView1_idx=0;
set(handles.panel_cropView1,'SelectedObject',handles.cropView1_orig);
handles.data.cropView2_idx=0;
set(handles.panel_cropView2,'SelectedObject',handles.cropView2_orig);
handles.data.enable_cropView1=true;
handles.data.enable_cropView2=true;
set(handles.chkBox_enableView1,'Value',1);
set(handles.chkBox_enableView2,'Value',1);
handles.data.curFocus_axis='origView';
set(handles.axis_origView,'xlimmode','manual','ylimmode','manual','zlimmode','manual');
set(handles.axis_origView,'XTick',[],'YTick',[]);
set(handles.axis_cropView1,'xlimmode','manual','ylimmode','manual','zlimmode','manual');
set(handles.axis_cropView1,'XTick',[],'YTick',[]);
set(handles.axis_cropView2,'xlimmode','manual','ylimmode','manual','zlimmode','manual');
set(handles.axis_cropView2,'XTick',[],'YTick',[]);
cwd=miscFns.extractDirPath(mfilename('fullpath'));
handles.data.viewRegex='view%d_%03d.png';
handles.data.numViews=10;
handles.data.saveDir=[cwd './tmpSave/'];
handles.data.lineH_cropView1={};
handles.data.ptH_cropView1={};
handles.data.annotations_colorCube=repmat(colorcube(10),[10 1]);

allOk=true;


% --- Outputs from this function are returned to the command line.
function varargout = uiVisualization_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in chkBox_enableView1.
function chkBox_enableView1_Callback(hObject, eventdata, handles)
% hObject    handle to chkBox_enableView1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkBox_enableView1

handles.data.enable_cropView1=logical(get(hObject,'Value'));
updateCropViews(handles);
guidata(hObject,handles);

% --- Executes on button press in chkBox_enableView2.
function chkBox_enableView2_Callback(hObject, eventdata, handles)
% hObject    handle to chkBox_enableView2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkBox_enableView2
handles.data.enable_cropView2=logical(get(hObject,'Value'));
updateCropViews(handles);
guidata(hObject,handles);

% --- Executes when selected object is changed in panel_origView.
function panel_origView_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in panel_origView 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)

switch(get(hObject,'tag'))
  case 'origView_orig'
    handles.data.origView_idx=0;    
  case 'origView_1'
    handles.data.origView_idx=1;    
  case 'origView_2'
    handles.data.origView_idx=2;    
  case 'origView_3'
    handles.data.origView_idx=3;    
  case 'origView_4'
    handles.data.origView_idx=4;    
  case 'origView_5'
    handles.data.origView_idx=5;    
  case 'origView_6'
    handles.data.origView_idx=6;    
  case 'origView_7'
    handles.data.origView_idx=7;    
  case 'origView_8'
    handles.data.origView_idx=8;    
  case 'origView_9'
    handles.data.origView_idx=9;    
  case 'origView_10'
    handles.data.origView_idx=10;    
end

updateMainView(handles);
guidata(hObject,handles);

% --- Executes when selected object is changed in panel_cropView1.
function panel_cropView1_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in panel_cropView1 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)


switch(get(hObject,'tag'))
  case 'cropView1_orig'
    handles.data.cropView1_idx=0;    
  case 'cropView1_1'
    handles.data.cropView1_idx=1;    
  case 'cropView1_2'
    handles.data.cropView1_idx=2;    
  case 'cropView1_3'
    handles.data.cropView1_idx=3;    
  case 'cropView1_4'
    handles.data.cropView1_idx=4;    
  case 'cropView1_5'
    handles.data.cropView1_idx=5;    
  case 'cropView1_6'
    handles.data.cropView1_idx=6;    
  case 'cropView1_7'
    handles.data.cropView1_idx=7;    
  case 'cropView1_8'
    handles.data.cropView1_idx=8;    
  case 'cropView1_9'
    handles.data.cropView1_idx=9;    
  case 'cropView1_10'
    handles.data.cropView1_idx=10;    
end

updateCropViews(handles);
guidata(hObject,handles);

% --- Executes when selected object is changed in panel_cropView2.
function panel_cropView2_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in panel_cropView2 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)

switch(get(hObject,'tag'))
  case 'cropView2_orig'
    handles.data.cropView2_idx=0;    
  case 'cropView2_1'
    handles.data.cropView2_idx=1;    
  case 'cropView2_2'
    handles.data.cropView2_idx=2;    
  case 'cropView2_3'
    handles.data.cropView2_idx=3;    
  case 'cropView2_4'
    handles.data.cropView2_idx=4;    
  case 'cropView2_5'
    handles.data.cropView2_idx=5;    
  case 'cropView2_6'
    handles.data.cropView2_idx=6;    
  case 'cropView2_7'
    handles.data.cropView2_idx=7;    
  case 'cropView2_8'
    handles.data.cropView2_idx=8;    
  case 'cropView2_9'
    handles.data.cropView2_idx=9;    
  case 'cropView2_10'
    handles.data.cropView2_idx=10;    
end

updateCropViews(handles);
guidata(hObject,handles);


% --- Executes on mouse motion over figure - except title and menu.
function figure1_WindowButtonMotionFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure1_WindowButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if(~strcmp(handles.data.uiState,'firstLoad')), return; end;
data=handles.data;
pt1=get(handles.axis_origView,'CurrentPoint'); pt1=pt1(1,1:2);
if(pt1(1) > 0 && pt1(1)<=data.wAxis_orig && pt1(2) > 0 && pt1(2)<=data.hAxis_orig)
  handles.data.curFocus_axis='origView';
  fprintf('Focus shifted to origView\n');
end
pt1=get(handles.axis_cropView1,'CurrentPoint'); pt1=pt1(1,1:2);
if(pt1(1) > 0 && pt1(1)<=data.wAxis_crop1 && pt1(2) > 0 && pt1(2)<=data.hAxis_crop1)
  handles.data.curFocus_axis='cropView1';
  fprintf('Focus shifted to cropView1\n');
end
pt1=get(handles.axis_cropView2,'CurrentPoint'); pt1=pt1(1,1:2);
if(pt1(1) > 0 && pt1(1)<=data.wAxis_crop2 && pt1(2) > 0 && pt1(2)<=data.hAxis_crop2)
  handles.data.curFocus_axis='cropView2';
  fprintf('Focus shifted to cropView2\n');
end

guidata(hObject,handles);

% --- Executes on key press with focus on figure1 and none of its controls.
function figure1_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

switch(eventdata.Key)
  case 'rightarrow'
    handles=moveForward(handles);
  case 'leftarrow'
    handles=moveBackward(handles);
end

handles=changeView(eventdata.Key,handles);
guidata(hObject,handles);

function handles=changeView(key,handles)

curFocus_axis=handles.data.curFocus_axis;
hFig=handles.data.hFig;

updateView=false;

switch(key)
  case {'1','2','3','4','5','6','7','8','9'}
    radioBox_initial=getRadioBoxName(curFocus_axis);
    radioBox_surname=key;
    updateView=true;
  case '0'
    radioBox_initial=getRadioBoxName(curFocus_axis);
    radioBox_surname='10';
    updateView=true;
  case 'o'
    radioBox_initial=getRadioBoxName(curFocus_axis);
    radioBox_surname='orig';
    updateView=true;
end

if(updateView)
  cmd=sprintf('set(handles.panel_%s,''SelectedObject'',handles.%s_%s);',...
              radioBox_initial,radioBox_initial,radioBox_surname);
  eval(cmd);
  cmd=sprintf('panel_%s_SelectionChangeFcn(handles.%s_%s,[],handles);',...
              radioBox_initial,radioBox_initial,radioBox_surname);
  eval(cmd);
  handles=guidata(hFig);
  updateCropViews(handles);
end

function radioBox_name=getRadioBoxName(axisFocus)

  switch(axisFocus)
    case 'origView'
      radioBox_name='origView';
    case 'cropView1'
      radioBox_name='cropView1';
    case 'cropView2'
      radioBox_name='cropView2';
    otherwise
      error('Invalid axis in focus: %s\n',axisFocus);
  end

function handles=moveBackward(handles)
  if(handles.data.frameNum<=1)
    fprintf('Beginning of video reached, cant move back further\n');
    return;
  end
  handles.data.vH.moveBackward();
  handles.data.frameNum=handles.data.frameNum-1;
  fprintf('Current frame: #%d\n',handles.data.frameNum);
  handles=loadViews(handles);
  updateViews(handles);

function handles=moveForward(handles)
  if(handles.data.frameNum>=handles.data.nFrames)
    fprintf('End of video reached at frame #%d, cant move forward further\n',...
           handles.data.frameNum);
    return;
  end
  handles.data.vH.moveForward();
  handles.data.frameNum=handles.data.frameNum+1;
  fprintf('Current frame: #%d\n',handles.data.frameNum);
  handles=loadViews(handles);
  updateViews(handles);

% --- Executes during object deletion, before destroying properties.
function figure1_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




% --- Executes on button press in button_updateCrop.
function button_updateCrop_Callback(hObject, eventdata, handles)
% hObject    handle to button_updateCrop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles=updateCropBox(handles);
updateViews(handles);
guidata(hObject,handles);


function handles=cleanUp(handles)
  delete(handles.data.vH);

  lineH=handles.data.lineH_cropView1;
  ptH=handles.data.ptH_cropView1;
  drawingHandles=horzcat(lineH,ptH);
  for i=1:length(drawingHandles)
    delete(drawingHandles{i});
  end
  handles.data.lineH_cropView1={};
  handles.data.ptH_cropView1={};

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
handles=cleanUp(handles);
delete(hObject);


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in button_load.
function button_load_Callback(hObject, eventdata, handles)
% hObject    handle to button_load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles=stateTransition_function(handles,'started');
cwd=miscFns.extractDirPath(mfilename('fullpath'));
[filename, pathname] = uigetfile({'*.*';'*.mat';'*.avi'},'Open video to segment',...
                        [cwd '../../data/cuts/']);
handles.data.videoPath=pathname;
handles.data.videoFile=filename;
handles=stateTransition_function(handles,'firstLoad');
guidata(hObject,handles);


% --- Executes on button press in button_saveView.
function button_saveView_Callback(hObject, eventdata, handles)
% hObject    handle to button_saveView (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

imgOrig=get(handles.data.imgH_origView,'cdata');
imgCrop1=get(handles.data.imgH_cropView1,'cdata');
imgCrop2=get(handles.data.imgH_cropView2,'cdata');

origFile=[handles.data.saveDir sprintf('orig_%03d.png',handles.data.frameNum)];
crop1File=[handles.data.saveDir sprintf('crop1_%03d.png',handles.data.frameNum)];
crop2File=[handles.data.saveDir sprintf('crop2_%03d.png',handles.data.frameNum)];

imwrite(imgOrig,origFile);
imwrite(imgCrop1,crop1File);
imwrite(imgCrop2,crop2File);

% --- Executes on button press in button_pop_origView.
function button_pop_origView_Callback(hObject, eventdata, handles)
% hObject    handle to button_pop_origView (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

imgOrig=get(handles.data.imgH_origView,'cdata');
figure;imshow(imgOrig);

% --- Executes on button press in button_pop_cropView1.
function button_pop_cropView1_Callback(hObject, eventdata, handles)
% hObject    handle to button_pop_cropView1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

imgCrop1=get(handles.data.imgH_cropView1,'cdata');
figure;imshow(imgCrop1);

% --- Executes on button press in button_pop_cropView2.
function button_pop_cropView2_Callback(hObject, eventdata, handles)
% hObject    handle to button_pop_cropView2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

imgCrop2=get(handles.data.imgH_cropView2,'cdata');
figure;imshow(imgCrop2);


% --- Executes on button press in button_diagnose.
function button_diagnose_Callback(hObject, eventdata, handles)
% hObject    handle to button_diagnose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

varList={};
tryList={'frameNum'};
nameList={'frameNum'};

for i=1:length(tryList)
  try
    eval(['handles.data.' tryList{i} ';']);
    cmd=[nameList{i} '=handles.data.' tryList{i} ';'];
    eval(cmd);
    varList{end+1}=nameList{i};
  catch
    continue;
  end
end

fprintf('\n------- Diagnosing -------\nVariables for you to analyse:\n');
for i=1:length(varList)
  fprintf([varList{i} '\n']);
end

keyboard;

% --- Executes on button press in button_drawLine_cropView1.
function button_drawLine_cropView1_Callback(hObject, eventdata, handles)
% hObject    handle to button_drawLine_cropView1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

lineH=handles.data.lineH_cropView1;
numLines=length(lineH);
xStart=handles.data.wAxis_crop1/2;
yStart=handles.data.hAxis_crop1/2;
curLine=imline(handles.axis_cropView1,[xStart yStart; xStart+20 yStart+20]);
setColor(curLine,handles.data.annotations_colorCube(numLines+1,:));
lineH{end+1}=curLine;
handles.data.lineH_cropView1=lineH;
guidata(hObject,handles);

% --- Executes on button press in button_drawPt_cropView1.
function button_drawPt_cropView1_Callback(hObject, eventdata, handles)
% hObject    handle to button_drawPt_cropView1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

ptH=handles.data.ptH_cropView1;
numPts=length(ptH);

curPt=impoint(handles.axis_cropView1,[handles.data.wAxis_crop1/2 ...
                                      handles.data.hAxis_crop1/2]);
setColor(curPt,handles.data.annotations_colorCube(numPts+1,:));
ptH{end+1}=curPt;
handles.data.ptH_cropView1=ptH;
guidata(hObject,handles);

% --- Executes on button press in button_delLine_cropView1.
function button_delLine_cropView1_Callback(hObject, eventdata, handles)
% hObject    handle to button_delLine_cropView1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

lineH=handles.data.lineH_cropView1;
if(length(lineH)>0)
  delete(lineH{end});
  lineH=lineH(1:end-1);
  handles.data.lineH_cropView1=lineH;
  guidata(hObject,handles);
end


% --- Executes on button press in button_delPt_cropView1.
function button_delPt_cropView1_Callback(hObject, eventdata, handles)
% hObject    handle to button_delPt_cropView1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

ptH=handles.data.ptH_cropView1;
if(length(ptH)>0)
  delete(ptH{end});
  ptH=ptH(1:end-1);
  handles.data.ptH_cropView1=ptH;
  guidata(hObject,handles);
end

% --- Executes on button press in button_toggleAll_cropView1.
function button_toggleAll_cropView1_Callback(hObject, eventdata, handles)
% hObject    handle to button_toggleAll_cropView1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

lineH=handles.data.lineH_cropView1;
ptH=handles.data.ptH_cropView1;
drawingHandles=horzcat(lineH,ptH);
for i=1:length(drawingHandles)
  curVis=get(drawingHandles{i},'Visible');
  if(strcmp(curVis,'on')),
    set(drawingHandles{i},'Visible','off');
  else
    set(drawingHandles{i},'Visible','on');
  end
end

% --- Executes on button press in button_delAll_cropView1.
function button_delAll_cropView1_Callback(hObject, eventdata, handles)
% hObject    handle to button_delAll_cropView1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

lineH=handles.data.lineH_cropView1;
ptH=handles.data.ptH_cropView1;
drawingHandles=horzcat(lineH,ptH);
for i=1:length(drawingHandles)
  delete(drawingHandles{i});
end
handles.data.lineH_cropView1={};
handles.data.ptH_cropView1={};
guidata(hObject,handles);


% --- Executes on button press in button_changeResDir.
function button_changeResDir_Callback(hObject, eventdata, handles)
% hObject    handle to button_changeResDir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

rootresDir=uigetdir(handles.data.rootresDir,'Choose result directory');
if(~ischar(rootresDir)),  return; end;
if(rootresDir(end)~='/'), rootresDir=[rootresDir '/']; end;

handles.data.rootresDir=rootresDir;
fprintf('Result directory now points to %s, load video to see results from this dir\n',...
        rootresDir);

guidata(hObject,handles);
