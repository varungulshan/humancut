function pp_globalPb(videoFile)

videoPath='./data/myVideos/';
%videoFile='buffy_s5e03_2.avi';
fullPath=[videoPath videoFile];
outDir=['./pp/globalPb1/' extractImgName(fullPath) '/'];
tmpDir=outDir;

% set pb parameters , see in software/globalPb/example.m
opts.rsz = 0.5;          
tmpIn=[tmpDir 'tmpIn.png'];
tmpOut=[tmpDir 'tmpOut.bmp'];

if(~exist(outDir,'dir')), mkdir(outDir); end;
optsFile=[outDir 'pbOpts.mat'];
if(exist(optsFile,'file')),
  msg=sprintf('Overwrite existing pb output %s? (y/n)[n]: ',optsFile);
  ch=input(msg,'s');
  if(~strcmp(ch,'y')), return; end;
end
save(optsFile,'opts');

vH=myVideoReader(videoPath,videoFile);
frIndex=1;

while(1)
  curFrame=vH.curFrame;
  imwrite(curFrame,tmpIn);
  stTime=clock;
  [pb_thin,pb_thick,xx]=globalPb(tmpIn,tmpOut,opts.rsz);
  fprintf('Pb took %d seconds\n',round(etime(clock,stTime)));

  save([outDir sprintf('%04d.mat',frIndex)],'pb_thin');
  frIndex=frIndex+1;
  if(frIndex<=vH.nFrames)
    vH.moveForward();
  else
    break;
  end
end

delete(vH);
