function viewVideoSlice(video,frameNum,orientation)
% video -> h x w x nCh x nFrames volume
% frameNum -> the frame to show for user input
% orientation -> 'h' or 'v' for horizontal or vertical

curFrame=video(:,:,:,frameNum);
fHandle=figure;imshow(curFrame);
fSlice=figure;

[h w nCh]=size(curFrame);

fprintf('Click on top right corner to exit\n');

while(true)
  figure(fHandle);
  [x,y]=ginput(1);
  if(y<0.1*h & x>0.9*w), break; end;
  x=round(x);y=round(y);
  figure(fHandle);
  tmpFrame=drawSlice(curFrame,orientation,x,y);
  imshow(tmpFrame);

  figure(fSlice);%imshow(slice);
  slice=showSlice(video,orientation,x,y);
end

function slice=showSlice(video,orientation,x,y)
[h w nCh nFrames]=size(video);
switch(orientation)
  case 'h'
    slice=video(y,:,:,:);
    slice=shiftdim(slice);
    slice=shiftdim(slice,2); 
    % slice is now nFrames x w x nCh
    slice=flipdim(slice,1);
    imshow(slice);
    ylabel(sprintf('nFrames [1 .. %d]',nFrames));
    xlabel(sprintf('w = [1 .. %d ]',w));
  case 'v'
    slice=video(:,x,:,:);
    slice=shiftdim(slice,1);
    slice=shiftdim(slice); 
    slice=permute(slice,[3 2 1]);
    %slice=shiftdim(slice,1);
    % slice is now h x nFrames x nCh
    %slice=flipdim(slice,2);
    imshow(slice);
    xlabel(sprintf('nFrames [1 .. %d]',nFrames));
    ylabel(sprintf('h = [1 .. %d ]',h));
  otherwise
    error('Invalid orientation %s\n',orientation);
end

function img=drawSlice(img,orientation,x,y)

[h w nCh]=size(img);
rad=1;
clr=[255 0 0];

switch(orientation)
  case 'h'
    yRng=[y-rad:y+rad];
    xRng=[1:w];
  case 'v'
    xRng=[x-rad:x+rad];
    yRng=[1:h];
  otherwise
    error('Invalid orientation %s\n',orientation);
end

for i=1:nCh
  img(yRng,xRng,i)=clr(i);
end
