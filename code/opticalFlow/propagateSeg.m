function seg=propagateSeg(curSeg,flow)

flow=round(flow); % Taking the nearest neighbour
deltaX=flow(:,:,1);deltaX=deltaX(:);
deltaY=flow(:,:,2);deltaY=deltaY(:);

[h w]=size(curSeg);
n=h*w;

[x,y]=meshgrid([1:w],[1:h]);
x=x(:);y=y(:);
nbrsX=x+deltaX;
nbrsY=y+deltaY;
validNbrs=(nbrsX>0 & nbrsX<=w & nbrsY>0 & nbrsY<=h);
nbrs=(nbrsX-1)*h+nbrsY;
%deltaIndex=(deltaX-1)*h+deltaY;
%nbrs=[1:n]'+deltaIndex;

seg=128*ones([h w],'uint8');
seg(validNbrs)=curSeg(nbrs(validNbrs));
