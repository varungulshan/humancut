function img=makeColorWheel(patchRad)
% To visualize the color wheel of Ce-liu's code

%patchRad=15;

[u,v]=meshgrid([-patchRad:patchRad],[-patchRad:patchRad]);
mask=zeros(2*patchRad+1,2*patchRad+1);
mask((u.*u+v.*v)<=(patchRad*patchRad))=1;
u(~mask)=0;
v(~mask)=0;
img=flowToColor(cat(3,u,v));
