function pp_opticalFlow(videoFile)

videoPath='./data/myVideos/';
%videoFile='buffy_s5e03_2.avi';
fullPath=[videoPath videoFile];
outDir=['./pp/flows1/' extractImgName(fullPath) '/'];

% set optical flow parameters (see Coarse2FineTwoFrames.m for the definition of the parameters)
opts.alpha = 0.02;          
opts.ratio = 0.75;
opts.minWidth = 30;
opts.nOuterFPIterations = 20;
opts.nInnerFPIterations = 1;
opts.nCGIterations = 50;
opts.rescale=0.5;

para = [opts.alpha,opts.ratio,opts.minWidth,opts.nOuterFPIterations, ...
        opts.nInnerFPIterations,opts.nCGIterations];

if(~exist(outDir,'dir')), mkdir(outDir); end;
optsFile=[outDir 'flowOpts.mat'];
if(exist(optsFile,'file')),
  msg=sprintf('Overwrite existing flow %s? (y/n)[n]: ',optsFile);
  ch=input(msg,'s');
  if(~strcmp(ch,'y')), return; end;
end
save(optsFile,'opts');

vH=myVideoReader(videoPath,videoFile);
prevFrame=vH.curFrame;
frIndex=2;

while(frIndex<=vH.nFrames)
  vH.moveForward();
  curFrame=vH.curFrame;
  im1 = imresize(curFrame,opts.rescale);
  im2 = imresize(prevFrame,opts.rescale); % resizing for computation reasons
  % this is the core part of calling the mexed dll file for computing optical flow
  % it also returns the time that is needed for two-frame estimation
  stTime=clock;
  [vx,vy,warpI2] = Coarse2FineTwoFrames(im1,im2,para);
  fprintf('Two frame optical flow took %d seconds\n',round(etime(clock,stTime)));
  vx=vx/opts.rescale;
  vy=vy/opts.rescale;
  if(opts.rescale~=1)
    vx=imresize(vx,[vH.h vH.w]);
    vy=imresize(vy,[vH.h vH.w]);
  end
  clear flowVec;
  flowVec(:,:,1) = vx;
  flowVec(:,:,2) = vy;

  imflow = flowToColor(flowVec);
  prevFrame=curFrame;
  imwrite(imflow,[outDir sprintf('backward-%04d.png',frIndex)]);
  save([outDir sprintf('backward-%04d.mat',frIndex)],'flowVec');
  frIndex=frIndex+1;
end

delete(vH);
%figure;imshow(im1);figure;imshow(warpI2);
%
%% output gif
%clear volume;
%volume(:,:,:,1) = im1;
%volume(:,:,:,2) = im2;
%if exist('output','dir')~=7
    %mkdir('output');
%end
%frame2gif(volume,fullfile('output',[example '_input.gif']));
%volume(:,:,:,2) = warpI2;
%frame2gif(volume,fullfile('output',[example '_warp.gif']));

% visualize flow field
clear flow;
