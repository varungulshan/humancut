#include "mex.h"
#include "graph.h"
#include <cmath>
#include <iostream>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Input parameters combinations
     rhs[0] = N=number of nodes, uint32
     rhs[1] = gamma, double
     rhs[2] = gamma_ising, double
     rhs[3] = unaries, 2 x N , int32 array
     rhs[4] = curImg, [h x w x nCh] double image
     rhs[5] = roffset, [L x 1] int32
     rhs[6] = coffset, [L x 1] int32
     rhs[7] = gcScale, double
     rhs[8] = edgeMap, [h x w] boolean

     lhs[0] -> returns the binary labels of all nodes (array of type 1xN BOOL)
     lhs[1] -> returns the flow
  */

  if (nrhs != 9)
    mexErrMsgTxt("9 inputs required");

  if(mxGetClassID(prhs[0])!=mxUINT32_CLASS)
    mexErrMsgTxt("prhs[0] (num nodes) should be of type char\n");
  if(mxGetClassID(prhs[1])!=mxDOUBLE_CLASS)
    mexErrMsgTxt("prhs[1] (gamma) should be of type double\n");
  if(mxGetClassID(prhs[2])!=mxDOUBLE_CLASS)
    mexErrMsgTxt("prhs[2] (gamma_ising) should be of type double\n");
  if(mxGetClassID(prhs[3])!=mxINT32_CLASS)
    mexErrMsgTxt("prhs[3] (unaries) should be of type int32\n");
  if(mxGetClassID(prhs[4])!=mxDOUBLE_CLASS)
    mexErrMsgTxt("prhs[4] (image) should be of type double\n");
  if(mxGetClassID(prhs[5])!=mxINT32_CLASS)
    mexErrMsgTxt("prhs[5] (roffset) should be of type int32\n");
  if(mxGetClassID(prhs[6])!=mxINT32_CLASS)
    mexErrMsgTxt("prhs[6] (coffset) should be of type int32\n");
  if(mxGetClassID(prhs[7])!=mxDOUBLE_CLASS)
    mexErrMsgTxt("prhs[7] (gcScale) should be of type double\n");
  if(mxGetClassID(prhs[8])!=mxLOGICAL_CLASS)
    mexErrMsgTxt("prhs[8] (edgeMap) should be of type logical\n");


  typedef Graph<int,int,int> GraphInt;

  if(mxGetN(prhs[0])!=1 || mxGetM(prhs[0])!=1) mexErrMsgTxt("prhs[0] (number of nodes) shd be 1x1\n");

  if(mxGetN(prhs[1])!=1 || mxGetM(prhs[1])!=1) mexErrMsgTxt("prhs[1] (gamma) shd be 1x1\n");
  if(mxGetN(prhs[2])!=1 || mxGetM(prhs[2])!=1) mexErrMsgTxt("prhs[2] (gamma_ising) shd be 1x1\n");
  if(mxGetN(prhs[7])!=1 || mxGetM(prhs[7])!=1) mexErrMsgTxt("prhs[7] (gcScale) shd be 1x1\n");

  // ---- Data verified ok, now initialize the graph cut code
  int n,nOffset,mBound;
  n=*((unsigned int*)mxGetData(prhs[0]));
  nOffset=mxGetM(prhs[5]);
  if(nOffset!=mxGetM(prhs[6]))
    mexErrMsgTxt("cOffset does not have same dimensions as rOffset\n");
  if(mxGetN(prhs[5])!=1) mexErrMsgTxt("prhs[5] (roffset) shd be L x 1\n");
  if(mxGetN(prhs[6])!=1) mexErrMsgTxt("prhs[5] (roffset) shd be L x 1\n");
  int numDims=mxGetNumberOfDimensions(prhs[4]);
  int h=mxGetDimensions(prhs[4])[0];
  int w=mxGetDimensions(prhs[4])[1];
  if(n!=(h*w))
    mexErrMsgTxt("No. of image pixels not same as number of nodes\n");
  int D;
  if(numDims==3)
    D=mxGetDimensions(prhs[4])[2];
  else
    D=1;

  mBound=n*nOffset; // Upper bound on number of edges in graph

  GraphInt *g=newGraph<int,int,int>(n,mBound,NULL); 
    
  g->add_node(n);

  // --- Data verified ok ------------

  if(mxGetN(prhs[3])!=n) 
    {mexErrMsgTxt("Num of unary energies not equal to num of nodes\n");}
  if(mxGetM(prhs[3])!=2) 
    {mexErrMsgTxt("Num of unary energies shd be 2 x N\n");}

  int *unaryEnergyIter = (int*)mxGetData(prhs[3]);
  // --- Enter the unary energies ---
  for(int i=0;i<n;i++){
    int si,it;
    si=*unaryEnergyIter;unaryEnergyIter++;
    it=*unaryEnergyIter;unaryEnergyIter++;
    g->add_tweights(i,si,it);
  }

  // ---- Enter the interaction energies ---
  int *roffsets=(int*)mxGetData(prhs[5]);
  int *coffsets=(int*)mxGetData(prhs[6]);
  double *C=mxGetPr(prhs[4]);
  bool* edgeMap=(bool*)mxGetData(prhs[8]);
  double gcScale=*mxGetPr(prhs[7]);
  double gamma_ising=*mxGetPr(prhs[2]);
  double gamma=*mxGetPr(prhs[1]);

  if(h!=mxGetM(prhs[8]) || w!=mxGetN(prhs[8]))
    mexErrMsgTxt("edgeMap not the same size as image\n");

  int *indexOffsets=(int*)mxMalloc(nOffset*sizeof(int));
  for(int i=0;i<nOffset;i++) {
    indexOffsets[i]=coffsets[i]*h+roffsets[i];
  }

  for(int x=0;x<w;x++){
    for(int y=0;y<h;y++){
      int lEdge=y+x*h;
      bool isEdge_l=edgeMap[lEdge];
      for(int i=0;i<nOffset;i++){
        int r=y+roffsets[i];
        if(r>=0 && r<h){
          int c=x+coffsets[i];
          if(c>=0 && c<w){
            int rEdge=lEdge+indexOffsets[i];
            bool isEdge_r=edgeMap[rEdge];
            int edgeW;
            if(isEdge_l|isEdge_r){
              edgeW=(int)(gcScale*gamma_ising);
            }
            else{
              edgeW=(int)(gcScale*(gamma_ising+gamma));
            }
            // add edge to graph
            g->add_edge(lEdge,rEdge,edgeW,edgeW); 
          }
        }
      }
    }
  }

  int flow=g->maxflow();

  // --- Now prepare the output ------------
  if(nlhs < 2) {mexErrMsgTxt("Atleast two outputs required\n");}
  int dims[2];dims[0]=1;dims[1]=g->get_node_num();
  plhs[0]=mxCreateNumericArray(2,dims,mxUINT8_CLASS,mxREAL);
  plhs[1]=mxCreateDoubleScalar((double)flow);
  unsigned char *segmentIter=(unsigned char*)mxGetData(plhs[0]);
  for(int i=0;i<dims[1];i++,segmentIter++){
    *segmentIter=(g->what_segment(i)==GraphInt::SOURCE?0:1); // 0 for source, 1 for sink
  }

  // ------- To free -------------
  mxFree(indexOffsets);
  deleteGraph<int,int,int>(&g);
}
