function klt_demo()
% Demo of KLT tracker of VGG Matlab
%
%
% Authors:
% V. Ferrari and M. Everingham
%

% setup display of tracks

% filename of video to process
%frames_dir = [vgg_matlab_root '/vgg_demodata/klt'];
%videoFile='/home/varun/windows/rugbyShared/work/peopleCut/trunk/data/myVideos/my_cousin_vinny_1-noBlackBars-recoded.avi';
%videoFile='/home/varun/windows/rugbyShared/work/peopleCut/trunk/data/myVideos/buffy_s5e03_2.avi';
%videoFile='/home/varun/windows/rugbyShared/work/peopleCut/trunk/data/myVideos/as_good_as_it_gets_1.avi';
%videoFile='/home/varun/windows/rugbyShared/work/peopleCut/trunk/data/myVideos/buffy_s5e03_1.avi';
%videoFile='/home/varun/windows/rugbyShared/work/peopleCut/trunk/data/myVideos/love_actually_1-noBlackBars-recoded.avi';
%outVideo='../results/kltOutputs/buffy_s5e03_2.avi';
%outVideo='../results/kltOutputs/as_good_as_it_gets.avi';
%outVideo='../results/kltOutputs/buffy_s5e03_1.avi';
%outVideo='../results/kltOutputs/vinny.avi';
%outVideo='../results/kltOutputs/loveActually.avi';

cwd=extractDirPath(mfilename('fullpath'));
videoFilePath='../../data/snapCut_videos/footballer/';
videoFileName='00001.png';
outVideo='../../results/kltTracks/footballer.avi';

videoFilePath=[cwd videoFilePath];
outVideo=[cwd outVideo];

vH=myVideoReader(videoFilePath,videoFileName);
vH_out=videoWriter(outVideo);
% first and last frame to track
% (extract features from first frame)
nFrames=vH.nFrames;
nFramesActual=vH.nFrames;

f1 = 0;   
f2 = nFrames-1;
showLiveTracks=false; % set to true to see tracks as the algo goes
drawRadius=1; % thickness of crosses to draw for features on the saved output video

if(showLiveTracks)
  figure;
  clf reset;
  set(gcf,'doublebuffer','on');
end

% initializations
tc = vgg_klt_init('nfeats',1000,'mindisp',0.5,'pyramid_levels',2);
%
K = zeros(3,tc.nfeats,f2-f1+1);
%
%next(vH);
I = vH.curFrame;
I = im2double(rgb2gray(I));
If1 = I;
M = [];
stTime=clock;
[tc,P] = vgg_klt_selfeats(tc,I,M);
fprintf('Time taken to select features= %.3f seconds\n',etime(clock,stTime));
K(:,:,1) = P;                                        % feat locs in all frames K(x,y,f)
%
n=sum(P(3,:)>=0);
vgg_printf('%d features found in first frame\n', n);

% tracking frames (f1+1):f2
if(showLiveTracks)
  h = subplot(2,1,1);
end
for f = (f1+1):f2    
    vH.moveForward();
    I = vH.curFrame;
    I_color=im2double(I);
    I = im2double(rgb2gray(I));
    %tic;
    stTime=clock;
    [tc,P] = vgg_klt_track(tc,P,I,M);
    fprintf('Tracking features = %.3f seconds\n',etime(clock,stTime));
    stTime=clock;

    nt = sum(P(3,:)>=0);
    vgg_printf('%d tracked\n', n);
    
    [tc,P] = vgg_klt_selfeats(tc,I,M,P);    
    K(:,:,f-f1+1) = P;
    fprintf('Replacing features = %.3f seconds\n',etime(clock,stTime));
    
    nn = sum(P(3,:)>=0);
    %toc;
    
    vgg_printf('now %d features (%d replaced)\n', nn, nn-nt);
   
    % display currently active tracks
    outImg=drawFeatures(I_color,P,drawRadius);
    addframe(vH_out,outImg);

    if(showLiveTracks)
      clf(h); subplot(2,1,1);
      imshow(I);       % display frame (clf -> speed)
      hold on;
      plot(P(1,P(3,:)==0),P(2,P(3,:)==0),'g+');
      plot(P(1,P(3,:)>0),P(2,P(3,:)>0),'r+');
      hold off;
      axis image;
      title('currently active features');
      %
      % show tracks from first to current
      subplot(2,1,2);
      title(['tracks from frame ' num2str(f1) ' to frame ' num2str(f)]);
      % assemble KLT output into distinct tracks T(:,f-f1+1,track) = [x;y]
      [T,v] = vgg_klt_parse(K);
      Isize = size(I); Isize = Isize([2 1]);
      vgg_klt_show_tracks(T(:,1:(f-f1+1),:), Isize, false);
      drawnow;
    end
end

if ~showLiveTracks, [T,v] = vgg_klt_parse(K); end;

% remove small tracks
%T = vgg_klt_clean_tracks(T, 6, 6); % Modified by varun, as was not
%working, and is not very important

% show cleaned tracks
delete(vH);
close(vH_out);
fprintf('Output tracks written to video file: %s\n',outVideo);
T=T(:,1:nFramesActual,:);
h = figure; imshow(If1); axis image; axis off;
Isize = size(If1); Isize = Isize([2 1]);
vgg_klt_show_tracks(T, Isize, false);

function img=drawFeatures(img,P,drawRadius)

if(size(img,3)==1), img=repmat(img,[1 1 3]); end;

for i=1:size(P,2)
  if(P(3,i)>0), drawColor=[1 0 0];
  else drawColor=[0 1 0];
  end
  img=drawCross(img,P(1,i),P(2,i),drawColor,drawRadius);
end

function img=drawCross(img,x,y,color,rad)
  x=round(x);y=round(y);
  [h,w,nCh]=size(img);
  xTop=max(1,x-3);
  yTop=max(1,y-rad);
  xBot=min(w,x+3);
  yBot=min(h,y+rad);

  for i=1:3
    img(yTop:yBot,xTop:xBot,i)=color(i);
  end

  xTop=max(1,x-rad);
  yTop=max(1,y-3);
  xBot=min(w,x+rad);
  yBot=min(h,y+3);

  for i=1:3
    img(yTop:yBot,xTop:xBot,i)=color(i);
  end
