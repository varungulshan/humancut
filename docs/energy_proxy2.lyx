#LyX 1.6.7 created this file. For more info see http://www.lyx.org/
\lyxformat 345
\begin_document
\begin_header
\textclass article
\begin_preamble
%\date{}
\usepackage{graphicx}
\usepackage[centerlast]{subfigure}
\setlength{\parindent}{0in}
\addtolength{\oddsidemargin}{-0.98in}
%\setlength{\oddsidemargin}{0in}
\setlength{\topmargin}{-0.5in}
\setlength{\textheight}{9.0in}
\setlength{\textwidth}{7.0in}
\usepackage[pagebackref=true,breaklinks=true,letterpaper=true,colorlinks,bookmarks=false]{hyperref}
\usepackage[english]{babel}
\end_preamble
\use_default_options true
\language english
\inputencoding auto
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\paperfontsize 11
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\cite_engine basic
\use_bibtopic false
\paperorientation portrait
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\author "" 
\author "" 
\end_header

\begin_body

\begin_layout Title
Segmenting humans in videos: Energy formulation and optimization
\end_layout

\begin_layout Standard
This document defines an energy function for segmenting a human.
 It introduces a parametrization of the human body parts.
 This parametrization, along with color models, segmentation variables and
 the observed video define an energy function.
\end_layout

\begin_layout Section
Notation
\end_layout

\begin_layout Standard
Following list enumerates the variables/parameters used in the energy formulatio
n:
\end_layout

\begin_layout Itemize
Index 
\begin_inset Formula $t$
\end_inset

 is used to index frames in a video.
 
\begin_inset Formula $t\in{\{1,\,\cdots\,,T\}}$
\end_inset


\end_layout

\begin_layout Itemize
Index 
\begin_inset Formula $i$
\end_inset

 is used to index pixels in a particular frame.
 
\begin_inset Formula $i\in{\{1,\,\cdots\,,N\}}$
\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Formula $\mathbf{x}$
\end_inset

 is the vector of all pixels in the video, i.e 
\begin_inset Formula $x_{i,t}$
\end_inset

 denotes the rgb value of the 
\begin_inset Formula $i^{th}$
\end_inset

 pixel in frame 
\begin_inset Formula $t$
\end_inset

.
 
\begin_inset Formula $\mathbf{x}_{t}$
\end_inset

is used to denote the vector of all pixels at time 
\begin_inset Formula $t$
\end_inset

.
\end_layout

\begin_layout Itemize
\begin_inset Formula $\mathbf{y}$
\end_inset

 is the vector of segmentation labels, i.e 
\begin_inset Formula $y_{i,t}$
\end_inset

 denotes the labeling of the 
\begin_inset Formula $i^{th}$
\end_inset

 pixel in frame 
\begin_inset Formula $t$
\end_inset

.
 
\begin_inset Formula $\mathbf{y}_{t}$
\end_inset

 is used to denote the labeling at time 
\begin_inset Formula $t$
\end_inset

.
\end_layout

\begin_layout Itemize
\begin_inset Formula $\Theta$
\end_inset

 denotes parametrization of the human body.
 It is represented as a vector.
 Different kinds of parametrizations can be used, and are described later.
\end_layout

\begin_layout Itemize
\begin_inset Formula $\mathbf{w}$
\end_inset

 denotes the appearance models.
 They maybe global color models or might correspond to specific parts.
\end_layout

\begin_layout Section
Energy function
\end_layout

\begin_layout Standard
The energy function at the video level can be written as:
\begin_inset Formula \[
E(\mathbf{y},\mathbf{w},\Theta|\mathbf{x})=\sum_{t=1}^{T}E_{{\rm img}}(\mathbf{y}_{t},\mathbf{w}_{t},\Theta_{t}|\mathbf{x}_{t})+\sum_{t=1}^{T-1}E_{{\rm track}}(\mathbf{y}_{t},\mathbf{y}_{t+1},\Theta_{t},\Theta_{t+1}|\mathbf{x}_{t},\mathbf{x}_{t+1})\]

\end_inset


\end_layout

\begin_layout Standard
where the 
\begin_inset Formula $E_{{\rm img}}(\mathbf{y}_{t},\mathbf{w}_{t},\Theta_{t}|\mathbf{x}_{t})$
\end_inset

 term captures the energy function at a frame level (in a very similar fashion
 to PoseCut
\begin_inset space ~
\end_inset


\begin_inset CommandInset citation
LatexCommand cite
key "Bray06"

\end_inset

).
 The 
\begin_inset Formula $E_{{\rm track}}()$
\end_inset

 term captures temporal interactions between consecutive segmentations and
 poses.
\end_layout

\begin_layout Subsection
Energy function at frame level
\end_layout

\begin_layout Standard
The energy function at the image level is similar to the one used in PoseCut
\begin_inset space ~
\end_inset


\begin_inset CommandInset citation
LatexCommand cite
key "Bray06"

\end_inset

.
 It consists of unary terms involving color models and a shape prior induced
 by the human model.
 The unary terms for the foreground are obtained using part based color
 models, and these color models are shared across the entire video.
 The unary terms for the background are obtained using global color models,
 with a color model learnt for each frame separately.
 The pairwise terms enforce label consistency of neighbouring pixels.
 For simplicity of notation, the subscript 
\begin_inset Formula $t$
\end_inset

 will be dropped from each vector, as all the variables in this function
 belong to the same frame.
\begin_inset Formula \begin{equation}
E_{{\rm img}}(\mathbf{y},\mathbf{w},\Theta|\mathbf{x})=\gamma_{1}\sum_{i=1}^{N}U_{{\rm color}}(y_{i},\mathbf{w}|x_{i})+\gamma_{2}\sum_{i=1}^{N}U_{{\rm shape}}(y_{i},\Theta,i)+\gamma_{3}\sum_{(i,j)\in\mathcal{N}}V_{{\rm edge}}(y_{i},y_{j}|x_{i},x_{j})\label{eq:frameEnergy}\end{equation}

\end_inset


\end_layout

\begin_layout Standard
Where the individual terms of the energy function are defined as:
\end_layout

\begin_layout Itemize
\begin_inset Formula $U_{{\rm color}}(y_{i},\mathbf{w}|x_{i})$
\end_inset

 is the color likelihood unary as used in grabCut
\begin_inset space ~
\end_inset


\begin_inset CommandInset citation
LatexCommand cite
key "Rother04"

\end_inset

:
\begin_inset Formula \[
U_{{\rm color}}(y_{i},\mathbf{w}|x_{i})=\begin{cases}
-\log\Bigl(p(x_{i}|\mathbf{w}_{fg})\Bigr) & {\rm if}\,\,\, y_{i}=1\\
-\log\Bigl(p(x_{i}|\mathbf{w}_{fg})\Bigr) & {\rm if}\,\,\, y_{i}=0\end{cases}\]

\end_inset

The color models 
\begin_inset Formula $\mathbf{w}_{fg}$
\end_inset

 and 
\begin_inset Formula $\mathbf{w}_{bg}$
\end_inset

 are parametrized as gaussian mixture models.
\end_layout

\begin_layout Itemize
\begin_inset Formula $U_{{\rm shape}}(\mathbf{y},\Theta,i)$
\end_inset

 is a unary term which penalises shape deviations from the underlying human
 model represented by 
\begin_inset Formula $\Theta$
\end_inset

.
 Note that the pixel position 
\begin_inset Formula $i$
\end_inset

 is also a input to this function.
 This term is described later in Section
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "sec:shapeUnary"

\end_inset

, after the parametrization of the human model.
\end_layout

\begin_layout Itemize
\begin_inset Formula $V_{{\rm edge}}(y_{i},y_{j}|x_{i},x_{j})$
\end_inset

 is a contrast dependant pairwise term as described in grabCut
\begin_inset space ~
\end_inset


\begin_inset CommandInset citation
LatexCommand cite
key "Rother04"

\end_inset

:
\begin_inset Formula \[
V_{{\rm edge}}(y_{i},y_{j}|x_{i},x_{j})=(y_{i}\neq y_{j})\exp(-\beta\|x_{i}-x_{j}\|^{2})\]

\end_inset


\end_layout

\begin_layout Subsection
Energy function at temporal level
\end_layout

\begin_layout Standard
The term 
\begin_inset Formula $E_{{\rm track}}(\mathbf{y}_{t},\mathbf{y}_{t+1},\Theta_{t},\Theta_{t+1}|\mathbf{x}_{t},\mathbf{x}_{t+1})$
\end_inset

 captures the terms that span across two adjacent frames (hence temporal).
 The exact form of this energy function depends upon the representation
 of the human.
 Ref.
 section
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "sec:temporalEnergy"

\end_inset

 for an explicit form of this energy for different human representation.
 
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement t
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
centering
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/jointsModel.pdf
	lyxscale 40
	width 50col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Joints model parametrization of human body.
 The parameter vector 
\begin_inset Formula $\Theta$
\end_inset

 for the human consists of 2D coordinates of each joint.
 There is a total of 
\begin_inset Formula $13$
\end_inset

 joints, giving a 
\begin_inset Formula $26$
\end_inset

 dimensional representation.
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "Flo:jointsModel"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Human model and parametrization
\end_layout

\begin_layout Standard
This section discusses possible parametrization of human models, and how
 they can be used to induce a shape prior term on the segmentation.
\end_layout

\begin_layout Subsection
Joints model
\end_layout

\begin_layout Standard
This parametrization is inspired by the H3D dataset
\begin_inset space ~
\end_inset


\begin_inset CommandInset citation
LatexCommand cite
key "Bourdev09"

\end_inset

.
 The parameter vector 
\begin_inset Formula $\Theta$
\end_inset

 stores the coordinates of the joints of each body part.
 This is shown in Fig.
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "Flo:jointsModel"

\end_inset

.
 This representation does not capture the width of the parts and thus needs
 to be augmented with those values.
 For now, the width of a part is assumed to be a function of the length
 of each body part and not a separate parameter.
 So there is no need to represent the widths in the parameter 
\begin_inset Formula $\Theta$
\end_inset

.
 Ref.
 Section
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "sec:shapeUnary"

\end_inset

 for details.
\end_layout

\begin_layout Section
Human model to shape unary
\begin_inset CommandInset label
LatexCommand label
name "sec:shapeUnary"

\end_inset


\end_layout

\begin_layout Standard
The parameteric representation 
\begin_inset Formula $\Theta$
\end_inset

 of the human model is used in the shape prior term in
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand eqref
reference "eq:frameEnergy"

\end_inset

.
 This conversion is described for different human models in the following
 sections.
\end_layout

\begin_layout Subsection
Joints model to shape unary
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
centering
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
subfigure[Part measurements]{
\end_layout

\end_inset


\begin_inset Graphics
	filename images/jointToShape.pdf
	lyxscale 50
	width 40col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\begin_layout Plain Layout


\backslash
subfigure[Shape Prior]{
\end_layout

\end_inset


\begin_inset Graphics
	filename images/jointsModel_posterior.png
	lyxscale 50
	width 40col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Converting a joint based representation to parts based representation for
 obtaining a shape prior.
 (a) Shows how the shoulder and elbow joints are converted to the upper
 arm part.
 The part is indexed by 
\begin_inset Formula $p$
\end_inset

.
 The shoulder and the elbow joint specify the length of the arm 
\begin_inset Formula $l(p)$
\end_inset

.
 The width of the arm 
\begin_inset Formula $w(p)$
\end_inset

 is a fixed factor of the length.
 The border 
\begin_inset Formula $b(p)$
\end_inset

 around the arm is also a fixed factor of the length.
 (b) Shape prior 
\begin_inset Formula $\Psi(i,\Theta)$
\end_inset

, generated by taking a distance transform upto distance 
\begin_inset Formula $b(p)$
\end_inset

 from each part.
 See
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand eqref
reference "eq:shapePrior"

\end_inset

 and
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand eqref
reference "eq:shapePriorPart"

\end_inset

 for the definition of 
\begin_inset Formula $\Psi(i,\Theta)$
\end_inset

.
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "Flo:jointToShape"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
The joints are used to obtain locations of body parts.
 Let 
\begin_inset Formula $p$
\end_inset

 index over body parts.
 Each part 
\begin_inset Formula $p$
\end_inset

 has a certain thickness 
\begin_inset Formula $w(p)$
\end_inset

 and a region of uncertainity of width 
\begin_inset Formula $b(p)$
\end_inset

 around it.
 This conversion from joints to body part is illustrated in Fig.
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "Flo:jointToShape"

\end_inset

.
 Given the positions of the joints in 
\begin_inset Formula $\Theta$
\end_inset

, for each part 
\begin_inset Formula $p$
\end_inset

 of the body, its polygon can be obtained as some function 
\begin_inset Formula ${\rm Plgn}(\Theta,p)$
\end_inset

.
 Given the polygon for the part, the shape posterior for part 
\begin_inset Formula $p$
\end_inset

 is given as:
\begin_inset Formula \begin{equation}
\Psi_{p}(i,\Theta)=\begin{cases}
1 & {\rm if}\,\,\, i\in{\rm Plgn}(\Theta,p)\\
1-\frac{d(i,{\rm Plgn}(\Theta,p))}{b(p)} & {\rm if}\,\,\,0<d(i,{\rm Plgn}(\Theta,p))\leq b(p)\\
0 & {\rm if}\,\,\, d(i,{\rm Plgn}(\Theta,p))>b(p)\end{cases}\label{eq:shapePriorPart}\end{equation}

\end_inset


\end_layout

\begin_layout Standard
where 
\begin_inset Formula $i$
\end_inset

 indexes over pixels in the above equation, 
\begin_inset Formula $d(i,{\rm Plgn}(\Theta,p))$
\end_inset

 denotes the eucledian distance of pixel 
\begin_inset Formula $i$
\end_inset

 from the polygon 
\begin_inset Formula ${\rm Plgn}(\Theta,p)$
\end_inset

 and 
\begin_inset Formula $b(p)$
\end_inset

 is the uncertanity region around part 
\begin_inset Formula $p$
\end_inset

 as shown in Fig.
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "Flo:jointToShape"

\end_inset

.
 The shape posterior for the entire human body is just a supremum of the
 shape prior's of each part:
\begin_inset Formula \begin{equation}
\Psi(i,\Theta)=\max_{p\in[1,P]}\Psi_{p}(i,\Theta)\label{eq:shapePrior}\end{equation}

\end_inset


\end_layout

\begin_layout Standard
The above shape prior is used to define the shape unary in
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand eqref
reference "eq:frameEnergy"

\end_inset

 as follows:
\begin_inset Formula \begin{equation}
U_{{\rm shape}}(y_{i},\Theta,i)=\begin{cases}
-\log\bigl(\Psi(i,\Theta)\bigr) & {\rm if}\,\,\, y_{i}=1\\
-\log\bigl(1-\Psi(i,\Theta)\bigr) & {\rm if}\,\,\, y_{i}=0\end{cases}\end{equation}

\end_inset


\end_layout

\begin_layout Standard
Note that when 
\begin_inset Formula $\Psi(i,\Theta)\in\{0,1\}$
\end_inset

 then the unary term 
\begin_inset Formula $U_{{\rm shape}}(y_{i},\Theta,i)$
\end_inset

 acts as a hard constraint (because the 
\begin_inset Formula $-\log()$
\end_inset

 term becomes 
\begin_inset Formula $\infty$
\end_inset

).
 
\end_layout

\begin_layout Section
Human model to temporal energy
\begin_inset CommandInset label
LatexCommand label
name "sec:temporalEnergy"

\end_inset


\end_layout

\begin_layout Standard
Different human models 
\begin_inset Formula $\Theta_{t}$
\end_inset

 across time induce a temporal energy to ensure changes in human models
 are consistent with motion in the video.
 Following sections detail this energy for different human representations.
\end_layout

\begin_layout Subsection
Joints model to temporal energy
\end_layout

\begin_layout Standard
The high level temporal energy function is written as 
\begin_inset Formula $E_{{\rm track}}(\mathbf{y}_{t},\mathbf{y}_{t+1},\Theta_{t},\Theta_{t+1}|\mathbf{x}_{t},\mathbf{x}_{t+1})$
\end_inset

.
 In the joints model, the temporal energy does not depend on the segmentation,
 so we can drop the segmentation variables and call it 
\begin_inset Formula $E_{{\rm track}}(\Theta_{t},\Theta_{t+1}|\mathbf{x}_{t},\mathbf{x}_{t+1})$
\end_inset

.
 Lets assume that we are given pre-computed point correpondances between
 frames 
\begin_inset Formula $t$
\end_inset

 and 
\begin_inset Formula $t+1$
\end_inset

.
 We use the following notation to describe the energy function:
\end_layout

\begin_layout Itemize
\begin_inset Formula $i$
\end_inset

 is used to index over joints.
 So 
\begin_inset Formula $\Theta_{t}(i)$
\end_inset

 represents a 2D vector with the x and y coordinates of the 
\begin_inset Formula $i^{{\rm th}}$
\end_inset

 joint.
\end_layout

\begin_layout Itemize
\begin_inset Formula $k$
\end_inset

 is used to index over point correspondances.
\end_layout

\begin_layout Itemize
\begin_inset Formula $\mathbf{p}_{t}(k)$
\end_inset

 is the position of the 
\begin_inset Formula $k^{th}$
\end_inset

 point correspondance at time 
\begin_inset Formula $t$
\end_inset

 and analogously for 
\begin_inset Formula $\mathbf{p}_{t+1}(k)$
\end_inset

.
\end_layout

\begin_layout Standard
Lets write down the energy for a specific joint indexed by 
\begin_inset Formula $i$
\end_inset

 :
\begin_inset Formula \begin{eqnarray*}
E_{{\rm joint}}(\Theta_{t}(i),\Theta_{t+1}(i)|\mathbf{p}_{t},\mathbf{p}_{t+1}) & = & E_{{\rm fwd}}(\Theta_{t}(i),\Theta_{t+1}(i)|\mathbf{p}_{t},\mathbf{p}_{t+1})+E_{{\rm bwd}}(\Theta_{t}(i),\Theta_{t+1}(i)|\mathbf{p}_{t},\mathbf{p}_{t+1})\end{eqnarray*}

\end_inset


\end_layout

\begin_layout Standard
where the forward and backward energies are symettric and given by:
\end_layout

\begin_layout Standard
\begin_inset Formula \begin{eqnarray*}
E_{{\rm fwd}}(\Theta_{t}(i),\Theta_{t+1}(i)|\mathbf{p}_{t},\mathbf{p}_{t+1}) & = & \sum_{k}w\bigl(\Theta_{t}(i),\mathbf{p}_{t}(k)\bigr)\|(\Theta_{t+1}(i)-\Theta_{t}(i))-(\mathbf{p}_{t+1}(k)-\mathbf{p}_{t}(k))\|\\
E_{{\rm bwd}}(\Theta_{t}(i),\Theta_{t+1}(i)|\mathbf{p}_{t},\mathbf{p}_{t+1}) & = & \sum_{k}w\bigl(\Theta_{t+1}(i),\mathbf{p}_{t+1}(k)\bigr)\|(\Theta_{t}(i)-\Theta_{t+1}(i))-(\mathbf{p}_{t}(k)-\mathbf{p}_{t+1}(k))\|\end{eqnarray*}

\end_inset


\end_layout

\begin_layout Standard
where 
\begin_inset Formula $w(\Theta_{t}(i),\mathbf{p}_{t}(k))$
\end_inset

 is a weighing function that varies with the distance between the joint
 
\begin_inset Formula $\Theta_{t}(i$
\end_inset

) and the coordinate of the point correspondance 
\begin_inset Formula $\mathbf{p}_{t}(k)$
\end_inset

:
\begin_inset Formula \[
w(\mathbf{v}_{1},\mathbf{v}_{2})=\begin{cases}
0 & {\rm if }\,\, d(\mathbf{v}_{1},\mathbf{v}_{2})>d_{{\rm thresh}}\\
1-\frac{d(\mathbf{v}_{1},\mathbf{v}_{2})}{d_{{\rm thresh}}} & {\rm if }\,\, d(\mathbf{v}_{1},\mathbf{v}_{2})\leq d_{thresh}\end{cases}\]

\end_inset


\end_layout

\begin_layout Standard
where 
\begin_inset Formula $d(\mathbf{v}_{1},\mathbf{v}_{2})$
\end_inset

 is the eucledian distance between the 2D points 
\begin_inset Formula $\mathbf{v}_{1}$
\end_inset

 and 
\begin_inset Formula $\mathbf{v}_{2}$
\end_inset

.
 The complete energy function between two frames is the summation across
 all joints:
\begin_inset Formula \[
E_{track}(\Theta_{t},\Theta_{t+1}|\mathbf{x}_{t},\mathbf{x}_{t+1})=\gamma_{4}\sum_{i=1}^{N}E_{{\rm joint}}(\Theta_{t}(i),\Theta_{t+1}(i)|\mathbf{p}_{t},\mathbf{p}_{t+1})\]

\end_inset


\end_layout

\begin_layout Section
Optimizing the energy function
\end_layout

\begin_layout Standard
Currently only the frame level energy function has been defined (in
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand eqref
reference "eq:frameEnergy"

\end_inset

).
 Lets discuss how we can optimize the energy function setup in
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand eqref
reference "eq:frameEnergy"

\end_inset

.
 The formulation and discussion below is an adaption from PoseCut
\begin_inset space ~
\end_inset


\begin_inset CommandInset citation
LatexCommand cite
key "Bray06"

\end_inset

.
 Recalling the energy function:
\begin_inset Formula \begin{equation}
E_{img}(\mathbf{y},\mathbf{w},\Theta|\mathbf{x})=\gamma_{1}\sum_{i=1}^{N}U_{color}(y_{i},\mathbf{w}|x_{i})+\gamma_{2}\sum_{i=1}^{N}U_{shape}(y_{i},\Theta,i)+\gamma_{3}\sum_{(i,j)\in\mathcal{N}}V_{edge}(y_{i},y_{j}|x_{i},x_{j})\end{equation}

\end_inset


\end_layout

\begin_layout Standard
The above energy function has three unknowns, viz.
 the segmentation labels 
\begin_inset Formula $\mathbf{y}$
\end_inset

, the color models 
\begin_inset Formula $\mathbf{w}$
\end_inset

 and the human model 
\begin_inset Formula $\Theta$
\end_inset

.
 It is not possible to optimize over them jointly, hence an alternation
 strategy is used.
 For an alternation, we need to make an initialization for all the three
 variables.
 This is described as follows.
\end_layout

\begin_layout Subsection
Initialization
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
centering
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Graphics
	filename images/colorModelInit.png
	lyxscale 50
	width 40col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Initializing color models 
\begin_inset Formula $\mathbf{w}$
\end_inset

 given initial human model 
\begin_inset Formula $\Theta$
\end_inset

.
 The dark blue region gives seed pixels for learning 
\begin_inset Formula $\mathbf{w}_{fg}$
\end_inset

 and the light blue region gives seed pixels for learning 
\begin_inset Formula $\mathbf{w}_{bg}$
\end_inset

.
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "Flo:initColorModel"

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
The initialization is done manually at the moment.
 For every frame 
\begin_inset Formula $t$
\end_inset

 in the video, the human model 
\begin_inset Formula $\Theta_{t}$
\end_inset

 is specified manually by marking joints on the body using the H3D tool
\begin_inset space ~
\end_inset


\begin_inset CommandInset citation
LatexCommand cite
key "Bourdev09"

\end_inset

.
 For simplicity, the subscript 
\begin_inset Formula $t$
\end_inset

 is omitted from now on.
 We can use the intial 
\begin_inset Formula $\Theta$
\end_inset

 to obtain seed pixels for initializing the color model 
\begin_inset Formula $\mathbf{w}$
\end_inset

.
 This is depicted in Fig.
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "Flo:initColorModel"

\end_inset

.
 Given a 
\begin_inset Formula $\Theta$
\end_inset

 and a 
\begin_inset Formula $\mathbf{w}$
\end_inset

, we can initialize 
\begin_inset Formula $\mathbf{y}$
\end_inset

 by simply minimizing
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand eqref
reference "eq:frameEnergy"

\end_inset

.
\end_layout

\begin_layout Subsection
Alternation
\end_layout

\begin_layout Standard
Given an intial estimate of 
\begin_inset Formula $(\mathbf{y},\mathbf{w},\Theta)$
\end_inset

, the alternation proceeds as follows.
 Define:
\begin_inset Formula \[
E_{frame}^{2}(\mathbf{w},\Theta)=\min_{\mathbf{y}}E_{frame}(\mathbf{y},\mathbf{w},\Theta)\]

\end_inset

i.e, minimize out the segmentation variable from the energy function.
 This is efficient as the function 
\begin_inset Formula $E_{frame}^{2}(\mathbf{w},\Theta)$
\end_inset

 can be evaluated efficiently using dynamic graph cuts
\begin_inset space ~
\end_inset


\begin_inset CommandInset citation
LatexCommand cite
key "Kohli07"

\end_inset

 for slowly changing 
\begin_inset Formula $(\mathbf{w},\Theta)$
\end_inset

.
 The first step of the alternation is to minimize wrt to the human model
 
\begin_inset Formula $\Theta$
\end_inset

.
 As the energy function 
\begin_inset Formula $E_{frame}(\mathbf{y},\mathbf{w},\Theta)$
\end_inset

 does not have an analytic representation in terms of 
\begin_inset Formula $\Theta$
\end_inset

, the gradients for the minimization need to be computed empirically.
 The empirical partial derivatives are computed as:
\begin_inset Formula \[
\frac{\partial E_{frame}^{2}(\mathbf{w},\Theta)}{\partial\Theta_{i}}\approx\frac{E_{frame}^{2}(\mathbf{w},\Theta+e_{i}\Delta\Theta)-E_{frame}^{2}(\mathbf{w},\Theta)}{\Delta\Theta}\]

\end_inset


\end_layout

\begin_layout Standard
where 
\begin_inset Formula $e_{i}$
\end_inset

 is a unit vector will all zeros and 
\begin_inset Formula $1$
\end_inset

 at index 
\begin_inset Formula $i$
\end_inset

.
 Using the above partial derivatives , we can write down the gradient descent
 step as:
\begin_inset Formula \[
\Theta_{+1}=\Theta-\alpha\nabla_{\Theta}E_{frame}^{2}(\mathbf{w},\Theta)\]

\end_inset


\end_layout

\begin_layout Standard
The above update of 
\begin_inset Formula $\Theta$
\end_inset

 is repeated until a termination criteria to give the updated 
\begin_inset Formula $\Theta$
\end_inset

 which we denote as 
\begin_inset Formula $\Theta_{new}$
\end_inset

.
 The next step of the alternation involves updating 
\begin_inset Formula $\mathbf{y}$
\end_inset

 and 
\begin_inset Formula $\mathbf{w}$
\end_inset

 in a GrabCut
\begin_inset space ~
\end_inset


\begin_inset CommandInset citation
LatexCommand cite
key "Rother04"

\end_inset

 like fashion:
\begin_inset Formula \[
\mathbf{y}_{+1}=\arg\min_{\mathbf{y}}E_{frame}(\mathbf{y},\mathbf{w},\Theta_{new})\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula \[
\mathbf{w}_{+1}=\arg\min_{\mathbf{w}}E_{frame}(\mathbf{y}_{+1},\mathbf{w},\Theta_{new})\]

\end_inset


\end_layout

\begin_layout Standard
The above two steps are repeated a certain number of times as in GrabCut
 to give the updated segmentation and color models which we denote as 
\begin_inset Formula $\mathbf{y}_{new}$
\end_inset

 and 
\begin_inset Formula $\mathbf{w}_{new}$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "/home/varun/windows/vgg/reports/bib/shortstrings,/home/varun/windows/vgg/reports/bib/myBib,/home/varun/windows/vgg/reports/bib/vggroup"
options "ieeetr"

\end_inset


\end_layout

\end_body
\end_document
