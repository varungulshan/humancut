mex -O code/cpp/mex_setupTransductionGraph.cpp -outdir code/mexedFiles
mex -O code/cpp/dynamicGC_matlab/mexDGC.cpp code/cpp/dynamicGC_matlab/graph.cpp -I./code/cpp/dynamicGC_matlab/ -outdir code/mexedFiles
mex -O code/cpp/dynamicGC_matlab/mex_graphCut.cpp code/cpp/dynamicGC_matlab/graph.cpp -I./code/cpp/dynamicGC_matlab/ -outdir code/mexedFiles
mex -O code/cpp/dynamicGC_matlab/mex_graphCut_edgeMap.cpp code/cpp/dynamicGC_matlab/graph.cpp -I./code/cpp/dynamicGC_matlab/ -outdir code/mexedFiles
%mex -O classes/@flowPropagatorEdgeLearn2/mex_getNNdata.cpp -outdir code/mexedFiles
%mex -O classes/@flowPropagatorEdgeLearn2/mex_getNNdata_srch.cpp -outdir code/mexedFiles
